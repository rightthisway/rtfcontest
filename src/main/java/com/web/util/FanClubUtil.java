package com.web.util;

public class FanClubUtil {

	public static Integer maxRows = 10;
	
	public static final String ABUSETYPE_VIDEO = "VIDEO";
	public static final String ABUSETYPE_VIDEO_COMMENT = "VIDEOCOMMENT";
	public static final String ABUSETYPE_POST = "POST";
	public static final String ABUSETYPE_POST_COMMENT = "POSTCOMMENT";
	public static final String ABUSETYPE_FANCLUB = "FANCLUB";
	public static final String ABUSETYPE_FANCLUB_EVENT = "FANCLUBEVENT";
	
	public static final String anonymousErrorMessage = "Only members can access. Please join the fan club.";
	public static final String FANCLUB_ABUSE = "Abuse report captured successfully.We will investigate this fan club."; 
	public static final String FANCLUB_EVENT_ABUSE = "Abuse report captured successfully. We will investigate this fan club event.";
	public static final String FANCLUB_VIDEO_ABUSE = "Abuse report captured successfully. We will investigate this fan club Video.";
	public static final String FANCLUB_VIDEO_CMMT_ABUSE = "Abuse report captured successfully. We will investigate this fan club comment.";
	public static final String FANCLUB_POST_CMMT_ABUSE = "Abuse report captured successfully. We will investigate this fan club comment.";
	public static final String FANCLUB_POST_ABUSE = "Abuse report captured successfully. We will investigate this fan club post.";
	public static final String FANCLUB_RTF_VIDEO_COMMON = "Abuse report captured successfully. We will investigate the contents ";

}
