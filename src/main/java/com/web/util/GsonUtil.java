package com.web.util;

import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtil {
	
	
	static Gson gson = null;
	
	public static Gson  getGsonInstance() {
		
		if( gson == null ) {
			 //gson = new GsonBuilder().setPrettyPrinting().create();
			 gson = new GsonBuilder().serializeNulls().create();
			
		}
		 return gson;
		
	}
	
	static ObjectMapper mapper = new ObjectMapper();
	public static ObjectMapper getJasksonObjMapper() {
		if(mapper == null) {
			mapper = new ObjectMapper();
		}
		return mapper;
	}


}
