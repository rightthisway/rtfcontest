package com.web.rtfservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class QuizDashBoardServlet
 */

@WebServlet("/GetDashboardInfoOld")
public class QuizDashBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static long conuter = 0;
       
  
    public QuizDashBoardServlet() {
        super();       
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		String appVersion = request.getParameter("aVn");
		Integer customerId=null;
		try {
			
			CassCustomer customer = null;
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
				 
				try {
					customerId = Integer.parseInt(customerIdStr.trim());
					customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
					
					if(customer == null) {
						resMsg = "Customer Id is Invalid:"+customerIdStr;
						//error.setDesc("Customer Id is Invalid");
						error.setDesc(URLUtil.genericErrorMsg);
						dashboardInfo.setErr(error);
						dashboardInfo.setSts(0);
						//return dashboardInfo; // TODO GSON
					}
					
					Integer cartCount = 0;
					Connection conn = DatabaseConnections.getZonesApiConnectionOld();
					Statement stat = conn.createStatement();
					ResultSet rs = stat.executeQuery("select count(*) from rtf_customer_cart where cust_id="+customer.getId()+" AND cartstatus='ACTIVE'");
					if(rs.next()){
						cartCount = rs.getInt(1);
					}
					dashboardInfo.setCartCount(cartCount);
				} catch(Exception e) {
					resMsg = "Customer Id Not Exist:"+customerIdStr;
					//error.setDesc("Customer Id Not Exist");
					error.setDesc(URLUtil.genericErrorMsg);
					dashboardInfo.setErr(error);
					dashboardInfo.setSts(0);
					//return dashboardInfo; // TODO GSON
					e.printStackTrace();
				}
			}
			
			dashboardInfo.setSts(1);
			dashboardInfo.setCust(customer);
			dashboardInfo.sethRwds(true);
			
			
			resMsg = "Success:"+customerIdStr;
			//TrackingUtils.contestDashboardAPITracking(request, ,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Dashboard Information";
			e.printStackTrace();
			//error.setDesc("Error occured while Fetching Dashboard Information");
			error.setDesc(URLUtil.genericErrorMsg);
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			//return dashboardInfo; // TODO GSON 
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETDASHBOARDINFO, resMsg, null, 
					customerId, start, new Date(), request.getHeader("X-Forwarded-For"),appVersion,dashboardInfo.getSts(),null);
			//log.info("GETDASH INFO: "+customerIdStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}	
			String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(dashboardInfo);
			System.out.print(jsondashboardInfo);
			PrintWriter out = response.getWriter();
	        response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        out.print(jsondashboardInfo);
	        out.flush(); 

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		conuter = conuter + 1;
		System.out.println("called  Post  saytime ");
		PrintWriter out = response.getWriter();
		out.print("<html><body><h1 align='center'>" +
		new Date().toString() +    "    -- Counter is Now " +     conuter  +   "</h1></body></html>");
	}
	
	

		
		
		public static void main(String a[]) {
			System.out.println();
	}
	
	
	
	
	
	

}
