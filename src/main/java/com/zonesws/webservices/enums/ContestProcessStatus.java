package com.zonesws.webservices.enums;

public enum ContestProcessStatus {
	ACTIVE,STARTED,WINNERCOMPUTED,GRANDWINNERCOMPUTED,ENDCONTEST;
}
