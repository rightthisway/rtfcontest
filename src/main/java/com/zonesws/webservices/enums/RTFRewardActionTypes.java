package com.zonesws.webservices.enums;

public enum RTFRewardActionTypes {
	//PROFILE_BASIC, PROFILE_FANDOM, VIDEO_LIKES,
	PRB, PRF, VLK, 
	//VIDEO_SHARES, VIDEO_WATCH, VIDEO_UPLOAD, 
	VSH, VVW, VUP, 
	//REFERRAL, LIVE_POOLING	
	RFR, LVP 
}
