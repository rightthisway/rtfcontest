package com.zonesws.webservices.enums;

public enum JackpotCreditType {

	NONE,GIFTCARD,TICKET,LIFELINE,REWARD,SUPERFANSTAR,POINTS
}
