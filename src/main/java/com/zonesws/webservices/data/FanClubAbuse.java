package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FanClubAbuse")
public class FanClubAbuse implements Serializable {

	private static final long serialVersionUID = -5002044330929886861L;

	private Integer id;
	private Integer cuId;
	private Integer fcId;
	private Integer eventId;
	private Integer postId;
	private Integer abuseId;
	private Date crDate;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getFcId() {
		return fcId;
	}
	public void setFcId(Integer fcId) {
		this.fcId = fcId;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public Integer getPostId() {
		return postId;
	}
	public void setPostId(Integer postId) {
		this.postId = postId;
	}
	public Integer getAbuseId() {
		return abuseId;
	}
	public void setAbuseId(Integer abuseId) {
		this.abuseId = abuseId;
	}
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	} 
	  
	

}
