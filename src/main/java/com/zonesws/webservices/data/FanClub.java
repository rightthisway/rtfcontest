package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FanClub")
public class FanClub implements Serializable {

	private static final long serialVersionUID = -5002044330929886861L;

	private Integer id;
	@JsonIgnore
	private Integer cuId;
	private Integer catId;
	private String title;
	private String desc;
	private String posterUrl;
	@JsonIgnore
	private String status;
	private Integer noOfMembers;
	
	@JsonIgnore
	private String crUserId;//created user id
	@JsonIgnore
	private String upUserId;//updated user id
	@JsonIgnore
	private Date crDate;
	@JsonIgnore
	private Date upDate;
	
	private Boolean isAdmin;
	private Boolean isBlocked;
	private Boolean isMember;
	private Boolean isDeleted;
	
	private String shTxt;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getCatId() {
		return catId;
	}
	public void setCatId(Integer catId) {
		this.catId = catId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getPosterUrl() {
		if(null == posterUrl) {
			posterUrl = "";
		}
		return posterUrl;
	}
	public void setPosterUrl(String posterUrl) {
		this.posterUrl = posterUrl;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getNoOfMembers() {
		if(noOfMembers == null || noOfMembers < 0) {
			noOfMembers=0;
		}
		return noOfMembers;
	}
	public void setNoOfMembers(Integer noOfMembers) {
		this.noOfMembers = noOfMembers;
	}
	public String getCrUserId() {
		return crUserId;
	}
	public void setCrUserId(String crUserId) {
		this.crUserId = crUserId;
	}
	public String getUpUserId() {
		return upUserId;
	}
	public void setUpUserId(String upUserId) {
		this.upUserId = upUserId;
	}
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
	public Date getUpDate() {
		return upDate;
	}
	public void setUpDate(Date upDate) {
		this.upDate = upDate;
	}
	public Boolean getIsAdmin() {
		if(null == isAdmin) {
			isAdmin = false;
		}
		return isAdmin;
	}
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Boolean getIsBlocked() {
		if(null == isBlocked) {
			isBlocked = false;
		}
		return isBlocked;
	}
	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	public Boolean getIsMember() {
		if(isMember == null) {
			isMember = false;
		}
		return isMember;
	}
	public void setIsMember(Boolean isMember) {
		this.isMember = isMember;
	}
	public Boolean getIsDeleted() {
		if(isDeleted == null) {
			isDeleted = false;
		}
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getShTxt() {
		shTxt = "Join to "+this.title+" Fan Club get connected with fans. You can download our apps from http://onelink.to/5xwz4g";
		return shTxt;
	}
	public void setShTxt(String shTxt) {
		this.shTxt = shTxt;
	}

}
