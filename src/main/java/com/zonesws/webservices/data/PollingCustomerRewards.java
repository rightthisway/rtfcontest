package com.zonesws.webservices.data;

public class PollingCustomerRewards {
	
	
	private Integer custId;
	private Integer crystalBall;
	private Integer hiFive;
	private Integer magicWand;
	private Integer goldBar;
	
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Integer getCrystalBall() {
		return crystalBall;
	}
	public void setCrystalBall(Integer crystalBall) {
		this.crystalBall = crystalBall;
	}
	public Integer getHiFive() {
		return hiFive;
	}
	public void setHiFive(Integer hiFive) {
		this.hiFive = hiFive;
	}
	public Integer getMagicWand() {
		return magicWand;
	}
	public void setMagicWand(Integer magicWand) {
		this.magicWand = magicWand;
	}
	public Integer getGoldBar() {
		return goldBar;
	}
	public void setGoldBar(Integer goldBar) {
		this.goldBar = goldBar;
	}
	
	

}
