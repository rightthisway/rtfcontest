package com.zonesws.webservices.data;

import java.io.Serializable;


public class PollingCategory implements Serializable {

	private static final long serialVersionUID = -5002044330929886861L;

	private Integer categoryId;
	private Integer contestId;	
	private String pollingType;
	private String status;
	
	
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public String getPollingType() {
		return pollingType;
	}
	public void setPollingType(String pollingType) {
		this.pollingType = pollingType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
