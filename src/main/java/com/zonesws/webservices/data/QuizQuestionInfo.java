package com.zonesws.webservices.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("QuizQuestionInfo")
public class QuizQuestionInfo {
	
	private Integer status;
	private Error error; 
	private QuizContestQuestions quizContestQuestion;
	private String message;
	private Integer noOfQuestions;
	private Integer pqLifeCount;
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public QuizContestQuestions getQuizContestQuestion() {
		return quizContestQuestion;
	}
	public void setQuizContestQuestion(QuizContestQuestions quizContestQuestion) {
		this.quizContestQuestion = quizContestQuestion;
	}
	public Integer getNoOfQuestions() {
		if(noOfQuestions == null) {
			noOfQuestions = 0;
		}
		return noOfQuestions;
	}
	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}
	public Integer getPqLifeCount() {
		if(pqLifeCount == null) {
			pqLifeCount = 0;
		}
		return pqLifeCount;
	}
	public void setPqLifeCount(Integer pqLifeCount) {
		this.pqLifeCount = pqLifeCount;
	}
	
	
	
	
}
