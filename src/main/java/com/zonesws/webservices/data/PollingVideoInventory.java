package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

public class PollingVideoInventory implements Serializable {

	private static final long serialVersionUID = -5003088860929886861L;

	private Integer id;
	private String videoUrl;	
	private String status;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	}
