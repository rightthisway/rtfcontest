package com.zonesws.webservices.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FanClubEvent")
public class FanClubEvent implements Serializable {

	private static final long serialVersionUID = -5002044330929886861L;
	
	static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	static DateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	static DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	private Integer id;
	private Integer fcId;
	private Integer cuId;
	private String eName;
	private Date eDate;
	private Date eTime;
	private String venue;
	@JsonIgnore
	private String status;
	private Integer noIntr;
	private String pUrl;
	
	@JsonIgnore
	private String crUserId;//created user id
	@JsonIgnore
	private String upUserId;//updated user id
	@JsonIgnore
	private Date crDate;
	@JsonIgnore
	private Date upDate;

	private String eDateStr;
	private String eTimeStr;
	private Boolean liked = false;
	private Boolean isPstEv = false;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFcId() {
		return fcId;
	}

	public void setFcId(Integer fcId) {
		this.fcId = fcId;
	}

	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public Date geteDate() {
		return eDate;
	}

	public void seteDate(Date eDate) {
		this.eDate = eDate;
	}

	public Date geteTime() {
		return eTime;
	}

	public void seteTime(Date eTime) {
		this.eTime = eTime;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getNoIntr() {
		if(noIntr == null || noIntr < 0) {
			noIntr = 0;
		}
		return noIntr;
	}

	public void setNoIntr(Integer noIntr) {
		this.noIntr = noIntr;
	}

	public String getCrUserId() {
		return crUserId;
	}

	public void setCrUserId(String crUserId) {
		this.crUserId = crUserId;
	}

	public String getUpUserId() {
		return upUserId;
	}

	public void setUpUserId(String upUserId) {
		this.upUserId = upUserId;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	public Date getUpDate() {
		return upDate;
	}

	public void setUpDate(Date upDate) {
		this.upDate = upDate;
	}

	public String geteDateStr() {
		if(eDate == null) {
			eDateStr = "TBD";
			return eDateStr;
		}
		eDateStr = dateFormat.format(eDate);
		
		return eDateStr;
	}

	public void seteDateStr(String eDateStr) {
		this.eDateStr = eDateStr;
	}

	public String geteTimeStr() {
		if(eTime == null) {
			eTimeStr = "TBD";
			return eTimeStr;
		}
		eTimeStr = timeFormat.format(eTime);
		
		return eTimeStr;
	}

	public void seteTimeStr(String eTimeStr) {
		this.eTimeStr = eTimeStr;
	}

	public String getpUrl() {
		return pUrl;
	}

	public void setpUrl(String pUrl) {
		this.pUrl = pUrl;
	}

	public Boolean getLiked() {
		if(liked == null) {
			liked = false;
		}
		return liked;
	}

	public void setLiked(Boolean liked) {
		this.liked = liked;
	}

	public Boolean getIsPstEv() {
		try {
			Date now = new Date();
			
			if(eDate!= null && eTime != null) {
				Date eventDt = dateTimeFormat.parse(dateFormat.format(eDate)+" "+timeFormat.format(eTime));
				if(eventDt.before(now)) {
					isPstEv = true;
					return isPstEv;
				}
			} else if(eDate!= null) {
				if(eDate.before(dateFormat.parse(dateFormat.format(now)))){
					isPstEv = true;
					return isPstEv;
				}
			} 
			isPstEv = false;
			
		} catch(Exception e) {
			isPstEv = false;
		}
		return isPstEv;
	}

	public void setIsPstEv(Boolean isPstEv) {
		this.isPstEv = isPstEv;
	}
	
	

}
