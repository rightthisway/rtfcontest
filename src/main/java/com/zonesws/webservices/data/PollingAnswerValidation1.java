package com.zonesws.webservices.data;


import java.util.Date;
import com.quiz.cassandra.list.CassError;
import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("PollingAnswerValidation")
public class PollingAnswerValidation1 {	
	
	private Integer id;	
	private Integer sts;
	private CassError err; 
	private String msg;	
	private Integer contesId;
	private Integer categoryId;
	private Integer custId;
	private Integer questionId;
	private String correctAns;	
	private String userAns;
	private Boolean isAnsCorrect;		
	private String reward_type;	
	private Float reward_qty;
	private String remarks;	
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getContesId() {
		return contesId;
	}
	public void setContesId(Integer contesId) {
		this.contesId = contesId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getCorrectAns() {
		return correctAns;
	}
	public void setCorrectAns(String correctAns) {
		this.correctAns = correctAns;
	}

	public String getUserAns() {
		return userAns;
	}
	public void setUserAns(String userAns) {
		this.userAns = userAns;
	}

	public Boolean getIsAnsCorrect() {
		return isAnsCorrect;
	}
	public void setIsAnsCorrect(Boolean isAnsCorrect) {
		this.isAnsCorrect = isAnsCorrect;
	}	

	public String getReward_type() {
		return reward_type;
	}
	public void setReward_type(String reward_type) {
		this.reward_type = reward_type;
	}
	
	public Float getReward_qty() {
		return reward_qty;
	}
	public void setReward_qty(Float reward_qty) {
		this.reward_qty = reward_qty;
	}
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
}