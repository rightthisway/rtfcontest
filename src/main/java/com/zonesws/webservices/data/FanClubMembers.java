package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FanClubMembers")
public class FanClubMembers implements Serializable {

	private static final long serialVersionUID = -5002044330929886861L;

	private Integer id;
	private Integer fcId;
	private Integer cuId;
	@JsonIgnore
	private String status;
	
	@JsonIgnore
	private Date jnDate;
	@JsonIgnore
	private Date exDate;
	@JsonIgnore
	private Date crDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getFcId() {
		return fcId;
	}

	public void setFcId(Integer fcId) {
		this.fcId = fcId;
	}

	public Date getJnDate() {
		return jnDate;
	}

	public void setJnDate(Date jnDate) {
		this.jnDate = jnDate;
	}

	public Date getExDate() {
		return exDate;
	}

	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}


	
}
