package com.zonesws.webservices.data;

import java.io.Serializable;


public class Category implements Serializable {

	private static final long serialVersionUID = -5002044330929886861L;

	private Integer id;
	private String name;
	private String description;
	private String imageUrl;
	private String shTxt;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getShTxt() {
		shTxt = "Checkout "+name+" RTF TV Channel. You can download our apps from http://onelink.to/5xwz4g";
		return shTxt; 
	}
	public void setShTxt(String shTxt) {
		this.shTxt = shTxt;
	}
	
	
	
}
