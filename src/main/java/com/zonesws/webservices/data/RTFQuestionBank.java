package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

public class RTFQuestionBank  implements Serializable{
	
		private Integer id;
		private String question;
		private String optionA;
		private String optionB;
		private String optionC;		
		private String answer;		
		private String status;
		private String category;
		private String dificultyLevel;
		private Boolean factChecked;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getQuestion() {
			return question;
		}
		public void setQuestion(String question) {
			this.question = question;
		}
		public String getOptionA() {
			return optionA;
		}
		public void setOptionA(String optionA) {
			this.optionA = optionA;
		}
		public String getOptionB() {
			return optionB;
		}
		public void setOptionB(String optionB) {
			this.optionB = optionB;
		}
		public String getOptionC() {
			return optionC;
		}
		public void setOptionC(String optionC) {
			this.optionC = optionC;
		}
		public String getAnswer() {
			return answer;
		}
		public void setAnswer(String answer) {
			this.answer = answer;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		public String getDificultyLevel() {
			return dificultyLevel;
		}
		public void setDificultyLevel(String dificultyLevel) {
			this.dificultyLevel = dificultyLevel;
		}
		public Boolean getFactChecked() {
			return factChecked;
		}
		public void setFactChecked(Boolean factChecked) {
			this.factChecked = factChecked;
		}

}
