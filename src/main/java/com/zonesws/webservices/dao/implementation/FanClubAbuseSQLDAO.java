package com.zonesws.webservices.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.web.util.FanClubUtil;
import com.zonesws.webservices.data.FanClubAbuse;
import com.zonesws.webservices.utils.DatabaseConnections;

public class FanClubAbuseSQLDAO {
	private static Connection rtfZonesConnection; // remove this at deployment..
	
	
	public static FanClubAbuse saveFanClubAbuse(FanClubAbuse obj) {
		Calendar cal = Calendar.getInstance();
		String sql = " insert into "+ DatabaseConnections.quizApiLinkedServer +".fanclub_customer_reported_abuse_on_media(fanclub_id,customer_id,abuse_id,created_date)  "
				+ "values ("+obj.getFcId()+","+obj.getCuId()+","+obj.getAbuseId()+",'"+new Timestamp(obj.getCrDate().getTime())+"' ) ";
		try {
			System.out.println(sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Fan Club Abuse failed, no rows affected.");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					obj.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating Fan Club Abuse failed, no ID obtained.");
				}
			} catch (Exception ex) {
				//pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
	public static FanClubAbuse saveFanClubEventAbuse(FanClubAbuse obj) {
		Calendar cal = Calendar.getInstance();
		String sql = " insert into "+ DatabaseConnections.quizApiLinkedServer +".fanclub_customer_reported_abuse_on_media(fanclub_id,fanclub_event_id,customer_id,abuse_id,created_date)  "
				+ "values ("+obj.getFcId()+","+obj.getEventId()+","+obj.getCuId()+","+obj.getAbuseId()+",'"+new Timestamp(obj.getCrDate().getTime())+"' ) ";
		try {
			System.out.println(sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Fan Club Event Abuse failed, no rows affected.");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					obj.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating Fan Club Event Abuse failed, no ID obtained.");
				}
			} catch (Exception ex) {
				//pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	public static List<Integer>	fetchAbuseIdByActionType(String actionType,Integer custId, Integer sourceId) {
		List <Integer> abuseOptionReportedIds = null;
		
		String sql = " select abuse_id from "+DatabaseConnections.quizApiLinkedServer+".fanclub_customer_reported_abuse_on_media with(nolock) "
				+ " where customer_id = " + custId;
		 
		switch (actionType) {
			case FanClubUtil.ABUSETYPE_FANCLUB:
				sql += " AND fanclub_id = " + sourceId;				
				break;
			case FanClubUtil.ABUSETYPE_FANCLUB_EVENT:
				sql += " AND fanclub_event_id = " + sourceId;		
				break;
			case FanClubUtil.ABUSETYPE_POST:
				sql += " AND fanclub_posts_id = " + sourceId;		
				break;
			case FanClubUtil.ABUSETYPE_POST_COMMENT:
				sql += " AND fanclub_comments_id = " + sourceId;		
				break;
			case FanClubUtil.ABUSETYPE_VIDEO:
				sql += " AND fanclub_video_id = " + sourceId;		
				break;
			case FanClubUtil.ABUSETYPE_VIDEO_COMMENT:
				sql += " AND fanclub_id = " + sourceId;		
				break;
	
			default:
				break;
		}
		
			
				System.out.println("fetchAbuseOptionSelectedbyCustomerForFCPosts sql " + sql);
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			abuseOptionReportedIds = new ArrayList<Integer> ();
			while (rs.next()) {
				abuseOptionReportedIds.add(rs.getInt("abuse_id"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return abuseOptionReportedIds;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return abuseOptionReportedIds;
	}
	
}
