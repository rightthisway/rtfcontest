package com.zonesws.webservices.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.web.util.FanClubUtil;
import com.zonesws.webservices.data.FanClub;
import com.zonesws.webservices.utils.DatabaseConnections;

public class FanClubSQLDAO {
	private static Connection rtfZonesConnection; // remove this at deployment..

	public static FanClub getActiveFanClub(Integer fanClubId, Integer customerId) {
		/*String sql = "select id,title,description,poster_url,category_id,customer_id,created_user_id,updated_user_id,"
				+ "fanclub_status,no_of_members,created_date,updated_date "
				+ "from "+DatabaseConnections.quizApiLinkedServer+".fanclub_mst  where id= " + fanClubId+" and fanclub_status='ACTIVE' ";*/
		
		String sql = "select fc.id,title,description,poster_url,category_id,fanclub_status,no_of_members,fc.customer_id as adminCustId, "
				+ "CASE WHEN fcm.customer_id is null THEN 0 ELSE 1 END as isMember, fc.created_Date,fc.updated_date,fc.created_user_id,fc.updated_user_id from "
				+ ""+DatabaseConnections.quizApiLinkedServer+".fanclub_mst fc with(nolock) left join "
				+ ""+DatabaseConnections.quizApiLinkedServer+".fanclub_members fcm with(nolock) on fc.id = fcm.fanclub_mst_id "
				+ "and fcm.customer_id = "+customerId+" and fcm.member_status='JOIN' "
				+ "where fanclub_status='ACTIVE' and fc.id = "+fanClubId+" ";
		
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			FanClub fanClub = new FanClub();
			while (rs.next()) {
				fanClub.setId(rs.getInt("id"));
				fanClub.setCatId(rs.getInt("category_id"));
				fanClub.setCuId(rs.getInt("adminCustId"));
				fanClub.setDesc(rs.getNString("description"));
				fanClub.setNoOfMembers(rs.getInt("no_of_members"));
				fanClub.setPosterUrl(rs.getString("poster_url"));
				fanClub.setTitle(rs.getNString("title"));
				fanClub.setStatus(rs.getString("fanclub_status"));
				fanClub.setIsMember(rs.getBoolean("isMember"));
				if(customerId.equals(fanClub.getCuId())) {
					fanClub.setIsAdmin(true);
				}
				if(rs.getDate("created_Date") != null) {
					fanClub.setCrDate(new Date(rs.getTimestamp("created_Date").getTime()));	
				}
				if(rs.getDate("updated_date") != null) {
					fanClub.setUpDate(new Date(rs.getTimestamp("updated_date").getTime()));	
				}
				fanClub.setCrUserId(rs.getString("created_user_id"));
				fanClub.setUpUserId(rs.getString("updated_user_id"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return fanClub;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	
	
	public static List<FanClub> getAllActiveFanClubBySearchKeyAndPageNumber(String searchKey, Integer customerId,Integer pageNo) {
		
		String sql = "";
		if(null != customerId) {
			sql = "select distinct fc.id,title,description,poster_url,category_id,fanclub_status,no_of_members,fc.customer_id as adminCustId, "
					+ "CASE WHEN fcm.customer_id is null THEN 0 ELSE 1 END as isMember from "
					+ ""+DatabaseConnections.quizApiLinkedServer+".fanclub_mst fc with(nolock) left join "
					+ ""+DatabaseConnections.quizApiLinkedServer+".fanclub_members fcm with(nolock) on fc.id = fcm.fanclub_mst_id "
					+ "and fcm.customer_id = "+customerId+" and fcm.member_status='JOIN' "
					+ "where fanclub_status='ACTIVE' ";
		}else{
			sql = "select distinct fc.id,title,description,poster_url,category_id,fanclub_status,no_of_members,fc.customer_id as adminCustId,"
					+ "0 as isMember from "+DatabaseConnections.quizApiLinkedServer+".fanclub_mst fc with(nolock) where fanclub_status='ACTIVE' ";
		}
		 
		if(null != searchKey && !searchKey.isEmpty()) {
			sql += " and title like '%"+searchKey+"%' ";
		}
		sql +=" order by no_of_members desc OFFSET ("+pageNo+"-1)*"+FanClubUtil.maxRows+" ROWS FETCH NEXT "+FanClubUtil.maxRows+" ROWS ONLY";
		System.out.println(sql);
		Statement stmt = null;
		ResultSet rs = null;
		FanClub fanClub = null;
		List<FanClub> list = new ArrayList<FanClub>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				fanClub = new FanClub();
				fanClub.setId(rs.getInt("id"));
				fanClub.setCatId(rs.getInt("category_id"));
				fanClub.setCuId(rs.getInt("adminCustId"));
				fanClub.setDesc(rs.getNString("description"));
				fanClub.setNoOfMembers(rs.getInt("no_of_members"));
				fanClub.setPosterUrl(rs.getString("poster_url"));
				fanClub.setTitle(rs.getNString("title"));
				fanClub.setStatus(rs.getString("fanclub_status"));
				fanClub.setIsMember(rs.getBoolean("isMember"));
				if(customerId.equals(fanClub.getCuId())) {
					fanClub.setIsAdmin(true);
				}
				list.add(fanClub);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	
	public static List<FanClub> getAllFanClubByCustIdAndPageNumber(Integer customerId,Integer pageNo) {
		String sql = "select fc.id,title,description,poster_url,category_id,fanclub_status,no_of_members,fc.customer_id as adminCustId, "
				+ "CASE WHEN fcm.customer_id is null THEN 0 ELSE 1 END as isMember from "
				+ ""+DatabaseConnections.quizApiLinkedServer+".fanclub_mst fc with(nolock) inner join "
				+ ""+DatabaseConnections.quizApiLinkedServer+".fanclub_members fcm with(nolock) on fc.id = fcm.fanclub_mst_id and fcm.customer_id = "+customerId+" "
				+ "where fanclub_status='ACTIVE' ";
		 
		sql +=" order by no_of_members desc OFFSET ("+pageNo+"-1)*"+FanClubUtil.maxRows+" ROWS FETCH NEXT "+FanClubUtil.maxRows+" ROWS ONLY";
		
		Statement stmt = null;
		ResultSet rs = null;
		FanClub fanClub = null;
		List<FanClub> list = new ArrayList<FanClub>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				fanClub = new FanClub();
				fanClub.setId(rs.getInt("id"));
				fanClub.setCatId(rs.getInt("category_id"));
				fanClub.setCuId(rs.getInt("customer_id"));
				fanClub.setDesc(rs.getString("description"));
				fanClub.setNoOfMembers(rs.getInt("no_of_members"));
				fanClub.setPosterUrl(rs.getString("poster_url"));
				fanClub.setTitle(rs.getString("title"));
				fanClub.setStatus(rs.getString("fanclub_status"));
				if(customerId.equals(fanClub.getCuId())) {
					fanClub.setIsAdmin(true);
				}
				list.add(fanClub);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	
	public static List<FanClub> getAllMyFanClubsAndPageNumber(Integer customerId,Integer pageNo, String scKey) {
		
		String filter = "";
		if(null != scKey && !scKey.isEmpty()) {
			filter= " and fc.title like '%"+scKey+"%'";
		}
		
		String sql = "select distinct fc.id,title,description,poster_url,category_id,fanclub_status,no_of_members,fc.customer_id as adminCustId, "
				+ "CASE WHEN fcm.customer_id is null THEN 0 ELSE 1 END as isMember from "
				+ ""+DatabaseConnections.quizApiLinkedServer+".fanclub_mst fc with(nolock) inner join "
				+ ""+DatabaseConnections.quizApiLinkedServer+".fanclub_members fcm with(nolock) on fc.id = fcm.fanclub_mst_id "
				+ " and fcm.customer_id = "+customerId+" and fcm.member_status='JOIN' where 1=1 and fc.fanclub_status = 'ACTIVE' "+filter+" "
				+ " order by no_of_members desc OFFSET ("+pageNo+"-1)*"+FanClubUtil.maxRows+" ROWS FETCH NEXT "+FanClubUtil.maxRows+" ROWS ONLY";
		
		Statement stmt = null;
		ResultSet rs = null;
		FanClub fanClub = null;
		List<FanClub> list = new ArrayList<FanClub>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				fanClub = new FanClub();
				fanClub.setId(rs.getInt("id"));
				fanClub.setCatId(rs.getInt("category_id"));
				fanClub.setCuId(rs.getInt("adminCustId"));
				fanClub.setDesc(rs.getString("description"));
				fanClub.setNoOfMembers(rs.getInt("no_of_members"));
				fanClub.setPosterUrl(rs.getString("poster_url"));
				fanClub.setTitle(rs.getString("title"));
				fanClub.setStatus(rs.getString("fanclub_status"));
				if(customerId.equals(fanClub.getCuId())) {
					fanClub.setIsAdmin(true);
				} 
				if(!fanClub.getStatus().equals("ACTIVE")) {
					fanClub.setIsDeleted(true);
				}
				fanClub.setIsMember(true);
				list.add(fanClub);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	public static FanClub getActiveFanClubByTitleLike(String title, Integer fanClubId) {
		String sql = "select id,title from "+DatabaseConnections.quizApiLinkedServer+".fanclub_mst with(nolock) where title like '"+title+"' and fanclub_status='ACTIVE' ";
		
		if(null != fanClubId) {
			sql = sql+" and id not in ("+fanClubId+")";
		}
		
		Statement stmt = null;
		ResultSet rs = null;
		FanClub fanClub =null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				fanClub = new FanClub();
				fanClub.setId(rs.getInt("id")); 
				fanClub.setTitle(rs.getString("title")); 
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return fanClub;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	
	public static FanClub saveFanClub(FanClub fanClub) {
		Calendar cal = Calendar.getInstance();
		String sql = " insert into "+ DatabaseConnections.quizApiLinkedServer +".fanclub_mst(title,description,poster_url,"
				+ "category_id,customer_id,fanclub_status,no_of_members,created_date) "
				+ " values (N'"+fanClub.getTitle()+"', N'"+fanClub.getDesc()+"','"+fanClub.getPosterUrl()+"',"+fanClub.getCatId()+","
				+ ""+fanClub.getCuId()+",'"+fanClub.getStatus()+"',1,'"+new Timestamp(fanClub.getCrDate().getTime())+"' ) ";

		try {
			System.out.println(sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Fan Club failed, no rows affected.");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					fanClub.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating Fan Club failed, no ID obtained.");
				}
			} catch (Exception ex) {
				//pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fanClub;
	}
	
	
	public static Integer saveFanClubTransaction(Integer fanclubId) {
		Calendar cal = Calendar.getInstance();
		String sql = " insert into "+ DatabaseConnections.quizApiLinkedServer +".fanclub_trn(fanclub_mst_id,title,description,poster_url,"
				+ "category_id,customer_id,created_user_id,updated_user_id,fanclub_status,created_date,updated_date) "
				+ "select id,title,description,poster_url,category_id,customer_id,created_user_id,updated_user_id,fanclub_status,"
				+ "created_date,updated_date from  "+ DatabaseConnections.quizApiLinkedServer +".fanclub_mst with(nolock) where id="+fanclubId+"";

		try {
			System.out.println(sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				return null;
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					return generatedKeys.getInt(1);
				} else {
					return null;
				}
			} catch (Exception ex) {
				//pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	 
	public static int updateFanClub(FanClub fanClub) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_mst set title=N'"+fanClub.getTitle()+"',description=N'"+fanClub.getDesc()+"',"
				+ "category_id="+fanClub.getCatId()+", updated_date='"+new Timestamp(fanClub.getUpDate().getTime())+"' where id = "+fanClub.getId();
		int updateCnt = 0;
		try {
			System.out.println(sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
	
	public static int updateFanClubImage(Integer fanClubId, String imageUrl) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_mst set poster_url='"+imageUrl+"',"
				+ "updated_date='"+new Timestamp(new Date().getTime())+"' where id = "+fanClubId;
		int updateCnt = 0;
		try {
			System.out.println(sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
	
	public static int deleteFanClub(Integer fanClubId) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_mst set fanclub_status='DELETED',"
				+ "updated_date='"+new Timestamp(new Date().getTime())+"' where id = "+fanClubId;
		int updateCnt = 0;
		try {
			System.out.println(sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
	public static int updateFanClubMembersContForJoin(Integer fanClubId) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_mst set no_of_members=no_of_members+1 where id = "+fanClubId;
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
	public static int updateFanClubMembersContForExit(Integer fanClubId) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_mst set no_of_members=no_of_members-1 where id = "+fanClubId;
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}

}
