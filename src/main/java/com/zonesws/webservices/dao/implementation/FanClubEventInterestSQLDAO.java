package com.zonesws.webservices.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.AbuseReportInfo;
import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.list.MyVideoInfo;
import com.quiz.cassandra.list.PollingAnswerInfo;
import com.quiz.cassandra.list.PollingContest;
import com.quiz.cassandra.list.PollingQuestionInfo;
import com.quiz.cassandra.list.PollingRewardsInfo;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.PollingVideoRewardsConfig;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.service.PollingAnswerService;
import com.quiz.cassandra.service.PollingQuestionService;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.data.FanClubEventInterest;
import com.zonesws.webservices.data.PollingCategory;
import com.zonesws.webservices.data.PollingCustomerRewards;
import com.zonesws.webservices.data.PollingVideoInventory;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.SQLDaoUtil;

public class FanClubEventInterestSQLDAO {
	private static Connection rtfZonesConnection; // remove this at deployment..

	public static FanClubEventInterest getLikedFanClubEventInterestByCustomerIdAndEventId(Integer customerId,Integer fanClubEventId) {

		String sql = "select id,fanclub_events_mst_id,customer_id,interested_status "//,created_date,updated_date
				+ " from "+DatabaseConnections.quizApiLinkedServer+".fanclub_events_interested with(nolock) "
				+ " where fanclub_events_mst_id= " + fanClubEventId+" and interested_status='LIKED'";

		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			FanClubEventInterest fcEventInterest = new FanClubEventInterest();
			while (rs.next()) {
				fcEventInterest.setId(rs.getInt("id"));
				fcEventInterest.setFceId(rs.getInt("fanclub_events_mst_id"));
				fcEventInterest.setCuId(rs.getInt("customer_id"));
				fcEventInterest.setStatus(rs.getString("interested_status"));
				/*if(rs.getDate("created_Date") != null) {
					fcEventInterest.setCrDate(new Date(rs.getTimestamp("created_Date").getTime()));	
				}
				if(rs.getDate("updated_date") != null) {
					fcEventInterest.setUpDate(new Date(rs.getTimestamp("updated_date").getTime()));	
				}*/
				
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return fcEventInterest;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	public static FanClubEventInterest saveFanClubEventInterest(FanClubEventInterest fceInterest) {
		Calendar cal = Calendar.getInstance();
		String sql = " insert into "+ DatabaseConnections.quizApiLinkedServer +".fanclub_events_interested(fanclub_events_mst_id,customer_id,"
				+ " interested_status,created_date) "//,updated_date 
				+ " values ("+fceInterest.getFceId()+","+fceInterest.getCuId()+",'"+fceInterest.getStatus()+"',"
				+ " '"+new Timestamp(fceInterest.getCrDate().getTime())+"'"//,null 
				+ " ) ";

		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Polling Question info Insert Query : " + sql);
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating FanClubEventInterest failed, no rows affected.");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					fceInterest.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating FanClubEventInterest failed, no ID obtained.");
				}
			} catch (Exception ex) {
				//pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fceInterest;
	}
	public static int updateFanClubEventInterestExit(FanClubEventInterest fcEventInt) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_events_interested set interested_status='"+fcEventInt.getStatus()+"',"
				+ "updated_date='"+new Timestamp(fcEventInt.getUpDate().getTime())+"'"
				+" where id = "+fcEventInt.getId();
		int updateCnt = 0;
		try {
			// System.out.println("[PollingAnswerService][UPDATE ANSWER SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			// System.out.println("[PollingAnswerService][UPDATE ANSWER COUNT" + updateCnt);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
	public static List<Integer> getLikedFanClubEventIdsByCustomerIdAndFanclubId(Integer customerId,Integer fanClubId) {

		String sql = "select distinct fanclub_events_mst_id from " + DatabaseConnections.quizApiLinkedServer +".fanclub_events_interested fei with(nolock)" + 
				"	inner join " + DatabaseConnections.quizApiLinkedServer +".fanclub_Events_mst fe with(nolock) on fe.id=fei.fanclub_events_mst_id" + 
				"	where fei.customer_id="+customerId+" and fe.fanclub_mst_id="+fanClubId+" and fei.interested_status='LIKED'";

		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			List<Integer> eventIds = new ArrayList<Integer>();
			while (rs.next()) {
				eventIds.add(rs.getInt("fanclub_events_mst_id"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return eventIds;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	public static Integer getLikedFanClubEventIdsByEventIdAndCustomerId(Integer customerId,Integer eventId) {

		String sql = "select distinct fanclub_events_mst_id from " + DatabaseConnections.quizApiLinkedServer +".fanclub_events_interested fei with(nolock)" + 
				"	where fei.customer_id="+customerId+" and fei.fanclub_events_mst_id="+eventId+" and fei.interested_status='LIKED'";

		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			Integer fcEventId = null;
			if (rs.next()) {
				fcEventId = rs.getInt("fanclub_events_mst_id");
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return fcEventId;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
}
