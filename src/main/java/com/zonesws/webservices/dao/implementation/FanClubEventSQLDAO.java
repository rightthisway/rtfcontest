package com.zonesws.webservices.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.AbuseReportInfo;
import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.list.MyVideoInfo;
import com.quiz.cassandra.list.PollingAnswerInfo;
import com.quiz.cassandra.list.PollingContest;
import com.quiz.cassandra.list.PollingQuestionInfo;
import com.quiz.cassandra.list.PollingRewardsInfo;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.PollingVideoRewardsConfig;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.service.PollingAnswerService;
import com.quiz.cassandra.service.PollingQuestionService;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.data.PollingCategory;
import com.zonesws.webservices.data.PollingCustomerRewards;
import com.zonesws.webservices.data.PollingVideoInventory;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.SQLDaoUtil;

public class FanClubEventSQLDAO {
	private static Connection rtfZonesConnection; // remove this at deployment..

	public static FanClubEvent getActiveFanClubEventByEventId(Integer fanClubEventId) {

		String sql = "select id,fanclub_mst_id,customer_id,event_name,event_date,event_time,venue_address,event_status,no_of_interested," 
				+ "	poster_url,created_Date,updated_date,created_user_id,updated_user_id from "+DatabaseConnections.quizApiLinkedServer+".fanclub_events_mst with(nolock) "
				+ " where id= " + fanClubEventId+" and event_status='ACTIVE'";

		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			FanClubEvent fbevent = new FanClubEvent();
			while (rs.next()) {
				fbevent.setId(rs.getInt("id"));
				fbevent.setFcId(rs.getInt("fanclub_mst_id"));
				fbevent.setCuId(rs.getInt("customer_id"));
				fbevent.seteName(rs.getString("event_name"));
				if(rs.getDate("event_date") != null) {
					fbevent.seteDate(new Date(rs.getTimestamp("event_date").getTime()));	
				}
				if(rs.getDate("event_time") != null) {
					fbevent.seteTime(new Date(rs.getTimestamp("event_time").getTime()));	
				}
				fbevent.setVenue(rs.getString("venue_address"));
				fbevent.setStatus(rs.getString("event_status"));
				fbevent.setNoIntr(rs.getInt("no_of_interested"));
				fbevent.setpUrl(rs.getString("poster_url"));
				if(rs.getDate("created_Date") != null) {
					fbevent.setCrDate(new Date(rs.getTimestamp("created_Date").getTime()));	
				}
				if(rs.getDate("updated_date") != null) {
					fbevent.setUpDate(new Date(rs.getTimestamp("updated_date").getTime()));	
				}
				fbevent.setCrUserId(rs.getString("created_user_id"));
				fbevent.setUpUserId(rs.getString("updated_user_id"));
				
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return fbevent;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	public static FanClubEvent saveFanClubEvent(FanClubEvent fcEvent) {
		Calendar cal = Calendar.getInstance();
		String sql = " insert into "+ DatabaseConnections.quizApiLinkedServer +".fanclub_events_mst (fanclub_mst_id,customer_id,event_name,event_date,event_time,"
				+ " venue_address,event_status,poster_url,no_of_interested,created_Date,updated_date,created_user_id,updated_user_id) " 
				+ " values ("+fcEvent.getFcId()+","+fcEvent.getCuId()+",'"+fcEvent.geteName().replaceAll("'", "''")+"',";
			if(fcEvent.geteDate() != null) {
				sql = sql + "'"+(new Timestamp(fcEvent.geteDate().getTime()))+"',";
			} else {
				sql = sql + "null,";
			}
			if(fcEvent.geteTime() != null) {
				sql = sql + "'"+(new Timestamp(fcEvent.geteTime().getTime()))+"',";
			} else {
				sql = sql + "null,";
			}
			sql = sql + "'"+fcEvent.getVenue().replaceAll("'", "''")+"','"+fcEvent.getStatus()+"','"+fcEvent.getpUrl()+"',"+fcEvent.getNoIntr()+",'"+new Timestamp(fcEvent.getCrDate().getTime())+"',null,null,null" 
				+ " ) ";

		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Polling Question info Insert Query : " + sql);
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating FanClubEvent failed, no rows affected.");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					fcEvent.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating FanClubEvent failed, no ID obtained.");
				}
			} catch (Exception ex) {
				//pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fcEvent;
	}
	public static int updateFanClubEvent(FanClubEvent fcEvent) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_events_mst set event_name='"+fcEvent.geteName().replaceAll("'", "''")+"',";
		
			if(fcEvent.geteDate() != null) {
				sql = sql + "event_date='"+(new Timestamp(fcEvent.geteDate().getTime()))+"',";
			} else {
				sql = sql + "event_date=null,";
			}
			if(fcEvent.geteTime() != null) {
				sql = sql + "event_time='"+(new Timestamp(fcEvent.geteTime().getTime()))+"',";
			} else {
				sql = sql + "event_time=null,";
			}
				
			sql = sql + "venue_address='"+fcEvent.getVenue().replaceAll("'", "''")+"',event_status='"+fcEvent.getStatus()+"',"
				+ "updated_date='"+new Timestamp(fcEvent.getUpDate().getTime())+"',updated_user_id="+fcEvent.getUpUserId()+""
				+" where id = "+fcEvent.getId();
		int updateCnt = 0;
		try {
			// System.out.println("[PollingAnswerService][UPDATE ANSWER SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();

			statement.close();
			DatabaseConnections.closeConnection(conn);
			// System.out.println("[PollingAnswerService][UPDATE ANSWER COUNT" + updateCnt);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}

	public static int updateFanClubEventPosterUrl(Integer fcEventId,String posterUrl) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_events_mst set poster_url='"+posterUrl+"',"
				+ "updated_date='"+new Timestamp(new Date().getTime())+"',updated_user_id=null"
				+" where id = "+fcEventId;
		int updateCnt = 0;
		try {
			// System.out.println("[PollingAnswerService][UPDATE ANSWER SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();

			statement.close();
			DatabaseConnections.closeConnection(conn);
			// System.out.println("[PollingAnswerService][UPDATE ANSWER COUNT" + updateCnt);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
	public static int deleteFanClubEvent(Integer fcEventId) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_events_mst set event_status='DELETED',"
				+ "updated_date='"+new Timestamp(new Date().getTime())+"',updated_user_id=null where id = "+fcEventId;
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
	public static Integer saveFanClubEventsTransaction(Integer fcEventId) {
		String sql = " insert into "+ DatabaseConnections.quizApiLinkedServer +".fanclub_events_trn(fanclub_events_mst_id,fanclub_mst_id,customer_id,event_name,event_date,event_time,venue_address,event_status,poster_url,no_of_interested,"
				+" created_Date,updated_date,created_user_id,updated_user_id)"  
				+" select id,fanclub_mst_id,customer_id,event_name,event_date,event_time,venue_address,event_status,poster_url,no_of_interested,"  
				+" created_Date,updated_date,created_user_id,updated_user_id from "+ DatabaseConnections.quizApiLinkedServer +".fanclub_events_mst with(nolock) where id="+fcEventId+"";

		try {
			System.out.println(sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				return null;
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					return generatedKeys.getInt(1);
				} else {
					return null;
				}
			} catch (Exception ex) {
				//pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static List<FanClubEvent> getAllActiveFanClubEventByFanClubId(Integer fanClubId,Integer pageNo,Integer maxRow) {

		/*String sql = "select id,fanclub_mst_id,customer_id,event_name,event_date,event_time,venue_address,event_status,no_of_interested," 
				+ "	poster_url,created_Date,updated_date,created_user_id,updated_user_id from "+DatabaseConnections.quizApiLinkedServer+".fanclub_events_mst with(nolock) "
				+ " where fanclub_mst_id= " + fanClubId+" and event_status='ACTIVE'"
				+ " order by event_date,event_time";*/
		 /*"	select id,fanclub_mst_id,customer_id,event_name,event_date,event_time,venue_address,poster_url,no_of_interested" + 
				"  from "+DatabaseConnections.quizApiLinkedServer+".fanclub_events_mst e WITH(nolock) " + 
				"  where event_status='ACTIVE' and fanclub_mst_id="+fanClubId + 
				" (event_date is null or event_date >= Convert(date, getdate()) )" +
				" order by e.event_date,event_time" +*/

		String sql =" select id,fanclub_mst_id,customer_id,event_name,event_date,event_time,venue_address,poster_url,no_of_interested from ("
				+ " select id,fanclub_mst_id,customer_id,event_name,event_date,event_time,venue_address,poster_url,no_of_interested, 0 as srt"
				+ " from "+DatabaseConnections.quizApiLinkedServer+".fanclub_events_mst e WITH(nolock)"
				+ " where event_status='ACTIVE' and fanclub_mst_id="+fanClubId
				+ " and event_date >= Convert(date, getdate())"
				+ " union"
				+ " select id,fanclub_mst_id,customer_id,event_name,event_date,event_time,venue_address,poster_url,no_of_interested, 1 as srt"
				+ " from "+DatabaseConnections.quizApiLinkedServer+".fanclub_events_mst e WITH(nolock)"
				+ " where event_status='ACTIVE' and fanclub_mst_id="+fanClubId
				+ " and event_date is null"
				+ " ) as g"
				+ " order by srt,event_date,event_time,id" +
				" OFFSET ("+pageNo+"-1)*"+maxRow+" ROWS FETCH NEXT "+maxRow+"  ROWS ONLY";

		Statement stmt = null;
		ResultSet rs = null;
		List<FanClubEvent> fbEventList = new ArrayList<FanClubEvent>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			FanClubEvent fbevent = new FanClubEvent();
			while (rs.next()) {
				fbevent = new FanClubEvent();
				fbevent.setId(rs.getInt("id"));
				fbevent.setFcId(rs.getInt("fanclub_mst_id"));
				fbevent.setCuId(rs.getInt("customer_id"));
				fbevent.seteName(rs.getString("event_name"));
				if(rs.getDate("event_date") != null) {
					fbevent.seteDate(new Date(rs.getTimestamp("event_date").getTime()));	
				}
				if(rs.getDate("event_time") != null) {
					fbevent.seteTime(new Date(rs.getTimestamp("event_time").getTime()));	
				}
				fbevent.setVenue(rs.getString("venue_address"));
				//fbevent.setStatus(rs.getString("event_status"));
				fbevent.setNoIntr(rs.getInt("no_of_interested"));
				fbevent.setpUrl(rs.getString("poster_url"));
				/*if(rs.getDate("created_Date") != null) {
					fbevent.setCrDate(new Date(rs.getTimestamp("created_Date").getTime()));	
				}
				if(rs.getDate("updated_date") != null) {
					fbevent.setUpDate(new Date(rs.getTimestamp("updated_date").getTime()));	
				}
				fbevent.setCrUserId(rs.getInt("created_user_id"));
				fbevent.setUpUserId(rs.getInt("updated_user_id"));*/
				fbEventList.add(fbevent);
				
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return fbEventList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return fbEventList;
	}
	public static int updateFanClubEventInterestCountForJoin(Integer fanClubId) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_events_mst set no_of_interested=no_of_interested+1 where id = "+fanClubId;
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
	public static int updateFanClubEventInterestCountForExit(Integer fanClubId) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_events_mst set no_of_interested=no_of_interested-1 where id = "+fanClubId;
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}
}
