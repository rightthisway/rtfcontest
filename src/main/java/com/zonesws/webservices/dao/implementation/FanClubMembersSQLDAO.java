package com.zonesws.webservices.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.AbuseReportInfo;
import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.list.MyVideoInfo;
import com.quiz.cassandra.list.PollingAnswerInfo;
import com.quiz.cassandra.list.PollingContest;
import com.quiz.cassandra.list.PollingQuestionInfo;
import com.quiz.cassandra.list.PollingRewardsInfo;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.PollingVideoRewardsConfig;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.service.PollingAnswerService;
import com.quiz.cassandra.service.PollingQuestionService;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.data.FanClubEventInterest;
import com.zonesws.webservices.data.FanClubMembers;
import com.zonesws.webservices.data.PollingCategory;
import com.zonesws.webservices.data.PollingCustomerRewards;
import com.zonesws.webservices.data.PollingVideoInventory;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.SQLDaoUtil;

public class FanClubMembersSQLDAO {
	private static Connection rtfZonesConnection; // remove this at deployment..

	public static FanClubMembers getJoinedFanClubMembersByCustomerIdAndFanClubId(Integer customerId,Integer fanClubId) {

		String sql = "select id,fanclub_mst_id,customer_id,member_status "//,joined_date,exit_date,created_date
				+ " from "+DatabaseConnections.quizApiLinkedServer+".fanclub_members with(nolock) "
				+ " where customer_id="+customerId+" and fanclub_mst_id= " + fanClubId+" and member_status='JOIN'";

		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			FanClubMembers fcbMembers = new FanClubMembers();
			while (rs.next()) {
				fcbMembers.setId(rs.getInt("id"));
				fcbMembers.setFcId(rs.getInt("fanclub_mst_id"));
				fcbMembers.setCuId(rs.getInt("customer_id"));
				fcbMembers.setStatus(rs.getString("member_status"));
				/*if(rs.getDate("created_Date") != null) {
					fcbMembers.setCrDate(new Date(rs.getTimestamp("created_Date").getTime()));	
				}
				if(rs.getDate("joined_date") != null) {
					fcbMembers.setJnDate(new Date(rs.getTimestamp("joined_date").getTime()));	
				}
				if(rs.getDate("exit_date") != null) {
					fcbMembers.setExDate(new Date(rs.getTimestamp("exit_date").getTime()));	
				}*/
				
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return fcbMembers;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	public static FanClubMembers saveFanClubMembers(FanClubMembers fcMember) {

		String sql = " insert into "+ DatabaseConnections.quizApiLinkedServer +".fanclub_members(fanclub_mst_id,customer_id,member_status,"
				+ "created_date,joined_date) "//,exit_date 
				+ " values ("+fcMember.getFcId()+","+fcMember.getCuId()+",'"+fcMember.getStatus()+"',"
				+ " '"+new Timestamp(fcMember.getJnDate().getTime())+"','"+new Timestamp(fcMember.getJnDate().getTime())+"'"//,null 
				+ " ) ";

		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Polling Question info Insert Query : " + sql);
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating FanClubMembers failed, no rows affected.");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					fcMember.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating FanClubMembers failed, no ID obtained.");
				}
			} catch (Exception ex) {
				//pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fcMember;
	}
	public static int updateFanClubMemberExit(FanClubMembers fcMember) {
		String sql = " update " + DatabaseConnections.quizApiLinkedServer + ".fanclub_members set member_status='"+fcMember.getStatus()+"',"
				+ "exit_date ='"+new Timestamp(fcMember.getExDate().getTime())+"'"
				+" where id = "+fcMember.getId();
		int updateCnt = 0;
		try {
			// System.out.println("[PollingAnswerService][UPDATE ANSWER SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			
			statement.close();
			DatabaseConnections.closeConnection(conn);
			// System.out.println("[PollingAnswerService][UPDATE ANSWER COUNT" + updateCnt);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}

}
