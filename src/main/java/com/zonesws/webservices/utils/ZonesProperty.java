package com.zonesws.webservices.utils;

public class ZonesProperty  {
	private String emailList;
	private String orderEmailList;
	private String helpEmailList;
	private String infoEmailList;
	private String salesEmailList;
	//private Map<String,UserType> userTypeMap;
	private String iv;
	private String key;
	private String client;
	private String source;
	private String host;
	private String port;
	private String postBackUrl;
	private String postUrl;
	private String duplicateTicketPostUrl;
	private String emailFrom;
	private String emailFromForPromo;
	private String emailBcc;
	private String rtfDefaultDPFolder;
	private String rtfDefaultDPPrefix;
	private String rtfQuizVideo;
	
	private String signature;
	private String apiUserName;
	private String apiPassword;
	private String environment;
	private String serverWebAppBasePath;
	private String paypalEnvironment;
	private String rtfDesktopSiteUrl;
	private String rtfCustomerDPPrefix;
	private String rtfCustomerDPFolder;
	private String apiServerSvgMapsBaseURL;
	/*private String rewardTheFanServerSvgMapsBaseUrl;	*/
	private String apiServerBaseUrl;
	private Boolean isProductionEnvironment;
	private String masterNodeServerIP;
	private String rtfImageSharedPath;	
	private String rtfSharedDriveUserName;
	private String rtfSharedDrivePassword;
	private Boolean isSharedDriveOn;
	private String svgTextFilePath;
	private String imageServerURL;
	
	public String getImageServerURL() {
		return imageServerURL;
	}

	public void setImageServerURL(String imageServerURL) {
		this.imageServerURL = imageServerURL;
	}

	public String getEmailList() {
		return emailList;
	}

	public void setEmailList(String emailList) {
		this.emailList = emailList;
	}
	
	public String getOrderEmailList() {
		return orderEmailList;
	}

	public void setOrderEmailList(String orderEmailList) {
		this.orderEmailList = orderEmailList;
	}

	public String getHelpEmailList() {
		return helpEmailList;
	}

	public void setHelpEmailList(String helpEmailList) {
		this.helpEmailList = helpEmailList;
	}

	public String getInfoEmailList() {
		return infoEmailList;
	}

	public void setInfoEmailList(String infoEmailList) {
		this.infoEmailList = infoEmailList;
	}

	public String getSalesEmailList() {
		return salesEmailList;
	}

	public void setSalesEmailList(String salesEmailList) {
		this.salesEmailList = salesEmailList;
	}

	/*public Map<String,UserType> getUserTypeMap() {
		return userTypeMap;
	}

	public void setUserTypeMap(Map<String,UserType> userTypeMap) {
		this.userTypeMap = userTypeMap;
	}*/

	public void afterPropertiesSet() throws Exception {
		
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getPostBackUrl() {
		return postBackUrl;
	}

	public void setPostBackUrl(String postBackUrl) {
		this.postBackUrl = postBackUrl;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public String getDuplicateTicketPostUrl() {
		return duplicateTicketPostUrl;
	}

	public void setDuplicateTicketPostUrl(String duplicateTicketPostUrl) {
		this.duplicateTicketPostUrl = duplicateTicketPostUrl;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getApiUserName() {
		return apiUserName;
	}

	public void setApiUserName(String apiUserName) {
		this.apiUserName = apiUserName;
	}

	public String getApiPassword() {
		return apiPassword;
	}

	public void setApiPassword(String apiPassword) {
		this.apiPassword = apiPassword;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getPaypalEnvironment() {
		return paypalEnvironment;
	}

	public void setPaypalEnvironment(String paypalEnvironment) {
		this.paypalEnvironment = paypalEnvironment;
	}

	public String getServerWebAppBasePath() {
		return serverWebAppBasePath;
	}

	public void setServerWebAppBasePath(String serverWebAppBasePath) {
		this.serverWebAppBasePath = serverWebAppBasePath;
	}

	public String getRtfDesktopSiteUrl() {
		return rtfDesktopSiteUrl;
	}

	public void setRtfDesktopSiteUrl(String rtfDesktopSiteUrl) {
		this.rtfDesktopSiteUrl = rtfDesktopSiteUrl;
	}

	public String getRtfCustomerDPPrefix() {
		return rtfCustomerDPPrefix;
	}

	public void setRtfCustomerDPPrefix(String rtfCustomerDPPrefix) {
		this.rtfCustomerDPPrefix = rtfCustomerDPPrefix;
	}

	public String getRtfCustomerDPFolder() {
		return rtfCustomerDPFolder;
	}

	public void setRtfCustomerDPFolder(String rtfCustomerDPFolder) {
		this.rtfCustomerDPFolder = rtfCustomerDPFolder;
	}

	/*public String getRewardTheFanServerSvgMapsBaseUrl() {
		return rewardTheFanServerSvgMapsBaseUrl;
	}

	public void setRewardTheFanServerSvgMapsBaseUrl(String rewardTheFanServerSvgMapsBaseUrl) {
		this.rewardTheFanServerSvgMapsBaseUrl = rewardTheFanServerSvgMapsBaseUrl;
	}*/

	public String getApiServerBaseUrl() {
		return apiServerBaseUrl;
	}

	public void setApiServerBaseUrl(String apiServerBaseUrl) {
		this.apiServerBaseUrl = apiServerBaseUrl;
	}

	public String getApiServerSvgMapsBaseURL() {
		return apiServerSvgMapsBaseURL;
	}

	public void setApiServerSvgMapsBaseURL(String apiServerSvgMapsBaseURL) {
		this.apiServerSvgMapsBaseURL = apiServerSvgMapsBaseURL;
	}

	public String getRtfDefaultDPFolder() {
		return rtfDefaultDPFolder;
	}

	public void setRtfDefaultDPFolder(String rtfDefaultDPFolder) {
		this.rtfDefaultDPFolder = rtfDefaultDPFolder;
	}

	public String getRtfDefaultDPPrefix() {
		return rtfDefaultDPPrefix;
	}

	public void setRtfDefaultDPPrefix(String rtfDefaultDPPrefix) {
		this.rtfDefaultDPPrefix = rtfDefaultDPPrefix;
	}

	public String getEmailFrom() {
		if(emailFrom == null || emailFrom.trim().isEmpty()){
			emailFrom = "sales@rewardthefan.com";
		}
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailBcc() {
		return emailBcc;
	}

	public void setEmailBcc(String emailBcc) {
		this.emailBcc = emailBcc;
	}

	public String getRtfQuizVideo() {
		return rtfQuizVideo;
	}

	public void setRtfQuizVideo(String rtfQuizVideo) {
		this.rtfQuizVideo = rtfQuizVideo;
	}

	public String getEmailFromForPromo() {
		return emailFromForPromo;
	}

	public void setEmailFromForPromo(String emailFromForPromo) {
		this.emailFromForPromo = emailFromForPromo;
	}

	public Boolean getIsProductionEnvironment() {
		return isProductionEnvironment;
	}

	public void setIsProductionEnvironment(Boolean isProductionEnvironment) {
		this.isProductionEnvironment = isProductionEnvironment;
	}

	public String getMasterNodeServerIP() {
		return masterNodeServerIP;
	}

	public void setMasterNodeServerIP(String masterNodeServerIP) {
		this.masterNodeServerIP = masterNodeServerIP;
	}

	public String getRtfImageSharedPath() {
		return rtfImageSharedPath;
	}

	public void setRtfImageSharedPath(String rtfImageSharedPath) {
		this.rtfImageSharedPath = rtfImageSharedPath;
	}

	public String getRtfSharedDriveUserName() {
		return rtfSharedDriveUserName;
	}

	public void setRtfSharedDriveUserName(String rtfSharedDriveUserName) {
		this.rtfSharedDriveUserName = rtfSharedDriveUserName;
	}

	public String getRtfSharedDrivePassword() {
		return rtfSharedDrivePassword;
	}

	public void setRtfSharedDrivePassword(String rtfSharedDrivePassword) {
		this.rtfSharedDrivePassword = rtfSharedDrivePassword;
	}

	public Boolean getIsSharedDriveOn() {
		return isSharedDriveOn;
	}

	public void setIsSharedDriveOn(Boolean isSharedDriveOn) {
		this.isSharedDriveOn = isSharedDriveOn;
	}

	public String getSvgTextFilePath() {
		return svgTextFilePath;
	}

	public void setSvgTextFilePath(String svgTextFilePath) {
		this.svgTextFilePath = svgTextFilePath;
	}
	
}
