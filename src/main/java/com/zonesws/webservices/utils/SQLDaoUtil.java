package com.zonesws.webservices.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quiz.cassandra.list.CustDailyRewardsVO;
import com.quiz.cassandra.list.CustomerDailyRewardLimits;
import com.quiz.cassandra.list.HallOfFameDtls;
import com.quiz.cassandra.list.RtfPointsDetails;
import com.quiz.cassandra.list.RtfRewardConfigInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.data.RtfConfigContestClusterNodes;
import com.zonesws.webservices.enums.ContestJackpotType;
import com.zonesws.webservices.enums.JackpotCreditType;
import com.zonesws.webservices.enums.SourceType;

public class SQLDaoUtil {

	public static QuizContest getQuizContestForId(String contestId) {

		String sql = "select * from Contest where id  = " + contestId;
		QuizContest qz = null;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			qz = new QuizContest();
			while (rs.next()) {

				qz.setId(rs.getInt("id"));
				qz.setContestName(rs.getString("contest_name"));
				qz.setContestStartDateTime(rs.getDate("contest_start_datetime"));
				qz.setParticipantsCount(rs.getInt("participants_count"));
				qz.setWinnersCount(rs.getInt("winners_count"));
				qz.setTicketWinnersCount(rs.getInt("ticket_winners_count"));
				qz.setPointWinnersCount(rs.getInt("point_winners_count"));
				qz.setStatus(rs.getString("status"));
				qz.setCreatedDateTime(rs.getDate("created_datetime"));
				qz.setUpdatedDateTime(rs.getDate("updated_datetime"));
				qz.setCreatedBy(rs.getString("created_by"));
				rs.getString("updated_by");
				qz.setMaxFreeTicketWinners(rs.getInt("eligible_free_ticket_winners"));
				qz.setFreeTicketsPerWinner(rs.getInt("free_tickets_per_winner"));
				qz.setPointsPerWinner(rs.getDouble("points_per_winner"));
				qz.setNoOfQuestions(rs.getInt("no_of_questions"));
				qz.setContestType(rs.getString("contest_type"));
				qz.setTotalRewards(rs.getDouble("total_rewards"));
				qz.setContestMode(rs.getString("contest_mode"));
				qz.setContestPwd(rs.getString("contest_password"));
				// qz.setQ rs.getInt("question_size");
				qz.setRewardsPerQuestion(rs.getDouble("rewards_per_question"));
				// qz.setIsCustomerStatsUpdated(rs.getString("is_customer_stats_updated"));
				qz.setZone(rs.getString("zone"));
				qz.setPromoCode(rs.getString("promotional_code"));
				qz.setDiscountPercentage(rs.getDouble("discount_percentage"));
				qz.setPromoRefId(rs.getInt("promo_ref_id"));
				qz.setPromoRefName(rs.getString("promo_ref_name"));
				qz.setPromoExpiryDate(rs.getDate("promo_expiry_date"));
				qz.setPromoRefType(rs.getString("promo_ref_type"));
				// qz.setPromo rs.getInt("promo_offer_id");
				qz.setLastQuestionNo(rs.getInt("last_question_no"));
				qz.setLastAction(rs.getString("last_action"));
				qz.setSingleTicketPrice(rs.getDouble("single_tix_price"));
				// qz.setProcessStatus(rs.getString("process_status"));
				qz.setExtendedName(rs.getString("extended_name"));
				qz.setContestJackpotType(ContestJackpotType.valueOf(rs.getString("contest_jackpot_type")));
			}

			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}

		System.out.println(qz.getId() + " --- " + qz.getContestName() + " ---- " + qz.getRewardsPerQuestion());

		return qz;

	}

	public static List<QuizContestQuestions> getContestQuestions(String contestId) {

		String sql = "select * from contest_questions where contest_id  = " + contestId + " order by question_sl_no";
		QuizContestQuestions qzqs = null;
		List<QuizContestQuestions> qzqsList = new ArrayList<QuizContestQuestions>();
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			qzqs = new QuizContestQuestions();
			while (rs.next()) {
				qzqs = new QuizContestQuestions();
				qzqs.setId(rs.getInt("id"));
				qzqs.setContestId(rs.getInt("contest_id"));
				qzqs.setQuestionSNo(rs.getInt("question_sl_no"));

				qzqs.setQuestion(rs.getString("question"));
				qzqs.setOptionA(rs.getString("option_a"));
				qzqs.setOptionB(rs.getString("option_b"));
				qzqs.setOptionC(rs.getString("option_c"));
				// rs.getString("option_d");
				qzqs.setAnswer(rs.getString("answer"));

				qzqs.setCreatedDateTime(rs.getDate("created_datetime"));
				qzqs.setUpdatedDateTime(rs.getDate("updated_datetime"));
				rs.getString("created_by");
				rs.getString("updated_by");
				rs.getString("temp_option");
				qzqs.setQuestionRewards(rs.getDouble("question_reward"));
				qzqs.setNoOfJackpotWinners(rs.getInt("no_of_winner"));
				JackpotCreditType type = JackpotCreditType.values()[rs.getInt("mini_jackpot_type")];
				qzqs.setjCreditType(type);
				qzqs.setJackpotGiftCardId(rs.getInt("jackpot_gift_card_id"));
				qzqs.setJackpotQtyPerWinner(rs.getDouble("qty_per_winner"));
				// rs.getString("difficulty_level");
				qzqsList.add(qzqs);

				System.out.println(qzqs.getId() + " --- " + qzqs.getOptionA() + " ---- " + qzqs.getAnswer());
			}

			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {

			se.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}

		return qzqsList;

	}

	public static QuizContest getCurrentStartedContest(String contestType) {

		String sql = "select * from Contest where status ='STARTED' and contest_type ='" + contestType + "'"
				+ "  order by contest_start_datetime ";
		QuizContest qz = null;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			// stmt.setMaxRows(1);
			ResultSet rs = stmt.executeQuery(sql);
			qz = new QuizContest();
			while (rs.next()) {

				qz.setId(rs.getInt("id"));
				qz.setContestName(rs.getString("contest_name"));
				qz.setContestStartDateTime(rs.getDate("contest_start_datetime"));
				qz.setParticipantsCount(rs.getInt("participants_count"));
				qz.setWinnersCount(rs.getInt("winners_count"));
				qz.setTicketWinnersCount(rs.getInt("ticket_winners_count"));
				qz.setPointWinnersCount(rs.getInt("point_winners_count"));
				qz.setStatus(rs.getString("status"));
				qz.setCreatedDateTime(rs.getDate("created_datetime"));
				qz.setUpdatedDateTime(rs.getDate("updated_datetime"));
				qz.setCreatedBy(rs.getString("created_by"));
				rs.getString("updated_by");
				qz.setMaxFreeTicketWinners(rs.getInt("eligible_free_ticket_winners"));
				qz.setFreeTicketsPerWinner(rs.getInt("free_tickets_per_winner"));
				qz.setPointsPerWinner(rs.getDouble("points_per_winner"));
				qz.setNoOfQuestions(rs.getInt("no_of_questions"));
				qz.setContestType(rs.getString("contest_type"));
				qz.setTotalRewards(rs.getDouble("total_rewards"));
				qz.setContestMode(rs.getString("contest_mode"));
				qz.setContestPwd(rs.getString("contest_password"));
				// qz.setQ rs.getInt("question_size");
				qz.setRewardsPerQuestion(rs.getDouble("rewards_per_question"));
				// qz.setIsCustomerStatsUpdated(rs.getString("is_customer_stats_updated"));
				qz.setZone(rs.getString("zone"));
				;
				qz.setPromoCode(rs.getString("promotional_code"));
				qz.setDiscountPercentage(rs.getDouble("discount_percentage"));
				qz.setPromoRefId(rs.getInt("promo_ref_id"));
				qz.setPromoRefName(rs.getString("promo_ref_name"));
				qz.setPromoExpiryDate(rs.getDate("promo_expiry_date"));
				qz.setPromoRefType(rs.getString("promo_ref_type"));
				// qz.setPromo rs.getInt("promo_offer_id");
				qz.setLastQuestionNo(rs.getInt("last_question_no"));
				qz.setLastAction(rs.getString("last_action"));
				qz.setSingleTicketPrice(rs.getDouble("single_tix_price"));
				// qz.setProcessStatus(rs.getString("process_status"));
				qz.setExtendedName(rs.getString("extended_name"));
				qz.setContestJackpotType(ContestJackpotType.valueOf(rs.getString("contest_jackpot_type")));
			}

			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}

		System.out.println(qz.getId() + " --- " + qz.getContestName() + " ---- " + qz.getRewardsPerQuestion());

		return qz;

	}

	public static List<Integer> getAllBotsCustomerIds() {

		String sql = "select distinct c.id as customerId  from customer c with(nolock) " + "inner join "
				+ DatabaseConnections.quizApiLinkedServer
				+ ".customer_referral_tracking rt with(nolock)  on rt.customer_id = c.id  and c.is_test_account=0 "
				+ "where email not like '%rtw.com%' and email not like '%rightthisway.com%' and email not like '%amit%raut%' and "
				+ "signup_date between CONVERT(DATETIME,'2018-08-11 00:01:00') and  CONVERT(DATETIME,getdate()) and c.product_type='REWARDTHEFAN'  "
				+ "and device_id in (select device_id from customer c with(nolock) inner join "
				+ DatabaseConnections.quizApiLinkedServer + ".customer_referral_tracking rt with(nolock) "
				+ "on rt.customer_id = c.id and c.is_test_account=0  where product_type='REWARDTHEFAN' and device_id <> '' and device_id is not null "
				+ "group by device_id having count(1) > 3 )  ";

		try {
			System.out.println("sql : " + sql);
			// Connection conn = DatabaseConnections.getRtfConnection();
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			// stmt.setMaxRows(1);
			ResultSet rs = stmt.executeQuery(sql);
			List<Integer> result = new ArrayList<Integer>();
			while (rs.next()) {
				result.add(rs.getInt("customerId"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);

			System.out.println("bot list --- " + result);
			return result;

		} catch (SQLException se) {

			se.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}
		return null;
	}

	public static List<HallOfFameDtls> getHallOfFameByDateFromView(Integer hallOfFameCount) {

		// Get data from view
		String sql = " select top " + hallOfFameCount + " c.id as cuId,c.user_id as uId,c.cust_image_path as imgP,"
				+ " TotalTickets as rTix,TotalPoints as rPoints,CusRank as rRank"
				+ " from contest_winners_rank_for_week_view csw with(nolock)" + " inner join "
				+ DatabaseConnections.zonesApiLinkedServer + ".customer c with(nolock) on c.id=csw.customer_id"
				+ " where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by CusRank";

		List<HallOfFameDtls> hallOfFameList = new ArrayList<HallOfFameDtls>();
		try {

			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				HallOfFameDtls hallOfFameDtls = new HallOfFameDtls();
				hallOfFameDtls.setCuId(rs.getInt("cuId"));
				hallOfFameDtls.setuId(rs.getString("uId"));
				hallOfFameDtls.setImgP(rs.getString("imgP"));
				hallOfFameDtls.setrTix(rs.getInt("rTix"));
				hallOfFameDtls.setrPoints(rs.getDouble("rPoints"));
				hallOfFameDtls.setrRank(rs.getInt("rRank"));
				hallOfFameList.add(hallOfFameDtls);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return hallOfFameList;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return hallOfFameList;
	}

	public static List<HallOfFameDtls> getHallOfFameByTillDateFromView(Integer summaryListCount) {

		String sql = " select top " + summaryListCount + " c.id as cuId,c.user_id as uId,c.cust_image_path as imgP,"
				+ " TotalTickets as rTix,TotalPoints as rPoints,CusRank as rRank"
				+ " from contest_winners_rank_till_date_view csw with(nolock)" + " inner join "
				+ DatabaseConnections.zonesApiLinkedServer + ".customer c with(nolock) on c.id=csw.customer_id"
				+ " where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by CusRank";
		List<HallOfFameDtls> hallOfFameList = new ArrayList<HallOfFameDtls>();
		try {

			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				HallOfFameDtls hallOfFameDtls = new HallOfFameDtls();
				hallOfFameDtls.setCuId(rs.getInt("cuId"));
				hallOfFameDtls.setuId(rs.getString("uId"));
				hallOfFameDtls.setImgP(rs.getString("imgP"));
				hallOfFameDtls.setrTix(rs.getInt("rTix"));
				hallOfFameDtls.setrPoints(rs.getDouble("rPoints"));
				hallOfFameDtls.setrRank(rs.getInt("rRank"));
				hallOfFameList.add(hallOfFameDtls);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return hallOfFameList;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return hallOfFameList;
	}

	public static List<Integer> getBotsAndOldGrandWinners() {
		String sql = "select distinct custId from (select distinct CustomerId as custId from "
				+ DatabaseConnections.quizApiLinkedServer + ".bots_customer with(nolock)" + " union all "
				+ "select distinct customer_id as custId from (select customer_id, count(id) as winCount " + "from "
				+ DatabaseConnections.quizApiLinkedServer
				+ ".contest_grand_winners with(nolock) group by customer_id) w where w.winCount >=2 ) a ";
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			System.out.println("Bots Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			List<Integer> result = new ArrayList<Integer>();
			while (rs.next()) {
				result.add(rs.getInt("custId"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return result;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Integer> getOnlyBotsForJackpotWinners() {
		String sql = "select distinct CustomerId as custId from " + DatabaseConnections.quizApiLinkedServer
				+ ".bots_customer with(nolock) ";
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			System.out.println("Bots Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			List<Integer> result = new ArrayList<Integer>();
			while (rs.next()) {
				result.add(rs.getInt("custId"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return result;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean saveCustomerRewardHistory(Integer customerId, String trxType, SourceType sourceType,
			Integer sourceRefId, Integer lives, Integer magicWands, Integer sfStars, Integer rtfPoints,
			Double rewardDollar, String description) throws Exception {

		Connection connection = DatabaseConnections.getZonesApiConnection();
		connection.setAutoCommit(false);
		String query = "INSERT INTO " + DatabaseConnections.quizApiLinkedServer
				+ ".rtf_cust_reward_history(customer_id,transaction_type,source_type,ref_id,lives,magic_wands,"
				+ "sf_stars,rtf_points,reward_dollars,created_date,description) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,getdate(),?)";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		try {
			preparedStatement.setInt(1, customerId);
			preparedStatement.setString(2, trxType);
			preparedStatement.setString(3, sourceType.toString());
			preparedStatement.setInt(4, null != sourceRefId ? sourceRefId : -1);
			preparedStatement.setInt(5, null != lives ? lives : 0);
			preparedStatement.setInt(6, null != magicWands ? magicWands : 0);
			preparedStatement.setInt(7, null != sfStars ? sfStars : 0);
			preparedStatement.setInt(8, null != rtfPoints ? rtfPoints : 0);
			preparedStatement.setDouble(9, null != rewardDollar ? rewardDollar : 0.0);
			preparedStatement.setString(10, null != description ? description : "");
			preparedStatement.executeUpdate();
			connection.commit();
			DatabaseConnections.closeConnection(connection);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			// return false;
			throw e;
		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		saveCustomerRewardHistory(123456, "CREDIT", SourceType.REFERRAL, 25, null, null, null, 250, 0.02, "");
	}

	public static List<RtfConfigContestClusterNodes> getAllRtfConfigClusterNodeDetails() {

		String sql = " select * from  " + DatabaseConnections.zonesApiLinkedServer + ".rtf_conf_contest_cluster_nodes";
		List<RtfConfigContestClusterNodes> rtfClusterNodeList = new ArrayList<RtfConfigContestClusterNodes>();
		try {

			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				RtfConfigContestClusterNodes clusterNode = new RtfConfigContestClusterNodes();
				clusterNode.setId(rs.getInt("id"));
				clusterNode.setUrl(rs.getString("url"));
				clusterNode.setStatus(rs.getInt("status"));
				clusterNode.setDirectoryPath(rs.getString("directory_path"));
				rtfClusterNodeList.add(clusterNode);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return rtfClusterNodeList;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return rtfClusterNodeList;
	}

	public static boolean customerCanUploadVideo(Integer customerId) {
		String winnerOrderSql = " select distinct customer_id as CustId from dbo.contest_grand_winners with(nolock) where  "
				+ "customer_id = " + customerId + " and created_date > DATEADD(day, -1, GETDATE()) ";
		String videoUploadSql = " select cust_id as CustId, 'true' as isThere from " + ""
				+ DatabaseConnections.zonesApiLinkedServer
				+ ".polling_customer_video_uploads with(nolock) where cust_id = " + customerId + "  "
				+ "and upload_date > DATEADD(day, -1, GETDATE())";
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(winnerOrderSql);
			boolean canUploadVideo = false;
			while (rs.next()) {
				canUploadVideo = true;
			}
			if (canUploadVideo) {
				stmt = conn.createStatement();
				rs = stmt.executeQuery(videoUploadSql);
				while (rs.next()) {
					canUploadVideo = false;
				}
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return canUploadVideo;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static HallOfFameDtls getThisWeekHallofFameByCustomerId(Integer customerId) {

		// Get data from view
		String sql = " select csw.customer_id as cuId,"
				+ " total_tickets as rTix,total_points as rPoints,customer_rank as rRank "
				+ " from contest_summary_rank_for_week csw with(nolock)" + " where csw.customer_id=" + customerId;

		HallOfFameDtls hallOfFameDtls = null;
		try {

			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				hallOfFameDtls = new HallOfFameDtls();
				hallOfFameDtls.setCuId(rs.getInt("cuId"));
				// hallOfFameDtls.setuId(rs.getString("uId"));
				// hallOfFameDtls.setImgP(rs.getString("imgP"));
				hallOfFameDtls.setrTix(rs.getInt("rTix"));
				hallOfFameDtls.setrPoints(rs.getDouble("rPoints"));
				hallOfFameDtls.setrRank(rs.getInt("rRank"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return hallOfFameDtls;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return hallOfFameDtls;
	}

	public static HallOfFameDtls getTillDateHallofFameByCustomerId(Integer customerId) {

		String sql = " select csw.customer_id as cuId,"
				+ " total_tickets as rTix,total_points as rPoints,customer_rank as rRank"
				+ " from contest_summary_rank_till_date csw with(nolock)" + " where csw.customer_id=" + customerId;
		HallOfFameDtls hallOfFameDtls = null;
		try {

			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				hallOfFameDtls = new HallOfFameDtls();
				hallOfFameDtls.setCuId(rs.getInt("cuId"));
				// hallOfFameDtls.setuId(rs.getString("uId"));
				// hallOfFameDtls.setImgP(rs.getString("imgP"));
				hallOfFameDtls.setrTix(rs.getInt("rTix"));
				hallOfFameDtls.setrPoints(rs.getDouble("rPoints"));
				hallOfFameDtls.setrRank(rs.getInt("rRank"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return hallOfFameDtls;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return hallOfFameDtls;
	}

	
	public static RtfRewardConfigInfo getAllRtfRewardConfigurations(String actionType) {

		RtfRewardConfigInfo rtfRewardConfigInfo = new RtfRewardConfigInfo();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String sql = " select * from  " + DatabaseConnections.quizApiLinkedServer
					+ ".rtf_reward_config with(nolock)  where status = 1 and action_type = '" + actionType + "'";
			System.out.println(sql);
			conn = DatabaseConnections.getRtfConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				rtfRewardConfigInfo.setLives(rs.getInt("no_of_lives"));
				rtfRewardConfigInfo.setMagicWands(rs.getInt("no_of_magic_wand"));
				rtfRewardConfigInfo.setRtfPoints(rs.getInt("rtf_points"));
				rtfRewardConfigInfo.setRwdDollars(rs.getDouble("reward_dollars"));
				rtfRewardConfigInfo.setSfStars(rs.getInt("no_of_sf_stars"));
				rtfRewardConfigInfo.setRewardType(rs.getString("action_type"));
				rtfRewardConfigInfo.setBatchSizePerDay(rs.getInt("batch_size_per_day"));
				rtfRewardConfigInfo.setMaxActionsPerDay(rs.getInt("max_actions_per_day"));
				rtfRewardConfigInfo.setMaxRwdPerDay(rs.getInt("max_rewards_per_day"));
				rtfRewardConfigInfo.setMinRwdInterval(rs.getInt("min_reward_interval"));
				rtfRewardConfigInfo.setMinVidPlayTime(rs.getInt("min_vid_play_time"));
			}

			return rtfRewardConfigInfo;
		} catch (Exception e) {
			rtfRewardConfigInfo = null;
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				stmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			DatabaseConnections.closeConnection(conn); 
		}
		return rtfRewardConfigInfo;
	}

	public static int rewardCustomerWithRTFPoints(Integer cuId, Integer rtfPoints) {
		if (rtfPoints == null)
			rtfPoints = 0;
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".customer  SET "
				+ " rtf_points = isnull(rtf_points,0) + " + rtfPoints + ", last_updated_timestamp = '"
				+ PollingUtil.getCreatedDateStr() + "'" + " WHERE id = " + cuId;
		int updateCnt = 0;
		System.out.println(" update sql rewards : " + sql);
		try {

			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			System.out.println("updated sql reward point  count = " + updateCnt);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}

	public static int rewardCustomerWithLives(Integer custId, Integer lifeCount) {
		if (lifeCount == null)
			lifeCount = 0;
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".customer"
				+ " SET quiz_cust_lives = isnull(quiz_cust_lives , 0 ) + " + lifeCount + " WHERE id = " + custId;
		int updateCnt = 0;
		try {
			System.out.println("[][UPDATE CUSTOMER REWARDS LIFE SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static int rewardCustomerWithMagicWands(Integer custId, Integer magicwand) {
		if (magicwand == null)
			magicwand = 0;
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".customer"
				+ " SET magic_wands  = isnull(magic_wands  , 0 ) + " + magicwand + " WHERE id = " + custId;
		int updateCnt = 0;
		try {
			System.out.println("[][UPDATE CUSTOMER REWARDS magic_wands  SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static int rewardCustomerWithSuperFanStars(Integer custId, Integer superFanStar) {
		if (superFanStar == null)
			superFanStar = 0;
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".customer"
				+ " SET sf_stars   = isnull(sf_stars   , 0 ) + " + superFanStar + " WHERE id = " + custId;
		int updateCnt = 0;
		try {
			System.out.println("[][UPDATE CUSTOMER REWARDS sf_stars   SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			DatabaseConnections.closeConnection(conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static List<RtfPointsDetails> getAllRtfRewardConfigurationsForRtfPointsPage() {
		
		List<RtfPointsDetails> list = new ArrayList<RtfPointsDetails>();
			RtfPointsDetails rtfPointDetail = null;
			try {
				String sql = " select * from  "+ DatabaseConnections.quizApiLinkedServer + ".rtf_reward_config where status = 1 order by rtf_points desc,no_of_lives desc,no_of_sf_stars desc,no_of_magic_wand desc,reward_dollars desc" ;
				System.out.println(sql);
				Connection conn = DatabaseConnections.getRtfConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				while (rs.next()) {
					rtfPointDetail = new RtfPointsDetails();
					rtfPointDetail.setTitle(rs.getString("reward_title"));
					rtfPointDetail.setRtfPoints(rs.getInt("rtf_points"));
					rtfPointDetail.setLives(rs.getInt("no_of_lives"));
					rtfPointDetail.setErasers(rs.getInt("no_of_magic_wand"));
					rtfPointDetail.setSfStars(rs.getInt("no_of_sf_stars"));
					rtfPointDetail.setDollars(rs.getDouble("reward_dollars"));
					list.add(rtfPointDetail);
				}
				rs.close();
				stmt.close();
				DatabaseConnections.closeConnection(conn);
				return list;
			} catch (Exception e) {
				list = null;
				e.printStackTrace();
			}
			return list;
		}

	public static CustomerDailyRewardLimits fetchCustDailyRewardLimitConfig(Integer cuId ) {

		String sql = " SELECT  customer_id ,tot_fc_cr_po ,tot_fc_ev ," + " tot_fc_ev_lk ,tot_fc_po_lk "
				+ " ,tot_fc_vd_ul ,tot_ffo_vd_ul ,tot_sh_vd ," + " tot_vd_lk , tot_vd_vw , rwd_date "
				+ " ,rwd_fc_cr_po , rwd_fc_ev ,rwd_fc_ev_lk ," + " rwd_fc_po_lk ,rwd_fc_vd_ul "
				+ " ,rwd_ffo_vd_ul ,rwd_sh_vd,rwd_vd_lk ,rwd_vd_vw" + " , upd_dttm ," + "	fc_cr_po_dt , fc_ev_dt ,"
				+ "	fc_ev_lk_dt , fc_po_lk_dt ,fc_vd_ul_dt , "
				+ "	ffo_vd_ul_dt , sh_vd_dt ,vd_lk_dt , vd_vw_dt,getdate() as curr_dbdt " + "	FROM "
				+ DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits WHERE " + " customer_id = "
				+ cuId;
		
		
		
		// " and "
		// + " convert(varchar(10), upd_dttm, 102) = "
		// + " convert(varchar(10), getdate(), 102)" ;
		System.out.println(sql);
		CustomerDailyRewardLimits custDalyRwdsLimits = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = DatabaseConnections.getRtfConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			custDalyRwdsLimits = new CustomerDailyRewardLimits();
			while (rs.next()) {
				custDalyRwdsLimits.setCustomerId(rs.getInt("customer_id"));
				custDalyRwdsLimits.setTotFcCrPo(rs.getInt("tot_fc_cr_po"));
				custDalyRwdsLimits.setTotFcEv(rs.getInt("tot_fc_ev"));
				custDalyRwdsLimits.setTotFcEv_lk(rs.getInt("tot_fc_ev_lk"));
				custDalyRwdsLimits.setTotFcPoLk(rs.getInt("tot_fc_po_lk"));
				custDalyRwdsLimits.setTotFcVdUl(rs.getInt("tot_fc_vd_ul"));
				custDalyRwdsLimits.setTotFfo_vd_ul(rs.getInt("tot_ffo_vd_ul"));
				custDalyRwdsLimits.setTot_sh_vd(rs.getInt("tot_sh_vd"));
				custDalyRwdsLimits.setTot_vd_lk(rs.getInt("tot_vd_lk"));
				custDalyRwdsLimits.setTotVdVw(rs.getInt("tot_vd_vw"));			
				
				custDalyRwdsLimits.setRwd_date(rs.getDate("rwd_date"));
				custDalyRwdsLimits.setRwdFc_cr_po(rs.getInt("rwd_fc_cr_po"));
				custDalyRwdsLimits.setRwdFcEv(rs.getInt("rwd_fc_ev"));
				custDalyRwdsLimits.setRwdFcEv_lk(rs.getInt("rwd_fc_ev_lk"));
				custDalyRwdsLimits.setRwdFc_po_lk(rs.getInt("rwd_fc_po_lk"));
				custDalyRwdsLimits.setRwdFc_vd_ul(rs.getInt("rwd_fc_vd_ul"));
				custDalyRwdsLimits.setRwdFfo_vd_ul(rs.getInt("rwd_ffo_vd_ul"));
				custDalyRwdsLimits.setRwd_sh_vd(rs.getInt("rwd_sh_vd"));
				custDalyRwdsLimits.setRwd_vd_lk(rs.getInt("rwd_vd_lk"));
				custDalyRwdsLimits.setRwd_vd_vw(rs.getInt("rwd_vd_vw"));				
				
				custDalyRwdsLimits.setUpd_dttm(rs.getDate("upd_dttm"));
				custDalyRwdsLimits.setFcCrPostDate(rs.getDate("fc_cr_po_dt"));
				custDalyRwdsLimits.setFcEvDate(rs.getDate("fc_ev_dt"));
				custDalyRwdsLimits.setFcEvLkDate(rs.getDate("fc_ev_lk_dt"));
				custDalyRwdsLimits.setFcPostLikeDate(rs.getDate("fc_po_lk_dt"));
				custDalyRwdsLimits.setFcVidUpldDate(rs.getDate("fc_vd_ul_dt"));
				custDalyRwdsLimits.setFfoVidUpldDate(rs.getDate("ffo_vd_ul_dt"));
				custDalyRwdsLimits.setShVidDate(rs.getDate("sh_vd_dt"));
				custDalyRwdsLimits.setVidLkDate(rs.getDate("vd_lk_dt"));
				custDalyRwdsLimits.setVdVwDate(rs.getDate("vd_vw_dt"));
				
				custDalyRwdsLimits.setCurrDate(rs.getDate("curr_dbdt"));

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				stmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			DatabaseConnections.closeConnection(conn);

		}
		return custDalyRwdsLimits;

	}

	final static Map<String, String> custDalyRwdsLimitsTotalColumnMap;
	static {

		custDalyRwdsLimitsTotalColumnMap = new HashMap<String, String>();
		custDalyRwdsLimitsTotalColumnMap.put(SourceType.LIKE_VIDEO.name(), "tot_vd_lk");
		custDalyRwdsLimitsTotalColumnMap.put(SourceType.SHARE_VIDEO.name(), "tot_sh_vd");
		custDalyRwdsLimitsTotalColumnMap.put(SourceType.LIKE_POST.name(), "tot_fc_po_lk");
		custDalyRwdsLimitsTotalColumnMap.put(SourceType.UPLOAD_FAN_FREAKOUT_VIDEO.name(), "tot_ffo_vd_ul");
		custDalyRwdsLimitsTotalColumnMap.put(SourceType.UPLOAD_FANCLUB_VIDEO.name(), "tot_fc_vd_ul");
		custDalyRwdsLimitsTotalColumnMap.put(SourceType.CREATE_POST.name(), "tot_fc_cr_po");
		custDalyRwdsLimitsTotalColumnMap.put(SourceType.CREATE_EVENT.name(), "tot_fc_ev");
		custDalyRwdsLimitsTotalColumnMap.put(SourceType.SHOW_INTEREST_TO_EVENT.name(), "tot_fc_ev_lk");
		// custDalyRwdsLimitsTotalColumnMap.put(SourceType.SCRATCH_AND_WIN.name());
		// custDalyRwdsLimitsTotalColumnMap.put(SourceType.WATCH_VIDEO.name());
	}

	public static String getCustDailyTotalActionColumnForActionType(String key) {
		return custDalyRwdsLimitsTotalColumnMap.get(key);
	}

	final static Map<String, String> custDalyRwdsGivenTotalColumnMap;
	static {

		custDalyRwdsGivenTotalColumnMap = new HashMap<String, String>();
		custDalyRwdsGivenTotalColumnMap.put(SourceType.LIKE_VIDEO.name(), "rwd_vd_lk");
		custDalyRwdsGivenTotalColumnMap.put(SourceType.SHARE_VIDEO.name(), "rwd_sh_vd");
		custDalyRwdsGivenTotalColumnMap.put(SourceType.LIKE_POST.name(), "rwd_fc_po_lk");
		custDalyRwdsGivenTotalColumnMap.put(SourceType.UPLOAD_FAN_FREAKOUT_VIDEO.name(), "rwd_ffo_vd_ul");
		custDalyRwdsGivenTotalColumnMap.put(SourceType.UPLOAD_FANCLUB_VIDEO.name(), "rwd_fc_vd_ul");
		custDalyRwdsGivenTotalColumnMap.put(SourceType.CREATE_POST.name(), "rwd_fc_cr_po");
		custDalyRwdsGivenTotalColumnMap.put(SourceType.CREATE_EVENT.name(), "rwd_fc_ev");
		custDalyRwdsGivenTotalColumnMap.put(SourceType.SHOW_INTEREST_TO_EVENT.name(), "rwd_fc_ev_lk");
		// custDalyRwdsLimitsTotalColumnMap.put(SourceType.SCRATCH_AND_WIN.name());
		// custDalyRwdsLimitsTotalColumnMap.put(SourceType.WATCH_VIDEO.name());
	}

	public static String getCustDailyTotalRwdsGivenColumnForActionType(String key) {
		return custDalyRwdsGivenTotalColumnMap.get(key);
	}

	public static void insertCustDailyRwdsLimitVidLikeStats(CustomerDailyRewardLimits cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_vd_lk,rwd_vd_lk, vd_lk_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustomerId());
			st.setInt(2, cDrl.getTot_vd_lk());
			st.setInt(3, cDrl.getRwd_vd_lk());
			//st.setDate(4, java.sql.Date.valueOf(java.time.LocalDate.now()));
			//st.setTimestamp(5, x);			
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

	}
	
	
	public static void updateCustDailyRwdsLimitVidLikeStats(CustomerDailyRewardLimits cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_vd_lk = ? , rwd_vd_lk = ? ,  vd_lk_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTot_vd_lk());
			st.setInt(2, cDrl.getRwd_vd_lk());
			st.setInt(3, cDrl.getCustomerId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

	}
	
	
	
	
	
	public static void insertCustDailyRwdsLimitVidViewStats(CustomerDailyRewardLimits cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_vd_vw,rwd_vd_vw, vd_vw_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustomerId());
			st.setInt(2, cDrl.getTotVdVw());
			st.setInt(3, cDrl.getRwd_vd_vw());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

	}
	
	public static void updateCustDailyRwdsLimitVidViewStats(CustomerDailyRewardLimits cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_vd_vw = ? , rwd_vd_vw = ? ,  vd_vw_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {
			
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTotVdVw());
			st.setInt(2, cDrl.getRwd_vd_vw());
			st.setInt(3, cDrl.getCustomerId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

	}

	public static void insertCustDailyRwdsLimitCreatePostStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_fc_cr_po,rwd_fc_cr_po, fc_cr_po_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustId());
			st.setInt(2, cDrl.getTotalAction());
			st.setInt(3, cDrl.getRewardedAction());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}
	
	public static void updateCustDailyRwdsLimitCreatePostStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_fc_cr_po = ? , rwd_fc_cr_po = ? ,  fc_cr_po_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {
			
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTotalAction());
			st.setInt(2, cDrl.getRewardedAction());
			st.setInt(3, cDrl.getCustId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

	}

	public static void insertCustDailyRwdsLimitCreateEventStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_fc_ev,rwd_fc_ev, fc_ev_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustId());
			st.setInt(2, cDrl.getTotalAction());
			st.setInt(3, cDrl.getRewardedAction());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}

	public static void updateCustDailyRwdsLimitCreateEventStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_fc_ev = ? , rwd_fc_ev = ? ,  fc_ev_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {
			
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTotalAction());
			st.setInt(2, cDrl.getRewardedAction());
			st.setInt(3, cDrl.getCustId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}

	public static void insertCustDailyRwdsLimitFCVideoUploadStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_fc_vd_ul,rwd_fc_vd_ul, fc_vd_ul_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {
			  
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustId());
			st.setInt(2, cDrl.getTotalAction());
			st.setInt(3, cDrl.getRewardedAction());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}

	public static void updateCustDailyRwdsLimitFCVideoUploadStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_fc_vd_ul = ? , rwd_fc_vd_ul = ? ,  fc_vd_ul_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {
			
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTotalAction());
			st.setInt(2, cDrl.getRewardedAction());
			st.setInt(3, cDrl.getCustId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}

	public static void insertCustDailyRwdsLimitFFOVideoUploadStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_ffo_vd_ul,rwd_ffo_vd_ul, ffo_vd_ul_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {
			  
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustId());
			st.setInt(2, cDrl.getTotalAction());
			st.setInt(3, cDrl.getRewardedAction());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}

	public static void updateCustDailyRwdsLimitFFOVideoUploadStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_ffo_vd_ul = ? , rwd_ffo_vd_ul = ? ,  ffo_vd_ul_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {
			  
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTotalAction());
			st.setInt(2, cDrl.getRewardedAction());
			st.setInt(3, cDrl.getCustId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}
	
	

	public static void insertCustDailyRwdsLimitFCEventLikeStats(CustomerDailyRewardLimits cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_fc_ev_lk,rwd_fc_ev_lk, fc_ev_lk_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {
		 
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustomerId());
			st.setInt(2, cDrl.getTotFcEv_lk());
			st.setInt(3, cDrl.getRwdFcEv_lk());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}
	  
	public static void updateCustDailyRwdsLimitFCEventLikeStats(CustomerDailyRewardLimits cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_fc_ev_lk = ? , rwd_fc_ev_lk = ? ,  fc_ev_lk_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {
			  
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTotFcEv_lk());
			st.setInt(2, cDrl.getRwdFcEv_lk());
			st.setInt(3, cDrl.getCustomerId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		
	}
	
		
	
	public static void insertCustDailyRwdsLimitFCPostLikeStats(CustomerDailyRewardLimits cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_fc_po_lk,rwd_fc_po_lk, fc_po_lk_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {			  
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustomerId());
			st.setInt(2, cDrl.getTotFcPoLk());
			st.setInt(3, cDrl.getRwdFc_po_lk());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}		
	}	  
	  
	public static void updateCustDailyRwdsLimitFCPostLikeStats(CustomerDailyRewardLimits cDrl, Connection conn) {
		PreparedStatement st = null;

		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_fc_po_lk = ? , rwd_fc_po_lk = ? ,  fc_po_lk_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {			  
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTotFcPoLk());
			st.setInt(2, cDrl.getRwdFc_po_lk());
			st.setInt(3, cDrl.getCustomerId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}		
	}

	public static void insertCustDailyRwdsLimitShareVideoStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;
		String sql = " INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " (customer_id, tot_sh_vd,rwd_sh_vd, sh_vd_dt,upd_dttm ) "
				+ " VALUES " + " (?,?,?, getDate(),getDate())  ";
		try {			 
			st = conn.prepareStatement(sql);
			st.setInt(1, cDrl.getCustId());
			st.setInt(2, cDrl.getTotalAction());
			st.setInt(3, cDrl.getRewardedAction());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}		
	}	  
	  
	public static void updateCustDailyRwdsLimitShareVideoStats(CustDailyRewardsVO cDrl, Connection conn) {
		PreparedStatement st = null;
		String sql = " UPDATE  " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_daily_reward_limits"
				+ " SET tot_sh_vd = ? , rwd_sh_vd = ? ,  sh_vd_dt = getDate() ,upd_dttm = getDate() "
				+ "where customer_id = ? " ;
		try {				  
			st = conn.prepareStatement(sql);			
			st.setInt(1, cDrl.getTotalAction());
			st.setInt(2, cDrl.getRewardedAction());
			st.setInt(3, cDrl.getCustId());					
			st.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				st.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}		
	}	
	
	public static List<HallOfFameDtls> getRtfLivtHallOfFameByTillDateFromView(Integer summaryListCount) {

		/*	String sql = " select top " + summaryListCount + " c.id as cuId,c.user_id as uId,c.cust_image_path as imgP,"
					+ " TotalTickets as rTix,TotalPoints as rPoints,CusRank as rRank"
					+ " from contest_winners_rank_till_date_view csw with(nolock)" + " inner join "
					+ DatabaseConnections.zonesApiLinkedServer + ".customer c with(nolock) on c.id=csw.customer_id"
					+ " where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by CusRank";*/
			
			String sql = "select Top "+summaryListCount+" customer_id,user_id, user_img, total_winnings, customer_rank from rtf_hof_summary_rank_till_date "
					+ "	where customer_rank > 0"
					+ "	order by customer_rank";
			
			List<HallOfFameDtls> hallOfFameList = new ArrayList<HallOfFameDtls>();
			try {

				Connection conn = DatabaseConnections.getRtfEcomConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				while (rs.next()) {
					HallOfFameDtls hallOfFameDtls = new HallOfFameDtls();
					hallOfFameDtls.setCuId(rs.getInt("customer_id"));
					hallOfFameDtls.setuId(rs.getString("user_id"));
					hallOfFameDtls.setImgP(rs.getString("user_img"));
					//hallOfFameDtls.setrTix(rs.getInt("rTix"));
					//hallOfFameDtls.setrPoints(rs.getDouble("rPoints"));
					hallOfFameDtls.setGameWon(rs.getInt("total_winnings"));
					hallOfFameDtls.setrRank(rs.getInt("customer_rank"));
					hallOfFameList.add(hallOfFameDtls);
				}
				rs.close();
				stmt.close();
				DatabaseConnections.closeConnection(conn);
				return hallOfFameList;
			} catch (Exception e) {

				e.printStackTrace();
			}
			return hallOfFameList;
		}
	public static HallOfFameDtls getRtfLivtTillDateHallofFameByCustomerId(Integer customerId) {

		String sql = "select customer_id,user_id, user_img, total_winnings, customer_rank from rtf_hof_summary_rank_till_date "
				+ "	where customer_id=" + customerId;
		HallOfFameDtls hallOfFameDtls = null;
		try {

			Connection conn = DatabaseConnections.getRtfEcomConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				hallOfFameDtls = new HallOfFameDtls();
				hallOfFameDtls.setCuId(rs.getInt("customer_id"));
				hallOfFameDtls.setuId(rs.getString("user_id"));
				hallOfFameDtls.setImgP(rs.getString("user_img"));
				//hallOfFameDtls.setrTix(rs.getInt("rTix"));
				//hallOfFameDtls.setrPoints(rs.getDouble("rPoints"));
				hallOfFameDtls.setGameWon(rs.getInt("total_winnings"));
				hallOfFameDtls.setrRank(rs.getInt("customer_rank"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return hallOfFameDtls;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return hallOfFameDtls;
	}

	
	
}
