package com.zonesws.webservices.utils;

import java.io.File;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.quiz.cassandra.servlet.LoadApplicationValuesServlet;
import com.zonesws.webservices.data.RtfConfigContestClusterNodes;
import com.zonesws.webservices.enums.ApplicationPlatForm;



public class URLUtil {
	
	private static ZonesProperty properties;
	 private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	/* public ZonesProperty getProperties() {
		return properties;
	 }

	 public void setProperties(ZonesProperty zonesProperties) {
		properties = zonesProperties;
	 }*/
	public static String genericErrorMsg = "Something went wrong, Please try again.";
	public static String basePath = "";
	public static String emailImageBasePath = "";
	public static String eventShareLinkBaseUrl = "";
	public static String apiServerSvgMapsBaseURL = "";
	public static String DP_PRFEIX_CODE = "";
	public static String DP_FOLDER_NAME = "";
	public static String rewardTheFanServerSvgMapsBaseUrl;
	public static String apiServerBaseUrl;
	public static String rewardTheFanServerBaseUrl;
	public static String paypalEnvironment;
	public static String DEFAULT_DP_PRFEIX_CODE = "";
	public static String DEFAULT_DP_FOLDER_NAME = "";
	public static String fromEmail;
	public static String bccEmails;
	public static String fromEmailForPromo;
	public static String rtfImageSharedPath;	
	public static String rtfSharedDriveUserName;
	public static String rtfSharedDrivePassword;
	public static Boolean isProductionEnvironment;
	public static Boolean isMasterNodeServer;
	public static Boolean isSharedDriveOn=false;
	public static String svgTextFilePath;
	public static String imageServerURL;
	
	public static String CUSTOMER_DP_DIRECTORY = "dp";
	public static String awsS3Url = "https://rtfservermedia.s3.amazonaws.com/";
	
	public static Integer clusterNodeId=0;
	
	public static void setZonesPropertValues() {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		ZonesProperty zonesProperty = new ZonesProperty();
		
		zonesProperty.setServerWebAppBasePath(resourceBundle.getString("server.webapp.basepath"));
		zonesProperty.setRtfDesktopSiteUrl(resourceBundle.getString("rtf.desktop.site.url"));
		zonesProperty.setRtfCustomerDPPrefix(resourceBundle.getString("rtf.customer.dp.prefix"));
		zonesProperty.setRtfCustomerDPFolder(resourceBundle.getString("rtf.customer.dp.folder"));
		zonesProperty.setRtfDefaultDPPrefix(resourceBundle.getString("rtf.customer.default.dp.prefix"));
		zonesProperty.setRtfDefaultDPFolder(resourceBundle.getString("rtf.customer.default.dp.folder"));
		zonesProperty.setRtfQuizVideo(resourceBundle.getString("rtf.quiz.video"));
		zonesProperty.setApiServerSvgMapsBaseURL(resourceBundle.getString("api.server.svg.maps.base.url"));
		zonesProperty.setApiServerBaseUrl(resourceBundle.getString("api.server.base.url"));
		zonesProperty.setRtfImageSharedPath(resourceBundle.getString("rtf.image.shared.path"));
		zonesProperty.setRtfSharedDriveUserName(resourceBundle.getString("rtf.shared.drive.user.name"));
		zonesProperty.setRtfSharedDrivePassword(resourceBundle.getString("rtf.shared.drive.password"));
		zonesProperty.setEmailFrom(resourceBundle.getString("rtf.email.from"));
		zonesProperty.setEmailBcc(resourceBundle.getString("rtf.email.bcc"));
		zonesProperty.setEmailFromForPromo(resourceBundle.getString("rtf.email.from.for.promo"));
		zonesProperty.setIsProductionEnvironment(Boolean.valueOf(resourceBundle.getString("rtf.is.production.environment")));
		zonesProperty.setMasterNodeServerIP(resourceBundle.getString("rtf.master.node.server.ip"));
		zonesProperty.setIsSharedDriveOn(Boolean.valueOf(resourceBundle.getString("rtf.is.shared.drive.on")));
		zonesProperty.setSvgTextFilePath(resourceBundle.getString("rtf.svg.text.file.path"));
		zonesProperty.setImageServerURL(resourceBundle.getString("rtf.imageserver.url"));
		
		System.out.println(" IMAGE SERVER PATH == " + zonesProperty.getImageServerURL());
		
		
		
		properties = zonesProperty;
	}

//public void afterPropertiesSet() throws Exception {	
	public static void loadApplicationValues() throws Exception {
		
		try{
			setZonesPropertValues();
			
			rewardTheFanServerSvgMapsBaseUrl = properties.getApiServerSvgMapsBaseURL();
			basePath = properties.getServerWebAppBasePath();			
			emailImageBasePath = "/rtw/apache-tomcat-7.0.92/webapps/ROOT/resources/email-resources//";
			eventShareLinkBaseUrl = properties.getRtfDesktopSiteUrl();
			fromEmail = properties.getEmailFrom();
			bccEmails = properties.getEmailBcc();
			fromEmailForPromo = properties.getEmailFromForPromo();
			apiServerSvgMapsBaseURL = properties.getApiServerSvgMapsBaseURL();
			apiServerBaseUrl = properties.getApiServerBaseUrl();
			DP_PRFEIX_CODE = properties.getRtfCustomerDPPrefix();
			DP_FOLDER_NAME = properties.getRtfCustomerDPFolder();
			DEFAULT_DP_PRFEIX_CODE = properties.getRtfDefaultDPPrefix();
			DEFAULT_DP_FOLDER_NAME = properties.getRtfDefaultDPFolder();
			paypalEnvironment = properties.getPaypalEnvironment();
			isProductionEnvironment = properties.getIsProductionEnvironment();
			rtfImageSharedPath = properties.getRtfImageSharedPath();
			rtfSharedDriveUserName = properties.getRtfSharedDriveUserName();
			rtfSharedDrivePassword = properties.getRtfSharedDrivePassword();
			rewardTheFanServerBaseUrl = "https://www.rewardthefan.com/";
			
						
			String masterIp = properties.getMasterNodeServerIP();
			String localIP = InetAddress.getLocalHost().getHostAddress();
			if(masterIp != null && masterIp.equals(localIP)) {
				isMasterNodeServer = true;
			}else {
				isMasterNodeServer = false;
			}
			
			isSharedDriveOn = properties.getIsSharedDriveOn();
			svgTextFilePath = properties.getSvgTextFilePath();
			
			imageServerURL = properties.getImageServerURL();
			
			System.out.println("imageServerURL==========>"+imageServerURL);
			
			System.out.println("URLUTIL : masterIp : "+masterIp+" :localIP: "+localIP+" :isMasterNodeServer: "+isMasterNodeServer+" : "+new Date());
			
			System.out.println("basePath==========>"+basePath);
			System.out.println("eventShareLinkBaseUrl==========>"+eventShareLinkBaseUrl);
			System.out.println("apiServerBaseURL==========>"+apiServerSvgMapsBaseURL);
			System.out.println("DP_PRFEIX_CODE==========>"+DP_PRFEIX_CODE);
			System.out.println("DP_FOLDER_NAME==========>"+DP_FOLDER_NAME);
			System.out.println("DEFAULT_DP_PRFEIX_CODE==========>"+DEFAULT_DP_PRFEIX_CODE);
			System.out.println("DEFAULT_DP_FOLDER_NAME==========>"+DEFAULT_DP_FOLDER_NAME);
			System.out.println("FROM EMAIL==========>"+fromEmail);
			System.out.println("BCC EMAIL==========>"+bccEmails);
			System.out.println("From Promo EMAIL==========>"+fromEmailForPromo);
			System.out.println("IS Production ==========>"+isProductionEnvironment);
			System.out.println("IS MASTER NODE IP ==========>"+isMasterNodeServer);
			System.out.println("IS SHARED DRIVE ON ==========>"+isSharedDriveOn);
		}catch(Exception e){
			basePath = "////C://Tomcat 7.0_Tomcat7.production//webapps//";
			emailImageBasePath = "////C://Tomcat 7.0_Tomcat7.production//webapps//ROOT//resources//email-resources//";
			eventShareLinkBaseUrl = "https://www.rewardthefan.com/";
			apiServerSvgMapsBaseURL = "https://api.rewardthefan.com/SvgMaps/";
			DP_PRFEIX_CODE = "REWARDTHEFAN_DP";
			DP_FOLDER_NAME = "CUSTOMER_DP";
			DEFAULT_DP_PRFEIX_CODE = "RTF_DEFAULT_DP";
			DEFAULT_DP_FOLDER_NAME = "DEFAULT_DP";
			rtfImageSharedPath = "smb://10.0.0.91//d$//Rewardthefan//";
			rtfSharedDriveUserName = "dev";
			rtfSharedDrivePassword = "Rb#32H8!";
			isProductionEnvironment = true;
			isMasterNodeServer = false;
			isSharedDriveOn = false;
			svgTextFilePath = "//rtw//SVG//Texts//";
			imageServerURL = "http://18.213.28.88";
			e.printStackTrace();
		}
	}
	
	public static String getStoredSvgApiUrl(){
		return apiServerSvgMapsBaseURL+"Stored_SVG/";
	}
	
	public static String getSVGTextFolderPath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"Texts//";
		}else {
			return "/rtw/SVG//Texts/";
		}
	}
	
	public static String getSVGMapPath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//";
		}else {
			return basePath+"SvgMaps//";
		}
	}
	
	public static String getStoredSVGMapPath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//Stored_SVG//";
		}else {
			return basePath+"SvgMaps//Stored_SVG//";
		}
	}
	
	public static String getCardsImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//Cards//";
		}else {
			return basePath+"SvgMaps//Cards//";
		}
	}
	
	public static String getExploreCardsImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//Cards//Explore//";
		}else {
			return basePath+"SvgMaps//Cards//Explore//";
		}
	}
	
	public static String getPaymentIconImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//PaymentIcons//";
		}else {
			return basePath+"SvgMaps//PaymentIcons//";
		}
	} 
	
	public static String getRegularImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//";
		}else {
			return basePath+"SvgMaps//";
		}
	}
	
	public static String getCustomerProfileImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//"+DP_FOLDER_NAME+"//";
		}else {
			return basePath+"SvgMaps//"+DP_FOLDER_NAME+"//";
		}
	}
	 
	public static String getPromotionalDialogImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//PromotionalDialog//";
		}else {
			return basePath+"SvgMaps//PromotionalDialog//";
		}
	}
	
	
	public static String getCustomerProfilePicUrl(String fileName){
		//http://52.201.48.95:90/staticfiles/REWARDTHEFAN_DP_100524.jpg
		return "http://52.201.48.95:90/staticfiles/"+fileName;
		//return apiServerBaseUrl+FilesController.profileImagePath+"?ft="+FilesType.profilepic+"&fn="+fileName;
	}
	
	public static String profilePicUploadDirectoryOld(String profilePicPrefix,Integer customerId,String ext){
		String fileLocation = "";
		try{
			fileLocation = URLUtil.basePath+"SvgMaps//"+DP_FOLDER_NAME+"//" + profilePicPrefix+"_"+customerId+"."+ext;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String profilePicUploadDirectory(String profilePicPrefix,Integer customerId,String ext){
		String fileLocation = "";
		try{
			fileLocation = getSVGMapPath()+DP_FOLDER_NAME+"//" + profilePicPrefix+"_"+customerId+"."+ext;
		}catch(Exception e){
			e.printStackTrace(); 
		} 
		return fileLocation;
	}
	
	public static String profilePicBaseDirectory(){
		String fileLocation = "";
		try{
			fileLocation = getSVGMapPath()+DP_FOLDER_NAME+"//";
		}catch(Exception e){
			e.printStackTrace(); 
		} 
		return fileLocation;
	}
	
	public static String profilePicWebURL(String profilePicPrefix,Integer customerId,String ext, ApplicationPlatForm platform){
		String fileLocation = "";
		try{
			if(URLUtil.isSharedDriveOn) {
				fileLocation = getCustomerProfilePicUrl(profilePicPrefix+"_"+customerId+"."+ext);
			}else {
				fileLocation = apiServerSvgMapsBaseURL+DP_FOLDER_NAME+"/"+profilePicPrefix+"_"+customerId+"."+ext;
			} 
		}catch(Exception e){ 
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String profilePicWebURByImageName(String fileName, ApplicationPlatForm platform){
		String fileLocation = "";
		try{
			if(URLUtil.isSharedDriveOn) {
				fileLocation = getCustomerProfilePicUrl(fileName);
			}else {
				fileLocation = apiServerSvgMapsBaseURL+DP_FOLDER_NAME+"/"+fileName;
				//fileLocation = imageServerURL+"/"+fileName;
			}  
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}

	public static String profilePicWebURByImageName(String fileName){
		String fileLocation = "";
		try{
			if(URLUtil.isSharedDriveOn) {
				fileLocation = getCustomerProfilePicUrl(fileName);
			}else {
				fileLocation = URLUtil.apiServerSvgMapsBaseURL+DP_FOLDER_NAME+"/"+fileName;
			} 
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getProfilePicFile(String custImagePath){
		String fileLocation = "";
		try{
			fileLocation = getSVGMapPath()+DP_FOLDER_NAME+"//" + custImagePath;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getProfilePicFolderPathNotUsing(){
		return URLUtil.basePath+"SvgMaps//"+DP_FOLDER_NAME+"//";
	}
	
	public static String getDefaultProfilePicFile(String defaultImagePath){
		String fileLocation = "";
		try{
			fileLocation = URLUtil.getSVGMapPath()+DEFAULT_DP_FOLDER_NAME+"//" + defaultImagePath+".jpg";
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	
	public static String profilePicForSummaryNotUsing(String fileName){
		String fileLocation = "";
		try{
			if(URLUtil.isSharedDriveOn) {
				fileLocation = getCustomerProfilePicUrl(fileName);
			}else {
				if(URLUtil.isProductionEnvironment) {
					//fileLocation = apiServerSvgMapsBaseURL+DP_FOLDER_NAME+"/"+fileName;
					fileLocation = imageServerURL+"/"+fileName;
				} else {
					fileLocation = apiServerSvgMapsBaseURL+DP_FOLDER_NAME+"/"+fileName;
				}
			}  
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String profilePicForSummary(String fileName){
		String fileUrl = awsS3Url+CUSTOMER_DP_DIRECTORY+"/"+fileName;
		return fileUrl;
	}
	public static void main(String[] args) throws Exception {
		computeClusterNodeId();
	}
	public static void computeClusterNodeId() throws Exception {
		String ipAddress = InetAddress.getLocalHost().getHostAddress();
		String tomcatBaseDirectory = System.getProperty( "catalina.base" ) ;
		String nodeDirectoryPath = ipAddress+"/"+tomcatBaseDirectory;
		//System.out.println("Cluster Node ipAddress :"+ipAddress+" :tomcatBaseDirectory :"+tomcatBaseDirectory+" : address:"+nodeDirectoryPath);
		
		String location = tomcatBaseDirectory+"/conf/server.xml";
		System.out.println("Location : "+location);
		File serverXml = new File (location); 
		Integer port = LoadApplicationValuesServlet.getTomcatPortFromConfigXml(serverXml);
		System.out.println("Port : "+port);
		nodeDirectoryPath = ipAddress+"/"+port;
		
		List<RtfConfigContestClusterNodes> clusterNodeList = SQLDaoUtil.getAllRtfConfigClusterNodeDetails();
		if(clusterNodeList != null) {
			for (RtfConfigContestClusterNodes clusterNode : clusterNodeList) {
				if(clusterNode.getDirectoryPath()!= null && clusterNode.getDirectoryPath().equalsIgnoreCase(nodeDirectoryPath)) {
					clusterNodeId = clusterNode.getId();
					break;
				}
			}
		}
		System.out.println("Cluster Node ipAddress :"+ipAddress+" :tomcatBaseDirectory :"+tomcatBaseDirectory+" : address:"+nodeDirectoryPath+":clusterNodeId : "+clusterNodeId);
	}
}