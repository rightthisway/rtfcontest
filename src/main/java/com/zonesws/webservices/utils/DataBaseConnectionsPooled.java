package com.zonesws.webservices.utils;

import java.sql.Connection;

import javax.servlet.ServletContext;


public class DataBaseConnectionsPooled {


	public static final String ID = "rtfpoolconn:1:1";

	public static Connection getRtfConnection(ServletContext context) throws Exception {
		Connection con = null;
		try {
			con =  (Connection) context.getAttribute("rtfdatasourceConn");			
			System.out.println(" Aquired RTF FROM POOLED CONNECTION :::: " + con.getCatalog());
			return con;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
	
	public static Connection getZonesApiConnection(ServletContext context) throws Exception {
		Connection con = null;
		try {
			con =  (Connection) context.getAttribute("zonesdatasourceConn");			
			System.out.println(" Aquired ZONES FROM POOLED CONNECTION :::: " + con.getCatalog());
			return con;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

}