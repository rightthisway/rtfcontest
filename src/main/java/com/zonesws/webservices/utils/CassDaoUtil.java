package com.zonesws.webservices.utils;

import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.quiz.cassandra.config.CassandraConnector;

public class CassDaoUtil {

	public static Integer updateCustomerRTFPoints(Integer cuId, Integer rtfPoints) {
		Integer updCnt = 0;
		try {
			CassandraConnector.getSession().execute("UPDATE customer set  " + "rtf_points = ? where customer_id = ? ",
					rtfPoints, cuId);
			updCnt = 1;
		} catch (QueryExecutionException qex) {
			System.out.println(qex);
			updCnt = 0;
		} catch (QueryValidationException qvx) {
			System.out.println(qvx);
			updCnt = 0;
		}
		return updCnt;
	}

	public static Integer updateCustomerLives(Integer cuId, Integer quizLives) {
		Integer updCnt = 0;
		try {
			CassandraConnector.getSession().execute("UPDATE customer set  " + "no_of_lives = ? where customer_id = ? ",
					quizLives, cuId);
			updCnt = 1;
		} catch (QueryExecutionException qex) {
			System.out.println(qex);
			updCnt = 0;
		} catch (QueryValidationException qvx) {
			System.out.println(qvx);
			updCnt = 0;
		}
		return updCnt;
	}

	public static Integer updateCustomerMagicWand(Integer cuId, Integer magicWands) {
		Integer updCnt = 0;
		try {
			CassandraConnector.getSession().execute("UPDATE customer set  " + "magicwand = ? where customer_id = ? ",
					magicWands, cuId);
			updCnt = 1;
		} catch (QueryExecutionException qex) {
			System.out.println(qex);
			updCnt = 0;
		} catch (QueryValidationException qvx) {
			System.out.println(qvx);
			updCnt = 0;
		}
		return updCnt;
	}

	public static Integer updateCustomerSFStars(Integer cuId, Integer sfstars) {
		Integer updCnt = 0;
		try {
			CassandraConnector.getSession()
					.execute("UPDATE customer set  " + "super_fan_chances = ? where customer_id = ? ", sfstars, cuId);
			updCnt = 1;
		} catch (QueryExecutionException qex) {
			System.out.println(qex);
			updCnt = 0;
		} catch (QueryValidationException qvx) {
			System.out.println(qvx);
			updCnt = 0;
		}
		return updCnt;
	}

}
