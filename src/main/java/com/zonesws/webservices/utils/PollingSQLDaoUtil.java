package com.zonesws.webservices.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.AbuseReportInfo;
import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.list.CustomerDailyRewardLimits;
import com.quiz.cassandra.list.FCPlayListInfo;
import com.quiz.cassandra.list.FanClubPostsDTO;
import com.quiz.cassandra.list.FanClubVidPlayListDTO;
import com.quiz.cassandra.list.MyVideoInfo;
import com.quiz.cassandra.list.PollingAnswerInfo;
import com.quiz.cassandra.list.PollingContest;
import com.quiz.cassandra.list.PollingQuestionInfo;
import com.quiz.cassandra.list.PollingRewardsInfo;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.PollingVideoRewardsConfig;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.service.PollingAnswerService;
import com.quiz.cassandra.service.PollingQuestionService;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.data.Category;
import com.zonesws.webservices.data.PollingCategory;
import com.zonesws.webservices.data.PollingCustomerRewards;
import com.zonesws.webservices.data.PollingVideoInventory;
import com.zonesws.webservices.enums.SourceType;

public class PollingSQLDaoUtil {
	private static Connection rtfZonesConnection; // remove this at deployment..

	public static PollingContest getActivePollingContest() {

		String sql = "select id , polling_interval , max_que_per_cust," + "start_date , end_date " + "  from "
				+ DatabaseConnections.zonesApiLinkedServer + ".polling_contest with(nolock)  where status='ACTIVE' "
				// + " and convert(varchar(10), end_date, 102) "
				// + " >= convert(varchar(10), getdate(), 102) ";
				+ " and end_date  >= getdate() ";

		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Polling Contest Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			PollingContest pollingContest = new PollingContest();
			while (rs.next()) {
				pollingContest.setId(rs.getInt("id"));
				pollingContest.setPollingInterval(rs.getInt("polling_interval"));
				pollingContest.setMaxQuePerCust(rs.getInt("max_que_per_cust"));
				pollingContest.setStartDate(rs.getDate("start_date"));
				pollingContest.setEndDate(rs.getDate("end_date"));

			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return pollingContest;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<PollingCategory> getCategoryForPollingId(Integer ContestId) {

		String sql = " SELECT A.contest_id  AS contest_id, b.polling_type as "
				+ " polling_type ,  A.category_id  AS category_id from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_contest_category_mapper A with(nolock) " + " JOIN " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_category B " + " on  A.category_id=B.id " + " WHERE A.CONTEST_ID ="
				+ String.valueOf(ContestId);

		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// Connection conn = getZonesApiConnection();
			// System.out.println("Polling Contest Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			Integer pollingId = null;
			List<PollingCategory> pollCatMappedList = new ArrayList<PollingCategory>();
			while (rs.next()) {
				PollingCategory pollCat = new PollingCategory();
				pollCat.setContestId(rs.getInt("contest_id"));
				pollCat.setPollingType(rs.getString("polling_Type"));
				pollCat.setCategoryId(rs.getInt("category_id"));
				pollCatMappedList.add(pollCat);

			}

			// System.out.println(pollCatMappedList);
			rs.close();
			stmt.close();
			closeConnection( conn);
			return pollCatMappedList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static PollingQuestionInfo getRTFQuestionForCustomer(PollingQuestionInfo pollingQuestionInfo) {
		String sql = "select  top (1)  id ," + " question , " + " option_a , " + " option_b ," + " option_c ,"
				+ " answer , " + " status , " + " category, " + " dificulty_level, " + " fact_checked " + " from    "
				+ " " + DatabaseConnections.quizApiLinkedServer + ".question_bank with(nolock) "
				+ " where fact_checked=1 and id not in "
				+ " ( select IIF( question_id IS NULL , 0 , question_id ) from  "
				+ DatabaseConnections.zonesApiLinkedServer + ".polling_answer_validation" + " where cust_id = "
				+ pollingQuestionInfo.getCustId() + " and contest_id = " + pollingQuestionInfo.getpCtId()
				+ " and convert(varchar(10), created_date, 102) " + " = convert(varchar(10), getdate(), 102) "
				+ " and polling_type = '" + pollingQuestionInfo.getPollType() + "'" + " ) ";

		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			// System.out.println("Polling QuestionBank Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			List<String> optList = new ArrayList<String>();
			while (rs.next()) {
				pollingQuestionInfo.setqTxt(rs.getString("question"));
				pollingQuestionInfo.setqId(rs.getInt("id"));
				optList.add(rs.getString("option_a"));
				optList.add(rs.getString("option_b"));
				optList.add(rs.getString("option_c"));
				pollingQuestionInfo.setAnsOpts(optList);
				pollingQuestionInfo.setAnswer(rs.getString("answer"));
				// rtfQBank.setStatus(rs.getString("status"));
				// rtfQBank.setCategory(rs.getString("category"));
				// rtfQBank.setDificultyLevel(rs.getString("dificulty_level"));

			}

			rs.close();
			stmt.close();
			closeConnection( conn);
			return pollingQuestionInfo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * public static PollingQuestionInfo
	 * getRTFQuestionForCustomerFromSponsor(PollingQuestionInfo pollingQuestionInfo,
	 * Integer pollCategoryId) { String sql = "select top (1) id ," + " question , "
	 * + " option_a , " + " option_b ," + " option_c ," + " answer , " + " status "
	 * + " from " + " " + DatabaseConnections.zonesApiLinkedServer +
	 * ".polling_question_bank with(nolock) " + " where poll_cat_id= " +
	 * pollCategoryId + " and id not in " + " ( select IIF( question_id IS NULL , 0
	 * , question_id ) from " + DatabaseConnections.zonesApiLinkedServer +
	 * ".polling_answer_validation" + " where cust_id = " +
	 * pollingQuestionInfo.getCustId() + " and contest_id = " +
	 * pollingQuestionInfo.getpCtId() + " and convert(varchar(10), created_date,
	 * 102) " + " = convert(varchar(10), getdate(), 102) " + " and category_id = " +
	 * pollCategoryId + " ) " ;
	 * 
	 * try { Connection conn = DatabaseConnections.getRtfConnection();
	 * //System.out.println("Polling Sponsor QuestionBank Query : " + sql);
	 * Statement stmt = conn.createStatement(); ResultSet rs =
	 * stmt.executeQuery(sql); List<String> optList = new ArrayList<String>(); while
	 * (rs.next()) { pollingQuestionInfo.setqTxt(rs.getString("question"));
	 * pollingQuestionInfo.setqId(rs.getInt("id"));
	 * optList.add(rs.getString("option_a")); optList.add(rs.getString("option_b"));
	 * optList.add(rs.getString("option_c"));
	 * pollingQuestionInfo.setAnsOpts(optList);
	 * pollingQuestionInfo.setAnswer(rs.getString("answer")); //
	 * rtfQBank.setStatus(rs.getString("status")); //
	 * rtfQBank.setCategory(rs.getString("category")); //
	 * rtfQBank.setDificultyLevel(rs.getString("dificulty_level"));
	 * 
	 * }
	 * 
	 * rs.close(); stmt.close(); return pollingQuestionInfo; } catch (SQLException
	 * se) { se.printStackTrace(); } catch (Exception e) { e.printStackTrace(); }
	 * return null; }
	 **/

	public static PollingQuestionInfo getRTFQuestionForCustomerFromSponsor(PollingQuestionInfo pollingQuestionInfo,
			Integer pollCategoryId) {
		String sql = "select    id ," + " question , " + " option_a , " + " option_b ," + " option_c ," + " answer , "
				+ " status  " + " from    " + " " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_question_bank with(nolock) " + " where poll_cat_id= " + pollCategoryId + " and  id not in "
				+ " ( select IIF( question_id IS NULL , 0 , question_id ) from  "
				+ DatabaseConnections.zonesApiLinkedServer + ".polling_answer_validation with(nolock) " + " where cust_id = "
				+ pollingQuestionInfo.getCustId() + " and contest_id = " + pollingQuestionInfo.getpCtId()
				+ " and convert(varchar(10), created_date, 102) " + " = convert(varchar(10), getdate(), 102) "
				+ " and category_id = " + pollCategoryId + " ) ";

		List<PollingQuestionInfo> queInfoList = new ArrayList<PollingQuestionInfo>();

		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Polling QuestionBank Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				PollingQuestionInfo rsPollingQuestionInfo = new PollingQuestionInfo();
				List<String> optList = new ArrayList<String>();
				// pollingQuestionInfo.setqTxt(rs.getString("question"));
				// pollingQuestionInfo.setqId(rs.getInt("id"));
				// optList.add(rs.getString("option_a"));
				// optList.add(rs.getString("option_b"));
				// optList.add(rs.getString("option_c"));
				// pollingQuestionInfo.setAnsOpts(optList);
				// pollingQuestionInfo.setAnswer(rs.getString("answer"));
				// rtfQBank.setStatus(rs.getString("status"));
				// rtfQBank.setCategory(rs.getString("category"));
				// rtfQBank.setDificultyLevel(rs.getString("dificulty_level"));

				rsPollingQuestionInfo.setqTxt(rs.getString("question"));
				rsPollingQuestionInfo.setqId(rs.getInt("id"));
				optList.add(rs.getString("option_a"));
				optList.add(rs.getString("option_b"));
				optList.add(rs.getString("option_c"));
				rsPollingQuestionInfo.setAnsOpts(optList);
				rsPollingQuestionInfo.setAnswer(rs.getString("answer"));
				queInfoList.add(rsPollingQuestionInfo);

			}

			rs.close();
			stmt.close();
			closeConnection( conn);
			// conn.close();
			if (queInfoList != null && queInfoList.size() > 0) {
				Collections.shuffle(queInfoList);
				PollingQuestionInfo randInfo = (PollingQuestionInfo) queInfoList.get(0);
				pollingQuestionInfo.setqTxt(randInfo.getqTxt());
				pollingQuestionInfo.setqId(randInfo.getqId());
				pollingQuestionInfo.setAnsOpts(randInfo.getAnsOpts());
				pollingQuestionInfo.setAnswer(randInfo.getAnswer());
				return pollingQuestionInfo;
			}

			// return pollingQuestionInfo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static PollingQuestionInfo insertCustomerQuestionInfo(PollingQuestionInfo pollingQuestionInfo) {
		Calendar cal = Calendar.getInstance();
		java.sql.Timestamp timestamp = new java.sql.Timestamp(cal.getTimeInMillis());
		String sql = "  INSERT INTO  " + DatabaseConnections.zonesApiLinkedServer + ".polling_answer_validation" + " ("
				+ " contest_id " + ", category_id " + ", cust_id " + ", question_id " + ", correct_ans "
				+ ", created_date" + ", polling_type, correct_ans_text  ) " + "values ( "
				+ pollingQuestionInfo.getpCtId() + " , " + pollingQuestionInfo.getpCatId() + " , "
				+ pollingQuestionInfo.getCustId() + " , " + pollingQuestionInfo.getqId() + " , '"
				+ pollingQuestionInfo.getAnswer() + "'" + ", '" + timestamp.toString() + "'" + " , '"
				+ pollingQuestionInfo.getPollType() + "'," + " " + "'" + pollingQuestionInfo.getCrtAnsTxt() + "'"
				+ " ) ";

		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Polling Question info Insert Query : " + sql);
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating QuestionInfo failed, no rows affected.");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					pollingQuestionInfo.setId(generatedKeys.getInt(1));					
				} else {
					throw new SQLException("Creating QuestionInfo failed, no ID obtained.");
				}
				generatedKeys.close();
			} catch (Exception ex) {
				pollingQuestionInfo.setSts(0);
				ex.printStackTrace();
			}

			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pollingQuestionInfo;

	}

	public static Integer getPollQuestionAnsweredCountForCustomer(PollingQuestionInfo pollingQuestionInfo) {
		String sql = "select  count(*) as queCount from  " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_answer_validation with(nolock)" + " where cust_id = " + pollingQuestionInfo.getCustId()
				+ " and contest_id = " + pollingQuestionInfo.getpCtId()
				+ " and convert(varchar(10), created_date, 102) " + " = convert(varchar(10), getdate(), 102) ";

		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			// System.out.println("Polling question count Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			Integer qCount = new Integer("0");
			while (rs.next()) {
				qCount = rs.getInt("queCount");
			}

			rs.close();
			stmt.close();
			closeConnection( conn);
			return qCount;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static PollingAnswerInfo fetchPollingAnswerDets(PollingAnswerInfo pollingAnswerInfo) {

		String sql = " select * from " + DatabaseConnections.zonesApiLinkedServer + ".polling_answer_validation  with(nolock)"
				+ " where id = " + pollingAnswerInfo.getId() + " and cust_id =" + pollingAnswerInfo.getCuId();

		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			// System.out.println("Polling validateUserAnswer Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pollingAnswerInfo.setCrtAnsTxt(rs.getString("correct_ans_text"));
				pollingAnswerInfo.setCorrectAns(rs.getString("correct_ans"));
				pollingAnswerInfo.setPollingType(rs.getString("polling_type"));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return pollingAnswerInfo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void updatePollingRewardsForCustomer() {

	}

	public static PollingRewardsInfo getPollingRewardsForContest(Integer pollingContestId) {
		PollingRewardsInfo pollingRewardsInfo = new PollingRewardsInfo();
		String sql = " select * from " + DatabaseConnections.zonesApiLinkedServer + ".polling_rewards"
				+ " where contest_id = " + pollingContestId;

		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			// System.out.println("Polling Rewards Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pollingRewardsInfo.setContestId(rs.getInt("contest_id"));
				pollingRewardsInfo.setCrstballPerQue(rs.getInt("crstball_per_que"));
				pollingRewardsInfo.setEraserPerQue(rs.getInt("eraser_per_que"));
				pollingRewardsInfo.setGbricksPerQue(rs.getInt("gbricks_per_que"));
				pollingRewardsInfo.setHifivePerQue(rs.getInt("hifive_per_que"));
				pollingRewardsInfo.setLifePerQue(rs.getInt("life_per_que"));
				pollingRewardsInfo.setSupFanStarsPerQue(rs.getInt("stars_per_que"));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			// System.out.println("[polllin rewards] [crystal ball ]" +
			// pollingRewardsInfo.getCrstballPerQue());
			return pollingRewardsInfo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public static int updateCustomerAnswerDetails(PollingAnswerInfo pollingAnswerInfo) {
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".polling_answer_validation set"
				+ " user_ans = '" + pollingAnswerInfo.getAnswer() + "'" + ", is_ans_correct = "
				+ pollingAnswerInfo.getIsAnsCrt() + ", reward_type = '" + pollingAnswerInfo.getRwdType() + "'"
				+ ", reward_qty = " + pollingAnswerInfo.getRwdQty() + " WHERE " + "  id = " + pollingAnswerInfo.getId()
				+ " AND contest_id =  " + pollingAnswerInfo.getpCtId() + " AND cust_id = "
				+ pollingAnswerInfo.getCuId();
		int updateCnt = 0;
		try {
			// System.out.println("[PollingAnswerService][UPDATE ANSWER SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();

			// System.out.println("[PollingAnswerService][UPDATE ANSWER COUNT" + updateCnt);
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}

	public static void processAnswerText(PollingQuestionInfo pollingQuestionInfo) {
		List<String> ansOptList = pollingQuestionInfo.getAnsOpts();
		String answer = pollingQuestionInfo.getAnswer();
		if ("A".equals(answer)) {
			pollingQuestionInfo.setCrtAnsTxt(ansOptList.get(0));
		} else if ("B".contentEquals(answer)) {
			pollingQuestionInfo.setCrtAnsTxt(ansOptList.get(1));
		} else if ("C".contentEquals(answer)) {
			pollingQuestionInfo.setCrtAnsTxt(ansOptList.get(2));
		} else {
			// do Nothing.. can throw Exception here ...
		}

	}

	public static void updateCustomerRewardStats(PollingAnswerInfo pollingAnswerInfo) {

		Integer custId = getCustIdinPollingRewards(pollingAnswerInfo);

		if (custId == null) {
			// System.out.println(" [updateCustomerRewardStats ][customer not found ]");
			insertCustomerPollingRewardStats(pollingAnswerInfo);
		} else {
			// System.out.println(" [updateCustomerRewardStats ][customer found ] - " +
			// custId);
			updateCustomerPollingRewards(pollingAnswerInfo);
		}
	}

	private static int updateCustomerPollingRewards(PollingAnswerInfo pollingAnswerInfo) {
		String columnName = PollingUtil.getRewardStatsColumnForRwdType(pollingAnswerInfo.getRwdType());
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_rewards" + " SET "
				+ columnName + "=" + columnName + " + " + pollingAnswerInfo.getRwdQty() + " WHERE CUST_ID = "
				+ pollingAnswerInfo.getCuId();
		int updateCnt = 0;
		try {
			// System.out.println("[PollingAnswerService][UPDATE CUSTOMER REWARDS SQL]" +
			// sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();

			// System.out.println("[PollingAnswerService][UPDATE ANSWER COUNT" + updateCnt);
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	private static int insertCustomerPollingRewardStats(PollingAnswerInfo pollingAnswerInfo) {
		String columnName = PollingUtil.getRewardStatsColumnForRwdType(pollingAnswerInfo.getRwdType());
		String sql = " INSERT INTO " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_rewards "
				+ " (CUST_ID , " + columnName + ") VALUES (" + pollingAnswerInfo.getCuId() + ", "
				+ pollingAnswerInfo.getRwdQty() + " )";
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			// conn.close();
			// System.out.println("[PollingAnswerService][INSERT CUSTOMER REWARD COUNT" +
			// updateCnt);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;

	}

	public static Integer getCustIdinPollingRewards(PollingAnswerInfo pollingAnswerInfo) {
		String sql = "Select cust_id from " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_rewards with(nolock) "
				+ " where cust_id = " + pollingAnswerInfo.getCuId();
		Integer custId = null;
		try {
			// System.out.println("[PollingAnswerService][UPDATE ANSWER SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				custId = rs.getInt("cust_id");
			}
			rs.close();
			statement.close();
			closeConnection( conn);
			// System.out.println("[PollingAnswerService][UPDATE CUSTOMER REWARDS CUSTOMER
			// ID " + custId);

		} catch (Exception ex) {
			ex.printStackTrace();

		}

		return custId;
	}

	public static Integer fetchCassCustPollingStats(Integer cuId, String rwdType) {
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(cuId);
		if (customer == null)
			return Integer.valueOf(0);
		if (PollingUtil.REWARD_MAGICWAND.equals(rwdType)) {
			return PollingUtil.getZeroIfNull(customer.getMw());
		} else if (PollingUtil.REDEEM_TO_STARS.equals(rwdType)) {
			return PollingUtil.getZeroIfNull(customer.getSfcCount());
		} else if (PollingUtil.REDEEM_TO_LIFE.equals(rwdType)) {
			return PollingUtil.getZeroIfNull(customer.getqLive());
		} else {
			return Integer.valueOf(0);
		}
	}

	public static Integer fetchCassCachedCustPollingStats(CassCustomer customer, String rwdType) {
		// CassCustomer customer =
		// CassandraDAORegistry.getCassCustomerDAO().getCustomerById(cuId);
		if (customer == null)
			return Integer.valueOf(0);
		if (PollingUtil.REWARD_MAGICWAND.equals(rwdType)) {
			return PollingUtil.getZeroIfNull(customer.getMw());
		} else if (PollingUtil.REDEEM_TO_STARS.equals(rwdType)) {
			return PollingUtil.getZeroIfNull(customer.getSfcCount());
		} else if (PollingUtil.REDEEM_TO_LIFE.equals(rwdType)) {
			return PollingUtil.getZeroIfNull(customer.getqLive());
		} else {
			return Integer.valueOf(0);
		}
	}

	public static void cassExecuteCQL(String cql) {
		CassandraConnector.getSession().execute(cql);
	}

	public static void cassUpdateCustomerRewards(PollingAnswerInfo pollingAnswerInfo) {

		Integer currentStat = fetchCassCustPollingStats(pollingAnswerInfo.getCuId(), pollingAnswerInfo.getRwdType());
		Integer gainedRwds = PollingUtil.getZeroIfNull(pollingAnswerInfo.getRwdQty());

		Integer updateToStat = currentStat + gainedRwds;

		String columnName = PollingUtil.getPollingRewardSQLColumn(pollingAnswerInfo.getRwdType());
		if (columnName == null || columnName.equals(""))
			return;
		if (PollingUtil.COLUMN_FUNMSG.equals(columnName))
			return;
		String sql = "UPDATE customer set " + columnName + " = ? where customer_id = ? , " + updateToStat + ", "
				+ pollingAnswerInfo.getCuId();
		// System.out.println("CASSANDRA DB UPDATE SQL : " + sql);
		CassandraConnector.getSession().execute("UPDATE customer set " + columnName + " = ? where customer_id = ? ",
				updateToStat, pollingAnswerInfo.getCuId());

	}

	public static int updatePollingRedeemLife(Integer custId, int lifeCount) {
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".customer"
				+ " SET quiz_cust_lives = quiz_cust_lives " + " + " + lifeCount + " WHERE id = " + custId;
		int updateCnt = 0;
		try {
			// System.out.println("[PollingRedeemService][UPDATE CUSTOMER REWARDS LIFE SQL]"
			// + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();

			// System.out.println("[PollingRedeemService][UPDATE CUSTOMER REWARDS LIFE
			// COUNT" + updateCnt);
			statement.close();
			closeConnection( conn);
			// conn.close();
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static int saveOrUpdatePollingRedeemSuperFanStars(Integer custId, Integer starCount) {
		Integer customerId = null;
		String sql = " SELECT * FROM " + DatabaseConnections.quizApiLinkedServer + ".quiz_super_fan_stat with(nolock) "
				+ "where customer_id = " + custId;

		// System.out.println("[PollingRedeemService][saveOrUpdatePollingRedeemSuperFanStars
		// SQL]" + sql);
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				customerId = rs.getInt("customer_id");
			}
			rs.close();
			statement.close();
			closeConnection( conn);
			// conn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		String superfanStatSQL = "";
		if (customerId == null) {

			superfanStatSQL = " insert into " + DatabaseConnections.quizApiLinkedServer + ".quiz_super_fan_stat"
					+ "(   customer_id" + "    ,game_played " + "      ,total_no_of_chances "
					+ "      ,referral_chances " + "      ,participant_chances " + "      ,created_date, "
					+ " updated_date, status ,polling_reward_redeem " + ") values (" + custId + "" + ", " + 0 + ", "
					+ starCount + ", " + 0 + ", " + 0 + "," + "'" + PollingUtil.getCreatedDateStr() + "'," + "'"
					+ PollingUtil.getCreatedDateStr() + "'," + "'ACTIVE'" + "" + "," + starCount + ")";

			System.out.println("[Redeem Service ] [Super fan Insert SQL] ");
		} else {
			// insert new record .in quiz_super_fan_stat
			superfanStatSQL = " UPDATE " + DatabaseConnections.quizApiLinkedServer + ".quiz_super_fan_stat  " + "SET "
					+ "  total_no_of_chances = isnull(total_no_of_chances,0) + " + starCount
					+ ", polling_reward_redeem = isnull(polling_reward_redeem,0) +  " + starCount + " WHERE CUSTOMER_ID = "
					+ customerId;

			 System.out.println("[Redeem Service ] [Super fan update SQL] " );
		}
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(superfanStatSQL);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			// conn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}

	public static int saveOrUpdatePollingIntroSuperFanStars(Integer custId, Integer starCount) {
		Integer customerId = null;
		String sql = " SELECT * FROM " + DatabaseConnections.quizApiLinkedServer + ".quiz_super_fan_stat with(nolock)"
				+ "where customer_id = " + custId;

		// System.out.println("[PollingIntroVideoService][saveOrUpdatePollingIntroSuperFanStars
		// SQL]" + sql);
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				customerId = rs.getInt("customer_id");
			}
			rs.close();
			statement.close();
			closeConnection( conn);
			// conn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String superfanStatSQL = "";
		if (customerId == null) {

			superfanStatSQL = " insert into " + DatabaseConnections.quizApiLinkedServer + ".quiz_super_fan_stat"
					+ "(   customer_id" + "    ,game_played " + "      ,total_no_of_chances "
					+ "      ,referral_chances " + "      ,participant_chances " + "      ,created_date, "
					+ " updated_date, status ,polling_reward_redeem " + ") values (" + custId + "" + ", " + 0 + ", "
					+ starCount + ", " + 0 + ", " + 0 + "," + "'" + PollingUtil.getCreatedDateStr() + "'," + "'"
					+ PollingUtil.getCreatedDateStr() + "'," + "'ACTIVE'" + "" + "," + starCount + ")";

			// System.out.println("[Intro Video Service ] [Super fan Insert SQL] " +
			// superfanStatSQL);
		} else {
			// insert new record .in quiz_super_fan_stat
			superfanStatSQL = " UPDATE " + DatabaseConnections.quizApiLinkedServer + ".quiz_super_fan_stat  " + "SET "
					+ "  total_no_of_chances =  " + starCount + ", polling_reward_redeem = " + starCount
					+ " WHERE CUSTOMER_ID = " + customerId;

			// System.out.println("[Intro Video Service ] [Super fan update SQL] " +
			// superfanStatSQL);
		}
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(superfanStatSQL);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			// conn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}

	public static PollingCustomerRewards fetchCustomerPollingRewards(Integer cuId) {
		String sql = " select * from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_customer_rewards with(nolock)  where cust_id = " + cuId;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			// System.out.println("Polling QuestionBank Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			PollingCustomerRewards pollingCustomerRewards = new PollingCustomerRewards();
			while (rs.next()) {
				pollingCustomerRewards.setCustId(rs.getInt("cust_id"));
				pollingCustomerRewards.setCrystalBall(rs.getInt("crystalball"));
				pollingCustomerRewards.setGoldBar(rs.getInt("goldbar"));
				pollingCustomerRewards.setHiFive(rs.getInt("hifive"));
				pollingCustomerRewards.setMagicWand(rs.getInt("magicwand"));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return pollingCustomerRewards;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public static int updateCustomerAccountForRedeemedFromQty(Integer cuId, String fromType, Integer fromQty) {
		String columnName = PollingUtil.getPollingRewardSQLColumn(fromType);

		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_rewards " + " SET  "
				+ columnName + "= " + columnName + "- " + fromQty + "  where cust_id = " + cuId;

		int updateCnt = 0;
		try {
			// System.out.println("[PollingRedeemService][DEDUCT FROM CUSTOMER REWARD STAT]"
			// + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			// conn.close();
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;

	}

	public static PollingVideoInventory getVideoUrlForCustomer(Integer cuId) {

		String sql = " SELECT top 1 id , video_url FROM " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_inventory with(nolock) " + " WHERE  id not in ( "
				+ " SELECT  IIF( vid_id IS NULL , 0 , vid_id )    FROM " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_played  with(nolock) " + " WHERE CUST_ID =  " + cuId + " AND "
				+ " convert(varchar(10), play_date, 102) " + " = convert(varchar(10), getdate(), 102)  ) ";

		PollingVideoInventory pollingVideoInventory = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Video Url Inventory Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pollingVideoInventory = new PollingVideoInventory();
				pollingVideoInventory.setId(rs.getInt("id"));
				pollingVideoInventory.setVideoUrl(rs.getString("video_url"));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return pollingVideoInventory;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pollingVideoInventory;

	}

	public static List<String> getMessageListForCustomerRewards() {

		String sql = " SELECT message FROM " + DatabaseConnections.zonesApiLinkedServer + ".polling_rwd_messages";

		List<String> msgList = new ArrayList<String>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Reward msg List Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				msgList.add(rs.getString("message"));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return msgList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msgList;

	}

	public static Map<Integer, String> getVideoUrlListFromInventory(String catId,Integer cuId) {

		// System.out.println("**** catId********" + catId);
		String sql = " SELECT id , video_url FROM " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_inventory with(nolock) " + "where status = 1  "
				+ "and is_deleted=0  and  category_id =  " + catId
				+ " order by created_date desc " ;
				

		if ("DEF".equalsIgnoreCase(catId) || StringUtils.isEmpty(catId)) {

			sql = " SELECT id , video_url FROM " + DatabaseConnections.zonesApiLinkedServer
					+ ".polling_video_inventory with(nolock) " + "where status = 1  "
					+ "and is_deleted=0  and isnull(is_default,0) = 1 "				
					+ " order by created_date desc " ;
		}

		Map<Integer, String> vUrlMap = new LinkedHashMap<Integer, String>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Video Url Inventory Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				vUrlMap.put(rs.getInt("id"), rs.getString("video_url"));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return vUrlMap;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vUrlMap;

	}

	public static int updatePollingVideoPlayedStats(Integer cuId, Integer vidId) {
		String sql = " INSERT INTO  " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_played"
				+ " (vid_id , cust_id , play_date )  VALUES " + " ( " + vidId + " ," + cuId + " ,'"
				+ PollingUtil.getCreatedDateStr() + "')";

		int updateCnt = 0;

		try {
			// //System.out.println("[PollingVideoStats Update][]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			// //System.out.println("[PollingVideoStats][Updated no of records ]" +
			// updateCnt);
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static List<Integer> getPollingVideoPlayedListByCustomer(Integer cuId) {
		String sql = " select vid_id from   " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_played with(nolock) "
				+ " where  cust_id =" + cuId;
		List<Integer> vUrlList = new ArrayList<Integer>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Video Url Inventory Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				vUrlList.add(rs.getInt("vid_id"));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return vUrlList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vUrlList;
	}

	public static void deletePollingVideoPlayedListByCustomer(Integer cuId) {
		String sql = " DELETE from   " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_played"
				+ " where  cust_id =" + cuId;

		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Video UrlPlay List Delete Query : " + sql);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
			closeConnection( conn);

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void insertPollingVideoPlayedStats(Integer cuId, List<Integer> vidIdList) {
		String sql = " INSERT INTO  " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_played"
				+ " (vid_id , cust_id , play_date )  VALUES (?, ?, ?)";

		try {

			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			for (Integer id : vidIdList) {
				statement.setInt(1, id);
				statement.setInt(2, cuId);
				statement.setString(3, PollingUtil.getCreatedDateStr());
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void insertPollingVideoPlayedHistory(Integer cuId, List<Integer> vidIdList) {

		String sql = " INSERT INTO  " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_played_hist"
				+ " (vid_id , cust_id , play_date )  VALUES (?, ?, ?)";

		try {

			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			for (Integer id : vidIdList) {
				statement.setInt(1, id);
				statement.setInt(2, cuId);
				statement.setString(3, PollingUtil.getCreatedDateStr());
				statement.addBatch();

			}
			statement.executeBatch();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static List<String> getNextContestDetails() {
		String sql = "select TOP 1 contest_name, contest_start_datetime" + " from "
				+ DatabaseConnections.quizApiLinkedServer + ".contest co  with(nolock)"
				+ " where  co.contest_start_datetime between DATEADD(day,0,datediff(day,0,GETDATE())) and DATEADD(day,7,datediff(day,0,GETDATE())) "
				+ " and co.contest_name not like '%TEST%' " + " and status ='ACTIVE'"
				+ " AND contest_category  != 'MARKET' " + " order by contest_start_datetime";
		List<String> cntList = new ArrayList<String>();

		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			// System.out.println("Nexrt Contest Details Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				cntList.add(rs.getString("contest_name"));
				cntList.add(rs.getString("contest_start_datetime"));

			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return cntList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cntList;

	}

	public static Integer isFirstQuestion(PollingQuestionInfo pollingQuestionInfo) {
		String sql = " Select count(*) as cnt  from  " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_answer_validation with(nolock) " + " where cust_id = " + pollingQuestionInfo.getCustId()
				+ " and contest_id = " + pollingQuestionInfo.getpCtId()
				+ " and convert(varchar(10), created_date, 102) " + " = convert(varchar(10), getdate(), 102) ";
		// + " and category_id = " + pollingQuestionInfo.getpCatId() ;
		Integer cnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Polling Sponsor QuestionBank Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return cnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cnt;

	}

	public static Integer getQuestioNumOftheContestForCustomer(PollingAnswerInfo pollingAnswerInfo) {
		String sql = " Select count(*) as cnt  from  " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_answer_validation with(nolock)" + " where cust_id = " + pollingAnswerInfo.getCuId()
				+ " and contest_id = " + pollingAnswerInfo.getpCtId() + " and convert(varchar(10), created_date, 102) "
				+ " = convert(varchar(10), getdate(), 102) ";

		Integer cnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Polling Answer : getQuestioNumOftheContestForCustomer
			// Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return cnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cnt;
	}

	public static Integer updateRandomMessageRewardsQueNums(PollingQuestionInfo pollingQuestionInfo,
			String msgQueNumStr) {

		int updateCnt = 0;

		String sql = " INSERT INTO " + DatabaseConnections.zonesApiLinkedServer + ".polling_cust_rand_fmsg_rwds"
				+ " ( cust_id , polling_contest_id , polling_reference_date ,random_quenum_ids )" + "VALUES ("
				+ pollingQuestionInfo.getCustId() + " , " + pollingQuestionInfo.getpCtId() + " , " + " '"
				+ PollingUtil.getCreatedDateStr() + "' , " + " '" + msgQueNumStr + "' ) ";

		try {
			// System.out.println("[PollingQuestionService][Update ransdom msg numbers]" +
			// sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}

	public static String fetchFunMessageQuestioNums(PollingAnswerInfo pollingAnswerInfo) {

		String sql = " select random_quenum_ids from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_cust_rand_fmsg_rwds  with(nolock)" + " where cust_id =  " + pollingAnswerInfo.getCuId()
				+ " and polling_contest_id =  " + pollingAnswerInfo.getpCtId() + " and id in(select max(id) from "
				+ DatabaseConnections.zonesApiLinkedServer + ".polling_cust_rand_fmsg_rwds" + " where cust_id =  "
				+ pollingAnswerInfo.getCuId() + " and polling_contest_id =  " + pollingAnswerInfo.getpCtId()
				+ " group by cust_id, polling_contest_id) ";

		String randQidNums = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("PollingAnswer Service _ polling_cust_rand_fmsg_rwds_
			// Query : " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				randQidNums = rs.getString("random_quenum_ids");
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return randQidNums;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return randQidNums;
	}

	public static List<Boolean> customerCanUploadVideo(Integer customerId) {
		String winnerOrderSql = " select distinct customer_id as CustId from dbo.contest_grand_winners with(nolock) where  "
				+ "customer_id = " + customerId + " and created_date > DATEADD(day, -1, GETDATE()) ";
		String videoUploadSql = " select cust_id as CustId, 'true' as isThere from " + ""
				+ DatabaseConnections.zonesApiLinkedServer
				+ ".polling_customer_video_uploads with(nolock) where cust_id = " + customerId + "  "
				+ "and is_winner_or_general = 1 and upload_date > DATEADD(day, -1, GETDATE())";
		List<Boolean> eligibilityList = new ArrayList<Boolean>();
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(winnerOrderSql);
			Boolean canUploadVideo = false;
			Boolean hasUploadedVideo = false;
			while (rs.next()) {
				canUploadVideo = true;
			}
			if (canUploadVideo) {
				stmt = conn.createStatement();
				rs = stmt.executeQuery(videoUploadSql);
				while (rs.next()) {
					// canUploadVideo = false;
					hasUploadedVideo = true;
				}
			}
			eligibilityList.add(canUploadVideo);
			eligibilityList.add(hasUploadedVideo);
			rs.close();
			stmt.close();
			closeConnection( conn);
			return eligibilityList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eligibilityList;
	}

	public static Integer insertIntroVideoMWReward(Integer custId, String rwdType) {
		String columnName = PollingUtil.getRewardStatsColumnForRwdType(rwdType);
		String sql = " INSERT INTO " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_rewards "
				+ " (CUST_ID , " + columnName + ") VALUES (" + custId + ", " + 1 + " )";
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			// System.out.println("[Polling Intro video ][INSERT CUSTOMER MAGIC WAND COUNT"
			// + updateCnt);

			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;

	}

	public static int updateIntroVideoLife(Integer custId, int lifeCount) {
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".customer" + " SET quiz_cust_lives = "
				+ lifeCount + " WHERE id = " + custId;
		int updateCnt = 0;
		try {
			// System.out.println("[updateIntroVideoLife][UPDATE updateIntroVideoLife
			// CUSTOMER REWARDS LIFE SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static int insertIntroVideoSuperFanStars(Integer custId, Integer starCount) {

		String superfanStatSQL = " insert into " + DatabaseConnections.quizApiLinkedServer + ".quiz_super_fan_stat"
				+ "(   customer_id" + "    ,game_played " + "      ,total_no_of_chances " + "      ,referral_chances "
				+ "      ,participant_chances " + "      ,created_date, " + " updated_date, status  " + ") values ("
				+ custId + "" + ", " + 0 + ", " + starCount + ", " + 0 + ", " + 0 + "," + "'"
				+ PollingUtil.getCreatedDateStr() + "'," + "'" + PollingUtil.getCreatedDateStr() + "'," + "'ACTIVE'"
				+ "" + ")";

		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(superfanStatSQL);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}

	public static void updateCassCustomerStatsForIntroVideo(Integer custId) throws Exception {
		String cassSQL = " update customer set magicwand = 1 , " + "super_fan_chances = 1 , " + " no_of_lives = 1"
				+ "  where  customer_id = " + custId;
		CassandraConnector.getSession().execute(cassSQL);

	}

	public static int insertCustomerMediaUploadStats(Integer custId, Integer isWinnerOrGeneralTy,
			String mediaFileName,String posterUrl,String catId, String desc, String title) {

		String mediaUploadSQL = " insert into " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_customer_video_uploads" + "(   cust_id" + "    ,video_url " + " ,is_winner_or_general "
				+ "      ,upload_date " + "      ,status, poster_url,cat_id,title,description " + ") values (" + custId + "" + ", '" + mediaFileName + "', "
				+ isWinnerOrGeneralTy + ", " + "'" + PollingUtil.getCreatedDateStr() + "'," + "'ACTIVE',"
						+ " '" + posterUrl  + "', " + catId  + " ,N'" + desc + "',N'" + title + "')";
		 System.out.println(" - mediaUploadSQL - " + mediaUploadSQL);
		Integer genKey = 0;
		try {
			
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(mediaUploadSQL,Statement.RETURN_GENERATED_KEYS);
			statement.executeUpdate();
			
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					genKey = 	generatedKeys.getInt(1);					
				} else {
					throw new SQLException("Creating uploaded video record failed");
				}
				generatedKeys.close();
			} catch (Exception ex) {				
				ex.printStackTrace();
			}
			
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return genKey;

	}

	public static Integer validateMediaUploadEligibilityQuota(Integer customerId) {
		String uploadEligibilitySql = " select count(*) AS upldCnt from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_customer_video_uploads" + "	where 	cust_id =  " + customerId
				+ " and is_winner_or_general  = 0 " + " and convert(varchar(10), upload_date, 102)"
				+ " = convert(varchar(10), getdate(), 102)" + " and status = 'ACTIVE'";
		// System.out.println("GENERAL UPLOAD SQL " + uploadEligibilitySql);
		Integer todaysCount = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(uploadEligibilitySql);

			while (rs.next()) {
				todaysCount = rs.getInt("upldCnt");
			}

			rs.close();
			stmt.close();
			closeConnection( conn);
			return todaysCount;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return todaysCount;
	}

	private static void testquestion() throws Exception {

		PollingQuestionInfo pollingQuestionInfo = PollingQuestionService.processQuestionForCustomer(351111);

		// System.out.println(" Status - " + pollingQuestionInfo.getSts());
		// System.out.println(" message - " + pollingQuestionInfo.getMsg());
		// System.out.println("Next Contest Details " +
		// pollingQuestionInfo.getNxtCTxt());
		// System.out.println("pcatId " + pollingQuestionInfo.getpCatId());
		// System.out.println("pollingType " + pollingQuestionInfo.getPollType());
		// System.out.println("polling question " + pollingQuestionInfo.getqTxt());
	}

	public static void testAnswer() throws Exception {

		PollingAnswerInfo pollingAnswerInfo = new PollingAnswerInfo();
		pollingAnswerInfo.setCuId(351111);
		pollingAnswerInfo.setpCtId(1002);
		pollingAnswerInfo.setId(1);
		pollingAnswerInfo.setAnswer("1");
		pollingAnswerInfo = PollingAnswerService.processValidateAnswerResponse(pollingAnswerInfo);
		// System.out.println("Finished answer" + pollingAnswerInfo.getShowMsg());
	}

	public static String getCustomerUserId(Integer custId) {
		String customerSQL = " select USER_ID from " + DatabaseConnections.zonesApiLinkedServer + ".customer"
				+ "	where 	cust_id =  " + custId;

		// System.out.println("GENERAL UPLOAD SQL customer user id " + customerSQL);
		String userId = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(customerSQL);

			while (rs.next()) {
				userId = rs.getString("USER_ID");
			}

			rs.close();
			stmt.close();
			closeConnection( conn);
			return userId;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userId;

	}

	public static Integer updateVideoLikeStats(Integer videoId, Integer custId) {
		String mediaUploadSQL = " insert into " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_cust_video_like_stats" + "(   cust_id  ,vid_id  ,like_date, like_status ) "
					+ "VALUES ( "+custId+", "+videoId+", '"+ PollingUtil.getCreatedDateStr()+"', 1)";
		// System.out.println(" - updateVideoLikeStats - " + mediaUploadSQL);
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(mediaUploadSQL);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}
	
	public static Integer updateVideoDisLikeStats(Integer videoId, Integer custId) {
		String mediaUploadSQL = " update " + DatabaseConnections.zonesApiLinkedServer+".polling_cust_video_like_stats set like_status =0 "
				+ "where cust_id="+custId+" and vid_id="+videoId;
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(mediaUploadSQL);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}

	public static List<String> getPollingVideoCategoryList() {

		String sql = " select ID , icon_url  from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_categories  with(nolock) where id in (" + " SELECT category_id FROM "
				+ DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory  with(nolock)  "
				+ " where status = 1  and isnull(is_deleted,0)=0 " + "	)  order by sequence_num ";

		 System.out.println("getPollingVideoCategoryList " + sql);
		List<String> catList = new ArrayList<String>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				catList.add(String.valueOf(rs.getString("ID") + "," + rs.getString("icon_url")  ));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return catList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return catList;
	}
	
	public static List<Category> getAllCategories() {
		String sql = " select id,category_name,description,image_url from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_categories with(nolock) order by sequence_num ";
 
		List<Category> catList = new ArrayList<Category>();
		Category categoryObj = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				categoryObj = new Category();
				categoryObj.setDescription(rs.getString("description"));;
				categoryObj.setId(rs.getInt("id"));
				categoryObj.setImageUrl(rs.getString("image_url"));
				categoryObj.setName(rs.getString("category_name"));
				catList.add(categoryObj);
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return catList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return catList;
	}
	
	public static List<String> getVideoCategoryList() {

		String sql = " select ID , icon_url , category_name from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_categories with(nolock) where id in (" + " SELECT category_id FROM "
				+ DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory with(nolock) "
				+ " where status = 1  and isnull(is_deleted,0)=0 " + "	)  order by sequence_num ";

		// System.out.println("getPollingVideoCategoryList " + sql);
		List<String> catList = new ArrayList<String>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				catList.add(String.valueOf(rs.getString("ID") + "," + rs.getString("icon_url") + "," + rs.getString("category_name") ));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return catList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return catList;
	}
	

	public static Map<Integer, Integer> getVideoViewCount(List<Integer> vidIdList) {

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < vidIdList.size(); i++) {
			builder.append("?,");
		}
		String sql = " select vid_id ,  count(*) as viewCount from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_played_hist with(nolock) " + "  where vid_id  in ("
				+ builder.deleteCharAt(builder.length() - 1).toString() + ")" + " group by vid_id ";
		// System.out.println("getVideoViewCount " + sql);
		Map<Integer, Integer> videoMapCountMap = new HashMap<Integer, Integer>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			int index = 1;
			for (Integer o : vidIdList) {
				pstmt.setObject(index++, o);
			}
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				videoMapCountMap.put(Integer.valueOf(rs.getInt("vid_id")), Integer.valueOf(rs.getInt("viewCount")));

			}
			rs.close();
			pstmt.close();
			closeConnection( conn);
			return videoMapCountMap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return videoMapCountMap;
	}

	public static List<Integer> getVideosLikedByCust(List<Integer> vidIdList, Integer cuId) {

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < vidIdList.size(); i++) {
			builder.append("?,");
		}
		String sql = " select vid_id   from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_cust_video_like_stats with(nolock) " + "  where like_status =1 and vid_id  in ("
				+ builder.deleteCharAt(builder.length() - 1).toString() + ")" + " and cust_id =   " + cuId;
		System.out.println("getVideosLikedByCust " + sql);
		List<Integer> videoList = new ArrayList<Integer>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			int index = 1;
			for (Integer o : vidIdList) {
				pstmt.setObject(index++, o);
			}
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				videoList.add(Integer.valueOf(rs.getInt("vid_id")));

			}
			System.out.println(" Liked id list " + videoList);
			rs.close();
			pstmt.close();
			closeConnection( conn);
			return videoList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return videoList;

	}

	public static int insertPollingVideoPlayed(Integer cuId, Integer id) {
		String sql = " INSERT INTO  " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_played"
				+ " (vid_id , cust_id , play_date )  VALUES (" + id + " , " + cuId + ", '"
				+ PollingUtil.getCreatedDateStr() + "' )";

		int affectedRows = 0;
		try {

			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return affectedRows;
	}

	public static int insertPollingVideoPlayedHist(Integer cuId, Integer id) {

		String sql = " INSERT INTO  " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_played_hist"
				+ " (vid_id , cust_id , play_date )  VALUES (" + id + " , " + cuId + ", '"
				+ PollingUtil.getCreatedDateStr() + "' )";
		int affectedRows = 0;
		try {

			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return affectedRows;
	}

	public static Map<Integer, Integer> getVideoLikedCount(List<Integer> vidIdList, Integer cuId) {

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < vidIdList.size(); i++) {
			builder.append("?,");
		}
		String sql = " select vid_id ,  count(*) as likeCount from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_cust_video_like_stats with(nolock)  " + "  where  like_status =1 and vid_id  in ("
				+ builder.deleteCharAt(builder.length() - 1).toString() + ")" + " group by vid_id ";
		// System.out.println("New getVideoLikedCount " + sql);
		Map<Integer, Integer> videoMapCountMap = new HashMap<Integer, Integer>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			int index = 1;
			for (Integer o : vidIdList) {
				pstmt.setObject(index++, o);
			}
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				videoMapCountMap.put(Integer.valueOf(rs.getInt("vid_id")), Integer.valueOf(rs.getInt("likeCount")));

			}
			rs.close();
			pstmt.close();
			closeConnection( conn);
			return videoMapCountMap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return videoMapCountMap;
	}

	public static Map<Integer, String> getVideoSponsorLogos(List<Integer> vidIdList) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < vidIdList.size(); i++) {
			builder.append("?,");
		}
		String sql = " select a.id as id, b.logo_path as logo from  " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_inventory a with(nolock) "
				+ " left join polling_sponsors b on a.sponsor_id = b.id  where a.id in ( "
				+ builder.deleteCharAt(builder.length() - 1).toString() + ")";

		// System.out.println("getVideoSponsor LOGO " + sql);
		Map<Integer, String> videoSponsorLogoMap = new HashMap<Integer, String>();
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			int index = 1;
			for (Integer o : vidIdList) {
				pstmt.setObject(index++, o);
			}
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				videoSponsorLogoMap.put(Integer.valueOf(rs.getInt("id")), rs.getString("logo"));
			}
			rs.close();
			pstmt.close();
			closeConnection( conn);
			return videoSponsorLogoMap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return videoSponsorLogoMap;

	}

	public static String getPollingSponsorLogo(Integer catId) {

		String sql = " select a.id, a.logo_path as logopath from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_sponsors a  with(nolock)" + " where a.id = ( select sponsor_id from "
				+ DatabaseConnections.zonesApiLinkedServer + ".polling_category with(nolock) where id = " + catId + ")  ";

		// System.out.println("getPollingSponsorLogo " + sql);
		String pollingSponsorLogo = PollingUtil.BLANK_STRING;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				pollingSponsorLogo = rs.getString("logopath");
			}
			rs.close();
			pstmt.close();
			closeConnection( conn);
			return pollingSponsorLogo;
		} catch (Exception e) {
			e.printStackTrace();
			pollingSponsorLogo = PollingUtil.BLANK_STRING;
		}
		return pollingSponsorLogo;
	}

	public static Integer getPollingInterval(Integer getpCtId) {
		PollingContest pollingContest = getActivePollingContest();
		return pollingContest.getPollingInterval();
	}

	public static PollingVideoRewardsConfig getVideoRewardsConfig() {

		String sql = " SELECT id" + " ,min_vid_play_time " + "  ,min_reward_interval" + "   ,max_rewards_per_day"
				+ " ,max_lives" + " ,max_sfstars" + " ,max_magicwand" + "      ,updated_date" + " ,update_by"
				+ " ,is_reward_enabled   FROM  " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_video_reward_config";
		// System.out.println("getVideoRewardsConfig " + sql);
		PollingVideoRewardsConfig pollingVideoRewardsConfig = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				pollingVideoRewardsConfig = new PollingVideoRewardsConfig();
				pollingVideoRewardsConfig.setId(rs.getInt("id"));
				pollingVideoRewardsConfig.setMinSecs(rs.getInt("min_vid_play_time"));
				pollingVideoRewardsConfig.setMinTimeBetnRewards(rs.getInt("min_reward_interval"));
				pollingVideoRewardsConfig.setMaxRwdsPerDay(rs.getInt("max_rewards_per_day"));
				pollingVideoRewardsConfig.setMaxLives(rs.getInt("max_lives"));
				pollingVideoRewardsConfig.setMaxMagicWand(rs.getInt("max_magicwand"));
				pollingVideoRewardsConfig.setMaxStars(rs.getInt("max_sfstars"));
				pollingVideoRewardsConfig.setIsRewardsEnabled(rs.getBoolean("is_reward_enabled"));
			}
			rs.close();
			pstmt.close();
			closeConnection( conn);
			return pollingVideoRewardsConfig;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return pollingVideoRewardsConfig;

	}

	public static Integer getVideoRewardCountForTheDay(Integer cuId) {
		String sql = "  SELECT   count(*) as count   " + " FROM " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_customer_video_reward_stat  " + " where cust_id  =  " + cuId
				+ " and  convert(varchar(10), reward_date, 102) " + " = convert(varchar(10), getdate(), 102) ";
		// System.out.println("getVideoRewardCountForTheDay " + sql);
		Integer rwdCount = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				rwdCount = rs.getInt("count");
			}
			rs.close();
			pstmt.close();
			closeConnection( conn);
			return rwdCount;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rwdCount;
	}

	public static Integer getVideoRewardCountForTheDay(Integer cuId, String awsVideoId) {
		String sql = "  SELECT   count(*) as count   " + " FROM " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_customer_video_reward_stat  " + " where cust_id  =  " + cuId + " and video_aws_id = '"
				+ awsVideoId.trim() + "'";
		// System.out.println("getVideoRewardCountForTheDay " + sql);
		Integer rwdCount = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				rwdCount = rs.getInt("count");
			}
			rs.close();
			pstmt.close();
			closeConnection( conn);
			return rwdCount;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rwdCount;
	}

	public static Integer insertCustomerVideoRewardStats(String rewardCode, Integer rwdQty, Integer custId,
			String clientIp, String videoId) {
		String sql = " INSERT INTO " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_video_reward_stat "
				+ " (CUST_ID , reward_type,reward_count,reward_date,client_ip,video_aws_id)" + " VALUES (" + custId
				+ ", '" + rewardCode + "'," + rwdQty + " , " + "'" + PollingUtil.getCreatedDateStr() + "'  , " + "'"
				+ clientIp + "', '" + videoId + "' )";
		// System.out.println("insertCustomerVideoRewardStats " + sql ) ;
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;

	}

	public static Integer getCustIdinPollingRewardStats(Integer cuId) {
		String sql = "Select cust_id from " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_rewards"
				+ " where cust_id = " + cuId;
		Integer custId = null;
		try {
			// System.out.println("[getCustIdinPollingRewardStats][]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				custId = rs.getInt("cust_id");
			}
			rs.close();
			statement.close();
			closeConnection( conn);
		} catch (Exception ex) {
			ex.printStackTrace();

		}

		return custId;
		
	}

	private static int insertCustomerVideoMagicWandStats(Integer cuId, Integer rqdQty) {

		String sql = " INSERT INTO " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_rewards "
				+ " (CUST_ID , magicwand) VALUES (" + cuId + ", " + rqdQty + " )";
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;

	}

	private static int updateCustomerVideoMagicWandStats(Integer cuId, Integer rwdQty) {

		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_rewards" + " SET "
				+ "magicwand = magicwand + " + rwdQty + " WHERE CUST_ID = " + cuId;
		int updateCnt = 0;
		try {
			// System.out.println("[updateCustomerVideoMagicWandStats SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static void saveUpdateCustMagicWandRewardStats(Integer cuId, Integer rwdQty) {
		Integer custId = getCustIdinPollingRewardStats(cuId);

		if (custId == null) {
			// System.out.println(" [InsertCustMagicWandRewardStats ][customer not found
			// ]");
			insertCustomerVideoMagicWandStats(cuId, rwdQty);
		} else {
			// System.out.println(" [saveUpdateCustMagicWandRewardStats ][customer found ] -
			// " + custId);
			updateCustomerVideoMagicWandStats(cuId, rwdQty);
		}

	}

	public static void updateCassCustomerVideoRewards(String rwdType, Integer rwdQty, Integer cuId) {
		Integer currentStat = fetchCassCustPollingStats(cuId, rwdType);
		Integer gainedRwds = PollingUtil.getZeroIfNull(rwdQty);
		Integer updateToStat = currentStat + gainedRwds;
		String columnName = PollingUtil.getPollingRewardSQLColumn(rwdType);
		if (columnName == null || columnName.equals(""))
			return;
		if (PollingUtil.COLUMN_FUNMSG.equals(columnName))
			return;
		String sql = "UPDATE customer set " + columnName + " = ? where customer_id = ? , " + updateToStat + ", " + cuId;
		// System.out.println("CASSANDRA DB UPDATE SQL : " + sql);
		CassandraConnector.getSession().execute("UPDATE customer set " + columnName + " = ? where customer_id = ? ",
				updateToStat, cuId);

	}

	

	public static Integer insertCustomerVideoRewardStats(String rewardCode, Integer rwdQty, Integer custId,
			String clientIp) {
		String sql = " INSERT INTO " + DatabaseConnections.zonesApiLinkedServer + ".polling_customer_video_reward_stat "
				+ " (CUST_ID , reward_type,reward_count,reward_date,client_ip)" + " VALUES (" + custId + ", '"
				+ rewardCode + "'," + rwdQty + " , " + "'" + PollingUtil.getCreatedDateStr() + "'  , " + "'" + clientIp
				+ "' )";
		// System.out.println("insertCustomerVideoRewardStats " + sql ) ;
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;

	}

	public static boolean validateIfPollQWonRwardsEarlier(PollingAnswerInfo pollingAnswerInfo) {

		String sql = "  select cust_id from " + DatabaseConnections.zonesApiLinkedServer
				+ ". polling_answer_validation with(nolock) " + "  where cust_id =   " + pollingAnswerInfo.getCuId()
				+ "  and question_id =   " + pollingAnswerInfo.getoQId() + "  and reward_qty > 0 ";
		boolean hasRecords = false;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				hasRecords = true;
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return hasRecords;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasRecords;
	}

	public static int insertVideoFavStats(Integer videoId, Integer custId, String isFavAdd) {

		String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer + ".polling_customer_video_favorites "
				+ " (CUST_ID , vid_id , fav_status , fav_date) VALUES (" + custId + ", " + videoId + ", " + isFavAdd
				+ " , '" + PollingUtil.getCreatedDateStr() + "'  )";
		// System.out.println("FAV VIDEO INSERT " + sql);
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;

	}

	public static int updateVideoFavStats(Integer videoId, Integer custId, String isFavAdd) {

		String sql = " UPDATE " + DatabaseConnections.quizApiLinkedServer + ".polling_customer_video_favorites "
				+ " SET fav_status = " + isFavAdd + " , fav_date = '" + PollingUtil.getCreatedDateStr() + "'"
				+ " WHERE CUST_ID =  " + custId + " AND vid_id = " + videoId;

		// System.out.println("FAV VIDEO UPDATE " + sql);
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;

	}

	public static PollingVideoInfo fetchCustomerUploadedVideo(Integer custId) {
		
		String sql = 
				  " select a.id as id , a.video_url as url, a.title as title,"
				  + " a.created_date as createddate,"
				  + " a.sponsor_id ,a.poster_url, a.video_description, "
				  + " a.category_id , a.view_cnt, a.like_cnt "  
				+ " from  " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory a with(nolock)"
				+ " where a.created_by = '" + custId  + "'"
		+ " and isnull(a.is_deleted,0) = 0  "
		+ " and isnull(a.hide_media,0) !=1 and isnull(a.block_status,0) !=1 " ;
		
		
		
		/*
		 * String sql =
		 * " select video_url , is_winner_or_general, upload_date,poster_url , cat_id, title , description  from "
		 * + DatabaseConnections.zonesApiLinkedServer +
		 * ".polling_customer_video_uploads with(nolock) " + " where cust_id = " +
		 * custId;
		 */
		PollingVideoInfo pollvidInfo = null;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			// System.out.println("Check for Polling Question Earned Rewards earlier Query :
			// " + sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			pollvidInfo = new PollingVideoInfo();
			List<MyVideoInfo> vfileNameList = new ArrayList<MyVideoInfo>();
			while (rs.next()) {
				MyVideoInfo vidInfo = new MyVideoInfo();
				//Boolean isWinner = rs.getBoolean("is_winner_or_general");
				//String filePath = (isWinner ? "winneruploads/" : "generaluploads/")	+ rs.getString("video_url");
				String filePath = rs.getString("url");
				//String winnerTxt = (isWinner ? PollingUtil.WINNER_UPLOAD_TEXT :  PollingUtil.GENERAL_UPLOAD_TEXT);

			//	if (filePath.endsWith(".mp4") || filePath.endsWith(".3gp")) {
					/*vidInfo.set
					vidInfo.setAwsfname(filePath);
					vidInfo.setvImg(rs.getString("poster_url"));
					vidInfo.setUpdDte(String.valueOf(rs.getDate("upload_date")));
					vidInfo.setIsWinr(winnerTxt);
					vidInfo.setvTitle(rs.getString("title"));
					vidInfo.setDesc(rs.getString("description"));
					vidInfo.setCatId(rs.getInt("cat_id"));*/					
					
					String urlStr = rs.getString("poster_url");
					Integer id = rs.getInt("id");
					vidInfo.setvId(id);
					vidInfo.setvUrl(filePath);
					vidInfo.setvImg(urlStr);
					vidInfo.setPostrImg(urlStr);
					//vidInfo.setSpLogoUrl(rs.getString("path"));
					vidInfo.setvTitle(rs.getString("title"));
					vidInfo.setUpdDte(String.valueOf(rs.getDate("createddate")));
					vidInfo.setDesc(rs.getString("video_description"));
					vidInfo.setCatId(rs.getInt("category_id"));
					vidInfo.setLkCnt(rs.getInt("like_cnt"));
					vidInfo.setVwCnt(rs.getInt("view_cnt"));
					String vidStr = vidInfo.getRtfUpdVideoStr() ;
					vidInfo.setRtfUpdVideoStr(vidStr);
					System.out.println("MY MEDIA " + vidInfo.getRtfTvMediaDets());
					vfileNameList.add(vidInfo);	
					
				//}
			}
			pollvidInfo.setMyVidInfoList(vfileNameList);
			System.out.println(vfileNameList);
			rs.close();
			stmt.close();
			closeConnection( conn);
			pollvidInfo.setSts(1);
			return pollvidInfo;
		} catch (SQLException se) {
			pollvidInfo.setSts(0);
			se.printStackTrace();
		} catch (Exception e) {
			pollvidInfo.setSts(0);
			e.printStackTrace();
		}
		return pollvidInfo;
	}

	public static Integer insertMediaSharedStats(VideoLikeInfo videoInfo) {
		String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer + ".rtf_cust_media_shared_details "
				+ " (CUST_ID , video_id , shared_to , shared_platform, shared_date) " + " VALUES ("
				+ videoInfo.getCuId() + ", " + videoInfo.getmId() + ", '" + videoInfo.getsTo() + "' , '"
				+ videoInfo.getSmPfm() + "' , '" + PollingUtil.getCreatedDateStr() + "'  )";
		// System.out.println("Media shared INSERT " + sql);
		int updateCnt = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
	}

	public static Integer getMediaSharedDetails(VideoLikeInfo videoInfo) {
		String sql = " select count( CUST_ID ) as cnt  from " + DatabaseConnections.quizApiLinkedServer
				+ ".rtf_cust_media_shared_details with(nolock) "
				+ " where cust_id = " + videoInfo.getCuId() + " AND video_id = "
				+ videoInfo.getmId() + ""
				+ " AND shared_to  = '" + videoInfo.getsTo() + "'" + " "
				+ "AND shared_platform = '"
				+ videoInfo.getSmPfm() + "'";
		Integer cnt = 0;
		// System.out.println("Media shared count sql " + sql);
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return cnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cnt;
	}

	public static void updateCustomerRTFPointsStats(PollingAnswerInfo pollingAnswerInfo) {

		SQLDaoUtil.rewardCustomerWithRTFPoints(pollingAnswerInfo.getCuId(), pollingAnswerInfo.getRwdQty());

	}

	public static void main(String a[]) throws Exception {
		// List<Integer> l = new ArrayList<Integer>();
		// l.add(1);
		// l.add(10);
		// l.add(15);
		/// //System.out.println(getVideoViewCount(l));

		fetchCustomerUploadedVideo(351111);

		// testAnswer();

		// cassUpdateCustomerRewards(pollingAnswerInfo);

		// List<String> l = getNextContestDetails();
		// //System.out.println(l.get(0));
		// //System.out.println(l.get(1));

	}

	public static Boolean isVideoLikedEarlier(Integer videoId, Integer custId) {
		String isVideoLikedEarlierSQL = " select cust_id  from " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_cust_video_like_stats where cust_id = " + custId + "  and vid_id =  " + videoId;
		Boolean hasRecords = false;
		try {
			System.out.println(isVideoLikedEarlierSQL);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(isVideoLikedEarlierSQL);
			while (rs.next()) {
				hasRecords = true;
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return hasRecords;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasRecords;

	}
	
	
	public static Map<Integer, MyVideoInfo> fetchCustLikedVideos( Integer custId) {
		String sql = 
		  " select a.id as id , a.video_url as url, a.title as title, a.created_date as createddate,"
		  + " a.sponsor_id ,a.poster_url, a.video_description, "
		  + "a.category_id, c.logo_path as path , a.view_cnt, a.like_cnt"
		+ " from  " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory a with(nolock) "
		+ " right join " + DatabaseConnections.zonesApiLinkedServer + ".polling_cust_video_like_stats b on a.id = b.vid_id   "  
		+ " left join " + DatabaseConnections.zonesApiLinkedServer + ".polling_sponsors c on c.id=a.sponsor_id where b.cust_id =  " + custId ;
		Map <Integer , MyVideoInfo> infoMap = null;
		System.out.println("sql from liked videos " + sql);
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			infoMap = new HashMap <Integer , MyVideoInfo>();
			while (rs.next()) {
				MyVideoInfo myVideoInfo = new MyVideoInfo();
				Integer id = rs.getInt("id");
				if(id == null || id ==0)  continue;
				myVideoInfo.setvId(id);
				myVideoInfo.setvUrl(rs.getString("url"));
				myVideoInfo.setPostrImg(rs.getString("poster_url"));
				myVideoInfo.setSpLogoUrl(rs.getString("path"));
				myVideoInfo.setvTitle(rs.getString("title"));
				myVideoInfo.setUpdDte(rs.getString("createddate"));
				myVideoInfo.setDesc(rs.getString("video_description"));
				myVideoInfo.setCatId(rs.getInt("category_id"));
				myVideoInfo.setVwCnt(rs.getInt("view_cnt"));
				myVideoInfo.setLkCnt(rs.getInt("like_cnt"));
				infoMap.put(id, myVideoInfo);
			}
			System.out.println("from result set " + infoMap.size());
			rs.close();
			stmt.close();
			closeConnection( conn);
			return infoMap;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return infoMap;

	}

	public static Integer custRewardCountForSameVideo(Integer cuId, String awsVideoId) {
	
		Integer rwdCnt = 0;
		String sql = "select customer_id , source_type ,ref_id from " + DatabaseConnections.quizApiLinkedServer
				+ ".rtf_cust_reward_history with(nolock)" + " where customer_id = " + cuId + " and ref_id =" + awsVideoId
				+ " and source_type = '" + SourceType.WATCH_VIDEO.toString() + "'";
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				rwdCnt = 1;
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return rwdCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rwdCnt;
	}
	
	public static Map<Integer , AbuseReportInfo> fetchAbuseOptions(String abuseType) {
		Map <Integer, AbuseReportInfo> abuseOptionMap = null;
		String sql = " select a.id, a.abuse_text, a.abuse_info ,  a.abuse_type , a.status " + 
				" from  " + DatabaseConnections.quizApiLinkedServer 
				+ ".mst_static_abuse_options a where a.status = 1 and a.abuse_type=  '" + abuseType + "'";
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			abuseOptionMap = new HashMap<Integer, AbuseReportInfo> ();
			while (rs.next()) {
			AbuseReportInfo abuseInfo = new AbuseReportInfo(); 
			Integer id = rs.getInt("id");
			abuseInfo.setId(id);
			abuseInfo.setAbuseTxt(rs.getString("abuse_text"));
			abuseInfo.setAbuseInfo(rs.getString("abuse_info"));
			abuseOptionMap.put(id, abuseInfo);
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return abuseOptionMap;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return abuseOptionMap;
	}
	

	public static List<Integer>	fetchAbuseOptionSelectedbyCustomerForMedia(String abuseType,Integer custId, Integer sourceId,Integer commentId) {
		List <Integer> abuseOptionReportedIds = null;
		String sql = "select abuse_id, media_id,comment_id from " + 
				DatabaseConnections.quizApiLinkedServer 
				+ ".customer_reported_abuse_on_media"  
				+ " where customer_id = " + custId 
				+ " AND media_id = " + sourceId ;
				String whereClause = " AND 1 = 2 ";				
				if (PollingUtil.ABUSETYPE_COMMENT.equals(abuseType))
				{
					 whereClause = " AND comment_id = " + commentId;
				}
				else {
					//defaults ..
				}		
				sql = sql + whereClause;
				System.out.println("Already reported abused sql " + sql);
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			abuseOptionReportedIds = new ArrayList<Integer> ();
			while (rs.next()) {
				abuseOptionReportedIds.add(rs.getInt("abuse_id"));
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return abuseOptionReportedIds;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return abuseOptionReportedIds;
	}

	public static Integer insertRTFTvMedia(Integer custId, String mediaFileName, String turl, String catId,String title,String description,Integer custRefId) {
		String sql = " INSERT INTO " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory"
				+ "  (video_url   " + "  ,title " + "  ,status " + "   ,created_date " + "  ,created_by "
				+ "   ,is_default " + "    ,category_id " + "    ,is_deleted  " + "   ,poster_url,video_description , cust_video_id) " + "  VALUES  ( '"
				+ mediaFileName + "',N'" + title + "' , " + 1 + "," + "'" + PollingUtil.getCreatedDateStr() + "' ," + "'"
				+ custId + "' ," + 0 + ", " + catId + ", " + 0 + ", " + "'" + turl + "', N'"  +description + "' , " + custRefId + " )";

		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
			affectedRows = 0;
		} catch (Exception e) {
			e.printStackTrace();
			affectedRows = 0;
		}
		return affectedRows;


		
	}
	public static Integer insertFanClubTvMedia(Integer custId, String mediaFileName, String turl, String catId,String title,String description,Integer custRefId) {
		String sql = " INSERT INTO " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory"
				+ "  (video_url   " + "  ,title " + "  ,status " + "   ,created_date " + "  ,created_by "
				+ "   ,is_default " + "    ,category_id " + "    ,is_deleted  " + "   ,poster_url,video_description , fanclub_video_id) " + "  VALUES  ( '"
				+ mediaFileName + "',N'" + title + "' , " + 1 + "," + "'" + PollingUtil.getCreatedDateStr() + "' ," + "'"
				+ custId + "' ," + 0 + ", " + catId + ", " + 0 + ", " + "'" + turl + "', N'"  +description + "' , " + custRefId + " )";

		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
			affectedRows = 0;
		} catch (Exception e) {
			e.printStackTrace();
			affectedRows = 0;
		}
		return affectedRows;
		
	}

	public static Integer saveAbuseReported(Integer cuId, Integer srcId, String abuseType, Integer commentId,Integer abuseId) {
		String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer +".customer_reported_abuse_on_media("
				+ "customer_id  , media_id ,comment_id,abuse_id ,created_date)   VALUES ( "
            + cuId  + "," + srcId + "," + commentId + " , " + abuseId + ",'" +PollingUtil.getCreatedDateStr() + "' ) ";
		
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
			affectedRows = 0;
		} catch (Exception e) {
			e.printStackTrace();
			affectedRows = 0;
		}
		return affectedRows;
	}
	
	

		public static Map<Integer, PollingVideoInfo> getVideoUrlListForCustomer(String catId,Integer cuId) {

			// System.out.println("**** catId********" + catId);
			String sql = " SELECT a.id , a.video_url,a.title, a.poster_url, "
					+ "a.video_description,a.sponsor_id , b.logo_path,a.status ,"
					+ " a.view_cnt, a.like_cnt "
					+ " FROM " + DatabaseConnections.zonesApiLinkedServer
					+ ".polling_video_inventory a with(nolock) left join polling_sponsors b on a.sponsor_id = b.id  "
					+ " where a.status = 1  and isnull(a.is_deleted,0) = 0 and isnull(a.block_status,0) !=1  "
					+ "and isnull(a.hide_media,0) !=1  and  a.category_id =  " + catId 
					+ " and a.id not in (  select b.vid_id  from "
					+ DatabaseConnections.zonesApiLinkedServer + ".polling_video_played b with(nolock)  "
					+ "where b.cust_id = " + cuId + "  ) "
					+ " order by b.created_date desc " ;			
			

			if ("DEF".equalsIgnoreCase(catId) || StringUtils.isEmpty(catId)) {

				 sql = " SELECT a.id , a.video_url,a.title, a.poster_url, "
						+ "a.video_description,a.sponsor_id , b.logo_path,a.status, "
						+  " a.view_cnt, a.like_cnt " 
						+ " FROM " + DatabaseConnections.zonesApiLinkedServer
						+ ".polling_video_inventory a with(nolock) left join polling_sponsors b on a.sponsor_id = b.id  "
						+ " where a.status = 1 and isnull(a.is_deleted,0) = 0  "
						+ "and isnull(a.hide_media,0) !=1 and isnull(a.is_default,0) = 1 " 
						+ " and a.id not in (  select b.vid_id  from "
						+ DatabaseConnections.zonesApiLinkedServer + ".polling_video_played b with(nolock)  "
						+ "where b.cust_id = " + cuId + "  ) "
						+ " order by b.created_date desc " ;
			}

			Map<Integer, PollingVideoInfo> vUrlMap = new HashMap<Integer, PollingVideoInfo>();
			try {
				Connection conn = DatabaseConnections.getZonesApiConnection();
				 System.out.println("New Video Url Inventory Query : " + sql);
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				
				while (rs.next()) {
					Integer vidId = rs.getInt("id"); 
					PollingVideoInfo vidInfo = new PollingVideoInfo();
					vidInfo.setVidId(vidId);
					vidInfo.setvUrl(rs.getString("video_url"));
					vidInfo.setTitle(rs.getString("title"));
					vidInfo.settUrl(rs.getString("poster_url"));
					vidInfo.setDescription(rs.getString("video_description"));
					vidInfo.setSplogo(rs.getString("logo_path"));
					vidInfo.setLkCnt(rs.getInt("like_cnt"));
					vidInfo.setVwCnt(rs.getInt("view_cnt"));
					vUrlMap.put(vidId, vidInfo);
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return vUrlMap;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return vUrlMap;

		}
	
		public static Map<Integer, PollingVideoInfo> getAddtnlVideoUrlListForCustomer(String catId,Integer cuId, List<Integer> vIds,Map<Integer, PollingVideoInfo> vUrlMap) {
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < vIds.size(); i++) {
					builder.append("?,");
				}
				
			String sql = " SELECT top (10)  a.id , a.video_url,a.title, a.poster_url, "
					+ "a.video_description,a.sponsor_id , b.logo_path,a.status , "
					+  " a.view_cnt, a.like_cnt " 					
					+ " FROM " + DatabaseConnections.zonesApiLinkedServer
					+ ".polling_video_inventory a with(nolock) left join polling_sponsors b on a.sponsor_id = b.id  "
					+ " where a.status = 1  "
					+ "and isnull(a.hide_media,0) !=1 and isnull(a.block_status,0) !=1 "
					+ "and  a.category_id =  " + catId 
					+ " and a.id not in "
					+ " ( "
						+ builder.deleteCharAt(builder.length() - 1).toString() + ")"					
					+ " order by a.created_date desc " ;			
			if ("DEF".equalsIgnoreCase(catId) || StringUtils.isEmpty(catId)) {

				sql = " SELECT top (10) a.id , a.video_url,a.title, a.poster_url, "
						+ "a.video_description,a.sponsor_id , b.logo_path,a.status , "
						+  " a.view_cnt, a.like_cnt " 	
						+ " FROM " + DatabaseConnections.zonesApiLinkedServer
						+ ".polling_video_inventory a with(nolock) left join polling_sponsors b on a.sponsor_id = b.id  "
						+ " where a.status = 1  "
						+ "and isnull(a.hide_media,0) !=1 and isnull(a.is_default,0) = 1 "
						+ " and a.id not in "
						+ " ( "
							+ builder.deleteCharAt(builder.length() - 1).toString() + ")"						
						+ " order by a.created_date desc " ;					
			}

			//Map<Integer, PollingVideoInfo> vUrlMap = new HashMap<Integer, PollingVideoInfo>();
			try {
				Connection conn = DatabaseConnections.getZonesApiConnection();
				 System.out.println("Video Url Inventory Addntl List sql: " + sql);
				PreparedStatement stmt = conn.prepareStatement(sql);
				int index = 1;
				for (Integer o : vIds) {
					stmt.setObject(index++, o);
				}

				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					Integer vidId = rs.getInt("id"); 
					PollingVideoInfo vidInfo = new PollingVideoInfo();
					vidInfo.setVidId(vidId);
					vidInfo.setvUrl(rs.getString("video_url"));
					vidInfo.setTitle(rs.getString("title"));
					vidInfo.settUrl(rs.getString("poster_url"));
					vidInfo.setDescription(rs.getString("video_description"));
					vidInfo.setSplogo(rs.getString("logo_path"));
					vidInfo.setLkCnt(rs.getInt("like_cnt"));
					vidInfo.setVwCnt(rs.getInt("view_cnt"));
					vUrlMap.put(vidId, vidInfo);
					
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return vUrlMap;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return vUrlMap;

		}

		public static Integer saveCommentsOnMedia(Comments comments) {
		
			String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer + ".customer_comments_on_media "
				+ " (customer_id , media_id, " 
				+ "cust_comment_text,"
				+ "cust_comment_media_url,cust_comment_poster_url,cust_comment_image_url,"
				+ "created_date)" + " VALUES"
						+ " (" + comments.getCuId() + "," + comments.getSrcId() + ", N'" 
				+ comments.getCmtTxt() + "',"
				+ "'" + comments.getVidUrl() + "', "
				+ "'" + comments.gettUrl()  + "','" + comments.getImgUrl()  + "' , getDate() "
				+ " )";		
			int updateCnt = 0;  
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}
			return updateCnt;
			
		}

		public static List<Comments> fetchMediaComments(Integer srcId) {
			String sql = 
					  " select a.id,  a.customer_id, a.media_id, a.cust_comment_text, "
					+ " a.cust_comment_media_url , a.cust_comment_poster_url ,"
					+ " a.cust_comment_image_url , convert(varchar, a.created_date, 120) as created_date "
					+ " , b.user_id , b.cust_image_path "
					+ " from  " + DatabaseConnections.quizApiLinkedServer + ".customer_comments_on_media a  with(nolock) "
					+ " left join   "  + DatabaseConnections.zonesApiLinkedServer + ".customer b on "
					+ " a.customer_id = b.id  where a.media_id =  " + srcId
					+ " and isnull(a.hide_comment,0) = 0  and isnull(a.is_deleted,0) = 0 and isnull(a.block_status,0) = 0 order by a.id desc " ; 
					
			List<Comments> commentList = new ArrayList<Comments>();		  
			try {
				Connection conn = DatabaseConnections.getRtfConnection();				
				PreparedStatement stmt = conn.prepareStatement(sql);
				ResultSet rs = stmt.executeQuery();			
				while (rs.next()) {
					Integer cmtId = rs.getInt("id"); 
					Comments comments = new Comments();
					comments.setSrcId(srcId);
					comments.setVidUrl(rs.getString("cust_comment_media_url"));
					comments.settUrl(rs.getString("cust_comment_poster_url"));
					comments.setCmtTxt(rs.getNString("cust_comment_text"));
					comments.setImgUrl(rs.getString("cust_comment_image_url"));
					comments.setUsr(rs.getString("user_id"));
					comments.setCreateDtStr(rs.getString("created_date"));									
					comments.setCmtId(cmtId);
					comments.setDpImg(PollingUtil.aws_s3_cdp_url + rs.getString("cust_image_path"));
					commentList.add(comments);					
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return commentList;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return commentList;		 
					 
					
					 
			
		}

		public static Integer deleteMediaComments(Integer commentId) {
			int updateCnt = 0;
			String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".customer_comments_on_media "
					+ " set is_deleted = 1 where id = " + commentId;
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}

			return updateCnt;
		}

		public static Integer editMediaComments(Comments comments) {
			int updateCnt = 0;
			String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".customer_comments_on_media "
					+ " set cust_comment_text = N'" + comments.getCmtTxt() + "' "
					+ ", updated_date = getDate() where id = " + comments.getCmtId();
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}

			return updateCnt;
		}	
		
		
		public static Map<Integer , AbuseReportInfo> fetchAbuseOptionsForFanClubPosts(String abuseType) {
			Map <Integer, AbuseReportInfo> abuseOptionMap = null;
			String sql = " select a.id, a.abuse_text, a.abuse_info ,  a.abuse_type , a.status " + 
					" from  " + DatabaseConnections.quizApiLinkedServer 
					+ ".mst_static_abuse_options a where a.status = 1 and a.abuse_type=  'FCPOST'";
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				abuseOptionMap = new HashMap<Integer, AbuseReportInfo> ();
				while (rs.next()) {
				AbuseReportInfo abuseInfo = new AbuseReportInfo(); 
				Integer id = rs.getInt("id");
				abuseInfo.setId(id);
				abuseInfo.setAbuseTxt(rs.getString("abuse_text"));
				abuseInfo.setAbuseInfo(rs.getString("abuse_info"));
				abuseOptionMap.put(id, abuseInfo);
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return abuseOptionMap;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return abuseOptionMap;
		}
		
		public static List<Integer>	fetchAbuseOptionSelectedbyCustomerForFCPosts(String abuseType,Integer custId, Integer sourceId,Integer commentId) {
			List <Integer> abuseOptionReportedIds = null;
			String sql = " select abuse_id, fanclub_posts_id from  " + 
					DatabaseConnections.quizApiLinkedServer 
					+ ".fanclub_customer_reported_abuse_on_media with(nolock) "  
					+ "  where customer_id = " + custId 
					+ "  AND fanclub_id = " + sourceId + " AND fanclub_posts_id = " + commentId ;					
					System.out.println("fetchAbuseOptionSelectedbyCustomerForFCPosts sql " + sql);
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				abuseOptionReportedIds = new ArrayList<Integer> ();
				while (rs.next()) {
					abuseOptionReportedIds.add(rs.getInt("abuse_id"));
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return abuseOptionReportedIds;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return abuseOptionReportedIds;
		}
		
		public static Integer saveFanClubPostAbuseReported(Integer cuId, Integer srcId, String abuseType, Integer commentId,Integer abuseId) {
			String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer +".fanclub_customer_reported_abuse_on_media("
					+ "customer_id  , fanclub_posts_id ,abuse_id ,created_date)   VALUES ( "
	            + cuId  + "," + srcId + ","  + abuseId + ",'" +PollingUtil.getCreatedDateStr() + "' ) ";
			
			int affectedRows = 0;
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				affectedRows = statement.executeUpdate();
				statement.close();	
				closeConnection( conn);
			} catch (SQLException se) {
				se.printStackTrace();
				affectedRows = 0;
			} catch (Exception e) {
				e.printStackTrace();
				affectedRows = 0;
			}
			return affectedRows;
		}
		
		
		public static Integer saveFanClubPosts(FanClubPostsDTO fanClubPostsDTO) {
			String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer + ".fanclub_posts "
					+ " (customer_id , fanclub_mst_id, " 
					+ "posts_text,posts_video_url,posts_video_thumbnail_url,posts_image_url,"
					+ "created_date,posts_status)" + " VALUES"
							+ " (" + fanClubPostsDTO.getCuId() + "," + fanClubPostsDTO.getFcId() + ", N'" 
					+ fanClubPostsDTO.getCmtTxt() + "',"
					+ "'" + fanClubPostsDTO.getVidUrl() + "', "
					+ "'" + fanClubPostsDTO.gettUrl()  + "','" + fanClubPostsDTO.getImgUrl()  + "' , getDate() ,'ACTIVE' "
					+ " )";		
				int updateCnt = 0;  
				try {
					Connection conn = DatabaseConnections.getRtfConnection();
					PreparedStatement statement = conn.prepareStatement(sql);
					updateCnt = statement.executeUpdate();
					statement.close();
					closeConnection( conn);
					return updateCnt;
				} catch (Exception ex) {
					ex.printStackTrace();
					updateCnt = 0;
				}
				return updateCnt;
		}
		
	
		
		public static Integer editFanClubPosts(FanClubPostsDTO fanClubPostsDTO) {
			int updateCnt = 0;
			String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_posts "
					+ " set posts_text = N'" + fanClubPostsDTO.getCmtTxt() + "' "
					+ ", updated_date = getDate() where id = " + fanClubPostsDTO.getpId();
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}

			return updateCnt;
		}

		
		public static Integer deleteFanClubPosts(Integer pId) {
			int updateCnt = 0;
			String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_posts "
					+ " set posts_status = 'DELETED' , updated_date=getdate() where id = " + pId;
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}

			return updateCnt;
		}
		public static Integer saveCommentsOnFanClubPosts(FanClubPostsDTO fanClubPostsDTO) {
			String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer + ".fanclub_customer_comments_on_media "
					+ " (customer_id , fanclub_posts_id, " 
					+ " cust_comment_text,cust_comment_media_url,cust_comment_poster_url,cust_comment_image_url,"
					+ " created_date,comment_status)" + " VALUES "
							+ " (" + fanClubPostsDTO.getCuId() + "," + fanClubPostsDTO.getpId() + ", N'" 
					+ fanClubPostsDTO.getCmtTxt() + "',"
					+ "'" + fanClubPostsDTO.getVidUrl() + "', "
					+ "'" + fanClubPostsDTO.gettUrl()  + "','" + fanClubPostsDTO.getImgUrl()  + "' , getDate() ,'ACTIVE' "
					+ " )";		
				int updateCnt = 0;  
				try {
					Connection conn = DatabaseConnections.getRtfConnection();
					PreparedStatement statement = conn.prepareStatement(sql);
					updateCnt = statement.executeUpdate();
					statement.close();
					closeConnection( conn);
					return updateCnt;
				} catch (Exception ex) {
					ex.printStackTrace();
					updateCnt = 0;
				}
				return updateCnt;
		}
		
		public static Integer editFanClubPostComments(FanClubPostsDTO fanClubPostsDTO) {
			int updateCnt = 0;
			String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_customer_comments_on_media "
					+ " set cust_comment_text = N'" + fanClubPostsDTO.getCmtTxt() + "' "
					+ ", updated_date = getDate() where id = " + fanClubPostsDTO.getCmtId();
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}

			return updateCnt;
		}
		
		public static Integer deleteFanClubPostsComments(Integer postCommentId) {
			int updateCnt = 0;
			String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_customer_comments_on_media "
					+ " set comment_status = 'DELETED' , updated_date=getdate() where id = " + postCommentId;
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();	
				closeConnection( conn);
				return updateCnt;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}

			return updateCnt;
		}
		
		
		public static Map<Integer , Comments> getFanClubPosts(String pageNo,FanClubPostsDTO  fcp  ) {
			
			String sql = "	select a.id,a.posts_text,a.posts_image_url,a.posts_video_url,"
					+ " a.posts_video_url,a.posts_video_thumbnail_url,a.no_of_likes, "
					+ " convert(varchar, a.created_date, 120) as created_date "
					+ " , b.user_id , b.cust_image_path , b.id as cust_id "
					+ " from " + DatabaseConnections.quizApiLinkedServer + ".fanclub_posts a with(nolock)  "
					+ " left join   "  + DatabaseConnections.zonesApiLinkedServer + ".customer b on "
					+ " a.customer_id = b.id  "
					+ " where a.posts_status = '" +  PollingUtil.ACTIVE + "' AND a.fanclub_mst_id = "  + fcp.getFcId()
					+ "  order by  a.created_date desc, a.no_of_likes desc "  
					+ " OFFSET ("+pageNo+"-1)*"+PollingUtil.MAX_PAGE_ROWS+" ROWS FETCH NEXT "+PollingUtil.MAX_PAGE_ROWS+"  ROWS ONLY";
			
			System.out.println(" fetch post sql " + sql);
			Map<Integer , Comments> cmtMap = null;
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);				
				cmtMap = new LinkedHashMap<Integer , Comments>();
				while (rs.next()) {
					Comments c = new Comments();
					Integer id = rs.getInt("id");					
					
					c.setPostId(id);
					c.setCmtTxt(rs.getNString("posts_text"));
					c.setImgUrl(rs.getString("posts_image_url"));
					c.setVidUrl(rs.getString("posts_video_url"));
					c.settUrl(rs.getString("posts_video_thumbnail_url"));
					c.setLkCnt(rs.getInt("no_of_likes"));
					c.setCreateDtStr(rs.getString("created_date"));
					c.setUsr(rs.getString("user_id"));
					c.setCuId(rs.getInt("cust_id"));
					c.setDpImg(PollingUtil.aws_s3_cdp_url + rs.getString("cust_image_path"));
					cmtMap.put(id,c);
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return cmtMap;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return cmtMap;
			
		}

		public static Map<Integer, Integer> getCommentCountforPost(List<Integer> postIdList) {
			
		/*
		 * String sql =
		 * "	select fanclub_posts_id ,  count(fanclub_posts_id) as cnt from " +
		 * DatabaseConnections.quizApiLinkedServer +
		 * ".fanclub_customer_comments_on_media with(nolock)" + " where posts_status = "
		 * +
		 * " where fanclub_posts_id in (?) and comment_status='ACTIVE'  group by fanclub_posts_id "
		 * ;
		 */	
			Map<Integer , Integer> cmtCountMap = null;   

			try {
				
				
				
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < postIdList.size(); i++) {
					builder.append("?,");
				}
				
				String sql = "	select fanclub_posts_id ,  count(fanclub_posts_id) as cnt "
						+ "from " + DatabaseConnections.quizApiLinkedServer + ".fanclub_customer_comments_on_media with(nolock)"
						+ " where comment_status = 'ACTIVE' "
						+ " AND fanclub_posts_id in  ("
						+ builder.deleteCharAt(builder.length() - 1).toString() + ")"
						
						+ "and comment_status='ACTIVE'  group by fanclub_posts_id " ; 
					
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement ps = conn.prepareStatement(sql);
				int index = 1;
				for (Integer o : postIdList) {
					ps.setObject(index++, o);
				}
			
				ResultSet rs = ps.executeQuery();				
				cmtCountMap = new HashMap<Integer , Integer>();
				while (rs.next()) {				
					
					cmtCountMap.put(rs.getInt("fanclub_posts_id"),rs.getInt("cnt"));
				}
				rs.close();
				ps.close();
				closeConnection( conn);
				return cmtCountMap;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return cmtCountMap;
		}

		public static Map<Integer, Comments> getFanClubCommentsOnPosts(String pageNo, FanClubPostsDTO fanClubPostsDTO) {
			String sql = "	select a.id, a.fanclub_posts_id , a.cust_comment_text ,a.cust_comment_media_url  "
					+ ", a.cust_comment_image_url , a.cust_comment_poster_url , "					
					+ "convert(varchar, a.created_date, 120) as created_date "
					+ " , b.user_id , b.cust_image_path , b.id  as cust_id "
					+ "from " + DatabaseConnections.quizApiLinkedServer + ".fanclub_customer_comments_on_media a with(nolock)"
					
					+ " left join   "  + DatabaseConnections.zonesApiLinkedServer + ".customer b on "
					+ " a.customer_id = b.id  "
					+ " where a.comment_status = '" +  PollingUtil.ACTIVE + "' AND a.fanclub_posts_id = "  + fanClubPostsDTO.getpId()
					+ " order by a.created_date desc "  
					+ " OFFSET ("+pageNo+"-1)*"+PollingUtil.MAX_PAGE_ROWS+" ROWS FETCH NEXT "+PollingUtil.MAX_PAGE_ROWS+"  ROWS ONLY";
			Map<Integer , Comments> cmtMap = null;
			try {
				
				System.out.println(sql);
				Connection conn = DatabaseConnections.getRtfConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);				
				cmtMap = new LinkedHashMap<Integer , Comments>();
				while (rs.next()) {
					Comments c = new Comments();
					Integer id = rs.getInt("id");					
					c.setCmtId(id);
					c.setPostId(rs.getInt("fanclub_posts_id"));
					c.setCmtTxt(rs.getNString("cust_comment_text"));
					c.setImgUrl(rs.getString("cust_comment_image_url"));
					c.setVidUrl(rs.getString("cust_comment_media_url"));
					c.settUrl(rs.getString("cust_comment_poster_url"));
					//c.setLkCnt(rs.getInt("no_of_likes"));
					c.setCreateDtStr(rs.getString("created_date"));
					c.setUsr(rs.getString("user_id"));
					c.setCuId(rs.getInt("cust_id"));
					c.setDpImg(PollingUtil.aws_s3_cdp_url + rs.getString("cust_image_path"));
					cmtMap.put(id,c);
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return cmtMap;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return cmtMap;
		}

		public static Integer insertFanClubVideoMaster(Comments vidUpdInfo ) {
			Integer id = null;
			String sql = "INSERT INTO  " + DatabaseConnections.quizApiLinkedServer + ".Fanclub_videos_mst  ( "
					+ " fanclub_mst_id,"
					+ " customer_id ,"
					+ " video_title ,"
					+ " video_description ,"
					+ " video_url ,"
					+ " video_thumbnail_url , "
					+ " video_category_id ,"
					+ " video_status , "
					+ " created_date  ) "
					
					+ " VALUES (  "
					+  vidUpdInfo.getFcId() + ", "
					+  vidUpdInfo.getCuId() +  " , N'"
					+  vidUpdInfo.getTitle()  +  "' , N'"
					+  vidUpdInfo.getDescription()  +  "' , '"
					+  vidUpdInfo.getVidUrl()  +  "' , '"
					+  vidUpdInfo.gettUrl()  +  "' , "
					+  vidUpdInfo.getCatId()  +  " , '"
					+ PollingUtil.ACTIVE  +  "' , "
					+  " getDate() ) " ;
					
			
			
			try {
				Connection conn = DatabaseConnections.getZonesApiConnection();				
				PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int affectedRows = statement.executeUpdate();
				if (affectedRows == 0) {
					throw new SQLException("Creating Video Master record failed, no rows affected.");
				}
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						id = generatedKeys.getInt(1);
						
					} else {
						//throw new SQLException("Creating QuestionInfo failed, no ID obtained.");
					}
					generatedKeys.close();
				} catch (Exception ex) {				
					ex.printStackTrace();
				}
				statement.close();
				closeConnection( conn);
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return id;		
			
		}

		public static Map<Integer, Comments> getFanClubVideos(String pageNo, FanClubPostsDTO fanClubPostsDTO) {
			
			String sql = " select a.id,a.fanclub_mst_id, a.customer_id , a.video_title ,a.video_description  "
					+ ", a.video_url , a.video_thumbnail_url , a.video_category_id , "
					+ " isnull(a.no_of_views,5) as no_of_views ,isnull(a.no_of_likes,1) as no_of_likes , "					
					+ "convert(varchar, a.created_date, 120) as created_date "
					+ " , b.user_id , b.cust_image_path , b.id as cust_id "
					+ "from " + DatabaseConnections.quizApiLinkedServer + ".Fanclub_videos_mst a with(nolock)"
					
					+ " left join   "  + DatabaseConnections.zonesApiLinkedServer + ".customer b on "
					+ " a.customer_id = b.id  "
					+ " where a.video_status = '" +  PollingUtil.ACTIVE + "' AND a.fanclub_mst_id = "  + fanClubPostsDTO.getFcId()
					+ " order by a.created_date desc "  
					+ " OFFSET ("+pageNo+"-1)*"+PollingUtil.MAX_PAGE_ROWS+" ROWS FETCH NEXT "+PollingUtil.MAX_PAGE_ROWS+"  ROWS ONLY";
			Map<Integer , Comments> cmtMap = null;
			try {
				System.out.println("*** NEW ******" + sql);
				Connection conn = DatabaseConnections.getRtfConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);				
				cmtMap = new LinkedHashMap<Integer , Comments>();
				while (rs.next()) {
					Comments c = new Comments();
					Integer id = rs.getInt("id");
					c.setvId(id);
					c.setCmtId(id);
					c.setFcId(rs.getInt("fanclub_mst_id"));
					c.setTitle(rs.getNString("video_title"));
					c.setDescription(rs.getNString("video_description"));					
					c.setVidUrl(rs.getString("video_url"));
					c.settUrl(rs.getString("video_thumbnail_url"));					
					c.setCreateDtStr(rs.getString("created_date"));
					c.setUsr(rs.getString("user_id"));
					c.setCuId(rs.getInt("cust_id"));
					c.setCatId(rs.getString("video_category_id"));
					c.setDpImg(PollingUtil.aws_s3_cdp_url + rs.getString("cust_image_path"));
					c.setLkCnt(rs.getInt("no_of_likes"));
					c.setVwCnt(rs.getInt("no_of_views"));
					cmtMap.put(id,c);
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return cmtMap;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return cmtMap;
		}

		public static Integer saveCommentsOnFanClubVideos(FanClubPostsDTO fanClubPostsDTO) {
			
			String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer + ".fanclub_customer_comments_on_media "
					+ " (customer_id , fanclub_video_id, " 
					+ " cust_comment_text,cust_comment_media_url,cust_comment_poster_url,cust_comment_image_url,"
					+ " created_date,comment_status)" + " VALUES "
							+ " (" + fanClubPostsDTO.getCuId() + "," + fanClubPostsDTO.getvId() + ", N'" 
					+ fanClubPostsDTO.getCmtTxt() + "',"
					+ "'" + fanClubPostsDTO.getVidUrl() + "', "
					+ "'" + fanClubPostsDTO.gettUrl()  + "','" + fanClubPostsDTO.getImgUrl()  + "' , getDate() ,'ACTIVE' "
					+ " )";		
				int updateCnt = 0;  
				try {
					Connection conn = DatabaseConnections.getRtfConnection();
					PreparedStatement statement = conn.prepareStatement(sql);
					updateCnt = statement.executeUpdate();
					statement.close();
					closeConnection( conn);
					return updateCnt;
				} catch (Exception ex) {
					ex.printStackTrace();
					updateCnt = 0;
				}
				return updateCnt;
		}
		
		public static Integer deleteFanClubVideoomments(Integer vidCommentId) {
			int updateCnt = 0;
			String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_customer_comments_on_media "
					+ " set comment_status = 'DELETED' , updated_date=getdate() where id = " + vidCommentId;
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();	
				closeConnection( conn);
				return updateCnt;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}

			return updateCnt;
		}

		public static Map<Integer, Comments> getFanClubCommentsOnVideos(String pNoStr,
				FanClubPostsDTO fanClubPostsDTO) {
			String sql = "	select a.id, a.fanclub_video_id , a.cust_comment_text ,a.cust_comment_media_url  "
					+ ", a.cust_comment_image_url , a.cust_comment_poster_url , "					
					+ "convert(varchar, a.created_date, 120) as created_date "
					+ " , b.user_id , b.cust_image_path "
					+ "from " + DatabaseConnections.quizApiLinkedServer + ".fanclub_customer_comments_on_media a with(nolock)"
					
					+ " left join   "  + DatabaseConnections.zonesApiLinkedServer + ".customer b on "
					+ " a.customer_id = b.id  "
					+ " where a.comment_status = '" +  PollingUtil.ACTIVE + "' AND a.fanclub_video_id = "  + fanClubPostsDTO.getvId()
					+ " order  a.created_date desc "  
					+ " OFFSET ("+pNoStr+"-1)*"+PollingUtil.MAX_PAGE_ROWS+" ROWS FETCH NEXT "+PollingUtil.MAX_PAGE_ROWS+"  ROWS ONLY";
			Map<Integer , Comments> cmtMap = null;
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);				
				cmtMap = new LinkedHashMap<Integer , Comments>();
				while (rs.next()) {
					Comments c = new Comments();
					Integer id = rs.getInt("id");					
					c.setCmtId(id);
					c.setPostId(rs.getInt("fanclub_video_id"));
					c.setCmtTxt(rs.getNString("cust_comment_text"));
					c.setImgUrl(rs.getString("cust_comment_image_url"));
					c.setVidUrl(rs.getString("cust_comment_media_url"));
					c.settUrl(rs.getString("cust_comment_poster_url"));
					c.setLkCnt(rs.getInt("no_of_likes"));
					c.setCreateDtStr(rs.getString("created_date"));
					c.setUsr(rs.getString("user_id"));
					c.setDpImg(PollingUtil.aws_s3_cdp_url + rs.getString("cust_image_path"));
					cmtMap.put(id,c);
				}
				rs.close();
				stmt.close();
				closeConnection( conn);
				return cmtMap;
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return cmtMap;

		}
		
	public static Integer saveLikesOnFanClubPosts(FanClubPostsDTO fanClubPostsDTO) {
			
			String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer +
					".fanclub_media_likes "
					+ " (fanclub_posts_id, customer_id , like_status,like_date )" 
					+ " VALUES  ("
					 + fanClubPostsDTO.getpId() + "," + fanClubPostsDTO.getCuId() + ","
							+ 1  + " , getDate()  "
					+ " )";		
				int updateCnt = 0;  
				try {
					Connection conn = DatabaseConnections.getRtfConnection();
					PreparedStatement statement = conn.prepareStatement(sql);
					updateCnt = statement.executeUpdate();
					statement.close();
					closeConnection( conn);
					return updateCnt;
				} catch (Exception ex) {
					ex.printStackTrace();
					updateCnt = 0;
				}
				return updateCnt;
		}
	public static Integer updateDisLikesOnFanClubPosts(FanClubPostsDTO fanClubPostsDTO) {
		
		String sql = " UPDATE " + DatabaseConnections.quizApiLinkedServer +
				".fanclub_media_likes set like_status=0,like_date=getdate() where like_status=1 and fanclub_posts_id="+fanClubPostsDTO.getpId()+" and customer_id=" + fanClubPostsDTO.getCuId();
			int updateCnt = 0;  
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}
			return updateCnt;
	}

	public static Integer updateFanClubPostLikeCount(FanClubPostsDTO fanClubPostsDTO) {
		int updateCnt = 0;
		String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_posts "
				+ " set no_of_likes = isnull(no_of_likes,0) + 1 where id = " + fanClubPostsDTO.getpId();
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}
	public static Integer updateFanClubPostDisLikeCount(FanClubPostsDTO fanClubPostsDTO) {
		int updateCnt = 0;
		String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_posts "
				+ " set no_of_likes = no_of_likes - 1 where id = " + fanClubPostsDTO.getpId()+" and no_of_likes > 0";
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}
	
	public static Integer saveLikesOnFanClubVideos(FanClubPostsDTO fanClubPostsDTO) {
		
		String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer +
				".fanclub_media_likes "
				+ " (fanclub_videos_id, customer_id , like_status,like_date )" 
				+ " VALUES  ("
				 + fanClubPostsDTO.getvId() + "," + fanClubPostsDTO.getCuId() + ","
						+ 1  + " , getDate()  "
				+ " )";		
			int updateCnt = 0;  
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}
			return updateCnt;
	}
	public static Integer updateDisLikesOnFanClubVideos(FanClubPostsDTO fanClubPostsDTO) {
		
		String sql = " UPDATE " + DatabaseConnections.quizApiLinkedServer +
				".fanclub_media_likes set like_status=0,like_date=getdate() where like_status=1 and fanclub_videos_id="+fanClubPostsDTO.getvId()+" and customer_id=" + fanClubPostsDTO.getCuId();
			int updateCnt = 0;  
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}
			return updateCnt;
	}
	

	public static Integer updateFanClubVideoLikeCount(FanClubPostsDTO fanClubPostsDTO) {
		int updateCnt = 0;
		String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_videos_mst  "
				+ " set no_of_likes = isnull(no_of_likes,0) + 1 where id = " + fanClubPostsDTO.getvId();
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}
	
	public static Integer updateFanClubVideoDisLikeCount(FanClubPostsDTO fanClubPostsDTO) {
		int updateCnt = 0;
		String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_videos_mst  "
				+ " set no_of_likes = no_of_likes - 1 where id = " + fanClubPostsDTO.getvId()+" and no_of_likes > 0";
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static Integer updateFanClubVideoMaster(Comments vidInfo) {
		
		Integer updateCnt = 0;
		String sql = 
				" UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_videos_mst  "
						+ " set  video_title  =  N'" + vidInfo.getTitle()  + "',"
						+ " video_description = N'" +  vidInfo.getDescription() + "' , "
						+ " video_category_id  = "  + vidInfo.getCatId() + " , "
						+ " updated_date = getDate() "
						+ " where  id =   "  + vidInfo.getvId() ; 
						
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}

	public static List<Integer> fetchLikedPostIds(List<Integer> postIdList, Integer custId) {
		List<Integer> likedPostList = null;
		try {
			
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < postIdList.size(); i++) {
				builder.append("?,");
			}			
			String sql = "	select fanclub_posts_id  "
					+ "from " + DatabaseConnections.quizApiLinkedServer + ".fanclub_media_likes with(nolock) "
					+ " where customer_id =  " + custId
					+ " AND like_status=1 AND fanclub_posts_id in  ("
					+ builder.deleteCharAt(builder.length() - 1).toString() + ")";				
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			int index = 1;
			for (Integer o : postIdList) {
				ps.setObject(index++, o);
			}		
			ResultSet rs = ps.executeQuery();				
			likedPostList = new ArrayList<Integer>();
			while (rs.next()) {				
				
				likedPostList.add(rs.getInt("fanclub_posts_id"));
			}
			rs.close();
			ps.close();
			closeConnection( conn);
			return likedPostList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return likedPostList;
	
	}
		
	public static List<Integer> fetchLikedFanClubVideoIds(List<Integer> vidIdList, Integer custId) {
		List<Integer> likedVidList = null;
		try {
			
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < vidIdList.size(); i++) {
				builder.append("?,");
			}			
			String sql = "	select fanclub_videos_id  "
					+ "from " + DatabaseConnections.quizApiLinkedServer + ".fanclub_media_likes with(nolock) "
					+ " where customer_id =  " + custId
					+ " AND like_status=1 AND fanclub_videos_id in  ("
					+ builder.deleteCharAt(builder.length() - 1).toString() + ")";				
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			int index = 1;
			for (Integer o : vidIdList) {
				ps.setObject(index++, o);
			}		
			ResultSet rs = ps.executeQuery();				
			likedVidList = new ArrayList<Integer>();
			while (rs.next()) {
				likedVidList.add(rs.getInt("fanclub_videos_id"));
			}
			rs.close();
			ps.close();
			closeConnection( conn);
			return likedVidList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return likedVidList;
	
	}

	public static Integer saveFanClubVideoAbuseReported(AbuseReportInfo abuseReportInfo) {
		String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer +".fanclub_customer_reported_abuse_on_media("
				+ "customer_id  , fanclub_video_id ,abuse_id ,created_date)   VALUES ( "
            + abuseReportInfo.getCuId()  + "," + abuseReportInfo.getSrcId()  + " , " + abuseReportInfo.getAbuseOptionId() + ",'" +PollingUtil.getCreatedDateStr() + "' ) ";
		
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
			affectedRows = 0;
		} catch (Exception e) {
			e.printStackTrace();
			affectedRows = 0;
		}
		return affectedRows;
	}

	public static Integer saveFanClubPostCommentsAbuseReported(AbuseReportInfo abuseReportInfo) {
		String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer +".fanclub_customer_reported_abuse_on_media("
				+ "customer_id  , fanclub_comments_id ,abuse_id ,created_date)   VALUES ( "
            + abuseReportInfo.getCuId()  + "," + abuseReportInfo.getSrcId()  + " , " + abuseReportInfo.getAbuseOptionId() + ",'" +PollingUtil.getCreatedDateStr() + "' ) ";
		
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
			affectedRows = 0;
		} catch (Exception e) {
			e.printStackTrace();
			affectedRows = 0;
		}
		return affectedRows;
	}

	public static Integer getFCPostCommentsCount(Integer pId) {
		String sql = " select COUNT(CUST_COMMENT_TEXT) as cmtcnt from fanclub_customer_comments_on_media "
				  + " WITH(NOLOCK) WHERE FANCLUB_POSTS_ID = " + pId + " AND COMMENT_STATUS = 'ACTIVE' " ;
		
		Integer cnt = 0;
		try {
		
			Connection conn = DatabaseConnections.getRtfConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);	
			
			while (rs.next()) {				
				 cnt = rs.getInt("cmtcnt");	
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return cnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cnt;
	}

	public static int applyCustomerMagicWandStat(Integer custId, Integer magicwand) {
		if(magicwand == null) magicwand = 0;
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".customer" + 
				" SET magic_wands  = isnull(magic_wands  , 1 ) - "
				+ magicwand + " WHERE id = " + custId;
		int updateCnt = 0;
		try {
			System.out.println("[][UPDATE CUSTOMER REWARDS magic_wands  SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}

		return updateCnt;
	}
	


	public static int updateVidInventoryLikeCount(Integer videoId) {
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory set"
				+ " like_cnt = isnull(like_cnt,0) + 1 where id = " + videoId;
		int updateCnt = 0;
		try {
			//System.out.println("[rtftv like][UPDATE lk cnt in rtftv SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();			
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
		
	}
	public static int updateVidInventoryDisLikeCount(Integer videoId) {
		String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory set"
				+ " like_cnt = isnull(like_cnt,0) - 1 where id = " + videoId;
		int updateCnt = 0;
		try {
			//System.out.println("[rtftv like][UPDATE lk cnt in rtftv SQL]" + sql);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();			
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
		
	}

	public static Integer updateExistingLikeStatsToLike(Integer videoId, Integer custId) {
		String mediaUploadSQL = " update " + DatabaseConnections.zonesApiLinkedServer+".polling_cust_video_like_stats set like_status =1 "
				+ "where cust_id="+custId+" and vid_id="+videoId;
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(mediaUploadSQL);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}
	

	public static Integer insertVideoLikeStats(Integer videoId, Integer custId) {
		String mediaUploadSQL = " insert into " + DatabaseConnections.zonesApiLinkedServer
				+ ".polling_cust_video_like_stats" + "(   cust_id  ,vid_id  ,like_date, like_status ) "
					+ "VALUES ( "+custId+", "+videoId+", '"+ PollingUtil.getCreatedDateStr()+"', 1)";
		// System.out.println(" - updateVideoLikeStats - " + mediaUploadSQL);
		int affectedRows = 0;
		try {
			Connection conn = DatabaseConnections.getZonesApiConnection();
			PreparedStatement statement = conn.prepareStatement(mediaUploadSQL);
			affectedRows = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}
	
	public static Boolean isFanClubVideoLikedEarlier(Integer videoId, Integer custId) {
			
		String isVideoLikedEarlierSQL = " select customer_id  from " + DatabaseConnections.quizApiLinkedServer
				+ ".fanclub_media_likes where customer_id = " + custId + "  and fanclub_videos_id =  " + videoId;
		Boolean hasRecords = false;
		try {
			System.out.println("FC VIDEO LIKE " + isVideoLikedEarlierSQL);
			Connection conn = DatabaseConnections.getZonesApiConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(isVideoLikedEarlierSQL);
			while (rs.next()) {
				hasRecords = true;
			}
			rs.close();
			stmt.close();
			closeConnection( conn);
			return hasRecords;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasRecords;
	}
	
public static Integer updateExistingLikesOnFanClubVideos(FanClubPostsDTO fanClubPostsDTO) {
		
		String sql = " UPDATE " + DatabaseConnections.quizApiLinkedServer +
				".fanclub_media_likes set like_status=1,like_date=getdate() where fanclub_videos_id="+fanClubPostsDTO.getvId()+" and customer_id=" + fanClubPostsDTO.getCuId();
			int updateCnt = 0;  
			try {
				Connection conn = DatabaseConnections.getRtfConnection();
				PreparedStatement statement = conn.prepareStatement(sql);
				updateCnt = statement.executeUpdate();
				statement.close();
				closeConnection( conn);
				return updateCnt;
			} catch (Exception ex) {
				ex.printStackTrace();
				updateCnt = 0;
			}
			return updateCnt;
	}

public static Integer insertLikesOnFanClubVideos(FanClubPostsDTO fanClubPostsDTO) {
	
	String sql = " INSERT INTO " + DatabaseConnections.quizApiLinkedServer +
			".fanclub_media_likes "
			+ " (fanclub_videos_id, customer_id , like_status,like_date )" 
			+ " VALUES  ("
			 + fanClubPostsDTO.getvId() + "," + fanClubPostsDTO.getCuId() + ","
					+ 1  + " , getDate()  "
			+ " )";		
		int updateCnt = 0;  
		try {
			Connection conn = DatabaseConnections.getRtfConnection();
			PreparedStatement statement = conn.prepareStatement(sql);
			updateCnt = statement.executeUpdate();
			statement.close();
			closeConnection( conn);
			return updateCnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			updateCnt = 0;
		}
		return updateCnt;
}

public static int updateVideoViewCount(Integer cuId, Integer vid) {
	String sql = " UPDATE " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory set"
			+ " view_cnt = isnull(view_cnt,0) + 1 where id = " + vid;
	int updateCnt = 0;
	try {
		//System.out.println("[rtftv like][UPDATE lk cnt in rtftv SQL]" + sql);
		Connection conn = DatabaseConnections.getZonesApiConnection();
		PreparedStatement statement = conn.prepareStatement(sql);
		updateCnt = statement.executeUpdate();			
		statement.close();
		closeConnection( conn);
		return updateCnt;
	} catch (Exception ex) {
		ex.printStackTrace();
		updateCnt = 0;
	}
	return updateCnt;
	
}


public static Boolean fetchPrevLikedStatusOnFanClubPost(FanClubPostsDTO fanClubPostsDTO) {
	
	String sql = " SELECT  customer_id FROM " + DatabaseConnections.quizApiLinkedServer +
			".fanclub_media_likes WHERE fanclub_posts_id =  " + fanClubPostsDTO.getpId();
				
	Boolean hasRecords = false;
	try {
		
		Connection conn = DatabaseConnections.getZonesApiConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			hasRecords = true;
		}
		rs.close();
		stmt.close();
		closeConnection( conn);
		return hasRecords;
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return hasRecords;
}


public static CustomerDailyRewardLimits fetchCustDailyRewardLimitConfig(Integer cuId) {
	
		String sql = " SELECT  customer_id " + ",tot_fc_cr_po ,tot_fc_ev ,"
				+ " tot_fc_ev_lk ,tot_fc_po_lk "
				+ " ,tot_fc_vd_ul ,tot_ffo_vd_ul ,tot_sh_vd ,"
				+ " tot_vd_lk ,rwd_date "
				+ " ,rwd_fc_cr_po , rwd_fc_ev ,rwd_fc_ev_lk ,"
				+ " rwd_fc_po_lk ,rwd_fc_vd_ul "
				+ " ,rwd_ffo_vd_ul ,rwd_sh_vd,rwd_vd_lk "
				+ " , upd_dttm FROM "
				+ DatabaseConnections.quizApiLinkedServer
				+ ".rtf_cust_daily_reward_limits WHERE "
				+ " customer_id = "	+ cuId +  " and "
				+ " convert(varchar(10), upd_dttm, 102) = "
				+ " convert(varchar(10), getdate(), 102)" ;
System.out.println(sql);
		CustomerDailyRewardLimits custDalyRwdsLimits = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs=null;		
		try {
			conn = DatabaseConnections.getRtfConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			custDalyRwdsLimits = new CustomerDailyRewardLimits();
			while (rs.next()) {
				custDalyRwdsLimits.setCustomerId(rs.getInt("customer_id"));
				custDalyRwdsLimits.setTotFcCrPo(rs.getInt("tot_fc_cr_po"));
				custDalyRwdsLimits.setTotFcEv(rs.getInt("tot_fc_ev"));
				custDalyRwdsLimits.setTotFcEv_lk(rs.getInt("tot_fc_ev_lk"));
				custDalyRwdsLimits.setTotFcPoLk(rs.getInt("tot_fc_po_lk"));
				custDalyRwdsLimits.setTotFcVdUl(rs.getInt("tot_fc_vd_ul"));
				custDalyRwdsLimits.setTotFfo_vd_ul(rs.getInt("tot_ffo_vd_ul"));
				custDalyRwdsLimits.setTot_sh_vd(rs.getInt("tot_sh_vd"));
				custDalyRwdsLimits.setTot_vd_lk(rs.getInt("tot_vd_lk"));
				custDalyRwdsLimits.setRwd_date(rs.getDate("rwd_date"));
				custDalyRwdsLimits.setRwdFc_cr_po(rs.getInt("rwd_fc_cr_po"));
				custDalyRwdsLimits.setRwdFcEv(rs.getInt("rwd_fc_ev"));
				custDalyRwdsLimits.setRwdFcEv_lk(rs.getInt("rwd_fc_ev_lk"));
				custDalyRwdsLimits.setRwdFc_po_lk(rs.getInt("rwd_fc_po_lk"));
				custDalyRwdsLimits.setRwdFc_vd_ul(rs.getInt("rwd_fc_vd_ul"));
				custDalyRwdsLimits.setRwdFfo_vd_ul(rs.getInt("rwd_ffo_vd_ul"));
				custDalyRwdsLimits.setRwd_sh_vd(rs.getInt("rwd_sh_vd"));
				custDalyRwdsLimits.setRwd_vd_lk(rs.getInt("rwd_vd_lk"));
				custDalyRwdsLimits.setUpd_dttm(rs.getDate("upd_dttm"));
			}
		
		
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
			rs.close();
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			try {
			stmt.close();
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			closeConnection(conn);
		}
		return custDalyRwdsLimits;
		
}




public static void closeConnection(Connection con) {
	try {
		//null check not required as pooled obj.
		DatabaseConnections.closeConnection(con);
	}catch(Exception ex) {
		ex.printStackTrace();
	}
	
}






public static void closeAllDBResources(ResultSet rs, Statement st , Connection con) {
	try {
		rs.close();
		
	}catch(Exception ex) {
		ex.printStackTrace();
	}
	try {
		st.close();
		
	}catch(Exception ex) {
		ex.printStackTrace();
	}
	try {
		con.close();
		
	}catch(Exception ex) {
		ex.printStackTrace();
	}
	
}


public static FanClubVidPlayListDTO getFanClubVideoPlayList(String pageNo, FanClubVidPlayListDTO fanClubVidPlayListDTO ) {
	String sql = " select a.id , a.video_url , video_thumbnail_url  "
			+ "from " + DatabaseConnections.quizApiLinkedServer + ".Fanclub_videos_mst a with(nolock)"
			+ " where a.video_status = '" +  PollingUtil.ACTIVE + "' AND a.fanclub_mst_id = "  + fanClubVidPlayListDTO.getFcId()
			+ " and a.id <=  " + fanClubVidPlayListDTO.getvId() + " order by a.id desc "  
			+ " OFFSET ("+pageNo+"-1)*"+PollingUtil.MAX_PAGE_ROWS+" ROWS FETCH NEXT "+PollingUtil.MAX_PAGE_ROWS+"  ROWS ONLY";
			
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
	try {
		System.out.println(sql);
		 conn = DatabaseConnections.getRtfConnection();
		 stmt = conn.createStatement();
		 rs = stmt.executeQuery(sql);
		
		 List<FCPlayListInfo> playlist = new ArrayList<FCPlayListInfo>();
		while (rs.next()) {	
			FCPlayListInfo plinfo = new FCPlayListInfo();
			plinfo.setVurl(rs.getString("video_url"));
			plinfo.setvId(rs.getInt("id"));
			plinfo.setTurl(rs.getString("video_thumbnail_url"));
			playlist.add(plinfo);				
		}
		fanClubVidPlayListDTO.setPlayLst(playlist);				
		return fanClubVidPlayListDTO;
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}
	finally {
		try {
		rs.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		try {
		stmt.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		closeConnection(conn);
	}
	return fanClubVidPlayListDTO;
}

public static Integer fetchInventoryIdForFanClubVideo(String awsVideoId) {
	String sql = " select a.id  "
			+ " from " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_inventory a with(nolock)"
			+ " where a.fanclub_video_id = " + awsVideoId ;
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			Integer vidId = 0;
	try {		
		 System.out.println(sql);
		 conn = DatabaseConnections.getRtfConnection();
		 stmt = conn.createStatement();
		 rs = stmt.executeQuery(sql);		
		vidId = 0;
		while (rs.next()) {			
			vidId = rs.getInt("id");							
		}
		
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}
	finally {
		try {
		rs.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		try {
		stmt.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		closeConnection(conn);
	}
	return vidId;
}


public static Integer updateCassSFStars(Integer finalSfstars , Integer cuId) {
	Integer sts = 0;
	try {
	CassandraConnector.getSession().execute("UPDATE customer set super_fan_chances = ? where customer_id = ? ",
			finalSfstars, finalSfstars);
	sts =  1;
	}catch(Exception ex) {
		ex.printStackTrace();
		sts =  0;
	}
	return sts;
}

public static Integer updateFanClubVideoViewCount(String fcVidId) {
	int updateCnt = 0;
	String sql = " UPDATE " +  DatabaseConnections.quizApiLinkedServer + ".fanclub_videos_mst  "
			+ " set no_of_views = isnull(no_of_views,0) + 1 where id = " + fcVidId;
	try {
		Connection conn = DatabaseConnections.getRtfConnection();
		PreparedStatement statement = conn.prepareStatement(sql);
		updateCnt = statement.executeUpdate();
		statement.close();
		closeConnection( conn);
		return updateCnt;
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception ex) {
		ex.printStackTrace();
		updateCnt = 0;
	}

	return updateCnt;
}

public static int insertFanClubVideoPlayedHist(Integer cuId, Integer id) {

	String sql = " INSERT INTO  " + DatabaseConnections.zonesApiLinkedServer + ".polling_video_played_hist"
			+ " (vid_id , cust_id , play_date,pldsrc )  VALUES (" + id + " , " + cuId + ", '"
			+ PollingUtil.getCreatedDateStr() + "' , 'FC'  )";
	int affectedRows = 0;
	try {

		Connection conn = DatabaseConnections.getRtfConnection();
		PreparedStatement statement = conn.prepareStatement(sql);
		affectedRows = statement.executeUpdate();
		statement.close();
		closeConnection( conn);
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	return affectedRows;
}

	
}
