package com.zonesws.webservices.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.utils.CassContestUtil;

/**
 * CustomerCacheUtil
 * @author KUlaganathan
 *
 */
public class CustomerCacheUtil {
	private static Logger logger = LoggerFactory.getLogger(CustomerCacheUtil.class);
	public static Map<Integer, CassCustomer> customerMap = new ConcurrentHashMap<Integer, CassCustomer>();
	
	public static CassCustomer getCustomer(Integer custId){
		CassCustomer obj = customerMap.get(custId);
		if(null != obj) {
			return obj;
		}
		try {
			obj = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
			return obj;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	 
	public static void init() throws Exception{
		logger.info("Customer Cahce Util :- Begins: "+new Date());
		
		List<CassCustomer> list = CassandraDAORegistry.getCassCustomerDAO().getAll();
		
		Map<Integer, CassCustomer> customerTempMap = new HashMap<Integer, CassCustomer>();
		
		for (CassCustomer cassCustomer : list) {
			//customerTempMap.put(cassCustomer.getId(), cassCustomer);
			try {
				CassContestUtil.updateCassCustomerMap(cassCustomer);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if(null != customerTempMap && !customerTempMap.isEmpty()) {
			//customerMap.clear();
			//customerMap = new ConcurrentHashMap<Integer, CassCustomer>(customerTempMap);
		}
		logger.info("Customer Cahce Util :- Ends: "+new Date());
	}
	  
	
}