package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RTFBotsUtil
 * @author KUlaganathan
 *
 */
public class RTFBotsUtil {
	private static Logger logger = LoggerFactory.getLogger(RTFBotsUtil.class);
	public static List<Integer> botCustomerIds = new ArrayList<Integer>();
	public static List<Integer> botsExcludeGrandWinners = new ArrayList<Integer>();
	
	private static boolean isMigrated = false;
	
	public static List<Integer> getAllBots(){
		if(null != botCustomerIds && !botCustomerIds.isEmpty()) {
			return botCustomerIds;
		}else {
			try {
				init();
			}catch(Exception e) {
				e.printStackTrace();
			}
			return botCustomerIds;
		} 
	}
	
	public static List<Integer> getAllBotsForJackpot(){
		if(null != botsExcludeGrandWinners && !botsExcludeGrandWinners.isEmpty()) {
			return botsExcludeGrandWinners;
		}else {
			try {
				initJackpotBots();
			}catch(Exception e) {
				e.printStackTrace();
			}
			return botsExcludeGrandWinners;
		} 
	}
	
	public static List<Integer> getBotsForGrandWinnerComputation(){
		return botCustomerIds; 
	}
	
	public static void init() throws Exception{
		List<Integer> botIds = SQLDaoUtil.getBotsAndOldGrandWinners();
		if(null != botIds && !botIds.isEmpty()) {
			botCustomerIds.clear();
			botCustomerIds =  new ArrayList<Integer>(botIds);
		}
		logger.info("BOTS Util :- Ends: "+new Date());
	}
	
	public static void initJackpotBots() throws Exception{
		
		//List<Integer> botsForJackpot = SQLDaoUtil.getOnlyBotsForJackpotWinners();
		List<Integer> botsForJackpot = SQLDaoUtil.getBotsAndOldGrandWinners();
		if(null != botsForJackpot && !botsForJackpot.isEmpty()) {
			botsExcludeGrandWinners.clear();
			botsExcludeGrandWinners =  new ArrayList<Integer>(botsForJackpot);
		}
		logger.info("BOTS Util :- Ends: "+new Date());
	}
	
	
}