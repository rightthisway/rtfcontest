package com.zonesws.webservices.utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseConnections {
	private static Logger logger = LoggerFactory.getLogger(DatabaseConnections.class);
	
	private	static Connection rtfZonesConnection;
	private	static Connection rtfEcommConnection;
	private	static Connection rtfQuizConnection;

	//public static String quizApiLinkedServer = "RTFQuizMaster"; -- PROD 
	public static String quizApiLinkedServer;// = "RTFQuizMaster_Sanbox.dbo";
	//public static String zonesApiLinkedServer = "ZonesApiPlatformV2_Sanbox.dbo";
	public static String zonesApiLinkedServer;// = "ZonesApiPlatformV2";
	
	public static String quizMasterDbUrl;
	public static String quizMasterDbUserName;
	public static String quizMasterDbPassword;
	
	public static String rtfZonesDbUrl;
	public static String rtfZonesDbUserName;
	public static String rtfZonesDbPassword;
		
	public static void loadApplicationValues() throws Exception {
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		quizApiLinkedServer = resourceBundle.getString("rtf.quiz.master.db.linked.server");//RTFQuizMaster
		zonesApiLinkedServer = resourceBundle.getString("quiz.rtf.linked.server");//ZonesApiPlatformV2
		
		quizMasterDbUrl = resourceBundle.getString("quiz.contest.db.url");
		quizMasterDbUserName = resourceBundle.getString("quiz.contest.db.user.name");
		quizMasterDbPassword = resourceBundle.getString("quiz.contest.db.password");
		
		System.out.println("DATBASE CONN : quiz.contest.db.url : "+quizMasterDbUrl+" : "+resourceBundle.getString("quiz.contest.db.url"));
		System.out.println("DATBASE CONN : quiz.contest.db.user.name : "+quizMasterDbUserName+" : "+resourceBundle.getString("quiz.contest.db.user.name"));;
		System.out.println("DATBASE CONN : quiz.contest.db.password : "+quizMasterDbPassword+" : "+resourceBundle.getString("quiz.contest.db.password"));
		
		
		rtfZonesDbUrl = resourceBundle.getString("zones.admin.jdbc.url");
		rtfZonesDbUserName = resourceBundle.getString("zones.admin.jdbc.user.name");
		rtfZonesDbPassword = resourceBundle.getString("zones.admin.jdbc.password");
		
		System.out.println("DATBASE CONN : zones.admin.jdbc.url : "+rtfZonesDbUrl+" : "+resourceBundle.getString("zones.admin.jdbc.url"));
		System.out.println("DATBASE CONN : zones.admin.jdbc.user.name : "+rtfZonesDbUserName+" : "+resourceBundle.getString("zones.admin.jdbc.user.name"));;
		System.out.println("DATBASE CONN : zones.admin.jdbc.password : "+rtfZonesDbPassword+" : "+resourceBundle.getString("zones.admin.jdbc.password"));
	}
	
	public static DataSource rtfQuizDataSource=null;
	public static DataSource zonesDataSource=null;
	public static DataSource rtfEcomDataSource=null;
	
	public static void setRtfDatasource(DataSource datasource) {
		rtfQuizDataSource = datasource;
	}
	public static void setZonesDatasource(DataSource datasource) {
		zonesDataSource = datasource;
	}
	public static void setRtfEcomDataSource(DataSource datasource) {
		rtfEcomDataSource = datasource;
	}
	
	public static Connection getRtfConnection() throws Exception {
		//System.out.println("Max Idle: " + rtfQuizDataSource.getMaxIdle()  + "Max Active: " + rtfQuizDataSource.getMaxIdle() + "Active: " + rtfQuizDataSource.getNumActive() + "Idle: " + rtfQuizDataSource.getNumIdle() );
		return rtfQuizDataSource.getConnection();
	}
	public static Connection getZonesApiConnection() throws Exception {
		return zonesDataSource.getConnection();
	}
	public static Connection getRtfEcomConnection() throws Exception {
		return rtfEcomDataSource.getConnection();
		//return getrtfEcoomConnectionOld();
	}
	public static void closeConnection(Connection con) {
		try {
			//null check not required as pooled obj.
			con.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}
		
	public static Connection getRtfConnectionOld() throws Exception{
		if(rtfQuizConnection!=null){
			try {
				if(!rtfQuizConnection.isClosed()){
					return rtfQuizConnection;
				}
			} catch (SQLException e) {
				System.out.println("..............CONNECTION FOUND CLOSE........");
			}
		}
		/*ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("quiz.contest.db.url");
		String username = resourceBundle.getString("quiz.contest.db.user.name");
		String password =resourceBundle.getString("quiz.contest.db.password");*/
		
		/*String url ="jdbc:sqlserver://10.0.1.51:1433;database=RTFQuizMaster_Sanbox;sendStringParametersAsUnicode=false";
		String username ="tayo";
		String password ="po01ro02ro30";*/
		
		try{
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			rtfQuizConnection = DriverManager.getConnection(quizMasterDbUrl, quizMasterDbUserName, quizMasterDbPassword);
			return rtfQuizConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	
	public static Connection getZonesApiConnectionOld() throws Exception{
		if(rtfZonesConnection!=null){
			try {
				if(!rtfZonesConnection.isClosed()){
					return rtfZonesConnection;
				}
			} catch (SQLException e) {
				System.out.println("..............CONNECTION FOUND CLOSE........");
			}
		}
		/*ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("zones.admin.jdbc.url");
		String username = resourceBundle.getString("zones.admin.jdbc.user.name");
		String password =resourceBundle.getString("zones.admin.jdbc.password");*/
		
		/*String url ="jdbc:sqlserver://54.209.112.185:1433;database=ZonesApiPlatformV2_Sanbox;sendStringParametersAsUnicode=false";
		String username ="tayo";
		String password ="po01ro02ro30";*/
		
		try{
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			rtfZonesConnection = DriverManager.getConnection(rtfZonesDbUrl, rtfZonesDbUserName, rtfZonesDbPassword);
			return rtfZonesConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	public static Connection getrtfEcoomConnectionOld() throws Exception{
		/*if(rtfZonesConnection!=null){
			try {
				if(!rtfZonesConnection.isClosed()){
					return rtfZonesConnection;
				}
			} catch (SQLException e) {
				System.out.println("..............CONNECTION FOUND CLOSE........");
			}
		}*/
		
		String url ="jdbc:sqlserver://34.233.251.129:1433;database=ProdRTFeComm;sendStringParametersAsUnicode=false";
		String username ="tayo";
		String password ="po01ro02ro30";
		
		try{
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			return DriverManager.getConnection(url, username, password);
			
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	
	
	public static Connection getEcommDatabaseConn() throws Exception{
		if(rtfEcommConnection!=null){
			try {
				if(!rtfEcommConnection.isClosed()){
					return rtfEcommConnection;
				}
			} catch (SQLException e) {
				System.out.println("..............CONNECTION FOUND CLOSE........");
			}
		}
		/*ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("zones.admin.jdbc.url");
		String username = resourceBundle.getString("zones.admin.jdbc.user.name");
		String password =resourceBundle.getString("zones.admin.jdbc.password");*/
		
		String url ="jdbc:sqlserver://10.0.0.31:1433;database=ProdRTFeComm;sendStringParametersAsUnicode=false";
		String username ="tayo";
		String password ="po01ro02ro30";
		
		try{
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			//rtfZonesConnection = DriverManager.getConnection(rtfZonesDbUrl, rtfZonesDbUserName, rtfZonesDbPassword);
			rtfEcommConnection = DriverManager.getConnection(url, username, password);
			return rtfEcommConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	
	
	
	
	public static ResultSet getResultSet(Statement statement,String sql){
		try{
			return statement.executeQuery(sql);
		}catch(SQLException sqlEx){
			return null;
		}
	}
	

	public static CallableStatement getCallableStatement(Connection connnection,String sql){
		try{
			return connnection.prepareCall(sql);
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}
		
	}
	
}
