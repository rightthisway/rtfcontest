package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.list.AbuseReportInfo;
import com.web.util.GsonUtil;
import com.zonesws.webservices.utils.DatabaseConnections;

/**
 * Servlet implementation class ManageFanClubServlet
 */

@WebServlet("/conpoolserv.json")
public class TestPooledConnectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(TestPooledConnectionServlet.class);

	public TestPooledConnectionServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException { 
		Map<Integer , AbuseReportInfo> map = null;
try {
	//Connection conn1 = DataBaseConnectionsPooled.getRtfConnection(this.getServletConfig().getServletContext());
	//Connection conn = (Connection)this.getServletConfig().getServletContext().getAttribute("rtfdatasourceConn");
//	Connection conn = DataBaseConnectionsPooled.getRtfConnection();
	
	Connection conn = DatabaseConnections.getRtfConnection();
	 map =	fetchAbuseOptionsForFanClubPosts("VIDEO" , conn) ;

	generateResponse(response, map);
					return response;
	
		}catch(	Exception e)
	{

		e.printStackTrace();

		generateResponse(response, map);
		return response;

	}

	
	//	return response;

	}

	public void generateResponse(HttpServletResponse response, Map<Integer, AbuseReportInfo> responseDTO)
			throws ServletException, IOException {
		Map<String, Map<Integer, AbuseReportInfo>> map = new HashMap<String, Map<Integer, AbuseReportInfo>>();
		map.put("fanClubDTO", responseDTO);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

	public static Map<Integer, AbuseReportInfo> fetchAbuseOptionsForFanClubPosts(String abuseType , Connection conn) {
		Map<Integer, AbuseReportInfo> abuseOptionMap = null;
		String sql = " select a.id, a.abuse_text, a.abuse_info ,  a.abuse_type , a.status " + " from  "
				+ " mst_static_abuse_options a where a.status = 1 and a.abuse_type=  'FCPOST'";
		try {
			
			System.out.println(sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			abuseOptionMap = new HashMap<Integer, AbuseReportInfo>();
			while (rs.next()) {
				AbuseReportInfo abuseInfo = new AbuseReportInfo();
				Integer id = rs.getInt("id");
				abuseInfo.setId(id);
				abuseInfo.setAbuseTxt(rs.getString("abuse_text"));
				abuseInfo.setAbuseInfo(rs.getString("abuse_info"));
				abuseOptionMap.put(id, abuseInfo);
			}
			rs.close();
			stmt.close();
			conn.close();
			
			System.out.println(" -- abuseOptionMap  "  + abuseOptionMap);
			return abuseOptionMap;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return abuseOptionMap;
	}

}
