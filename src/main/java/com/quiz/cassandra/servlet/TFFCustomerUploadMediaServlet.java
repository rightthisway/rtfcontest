package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.MediaReward;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.service.CustomerMediaServices;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class TFFCustomerUploadMediaServlet.java
 * API to Add / remove favourite videos
 * 
 */

@WebServlet("/UploadMediaTFFRewards.json")
public class TFFCustomerUploadMediaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public TFFCustomerUploadMediaServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		VideoLikeInfo videoInfo = new VideoLikeInfo();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");				
		String medId = request.getParameter("mId");
		
		try {
			System.out.println("[TFF Allot points Info]  " + " [cuId] " + customerIdStr );			
			
			
			try {
				 Integer.parseInt(medId);	
			}catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid format of media Id:" + medId;
				error.setDesc(URLUtil.genericErrorMsg);
				videoInfo.setErr(error);
				videoInfo.setSts(0);
				generateResponse(response, videoInfo);
				return response;
			}
			
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = URLUtil.genericErrorMsg;
				error.setDesc(URLUtil.genericErrorMsg);
				videoInfo.setErr(error);
				videoInfo.setSts(0);

				generateResponse(response, videoInfo);
				return response;
			}
			
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				videoInfo.setErr(error);
				videoInfo.setSts(0);
				generateResponse(response, videoInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				videoInfo.setErr(error);
				videoInfo.setSts(0);
				generateResponse(response, videoInfo);
				return response;
			}			
						
			videoInfo.setmId(PollingUtil.getEmptyifNull(medId));
			videoInfo.setCuId(customer.getId());
			MediaReward mediaReward= CustomerMediaServices.allotRandomMediaRewardsToCustomer(customer,SourceType.VIDEO_UPLOAD,Integer.valueOf(medId));
			if(mediaReward == null || mediaReward.getSts() ==0) throw new Exception();
				videoInfo.setSts(mediaReward.getSts());	

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			videoInfo.setErr(error);
			videoInfo.setSts(0);
			generateResponse(response, videoInfo);
			return response;

		} 
		generateResponse(response, videoInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, VideoLikeInfo videoInfo)
			throws ServletException, IOException {
		Map<String, VideoLikeInfo> map = new HashMap<String, VideoLikeInfo>();
		map.put("VideoSharedInfo", videoInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
