package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.list.ContestGrandWinner;
import com.quiz.cassandra.list.CustomerOrder;
import com.quiz.cassandra.list.CustomerOrdersResponse;
import com.web.util.GsonUtil;
import com.zonesws.webservices.utils.TextUtil;

/**
 * Servlet implementation class GetMyOrdersServlet
 */

@WebServlet("/ListOrders.json")
public class GetMyOrdersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(GetMyOrdersServlet.class);
  
    public GetMyOrdersServlet() {
        super();       
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CustomerOrdersResponse customerOrderResponse = new CustomerOrdersResponse();
		com.zonesws.webservices.data.Error error = new com.zonesws.webservices.data.Error();
		try {
			List<CustomerOrder> activeOrders = new ArrayList<CustomerOrder>();
			List<CustomerOrder> pastOrders = new ArrayList<CustomerOrder>();
			customerOrderResponse.setCustomerOrders(activeOrders);
			customerOrderResponse.setPastOrders(pastOrders);
			List<ContestGrandWinner> grandWiners = new ArrayList<>();
			customerOrderResponse.setContestGrandWinners(grandWiners);
			customerOrderResponse.setContestTicketsConfirmDialog(TextUtil.getContestTicketsConfirmDialog());
			customerOrderResponse.setStatus(1);
		}catch(Exception e){
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Oops something went wrong. Please try search again.");
			customerOrderResponse.setError(error);
			customerOrderResponse.setStatus(0);
		}  
		generateResponse(response, customerOrderResponse);
		return response;
	}
	
	public void generateResponse(HttpServletResponse response,CustomerOrdersResponse customerOrderResponse) throws ServletException, IOException {
		Map<String, CustomerOrdersResponse> map = new HashMap<String, CustomerOrdersResponse>();
		map.put("customerOrdersResponse", customerOrderResponse);
		String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		//System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    out.print(jsondashboardInfo);
	    out.flush(); 
	}
 
}
