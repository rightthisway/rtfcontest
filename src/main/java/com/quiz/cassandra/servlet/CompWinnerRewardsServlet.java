package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContWinnerRewardsInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/CompContestWinnerRewards.json")
public class CompWinnerRewardsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(CompWinnerRewardsServlet.class);
  
    public CompWinnerRewardsServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	ContWinnerRewardsInfo contestWinnerRewards =new ContWinnerRewardsInfo();
	CassError error = new CassError();
	Date start = new Date();
	//String customerIdStr = request.getParameter("cuId");
	String contestIdStr = request.getParameter("coId");
	String resMsg = "";
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	Integer contestId = null;
	try {
		//log.info("CASS WINNER REWARDs : "+": "+contestIdStr+" INSIDE");
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			contestWinnerRewards.setErr(authError);
			contestWinnerRewards.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.COMPCONTESTWINNERREWARDS,authError.getDescription());
			return contestWinnerRewards;
		}*/
		
		/*if(TextUtil.isEmptyOrNull(customerIdStr)){
			error.setDesc("Customer Id is mandatory");
			contestWinnerRewards.setErr(error);
			contestWinnerRewards.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.COMPCONTESTWINNERREWARDS,"Customer Id is mandatory");
			return contestWinnerRewards;
		}
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			error.setDesc("Contest Id is mandatory");
			contestWinnerRewards.setErr(error);
			contestWinnerRewards.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.COMPCONTESTWINNERREWARDS,"Contest Id is mandatory");
			return contestWinnerRewards;
		}*/
		//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
		QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
		if(quizContest == null) {
			resMsg = "Contest Id is Invalid:"+contestIdStr;
			//error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			contestWinnerRewards.setErr(error);
			contestWinnerRewards.setSts(0);
			
			generateResponse(response, contestWinnerRewards);
			return response;

		}
		contestId = quizContest.getId();
		/*Double contestAPITracking = 0.0;
		if(quizContest.getProcessStatus().equals(ContestProcessStatus.STARTED)) {
			rewardsPerCustomer = QuizContestUtil.updateContestWinnersRewadPoints(Integer.parseInt(contestIdStr), Integer.parseInt(customerIdStr));	
		} else {
			QuizContestUtil.getContestWinnerRewardPoints(quizContest.getId());
		}*/
		Double rewardsPerCustomer = CassContestUtil.getContestWinnerRewardPoints(quizContest.getId());
		if(rewardsPerCustomer == null || rewardsPerCustomer <= 0.0) {
			rewardsPerCustomer = CassContestUtil.updateContestWinnersRewadPoints(quizContest);
//Fix This			
			//QuizContestUtil.refreshContestSummaryData(quizContest.getId());
		}
		
		//quizContest.setProcessStatus(ContestProcessStatus.WINNERCOMPUTED);
		//QuizContestUtil.updateQuizContest(quizContest);
		
		//ansCountInfo.setQuizContestQuestion(quizQuestion);
		contestWinnerRewards.setSts(1);
		contestWinnerRewards.setMsg("Customer Rewards Computed successfully.");
		contestWinnerRewards.setWinnerRwds(rewardsPerCustomer);
		//quizContestDetails.setQuizCustomer(quizCustomer);
		//log.info("CASS WINNER REWARDs : "+contestIdStr+" SUCCESS");
		resMsg = "Success:"+rewardsPerCustomer;
	}catch(Exception e){
		resMsg = "Error occured while Fetching Customer Reward Compute Details.";
		e.printStackTrace();
		log.info("CASS WINNER REWARDs : "+": "+contestIdStr+" EXECEPTION");
		//error.setDesc("Error occured while Fetching Customer Reward Compute Details.");
		error.setDesc(URLUtil.genericErrorMsg);
		contestWinnerRewards.setErr(error);
		contestWinnerRewards.setSts(0);
		
		generateResponse(response, contestWinnerRewards);
		return response;

	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.COMPCONTESTWINNERREWARDS, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,contestWinnerRewards.getSts(),null);
		log.info("CASS UPD REWRD COMP : "+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}

	
	generateResponse(response, contestWinnerRewards);
	return response;


}

public void generateResponse(HttpServletResponse response,ContWinnerRewardsInfo contestWinnerRewards) throws ServletException, IOException {
	Map<String, ContWinnerRewardsInfo> map = new HashMap<String, ContWinnerRewardsInfo>();
	map.put("contWinnerRewardsInfo", contestWinnerRewards);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
