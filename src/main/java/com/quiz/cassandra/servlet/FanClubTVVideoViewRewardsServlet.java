package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingVideoRewardInfo;
import com.quiz.cassandra.service.CustomerMediaServices;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class FanClubTVVideoViewRewardsServlet
 */

@WebServlet("/FanclubTvMediaWatchRwd.json")
public class FanClubTVVideoViewRewardsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingVideoRewardsServlet.class);

	public FanClubTVVideoViewRewardsServlet() {
		super();
	}

	/*
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { process(request, response);
	 * 
	 * }
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PollingVideoRewardInfo pollingVideoRewardInfo = new PollingVideoRewardInfo();
		CassError error = new CassError();	

		String vwdVidIdStr = request.getParameter("vId");
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String source = request.getParameter("source"); // TV or Fan Club Video

		try {

			ApplicationPlatForm applicationPlatForm = null;

			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;
				// error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoRewardInfo.setErr(error);
				pollingVideoRewardInfo.setSts(0);

				generateResponse(response, pollingVideoRewardInfo);
				return response;
			}

			if (source == null) {
				resMsg = "Please send valid source:" + source;
				error.setDesc("Please send valid application platform");
				// error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoRewardInfo.setErr(error);
				pollingVideoRewardInfo.setSts(0);
				System.out.println(resMsg);
				generateResponse(response, pollingVideoRewardInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				// error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoRewardInfo.setErr(error);
				pollingVideoRewardInfo.setSts(0);

				generateResponse(response, pollingVideoRewardInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				// error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoRewardInfo.setErr(error);
				pollingVideoRewardInfo.setSts(0);
				generateResponse(response, pollingVideoRewardInfo);
				return response;
			}
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				// error.setDesc(resMsg);
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoRewardInfo.setErr(error);
				pollingVideoRewardInfo.setSts(0);

				generateResponse(response, pollingVideoRewardInfo);
				return response;
			}

			if (TextUtil.isEmptyOrNull(vwdVidIdStr)) {
				resMsg = "Valid video id required";
				// error.setDesc(resMsg);
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoRewardInfo.setErr(error);
				pollingVideoRewardInfo.setSts(0);

				generateResponse(response, pollingVideoRewardInfo);
				return response;
			}

			pollingVideoRewardInfo.setCuId(customer.getId());
			pollingVideoRewardInfo.setAwsVideoId(vwdVidIdStr);
			pollingVideoRewardInfo.setSource(source);

			/*
			 * 1) check customer max limit for the day 2) check customer time interval for
			 * the day 3) get random rewards for the day 4) set message with rewards ... 5)
			 * insert rewards details for the customer 6) update sql with rewards 7) update
			 * cassandra with rewards
			 */

			try {
				
				if("FC".equals(source)) {
					PollingSQLDaoUtil.updateFanClubVideoViewCount(vwdVidIdStr);
				}
				pollingVideoRewardInfo = CustomerMediaServices.processVideoWatchRewards(customer,
						pollingVideoRewardInfo);
				if (pollingVideoRewardInfo.getSts() == 1) {
					pollingVideoRewardInfo.setMsg("Video watch rewards Credited to Account");

				} else {
					error.setDesc(URLUtil.genericErrorMsg);
					pollingVideoRewardInfo.setErr(error);
					pollingVideoRewardInfo.setSts(0);

				}

			} catch (Exception ex) {
				ex.printStackTrace();
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoRewardInfo.setErr(error);
				pollingVideoRewardInfo.setSts(0);
				// this exception can be ignored ..
			}

			resMsg = "Success:" + customerIdStr + ":POLLING:" + contestType;

		} catch (Exception e) {
			resMsg = "Error occured During Answer Validation. ";
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			pollingVideoRewardInfo.setErr(error);
			pollingVideoRewardInfo.setSts(0);
			generateResponse(response, pollingVideoRewardInfo);
			return response;

		} finally {

		}

		generateResponse(response, pollingVideoRewardInfo);
		return response;

	}

	public void generateResponse(HttpServletResponse response, PollingVideoRewardInfo pollingVideoRewardInfo)
			throws ServletException, IOException {
		Map<String, PollingVideoRewardInfo> map = new HashMap<String, PollingVideoRewardInfo>();
		map.put("pollingVideoRewardInfo", pollingVideoRewardInfo);
		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
