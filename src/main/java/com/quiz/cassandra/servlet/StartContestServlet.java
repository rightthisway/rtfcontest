package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContestValidateAnsInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestInfo;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.ContestProcessStatus;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;

/**
 * Servlet implementation class StartContestServlet
 */

@WebServlet("/UpdateStartQuizContest.json")
public class StartContestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(StartContestServlet.class);
  
    public StartContestServlet() {
        super();       
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	
	QuizContestInfo quizContestDetails =new QuizContestInfo();
	com.zonesws.webservices.data.Error error = new com.zonesws.webservices.data.Error();
	Date start = new Date();
	String resMsg = "";
	String contestIdStr = request.getParameter("contestId");
	String platForm = request.getParameter("platForm");
	String deviceType = request.getParameter("deviceType");
	Integer contestId = null;
	Integer customerId = null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			quizContestDetails.setError(authError);
			quizContestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,authError.getDescription());
			return quizContestDetails;
		}*/
		
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			resMsg = "Contest Id is mandatory : "+contestIdStr;
			error.setDescription("Contest Id is mandatory");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			
			generateResponse(response, quizContestDetails);
			return response;
		}
		/*QuizConfigSettings quizConfigSettings = QuizDAORegistry.getQuizConfigSettingsDAO().getConfigSettings();
		if(quizConfigSettings == null) {
			error.setDescription("Quiz Configuration Settings not yet declared.");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Quiz Configuration Settings not yet declared.");
			return quizContestDetails;
		}*/
		QuizContest contest = CassContestUtil.getContestByIDForStartContest(Integer.parseInt(contestIdStr));
		if(contest == null) {
			resMsg = "Quiz Contest Id is Invalid. : "+contestIdStr;
			error.setDescription("Quiz Contest Id is Invalid.");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			
			generateResponse(response, quizContestDetails);
			return response;
		}
		contestId = contest.getId();
		
		Boolean flag = CassContestUtil.refreshStartContestData(contest.getContestType());
		if(!flag) {
			resMsg = "Contest Not Yet Started.. : "+contestIdStr;
			error.setDescription("Contest Not Yet Started.");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			
			generateResponse(response, quizContestDetails);
			return response;
		}
		contest.setProcessStatus(ContestProcessStatus.STARTED);
		
		//CassContestUtil.updateQuizContest(contest);
		
		CassContestUtil.HIDE_DASHBOARD_REWARDS = true;
		CassContestUtil.hallOfFamePageMaxDataSize = 5;
		
		quizContestDetails.setStatus(1);
		quizContestDetails.setMessage("Quiz Contest Started.");
		
		resMsg = "Success : "+contestIdStr;
		
	}catch(Exception e){
		resMsg = "Error occured while Fetching Start Contest Information : "+contestIdStr;
		e.printStackTrace();
		error.setDescription("Error occured while Fetching Start Contest Information.");
		quizContestDetails.setError(error);
		quizContestDetails.setStatus(0);

		generateResponse(response, quizContestDetails);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.STARTQUIZCONTEST, resMsg, contestId,
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null,quizContestDetails.getStatus(),null);
		log.info("QUIZ START CONT : "+request.getParameter("cuId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	generateResponse(response, quizContestDetails);
	return response;


}

public void generateResponse(HttpServletResponse response,QuizContestInfo quizContestDetails) throws ServletException, IOException {
	Map<String, QuizContestInfo> map = new HashMap<String, QuizContestInfo>();
	map.put("quizContestInfo", quizContestDetails);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
