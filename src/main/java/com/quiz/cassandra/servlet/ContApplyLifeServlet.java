package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContDtls;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContestValidateAnsInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/ContApplyLife.json")
public class ContApplyLifeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ContApplyLifeServlet.class);
  
    public ContApplyLifeServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//process(request, response);
		processWithAutoCreate(request, response);
	}
	
protected HttpServletResponse processWithAutoCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	
	//System.out.println("APPLYLIFE Inside : "+request.getParameter("cuId")+" : "+ new Date());
	
	ContApplyLifeInfo applyLifeInfo =new ContApplyLifeInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String customerIdStr = request.getParameter("cuId");
	String contestIdStr = request.getParameter("coId");
	String questionNoStr = request.getParameter("qNo");
	String questionIdStr = request.getParameter("qId");
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time

	String retryCountStr = request.getParameter("rtc");
	
	Integer contestId = null;
	Integer customerId = null;
	
	Integer retryCnt=0;
	String description="";
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			resMsg = authError.getDescription();
			applyLifeInfo.setErr(authError);
			applyLifeInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
			return applyLifeInfo;
		}*/
		
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(Integer.parseInt(customerIdStr));
		if(customer == null) {
			resMsg = "Customer Id is Invalid:"+customerIdStr;
			description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			//error.setDesc("Customer Id is Invalid.");
			error.setDesc(URLUtil.genericErrorMsg);
			applyLifeInfo.setErr(error);
			applyLifeInfo.setSts(0);
			
			generateResponse(response, applyLifeInfo);
			return response;
		}
		customerId = customer.getId();
		
		//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
		QuizContest contest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
		if(contest == null){
			resMsg = "Contest Id is Invalid:"+contestIdStr;
			description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			//error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			applyLifeInfo.setErr(error);
			applyLifeInfo.setSts(0);
			
			generateResponse(response, applyLifeInfo);
			return response;
		}
		contestId = contest.getId();
		
		if(!TextUtil.isEmptyOrNull(retryCountStr)){
			try {
				retryCnt = Integer.parseInt(retryCountStr);
			} catch (Exception e) {
				e.printStackTrace();
				//description="Invalid RetryCount"+retryCountStr;
			}
		}
		Boolean isIOSDevice = true;
		try {
			if(platForm!=null && platForm.equalsIgnoreCase(ApplicationPlatForm.IOS.toString())) {
				isIOSDevice = true;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		QuizContestQuestions quizQuestion = CassContestUtil.getQuizContestQuestionById(Integer.parseInt(questionIdStr));
		if(quizQuestion == null) {
			resMsg = "Question Id is Invalid:"+questionIdStr;
			description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			//error.setDesc("Question Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			applyLifeInfo.setErr(error);
			
			if(isIOSDevice) {
				applyLifeInfo.setSts(1);
				applyLifeInfo.setIsLifeUsed(true);
				applyLifeInfo.setMsg("LifeLine Applied Successfully.");
			} else {
				applyLifeInfo.setSts(0);
			}
			
			generateResponse(response, applyLifeInfo);
			return response;
		}
		
		Integer questionNo = Integer.parseInt(questionNoStr);
		if(!quizQuestion.getQuestionSNo().equals(questionNo) || !quizQuestion.getContestId().equals(contest.getId())) {
			resMsg = "Question No or Id is Invalid:"+questionNoStr;
			description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			//error.setDesc("Question No or Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			applyLifeInfo.setErr(error);

			if(isIOSDevice) {
				applyLifeInfo.setSts(1);
				applyLifeInfo.setIsLifeUsed(true);
				applyLifeInfo.setMsg("LifeLine Applied Successfully.");
			} else {
				applyLifeInfo.setSts(0);
			}
			
			generateResponse(response, applyLifeInfo);
			return response;
		}
		if(quizQuestion.getQuestionSNo() >= contest.getNoOfQuestions()) {
			resMsg = "LifeLine can't be used for Last question:"+questionNoStr;
			description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			error.setDesc("Lifeline can't be used for Last question.");
			applyLifeInfo.setErr(error);
			
			if(isIOSDevice) {
				applyLifeInfo.setSts(1);
				applyLifeInfo.setIsLifeUsed(true);
				applyLifeInfo.setMsg("LifeLine Applied Successfully.");
			} else {
				applyLifeInfo.setSts(0);
			}
			
			generateResponse(response, applyLifeInfo);
			return response;
		}
		Integer sfLevelQNo = 0; 
		if(CassContestUtil.SuperFanLevelEnabled) {
			SuperFanContCustLevels sfContCustLevels = CassContestUtil.getSuperFanContCustLevels(customer.getId(), contest.getId());
			if(sfContCustLevels != null) {
				sfLevelQNo = sfContCustLevels.getqNo();
			}
			
			if(sfLevelQNo >= questionNo) {
				resMsg = "Lifeline can't be used SF Level questions:"+questionNoStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Question No or Id is Invalid");
				error.setDesc(URLUtil.genericErrorMsg);
				applyLifeInfo.setErr(error);

				if(isIOSDevice) {
					applyLifeInfo.setSts(1);
					applyLifeInfo.setIsLifeUsed(true);
					applyLifeInfo.setMsg("LifeLine Applied Successfully.");
				} else {
					applyLifeInfo.setSts(0);
				}
				
				generateResponse(response, applyLifeInfo);
				return response;
			}
		}
		
		
		CustContAnswers customerAnswers = null;
		/*CustContAnswers customerAnswers = CassContestUtil.getCustomerQuestionAnswers(customerId,questionNo, contestId);
		if(customerAnswers != null) {
			if(customerAnswers.getIsCrt()) {
				resMsg += "You have already selected correct Answer:"+questionNoStr+".";
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				
				applyLifeInfo.setSts(1);
				applyLifeInfo.setIsLifeUsed(true);
				applyLifeInfo.setMsg("LifeLine Applied Successfully.");
				generateResponse(response, applyLifeInfo);
				return response;
				
			} else if (customerAnswers.getIsLife()) {
				resMsg += "LifeLine already used for this Question:"+questionNoStr+".";
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				
				applyLifeInfo.setSts(1);
				applyLifeInfo.setIsLifeUsed(true);
				applyLifeInfo.setMsg("LifeLine Applied Successfully.");
				generateResponse(response, applyLifeInfo);
				return response;
			} else if (customerAnswers.getCuLife() > 0) {
				resMsg += "LifeLine already used for this contest:"+questionNoStr+".";
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			}
		}*/
		
		Boolean isAutoFlag = false;
		Integer startQno=0,endQno=0;
		CustContDtls custContDtls = CassContestUtil.getCustomerAnswers(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
		if(custContDtls != null) {
			if(custContDtls.getLqNo() > questionNo) {
				resMsg += "You have already answered Next Question:"+questionNoStr+".";
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				
				applyLifeInfo.setSts(1);
				applyLifeInfo.setIsLifeUsed(true);
				applyLifeInfo.setMsg("LifeLine Applied Successfully.");
				generateResponse(response, applyLifeInfo);
				return response;
			} else if(custContDtls.getLqNo().equals(questionNo) && (custContDtls.getIsLqCrt() || custContDtls.getIsLqLife())) {
				resMsg += "You have already selected correct Answer or Applied Life:"+questionNoStr+".";
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				
				applyLifeInfo.setSts(1);
				applyLifeInfo.setIsLifeUsed(true);
				applyLifeInfo.setMsg("LifeLine Applied Successfully.");
				generateResponse(response, applyLifeInfo);
				return response;
			} else if (custContDtls.getCuLife() > 0) {
				resMsg += "LifeLine already used for this contest:"+questionNoStr+".";
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			}
		}
		if(custContDtls != null) {
			if(questionNo - custContDtls.getLqNo() > 1) {
				startQno = custContDtls.getLqNo()+1;
				endQno = questionNo - 1;
				isAutoFlag = true;
			}
		} else if(questionNo > 1){
			startQno = 1;
			endQno = questionNo - 1;
			isAutoFlag = true;
		}
		if(isAutoFlag) {
			try {
				custContDtls = CassContestUtil.computeAutoCreateAnswers(customerId, contestId, startQno, endQno, resMsg, custContDtls);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error Occured in AUTO ANs Creation Block:"+customerIdStr+":"+contestIdStr+":"+questionNoStr+":"+new Date());
			}
		}
		
		if(customerAnswers == null ) {//|| customerAnswers.getqNo()< questionNo
			//if customer didn't answered and applied life then cumulate previous data
			Integer cuLife = 0;
			Double cuRwds = 0.0;
			/*if(customerAnswers != null) {
				cuLife = customerAnswers.getCuLife();
				cuRwds = customerAnswers.getCuRwds();
			}*/
			 customerAnswers = new CustContAnswers();
			 customerAnswers.setCuId(Integer.parseInt(customerIdStr));
			 customerAnswers.setCoId(Integer.parseInt(contestIdStr));
			 customerAnswers.setqId(Integer.parseInt(questionIdStr));
			 customerAnswers.setqNo(Integer.parseInt(questionNoStr));
			 //customerAnswers.setAnswer(answerOption);
			 customerAnswers.setIsCrt(false);
			 customerAnswers.setCrDate(new Date().getTime());
			 customerAnswers.setIsLife(false);
			 customerAnswers.setCuLife(cuLife);
			 customerAnswers.setCuRwds(cuRwds);
		}
		
		if(custContDtls != null) {
			customerAnswers.setCuRwds(custContDtls.getCuRwds());
			customerAnswers.setCuLife(custContDtls.getCuLife());
		}
/*Cache data validation blog ends */
		
		/*QuizCustomerContestAnswers lifeLineUsedcustomers = QuizDAORegistry.getQuizCustomerContestAnswersDAO().
				getLifeLineUsedContestAnswerByCustomerIdandContestId(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
				
		if(lifeLineUsedcustomers != null) {
			resMsg = "You already Used lifeline for this conteset.";
			error.setDesc("You already Used lifeline for this conteset.");
			applyLifeInfo.setErr(error);
			applyLifeInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
			return applyLifeInfo;
		}*/
		
		if(customer.getqLive() == null || customer.getqLive() <= 0) {
			resMsg += "You do not have lifeline to use:"+customer.getqLive()+".";
			
			/*error.setDesc("You do not have lifeline to use.");
			applyLifeInfo.setErr(error);
			if(isIOSDevice) {
				applyLifeInfo.setSts(1);
				applyLifeInfo.setIsLifeUsed(true);
				applyLifeInfo.setMsg("LifeLine Applied Successfully.");
			} else {
				applyLifeInfo.setSts(0);
			}
			generateResponse(response, applyLifeInfo);
			return response;*/
		}
		if(customer.getqLive() > 0) {
			customer.setqLive(customer.getqLive()-1);
			//CustomerUtil.updatedCustomerUtil(customer);
			//CassandraDAORegistry.getCassCustomerDAO().updateCustomerLives(customer.getId(), customer.getqLive(),customer);
		}
		
		Integer cumulativeLifeLineUsed = 0;
		if(customerAnswers.getCuLife() != null) {
			cumulativeLifeLineUsed = customerAnswers.getCuLife();
		}
		cumulativeLifeLineUsed = cumulativeLifeLineUsed + 1;
		
		customerAnswers.setIsLife(true);
		customerAnswers.setUpDate(new Date().getTime());
		customerAnswers.setCuLife(cumulativeLifeLineUsed);

//Fix This		
		CassandraDAORegistry.getCustContAnswersDAO().saveForApplyLife(customerAnswers);
	
		CassContestUtil.updateCustomerAnswers(customerAnswers);
		
//Fix This			
		if(custContDtls == null) {
			custContDtls = new CustContDtls();
			custContDtls.setCoId(customerAnswers.getCoId());
			custContDtls.setCuId(customerAnswers.getCuId());
		} 
		if(custContDtls.getLqNo() == null || custContDtls.getLqNo() <= customerAnswers.getqNo()) {
			custContDtls.setLqNo(customerAnswers.getqNo());
			custContDtls.setIsLqCrt(customerAnswers.getIsCrt());
			custContDtls.setIsLqLife(customerAnswers.getIsLife());	
		}
		custContDtls.setCuRwds(customerAnswers.getCuRwds());
		custContDtls.setCuLife(customerAnswers.getCuLife());
		CassandraDAORegistry.getCustContDtlsDAO().save(custContDtls);

		
		//QuizContestUtil.updateCustomerLifeLineUsage(customerAnswers);
//		CassContestUtil.updateCustomerAnswerMap(customerAnswers);
//		CassContestUtil.updateCustomerQuestionAnswerMap(customerAnswers);
		
		applyLifeInfo.setIsLifeUsed(Boolean.TRUE);
		applyLifeInfo.setSts(1);
		applyLifeInfo.setMsg("LifeLine Applied Successfully.");
		//quizContestDetails.setQuizCustomer(quizCustomer);
		
		if(resMsg.isEmpty()) {
			resMsg = "Success:";	
		}
		description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
		
		
	}catch(Exception e){
		resMsg = "Error occurred while Applying Customer Lifeline.";
		description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",rtc=" + retryCnt+ ",cuId="+customerIdStr+",coId="+contestIdStr;
		e.printStackTrace();
		//error.setDesc("Error occurred while Applying Customer Lifeline.");
		error.setDesc(URLUtil.genericErrorMsg);
		applyLifeInfo.setErr(error);
		applyLifeInfo.setSts(0);
		
		generateResponse(response, applyLifeInfo);
		return response;
		
	} finally {
		//TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.CONTAPPLYLIFE, resMsg, contestId,
			//	customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,applyLifeInfo.getSts(),questionNoStr);
		
		TrackingUtil.contestValidateAnswerAPITrackingForDeviceTimeTracking(request.getHeader("x-platform"), null, request.getHeader("deviceId"), WebServiceActionType.CONTAPPLYLIFE, resMsg, 
				questionNoStr, questionIdStr, "", contestId, customerId, start, new Date(), request.getHeader("X-Forwarded-For"),apiHitStartTimeStr,null,null,
				applyLifeInfo.getSts(),questionNoStr,retryCnt,null,description);
		
		log.info("CASS APLY LIFE : "+request.getParameter("cuId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg+":"+applyLifeInfo);
	}
	generateResponse(response, applyLifeInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,ContApplyLifeInfo applyLifeInfo) throws ServletException, IOException {
	Map<String, ContApplyLifeInfo> map = new HashMap<String, ContApplyLifeInfo>();
	map.put("contApplyLifeInfo", applyLifeInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
