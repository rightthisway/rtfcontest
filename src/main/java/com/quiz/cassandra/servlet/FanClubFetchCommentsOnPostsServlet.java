package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.FanClubPostsDTO;
import com.quiz.cassandra.service.FanClubService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class FanClubFetchCommentsOnPostsServlet.java
 * API to fetch comments for a  Fan Club Post
 * 
 */

@WebServlet("/FanClubFetchCmtonPosts.json")
public class FanClubFetchCommentsOnPostsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubFetchCommentsOnPostsServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FanClubPostsDTO fanClubPostsDTO = new FanClubPostsDTO();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");		
		String fanClubId = request.getParameter("fcId"); // Fan Club Id	
		String postId = request.getParameter("pId"); // Fan Club Post Id	
		String pNoStr = request.getParameter("pNo");
		Integer fcId=null;
		Integer pId= null;
		
		
		try {
			System.out.println("[Fan Club Post comment fetch]  " + " [cuId] " + customerIdStr  + "[fanClubId]" + fanClubId );			

		
			
			try {
				fcId = Integer.parseInt(fanClubId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Fan Club Id:" + fanClubId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}
			try {
				pId = Integer.parseInt(postId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Post Id:" + postId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}
			
			
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);

				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			fanClubPostsDTO.setpId(pId);
			fanClubPostsDTO.setFcId(fcId);			
			fanClubPostsDTO.setCuId(customerId);			
			FanClubService.fetchFanClubCommentsOnPosts(fanClubPostsDTO,pNoStr);
			if(fanClubPostsDTO.getSts() == 1) {
				fanClubPostsDTO.setMsg("SUCCESS");
				if(fanClubPostsDTO.getcLst() == null || fanClubPostsDTO.getcLst().size() == 0 ) 
						{
							fanClubPostsDTO.setMsg(PollingUtil.NO_FANCLUB_POSTS_COMMENTS_FOUND);
						}
								
			}
			else {
				fanClubPostsDTO.setSts(0);
				fanClubPostsDTO.setMsg(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			fanClubPostsDTO.setErr(error);
			fanClubPostsDTO.setSts(0);
			generateResponse(response, fanClubPostsDTO);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, fanClubPostsDTO);
		return response;
	}

	public void generateResponse(HttpServletResponse response, FanClubPostsDTO fanClubPostsDTO)
			throws ServletException, IOException {
		
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Map<String, FanClubPostsDTO> map = new HashMap<String, FanClubPostsDTO>();
		map.put("FanClubPostsDTO", fanClubPostsDTO);
		String jsonfanClubPostsDTO = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonfanClubPostsDTO);
		out.flush();
	}

}
