package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CommonRespInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/PostMigrationStatsUpdate.json")
public class PostMigrationStatsUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PostMigrationStatsUpdateServlet.class);
  
    public PostMigrationStatsUpdateServlet() {
        super();       
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	
	CommonRespInfo commonRespInfo =new CommonRespInfo();
	CassError error = new CassError();
	Date start = new Date();
	String contestIdStr = request.getParameter("coId");
	String resMsg = "";
	Integer contestId = null;
	Integer customerId = null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			resMsg = authError.getDescription();
			applyLifeInfo.setErr(authError);
			applyLifeInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
			return applyLifeInfo;
		}*/
		
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			resMsg = "Contest Id is mandatory : "+contestIdStr;
			//error.setDesc("Contest Id is mandatory");
			error.setDesc(URLUtil.genericErrorMsg);
			commonRespInfo.setErr(error);
			commonRespInfo.setSts(0);
			
			generateResponse(response, commonRespInfo);
			return response;
		}
		QuizContest contest = CassContestUtil.getContestByIDForStartContest(Integer.parseInt(contestIdStr));
		if(contest == null) {
			resMsg = "Quiz Contest Id is Invalid. : "+contestIdStr;
			//error.setDesc("Quiz Contest Id is Invalid.");
			error.setDesc(URLUtil.genericErrorMsg);
			commonRespInfo.setErr(error);
			commonRespInfo.setSts(0);
			
			generateResponse(response, commonRespInfo);
			return response;
		}
		contestId = contest.getId();
		
		CassContestUtil.updatePostMigrationStats();
		
		
		commonRespInfo.setSts(1);
		commonRespInfo.setMsg("Post Migration Stats Updated Successfully.");
		//quizContestDetails.setQuizCustomer(quizCustomer);
		resMsg = "Success:";
	}catch(Exception e){
		resMsg = "Error occured while updating Post Migration Stats.";
		e.printStackTrace();
		//error.setDesc("Error occured while updating Post Migration Stats.");
		error.setDesc(URLUtil.genericErrorMsg);
		commonRespInfo.setErr(error);
		commonRespInfo.setSts(0);
		
		generateResponse(response, commonRespInfo);
		return response;
		
	} finally {
		TrackingUtil.contestAPITracking(null, null, request.getHeader("deviceId"), WebServiceActionType.POSTMIGRATIONSTATSUPDATE, resMsg, contestId,
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null,commonRespInfo.getSts(),null);
		log.info("LOAD APP VALUES: "+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	generateResponse(response, commonRespInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,CommonRespInfo commonRespInfo) throws ServletException, IOException {
	Map<String, CommonRespInfo> map = new HashMap<String, CommonRespInfo>();
	//map.put("CommonRespInfo", commonRespInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(commonRespInfo);
	System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
