package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.data.ContestPasswordAuth;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContestPasswordAuthInfo;
import com.quiz.cassandra.list.ContestPasswordAuthInfo;
import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class JoinContestServlet
 */

@WebServlet("/ContPasswordAuth.json")
public class ContestPasswordAuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ContestPasswordAuthServlet.class);
  
    public ContestPasswordAuthServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	
	//System.out.println("Join Inside : "+request.getParameter("cuId")+" : "+ new Date());
	
	ContestPasswordAuthInfo contPwdAuthInfo =new ContestPasswordAuthInfo();
	CassError error = new CassError();
	Date start = new Date();
	String customerIdStr = request.getParameter("cuId");
	String contestIdStr = request.getParameter("coId");
	String password = request.getParameter("pwd");
	String platForm = request.getParameter("pfm");
	//String productTypeStr = request.getParameter("productType");
	String loginIp = request.getParameter("lIp");
	Integer customerId = null;
	String resMsg = "";
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	Integer contestId=null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			contPwdAuthInfo.setErr(authError);
			contPwdAuthInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.JOINCONTEST,authError.getDescription());
			return contPwdAuthInfo;
		}*/
		
		
		 
		ApplicationPlatForm applicationPlatForm=null;
		if(!TextUtil.isEmptyOrNull(platForm)){
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				resMsg = "Please send valid application platform:"+platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				contPwdAuthInfo.setErr(error);
				contPwdAuthInfo.setSts(0);
			
				generateResponse(response, contPwdAuthInfo);
				return response;
			}
		}
		if(TextUtil.isEmptyOrNull(password)){
			
			resMsg = "Please send valid password:"+password;
			//error.setDesc("Please send valid application platform");
			error.setDesc(URLUtil.genericErrorMsg);
			contPwdAuthInfo.setErr(error);
			contPwdAuthInfo.setSts(0);
		
			generateResponse(response, contPwdAuthInfo);
			return response;
		}
		
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Invalid Customer Id:"+customerIdStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			contPwdAuthInfo.setErr(error);
			contPwdAuthInfo.setSts(0);
			
			generateResponse(response, contPwdAuthInfo);
			return response;
		}
		
		try{
			contestId = Integer.parseInt(contestIdStr.trim());
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Invalid Contest Id:"+contestIdStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			contPwdAuthInfo.setErr(error);
			contPwdAuthInfo.setSts(0);
			
			generateResponse(response, contPwdAuthInfo);
			return response;
		}
		QuizContest contest = CassContestUtil.getCurrentContestByContestId(contestId);
		if(contest == null){
			resMsg = "Contest Id is Invalid:"+contestIdStr;
			//description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			//error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			contPwdAuthInfo.setErr(error);
			contPwdAuthInfo.setSts(0);
			
			generateResponse(response, contPwdAuthInfo);
			return response;
		}
		
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
		//CassCustomer customer = CassContestUtil.getCasscustomerFromMap(customerId);
		if(customer == null) {
			resMsg = "Customer Id is Invalid:"+customerIdStr;
			//error.setDesc("Customer Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			contPwdAuthInfo.setErr(error);
			contPwdAuthInfo.setSts(0);
			
			generateResponse(response, contPwdAuthInfo);
			return response;
		}
		
		if(contest.getContestMode() == null || !contest.getContestMode().equals("PASSWORD")) {
			resMsg = "This Contest is not Password Proctect:"+contestIdStr;
			//error.setDesc("Customer Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			contPwdAuthInfo.setErr(error);
			contPwdAuthInfo.setSts(0);
			
			generateResponse(response, contPwdAuthInfo);
			return response;
		}
		if(contest.getContestPwd() != null && !contest.getContestPwd().equals(password)) {
			resMsg = "The password that you've entered is incorrect.";
			//error.setDesc("Customer Id is Invalid");
			error.setDesc(resMsg);
			contPwdAuthInfo.setErr(error);
			contPwdAuthInfo.setSts(0);
			
			generateResponse(response, contPwdAuthInfo);
			return response;
		}
		ContestPasswordAuth contPwdAuth = new ContestPasswordAuth();
		contPwdAuth.setCuId(customerId);
		contPwdAuth.setCoId(contestId);
		contPwdAuth.setPwd(password);
		contPwdAuth.setIsAuth(Boolean.TRUE);
		contPwdAuth.setCrDate(new Date().getTime());
		contPwdAuth.setUpDate(new Date().getTime());
		CassandraDAORegistry.getContestPasswordAuthDAO().save(contPwdAuth);
		
		contPwdAuthInfo.setSts(1);
		contPwdAuthInfo.setMsg("You are Authorized successfully.");
		resMsg = "Success:"+customerIdStr+":"+contest.getId();
		
	}catch(Exception e){
		resMsg = "Error occurred while contest password auth.";
		e.printStackTrace();
		//error.setDesc("Error occurred while Fetching Join contest Details.");
		error.setDesc(URLUtil.genericErrorMsg);
		contPwdAuthInfo.setErr(error);
		contPwdAuthInfo.setSts(0);
		//TrackingUtils.contestAPITracking(request, WebServiceActionType.JOINCONTEST,"Error occured while Fetching Join contest Details.");

		generateResponse(response, contPwdAuthInfo);
		return response;
		
	} finally {
		TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.CONTPASSWORDAUTH, resMsg, contestId, 
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,contPwdAuthInfo.getSts(),null);
		log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+contPwdAuthInfo);
	}
	
	
	generateResponse(response, contPwdAuthInfo);
	return response;

	
}

public void generateResponse(HttpServletResponse response,ContestPasswordAuthInfo contPwdAuthInfo) throws ServletException, IOException {
	Map<String, ContestPasswordAuthInfo> map = new HashMap<String, ContestPasswordAuthInfo>();
	map.put("contestPasswordAuthInfo", contPwdAuthInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
