package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.service.CustomerMediaServices;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerAddVidToMyFavServlet.java
 * API to Add / remove favourite videos
 * 
 */

@WebServlet("/ManageMyFavVid.json")
public class CustomerAddVidToMyFavServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public CustomerAddVidToMyFavServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		VideoLikeInfo videoLikeInfo = new VideoLikeInfo();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		String videoId = request.getParameter("vId");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		String isAdd = request.getParameter("isAdd"); // Y to Add , N to Remove.
		try {

			System.out.println("[videoId Info]  " + " [cuId] " + customerIdStr + "  [videoId]  " + videoId);

			if (TextUtil.isEmptyOrNull(videoId)) {
				resMsg = "Please send valid video Id:" + videoId;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);

				generateResponse(response, videoLikeInfo);
				return response;
			}
			Integer vidId = null;
			try {
				vidId = Integer.parseInt(videoId.trim());
			} catch (Exception ex) {
				resMsg = "Please send valid video Id:" + videoId;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);
				generateResponse(response, videoLikeInfo);
				return response;
			}

			if (TextUtil.isEmptyOrNull(videoId)) {
				resMsg = "Please send valid video Id:" + videoId;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);

				generateResponse(response, videoLikeInfo);
				return response;
			}

			ApplicationPlatForm applicationPlatForm = null;

			if (TextUtil.isEmptyOrNull(isAdd)) {
				resMsg = "Please send NON EMPTY Favourite boolean flag: " + isAdd;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);

				generateResponse(response, videoLikeInfo);
				return response;
			}
			if (!(PollingUtil.YES.equals(isAdd) || PollingUtil.NO.equals(isAdd))) {
				resMsg = "Please send  Favourite boolean flag in Y  || N " + isAdd;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);

				generateResponse(response, videoLikeInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);

				generateResponse(response, videoLikeInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);
				generateResponse(response, videoLikeInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);
				generateResponse(response, videoLikeInfo);
				return response;
			}

			videoLikeInfo.setCuId(customer.getId());
			videoLikeInfo.setVidId(vidId);
			videoLikeInfo.setIsFavAdd(isAdd);
			videoLikeInfo = updateVideoFavouriteResponse(videoLikeInfo, customer);

		} catch (Exception e) {
			resMsg = "Error occured while Updating Video Favourites ";
			e.printStackTrace();
			error.setDesc("Error occured while Updating Video Favourites");
			videoLikeInfo.setErr(error);
			videoLikeInfo.setSts(0);
			generateResponse(response, videoLikeInfo);
			return response;

		} finally {
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, videoLikeInfo);
		return response;
	}

	private VideoLikeInfo updateVideoFavouriteResponse(VideoLikeInfo videoLikeInfo, CassCustomer cassCustomer)
			throws Exception {
		videoLikeInfo = CustomerMediaServices.processVideoFavourite(videoLikeInfo, cassCustomer);

		return videoLikeInfo;
	}

	public void generateResponse(HttpServletResponse response, VideoLikeInfo videoLikeInfo)
			throws ServletException, IOException {
		Map<String, VideoLikeInfo> map = new HashMap<String, VideoLikeInfo>();
		map.put("VideoLikeInfo", videoLikeInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
