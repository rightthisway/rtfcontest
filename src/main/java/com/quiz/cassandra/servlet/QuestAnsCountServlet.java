package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.list.AnswerCountInfo;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.LiveCustDetails;
import com.quiz.cassandra.thread.MegaJackpotThread;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.ContestJackpotType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/GetQuestAnsCount.json")
public class QuestAnsCountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(QuestAnsCountServlet.class);
  
    public QuestAnsCountServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	AnswerCountInfo ansCountInfo =new AnswerCountInfo();
	CassError error = new CassError();
	Date start = new Date();
	Date temp = new Date();
	//String customerIdStr = request.getParameter("cuId");
	String contestIdStr = request.getParameter("coId");
	String questionNoStr = request.getParameter("qNo");
	String questionIdStr = request.getParameter("qId");
	//String summaryTypeStr = request.getParameter("type");
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	String isCustList = request.getParameter("cuList");
	String resMsg = "";
	Integer contestId = null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			ansCountInfo.setErr(authError);
			ansCountInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,authError.getDescription());
			return ansCountInfo;
		}*/
		
		//log.info("Inside Get Questions before contest fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
		temp = new Date();
		//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
		QuizContest contest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
		if(contest == null){
			resMsg = "Contest Id is Invalid:"+contestIdStr;
		//	error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			ansCountInfo.setErr(error);
			ansCountInfo.setSts(0);
			
			generateResponse(response, ansCountInfo);
			return response;
		}
		contestId = contest.getId();
		
		/*QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(Integer.parseInt(questionIdStr));
		if(quizQuestion == null) {
			error.setDesc("Question Id is Invalid");
			ansCountInfo.setErr(error);
			ansCountInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,"Question Id is Invalid");
			return ansCountInfo;
		}*/
//////		
		//log.info("Inside Get Questions after contest fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
		temp = new Date();
		//QuizContestQuestions quizQuestion = CassContestUtil.getContestCurrentQuestions(contest.getId());
		QuizContestQuestions quizQuestion = CassContestUtil.getQuizContestQuestionById(Integer.parseInt(questionIdStr));
		if(quizQuestion == null || !quizQuestion.getId().equals(Integer.parseInt(questionIdStr))) {
			resMsg = "Question Id is Invalid:"+questionIdStr;
			//error.setDesc("Question Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			ansCountInfo.setErr(error);
			ansCountInfo.setSts(0);

			generateResponse(response, ansCountInfo);
			return response;
		}
		quizQuestion.setIsAnswerCountComputed(true);
		CassContestUtil.updateContestCurrentQuestions(quizQuestion);
		
		ansCountInfo.setcAnswer(quizQuestion.getAnswer());
		ansCountInfo.setQuRwds(quizQuestion.getQuestionRewards());
		
		//log.info("Inside Get Questions before count fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
		temp = new Date();
		ansCountInfo = CassContestUtil.getQuestAnsCount(contest.getId(), quizQuestion.getId(),quizQuestion.getQuestionSNo(), ansCountInfo,isCustList);
		
		/*if(isCustList != null && isCustList.equals("Y")) {
			List<LiveCustDetails> custList = CassContestUtil.getQuestionAnsweredRandomcustomersByquestionNo(contest.getId(), quizQuestion.getId(), quizQuestion.getQuestionSNo());
			ansCountInfo.setCustList(custList);
		}*/
		
		System.out.println("MEGA JACKPOT: CID:"+contest.getId()+", ENABLED: "+contest.getContestJackpotType()+", QNO: "+quizQuestion.getQuestionSNo()+", LQNO: "+contest.getNoOfQuestions());
		log.info("MEGA JACKPOT: CID:"+contest.getId()+", ENABLED: "+contest.getContestJackpotType()+", QNO: "+quizQuestion.getQuestionSNo()+", LQNO: "+contest.getNoOfQuestions());
		try {
			if(null != contest.getContestJackpotType() && contest.getContestJackpotType().equals(ContestJackpotType.MEGA) && quizQuestion.getQuestionSNo() < contest.getNoOfQuestions()) {
				Boolean questionProcessed = MegaJackpotThread.getProcessedQuestionMap(contestId, quizQuestion.getId());
				System.out.println("MEGA JACKPOT: questionProcessed: "+questionProcessed);
				if(null == questionProcessed ) {
					List<CassContestWinners> grandWinners = CassContestUtil.getContestMegaJackpotGrandWinners(contest.getId(), quizQuestion.getId());
					if(null == grandWinners || grandWinners.isEmpty() || grandWinners.size() <= 0) {
						System.out.println("MEGA JACKPOT: THREAD Started: "+grandWinners.size());
						MegaJackpotThread  megaJackpotThread = new MegaJackpotThread(contest, quizQuestion, request);
						Thread t = new Thread(megaJackpotThread);
						t.start();
					}else {
						System.out.println("MEGA JACKPOT: grandWinners: "+grandWinners.size());
					}
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		/*List<Object[]> result = QuizDAORegistry.getQuizQueryManagerDAO().getContestAnswersCountByQuestionId(quizQuestion.getId());
		if(result != null) {
			for (Object[] object : result) {
				String answer = (String)object[1];
				Integer count = (Integer)object[0];
				if(answer != null) {
					if(answer.equals("A")) {
						ansCountInfo.setaCount(count);
					} else if(answer.equals("B")) {
						ansCountInfo.setbCount(count);
					} else if(answer.equals("C")) {
						ansCountInfo.setcCount(count);
					} else if(answer.equals("D")) {
						ansCountInfo.setOptionDCount(count);
					}
				}
			}
		}*/
		//log.info("Inside Get Questions after count fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
		
		resMsg = "Success:"+questionIdStr;
		//ansCountInfo.setQuizContestQuestion(quizQuestion);
		ansCountInfo.setSts(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);
	}catch(Exception e){
		resMsg = "Error occured while Fetching Answer Count Details.";
		e.printStackTrace();
		//error.setDesc("Error occured while Fetching Answer Count Details.");
		error.setDesc(URLUtil.genericErrorMsg);
		ansCountInfo.setErr(error);
		ansCountInfo.setSts(0);

		generateResponse(response, ansCountInfo);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETQUESTANSCOUNT, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,ansCountInfo.getSts(),questionNoStr);
		log.info("CASS ANS COUNT : "+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
	}
	
	generateResponse(response, ansCountInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,AnswerCountInfo ansCountInfo) throws ServletException, IOException {
	Map<String, AnswerCountInfo> map = new HashMap<String, AnswerCountInfo>();
	map.put("answerCountInfo", ansCountInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
