package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CommentsDTO;
import com.quiz.cassandra.service.CustomerCommentService;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerMediaCommentsFetchServlet.java
 * API to report abuse on videos / commentsDTO
 * 
 */

@WebServlet("/CustMediaCmtsFetch.json")
public class CustomerMediaCommentsFetchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public CustomerMediaCommentsFetchServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		CommentsDTO commentsDTO = new CommentsDTO();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");		
		
		String srcId = request.getParameter("srcId"); // Video  Id		
		Integer sourceId = null;
		
		
		
		try {
			System.out.println("[Cust Media FETCH Info]  " + " [cuId] " + customerIdStr );			

			
			
			try {
				sourceId = Integer.parseInt(srcId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid comment or video  Id:" + srcId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				commentsDTO.setErr(error);
				commentsDTO.setSts(0);
				generateResponse(response, commentsDTO);
				return response;
			}		
			
			
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				commentsDTO.setErr(error);
				commentsDTO.setSts(0);

				generateResponse(response, commentsDTO);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				commentsDTO.setErr(error);
				commentsDTO.setSts(0);
				generateResponse(response, commentsDTO);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				commentsDTO.setErr(error);
				commentsDTO.setSts(0);
				generateResponse(response, commentsDTO);
				return response;
			}

			
			commentsDTO.setSrcId(sourceId);			
			commentsDTO.setCuId(customer.getId());
			
			CustomerCommentService.fetchMediaComments(commentsDTO);
			
			if(commentsDTO.getSts() == 1) {
				commentsDTO.setMsg("SUCCESS");
				
			}
			else {
				commentsDTO.setSts(0);
				commentsDTO.setMsg(URLUtil.genericErrorMsg);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			commentsDTO.setErr(error);
			commentsDTO.setSts(0);
			generateResponse(response, commentsDTO);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, commentsDTO);
		return response;
	}

	public void generateResponse(HttpServletResponse response, CommentsDTO commentsDTO)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Map<String, CommentsDTO> map = new HashMap<String, CommentsDTO>();
		map.put("CommentsDTO", commentsDTO);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
