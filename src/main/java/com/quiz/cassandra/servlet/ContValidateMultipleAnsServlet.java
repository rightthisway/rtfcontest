package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContDtls;
import com.quiz.cassandra.data.ValidateAnsReqDtls;
import com.quiz.cassandra.data.ValidateAnsRequestInfo;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContestValidateAnsInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ExitContestServlet
 */

@WebServlet("/ContValidateMultipleAns.json")
public class ContValidateMultipleAnsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ContValidateMultipleAnsServlet.class);
  
    public ContValidateMultipleAnsServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	
	//System.out.println("VALIDANS Inside : "+request.getParameter("cuId")+" : "+ new Date());
	
	ContestValidateAnsInfo quizAnswerInfo =new ContestValidateAnsInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String customerIdStr = request.getParameter("cuId");
	String contestIdStr = request.getParameter("coId");
	//String questionNoStr = request.getParameter("qNo");
	//String questionIdStr = request.getParameter("qId");
	//String answerOption = request.getParameter("ans");
	String platform = request.getParameter("pfm");
	//String fbTimeStr = request.getParameter("fbctm");// - forebase callback time
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	String netspeedStr = request.getParameter("netspd");// - mobile net speed
	String ansJsonStr = request.getParameter("ansJson");
	Integer customerId = null,contestId = null;
	
	
	String description="";
	String questionNoStr="", questionIdStr="", answerOption="",fbTimeStr="";
	Integer retryCnt=0,ansCount=0;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			resMsg = authError.getDescription();
			quizAnswerInfo.setErr(authError);
			quizAnswerInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg);
			return quizAnswerInfo;
		}*/
		/*if(customerIdStr != null && customerIdStr.equals("0")) {
			customerIdStr = ""+CassContestUtil.getRandomCustomerIdForTest();
		}*/
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Customer Id:"+customerIdStr;
			description = ansJsonStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);


			generateResponse(response, quizAnswerInfo);
			return response;
		}
		try{
			contestId = Integer.parseInt(contestIdStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Contest Id:"+contestIdStr;
			description = ansJsonStr;
			//error.setDesc("Invalid Contest Id");
			error.setDesc(URLUtil.genericErrorMsg);
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			generateResponse(response, quizAnswerInfo);
			return response;
		}
		//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
		QuizContest contest = CassContestUtil.getCurrentContestByContestId(contestId);
		if(contest == null){
			resMsg = "Contest Id is Invalid:"+contestIdStr;
			description = ansJsonStr;
			//error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			
			generateResponse(response, quizAnswerInfo);
			return response;
		}
		//CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
		CassCustomer customer = CassContestUtil.getCasscustomerFromMap(customerId);
		if(customer == null){
			resMsg = "Customer Id is Invalid:"+customerIdStr;
			description = ansJsonStr;
			//error.setDesc("Custoemr Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			generateResponse(response, quizAnswerInfo);
			return response;
		}
		/*Integer questionId = null;
		try{
			questionId = Integer.parseInt(questionIdStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Question Id:"+questionIdStr;
			error.setDesc("Invalid Question Id");
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			generateResponse(response, quizAnswerInfo);
			return response;
		}
		Integer questionNo = null;
		try{
			questionNo = Integer.parseInt(questionNoStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Question No:"+questionNoStr;
			error.setDesc("Invalid Question No");
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			generateResponse(response, quizAnswerInfo);
			return response;
		}*/
		if(TextUtil.isEmptyOrNull(ansJsonStr)){
			resMsg = "Json String is Empty:"+ansJsonStr;
			description = ansJsonStr;
			//error.setDesc("Json String is Empty:");
			error.setDesc(URLUtil.genericErrorMsg);
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			generateResponse(response, quizAnswerInfo);
			return response;
		}
		ValidateAnsRequestInfo validAnsReqInfo = null;
		try {
			validAnsReqInfo = GsonUtil.getJasksonObjMapper().readValue(ansJsonStr, ValidateAnsRequestInfo.class);
		
		} catch (Exception e) {
			e.printStackTrace();
			
			resMsg = "Invalid json String:"+ansJsonStr;
			description = ansJsonStr;
			//error.setDesc("Invalid json String");
			error.setDesc(URLUtil.genericErrorMsg);
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			generateResponse(response, quizAnswerInfo);
			return response;
		}
		List<ValidateAnsReqDtls> validReqDetl =  validAnsReqInfo.getAnsDtls();
		if(validReqDetl.isEmpty()) {
			
			resMsg = "Answer Details is empty:"+ansJsonStr;
			description = ansJsonStr;
			//error.setDesc("Answer Details is empty:");
			error.setDesc(URLUtil.genericErrorMsg);
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			generateResponse(response, quizAnswerInfo);
			return response;
		}
		ansCount = validReqDetl.size();
		
		String ansResMsg = "";
		Integer processedAnsCnt = 0;
		for (ValidateAnsReqDtls validateAnsReqDtls : validReqDetl) {
			
			try {
				questionNoStr=""+validateAnsReqDtls.getqNo();
				questionIdStr=""+validateAnsReqDtls.getqId();
				answerOption=validateAnsReqDtls.getAns();
				fbTimeStr=validateAnsReqDtls.getFbctm();
				retryCnt = validateAnsReqDtls.getRtc();
						
				
				QuizContestQuestions quizQuestion = CassContestUtil.getQuizContestQuestionById(validateAnsReqDtls.getqId());
				if(quizQuestion == null) {
					ansResMsg += "Question Id is Invalid:"+validateAnsReqDtls.toString();
					continue;
				}
				if(!quizQuestion.getQuestionSNo().equals(validateAnsReqDtls.getqNo())) {
					ansResMsg += "Question No or Id is Invalid:"+validateAnsReqDtls.toString();
				}
				//CustContAnswers lastAnswer = CassContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
				CustContDtls custContDtls = CassContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
				if(custContDtls != null) {
					if(custContDtls.getLqNo().equals(quizQuestion.getQuestionSNo())) {
						ansResMsg += "Answer Already Selected:"+validateAnsReqDtls.toString();
						continue;
						
					} else if(quizQuestion.getQuestionSNo() != (custContDtls.getLqNo()+1)) {
						ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";
					} else if (custContDtls.getIsLqCrt() == null|| !custContDtls.getIsLqCrt() && (null == custContDtls.getIsLqLife() || !custContDtls.getIsLqLife())) {
						ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";;
					}
				} else if(quizQuestion.getQuestionSNo() > 1) {
					ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";
				}
				
				/*if(lastAnswer != null && lastAnswer.getqId().equals(validateAnsReqDtls.getqId())) {
					ansResMsg += "Answer Already Selected:"+validateAnsReqDtls.toString();
				}*/
				/*CustContAnswers questionAns = CassContestUtil.getCustomerQuestionAnswers(customer.getId(),validateAnsReqDtls.getqNo(), contest.getId());
				if(questionAns != null) {
					ansResMsg += "Answere Already selected:qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";
					continue;
				}
				if(lastAnswer != null) {
					if(lastAnswer.getqId().equals(validateAnsReqDtls.getqId())) {
						ansResMsg += "You have already answered for this question:"+validateAnsReqDtls.toString();
					} else if(validateAnsReqDtls.getqNo() != (lastAnswer.getqNo()+1)) {
						ansResMsg += "You didn't answer last question:"+validateAnsReqDtls.toString();
					} else if (lastAnswer.getIsCrt() == null|| !lastAnswer.getIsCrt() && (null == lastAnswer.getIsLife() || !lastAnswer.getIsLife())) {
						ansResMsg += "You didn't answer last question:"+validateAnsReqDtls.toString();
					}
				} else if(quizQuestion.getQuestionSNo() > 1) {
					ansResMsg += "You didn't answer last question:"+validateAnsReqDtls.toString();
				}*/
				
				/*if(quizQuestion.getQuestionSNo() > 1) {
					if(lastAnswer == null || validateAnsReqDtls.getqNo() != (lastAnswer.getqNo()+1)) {
						ansResMsg += "You didn't answer last question:"+validateAnsReqDtls.toString();
					}else if(null != lastAnswer) {
						if((null != lastAnswer.getIsCrt() && lastAnswer.getIsCrt()) || (null != lastAnswer.getIsLife()
								&& lastAnswer.getIsLife())) {
						}else {
							ansResMsg += "You didn't answer last question:"+validateAnsReqDtls.toString();
						}
					}
				} else {
					if(lastAnswer != null) {
						ansResMsg += "You have already answered for this question:"+validateAnsReqDtls.toString();
					}
				}*/
				
				Double cumulativeRewards = 0.0;
				Integer cumulativeLifeLineUsed = 0;
				if(custContDtls != null) {
					if(custContDtls.getCuRwds() != null) {
						cumulativeRewards = custContDtls.getCuRwds();
					}
					if(custContDtls.getCuLife() != null) {
						cumulativeLifeLineUsed = custContDtls.getCuLife();
					}
				}
				
				Boolean isCrtAns = false;
				if(quizQuestion.getAnswer().equals(validateAnsReqDtls.getAns())) {
					quizAnswerInfo.setIsCrt(Boolean.TRUE);
					isCrtAns = true;
					
					if(contest.getNoOfQuestions().equals(quizQuestion.getQuestionSNo())) {
						CassContestUtil.updateContestWinners(contest.getId(), customerId);
					}
				}
				CustContAnswers custContAns = new CustContAnswers();
				custContAns.setCuId(customerId);
				custContAns.setCoId(contestId);
				custContAns.setqId(validateAnsReqDtls.getqId());
				custContAns.setqNo(validateAnsReqDtls.getqNo());
				custContAns.setAns(validateAnsReqDtls.getAns());
				custContAns.setIsCrt(isCrtAns);
				custContAns.setCrDate(new Date().getTime());
				custContAns.setIsLife(false);
				custContAns.setFbCallbackTime(validateAnsReqDtls.getFbctm());
				custContAns.setAnswerTime(validateAnsReqDtls.getAnstm());
				custContAns.setRetryCount(validateAnsReqDtls.getRtc());
				custContAns.setIsAutoCreated(Boolean.FALSE);
			
				if(isCrtAns) {
					custContAns.setaRwds(quizQuestion.getQuestionRewards());
					if(quizQuestion.getQuestionRewards() != null) {
						cumulativeRewards = cumulativeRewards + quizQuestion.getQuestionRewards();	
					}
				}
				custContAns.setCuRwds(cumulativeRewards);
				custContAns.setCuLife(cumulativeLifeLineUsed);
	
				CassandraDAORegistry.getCustContAnswersDAO().save(custContAns);
				
				CassContestUtil.updateCustomerAnswers(custContAns);	
	
//Fix This			
				if(custContDtls == null) {
					custContDtls = new CustContDtls();
					custContDtls.setCoId(custContAns.getCoId());
					custContDtls.setCuId(custContAns.getCuId());
				} 
				if(custContDtls.getLqNo() == null || custContDtls.getLqNo() <= custContAns.getqNo()) {
					custContDtls.setLqNo(custContAns.getqNo());
					custContDtls.setIsLqCrt(custContAns.getIsCrt());
					custContDtls.setIsLqLife(custContAns.getIsLife());	
				}
				custContDtls.setCuRwds(custContAns.getCuRwds());
				custContDtls.setCuLife(custContAns.getCuLife());
				CassandraDAORegistry.getCustContDtlsDAO().save(custContDtls);
				
				
				//CassContestUtil.updateCustomerAnswers(custContAns);
				processedAnsCnt++;
			} catch (Exception e) {
				e.printStackTrace();
				ansResMsg += "Error occured in ans:"+validateAnsReqDtls.toString();
			}
		}
			
		if(ansResMsg.isEmpty()) {
			resMsg = "Success:"+ansCount+":"+processedAnsCnt+":";
			description = "Success:"+ansCount+":"+processedAnsCnt+":";
		} else {
			resMsg = ""+ansCount+":"+processedAnsCnt+":";
			description = ansCount+":"+processedAnsCnt+":"+ansResMsg;
			
		}
		
		//quizAnswerInfo.setQuizContestQuestion(quizQuestion);
		quizAnswerInfo.setSts(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);
		//resMsg = "Success:";//questionNoStr+":"+questionIdStr+":"+answerOption;
		
	}catch(Exception e){
		e.printStackTrace();
		resMsg = "Error occured while Fetching Validate Answer Info."+ansJsonStr;
		description = "Common Error occured :"+ansJsonStr;
		//error.setDesc("Error occured while Fetching Validate Answer Info.");
		error.setDesc(URLUtil.genericErrorMsg);
		quizAnswerInfo.setErr(error);
		quizAnswerInfo.setSts(0);
		
		generateResponse(response, quizAnswerInfo);
		return response;
		
	} finally {
		String deviceInfo = netspeedStr;
		TrackingUtil.contestValidateAnswerAPITrackingForDeviceTimeTracking(request.getHeader("x-platform"), null, request.getHeader("deviceId"), WebServiceActionType.CONTVALIDATEMULTIANS, resMsg, 
				questionNoStr, questionIdStr, answerOption, contestId, customerId, start, new Date(), request.getHeader("X-Forwarded-For"),apiHitStartTimeStr,fbTimeStr,deviceInfo,
				quizAnswerInfo.getSts(),questionNoStr,retryCnt,ansCount,description);
		System.out.println("CASS VALID ANSWER  : "+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :ans: "+answerOption+" :apitime: "+apiHitStartTimeStr+" :fbtime: "+fbTimeStr);
		//log.info("CASS VALID ANSWER IOS: QNO:"+questionNoStr+" : "+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :ans: "+answerOption+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+quizAnswerInfo);
	}
	
	generateResponse(response, quizAnswerInfo);
	return response;

}

public void generateResponse(HttpServletResponse response,ContestValidateAnsInfo quizAnswerInfo) throws ServletException, IOException {
	Map<String, ContestValidateAnsInfo> map = new HashMap<String, ContestValidateAnsInfo>();
	map.put("contestValidateAnsInfo", quizAnswerInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
    
}
	
public static void main(String[] args) throws Exception {
	
	String json = "{\"ansDtls\":[{\"qId\":\"4662\",\"qNo\":\"1\",\"ans\":\"A\",\"fbctm\":\"46625678\",\"anstm\":\"15124577\",\"rtc\":\"2\"},"
			+ "{\"qId\":\"4662\",\"qNo\":\"1\",\"ans\":\"A\",\"fbctm\":\"46625678\",\"anstm\":\"15124577\",\"rtc\":\"2\"}]}";
	System.out.println(json);
	ValidateAnsRequestInfo emp = GsonUtil.getJasksonObjMapper().readValue(json, ValidateAnsRequestInfo.class);
	System.out.println("size : "+emp.getAnsDtls().size());
	System.out.println("to Str : "+emp.toString());
	
}
	

}
