package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.service.CustomerMediaServices;
import com.quiz.cassandra.service.PollingVideoUrlService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingVideoServlet
 */

@WebServlet("/PollingVideo.json")
public class PollingVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingVideoServlet.class);

	public PollingVideoServlet() {
		super();
	}

	/*
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { process(request, response);
	 * 
	 * }
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PollingVideoInfo pollingVideoInfo = new PollingVideoInfo();
		CassError error = new CassError();
		Date start = new Date();
		
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		
		String customerIdStr = request.getParameter("cuId");
		String catId = request.getParameter("CAT");
		String isCatRequired = request.getParameter("isCat");		
		String appver = request.getParameter("ve");		
		String apprelease = request.getParameter("rel");
		
		

		try {

			System.out.println("[PollingVideoInfo]" + " [cuId] " + customerIdStr + " [pfm]" + platForm + "[CAT]" + catId);

			ApplicationPlatForm applicationPlatForm = null;

			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}
			
			
			if (TextUtil.isEmptyOrNull(isCatRequired)) {
				resMsg = "Please send iscat info required" + isCatRequired;
				error.setDesc("Please send iscat info required");
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
//				generateResponse(response, pollingVideoInfo);
//				return response;
			}
			Boolean isCatInfoRequerid  = true;
			try {
			 isCatInfoRequerid = Boolean.parseBoolean(isCatRequired);
			}catch(Exception ex) {
				System.out.println("Error parsing to boolean " + isCatRequired );
			}
			
			
			if (TextUtil.isEmptyOrNull(catId)) {
				resMsg = "Please send valid Category" + catId;
				error.setDesc("Please send valid Category");
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
//				generateResponse(response, pollingVideoInfo);
//				return response;
			}
			  
			pollingVideoInfo.setCatId(catId);
			
			

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}
			pollingVideoInfo.setCuId(customer.getId());
			// to support older versions as the structure of video play list & video category list changes ...
			pollingVideoInfo.setAppver(appver);			
			if(apprelease == null  || apprelease.isEmpty() ) {
				fetchPollingVideoInfo(customer , pollingVideoInfo);
			}else if("1".equals(apprelease)) {
				CustomerMediaServices.fetchVideoPlayList(customer , pollingVideoInfo);
			}else {
				// do nothing ..
			}
			
			
			
			
			if(isCatInfoRequerid) {
				fetchPollingVideoCategoryInfo(pollingVideoInfo);
			}
			
			try {
				String nxtCTxt = PollingUtil.getNextGameDetails();
				pollingVideoInfo.setNxtCTxt(nxtCTxt);
			}catch(Exception ex) {
				ex.printStackTrace();
				//this exception can be ignored .. 
			}
			
			resMsg = "Success:" + customerIdStr + ":POLLING:" + contestType;

		} catch (Exception e) {
			resMsg = "Error occured while Fetching Polling Answer Validation Details. ";
			e.printStackTrace();
			error.setDesc("");
			pollingVideoInfo.setErr(error);
			pollingVideoInfo.setSts(0);
			generateResponse(response, pollingVideoInfo);
			return response;

		} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
		}

		generateResponse(response, pollingVideoInfo);
		return response;

	}

	private PollingVideoInfo fetchPollingVideoInfo(CassCustomer customer , PollingVideoInfo pollingVideoInfo) throws Exception{
		pollingVideoInfo.setCuId(customer.getId());
		pollingVideoInfo = PollingVideoUrlService.processVideoUrlListforCustomer(customer , pollingVideoInfo);
		List vUrlList = pollingVideoInfo.getvUrlLst();
		if(vUrlList == null || vUrlList.size() == 0 )
		{
			pollingVideoInfo.setSts(0);
			pollingVideoInfo.setMsg(PollingUtil.MOBILE_MSG_NO_VIDEO);
			CassError error = new CassError();
			error.setDesc(PollingUtil.MOBILE_MSG_NO_VIDEO);
			pollingVideoInfo.setErr(error);
			return pollingVideoInfo;
		}		
		pollingVideoInfo.setvUrl("");
		pollingVideoInfo.setSts(1);				
		pollingVideoInfo.setMsg(PollingUtil.MOBILE_MSG_SUCCESS);		
		return pollingVideoInfo;
	}
	
private PollingVideoInfo fetchPollingVideoCategoryInfo( PollingVideoInfo pollingVideoInfo) throws Exception{
		
		pollingVideoInfo = PollingVideoUrlService.processVideoCategoryListforCustomer(pollingVideoInfo);
		List vCatList = pollingVideoInfo.getvCatList();
		if(vCatList == null || vCatList.size() == 0 )
		{
			pollingVideoInfo.setSts(0);
			pollingVideoInfo.setMsg(PollingUtil.MOBILE_MSG_NO_VIDEO_CATEGORIES_DEFINED);
			CassError error = new CassError();
			error.setDesc(PollingUtil.MOBILE_MSG_NO_VIDEO_CATEGORIES_DEFINED);
			pollingVideoInfo.setErr(error);
			return pollingVideoInfo;
		}	
		
		pollingVideoInfo.setSts(1);				
		pollingVideoInfo.setMsg(PollingUtil.MOBILE_MSG_SUCCESS);		
		return pollingVideoInfo;
	}

	public void generateResponse(HttpServletResponse response, PollingVideoInfo pollingVideoInfo)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Map<String, PollingVideoInfo> map = new HashMap<String, PollingVideoInfo>();
		map.put("pollingVideoInfo", pollingVideoInfo);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
