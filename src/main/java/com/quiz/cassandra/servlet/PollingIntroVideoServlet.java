package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.IntroVideoInfo;
import com.quiz.cassandra.service.IntroVideoService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingApplyMagicWandServlet.java
 */

@WebServlet("/IntroVideo.json")
public class PollingIntroVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingIntroVideoServlet.class);
  
    public PollingIntroVideoServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	IntroVideoInfo introVideoInfo =new IntroVideoInfo();
	CassError error = new CassError();
	Date start = new Date();	
	String customerIdStr = request.getParameter("cuId");
	String ans = request.getParameter("ans");
	String platForm = request.getParameter("pfm");	
	String loginIp = request.getParameter("lIp");
	
	Integer customerId = null;
	String resMsg = "";
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	
	try {
		
		System.out.println("[Intro Video  Info]  " + 
		 " [cuId] " + customerIdStr   + "  [ans]  " + ans );		
		 
		ApplicationPlatForm applicationPlatForm=null;	
		if(TextUtil.isEmptyOrNull(platForm))
		{
			resMsg = "Please send valid application platform:"+platForm;
			//error.setDesc("Please send valid application platform");
			error.setDesc(URLUtil.genericErrorMsg);
			introVideoInfo.setErr(error);
			introVideoInfo.setSts(0);
		
			generateResponse(response, introVideoInfo);
			return response;
		}
		
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				resMsg = "Please send valid application platform:"+platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				introVideoInfo.setErr(error);
				introVideoInfo.setSts(0);
			
				generateResponse(response, introVideoInfo);
				return response;
			}
		
		
		String contestType="POLLING";
		if(applicationPlatForm!= null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
			contestType="WEB";
		}
		if(platForm.contains("ANDROID")||platForm.contains("IOS")){
			loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
		}
			
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Invalid Customer Id:"+customerIdStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			introVideoInfo.setErr(error);
			introVideoInfo.setSts(0);			
			generateResponse(response, introVideoInfo);
			return response;
		}
		
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);		
		if(customer == null) {
			resMsg = "Customer Id is not Registered:"+customerIdStr;
			//error.setDesc("Customer Id is not Registered");
			error.setDesc(URLUtil.genericErrorMsg);
			introVideoInfo.setErr(error);
			introVideoInfo.setSts(0);			
			generateResponse(response, introVideoInfo);
			return response;
		}
		introVideoInfo.setCuId(customer.getId());		
		introVideoInfo.setAns(ans);
		
		updateIntroVideoRewards(introVideoInfo , customer);
		
		introVideoInfo.setSts(1);
		resMsg = "Success:"+customerIdStr+":POLLING:"+contestType;
		
	}catch(Exception e){
		resMsg = "Error occured while Applying Magic Wand ";
		e.printStackTrace();
		//error.setDesc("Error occured while Applying Magic Wand");
		error.setDesc(URLUtil.genericErrorMsg);
		introVideoInfo.setErr(error);
		introVideoInfo.setSts(0);
		generateResponse(response, introVideoInfo);
		return response;
		
	} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
	}
	

	generateResponse(response, introVideoInfo);
	return response;
	
}


private IntroVideoInfo updateIntroVideoRewards(IntroVideoInfo introVideoInfo , CassCustomer cassCustomer) throws Exception{		
		
	introVideoInfo = IntroVideoService.processIntroVideRewards(introVideoInfo, cassCustomer);
	return introVideoInfo;
}


public void generateResponse(HttpServletResponse response,IntroVideoInfo introVideoInfo) throws ServletException, IOException {
	Map<String, IntroVideoInfo> map = new HashMap<String, IntroVideoInfo>();
	map.put("IntroVideoInfo", introVideoInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsonMagicWandInfoInfo);
    out.flush(); 
}


	
	/*
	 * private class MagicWandInfo { private Integer sts; private String nxtCTxt;
	 * private String msg; private CassError err; private Integer cuId; private
	 * Integer coId; public Integer getCuId() { return cuId; } public void
	 * setCuId(Integer cuId) { this.cuId = cuId; } public Integer getCoId() { return
	 * coId; } public void setCoId(Integer coId) { this.coId = coId; }
	 * 
	 * public Integer getSts() { return sts; } public void setSts(Integer sts) {
	 * this.sts = sts; } public String getNxtCTxt() { return nxtCTxt; } public void
	 * setNxtCTxt(String nxtCTxt) { this.nxtCTxt = nxtCTxt; } public String getMsg()
	 * { return msg; } public void setMsg(String msg) { this.msg = msg; } public
	 * CassError getErr() { return err; } public void setErr(CassError err) {
	 * this.err = err; }
	 * 
	 * 
	 * 
	 * }
	 */
	 
}
