package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.Error;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.data.QuizQuestionInfo;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/GetQuizQuestionInfo.json")
public class ContQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ContQuestionServlet.class);
  
    public ContQuestionServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}	
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	

	
	log.info("Inside Get Question s 1 : "+ new Date());
	QuizQuestionInfo quizQuestionInfo =new QuizQuestionInfo();
	Error error = new Error();
	Date start = new Date();
	
	String customerIdStr = request.getParameter("customerId");
	String contestIdStr = request.getParameter("contestId");
	String questionIdStr = request.getParameter("questionNo");
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	String resMsg = "";
	Integer contestId = null;
	
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			quizQuestionInfo.setError(authError);
			quizQuestionInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,authError.getDescription());
			return quizQuestionInfo;
		}*/
		
		
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			error.setDescription("Contest Id is mandatory");
			quizQuestionInfo.setError(error);
			quizQuestionInfo.setStatus(0);

			generateResponse(response, quizQuestionInfo);
			return response;
		}
		if(TextUtil.isEmptyOrNull(questionIdStr)){
			error.setDescription("Question NO is mandatory");
			quizQuestionInfo.setError(error);
			quizQuestionInfo.setStatus(0);
			
			generateResponse(response, quizQuestionInfo);
			return response;
		}
		QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
		if(quizContest == null) {
			error.setDescription("Contest Id is Invalid");
			quizQuestionInfo.setError(error);
			quizQuestionInfo.setStatus(0);
			
			generateResponse(response, quizQuestionInfo);
			return response;
		}
		contestId = quizContest.getId();
		
		QuizContestQuestions quizQuestion = CassContestUtil.getQuizContestQuestionByContestIdandQuestionSlNo(quizContest.getId(), Integer.parseInt(questionIdStr));
		if(quizQuestion == null) {
			error.setDescription("Contest Id or Question NO is Invalid");
			quizQuestionInfo.setError(error);
			quizQuestionInfo.setStatus(0);
			
			generateResponse(response, quizQuestionInfo);
			return response;
		}
		quizQuestionInfo.setNoOfQuestions(quizContest.getNoOfQuestions());
		
		/*QuizContestQuestions previousquestion = CassContestUtil.getContestCurrentQuestions(quizContest.getId());
		if((previousquestion == null && quizQuestion.getQuestionSNo() != 1) ||
				(previousquestion != null && (previousquestion.getQuestionSNo() + 1) != quizQuestion.getQuestionSNo())) {
			error.setDescription("Question NO is Mismatch with Previous Question.");
			quizQuestionInfo.setError(error);
			quizQuestionInfo.setStatus(0);
			
			generateResponse(response, quizQuestionInfo);
			return response;
		}*/
		
		Integer pqLifeCount = 0;
		try {
			if(quizQuestion.getQuestionSNo() > 1) {
				Integer previousQuestionNo = quizQuestion.getQuestionSNo()-1;
				pqLifeCount = CassContestUtil.getLifelineUSedCustomersCountByQuestionNo(contestId, previousQuestionNo);
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error Occured While getting lifeline used customers count : "+quizQuestion.getQuestionSNo());
		}
		quizQuestionInfo.setPqLifeCount(pqLifeCount);
		
		CassContestUtil.updateContestCurrentQuestions(quizQuestion);
		
		
		/*if(TextUtil.isEmptyOrNull(customerIdStr)){
			error.setDescription("Customer Id is mandatory");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Customer Id is mandatory");
			return quizContestDetails;
		}*/
		
		/*QuizCustomer quizCustomer = QuizDAORegistry.getQuizCustomerDAO().getQuizCustomerById(Integer.parseInt(customerIdStr));
		if(quizCustomer == null){
			error.setDescription("Customer not valid");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GQUIZQUESTIONINFO,"Customer not valid");
			return quizContestDetails;
		}*/
		
		resMsg = "Success:"+quizQuestion.getId();
		quizQuestionInfo.setQuizContestQuestion(quizQuestion);
		quizQuestionInfo.setStatus(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);

	}catch(Exception e){
		e.printStackTrace();
		error.setDescription("Error occured while Fetching Question Info.");
		quizQuestionInfo.setError(error);
		quizQuestionInfo.setStatus(0);
		
		generateResponse(response, quizQuestionInfo);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.QUIZQUESTIONINFO, resMsg, contestId,
				null, start, new Date(), request.getHeader("X-Forwarded-For"),null,quizQuestionInfo.getStatus(),questionIdStr);
		log.info("QUIZ QUEST INFO : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}


	generateResponse(response, quizQuestionInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,QuizQuestionInfo quizQuestionInfo) throws ServletException, IOException {
	Map<String, QuizQuestionInfo> map = new HashMap<String, QuizQuestionInfo>();
	map.put("quizQuestionInfo", quizQuestionInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
