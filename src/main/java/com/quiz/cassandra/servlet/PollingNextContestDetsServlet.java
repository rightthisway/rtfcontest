package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingNextContestDetsServlet.java
 */

@WebServlet("/GetNxtConDets.json")
public class PollingNextContestDetsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingNextContestDetsServlet.class);

	public PollingNextContestDetsServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		CassError error = new CassError();
		
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		NextContestDets nextContestDets = new NextContestDets();
		try {

			System.out
					.println("[NextContestServlet Info]  " + " [cuId] " + customerIdStr + "  [coId]  " + contestIdStr);

			ApplicationPlatForm applicationPlatForm = null;

			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				nextContestDets.setErr(error);
				nextContestDets.setSts(0);
				generateResponse(response, nextContestDets);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				nextContestDets.setErr(error);
				nextContestDets.setSts(0);

				generateResponse(response, nextContestDets);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				nextContestDets.setErr(error);
				nextContestDets.setSts(0);
				generateResponse(response, nextContestDets);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				nextContestDets.setErr(error);
				nextContestDets.setSts(0);
				generateResponse(response, nextContestDets);
				return response;
			}
			String nxtContDets = PollingUtil.getNextGameDetails();
			nextContestDets.setNxtCTxt(nxtContDets);
			nextContestDets.setSts(1);
			resMsg = "Success:" + customerIdStr + ":POLLING:" + contestType;

		} catch (Exception e) {
			resMsg = "Error occured while fetching next contest details ";
			e.printStackTrace();
			//error.setDesc("Error occured while fetching next contest details");
			error.setDesc(URLUtil.genericErrorMsg);
			nextContestDets.setErr(error);
			nextContestDets.setSts(0);
			generateResponse(response, nextContestDets);
			return response;

		} finally {

		}

		generateResponse(response, nextContestDets);
		return response;

	}

	public void generateResponse(HttpServletResponse response, NextContestDets nextContestDets)
			throws ServletException, IOException {
		Map<String, NextContestDets> map = new HashMap<String, NextContestDets>();
		map.put("NextContestDets", nextContestDets);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

	private class NextContestDets {

		private Integer sts;
		private String nxtCTxt;
		private String msg;
		private CassError err;

		public Integer getSts() {
			return sts;
		}

		public void setSts(Integer sts) {
			this.sts = sts;
		}

		public String getNxtCTxt() {
			return nxtCTxt;
		}

		public void setNxtCTxt(String nxtCTxt) {
			this.nxtCTxt = nxtCTxt;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		public CassError getErr() {
			return err;
		}

		public void setErr(CassError err) {
			this.err = err;
		}

	}

}
