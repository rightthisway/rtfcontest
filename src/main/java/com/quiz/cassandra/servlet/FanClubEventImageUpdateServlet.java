package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.FanClubDTO;
import com.quiz.cassandra.list.FanClubEventInfo;
import com.web.util.GsonUtil;
import com.zonesws.webservices.dao.implementation.FanClubEventInterestSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubEventSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubSQLDAO;
import com.zonesws.webservices.data.FanClub;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ManageFanClubServlet
 */

@WebServlet("/FanClubEventImage.json")
public class FanClubEventImageUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(FanClubEventImageUpdateServlet.class);

	public FanClubEventImageUpdateServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		FanClubEventInfo fcEventInfo = new FanClubEventInfo();
		CassError error = new CassError();
		Date start = new Date();
		
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		Integer fcEventId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		//String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		
		String customerIdStr = request.getParameter("cuId");
		String fanClubEventIdStr = request.getParameter("fceId"); 
		String posterUrl = request.getParameter("pUrl"); 
		//String appver = request.getParameter("ve");		
		//String apprelease = request.getParameter("rel");
		
		try {
			ApplicationPlatForm applicationPlatForm = null;
			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
 
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}
			
			 
			if (TextUtil.isEmptyOrNull(posterUrl)) {
				resMsg = "Please enter valid PosterUrl";
				error.setDesc(resMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			
			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			try {
				fcEventId = Integer.parseInt(fanClubEventIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid fcEvent Id:" + fanClubEventIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);

				generateResponse(response, fcEventInfo);
				return response;
			}
			FanClubEvent fcEvent = FanClubEventSQLDAO.getActiveFanClubEventByEventId(fcEventId);
			if(fcEvent == null) {
				resMsg = "fcEvent Id is not Exist:" + fcEventId;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				
				generateResponse(response, fcEventInfo);
				return response;
			}
			if(fcEvent.getCuId() == null || !fcEvent.getCuId().equals(customerId)) {
				resMsg = "Admin Only Can Edit " + customerId;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				
				generateResponse(response, fcEventInfo);
				return response;
			}
			
			try {
				FanClubEventSQLDAO.saveFanClubEventsTransaction(fcEventId);
			}catch(Exception e) {
				e.printStackTrace();
			}
			fcEvent.setpUrl(posterUrl);
			fcEvent.setUpDate(new Date());
			FanClubEventSQLDAO.updateFanClubEventPosterUrl(fcEventId, posterUrl);
			  
			Integer likedEventId = FanClubEventInterestSQLDAO.getLikedFanClubEventIdsByEventIdAndCustomerId(customerId, fcEventId);
			if(likedEventId != null) {
				fcEvent.setLiked(Boolean.TRUE);
			}
			fcEventInfo.setFcEvent(fcEvent);
			fcEventInfo.setSts(1);
			
			resMsg = "Success:" + customerIdStr + ":fcEventId:" + fcEventId;
			fcEventInfo.setMsg("Image updated successfully.");
		} catch (Exception e) {
			resMsg = "Error occured while updating fcEvent image. ";
			e.printStackTrace();
			error.setDesc("");
			fcEventInfo.setErr(error);
			fcEventInfo.setSts(0);
			generateResponse(response, fcEventInfo);
			return response;

		} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
		}

		generateResponse(response, fcEventInfo);
		return response;

	}
  

	public void generateResponse(HttpServletResponse response, FanClubEventInfo fcEventInfo)
			throws ServletException, IOException {
		Map<String, FanClubEventInfo> map = new HashMap<String, FanClubEventInfo>();
		map.put("fanClubEventInfo", fcEventInfo);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
