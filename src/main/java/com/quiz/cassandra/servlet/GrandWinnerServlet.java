package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContSummaryInfo;
import com.quiz.cassandra.list.ContWinnerRewardsInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/GetGrandWinners.json")
public class GrandWinnerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(GrandWinnerServlet.class);
  
    public GrandWinnerServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	ContSummaryInfo contSummary =new ContSummaryInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	String contestIdStr = request.getParameter("coId");
	Integer contestId = null;
	
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			contSummary.setErr(authError);
			contSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,authError.getDescription());
			return contSummary;
		}*/
		
		//String customerIdStr = request.getParameter("customerId");
		//String summaryTypeStr = request.getParameter("summaryType");
		
		/*if(TextUtil.isEmptyOrNull(summaryTypeStr)){
			error.setDesc("Summary Type is mandatory");
			contSummary.setErr(error);
			contSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Summary Type is mandatory");
			return contSummary;
		}
		QuizSummaryType quizSummaryType = null;
		try {
			quizSummaryType = QuizSummaryType.valueOf(summaryTypeStr);
			
		} catch(Exception e) {
			error.setDesc("Summary Type is Invalid");
			contSummary.setErr(error);
			contSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Summary Type is Invalid");
			return contSummary;
		}*/
		
		/*if(TextUtil.isEmptyOrNull(customerIdStr)){
			error.setDesc("Customer Id is mandatory");
			contSummary.setErr(error);
			contSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Customer Id is mandatory");
			return contSummary;
		}*/
		
		//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			resMsg="Contest Id is mandatory:"+contestIdStr;
			//error.setDesc("Contest Id is mandatory");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			
			generateResponse(response, contSummary);
			return response;
		}
		
		//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
		QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
		if(quizContest == null) {
			resMsg="Contest Id is Invalid:"+contestIdStr;
			//error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			
			generateResponse(response, contSummary);
			return response;
		}
		contestId = quizContest.getId();
		
		/*List<QuizContestWinners> grandWinners = null;
		if(quizContest.getProcessStatus().equals(ContestProcessStatus.STARTED)) {
			grandWinners = QuizContestUtil.computeContestGrandWinners(Integer.parseInt(contestIdStr));	
		} else {
			grandWinners = QuizContestUtil.getContestGrandWinners(quizContest.getId());
		}*/
		List<CassContestWinners> grandWinners = CassContestUtil.getContestGrandWinners(quizContest.getId());
		if(grandWinners == null || grandWinners.isEmpty()) {
			Date startGrand = new Date();
			grandWinners = CassContestUtil.computeContestGrandWinners(Integer.parseInt(contestIdStr));
			/*try {
				Date startOne = new Date();
			//QuizContestUtil.forceSummaryRefreshTable();
			//QuizContestUtil.refreshSummaryDataTable();
			QuizContestUtil.refreshSummaryDataCache();
			
			log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestIdStr+" : "+new Date());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				Date startOne = new Date();
				//QuizDAORegistry.getQuizSummaryManagerDAO().updateCustomerPromoCodeAndContestORderStats();
				QuizCustomerPromocodeandContestOrderStatsScheduler.processCustomerPromoCodeAndContestOrderStats();
			log.info("Time to Update Cust PRomo and co stats Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestIdStr+" : "+new Date());
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			log.info("Time Grand Full Compute : "+(new Date().getTime()-startGrand.getTime())+" : "+ new Date());
		} else {
			log.info("Grand Winner Else  : "+(new Date().getTime()));
		}
		 
		//quizContest.setProcessStatus(ContestProcessStatus.GRANDWINNERCOMPUTED);
		//QuizContestUtil.updateQuizContest(quizContest);
		
		contSummary.setWinners(grandWinners);
		contSummary.setSts(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);
		resMsg="Success:"+contestIdStr;
	}catch(Exception e){
		e.printStackTrace();
		//error.setDesc("Error occured while Fetching Grand Winners.");
		error.setDesc(URLUtil.genericErrorMsg);
		contSummary.setErr(error);
		contSummary.setSts(0);
		
		generateResponse(response, contSummary);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.QUIZGRANDWINNERS, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,contSummary.getSts(),null);
		log.info("CASS GRANDWINNER : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	
	generateResponse(response, contSummary);
	return response;


}

public void generateResponse(HttpServletResponse response,ContSummaryInfo contSummary) throws ServletException, IOException {
	Map<String, ContSummaryInfo> map = new HashMap<String, ContSummaryInfo>();
	map.put("contSummaryInfo", contSummary);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
