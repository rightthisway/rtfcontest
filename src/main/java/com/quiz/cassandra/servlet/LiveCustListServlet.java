package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContestValidateAnsInfo;
import com.quiz.cassandra.list.LiveCustomerListInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/GetLiveCustList.json")
public class LiveCustListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(LiveCustListServlet.class);
  
    public LiveCustListServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	//System.out.println("LiveList Inside : "+request.getParameter("cuId")+" : "+ new Date());
	
	LiveCustomerListInfo liveCustListInfo = new LiveCustomerListInfo();
	CassError error = new CassError();
	Date start = new Date();
	String customerIdStr = request.getParameter("cuId");
	String contestIdStr = request.getParameter("coId");
	String pageNoStr = request.getParameter("pNo");
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	String resMsg = "";
	Integer contestId = null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			liveCustListInfo.setErr(authError);
			liveCustListInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,authError.getDescription());
			return liveCustListInfo;
		}*/
		 
		
		/*if(TextUtil.isEmptyOrNull(customerIdStr)){
			error.setDesc("Customer Id is mandatory");
			liveCustListInfo.setErr(error);
			liveCustListInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,"Customer Id is mandatory");
			return liveCustListInfo;
		}
		
		if(TextUtil.isEmptyOrNull(questionNoStr)){
			error.setDesc("Question No is mandatory");
			liveCustListInfo.setErr(error);
			liveCustListInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,"Question No is mandatory");
			return liveCustListInfo;
		}
		if(TextUtil.isEmptyOrNull(pageNoStr)){
			error.setDesc("Page No is mandatory");
			liveCustListInfo.setErr(error);
			liveCustListInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,"Page No is mandatory");
			return liveCustListInfo;
		}*/
		Integer pageNo = 1;
		try {
			pageNo = Integer.parseInt(pageNoStr);
		} catch(Exception e) {
			resMsg = "Page No is Invalid:"+pageNoStr;
			//error.setDesc("Page No is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			liveCustListInfo.setErr(error);
			liveCustListInfo.setSts(0);
			
			generateResponse(response, liveCustListInfo);
			return response;
		}
		
		/*if(TextUtil.isEmptyOrNull(contestIdStr)){
			error.setDesc("Contest Id is mandatory");
			liveCustListInfo.setErr(error);
			liveCustListInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,"Contest Id is mandatory");
			return liveCustListInfo;
		}*/
		
		//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
		QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
		if(quizContest == null) {
			resMsg = "Contest Id is Invalid:"+contestIdStr;
			//error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			liveCustListInfo.setErr(error);
			liveCustListInfo.setSts(0);
			
			generateResponse(response, liveCustListInfo);
			return response;
		}
		contestId = quizContest.getId();
		Integer maxRows = 25;
//Phase 2 Commented
		//liveCustListInfo = CassContestUtil.getLiveCustomerDetailsInfo(Integer.parseInt(contestIdStr),liveCustListInfo,pageNo,maxRows);
		
//Phase 2 UnCommented
		liveCustListInfo = CassContestUtil.getLiveCustomerDetailsInfoFromCache(Integer.parseInt(contestIdStr),liveCustListInfo);
		 
		
		//liveCustListInfo.setQuizContestWinners(grandWinners);
		resMsg = "Success:"+contestIdStr+":coId:"+contestIdStr+":pNo:"+pageNoStr;
		liveCustListInfo.setSts(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);
	}catch(Exception e){
		try {
			System.out.println("CASS CUT COUNT DTLS1 : "+customerIdStr+" : coId: "+contestIdStr+" :pNo: "+pageNoStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
			System.out.println("LIVE ERROR : "+e.fillInStackTrace());
		} catch (Exception ee) {
			System.out.println("In side catch catch : ");
			ee.printStackTrace();
		}
		resMsg = "Error occurred while Fetching Live Customer List.";
		e.printStackTrace();
		//error.setDesc("Error occurred while Fetching Live Customer List.");
		error.setDesc(URLUtil.genericErrorMsg);
		liveCustListInfo.setErr(error);
		liveCustListInfo.setSts(0);
		
		generateResponse(response, liveCustListInfo);
		return response;
	} finally {
		TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETLIVECUSTLIST, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,liveCustListInfo.getSts(),null);
		log.info("CASS CUT COUNT DTLS : "+customerIdStr+" : coId: "+contestIdStr+" :pNo: "+pageNoStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	
	generateResponse(response, liveCustListInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,LiveCustomerListInfo liveCustListInfo) throws ServletException, IOException {
	Map<String, LiveCustomerListInfo> map = new HashMap<String, LiveCustomerListInfo>();
	map.put("liveCustomerListInfo", liveCustListInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
