package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.service.CustomerMediaServices;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerFetchMyLikedMediaServlet.java
 * API to Add / remove favourite videos
 * 
 */

@WebServlet("/MyLikedMedia.json")
public class CustomerFetchMyLikedMediaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public CustomerFetchMyLikedMediaServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PollingVideoInfo pollingVideoInfo = new PollingVideoInfo();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time

		try {
						

			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = URLUtil.genericErrorMsg;
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = URLUtil.genericErrorMsg;
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = URLUtil.genericErrorMsg;
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			pollingVideoInfo = CustomerMediaServices.fetchCustomerLikedVideos(customer.getId(),pollingVideoInfo );
			List favVidlst = pollingVideoInfo.getMyVidInfoList();
			if(favVidlst == null || favVidlst.size() ==0) {
				pollingVideoInfo.setMsg(PollingUtil.EMPTY_MYLIKED_MEDIA);
			}
			pollingVideoInfo.setSts(1);
		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			pollingVideoInfo.setErr(error);
			pollingVideoInfo.setSts(0);
			generateResponse(response, pollingVideoInfo);
			return response;

		} finally {
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, pollingVideoInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, PollingVideoInfo pollingVideoInfo)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Map<String, PollingVideoInfo> map = new HashMap<String, PollingVideoInfo>();
		map.put("PollingVideoInfo", pollingVideoInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
