package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContestValidateAnsInfo;
import com.quiz.cassandra.thread.MegaJackpotThread;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestInfo;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ContestProcessStatus;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;

/**
 * Servlet implementation class StartContestServlet
 */

@WebServlet("/UpdateEndQuizContest.json")
public class EndContestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(EndContestServlet.class);
  
    public EndContestServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



	
	QuizContestInfo quizContestDetails =new QuizContestInfo();
	com.zonesws.webservices.data.Error error = new com.zonesws.webservices.data.Error();
	Date start = new Date();
	String resMsg = "";
	String contestIdStr = request.getParameter("contestId");
	String platForm = request.getParameter("platForm");
	String deviceType = request.getParameter("deviceType");
	String endType = request.getParameter("eType");
	Integer contestId = null;
	Integer customerId = null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			quizContestDetails.setError(authError);
			quizContestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.ENDQUIZCONTEST,authError.getDescription());
			return quizContestDetails;
		}*/
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			resMsg = "Contest Id is mandatory : "+contestIdStr;
			error.setDescription("Contest Id is mandatory");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			
			generateResponse(response, quizContestDetails);
			return response;
		}
		String contestType="MOBILE";
		/*if(platForm!= null && !platForm.equals(ApplicationPlatForm.ANDROID.toString()) && !platForm.equals(ApplicationPlatForm.IOS.toString())) {
			contestType="WEB";
		}*/
		System.out.println(" **************** Before calling getCurrentStartedContest()");
		
		QuizContest contest = CassContestUtil.getCurrentStartedContest(contestType);
		if (contest == null || !contest.getId().equals(Integer.parseInt(contestIdStr))) {
			System.out.println(" **************** inside if  Before calling getContestByIDForStartContest()");
			
			contest = CassContestUtil.getContestByIDForStartContest(Integer.parseInt(contestIdStr));
			//System.out.println(" **************** After call  contest id.." + contest.getId() );
			if(contest == null) {
				resMsg = "Quiz Contest Id is Invalid. : "+contestIdStr;
				error.setDescription("Quiz Contest Id is Invalid.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				
				generateResponse(response, quizContestDetails);
				return response;
			}	
		}
		 
		contestId = contest.getId();
		
		/*if(TextUtil.isEmptyOrNull(endType)){
			resMsg = "End Type is mandatory : "+contestIdStr+":"+endType;
			error.setDescription("End Type is mandatory");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			
			generateResponse(response, quizContestDetails);
			return response;
		}*/
		try {
			if(endType!= null && endType.equals("FORCESTOP")) {
				System.out.println(" ****************  before updatePostMigrationStats() " ) ;
				CassContestUtil.updatePostMigrationStats();
				System.out.println(" ****************  after updatePostMigrationStats() " ) ;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(" ****************  before refreshEndContestData() " ) ;
		CassContestUtil.refreshEndContestData();
		System.out.println(" ****************  after refreshEndContestData() " ) ;
		//contest.setProcessStatus(ContestProcessStatus.ENDCONTEST);
		//QuizContestUtil.updateQuizContest(contest);
		
		/* Change Hall of fame summary page max data size to 100 - By Ulaganathan on 11/27/2018 PM - Start */
		/*try {
			Integer hallOfFamePageMaxDataSize = QuizContestUtil.getHallOfFamePageMaxDataSize();
			if(hallOfFamePageMaxDataSize != 100) {
				QuizContestUtil.setHallOfFamePageMaxDataSize(100);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}*/
		/* Change Hall of fame summary page max data size to 100 - By Ulaganathan on 11/27/2018 PM - Ends */
		resMsg="Success";
		quizContestDetails.setStatus(1);
		quizContestDetails.setMessage("Quiz Contest Ended.");
		//TrackingUtils.webServiceTracking(request, WebServiceActionType.ENDQUIZCONTEST,"Success");
	}catch(Exception e){
		e.printStackTrace();
		error.setDescription("Error occured while Fetching Start Contest Information.");
		quizContestDetails.setError(error);
		quizContestDetails.setStatus(0);
		
		generateResponse(response, quizContestDetails);
		return response;

	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.ENDQUIZCONTEST, resMsg, contestId,
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null,quizContestDetails.getStatus(),null);
		log.info("QUIZ END CONT : "+request.getParameter("cuId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	generateResponse(response, quizContestDetails);
	return response;


}

public void generateResponse(HttpServletResponse response,QuizContestInfo quizContestDetails) throws ServletException, IOException {
	Map<String, QuizContestInfo> map = new HashMap<String, QuizContestInfo>();
	map.put("quizContestInfo", quizContestDetails);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
