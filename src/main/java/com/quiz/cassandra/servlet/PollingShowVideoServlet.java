package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingShowVideoInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingShowVideoServlet
 */

@WebServlet("/PollingShowVideo.json")
public class PollingShowVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingShowVideoServlet.class);
	public PollingShowVideoServlet() {
		super();
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PollingShowVideoInfo pollingShowVideoInfo = new PollingShowVideoInfo();
		CassError error = new CassError();
		Date start = new Date();
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		Integer contestId = null;
		try {

			System.out.println("[PollingShowVideoInfo]" + " [cuId] " + customerIdStr + " [pfm]" + platForm);

			ApplicationPlatForm applicationPlatForm = null;

			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingShowVideoInfo.setErr(error);
				pollingShowVideoInfo.setSts(0);

				generateResponse(response, pollingShowVideoInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingShowVideoInfo.setErr(error);
				pollingShowVideoInfo.setSts(0);

				generateResponse(response, pollingShowVideoInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingShowVideoInfo.setErr(error);
				pollingShowVideoInfo.setSts(0);
				generateResponse(response, pollingShowVideoInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingShowVideoInfo.setErr(error);
				pollingShowVideoInfo.setSts(0);
				generateResponse(response, pollingShowVideoInfo);
				return response;
			}
			List<Boolean> uploadVideoEligibiltyList = PollingSQLDaoUtil.customerCanUploadVideo(customerId);
			if(uploadVideoEligibiltyList.size() != 2) {
				pollingShowVideoInfo.setSts(0);
				//error.setDesc("Sorry We had trouble getting videos to play at this time.Please try again.");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingShowVideoInfo.setErr(error);
				generateResponse(response, pollingShowVideoInfo);
				return response;
				
			}
			
			Boolean canUploadVideo = uploadVideoEligibiltyList.get(0);
			Boolean hasUploadedVideo = uploadVideoEligibiltyList.get(1);			
			if(canUploadVideo == false ) {
				pollingShowVideoInfo.setSts(1);
				pollingShowVideoInfo.setIsVid(0); 
				pollingShowVideoInfo.setMsg(PollingUtil.MOBILE_MSG_ONLY_WINNER_CAN_UPLOAD);
				generateResponse(response, pollingShowVideoInfo);
				return response;
			}
			if(canUploadVideo == true &&  hasUploadedVideo == true ) {
				pollingShowVideoInfo.setSts(1);
				pollingShowVideoInfo.setIsVid(0); 
				pollingShowVideoInfo.setMsg(PollingUtil.MOBILE_MSG_WINNER_MAX_UPLOAD);
				generateResponse(response, pollingShowVideoInfo);
				return response;
			}
			if(canUploadVideo == true &&  hasUploadedVideo == false ) {
				pollingShowVideoInfo.setSts(1);
				pollingShowVideoInfo.setIsVid(1); 
				pollingShowVideoInfo.setMsg(PollingUtil.SUCCESS);
				generateResponse(response, pollingShowVideoInfo);
				return response;
			}
			//pollingShowVideoInfo.setSts(1); 
			//resMsg = "Success:" + customerIdStr + ":POLLING:" + contestType;
		} catch (Exception e) {
			resMsg = "Error occured while Fetching Polling Answer Validation Details. ";
			e.printStackTrace();
			//error.setDesc("Sorry We had trouble getting videos to play at this time.Please try again.");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingShowVideoInfo.setErr(error);
			pollingShowVideoInfo.setSts(0);
			generateResponse(response, pollingShowVideoInfo);
			return response;

		} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
		}

		generateResponse(response, pollingShowVideoInfo);
		return response;

	}
 

	public void generateResponse(HttpServletResponse response, PollingShowVideoInfo pollingShowVideoInfo)
			throws ServletException, IOException {
		Map<String, PollingShowVideoInfo> map = new HashMap<String, PollingShowVideoInfo>();
		map.put("pollingShowVideoInfo", pollingShowVideoInfo);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
