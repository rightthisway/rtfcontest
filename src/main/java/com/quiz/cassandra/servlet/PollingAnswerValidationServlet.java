package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingAnswerInfo;
import com.quiz.cassandra.service.PollingAnswerService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingAnswerValidationServlet
 */

@WebServlet("/PollingAnswerValidtion.json")
public class PollingAnswerValidationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingAnswerValidationServlet.class);
  
    public PollingAnswerValidationServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	PollingAnswerInfo pollingAnswerInfo =new PollingAnswerInfo();
	CassError error = new CassError();
	Date start = new Date();
	
	String qId = request.getParameter("qId");
	String pCtId = request.getParameter("pCtId");
	String ansOpt = request.getParameter("ansOpt");	
	String customerIdStr = request.getParameter("cuId");	
	String platForm = request.getParameter("pfm");	
	String loginIp = request.getParameter("lIp");
	Integer customerId = null;
	String resMsg = "";
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	Integer contestId=null;
	String oQId = request.getParameter("oQId");
	try {
		
		//System.out.println("[PollingContestAnswer]" + "[qId]" + qId  + " [pCtId]" + pCtId  + " [ansOpt]" + ansOpt  + " [cuId] " + customerIdStr  + " [pfm]" + platForm );
		if(TextUtil.isEmptyOrNull(qId))
		{
			resMsg = "Please send valid Question Id [qId] " ;
			//error.setDesc("Please send valid Question Id [qId]");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingAnswerInfo.setErr(error);
			pollingAnswerInfo.setSts(0);		
			generateResponse(response, pollingAnswerInfo);
			return response;
		}
		if(TextUtil.isEmptyOrNull(pCtId))
		{
			resMsg = "Please send valid ContestId [pCtId] " ;
			//error.setDesc("Please send valid ContestId [pCtId]");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingAnswerInfo.setErr(error);
			pollingAnswerInfo.setSts(0);		
			generateResponse(response, pollingAnswerInfo);
			return response;
		}
		if(TextUtil.isEmptyOrNull(ansOpt))
		{
			resMsg = "Please send Valid Answer Option [ansOpt] " ;
			//error.setDesc("Please send Valid Answer Option [ansOpt] ");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingAnswerInfo.setErr(error);
			pollingAnswerInfo.setSts(0);		
			generateResponse(response, pollingAnswerInfo);
			return response;
		}
		 
		ApplicationPlatForm applicationPlatForm=null;
		
		if(TextUtil.isEmptyOrNull(platForm))
		{
			resMsg = "Please send valid application platform:"+platForm;
			//error.setDesc("Please send valid application platform");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingAnswerInfo.setErr(error);
			pollingAnswerInfo.setSts(0);
		
			generateResponse(response, pollingAnswerInfo);
			return response;
		}
		
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				resMsg = "Please send valid application platform:"+platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingAnswerInfo.setErr(error);
				pollingAnswerInfo.setSts(0);
			
				generateResponse(response, pollingAnswerInfo);
				return response;
			}
		
		
		String contestType="POLLING";
		if(applicationPlatForm!= null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
			contestType="WEB";
		}
		if(platForm.contains("ANDROID")||platForm.contains("IOS")){
			loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
		}
		
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Invalid Customer Id:"+customerIdStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingAnswerInfo.setErr(error);
			pollingAnswerInfo.setSts(0);			
			generateResponse(response, pollingAnswerInfo);
			return response;
		}		
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);		
		if(customer == null) {
			resMsg = "Customer Id is not Registered:"+customerIdStr;
			//error.setDesc("Customer Id is not Registered");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingAnswerInfo.setErr(error);
			pollingAnswerInfo.setSts(0);
			
			generateResponse(response, pollingAnswerInfo);
			return response;
		}		
		pollingAnswerInfo.setId(Integer.valueOf(qId));
		pollingAnswerInfo.setpCtId(Integer.valueOf(pCtId));
		pollingAnswerInfo.setAnswer(ansOpt);
		pollingAnswerInfo.setCuId(customer.getId());
		Integer oQid = 0;
		try {
			oQid = Integer.valueOf(oQId);
		}catch (Exception ex) {
			//Ignore Exception ..
			 oQid = 0;
		}
		pollingAnswerInfo.setoQId(oQid);
		
		PollingAnswerService.processValidateAnswerResponse(pollingAnswerInfo);
				
		try {
			String nxtCTxt = PollingUtil.getNextGameDetails();
			pollingAnswerInfo.setNxtCTxt(nxtCTxt);			
			String uploadMsg = PollingUtil.UPLOAD_VID_MSG;
			pollingAnswerInfo.setVidUpdMsg(uploadMsg  + nxtCTxt );
			String msgTmp = pollingAnswerInfo.getMsg();
			if(msgTmp != null) {
				msgTmp = msgTmp.replace("magicwand", "Eraser");
				pollingAnswerInfo.setMsg(msgTmp);
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
			//this exception can be ignored .. 
		}
		
		
		resMsg = "Success:"+customerIdStr+":POLLING:"+contestType;
		
	}catch(Exception e){
		resMsg = "Error occured During Answer Validation. ";
		e.printStackTrace();
		//error.setDesc("Sorry. We had Trouble Processing Your Mini Game Question.Please Try again in Some Time");
		error.setDesc(URLUtil.genericErrorMsg);
		pollingAnswerInfo.setErr(error);
		pollingAnswerInfo.setSts(0);
		generateResponse(response, pollingAnswerInfo);
		return response;
		
	} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
	}
	
	
	generateResponse(response, pollingAnswerInfo);
	return response;
	
}


public void generateResponse(HttpServletResponse response,PollingAnswerInfo pollingAnswerInfo) throws ServletException, IOException {
	Map<String, PollingAnswerInfo> map = new HashMap<String, PollingAnswerInfo>();
	map.put("pollingAnswerInfo", pollingAnswerInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);
	String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsonPllingAnswerInfo);
    out.flush(); 
}





	
	

}
