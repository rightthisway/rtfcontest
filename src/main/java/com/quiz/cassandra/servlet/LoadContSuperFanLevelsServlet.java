package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CassJoinContestInfo;
import com.quiz.cassandra.list.CommonRespInfo;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.SQLDaoUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class LoadContSuperFanLevelsServlet
 */

@WebServlet("/LoadContSuperFanLevels.json")
public class LoadContSuperFanLevelsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(LoadContSuperFanLevelsServlet.class);
  
    public LoadContSuperFanLevelsServlet() {
        super();       
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	CommonRespInfo commResInfo = new CommonRespInfo();
	CassError error = new CassError();
	
	String contestIdStr = request.getParameter("coId");
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	String resMsg = "";
	Integer contestId = null;
	Date start = new Date();
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			quizJoinContestInfo.setErr(authError);
			quizJoinContestInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,authError.getDescription());
			return quizJoinContestInfo;
		}*/
		
		try{
			contestId = Integer.parseInt(contestIdStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Contest Id:"+contestIdStr;
			//error.setDesc("Invalid Contest Id");
			error.setDesc(URLUtil.genericErrorMsg);
			commResInfo.setErr(error);
			commResInfo.setSts(0);
			
			generateResponse(response, commResInfo);
			return response;
		}
		QuizContest contest = SQLDaoUtil.getQuizContestForId(contestId.toString());
		if(contest == null) {
			resMsg = "Contest Id is not Valid:"+contestIdStr;
			//error.setDesc("Invalid Contest Id");
			error.setDesc(URLUtil.genericErrorMsg);
			commResInfo.setErr(error);
			commResInfo.setSts(0);
			
			generateResponse(response, commResInfo);
			return response;
		}
		String msg = "0";
		CassContestUtil.refreshSuperFanContCustLevelsMap(contest);
		Map<Integer,SuperFanContCustLevels> map = CassContestUtil.getSuperFanContCustLevelsMap();
		if(map != null) {
			msg = ""+map.size();
		}
		
		commResInfo.setMsg(msg);
		commResInfo.setSts(1);
		
		resMsg = "Success:"+msg;
		//TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,"Success: "+totalCustomersCount);
	}catch(Exception e){
		resMsg = "Error occured while Loading Cont SF Levels.";
		e.printStackTrace();
		//error.setDesc("Error occured while Fetching Cass Connection Stats");
		error.setDesc(URLUtil.genericErrorMsg);
		commResInfo.setErr(error);
		commResInfo.setSts(0);
		
		generateResponse(response, commResInfo);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.LOADCONTSFLEVELS, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,commResInfo.getSts(),null);
		//log.info("CASS CUT COUNT DTLS : "+" : coId: "+contestIdStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	
	generateResponse(response, commResInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,CommonRespInfo commResInfo) throws ServletException, IOException {
	Map<String, CommonRespInfo> map = new HashMap<String, CommonRespInfo>();
	map.put("commonRespInfo", commResInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
