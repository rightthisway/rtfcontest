package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.FanClubEventIntInfo;
import com.quiz.cassandra.service.CustomerRewardLimtService;
import com.quiz.cassandra.service.FanClubService;
import com.web.util.GsonUtil;
import com.zonesws.webservices.dao.implementation.FanClubEventInterestSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubEventSQLDAO;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.data.FanClubEventInterest;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerMediafceIntInfoSaveServlet.java
 * API to report abuse on videos / fceIntInfo
 * 
 */

@WebServlet("/FanClubEventInterests.json")
public class FanClubEventInterestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubEventInterestServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FanClubEventIntInfo fceIntInfo = new FanClubEventIntInfo();
		CassError error = new CassError();

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		String deviceType = request.getParameter("dyType");
		
		String pType = request.getParameter("pType");
		String fcEventIdStr = request.getParameter("fceId");
		String customerIdStr = request.getParameter("cuId");
		
		Integer customerId = null;
		String resMsg = "";
		Integer fanClubId = null;
		Integer fcEventId = null;
		
		try {
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			ApplicationPlatForm applicationPlatForm = null;			
			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				fceIntInfo.setErr(error);
				fceIntInfo.setSts(0);

				generateResponse(response, fceIntInfo);
				return response;
			}
			if(pType == null || (!pType.equals("LIKED") && !pType.equals("EXIT"))) {
				resMsg = "Invalid ProcessType:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fceIntInfo.setErr(error);
				fceIntInfo.setSts(0);
				generateResponse(response, fceIntInfo);
				return response;
			}
			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fceIntInfo.setErr(error);
				fceIntInfo.setSts(0);
				generateResponse(response, fceIntInfo);
				return response;
			}
			try {
				fcEventId = Integer.parseInt(fcEventIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fceIntInfo.setErr(error);
				fceIntInfo.setSts(0);
				generateResponse(response, fceIntInfo);
				return response;
			}
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fceIntInfo.setErr(error);
				fceIntInfo.setSts(0);
				generateResponse(response, fceIntInfo);
				return response;
			}
			if(pType.equals("LIKED")) {
				FanClubEvent fcEvent = FanClubEventSQLDAO.getActiveFanClubEventByEventId(fcEventId);
				if(fcEvent == null) {
					resMsg = "fcEvent Id Not Exist.";
					error.setDesc(URLUtil.genericErrorMsg);
					fceIntInfo.setErr(error);
					fceIntInfo.setSts(0);
					generateResponse(response, fceIntInfo);
					return response;
				}
				FanClubEventInterest fceInterest = new FanClubEventInterest();
				fceInterest.setFceId(fcEventId);
				fceInterest.setCuId(customerId);
				fceInterest.setStatus("LIKED");
				fceInterest.setCrDate(new Date());
				
				//fceInterest = FanClubEventInterestSQLDAO.saveFanClubEventInterest(fceInterest);
				fceInterest = FanClubService.saveFCInterestShown(fceInterest , customer,fceIntInfo);
				
				if(fceInterest.getId() == null) {
					resMsg = "fcEvent Interest Not Created something Went Wrong.";
					error.setDesc(URLUtil.genericErrorMsg);
					fceIntInfo.setErr(error);
					fceIntInfo.setSts(0);
					generateResponse(response, fceIntInfo);
					return response;
				}
				FanClubEventSQLDAO.updateFanClubEventInterestCountForJoin(fcEventId);
				
				fceIntInfo.setMsg("Event Interest Updated Successfully");
			} else {
				FanClubEventInterest fceInterest = FanClubEventInterestSQLDAO.getLikedFanClubEventInterestByCustomerIdAndEventId(customerId, fcEventId);
				if(fceInterest == null) {
					resMsg = "FCEvent Interest Not Exit.";
					error.setDesc(URLUtil.genericErrorMsg);
					fceIntInfo.setErr(error);
					fceIntInfo.setSts(0);
					generateResponse(response, fceIntInfo);
					return response;
				}
				fceInterest.setStatus("EXIT");
				fceInterest.setUpDate(new Date());
				FanClubEventInterestSQLDAO.updateFanClubEventInterestExit(fceInterest);
				
				FanClubEventSQLDAO.updateFanClubEventInterestCountForExit(fcEventId);
				try {
				CustomerRewardLimtService.fanClubEventUnikeRewards(customer.getId());
				}catch(Exception ex) {
					ex.printStackTrace();
				}
				fceIntInfo.setMsg("Event Interest Removed Successfully");
			}
				
			
			fceIntInfo.setSts(1);
			
			
		} catch (Exception e) {
			resMsg = "Something Went Wrong While Updating FcEventInt";
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			fceIntInfo.setErr(error);
			fceIntInfo.setSts(0);
			generateResponse(response, fceIntInfo);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, fceIntInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, FanClubEventIntInfo fceIntInfo)
			throws ServletException, IOException {
		Map<String, FanClubEventIntInfo> map = new HashMap<String, FanClubEventIntInfo>();
		map.put("fanClubEventIntInfo", fceIntInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
