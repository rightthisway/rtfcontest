package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.service.PollingMediaUploadService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingVideoUploadServlet
 */

@WebServlet("/PollingVideoUpdate.json")
public class PollingVideoUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingVideoUploadServlet.class);

	public PollingVideoUploadServlet() {
		super();
	}

	/*
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { process(request, response);
	 * 
	 * }
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PollingVideoInfo pollingVideoInfo = new PollingVideoInfo();
		CassError error = new CassError();		
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");		
		String winnerOrGeneral = request.getParameter("wTy");		
		String vUrl = request.getParameter("vUrl");
		String tUrl = request.getParameter("tUrl");
		String categoryId = request.getParameter("catId");
		String title = request.getParameter("title");
		String description = request.getParameter("desc");
		Integer catId = null;
		try {

			System.out.println("[PollingVideoUpdate]" + " [cuId] " + customerIdStr + " [pfm]" + platForm);

			ApplicationPlatForm applicationPlatForm = null;

			
			// TO Support old versions ..
			if(categoryId == null || categoryId.isEmpty()) {
				categoryId = "6"; // Setting to GENERAL category ID .
			}
			
			if(tUrl == null || tUrl.isEmpty()) {
				
			}
			
			if (TextUtil.isEmptyOrNull(vUrl)) {
				resMsg = "Please send valid S3 Video URL ";
				//error.setDesc("Please send valid S3 Video URL");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}
			/*  // uncomment after app is live ..
			 * if (TextUtil.isEmptyOrNull(tUrl)) { resMsg =
			 * "Please send valid Video thumbnail  URL ";
			 * //error.setDesc("Please send valid S3 Video URL");
			 * error.setDesc(URLUtil.genericErrorMsg); pollingVideoInfo.setErr(error);
			 * pollingVideoInfo.setSts(0); System.out.println(resMsg);
			 * generateResponse(response, pollingVideoInfo); return response; }
			 */
			if (TextUtil.isEmptyOrNull(categoryId)) {
				resMsg = "Please send valid Video categoryId ";
				//error.setDesc("Please send valid S3 Video URL");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				System.out.println(resMsg);
				generateResponse(response, pollingVideoInfo);
				return response;
			}
			try {
				catId = Integer.parseInt(categoryId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid CategoryId:" + categoryId;				
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				System.out.println(resMsg);
				generateResponse(response, pollingVideoInfo);
				return response;
			}
			
			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;				
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}
			
			
			if(TextUtil.isEmptyOrNull(winnerOrGeneral) ){
				resMsg = "Please send valid Upload Type: General or Winner :" + platForm;
			//	error.setDesc("Please send valid Upload Type: General or Winner");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}
			
			pollingVideoInfo.setvUrl(vUrl);
			pollingVideoInfo.setCuId(customerId);			
			pollingVideoInfo.setwTy(winnerOrGeneral);
			pollingVideoInfo.setCatId(categoryId);
			pollingVideoInfo.settUrl(tUrl);
			pollingVideoInfo.setDescription(description);
			pollingVideoInfo.setTitle(title);
			

			updatePollingVideoInfo(pollingVideoInfo ,  customer);
			if(pollingVideoInfo.getSts() == 1) {
				pollingVideoInfo.setMsg(PollingUtil.CUST_VIDEO_UPLOAD_SUCCESS);
			}
			else {
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				pollingVideoInfo.setMsg(URLUtil.genericErrorMsg);
			}
			

		} catch (Exception e) {
			resMsg = "Error occured while uploading video ";
			e.printStackTrace();
			
			error.setDesc(URLUtil.genericErrorMsg);
			pollingVideoInfo.setErr(error);
			pollingVideoInfo.setSts(0);
			generateResponse(response, pollingVideoInfo);
			return response;

		} 
		generateResponse(response, pollingVideoInfo);
		return response;

	}

	private PollingVideoInfo updatePollingVideoInfo(PollingVideoInfo pollingVideoInfo ,CassCustomer customer ) {

		try {
			PollingMediaUploadService.processMediaUpload(pollingVideoInfo, customer);
			
		} catch (Exception ex) {			
			ex.printStackTrace();
			pollingVideoInfo.setSts(0);
			pollingVideoInfo.setMsg(PollingUtil.MOBILE_MSG_UPLOAD_FAILURE);
		}
		
		String nxtCTxt  = " Next Contest will be at 8.00 PM  EST " ; 
		pollingVideoInfo.setNxtCTxt(nxtCTxt);

		return pollingVideoInfo;
	}

	public void generateResponse(HttpServletResponse response, PollingVideoInfo pollingVideoInfo)
			throws ServletException, IOException {
		Map<String, PollingVideoInfo> map = new HashMap<String, PollingVideoInfo>();
		map.put("pollingVideoInfo", pollingVideoInfo);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
