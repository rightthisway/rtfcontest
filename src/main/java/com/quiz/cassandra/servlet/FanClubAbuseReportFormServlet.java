package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.AbuseReportDTO;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.service.AbuseReportingService;
import com.web.util.FanClubUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class FanClubAbuseReportFormServlet.java
 * API to report abuse on videos / comments
 * 
 */

@WebServlet("/FanClubAbuseReportForm.json")
public class FanClubAbuseReportFormServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public FanClubAbuseReportFormServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AbuseReportDTO abuseReportDTO = new AbuseReportDTO();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		
		String abuseType = request.getParameter("actionType"); // FC, FCE, FCP, FCPC, FCV, FCVC
		String srcId = request.getParameter("srcId"); // FanclubId, FanClubEventId,FanClubPostId, FanClubPostCommentId, FanClubVideoId,  FanClubVideoCommentId	
		Integer sourceId = null;
		Integer commentId=null;
		
		try {
			System.out.println("[AbuseReport Info]  " + " [cuId] " + customerIdStr );			

			if("FC".equals(abuseType)) {
				abuseType = FanClubUtil.ABUSETYPE_FANCLUB;
			}else if("FCE".equals(abuseType)) {
				abuseType = FanClubUtil.ABUSETYPE_FANCLUB_EVENT;
			}else if("FCP".equals(abuseType)){	
				abuseType = FanClubUtil.ABUSETYPE_POST;
			}else if("FCPC".equals(abuseType)) {
				abuseType = FanClubUtil.ABUSETYPE_POST_COMMENT;
			}else if("FCV".equals(abuseType)) {
				abuseType = FanClubUtil.ABUSETYPE_VIDEO;
			}else if("FCVC".equals(abuseType)) {
				abuseType = FanClubUtil.ABUSETYPE_VIDEO_COMMENT;
			} else {
				resMsg = "Please send valid ABUSE Type:" + abuseType;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);
				generateResponse(response, abuseReportDTO);
				return response;				
			}
			 
			try {
				sourceId = Integer.parseInt(srcId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid source  Id:" + srcId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);
				generateResponse(response, abuseReportDTO);
				return response;
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);
				generateResponse(response, abuseReportDTO);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);
				generateResponse(response, abuseReportDTO);
				return response;
			}

			abuseReportDTO.setSrcId(sourceId);
			abuseReportDTO.setAbuseType(abuseType);
			abuseReportDTO.setCuId(customer.getId()); 

			AbuseReportingService.fetchAbuseFanClubRelatedAbuseByActionType(abuseReportDTO);
			
			if(abuseReportDTO.getSts() == 1) {
				abuseReportDTO.setMsg("Abuse related information retrieved successfully.");
			}
			else {
				abuseReportDTO.setSts(0);
				abuseReportDTO.setMsg(URLUtil.genericErrorMsg);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			abuseReportDTO.setErr(error);
			abuseReportDTO.setSts(0);
			generateResponse(response, abuseReportDTO);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, abuseReportDTO);
		return response;
	}

	public void generateResponse(HttpServletResponse response, AbuseReportDTO abuseReportDTO)
			throws ServletException, IOException {
		Map<String, AbuseReportDTO> map = new HashMap<String, AbuseReportDTO>();
		map.put("AbuseReportDTO", abuseReportDTO);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}
}
