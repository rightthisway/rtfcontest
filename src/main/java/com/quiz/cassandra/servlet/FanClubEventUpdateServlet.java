package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.FanClubEventInfo;
import com.quiz.cassandra.service.FanClubService;
import com.web.util.GsonUtil;
import com.zonesws.webservices.dao.implementation.FanClubEventInterestSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubEventSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubMembersSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubSQLDAO;
import com.zonesws.webservices.data.FanClub;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.data.FanClubEventInterest;
import com.zonesws.webservices.data.FanClubMembers;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerMediafcEventInfoSaveServlet.java
 * API to report abuse on videos / fcEventInfo
 * 
 */

@WebServlet("/FanClubEventUpdate.json")
public class FanClubEventUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubEventUpdateServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FanClubEventInfo fcEventInfo = new FanClubEventInfo();
		CassError error = new CassError();

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		String deviceType = request.getParameter("dyType");
		
		String pType = request.getParameter("pType");
		String eventIdStr = request.getParameter("fceId");
		String customerIdStr = request.getParameter("cuId");
		String fanClubIdStr = request.getParameter("fcId");
		String eventName = request.getParameter("eName");
		String eventDateStr = request.getParameter("eDate");
		String eventtimeStr = request.getParameter("eTime");
		String venue = request.getParameter("venue");
		String posterUrl = request.getParameter("pUrl");
		
		Integer customerId = null;
		String resMsg = "";
		Integer fanClubId = null;
		Date eventDate = null;
		Date eventTime = null;
		
		try {
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			ApplicationPlatForm applicationPlatForm = null;			
			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);

				generateResponse(response, fcEventInfo);
				return response;
			}
			if(pType == null || (!pType.equals("SAVE") && !pType.equals("UPDATE"))) {
				resMsg = "Invalid ProcessType:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			try {
				fanClubId = Integer.parseInt(fanClubIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid FanClub Id:" + fanClubId;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			if(TextUtil.isEmptyOrNull(eventName)) {
				resMsg = "Event Name can't be Empty:" + eventName;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			if(TextUtil.isEmptyOrNull(venue)) {
				resMsg = "Venue can't be Empty:" + venue;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			if(!TextUtil.isEmptyOrNull(eventDateStr)) {
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				try {
					eventDate = dateFormat.parse(eventDateStr);
				} catch(Exception e) {
					resMsg = "Invalid Event Date:" + eventDateStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
			}
			if(!TextUtil.isEmptyOrNull(eventtimeStr)) {
				DateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
				try {
					eventTime = dateFormat.parse(eventtimeStr);
				} catch(Exception e) {
					resMsg = "Invalid Event Time:" + eventtimeStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
			}
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			String msg = "";
			FanClubEvent fcEvent = null;
			if(pType.equals("UPDATE")) {
				Integer fcEventId = null;
				try {
					fcEventId = Integer.parseInt(eventIdStr.trim());
				} catch (Exception e) {
					e.printStackTrace();
					resMsg = "Invalid fcEvent Id:" + fanClubId;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
				fcEvent = FanClubEventSQLDAO.getActiveFanClubEventByEventId(fcEventId);
				if (fcEvent == null) {
					resMsg = "Event Id Not Exist:" + customerIdStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
				if(fcEvent.getCuId() == null || !fcEvent.getCuId().equals(customerId)) {
					resMsg = "Admin Only Can Edit Events." + customerIdStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;	
				}
				//fcEvent.setFcId(fanClubId);
				//fcEvent.setCuId(customerId);
				fcEvent.seteName(eventName);
				fcEvent.seteDate(eventDate);
				fcEvent.seteTime(eventTime);
				fcEvent.setVenue(venue);
				fcEvent.setStatus("ACTIVE");
				fcEvent.setUpDate(new Date());
				fcEvent.setUpUserId(null);
				
				try {
					FanClubEventSQLDAO.saveFanClubEventsTransaction(fcEventId);
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				FanClubEventSQLDAO.updateFanClubEvent(fcEvent);
				msg = "Fanclub Event Updated Successfully.";
			} else {
				
				FanClub fanclub = FanClubSQLDAO.getActiveFanClub(fanClubId,customerId);
				if(fanclub == null) {
					resMsg = "Fanclub Id not registered."+fanClubIdStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
				FanClubMembers fcMember = FanClubMembersSQLDAO.getJoinedFanClubMembersByCustomerIdAndFanClubId(customerId, fanClubId);
				if(fcMember == null) {
					resMsg = "Fanclub Members only can create Events." + customerIdStr+":"+fanClubId;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;	
				}
				
				fcEvent = new FanClubEvent();
				fcEvent.setFcId(fanClubId);
				fcEvent.setCuId(customerId);
				fcEvent.seteName(eventName);
				fcEvent.seteDate(eventDate); 
				fcEvent.seteTime(eventTime);
				fcEvent.setVenue(venue);
				fcEvent.setStatus("ACTIVE");
				fcEvent.setCrDate(new Date());
				fcEvent.setNoIntr(1);
				fcEvent.setpUrl(posterUrl);
				fcEvent = FanClubService.createFanClubEvent(customer,fcEvent , fcEventInfo );
				//fcEvent = FanClubEventSQLDAO.saveFanClubEvent(fcEvent);
				if(fcEvent.getId() == null) {
					resMsg = "fcEvent Not Created something Went Wrong.";
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
				FanClubEventInterest fceInterest = new FanClubEventInterest();
				fceInterest.setFceId(fcEvent.getId());
				fceInterest.setCuId(customerId);
				fceInterest.setStatus("LIKED");
				fceInterest.setCrDate(new Date());
				fceInterest = FanClubEventInterestSQLDAO.saveFanClubEventInterest(fceInterest);
				
				msg = "FanClub Event created Successfully.";
			}
			Integer likedEventId = FanClubEventInterestSQLDAO.getLikedFanClubEventIdsByEventIdAndCustomerId(customerId, fcEvent.getId());
			if(likedEventId != null) {
				fcEvent.setLiked(Boolean.TRUE);
			}
			fcEventInfo.setFcEvent(fcEvent);
			fcEventInfo.setSts(1);
			fcEventInfo.setMsg("FanClub Event created Successfully");
			
		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			fcEventInfo.setErr(error);
			fcEventInfo.setSts(0);
			generateResponse(response, fcEventInfo);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, fcEventInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, FanClubEventInfo fcEventInfo)
			throws ServletException, IOException {
		Map<String, FanClubEventInfo> map = new HashMap<String, FanClubEventInfo>();
		map.put("fanClubEventInfo", fcEventInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
