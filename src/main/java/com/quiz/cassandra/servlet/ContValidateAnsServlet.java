package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContDtls;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContestValidateAnsInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ExitContestServlet
 */

@WebServlet("/ContValidateAns.json")
public class ContValidateAnsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ContValidateAnsServlet.class);
  
    public ContValidateAnsServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//process(request, response);
		processWithAutoCreate(request, response);
	}
	protected HttpServletResponse processWithAutoCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		//System.out.println("VALIDANS Inside : "+request.getParameter("cuId")+" : "+ new Date());
		
		ContestValidateAnsInfo quizAnswerInfo =new ContestValidateAnsInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String questionNoStr = request.getParameter("qNo");
		String questionIdStr = request.getParameter("qId");
		String answerOption = request.getParameter("ans");
		String platform = request.getParameter("pfm");
		String fbTimeStr = request.getParameter("fbctm");// - forebase callback time
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		String netspeedStr = request.getParameter("netspd");// - mobile net speed
		Integer customerId = null,contestId = null;
		String description = "";
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				quizAnswerInfo.setErr(authError);
				quizAnswerInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg);
				return quizAnswerInfo;
			}*/
			/*if(customerIdStr != null && customerIdStr.equals("0")) {
				customerIdStr = ""+CassContestUtil.getRandomCustomerIdForTest();
			
				QuizContest tempCont = CassContestUtil.getCurrentStartedContest("MOBILE");
				contestIdStr = ""+tempCont.getId();
				QuizContestQuestions quest = CassContestUtil.getContestCurrentQuestions(tempCont.getId());
				questionIdStr = ""+quest.getId();
				questionNoStr = ""+quest.getQuestionSNo();
				answerOption = quest.getAnswer();
			}*/
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Customer Id:"+customerIdStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);


				generateResponse(response, quizAnswerInfo);
				return response;
			}
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Contest Id:"+contestIdStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Invalid Contest Id");
				error.setDesc(URLUtil.genericErrorMsg);
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				
				generateResponse(response, quizAnswerInfo);
				return response;
			}
			Integer questionId = null;
			try{
				questionId = Integer.parseInt(questionIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Question Id:"+questionIdStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Invalid Question Id");
				error.setDesc(URLUtil.genericErrorMsg);
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				
				generateResponse(response, quizAnswerInfo);
				return response;
			}
			Integer questionNo = null;
			try{
				questionNo = Integer.parseInt(questionNoStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Question No:"+questionNoStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Invalid Question No");
				error.setDesc(URLUtil.genericErrorMsg);
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				
				generateResponse(response, quizAnswerInfo);
				return response;
			}
			
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			QuizContest contest = CassContestUtil.getCurrentContestByContestId(contestId);
			if(contest == null){
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Contest Id is Invalid");
				error.setDesc(URLUtil.genericErrorMsg);
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				
				
				generateResponse(response, quizAnswerInfo);
				return response;
			}
			//CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			CassCustomer customer = CassContestUtil.getCasscustomerFromMap(customerId);
			if(customer == null){
				resMsg = "Customer Id is Invalid:"+customerIdStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Custoemr Id is Invalid");
				error.setDesc(URLUtil.genericErrorMsg);
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				
				generateResponse(response, quizAnswerInfo);
				return response;
			}
			
			Boolean isIOSDevice = true;
			try {
				if(platform!=null && platform.equalsIgnoreCase(ApplicationPlatForm.IOS.toString())) {
					isIOSDevice = true;
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			QuizContestQuestions quizQuestion = CassContestUtil.getQuizContestQuestionById(questionId);
			
			if(quizQuestion == null ) {//|| !quizQuestion.getId().equals(questionId)
				resMsg = "Question Id is Invalid:"+questionIdStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Question Id is Invalid");
				error.setDesc(URLUtil.genericErrorMsg);
				quizAnswerInfo.setErr(error);
				
				if(isIOSDevice) {
					quizAnswerInfo.setSts(1);				
				} else {
					quizAnswerInfo.setSts(0);
				}
				
				generateResponse(response, quizAnswerInfo);
				return response;
			}
			if(!quizQuestion.getQuestionSNo().equals(questionNo)) {//|| !quizQuestion.getContestId().equals(contest.getId()
				resMsg = "Question No or Id is Invalid:"+questionNoStr;
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				//error.setDesc("Question No or Id is Invalid");
				error.setDesc(URLUtil.genericErrorMsg);
				quizAnswerInfo.setErr(error);

				if(isIOSDevice) {
					quizAnswerInfo.setSts(1);				
				} else {
					quizAnswerInfo.setSts(0);
				}
				
				generateResponse(response, quizAnswerInfo);
				return response;
			}
			
			String ansResMsg = "";
			/*CustContAnswers questionAns = CassContestUtil.getCustomerQuestionAnswers(customer.getId(),questionNo, contest.getId());
			if(questionAns != null) {
				description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
				resMsg = "Answere Already selected:"+questionNoStr;
				error.setDesc("Answere Already selected.");
				quizAnswerInfo.setErr(error);

				if(isIOSDevice) {
					quizAnswerInfo.setSts(1);				
				} else {
					quizAnswerInfo.setSts(0);
				}

				generateResponse(response, quizAnswerInfo);
				return response;
			}*/
			
			Integer sfLevelQNo = 0; 
			if(CassContestUtil.SuperFanLevelEnabled) {
				SuperFanContCustLevels sfContCustLevels = CassContestUtil.getSuperFanContCustLevels(customer.getId(), contest.getId());
				if(sfContCustLevels != null) {
					sfLevelQNo = sfContCustLevels.getqNo();
				}
			}
			
			//CustContAnswers lastAnswer = CassContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
			CustContDtls custContDtls = CassContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
			if(sfLevelQNo < questionNo) {
				if(custContDtls != null) {
					if(custContDtls.getLqNo().equals(questionNo)) {
						//ansResMsg += ":Success You have already answered for this question:qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";
						description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
						resMsg = "Answere Already selected:"+questionNoStr;
						//error.setDesc("Answer Already selected.");
						error.setDesc(URLUtil.genericErrorMsg);
						quizAnswerInfo.setErr(error);
	
						if(isIOSDevice) {
							quizAnswerInfo.setSts(1);				
						} else {
							quizAnswerInfo.setSts(0);
						}
	
						generateResponse(response, quizAnswerInfo);
						return response;
					} else if(questionNo != (custContDtls.getLqNo()+1)) {
						ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";
					} else if (custContDtls.getIsLqCrt() == null|| !custContDtls.getIsLqCrt() && (null == custContDtls.getIsLqLife() || !custContDtls.getIsLqLife())) {
						ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";;
					}
				} else { 
					if(quizQuestion.getQuestionSNo() > 1) {
						ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";
					}
				}
			}
			Boolean isAutoFlag = false;
			Integer startQno=0,endQno=0;
			if(custContDtls != null) {
				if(questionNo - custContDtls.getLqNo() > 1) {
					startQno = custContDtls.getLqNo()+1;
					endQno = questionNo - 1;
					isAutoFlag = true;
				}
			} else if(questionNo > 1){
				startQno = 1;
				endQno = questionNo - 1;
				isAutoFlag = true;
			}
			if(isAutoFlag) {
				if(sfLevelQNo >= endQno) {
					isAutoFlag = false;
					//endquestion rewards					
				} else if(sfLevelQNo >= startQno) {
					startQno = sfLevelQNo+1;
					//sfLevelQno - 1 rewards
				}
			}
			if(isAutoFlag) {
				try {
					custContDtls = CassContestUtil.computeAutoCreateAnswers(customerId, contestId, startQno, endQno, ansResMsg, custContDtls);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Error Occured in AUTO ANs Creation Block:"+customerIdStr+":"+contestIdStr+":"+questionNoStr+":"+new Date());
				}
			}
			
			Double cumulativeRewards = 0.0;
			Integer cumulativeLifeLineUsed = 0;
			Double sflRwds = 0.0;
			if(custContDtls != null) {
				if(custContDtls.getCuRwds() != null) {
					cumulativeRewards = custContDtls.getCuRwds();
				}
				if(custContDtls.getCuLife() != null) {
					cumulativeLifeLineUsed = custContDtls.getCuLife();
				}
				if(custContDtls.getSflRwds() != null) {
					sflRwds = custContDtls.getSflRwds();
				}
			}
			
			if(answerOption.equalsIgnoreCase(quizQuestion.getAnswer())) {
				quizAnswerInfo.setIsCrt(Boolean.TRUE);
				
				if(contest.getNoOfQuestions().equals(questionNo)) {
					CassContestUtil.updateContestWinners(contest.getId(), customerId);
				}
			}
			
			CustContAnswers custContAns = new CustContAnswers();
			custContAns.setCuId(customerId);
			custContAns.setCoId(contestId);
			custContAns.setqId(questionId);
			custContAns.setqNo(questionNo);
			custContAns.setAns(answerOption);
			custContAns.setIsCrt(quizAnswerInfo.getIsCrt());
			custContAns.setCrDate(new Date().getTime());
			custContAns.setIsLife(false);
			custContAns.setFbCallbackTime(fbTimeStr);
			custContAns.setAnswerTime(null);
			custContAns.setRetryCount(0);
			custContAns.setIsAutoCreated(Boolean.FALSE);
		
			if(quizAnswerInfo.getIsCrt()) {
				custContAns.setaRwds(quizQuestion.getQuestionRewards());
				if(quizQuestion.getQuestionRewards() != null) {
					cumulativeRewards = cumulativeRewards + quizQuestion.getQuestionRewards();	
					
					if(sfLevelQNo >= questionNo) {
						sflRwds = sflRwds + quizQuestion.getQuestionRewards();
					}
				}
				
			} 
			/*if (sfLevelQNo >= questionNo) {//fix This
				if(quizQuestion.getQuestionRewards() != null) {
					cumulativeRewards = cumulativeRewards + quizQuestion.getQuestionRewards();	
				}
			}*/
			custContAns.setCuRwds(cumulativeRewards);
			custContAns.setCuLife(cumulativeLifeLineUsed);

			CassandraDAORegistry.getCustContAnswersDAO().save(custContAns);
			CassContestUtil.updateCustomerAnswers(custContAns);
			
//Fix This			
			if(custContDtls == null) {
				custContDtls = new CustContDtls();
				custContDtls.setCoId(custContAns.getCoId());
				custContDtls.setCuId(custContAns.getCuId());
				
			} 
			if(custContDtls.getLqNo() == null || custContDtls.getLqNo() <= custContAns.getqNo()) {
				custContDtls.setLqNo(custContAns.getqNo());
				custContDtls.setIsLqCrt(custContAns.getIsCrt());
				custContDtls.setIsLqLife(custContAns.getIsLife());	
			}
			custContDtls.setCuRwds(custContAns.getCuRwds());
			custContDtls.setCuLife(custContAns.getCuLife());
			custContDtls.setSflRwds(sflRwds);
			CassandraDAORegistry.getCustContDtlsDAO().save(custContDtls);
			
			if(ansResMsg.isEmpty()) {
				resMsg = "Success:";
				description = "Success:Id=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ".";;
			} else {
				resMsg = ansResMsg;
				description = ansResMsg;
				
			}
			quizAnswerInfo.setSts(1);
			
			//log.info("Quiz Question validation : QNO:"+questionNoStr+": "+customerIdStr+":"+contestIdStr+":"+answerOption+":"+questionNoStr+":"+custContAns.getIsCrt()+":"+new Date());
			
			
		}catch(Exception e){
			description="qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption+ ",cuId="+customerIdStr+",coId="+contestIdStr;
			e.printStackTrace();
			resMsg = "Error occurred while Fetching Validate Answer Info.";
			//error.setDesc("Error occurred while Fetching Validate Answer Info.");
			error.setDesc(URLUtil.genericErrorMsg);
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			
			generateResponse(response, quizAnswerInfo);
			return response;
			
		} finally {
			String deviceInfo = netspeedStr;
			TrackingUtil.contestValidateAnswerAPITrackingForDeviceTimeTracking(request.getHeader("x-platform"), null, request.getHeader("deviceId"), WebServiceActionType.CONTVALIDATEANS, resMsg, 
					questionNoStr, questionIdStr, answerOption, contestId, customerId, start, new Date(), request.getHeader("X-Forwarded-For"),apiHitStartTimeStr,fbTimeStr,deviceInfo,
					quizAnswerInfo.getSts(),questionNoStr,null,null,description);
			//System.out.println("CASS VALID ANSWER  : "+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :ans: "+answerOption+" :apitime: "+apiHitStartTimeStr+" :fbtime: "+fbTimeStr);
			//log.info("CASS VALID ANSWER IOS: QNO:"+questionNoStr+" : "+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :ans: "+answerOption+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+quizAnswerInfo);
		}
		
		generateResponse(response, quizAnswerInfo);
		return response;

	}
	

public void generateResponse(HttpServletResponse response,ContestValidateAnsInfo quizAnswerInfo) throws ServletException, IOException {
	Map<String, ContestValidateAnsInfo> map = new HashMap<String, ContestValidateAnsInfo>();
	map.put("contestValidateAnsInfo", quizAnswerInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
