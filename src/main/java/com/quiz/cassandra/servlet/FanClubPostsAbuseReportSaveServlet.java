package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.AbuseReportDTO;
import com.quiz.cassandra.list.AbuseReportInfo;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.service.AbuseReportingService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.FanClubUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class FanClubPostsAbuseReportSaveServlet.java
 * API to report abuse on videos / comments
 * 
 */

@WebServlet("/FCPostAbuseReportSave.json")
public class FanClubPostsAbuseReportSaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubPostsAbuseReportSaveServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AbuseReportInfo abuseReportInfo = new AbuseReportInfo();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");	
		
		String srcId = request.getParameter("fcId"); // Video or comment Id
		String cId = request.getParameter("pId");
		String abuseOptSelected = request.getParameter("abId");
		Integer abuseOptionId = null;
		Integer sourceId = null;
		Integer commentId=null;
		
		try {
			System.out.println("[AbuseReport Info]  " + " [cuId] " + customerIdStr );			

						
			try {
				sourceId = Integer.parseInt(srcId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid comment or video  Id:" + srcId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				abuseReportInfo.setErr(error);
				abuseReportInfo.setSts(0);
				generateResponse(response, abuseReportInfo);
				return response;
			}
			
			try {
				if(cId != null && !cId.isEmpty() ) {
					commentId = Integer.parseInt(cId);				
				}				
			} catch (Exception e) {
				resMsg = "Please send valid commentId :" + commentId;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportInfo.setErr(error);
				abuseReportInfo.setSts(0);
				generateResponse(response, abuseReportInfo);
				return response;
			}
			
			try {
				abuseOptionId = Integer.parseInt(abuseOptSelected.trim());
			} catch (Exception e) {
				resMsg = "Please send valid abuseOption :" + abuseOptSelected;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportInfo.setErr(error);
				abuseReportInfo.setSts(0);
				generateResponse(response, abuseReportInfo);
				return response;
			}
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportInfo.setErr(error);
				abuseReportInfo.setSts(0);

				generateResponse(response, abuseReportInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				abuseReportInfo.setErr(error);
				abuseReportInfo.setSts(0);
				generateResponse(response, abuseReportInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportInfo.setErr(error);
				abuseReportInfo.setSts(0);
				generateResponse(response, abuseReportInfo);
				return response;
			}

			abuseReportInfo.setSrcId(sourceId);			
			abuseReportInfo.setCuId(customer.getId());
			abuseReportInfo.setCommentId(commentId);
			abuseReportInfo.setAbuseOptionId(abuseOptionId);
			AbuseReportingService.saveFanClubPostAbuseReport(abuseReportInfo);
			
			if(abuseReportInfo.getSts() == 1) {
				abuseReportInfo.setMsg(FanClubUtil.FANCLUB_POST_ABUSE);
				
			}
			else {
				abuseReportInfo.setSts(0);
				abuseReportInfo.setMsg(URLUtil.genericErrorMsg);
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportInfo.setErr(error);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			abuseReportInfo.setErr(error);
			abuseReportInfo.setSts(0);
			generateResponse(response, abuseReportInfo);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, abuseReportInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, AbuseReportInfo abuseReportInfo)
			throws ServletException, IOException {
		Map<String, AbuseReportInfo> map = new HashMap<String, AbuseReportInfo>();
		map.put("AbuseReportInfo", abuseReportInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
