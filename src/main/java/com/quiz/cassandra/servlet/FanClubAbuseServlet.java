package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.FanClubDTO;
import com.web.util.FanClubUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.dao.implementation.FanClubAbuseSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubSQLDAO;
import com.zonesws.webservices.data.FanClub;
import com.zonesws.webservices.data.FanClubAbuse;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class MyFanClubServlet
 */

@WebServlet("/FanClubAbuse.json")
public class FanClubAbuseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(FanClubAbuseServlet.class);

	public FanClubAbuseServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		FanClubDTO responseDTO = new FanClubDTO();
		CassError error = new CassError();
		Date start = new Date();
		
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm"); 
		String customerIdStr = request.getParameter("cuId");
		String fcIdStr = request.getParameter("fcId");
		String abuseIdStr = request.getParameter("abuseId");
		String appver = request.getParameter("ve");		
		String apprelease = request.getParameter("rel");
		 
		try {
			System.out.println("[ManageFanClubServlet]" + " [cuId] " + customerIdStr + "[fcIdStr]" + fcIdStr + "[abuseIdStr]" + abuseIdStr);
			ApplicationPlatForm applicationPlatForm = null;
			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				responseDTO.setErr(error);
				responseDTO.setSts(0);
				generateResponse(response, responseDTO);
				return response;
			}
 
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}
			
			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				responseDTO.setErr(error);
				responseDTO.setSts(0);
				generateResponse(response, responseDTO);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				responseDTO.setErr(error);
				responseDTO.setSts(0);

				generateResponse(response, responseDTO);
				return response;
			}
			
			Integer fcId = null;
			FanClub fanClub = null;
			try {
				fcId = Integer.parseInt(fcIdStr.trim());
				fanClub = FanClubSQLDAO.getActiveFanClub(fcId,customerId);
				if(null == fanClub) {
					resMsg = "Invalid fan club.";
					error.setDesc(resMsg);
					responseDTO.setErr(error);
					responseDTO.setSts(0);
					generateResponse(response, responseDTO);
					return response;
				}
			}catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Fan Club:" + fcIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				responseDTO.setErr(error);
				responseDTO.setSts(0);
				generateResponse(response, responseDTO);
				return response;
			}
			
			Integer abuseId = null;
			try {
				abuseId = Integer.parseInt(abuseIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Abuse Id:" + abuseIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				responseDTO.setErr(error);
				responseDTO.setSts(0);
				generateResponse(response, responseDTO);
				return response;
			}
			 
			FanClubAbuse abuseObj = new FanClubAbuse();
			abuseObj.setAbuseId(abuseId);
			abuseObj.setCrDate(new Date());
			abuseObj.setCuId(customerId);
			abuseObj.setEventId(null);
			abuseObj.setFcId(fcId);
			abuseObj.setPostId(null);
			FanClubAbuseSQLDAO.saveFanClubAbuse(abuseObj);
			responseDTO.setSts(1);
			responseDTO.setMsg(FanClubUtil.FANCLUB_ABUSE);
		} catch (Exception e) {
			resMsg = "Error occured while updating fanclub abuse. ";
			e.printStackTrace();
			error.setDesc("");
			responseDTO.setErr(error);
			responseDTO.setSts(0);
			generateResponse(response, responseDTO);
			return response;

		} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
		}

		generateResponse(response, responseDTO);
		return response;

	}
  

	public void generateResponse(HttpServletResponse response, FanClubDTO responseDTO)
			throws ServletException, IOException {
		Map<String, FanClubDTO> map = new HashMap<String, FanClubDTO>();
		map.put("fanClubDTO", responseDTO);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
