package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingQuestionInfo;
import com.quiz.cassandra.service.PollingQuestionService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingContestQuestionServlet
 */

@WebServlet("/PollingQuestion.json")
public class PollingQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingQuestionServlet.class);
  
    public PollingQuestionServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	PollingQuestionInfo pollingQuestionInfo =new PollingQuestionInfo();
	CassError error = new CassError();
	Date start = new Date();
	String customerIdStr = request.getParameter("cuId");	
	String platForm = request.getParameter("pfm");	
	String loginIp = request.getParameter("lIp");
	Integer customerId = null;
	String resMsg = "";
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	Integer contestId=null;
	
	System.out.println("[PollingContestAnswer]"  + " [cuId] " + customerIdStr  + " [pfm]" + platForm );
	try {
				
		 
		ApplicationPlatForm applicationPlatForm=null;
		
		if(TextUtil.isEmptyOrNull(platForm))
		{
			resMsg = "Please send valid application platform:"+platForm;
			//error.setDesc("Please send valid application platform");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingQuestionInfo.setErr(error);
			pollingQuestionInfo.setSts(0);
		
			generateResponse(response, pollingQuestionInfo);
			return response;
		}
		
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				resMsg = "Please send valid application platform:"+platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingQuestionInfo.setErr(error);
				pollingQuestionInfo.setSts(0);
			
				generateResponse(response, pollingQuestionInfo);
				return response;
			}
		
		
		String contestType="POLLING";
		if(applicationPlatForm!= null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
			contestType="WEB";
		}
		if(platForm.contains("ANDROID")||platForm.contains("IOS")){
			loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
		}
		
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Invalid Customer Id:"+customerIdStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingQuestionInfo.setErr(error);
			pollingQuestionInfo.setSts(0);
			
			generateResponse(response, pollingQuestionInfo);
			return response;
		}
		
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);		
		if(customer == null) {
			resMsg = "Customer Id is Invalid:"+customerIdStr;
			//error.setDesc("Customer Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingQuestionInfo.setErr(error);
			pollingQuestionInfo.setSts(0);
			
			generateResponse(response, pollingQuestionInfo);
			return response;
		}
		pollingQuestionInfo = PollingQuestionService.processQuestionForCustomer(customer.getId());	   
		pollingQuestionInfo.setoQId(pollingQuestionInfo.getqId());
		pollingQuestionInfo.setqId(pollingQuestionInfo.getId());
		//Above workaround to avoid code changes in Mobile App.
		try {
			String nxtCTxt = PollingUtil.getNextGameDetails();
			pollingQuestionInfo.setNxtCTxt(nxtCTxt);
		}catch(Exception ex) {
			ex.printStackTrace();
			//this exception can be ignored .. 
		}
		
		//pollingQuestionInfo.setSts(1);
		resMsg = "Success:"+customerIdStr+":POLLING:"+contestType;
		
	}catch(Exception e){
		resMsg = "We had Trouble Fetching the MiniGame Question . Please try in some time. ";
		e.printStackTrace();
		//error.setDesc("Error occured while Fetching Polling Question Details.");
		error.setDesc(URLUtil.genericErrorMsg);
		pollingQuestionInfo.setErr(error);
		pollingQuestionInfo.setSts(0);
		

		generateResponse(response, pollingQuestionInfo);
		return response;
		
	} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
	}
	
	
	generateResponse(response, pollingQuestionInfo);
	return response;
	
}


public void generateResponse(HttpServletResponse response,PollingQuestionInfo pollingQuestionInfo) throws ServletException, IOException {
	Map<String, PollingQuestionInfo> map = new HashMap<String, PollingQuestionInfo>();
	map.put("pollingQuestionInfo", pollingQuestionInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
