package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.service.FanClubService;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class FanClubVideoUploadServlet.java
 * API to Upload Fan Club Videos
 * 
 */

@WebServlet("/FanClubVideoUpld.json")
public class FanClubVideoUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubVideoUploadServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Comments vidUpdInfo = new Comments();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		
		String fanClubId = request.getParameter("fcId"); // Fan Club Id
		String commentTxt = request.getParameter("cmtTxt");
		String vUrl = request.getParameter("vUrl");
		String tUrl = request.getParameter("tUrl");
		String imageUrl = request.getParameter("imgUrl");
		String title = request.getParameter("title");
		String catId = request.getParameter("catId");
		String description = request.getParameter("description");
		Integer fcId=null;
		Integer pId = null;
		
		try {
			System.out.println("[Fan Club Post save]  " + " [cuId] " + customerIdStr  + "[fanClubId]" + fanClubId );			

		
			
			try {
				fcId = Integer.parseInt(fanClubId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Fan Club Id:" + fanClubId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				vidUpdInfo.setErr(error);
				vidUpdInfo.setSts(0);
				generateResponse(response, vidUpdInfo);
				return response;
			}			
			
			
			
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				vidUpdInfo.setErr(error);
				vidUpdInfo.setSts(0);

				generateResponse(response, vidUpdInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				vidUpdInfo.setErr(error);
				vidUpdInfo.setSts(0);
				generateResponse(response, vidUpdInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				vidUpdInfo.setErr(error);
				vidUpdInfo.setSts(0);
				generateResponse(response, vidUpdInfo);
				return response;
			}

			vidUpdInfo.setFcId(fcId);				
			vidUpdInfo.setDescription(description);
			vidUpdInfo.settUrl(tUrl);
			vidUpdInfo.setVidUrl(vUrl);
			vidUpdInfo.setTitle(title);
			vidUpdInfo.setCatId(catId);
			vidUpdInfo.setCuId(customerId);
			FanClubService.uploadFanClubVideo(vidUpdInfo , customer);
			if(vidUpdInfo.getSts() == 1) {
				vidUpdInfo.setMsg("SUCCESS");
				
			}
			else {
				vidUpdInfo.setSts(0);
				vidUpdInfo.setMsg(URLUtil.genericErrorMsg);
				error.setDesc(URLUtil.genericErrorMsg);
				vidUpdInfo.setErr(error);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			vidUpdInfo.setErr(error);
			vidUpdInfo.setSts(0);
			generateResponse(response, vidUpdInfo);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, vidUpdInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, Comments vidUpdInfo)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Map<String, Comments> map = new HashMap<String, Comments>();
		map.put("Comments", vidUpdInfo);
		String vidUpdInfoStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(vidUpdInfoStr);
		out.flush();
	}

}
