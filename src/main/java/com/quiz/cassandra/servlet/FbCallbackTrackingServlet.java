package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassFbCallBackTracking;
import com.quiz.cassandra.list.QuizGenericResponse;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContestInfo;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;

/**
 * Servlet implementation class StartContestServlet
 */

@WebServlet("/FirebaseCallBackTracking.json")
public class FbCallbackTrackingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(FbCallbackTrackingServlet.class);
  
    public FbCallbackTrackingServlet() {
        super();       
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	
	QuizGenericResponse quizGenericResponseInfo =new QuizGenericResponse();
	com.zonesws.webservices.data.Error error = new com.zonesws.webservices.data.Error();
	Date start = new Date();
	String resMsg = "";
	String customerIdStr = request.getParameter("customerId");
	String contestIdStr = request.getParameter("contestId");
	String platForm = request.getParameter("platForm");
	String deviceType = request.getParameter("deviceType");
	String loginIp = request.getParameter("loginIp");
	String questionNoStr = request.getParameter("questionNo");
	String osVersionStr = request.getParameter("osVersion");
	String deviceModelStr = request.getParameter("deviceModel");
	String callBackType = request.getParameter("type");
	String message = request.getParameter("message");
	Integer contestId = null;
	Integer customerId = null;
	
	
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			quizGenericResponseInfo.setError(authError);
			quizGenericResponseInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,authError.getDescription());
			return quizGenericResponseInfo;
		}*/
		
		if(TextUtil.isEmptyOrNull(platForm)){
			resMsg = "Application Platform is mandatory.";
			error.setDescription("Application Platform is mandatory.");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			
			generateResponse(response, quizGenericResponseInfo);
			return response;
		}
		ApplicationPlatForm applicationPlatForm=null;
		if(!TextUtil.isEmptyOrNull(platForm)){
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				resMsg = "Please send valid application platform.";
				error.setDescription("Please send valid application platform.");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				
				generateResponse(response, quizGenericResponseInfo);
				return response;
			}
		}
		
		/*if(TextUtil.isEmptyOrNull(questionNoStr)){
			error.setDescription("Question No is mandatory");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Question No is mandatory");
			return quizGenericResponseInfo;
		}*/
		if(TextUtil.isEmptyOrNull(customerIdStr)){
			resMsg = "Customer Id is mandatory.";
			error.setDescription("Customer Id is mandatory.");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			
			generateResponse(response, quizGenericResponseInfo);
			return response;
		}
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			resMsg = "Contest Id is mandatory.";
			error.setDescription("Contest Id is mandatory.");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			
			generateResponse(response, quizGenericResponseInfo);
			return response;
		}
		/*if(TextUtil.isEmptyOrNull(callBackType)){
			error.setDescription("Type is mandatory");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Type is mandatory");
			return quizGenericResponseInfo;
		}*/
		if(platForm.contains("ANDROID")||platForm.contains("IOS")){
			loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
		}
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Customer Id.";
			error.setDescription("Invalid Customer Id.");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			
			generateResponse(response, quizGenericResponseInfo);
			return response;
		}
		
		/*Customer customer = CustomerUtil.getCustomerById(customerId);
		if(customer == null) {
			error.setDescription("Customer Id is Invalid");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Customer Id is Invalid");
			return quizGenericResponseInfo;
		}*/
		try{
			contestId = Integer.parseInt(contestIdStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Contest Id.";
			error.setDescription("Invalid Contest Id.");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			
			generateResponse(response, quizGenericResponseInfo);
			return response;
		}

		CassFbCallBackTracking firebaseTracking = new CassFbCallBackTracking();
		firebaseTracking.setCuId(customerId);
		firebaseTracking.setCoId(contestId);
		if(questionNoStr != null && !questionNoStr.isEmpty()) {
			try {
				firebaseTracking.setqNo(Integer.parseInt(questionNoStr));
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		firebaseTracking.setIpAddr(loginIp);
		firebaseTracking.setdModel(deviceModelStr);
		firebaseTracking.setOsVer(osVersionStr);
		firebaseTracking.setPlatform(platForm);
		firebaseTracking.setCreateDate(new Date().getTime());
		firebaseTracking.setCbType(callBackType);
		firebaseTracking.setMessage(message);
		CassandraDAORegistry.getCassFbCallBackTrackingDAO().save(firebaseTracking);
		
		quizGenericResponseInfo.setStatus(1);
		//TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Success");
	}catch(Exception e){
		e.printStackTrace();
		resMsg = "Error occurred while Updating Firebase Tracking.";
		error.setDescription("Error occurred while Updating Firebase Tracking.");
		quizGenericResponseInfo.setError(error);
		quizGenericResponseInfo.setStatus(0);
		
		generateResponse(response, quizGenericResponseInfo);
		return response;
	
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.FBCALLBACKTRACKING, resMsg, contestId,
				customerId, start, new Date(), loginIp,null,quizGenericResponseInfo.getStatus(),null);
		
		log.info("FB CALLBACK : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	generateResponse(response, quizGenericResponseInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,QuizGenericResponse quizGenericResponseInfo) throws ServletException, IOException {
	Map<String, QuizGenericResponse> map = new HashMap<String, QuizGenericResponse>();
	map.put("quizGenericResponse", quizGenericResponseInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
