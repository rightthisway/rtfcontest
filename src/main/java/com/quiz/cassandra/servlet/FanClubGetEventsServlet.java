package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.FanClubEventInfo;
import com.web.util.GsonUtil;
import com.zonesws.webservices.dao.implementation.FanClubEventInterestSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubEventSQLDAO;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerMediafcEventInfoSaveServlet.java
 * API to report abuse on videos / fcEventInfo
 * 
 */

@WebServlet("/FanClubGetEvents.json")
public class FanClubGetEventsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubGetEventsServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FanClubEventInfo fcEventInfo = new FanClubEventInfo();
		CassError error = new CassError();

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		String deviceType = request.getParameter("dyType");
		
		String pType = request.getParameter("pType");
		String eventIdStr = request.getParameter("fceId");
		String customerIdStr = request.getParameter("cuId");
		String fanClubIdStr = request.getParameter("fcId");
		String pNoStr = request.getParameter("pNo");
		
		Integer customerId = null;
		String resMsg = "";
		Integer fanClubId = null;
		Integer fcEventId = null;
		
		try {
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			ApplicationPlatForm applicationPlatForm = null;			
			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);

				generateResponse(response, fcEventInfo);
				return response;
			}
			if(pType == null || (!pType.equals("FANCLUB") && !pType.equals("EVENT"))) {
				resMsg = "Invalid ProcessType:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcEventInfo.setErr(error);
				fcEventInfo.setSts(0);
				generateResponse(response, fcEventInfo);
				return response;
			}
			if(pType.equals("FANCLUB")) {
				try {
					fanClubId = Integer.parseInt(fanClubIdStr.trim());
				} catch (Exception e) {
					e.printStackTrace();
					resMsg = "Invalid FanClub Id:" + fanClubId;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
				Integer pageNo=0;
				try {
					pageNo = Integer.parseInt(pNoStr.trim());
				} catch (Exception e) {
					e.printStackTrace();
					resMsg = "Invalid PageNo:" + pNoStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
				
				Integer maxRow= 20;
				List<FanClubEvent> fbEventsList = FanClubEventSQLDAO.getAllActiveFanClubEventByFanClubId(fanClubId,pageNo,maxRow);
				if(fbEventsList != null && !fbEventsList.isEmpty()) {
					List<Integer> likedeventIds = FanClubEventInterestSQLDAO.getLikedFanClubEventIdsByCustomerIdAndFanclubId(customerId, fanClubId);
					if(likedeventIds != null) {
						Map<Integer,Boolean> tempMap = new HashMap<Integer,Boolean>();
						for (Integer eId : likedeventIds) {
							tempMap.put(eId, Boolean.TRUE);
						}
						for (FanClubEvent fcEventObj : fbEventsList) {
							if(tempMap.get(fcEventObj.getId()) != null) {
								fcEventObj.setLiked(Boolean.TRUE);
							}
						}
					}
					if(fbEventsList.size() >= maxRow) {
						fcEventInfo.setHme(Boolean.TRUE);
					}
					fcEventInfo.setFceList(fbEventsList);
				} else {
					fcEventInfo.setMsg("No Events Found.");
				}
				
			} else {
				try {
					fcEventId = Integer.parseInt(eventIdStr.trim());
				} catch (Exception e) {
					e.printStackTrace();
					resMsg = "Invalid fcEventId Id:" + eventIdStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
				
				FanClubEvent fcEvent = FanClubEventSQLDAO.getActiveFanClubEventByEventId(fcEventId);
				if (fcEvent == null) {
					resMsg = "Event Id Not Exist:" + customerIdStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcEventInfo.setErr(error);
					fcEventInfo.setSts(0);
					generateResponse(response, fcEventInfo);
					return response;
				}
				Integer fcEventInterestId = FanClubEventInterestSQLDAO.getLikedFanClubEventIdsByEventIdAndCustomerId(customerId, fcEventId);
				if(fcEventInterestId != null) {
					fcEvent.setLiked(Boolean.TRUE);
				}
				fcEventInfo.setFcEvent(fcEvent);
			}
				
			
			fcEventInfo.setSts(1);
			//fcEventInfo.setMsg("FanClub Event created Successfully");
			
		} catch (Exception e) {
			resMsg = "Something Went Wrong While getting FanClub Event";
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			fcEventInfo.setErr(error);
			fcEventInfo.setSts(0);
			generateResponse(response, fcEventInfo);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, fcEventInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, FanClubEventInfo fcEventInfo)
			throws ServletException, IOException {
		Map<String, FanClubEventInfo> map = new HashMap<String, FanClubEventInfo>();
		map.put("fanClubEventInfo", fcEventInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
