package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingRedeemInfo;
import com.quiz.cassandra.service.PollingAnswerService;
import com.quiz.cassandra.service.PollingRedeemService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingRewardsRedeemServlet
 */

@WebServlet("/PollingRewardsRedeem.json")
public class PollingRewardsRedeemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingRewardsRedeemServlet.class);
  
    public PollingRewardsRedeemServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	PollingRedeemInfo pollingRedeemInfo =new PollingRedeemInfo();
	CassError error = new CassError();
	Date start = new Date();

	
	String fromQty = request.getParameter("fmQty");
	String toQty =  request.getParameter("toQty");
	String fromType = request.getParameter("fmType");
	String toType = request.getParameter("toType");
	String customerIdStr = request.getParameter("cuId");	
	String platForm = request.getParameter("pfm");	
	String loginIp = request.getParameter("lIp");
	Integer customerId = null;
	String resMsg = "";
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	
	try {
		
		System.out.println("[PollingReedem Info]" + 
		"[fromQty]" + fromQty  + " [toQty]" + toQty + 
		" [fromType]" + fromType  + " [cuId] " + customerIdStr 
		+ " [toType]" + toType );
		if(TextUtil.isEmptyOrNull(fromQty))
		{
			resMsg = "Please send valid From Reward Type Quantity [fmQty] " ;
			//error.setDesc("Please send valid From Reward Type Quantity [fmQty]");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);		
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		
		if(TextUtil.isEmptyOrNull(toQty))
		{
			resMsg = "Please send valid To Reward Type Quantity [toQty] " ;
			//error.setDesc("Please send valid To Reward Type Quantity [toQty]");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);		
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		if(TextUtil.isEmptyOrNull(toType))
		{
			resMsg = "Please send valid To Reward Type  [toType] " ;
			//error.setDesc("Please send valid To Reward Type [toType]");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);		
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		if(TextUtil.isEmptyOrNull(fromType) )
		{
			resMsg = "Please send valid From Reward Type [fmType] " ;
			//error.setDesc("Please send valid From Reward Type[fmType]");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);		
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		
		
		
		List<String> fromTypList = PollingUtil.getRedeemToTypeList();
		List<String> toTypeList = PollingUtil.getRedeemFromTypeList();
			
			
			/*
			 * Map<String , String> rewardTypeCodeFromTextMap = new HashMap<String ,
			 * String>();
			 * 
			 * rewardTypeCodeFromTextMap.put("hf" , "Hi Five");
			 * rewardTypeCodeFromTextMap.put("gb" , "Gold Bricks");
			 * rewardTypeCodeFromTextMap.put("cb" , "Crystal Ball");
			 * 
			 * 
			 * Map<String , String> rewardTypeCodeToTextMap = new HashMap<String ,
			 * String>();
			 * 
			 * rewardTypeCodeToTextMap.put("lf" , "Lives"); rewardTypeCodeToTextMap.put("rd"
			 * , "Reward Dollars"); rewardTypeCodeToTextMap.put("sf" , "SuperFan Stars");
			 */
		
		if(!fromTypList.contains(fromType))
		{
			resMsg = "Please send valid From Reward Type [fromType]  " ;
			//error.setDesc("Please send valid From Reward Type[fromType]");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);		
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		 
		if(!toTypeList.contains(toType))
		{
			resMsg = "Please send valid To Reward Type  [toType] " ;
			//error.setDesc("Please send valid To Reward Type [toType]");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);		
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		
		 
		ApplicationPlatForm applicationPlatForm=null;
		
		if(TextUtil.isEmptyOrNull(platForm))
		{
			resMsg = "Please send valid application platform:"+platForm;
			//error.setDesc("Please send valid application platform");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);
		
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				resMsg = "Please send valid application platform:"+platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingRedeemInfo.setErr(error);
				pollingRedeemInfo.setSts(0);
			
				generateResponse(response, pollingRedeemInfo);
				return response;
			}
		
		
		String contestType="POLLING";
		if(applicationPlatForm!= null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
			contestType="WEB";
		}
		if(platForm.contains("ANDROID")||platForm.contains("IOS")){
			loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
		}
		
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Invalid Customer Id:"+customerIdStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);			
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);		
		if(customer == null) {
			resMsg = "Customer Id is not Registered:"+customerIdStr;
			//error.setDesc("Customer Id is not Registered");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingRedeemInfo.setErr(error);
			pollingRedeemInfo.setSts(0);
			
			generateResponse(response, pollingRedeemInfo);
			return response;
		}
		pollingRedeemInfo.setCuId(customer.getId());
		pollingRedeemInfo.setFmQty(Integer.valueOf(fromQty));
		pollingRedeemInfo.setFmRwdTy(fromType);
		pollingRedeemInfo.setToRwdTy(toType);
		pollingRedeemInfo.setToQty(Double.valueOf(toQty));
		
		PollingRedeemService.processCustPollingRwdsRedeem(pollingRedeemInfo);
		
		//updatePollingRewardRedeemResponse(pollingRedeemInfo , rewardTypeCodeToTextMap , rewardTypeCodeFromTextMap);
		//pollingRedeemInfo.setSts(1);
		resMsg = "Success:"+customerIdStr+":POLLING:"+contestType;
		
	}catch(Exception e){
		resMsg = "Error occured while Fetching Polling Answer Validation Details. ";
		e.printStackTrace();
		//error.setDesc("Error occured while Fetching Polling Answer Validation Details.");
		error.setDesc(URLUtil.genericErrorMsg);
		pollingRedeemInfo.setErr(error);
		pollingRedeemInfo.setSts(0);
		generateResponse(response, pollingRedeemInfo);
		return response;
		
	} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
	}
	

	generateResponse(response, pollingRedeemInfo);
	return response;
	
}


private PollingRedeemInfo updatePollingRewardRedeemResponse(PollingRedeemInfo pollingRedeemInfo, Map rwdCodeToMap,  Map rwdCodeFromMap) {		
	String fromRewardType = pollingRedeemInfo.getFmRwdTy();
	String toRewardType= pollingRedeemInfo.getToRwdTy();
	//String fromRewardTypeTxt = (String)rwdCodeMap.get(fromRewardType);
	String toRewardTypeTxt = (String)rwdCodeToMap.get(toRewardType);
	
	String fromQty =String.valueOf(pollingRedeemInfo.getFmQty());
	String toQty = String.valueOf(pollingRedeemInfo.getToQty());
	String sucMsg ="Congratulations !! Tou have succesfully redeemed "
			+ ""  + pollingRedeemInfo.getToQty()   + "  "
					+ "" + toRewardTypeTxt;
	pollingRedeemInfo.setSucsMsg(sucMsg);
	return pollingRedeemInfo;
}


public void generateResponse(HttpServletResponse response,PollingRedeemInfo pollingRedeemInfo) throws ServletException, IOException {
	Map<String, PollingRedeemInfo> map = new HashMap<String, PollingRedeemInfo>();
	map.put("pollingRedeemInfo", pollingRedeemInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsonPollingRedeemInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsonPollingRedeemInfo);
    out.flush(); 
}



private class RedeemUserInfo {
	
}

}
