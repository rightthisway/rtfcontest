package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.FanClubPostsDTO;
import com.quiz.cassandra.service.FanClubService;
import com.web.util.FanClubUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class FanClubSaveCommentsServlet.java
 * API to Save Fan Club Comments, videos, images
 * 
 */

@WebServlet("/FanClubSaveVidComments.json")
public class FanClubSaveCommentsOnVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubSaveCommentsOnVideoServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FanClubPostsDTO fanClubPostsDTO = new FanClubPostsDTO();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		
		String fanClubId = request.getParameter("fcId"); // Fan Club Id
		String videoId = request.getParameter("vId");
		String commentTxt = request.getParameter("cmtTxt");
		String vUrl = request.getParameter("vUrl");
		String tUrl = request.getParameter("tUrl");
		String imageUrl = request.getParameter("imgUrl");
		Integer fcId=null;
		Integer vId = null;
		
		try {
			System.out.println("[Fan Club Posts Comments save]  " + " [cuId] " + customerIdStr  + "  [fanClubId]" + fanClubId  + "   [videoId]" + videoId);			

			try {
				vId = Integer.parseInt(videoId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid videoId Id:" + videoId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}
			
			
			try {
				fcId = Integer.parseInt(fanClubId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Fan Club Id:" + fanClubId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}
			
			
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);

				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			fanClubPostsDTO.setFcId(fcId);			
			fanClubPostsDTO.setCmtTxt(commentTxt);
			fanClubPostsDTO.settUrl(tUrl);
			fanClubPostsDTO.setVidUrl(vUrl);
			fanClubPostsDTO.setImgUrl(imageUrl);
			fanClubPostsDTO.setCuId(customerId);
			fanClubPostsDTO.setvId(vId);
			FanClubService.saveCommentsOnFanClubVideos(fanClubPostsDTO);
			if(fanClubPostsDTO.getSts() == 1) {
				fanClubPostsDTO.setMsg(FanClubUtil.FANCLUB_VIDEO_CMMT_ABUSE);
				error.setDesc(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				
			}
			else {
				fanClubPostsDTO.setSts(0);
				fanClubPostsDTO.setMsg(URLUtil.genericErrorMsg);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			fanClubPostsDTO.setErr(error);
			fanClubPostsDTO.setSts(0);
			generateResponse(response, fanClubPostsDTO);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, fanClubPostsDTO);
		return response;
	}

	public void generateResponse(HttpServletResponse response, FanClubPostsDTO fanClubPostsDTO)
			throws ServletException, IOException {
		Map<String, FanClubPostsDTO> map = new HashMap<String, FanClubPostsDTO>();
		map.put("FanClubPostsDTO", fanClubPostsDTO);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
