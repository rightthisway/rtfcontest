package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContSummaryInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.ContestJackpotType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class MiniJackpotWinnersServlet
 */

@WebServlet("/GetMiniJackpotWinners.json")
public class MiniJackpotWinnersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(GrandWinnerServlet.class);
  
    public MiniJackpotWinnersServlet() {
        super();       
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	ContSummaryInfo contSummary =new ContSummaryInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dType");
	String contestIdStr = request.getParameter("coId");
	String qIdStr = request.getParameter("qId");
	Integer contestId = null;
	try {
		 
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			resMsg="Contest Id is mandatory:"+contestIdStr;
			//error.setDesc("Contest Id is mandatory");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			generateResponse(response, contSummary);
			return response;
		}
		
		if(TextUtil.isEmptyOrNull(qIdStr)){
			resMsg="Question Id is mandatory:"+qIdStr;
			//error.setDesc("Question Id is mandatory");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			generateResponse(response, contSummary);
			return response;
		}
		Integer qId = Integer.parseInt(qIdStr);
		
		QuizContestQuestions question = CassContestUtil.getQuizContestQuestionById(qId);
		if(question == null) {
			resMsg="Question Id is Invalid:"+qIdStr;
			//error.setDesc("Question Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			generateResponse(response, contSummary);
			return response;
		}
		
		QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
		if(quizContest == null) {
			resMsg="Contest Id is Invalid:"+contestIdStr;
			//error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			
			generateResponse(response, contSummary);
			return response;
		}
		
		if(null != quizContest.getContestJackpotType() && !quizContest.getContestJackpotType().equals(ContestJackpotType.MINI)) {
			resMsg="Mini Jackpot not applicable for this contest:"+contestIdStr;
			//error.setDesc("Mini Jackpot not applicable for this contest");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			generateResponse(response, contSummary);
			return response;
		}else {
			//Proceed
		}
		contestId = quizContest.getId();
		
		List<CassContestWinners> grandWinners = CassContestUtil.getContestMiniJackpotGrandWinners(quizContest.getId(),qId);
		System.out.println("MINI JACKPOT: CID:"+contestId+", QID:"+qId+", WINNER SIZE: "+grandWinners.size());
		if(grandWinners == null || grandWinners.isEmpty()) {
			Date startGrand = new Date();
			System.out.println("MINI JACKPOT LOTTERY BEGIN");
			grandWinners = CassContestUtil.computeContestJackpotWinners(Integer.parseInt(contestIdStr),qId,question.getQuestionSNo());
			log.info("Time Mini Jackpot Winner Full Compute : "+(new Date().getTime()-startGrand.getTime())+" : "+ new Date());
		} else {
			log.info("Mini Jackpot Winner Else  : "+(new Date().getTime()));
		}
		contSummary.setWinners(grandWinners);
		contSummary.setSts(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);
		resMsg="Success:"+contestIdStr;
	}catch(Exception e){
		e.printStackTrace();
		resMsg = "Error occured while Fetching MINIJACKPOT Winners.";
		//error.setDesc("Error occured while Fetching MINIJACKPOT Winners.");
		error.setDesc(URLUtil.genericErrorMsg);
		contSummary.setErr(error);
		contSummary.setSts(0);
		
		generateResponse(response, contSummary);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.QUIZMINIJACKPOT, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,contSummary.getSts(),null);
		log.info("CASS GRANDWINNER : : coId: "+request.getParameter("coId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	
	generateResponse(response, contSummary);
	return response;


}

public void generateResponse(HttpServletResponse response,ContSummaryInfo contSummary) throws ServletException, IOException {
	Map<String, ContSummaryInfo> map = new HashMap<String, ContSummaryInfo>();
	map.put("contSummaryInfo", contSummary);
	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
}