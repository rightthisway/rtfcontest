package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.FanClubEventInfo;
import com.quiz.cassandra.list.FanClubEventIntInfo;
import com.quiz.cassandra.list.FanClubMemberInfo;
import com.web.util.GsonUtil;
import com.zonesws.webservices.dao.implementation.FanClubEventInterestSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubEventSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubMembersSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubSQLDAO;
import com.zonesws.webservices.data.FanClub;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.data.FanClubEventInterest;
import com.zonesws.webservices.data.FanClubMembers;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerMediafcMemberInfoSaveServlet.java
 * API to report abuse on videos / fcMemberInfo
 * 
 */

@WebServlet("/FanClubMembers.json")
public class FanClubMembersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubMembersServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FanClubMemberInfo fcMemberInfo = new FanClubMemberInfo();
		CassError error = new CassError();

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		String deviceType = request.getParameter("dyType");
		
		String pType = request.getParameter("pType");
		String fanClubIdStr = request.getParameter("fcId");
		String customerIdStr = request.getParameter("cuId");
		
		Integer customerId = null;
		String resMsg = "";
		Integer fanClubId = null;
		
		try {
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			ApplicationPlatForm applicationPlatForm = null;			
			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				fcMemberInfo.setErr(error);
				fcMemberInfo.setSts(0);

				generateResponse(response, fcMemberInfo);
				return response;
			}
			if(pType == null || (!pType.equals("JOIN") && !pType.equals("EXIT"))) {
				resMsg = "Invalid ProcessType:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcMemberInfo.setErr(error);
				fcMemberInfo.setSts(0);
				generateResponse(response, fcMemberInfo);
				return response;
			}
			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcMemberInfo.setErr(error);
				fcMemberInfo.setSts(0);
				generateResponse(response, fcMemberInfo);
				return response;
			}
			try {
				fanClubId = Integer.parseInt(fanClubIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid FanClub Id:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcMemberInfo.setErr(error);
				fcMemberInfo.setSts(0);
				generateResponse(response, fcMemberInfo);
				return response;
			}
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fcMemberInfo.setErr(error);
				fcMemberInfo.setSts(0);
				generateResponse(response, fcMemberInfo);
				return response;
			}
			if(pType.equals("JOIN")) {
				FanClub fanClub = FanClubSQLDAO.getActiveFanClub(fanClubId,customerId);
				if(fanClub == null) {
					resMsg = "FanClub Id is not Registered:" + customerIdStr;
					error.setDesc(URLUtil.genericErrorMsg);
					fcMemberInfo.setErr(error);
					fcMemberInfo.setSts(0);
					generateResponse(response, fcMemberInfo);
					return response;
				}
				FanClubMembers fcMembers = new FanClubMembers();
				fcMembers.setFcId(fanClubId);
				fcMembers.setCuId(customerId);
				fcMembers.setStatus("JOIN");
				fcMembers.setJnDate(new Date());
				
				fcMembers = FanClubMembersSQLDAO.saveFanClubMembers(fcMembers);
				if(fcMembers.getId() == null) {
					resMsg = "fcMembers Not Created something Went Wrong.";
					error.setDesc(URLUtil.genericErrorMsg);
					fcMemberInfo.setErr(error);
					fcMemberInfo.setSts(0);
					generateResponse(response, fcMemberInfo);
					return response;
				}
				FanClubSQLDAO.updateFanClubMembersContForJoin(fanClubId);
				
				fcMemberInfo.setMsg("FanClub Member Updated Successfully");
			} else {
				FanClubMembers fcMember = FanClubMembersSQLDAO.getJoinedFanClubMembersByCustomerIdAndFanClubId(customerId, fanClubId);
				if(fcMember == null) {
					resMsg = "FC Members Not Exit.";
					error.setDesc(URLUtil.genericErrorMsg);
					fcMemberInfo.setErr(error);
					fcMemberInfo.setSts(0);
					generateResponse(response, fcMemberInfo);
					return response;
				}
				fcMember.setStatus("EXIT");
				fcMember.setExDate(new Date());
				FanClubMembersSQLDAO.updateFanClubMemberExit(fcMember);
				
				FanClubSQLDAO.updateFanClubMembersContForExit(fanClubId);
				
				fcMemberInfo.setMsg("FanClub Member Removed Successfully");
			}
				
			
			fcMemberInfo.setSts(1);
			
			
		} catch (Exception e) {
			resMsg = "Something Went Wrong While Updating FanClubMember";
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			fcMemberInfo.setErr(error);
			fcMemberInfo.setSts(0);
			generateResponse(response, fcMemberInfo);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, fcMemberInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, FanClubMemberInfo fcMemberInfo)
			throws ServletException, IOException {
		Map<String, FanClubMemberInfo> map = new HashMap<String, FanClubMemberInfo>();
		map.put("fanClubMemberInfo", fcMemberInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
