package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.AbuseReportDTO;
import com.quiz.cassandra.list.AbuseReportInfo;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.service.AbuseReportingService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class FanClubPostsAbuseReportFormServlet.java
 * API to report abuse on Fan Club Post comments
 * 
 */

@WebServlet("/FCPostAbuseReportForm.json")
public class FanClubPostsAbuseReportFormServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubPostsAbuseReportFormServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AbuseReportDTO abuseReportDTO = new AbuseReportDTO();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		
		
		String fanClubId = request.getParameter("fcId");		
		
		String cId = request.getParameter("pId"); // Comment Id
		Integer commentId=null;
		Integer fcId=null;
		
		try {
			System.out.println("[FanClub Comment Abuse  Info]  " + " [cuId] " + customerIdStr + "[fanClubId ] " + fanClubId );			try {
				fcId = Integer.parseInt(fanClubId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid comment or video  Id:" + fcId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);
				generateResponse(response, abuseReportDTO);
				return response;
			}
			
			try {
				if(cId != null && !cId.isEmpty() ) {
					commentId = Integer.parseInt(cId);				
				}				
			} catch (Exception e) {
				resMsg = "Please send valid commentId :" + commentId;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);
				generateResponse(response, abuseReportDTO);
				return response;
			}
			
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);

				generateResponse(response, abuseReportDTO);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);
				generateResponse(response, abuseReportDTO);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportDTO.setErr(error);
				abuseReportDTO.setSts(0);
				generateResponse(response, abuseReportDTO);
				return response;
			}

			abuseReportDTO.setSrcId(fcId);			
			abuseReportDTO.setCuId(customer.getId());
			abuseReportDTO.setCommentId(commentId);
			AbuseReportingService.fetchAbuseReportOptionsForFanClubPost(abuseReportDTO);
			
			if(abuseReportDTO.getSts() == 1) {
				abuseReportDTO.setMsg("SUCCESS");
				
			}
			else {
				abuseReportDTO.setSts(0);
				abuseReportDTO.setMsg(URLUtil.genericErrorMsg);
				error.setDesc(URLUtil.genericErrorMsg);
				abuseReportDTO.setErr(error);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			abuseReportDTO.setErr(error);
			abuseReportDTO.setSts(0);
			generateResponse(response, abuseReportDTO);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, abuseReportDTO);
		return response;
	}

	public void generateResponse(HttpServletResponse response, AbuseReportDTO abuseReportDTO)
			throws ServletException, IOException {
		Map<String, AbuseReportDTO> map = new HashMap<String, AbuseReportDTO>();
		map.put("AbuseReportDTO", abuseReportDTO);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
