package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CommonRespInfo;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContestValidateAnsInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/RemoveCassContCacheData.json")
public class RemoveContestCacheDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(RemoveContestCacheDataServlet.class);
  
    public RemoveContestCacheDataServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	CommonRespInfo commonRespInfo = new CommonRespInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	String contestIDStr = request.getParameter("coId");
	String clearDbDataStr = request.getParameter("clearDbData");
	Integer contestId = null;
	try {
		//String platForm = request.getParameter("platForm");
		//String productTypeStr = request.getParameter("productType");
		if(TextUtil.isEmptyOrNull(contestIDStr)){
			resMsg = "Contest Id is mandatory";
			//error.setDesc(resMsg);
			error.setDesc(URLUtil.genericErrorMsg);
			commonRespInfo.setErr(error);
			commonRespInfo.setSts(0);
			
			generateResponse(response, commonRespInfo);
			return response;
		}
		QuizContest contest = CassContestUtil.getContestByIDForStartContest(contestId);
		//QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIDStr));
		if(contest == null) {
			resMsg ="Contest Id is Invalid";
			//error.setDesc(resMsg);
			error.setDesc(URLUtil.genericErrorMsg);
			commonRespInfo.setErr(error);
			commonRespInfo.setSts(0);
			
			generateResponse(response, commonRespInfo);
			return response;
		}
		contestId = contest.getId();
		
		log.info("RESETCONTESTAPI: REMOVING DATA FROM CACHE- Begins:"+new Date());
		CassContestUtil.clearCacheDataByContestId(Integer.parseInt(contestIDStr));
//Fix This		
		//QuizContestUtil.clearCacheDataByContestId(Integer.parseInt(contestIDStr));
		log.info("RESETCONTESTAPI: REMOVING DATA FROM CACHE- Ends:"+new Date());
//Fix This		
		String cassDataRemoved = "No";
		if(clearDbDataStr == null || clearDbDataStr.equals("Y")) {
			CassContestUtil.truncateCassandraContestData(contestIDStr);
			cassDataRemoved  = "Yes";
		}
		
		
		resMsg = "successfully:cassDataRemoved:"+cassDataRemoved+":"+clearDbDataStr;
		commonRespInfo.setMsg("Contest Cache Data Removed successfully:cassDataRemoved:"+cassDataRemoved);
		//quizCustomerDetails.setError(error);
		commonRespInfo.setSts(1);
	}catch(Exception e){
		resMsg = "Error occured while removing contest cache data :";
		e.printStackTrace();
		//error.setDesc(resMsg);
		error.setDesc(URLUtil.genericErrorMsg);
		commonRespInfo.setErr(error);
		commonRespInfo.setSts(0);
		//TrackingUtils.contestAPITracking(request, WebServiceActionType.REMOVECASSCONTESTCACHE,"Error Occured While Removing Contest Cache Data.");
		
		generateResponse(response, commonRespInfo);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.REMOVECASSCONTESTCACHE, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,commonRespInfo.getSts(),null);
		log.info("CASS REMOVE CACHE : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+":"+resMsg);
	}
	
	generateResponse(response, commonRespInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,CommonRespInfo commonRespInfo) throws ServletException, IOException {
	Map<String, CommonRespInfo> map = new HashMap<String, CommonRespInfo>();
	map.put("commonRespInfo", commonRespInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
