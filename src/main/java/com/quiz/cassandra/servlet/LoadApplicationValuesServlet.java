package com.quiz.cassandra.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CommonRespInfo;
import com.quiz.cassandra.thread.MegaJackpotThread;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.RTFBotsUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

//@WebServlet(name="InitializeResources", urlPatterns="/LoadAppValue.json", loadOnStartup=1)
@WebServlet(urlPatterns="/LoadAppValue.json",loadOnStartup=1)
//@WebServlet("/LoadAppValue.json")
public class LoadApplicationValuesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(LoadApplicationValuesServlet.class);
  
    public LoadApplicationValuesServlet() {
        super();       
    }
	
    @Override
    public void init() throws ServletException {
        
    	try {
    		
    		executeLoadApppValues();
    		
    		System.out.println("LoadApp values Initialization Executed Successfully : "+ new Date());
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		System.out.println("Error Occured in LoadApp Values initialization : "+new Date());
		}
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	public static void executeLoadApppValues() throws Exception { 
		
		CassContestUtil.loadApplicationValues();
		RTFBotsUtil.init();
		RTFBotsUtil.initJackpotBots();
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	
	CommonRespInfo commonRespInfo =new CommonRespInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	
	Integer contestId = null;
	Integer customerId = null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			resMsg = authError.getDescription();
			applyLifeInfo.setErr(authError);
			applyLifeInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
			return applyLifeInfo;
		}*/
		
		executeLoadApppValues();
		
		
		commonRespInfo.setSts(1);
		commonRespInfo.setMsg("Application Values Loaded Successfully.");
		//quizContestDetails.setQuizCustomer(quizCustomer);
		resMsg = "Success:";
	}catch(Exception e){
		resMsg = "Error occured while Loading Application Values.";
		e.printStackTrace();
		//error.setDesc("Error occured while Loading Application Values.");
		error.setDesc(URLUtil.genericErrorMsg);
		commonRespInfo.setErr(error);
		commonRespInfo.setSts(0);
		
		generateResponse(response, commonRespInfo);
		return response;
		
	} finally {
		TrackingUtil.contestAPITracking(null, null, request.getHeader("deviceId"), WebServiceActionType.LOADAPPLICATIONVALUES, resMsg, contestId,
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null,commonRespInfo.getSts(),null);
		log.info("LOAD APP VALUES: "+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	generateResponse(response, commonRespInfo);
	return response;
}

public void generateResponse(HttpServletResponse response,CommonRespInfo commonRespInfo) throws ServletException, IOException {
	Map<String, CommonRespInfo> map = new HashMap<String, CommonRespInfo>();
	map.put("commonRespInfo", commonRespInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	

public static void main(String[] args) {
	String str = System.getProperty( "catalina.base" ) ;
	System.out.println("str================== :"+str);
	
	File file = new File("C:\\Tomcat 7.0\\conf\\server.xml");
	getTomcatPortFromConfigXml(file);
}
public static Integer getTomcatPortFromConfigXml(File serverXml) {
	 
	  Integer port;
	  try {
	    DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
	    domFactory.setNamespaceAware(true); // never forget this!
	    DocumentBuilder builder = domFactory.newDocumentBuilder();
	    Document doc = builder.parse(serverXml);
	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile("/Server/Service[@name='Catalina']/Connector[count(@scheme)=0]/@port[1]");
	    String result = (String) expr.evaluate(doc, XPathConstants.STRING);
	    port =  result != null && result.length() > 0 ? Integer.valueOf(result) : null;
	  } catch (Exception e) {
	    port = null;
	  }
	  System.out.println("Port : "+port);
	  return port;
	}

}
