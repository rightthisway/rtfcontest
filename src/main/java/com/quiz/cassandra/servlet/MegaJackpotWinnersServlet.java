package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContSummaryInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class MegaJackpotWinnersServlet
 */

@WebServlet("/GetMegaJackpotWinners.json")
public class MegaJackpotWinnersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(GrandWinnerServlet.class);
  
    public MegaJackpotWinnersServlet() {
        super();       
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	ContSummaryInfo contSummary =new ContSummaryInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dType");
	String contestIdStr = request.getParameter("coId");
	Integer contestId = null;
	try {
		 
		if(TextUtil.isEmptyOrNull(contestIdStr)){
			resMsg="Contest Id is mandatory:"+contestIdStr;
			//error.setDesc("Contest Id is mandatory");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			generateResponse(response, contSummary);
			return response;
		}
		
		
		QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
		if(quizContest == null) {
			resMsg="Contest Id is Invalid:"+contestIdStr;
			//error.setDesc("Contest Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			contSummary.setErr(error);
			contSummary.setSts(0);
			generateResponse(response, contSummary);
			return response;
		}
		contestId = quizContest.getId();
		 
		List<CassContestWinners> grandWinners = CassContestUtil.getContestMegaJackpotGrandWinners(contestId);
		System.out.println("MEGA JACKPOT VIEW: CID:"+contestId+", QWSIZE: "+grandWinners.size());
		if(grandWinners == null || grandWinners.isEmpty()) {
			Date startGrand = new Date();
			log.info("Time MEGA Jackpot Winner Full Compute : "+(new Date().getTime()-startGrand.getTime())+" : "+ new Date());
		} else {
			
			try {
				grandWinners.sort((CassContestWinners cw1, CassContestWinners cw2)->cw1.getJackpotSt().compareTo(cw2.getJackpotSt()));
			}catch(Exception e) {
				e.printStackTrace();
			}
			log.info("MEGA Jackpot Winner Else  : "+(new Date().getTime()));
		}
		contSummary.setWinners(grandWinners);
		contSummary.setSts(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);
		resMsg="Success:"+contestIdStr;
	}catch(Exception e){
		resMsg ="Error occured while Fetching MEGAJACKPOTWINNERS Winners.";
		e.printStackTrace();
		//error.setDesc("Error occured while Fetching MEGAJACKPOTWINNERS Winners.");
		error.setDesc(URLUtil.genericErrorMsg);
		contSummary.setErr(error);
		contSummary.setSts(0);
		
		generateResponse(response, contSummary);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.QUIZMEGAJACKPOT, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,contSummary.getSts(),null);
		log.info("CASS MEGA JACKPOT WINNER : coId: "+request.getParameter("coId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	
	generateResponse(response, contSummary);
	return response;


}

public void generateResponse(HttpServletResponse response,ContSummaryInfo contSummary) throws ServletException, IOException {
	Map<String, ContSummaryInfo> map = new HashMap<String, ContSummaryInfo>();
	map.put("contSummaryInfo", contSummary);
	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
}