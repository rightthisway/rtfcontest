package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.PollingVideoPlayedInfo;
import com.quiz.cassandra.service.PollingVideoUrlService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;

/**
 * Servlet implementation class PollingVideoPlayed
 */

@WebServlet("/PollingVideoPlayed.json")
public class PollingVideoPlayedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingVideoPlayedServlet.class);

	public PollingVideoPlayedServlet() {
		super();
	}

	/*
	 * protected void doGet(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { process(request, response);
	 * 
	 * }
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PollingVideoPlayedInfo pollingVideoPlayedInfo = new PollingVideoPlayedInfo();
		CassError error = new CassError();
		Date start = new Date();
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		String vId = request.getParameter("vId");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		Integer contestId = null;

		try {

			System.out.println("[PollingVideoPlayedInfo]" + " [cuId] " + customerIdStr + " [pfm]" + platForm);

			ApplicationPlatForm applicationPlatForm = null;

			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc("Please send valid application platform");
				pollingVideoPlayedInfo.setErr(error);
				pollingVideoPlayedInfo.setSts(0);

				generateResponse(response, pollingVideoPlayedInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc("Please send valid application platform");
				pollingVideoPlayedInfo.setErr(error);
				pollingVideoPlayedInfo.setSts(0);

				generateResponse(response, pollingVideoPlayedInfo);
				return response;
			}

			if (TextUtil.isEmptyOrNull(vId)) {
				resMsg = "Please send valid videoId" + vId;
				error.setDesc("Please send valid videoId");
				pollingVideoPlayedInfo.setErr(error);
				pollingVideoPlayedInfo.setSts(0);

//				generateResponse(response, pollingVideoInfo);
//				return response;
			}
			pollingVideoPlayedInfo.setVid(Integer.parseInt(vId));
			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				pollingVideoPlayedInfo.setErr(error);
				pollingVideoPlayedInfo.setSts(0);
				generateResponse(response, pollingVideoPlayedInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc("Customer Id is not Registered");
				pollingVideoPlayedInfo.setErr(error);
				pollingVideoPlayedInfo.setSts(0);

				generateResponse(response, pollingVideoPlayedInfo);
				return response;
			}

			updatePollingVideoPlayedInfo(customer, pollingVideoPlayedInfo);

			resMsg = "Success:" + customerIdStr + ":POLLING:" + contestType;

		} catch (Exception e) {
			resMsg = "Error occured while Updating Video played info. ";
			e.printStackTrace();
			error.setDesc("Error occured while Updating Video played info.");
			pollingVideoPlayedInfo.setErr(error);
			pollingVideoPlayedInfo.setSts(0);
			generateResponse(response, pollingVideoPlayedInfo);
			return response;

		} finally {

		}

		generateResponse(response, pollingVideoPlayedInfo);
		return response;

	}

	private PollingVideoPlayedInfo updatePollingVideoPlayedInfo(CassCustomer customer,
			PollingVideoPlayedInfo pollingVideoPlayedInfo) throws Exception {

		pollingVideoPlayedInfo = PollingVideoUrlService.processVideoPlayedForCustomer(customer, pollingVideoPlayedInfo);
		pollingVideoPlayedInfo.setSts(1);
		pollingVideoPlayedInfo.setMsg(PollingUtil.MOBILE_MSG_SUCCESS);
		return pollingVideoPlayedInfo;
	}

	public void generateResponse(HttpServletResponse response, PollingVideoPlayedInfo pollingVideoInfo)
			throws ServletException, IOException {
		Map<String, PollingVideoPlayedInfo> map = new HashMap<String, PollingVideoPlayedInfo>();
		map.put("pollingVideoInfo", pollingVideoInfo);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
