package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.ApplyMagicWandInfo;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.service.PollingApplyMagicWandService;
import com.quiz.cassandra.utils.PollingUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingApplyMagicWandServlet.java
 */

@WebServlet("/ApplyMagicwand.json")
public class PollingApplyMagicWandServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingApplyMagicWandServlet.class);

	public PollingApplyMagicWandServlet() {
		super();
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ApplyMagicWandInfo magicWandInfo = new ApplyMagicWandInfo();
		CassError error = new CassError();
		Date start = new Date();
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");		
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time

		try {

			System.out.println("[magicwand Info]  " + " [cuId] " + customerIdStr + "  [coId]  " + contestIdStr);

			ApplicationPlatForm applicationPlatForm = null;

			if (TextUtil.isEmptyOrNull(contestIdStr)) {
				resMsg = "Please send valid contest Id";
				//error.setDesc("Please send valid contest Id");
				error.setDesc(URLUtil.genericErrorMsg);
				magicWandInfo.setErr(error);
				magicWandInfo.setSts(0);
				generateResponse(response, magicWandInfo);
				return response;
			}

			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				magicWandInfo.setErr(error);
				magicWandInfo.setSts(0);

				generateResponse(response, magicWandInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				magicWandInfo.setErr(error);
				magicWandInfo.setSts(0);

				generateResponse(response, magicWandInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				magicWandInfo.setErr(error);
				magicWandInfo.setSts(0);
				generateResponse(response, magicWandInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				magicWandInfo.setErr(error);
				magicWandInfo.setSts(0);
				generateResponse(response, magicWandInfo);
				return response;
			}

			Integer mwQty = PollingUtil.getZeroIfNull(customer.getMw());
			if (mwQty < 1) {
				resMsg = "Magic Wand is not Available for customer" + customerIdStr;
				//error.setDesc("Magic Wand is not Available for customer");
				error.setDesc(URLUtil.genericErrorMsg);
				magicWandInfo.setErr(error);
				magicWandInfo.setSts(0);
				generateResponse(response, magicWandInfo);
				return response;
			}

			updateApplyMagicWandResponse(magicWandInfo, customer);

			magicWandInfo.setSts(1);
			resMsg = "Success:" + customerIdStr + ":POLLING:" + contestType;

		} catch (Exception e) {
			resMsg = "Error occured while Applying Magic Wand ";
			e.printStackTrace();
			//error.setDesc("Error occured while Applying Magic Wand");
			error.setDesc(URLUtil.genericErrorMsg);
			magicWandInfo.setErr(error);
			magicWandInfo.setSts(0);
			generateResponse(response, magicWandInfo);
			return response;
			
			
		} finally {
			try {
				Integer i_coId = Integer.parseInt(contestIdStr);
			
			TrackingUtil.contestValidateAnswerAPITrackingForDeviceTimeTracking(
					request.getHeader("x-platform"), null, request.getHeader("deviceId"), 
					WebServiceActionType.APPLYMAGICWAND, resMsg, 
					"-", "-", "", i_coId, customerId, start, new Date(), request.getHeader("X-Forwarded-For"),apiHitStartTimeStr,null,null,
					magicWandInfo.getSts(),"-",1,null,resMsg);
			}catch(Exception exc) {				
				exc.printStackTrace();
			}
			 
		}
		

		generateResponse(response, magicWandInfo);
		return response;

	}

	private ApplyMagicWandInfo updateApplyMagicWandResponse(ApplyMagicWandInfo magicWandInfo, CassCustomer cassCustomer)
			throws Exception {

		magicWandInfo = PollingApplyMagicWandService.processApplyMagicWand(magicWandInfo, cassCustomer);
		return magicWandInfo;
	}

	public void generateResponse(HttpServletResponse response, ApplyMagicWandInfo magicWandInfo)
			throws ServletException, IOException {
		Map<String, ApplyMagicWandInfo> map = new HashMap<String, ApplyMagicWandInfo>();
		map.put("MagicwandInfo", magicWandInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}


}
