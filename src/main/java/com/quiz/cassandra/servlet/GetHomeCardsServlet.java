package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.list.Card;
import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.list.EventCards;
import com.web.util.GsonUtil;

/**
 * Servlet implementation class GetHomeCardsServlet
 */

@WebServlet("/GetHomeCards.json")
public class GetHomeCardsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(GetHomeCardsServlet.class);
  
    public GetHomeCardsServlet() {
        super();       
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EventCards eventList =new EventCards();
		com.zonesws.webservices.data.Error error = new com.zonesws.webservices.data.Error();
		try {
			List<Card> cards = new ArrayList<Card>();
			eventList.setAlertMessage("");
		    eventList.setCards(cards);
			eventList.setStatus(1);
			eventList.setShowMoreButton(false);
		}catch(Exception e){
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Oops something went wrong. Please try search again.");
			eventList.setError(error);
			eventList.setStatus(0);
		}  
		generateResponse(response, eventList);
		return response;
	}
	
	public void generateResponse(HttpServletResponse response,EventCards eventList) throws ServletException, IOException {
		Map<String, EventCards> map = new HashMap<String, EventCards>();
		map.put("eventCards", eventList);
		String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		//System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    out.print(jsondashboardInfo);
	    out.flush(); 
	}
	public static void main(String a[]) {
		System.out.println();			
		
		DashboardInfo dashboardInfo = new DashboardInfo();
		Map<String, DashboardInfo> map = new HashMap<String, DashboardInfo>();
		map.put("dashboardInfo", dashboardInfo);
		String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);
		System.out.print(jsondashboardInfo);
	}
 
}
