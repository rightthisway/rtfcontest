package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContWinnerRewardsInfo;
import com.quiz.cassandra.list.HallOfFameDtls;
import com.quiz.cassandra.list.HallOfFameInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.QuizSummaryType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.SQLDaoUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/GetHallOfFameTemp.json")
public class HallOfFameServletTemp extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(HallOfFameServletTemp.class);
  
    public HallOfFameServletTemp() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	
	HallOfFameInfo hallOfFameInfo =new HallOfFameInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String customerIdStr = request.getParameter("cuId");
	//String contestIdStr = request.getParameter("coId");
	String summaryTypeStr = request.getParameter("type");
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	Integer contestId = null;
	Integer customerId=null;
	try {
		
		QuizSummaryType quizSummaryType = null;
		try {
			quizSummaryType = QuizSummaryType.valueOf(summaryTypeStr);
			
		} catch(Exception e) {
			resMsg = "Summary Type is Invalid";
			//error.setDesc("Summary Type is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			hallOfFameInfo.setErr(error);
			hallOfFameInfo.setSts(0);
			
			generateResponse(response, hallOfFameInfo);
			return response;
		}
		
		/*if(TextUtil.isEmptyOrNull(summaryTypeStr)){
			resMsg = "Summary Type is mandatory";
			error.setDesc("Summary Type is mandatory");
			quizContestSummary.setErr(error);
			quizContestSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
			return quizContestSummary;
		}*/
		
		CassCustomer customer = null;
		if(!TextUtil.isEmptyOrNull(customerIdStr)){
			 
			try {
				customerId = Integer.parseInt(customerIdStr.trim());
				customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(customer == null) {
					resMsg = "Customer Id is Invalid:"+customerIdStr;
					//error.setDesc("Your Cellular or WIFI Network is currently weak.");//Customer Id is Invalid
					//error.setDesc("Sorry, We are experiencing some issues right now. Please try in a few minutes.");//Customer Id is Invalid
					error.setDesc(URLUtil.genericErrorMsg);
					hallOfFameInfo.setErr(error);
					hallOfFameInfo.setSts(0);
					
					generateResponse(response, hallOfFameInfo);
					return response;
				}
			} catch(Exception e) {
				e.printStackTrace();
				resMsg = "Customer Id Not Exist:"+customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				hallOfFameInfo.setErr(error);
				hallOfFameInfo.setSts(0);
				
				generateResponse(response, hallOfFameInfo);
				return response;
			}
		}
					

		if(quizSummaryType.equals(QuizSummaryType.TILLDATE)) {
			
			List<HallOfFameDtls> hallOfFameList = CassContestUtil.getHallOfFameForTillDateData();
			hallOfFameInfo.setList(hallOfFameList);
			if(hallOfFameList != null) {
				hallOfFameInfo.setwCount(hallOfFameList.size());
				
				/*if(platForm != null && platForm.equalsIgnoreCase(ApplicationPlatForm.IOS.toString())) {
					List<HallOfFameDtls> list = new ArrayList<HallOfFameDtls>();
					if(hallOfFameList.size() >= 2) {
						list.add(hallOfFameList.get(1));
						list.add(hallOfFameList.get(0));
						list.addAll(hallOfFameList.subList(2, hallOfFameList.size()));
					} else  {
						list.addAll(hallOfFameList);
					}
					hallOfFameInfo.setList(list);
				} else {
					hallOfFameInfo.setList(hallOfFameList);
				}*/
				
			}
			if(customer != null) {
				HallOfFameDtls hallOfFameDtls = SQLDaoUtil.getTillDateHallofFameByCustomerId(customerId);
				if(hallOfFameDtls == null) {
					hallOfFameDtls = new HallOfFameDtls();
				}
				hallOfFameDtls.setCuId(customer.getId());
				hallOfFameDtls.setuId(customer.getuId());
				hallOfFameDtls.setImgP(customer.getImgP());
				
				hallOfFameInfo.setCustHof(hallOfFameDtls);
			}
			
			
		} else if(quizSummaryType.equals(QuizSummaryType.THISWEEK)) {

			//This Week starts from Sunday to till date
			/*Calendar cal = Calendar.getInstance();
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			dayOfWeek = 1-dayOfWeek;
			cal.add(Calendar.DAY_OF_MONTH, dayOfWeek);
			String startDateStr = dbDateFormat.format(new Date(cal.getTimeInMillis()))+" 00:00:00";
			String toDateStr = dbDateFormat.format(new Date())+" 23:59:59";*/
			
			List<HallOfFameDtls> hallOfFameList = CassContestUtil.getHallOfFameForThisWeekData();
			hallOfFameInfo.setList(hallOfFameList);
			if(hallOfFameList != null) {
				hallOfFameInfo.setwCount(hallOfFameList.size());
				
				/*if(platForm != null && platForm.equalsIgnoreCase(ApplicationPlatForm.IOS.toString())) {
					List<HallOfFameDtls> list = new ArrayList<HallOfFameDtls>();
					if(hallOfFameList.size() >= 2) {
						list.add(hallOfFameList.get(1));
						list.add(hallOfFameList.get(0));
						list.addAll(hallOfFameList.subList(2, hallOfFameList.size()));
					} else  {
						list.addAll(hallOfFameList);
					}
					hallOfFameInfo.setList(list);
				} else {
					hallOfFameInfo.setList(hallOfFameList);
				}*/
			}
			if(customer != null) {
				HallOfFameDtls hallOfFameDtls = SQLDaoUtil.getThisWeekHallofFameByCustomerId(customerId);
				if(hallOfFameDtls == null) {
					hallOfFameDtls = new HallOfFameDtls();
				}
				hallOfFameDtls.setCuId(customer.getId());
				hallOfFameDtls.setuId(customer.getuId());
				hallOfFameDtls.setImgP(customer.getImgP());
				
				hallOfFameInfo.setCustHof(hallOfFameDtls);
			}
			//quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
		}
		
		hallOfFameInfo.setSts(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);
		resMsg = "Success";
	}catch(Exception e){
		resMsg = "Error occurred while Fetching Hall Of Fame Data.";
		e.printStackTrace();
		//error.setDesc("Error occurred while Fetching Hall Of Fame Data.");
		error.setDesc(URLUtil.genericErrorMsg);
		hallOfFameInfo.setErr(error);
		hallOfFameInfo.setSts(0);
		
		generateResponse(response, hallOfFameInfo);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETHALLOFFAME, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,hallOfFameInfo.getSts(),null);
		log.info("CASS HALLOFFAME : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : stype: "+request.getParameter("summaryType")+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
	}

	generateResponse(response, hallOfFameInfo);
	return response;


}

public void generateResponse(HttpServletResponse response,HallOfFameInfo hallOfFameInfo ) throws ServletException, IOException {
	Map<String, HallOfFameInfo> map = new HashMap<String, HallOfFameInfo>();
	map.put("hallOfFameInfo", hallOfFameInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
