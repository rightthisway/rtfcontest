package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.RewardsInfo;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.service.PollingVideoLikeService;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingVideoLikeServlet.java
 */

@WebServlet("/VideoLike.json")
public class PollingVideoLikeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingVideoLikeServlet.class);

	public PollingVideoLikeServlet() {
		super();
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		VideoLikeInfo videoLikeInfo = new VideoLikeInfo();
		CassError error = new CassError();
		
		String customerIdStr = request.getParameter("cuId");
		
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		String videoId = request.getParameter("vId");	
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		String processType = request.getParameter("pType");

		try {

			System.out.println("[videoId Info]  " + " [cuId] " + customerIdStr + "  [videoId]  " + videoId);

			if (TextUtil.isEmptyOrNull(videoId)) {
				resMsg = "Please send valid video Id:" + videoId;
				error.setDesc("Please send valid video Id");
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);

				generateResponse(response, videoLikeInfo);
				return response;
			}
			Integer vidId = null;
			try {
				vidId = Integer.parseInt(videoId.trim());
			}catch (Exception ex) {
				resMsg = "Please send valid video Id:" + videoId;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);
				generateResponse(response, videoLikeInfo);
				return response;
			}
			
			videoLikeInfo.setVidId(vidId);			
			ApplicationPlatForm applicationPlatForm = null;		

			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);

				generateResponse(response, videoLikeInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);

				generateResponse(response, videoLikeInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);
				generateResponse(response, videoLikeInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);
				generateResponse(response, videoLikeInfo);
				return response;
			}
			
			if(processType == null || (!processType.equals("LIKE") && !processType.equals("DISLIKE") )) {
				resMsg = "Invalid Process Type:" + processType;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				videoLikeInfo.setErr(error);
				videoLikeInfo.setSts(0);
				generateResponse(response, videoLikeInfo);
				return response;
			}
			
			videoLikeInfo.setmId(videoId);
			videoLikeInfo.setCuId(customer.getId());
			if(processType.equals("LIKE")) {
				updateVideoLikeResponse(videoLikeInfo, customer, true);
			} else {
				updateVideoLikeResponse(videoLikeInfo, customer, false);
			}
			
			if(videoLikeInfo.getSts() == 0) throw new Exception();
			videoLikeInfo.setSts(1);
			resMsg = "Success:" + customerIdStr + ":POLLING:" + contestType;

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			videoLikeInfo.setErr(error);
			videoLikeInfo.setSts(0);
			generateResponse(response, videoLikeInfo);
			return response;
			
			
		} finally {
			try {
				
			}catch(Exception exc) {				
				exc.printStackTrace();
			}
			 
		}
		

		generateResponse(response, videoLikeInfo);
		return response;

	}

	private static VideoLikeInfo updateVideoLikeResponse(VideoLikeInfo videoLikeInfo, CassCustomer cassCustomer, Boolean isLike)
			throws Exception {
		videoLikeInfo.setSts(0);
		try {
			if(isLike) {
		
			videoLikeInfo = PollingVideoLikeService.processVideoLike(videoLikeInfo, cassCustomer);
				
		}else {
			videoLikeInfo = PollingVideoLikeService.processVideoDisLike(videoLikeInfo, cassCustomer);
		}
			videoLikeInfo.setSts(1);
		}catch(Exception ex) {
			ex.printStackTrace();
			videoLikeInfo.setSts(0);
		}
		
		return videoLikeInfo;
	}

	public void generateResponse(HttpServletResponse response, VideoLikeInfo videoLikeInfo)
			throws ServletException, IOException {
		Map<String, VideoLikeInfo> map = new HashMap<String, VideoLikeInfo>();
		map.put("VideoLikeInfo", videoLikeInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}
	
	public static void main(String[] args) throws Exception{
		VideoLikeInfo videoLikeInfo = new VideoLikeInfo();
		videoLikeInfo.setCuId(2);
		videoLikeInfo.setVidId(32);
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(2);
		updateVideoLikeResponse(videoLikeInfo ,  customer, false);
	}


}
