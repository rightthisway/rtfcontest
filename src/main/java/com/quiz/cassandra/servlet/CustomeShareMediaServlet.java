package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.service.CustomerMediaServices;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomeShareMediaServlet.java
 * API to Add / remove favourite videos
 * 
 */

@WebServlet("/ShareMedia.json")
public class CustomeShareMediaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public CustomeShareMediaServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		VideoLikeInfo videoInfo = new VideoLikeInfo();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		String sharedTo = request.getParameter("sTo");
		String sharedOn = request.getParameter("smPfm");
		String medId = request.getParameter("mId");
		String source = request.getParameter("source");
		
		try {
			System.out.println("[MediaShared Info]  " + " [cuId] " + customerIdStr );			

			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				videoInfo.setErr(error);
				videoInfo.setSts(0);

				generateResponse(response, videoInfo);
				return response;
			}			
			

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				videoInfo.setErr(error);
				videoInfo.setSts(0);
				generateResponse(response, videoInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				videoInfo.setErr(error);
				videoInfo.setSts(0);
				generateResponse(response, videoInfo);
				return response;
			}		
			
			videoInfo.setsTo(PollingUtil.getEmptyifNull(sharedTo));
			videoInfo.setSmPfm(PollingUtil.getEmptyifNull(sharedOn));
			videoInfo.setmId(PollingUtil.getEmptyifNull(medId));
			videoInfo.setCuId(customer.getId());
			videoInfo.setSource(source);
			videoInfo = CustomerMediaServices.processMediaShare(customer ,videoInfo );
			

		} catch (Exception e) {
			resMsg = "Error occured while Updating Video Shared Details ";
			e.printStackTrace();
			error.setDesc("Error occured while Updating Video Shared Details");
			videoInfo.setErr(error);
			videoInfo.setSts(0);
			generateResponse(response, videoInfo);
			return response;

		} finally {
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, videoInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, VideoLikeInfo videoInfo)
			throws ServletException, IOException {
		Map<String, VideoLikeInfo> map = new HashMap<String, VideoLikeInfo>();
		map.put("VideoSharedInfo", videoInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
