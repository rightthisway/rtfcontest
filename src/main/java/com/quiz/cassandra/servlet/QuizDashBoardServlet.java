package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class QuizDashBoardServlet
 */

@WebServlet("/GetDashboardInfo.json")
public class QuizDashBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(QuizDashBoardServlet.class);
  
    public QuizDashBoardServlet() {
        super();       
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		conuter = conuter + 1;
		System.out.println("called  Post  saytime ");
		PrintWriter out = response.getWriter();
		out.print("<html><body><h1 align='center'>" +
		new Date().toString() +    "    -- Counter is Now " +     conuter  +   "</h1></body></html>");
	}*/
	
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	

//	System.out.println("DAshboard Inside : "+request.getParameter("cuId")+" : "+ new Date());
	DashboardInfo dashboardInfo =new DashboardInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String customerIdStr = request.getParameter("cuId");
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	String appVersion = request.getParameter("aVn");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	Integer customerId=null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			resMsg = authError.getDescription();
			dashboardInfo.setErr(authError);
			dashboardInfo.setSts(0);
			TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.GETDASHBOARDINFO,resMsg);
			return dashboardInfo;
		}*/
		
		
		//String productTypeStr = request.getParameter("productType");
		/*if(TextUtil.isEmptyOrNull(platForm)){
			resMsg = "Application Platform is mandatory";
			error.setDesc("Application Platform is mandatory");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETDASHBOARDINFO,resMsg);
			return dashboardInfo;
		}
		if(TextUtil.isEmptyOrNull(deviceType)){
			resMsg = "Device Type is mandatory";
			error.setDesc("Device Type is mandatory");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.GETDASHBOARDINFO,resMsg);
			return dashboardInfo;
		}*/
		/*ApplicationPlatForm applicationPlatForm=null;
		if(!TextUtil.isEmptyOrNull(platForm)){
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				resMsg = "Please send valid application platform";
				error.setDesc("Please send valid application platform");
				dashboardInfo.setErr(error);
				dashboardInfo.setSts(0);
				TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.GETDASHBOARDINFO,resMsg);
				return dashboardInfo;
			}
		}*/
		/*if(customerIdStr == null || customerIdStr.equals("0")) {
			customerIdStr = ""+CassContestUtil.getRandomCustomerIdForTest();
		}*/
		CassCustomer customer = null;
		if(!TextUtil.isEmptyOrNull(customerIdStr)){
			 
			try {
				customerId = Integer.parseInt(customerIdStr.trim());
				customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(customer == null) {
					resMsg = "Customer Id is Invalid:"+customerIdStr;
					//error.setDesc("Your Cellular or WIFI Network is currently weak.");//Customer Id is Invalid
					//error.setDesc("Sorry, We are experiencing some issues right now. Please try in a few minutes.");//Customer Id is Invalid
					error.setDesc(URLUtil.genericErrorMsg);
					dashboardInfo.setErr(error);
					dashboardInfo.setSts(0);
					generateResponse(response, dashboardInfo);
					return response;
				}
			} catch(Exception e) {
				e.printStackTrace();
				resMsg = "Customer Id Not Exist:"+customerIdStr;
				//error.setDesc("Your Cellular or WIFI Network is currently weak.");//Customer Id Not Exist
				//error.setDesc("Sorry, We are experiencing some issues right now. Please try in a few minutes.");//Customer Id Not Exist
				error.setDesc(URLUtil.genericErrorMsg);
				dashboardInfo.setErr(error);
				dashboardInfo.setSts(0);
				generateResponse(response, dashboardInfo);
				return response;
			}
		}
		/*if(CassContestUtil.SuperFanLevelEnabled) {
			SuperFanContCustLevels sfContCustLevel = CassContestUtil.getSuperFanContCustLevels(customerId,null);
			if(sfContCustLevel != null) {
				dashboardInfo.setSflqNo(sfContCustLevel.getqNo());
			}
		}*/
		dashboardInfo.setSflqNo(0);
		try {
			Integer cartCount = 0;
			Connection conn = DatabaseConnections.getEcommDatabaseConn();
			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery("select sum(qty) from rtf_customer_cart where cust_id="+customer.getId()+" AND cartstatus='ACTIVE'");
			if(rs.next()){
				cartCount = rs.getInt(1);
			}
			dashboardInfo.setCartCount(cartCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		dashboardInfo.setSts(1);
		dashboardInfo.setCust(customer);
		dashboardInfo.sethRwds(CassContestUtil.HIDE_DASHBOARD_REWARDS);
		//dashboardInfo.setMessage("");
		
		resMsg = "Success:"+customerIdStr;
		//TrackingUtils.contestDashboardAPITracking(request, ,resMsg);
	}catch(Exception e){
		resMsg = "Error occurred while Fetching Dashboard Information";
		e.printStackTrace();
		//error.setDesc("Error occurred while Fetching Dashboard Information");
		error.setDesc(URLUtil.genericErrorMsg);
		dashboardInfo.setErr(error);
		dashboardInfo.setSts(0);
		
		generateResponse(response, dashboardInfo);
		return response;
		
	} finally {
		try {
			//System.out.println("Lloyd Start Cassandra insert " );
		TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETDASHBOARDINFO, resMsg, null, 
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),appVersion,apiHitStartTimeStr,dashboardInfo.getSts(),null);
		/*TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETDASHBOARDINFO, resMsg, null, 
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),"2");
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETDASHBOARDINFO, resMsg, null, 
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),"3");
		System.out.println("Lloyd Finished  Cassandra insert " );*/
		}catch(Exception ex){
			ex.printStackTrace();
			log.info("error dashboard " + ex.getMessage());
			log.info("" + ex);
			System.out.println("Error Wrtiting in CASS : "+ex.getStackTrace().toString());			
		}
		//log.info("GETDASH INFO: "+customerIdStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
	}
	
	generateResponse(response, dashboardInfo);
	return response;
}

public void generateResponse(HttpServletResponse response,DashboardInfo dashboardInfo) throws ServletException, IOException {
	Map<String, DashboardInfo> map = new HashMap<String, DashboardInfo>();
	map.put("dashboardInfo", dashboardInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

		
		
	public static void main(String a[]) {
			System.out.println();			
			
			DashboardInfo dashboardInfo = new DashboardInfo();
			Map<String, DashboardInfo> map = new HashMap<String, DashboardInfo>();
			map.put("dashboardInfo", dashboardInfo);
			String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);
			System.out.print(jsondashboardInfo);
	}
	
	
	
	
	
	

}
