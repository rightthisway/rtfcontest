package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.service.PollingMediaUploadValidityService;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class PollingMediaUploadEligibilityServlet
 */

@WebServlet("/PollingMediaUpdEligible.json")
public class PollingMediaUploadEligibilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(PollingMediaUploadEligibilityServlet.class);

	public PollingMediaUploadEligibilityServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PollingVideoInfo pollingVideoInfo = new PollingVideoInfo();
		CassError error = new CassError();		
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");		
			
		
		try {

			System.out.println("[PollingVideoUpdEligible]" + " [cuId] " + customerIdStr + " [pfm]" + platForm);

			ApplicationPlatForm applicationPlatForm = null;		
			if (TextUtil.isEmptyOrNull(platForm)) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}
			
			
		

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				//error.setDesc("Invalid Customer Id");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}
			
			
			pollingVideoInfo.setCuId(customerId);
			

			validateMediaUploadEligibility(pollingVideoInfo,customer);			
			resMsg = "Success:" + customerIdStr + ":POLLING:" + contestType;

		} catch (Exception e) {
			resMsg = "Error occured while Fetching Polling Answer Validation Details. ";
			e.printStackTrace();
			//error.setDesc("Error occured while Fetching Polling Answer Validation Details.");
			error.setDesc(URLUtil.genericErrorMsg);
			pollingVideoInfo.setErr(error);
			pollingVideoInfo.setSts(0);
			generateResponse(response, pollingVideoInfo);
			return response;

		} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
		}

		generateResponse(response, pollingVideoInfo);
		return response;

	}

	private PollingVideoInfo validateMediaUploadEligibility(PollingVideoInfo pollingVideoInfo ,  CassCustomer customer ) {

		try {
			PollingMediaUploadValidityService.processMediaUploadEligibility(pollingVideoInfo , customer);
		} catch (Exception ex) {			
			ex.printStackTrace();
			pollingVideoInfo.setSts(0);
			pollingVideoInfo.setMsg(PollingUtil.MOBILE_MSG_UPLOAD_FAILURE);
		}
		
		String nxtCTxt  = " Next Contest will be at 8.00 PM  EST " ; 
		pollingVideoInfo.setNxtCTxt(nxtCTxt);

		return pollingVideoInfo;
	}

	public void generateResponse(HttpServletResponse response, PollingVideoInfo pollingVideoInfo)
			throws ServletException, IOException {
		Map<String, PollingVideoInfo> map = new HashMap<String, PollingVideoInfo>();
		map.put("pollingVideoInfo", pollingVideoInfo);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
