package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.data.ContestPasswordAuth;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CassJoinContestInfo;
import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class JoinContestServlet
 */

@WebServlet("/JoinContest.json")
public class JoinContestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(JoinContestServlet.class);
  
    public JoinContestServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	
	//System.out.println("Join Inside : "+request.getParameter("cuId")+" : "+ new Date());
	
	CassJoinContestInfo joinContInfo =new CassJoinContestInfo();
	CassError error = new CassError();
	Date start = new Date();
	String customerIdStr = request.getParameter("cuId");
	//String contestIdStr = request.getParameter("contestId");
	String platForm = request.getParameter("pfm");
	//String productTypeStr = request.getParameter("productType");
	String loginIp = request.getParameter("lIp");
	Integer customerId = null;
	String resMsg = "";
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	Integer contestId=null;
	try {
		/*Error authError = authorizationValidation(request);
		if(authError != null) {
			joinContInfo.setErr(authError);
			joinContInfo.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.JOINCONTEST,authError.getDescription());
			return joinContInfo;
		}*/
		
		
		 
		ApplicationPlatForm applicationPlatForm=null;
		if(!TextUtil.isEmptyOrNull(platForm)){
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				resMsg = "Please send valid application platform:"+platForm;
				//error.setDesc("Please send valid application platform");
				error.setDesc(URLUtil.genericErrorMsg);
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
			
				generateResponse(response, joinContInfo);
				return response;
			}
		}
		
		String contestType="MOBILE";
		if(applicationPlatForm!= null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
			contestType="WEB";
		}
		if(platForm.contains("ANDROID")||platForm.contains("IOS")){
			loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
		}
		
		/*if(customerIdStr != null && customerIdStr.equals("0")) {
			customerIdStr = ""+CassContestUtil.getRandomCustomerIdForTest();
		}*/
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Invalid Customer Id:"+customerIdStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			joinContInfo.setErr(error);
			joinContInfo.setSts(0);
			
			generateResponse(response, joinContInfo);
			return response;
		}
		
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
		//CassCustomer customer = CassContestUtil.getCasscustomerFromMap(customerId);
		if(customer == null) {
			resMsg = "Customer Id is Invalid:"+customerIdStr;
			//error.setDesc("Customer Id is Invalid");
			error.setDesc(URLUtil.genericErrorMsg);
			joinContInfo.setErr(error);
			joinContInfo.setSts(0);
			
			generateResponse(response, joinContInfo);
			return response;
		}
		
		/*if(customer.getPhone() == null || !customer.getIsOtp()) {
			error.setDesc("Verify your Phone with OTP to Join Contest.");
			joinContInfo.setErr(error);
			joinContInfo.setSts(0);
			TrackingUtil.contestAPITracking(platForm, null, request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, 
					"Verify your Phone with OTP to Join Contest.", null, customerId, start, new Date(), request.getHeader("X-Forwarded-For"));
			return joinContInfo;
		}*/
		QuizContest contest =CassContestUtil.getCurrentStartedContest(contestType);
		if(contest == null) {
			resMsg = "contest is Not Yet Started:"+contestType;
			error.setDesc("contest is Not Yet Started.");
			joinContInfo.setErr(error);
			joinContInfo.setSts(0);
			
			generateResponse(response, joinContInfo);
			return response;
		}
		contestId = contest.getId();
		//joinContInfo.setContestId(contest.getId());

		if(contest.getContestMode() != null && contest.getContestMode().equals("PASSWORD")) {
			ContestPasswordAuth contPwdAuth = CassandraDAORegistry.getContestPasswordAuthDAO().getContestPasswordAuthByCustomerId(customerId);
			if(contPwdAuth == null) {
				resMsg = "You are not Authorized. please enter password to get authorized"+contestType;
				error.setDesc("You are not Authorized to play this contest.");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				
				generateResponse(response, joinContInfo);
				return response;
			}
		}
		
		joinContInfo.setqLive(customer.getqLive());
		joinContInfo.setMw(customer.getMw());
		if(contest != null && contest.getStatus().equals("STARTED")) {
			joinContInfo = CassContestUtil.updateJoinContestCustomersCount(joinContInfo,contest.getId(), customer);
			
			//Tamil : commented due to performance
			//CassContestUtil.updateExistingContestParticipantsStatusByJoinContest(customerId);
			//CassandraDAORegistry.getContestParticipantsDAO().updateExistingContestParticipantsByJoinContest(customer.getId(), contest.getId());
				
				ContestParticipants participant = new ContestParticipants();
				participant.setCoId(contest.getId());
				participant.setCuId(customerId);
				participant.setPfm(applicationPlatForm.toString());
				participant.setIpAdd(loginIp);
				participant.setJnDate(new Date().getTime());
				participant.setStatus("ACTIVE");
				participant.setuId(customer.getuId());

//Phase 2 UnCommented
				CassandraDAORegistry.getContestParticipantsDAO().saveForJoin(participant);
//Phase 2 Commented			
				//CassContestUtil.updateContestParticipantsMapByCustId(participant);
			
			//if(contest.getStatus().equals("STARTED")) {
//				Double contesTAnserRewards = QuizContestUtil.getCustomerContestAnswerRewards(contest.getId(), customer.getId());
//				joinContInfo.setContestAnswerRewards(contesTAnserRewards);
				/*if(contesTAnserRewards != null) {
					joinContInfo.setContestAnswerRewards(TicketUtil.getRoundedValueString(contesTAnserRewards));
				}*/
			//}
			if(CassContestUtil.SuperFanLevelEnabled) {
				SuperFanContCustLevels sfContCustLevel = CassContestUtil.getSuperFanContCustLevels(customerId, contest.getId());
				if(sfContCustLevel != null) {
					joinContInfo.setSflqNo(sfContCustLevel.getqNo());
				}
			}
				
			log.info("JOIN : "+customerIdStr+":"+joinContInfo.getLqNo()+":"+joinContInfo.getCaRwds()+":"+new Date());
		}
		
		joinContInfo.setSts(1);
		resMsg = "Success:"+customerIdStr+":"+contest.getId()+":"+contestType;
		
	}catch(Exception e){
		resMsg = "Error occurred while Fetching Join contest Details.";
		e.printStackTrace();
		//error.setDesc("Error occurred while Fetching Join contest Details.");
		error.setDesc(URLUtil.genericErrorMsg);
		joinContInfo.setErr(error);
		joinContInfo.setSts(0);
		//TrackingUtils.contestAPITracking(request, WebServiceActionType.JOINCONTEST,"Error occured while Fetching Join contest Details.");

		generateResponse(response, joinContInfo);
		return response;
		
	} finally {
		TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg, contestId, 
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.getSts(),null);
		log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
	}
	
	
	generateResponse(response, joinContInfo);
	return response;

	
}

public void generateResponse(HttpServletResponse response,CassJoinContestInfo joinContInfo) throws ServletException, IOException {
	Map<String, CassJoinContestInfo> map = new HashMap<String, CassJoinContestInfo>();
	map.put("cassJoinContestInfo", joinContInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
