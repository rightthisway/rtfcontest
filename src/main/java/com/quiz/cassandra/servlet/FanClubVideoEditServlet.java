package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.service.FanClubService;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class FanClubVideoEditServlet.java
 * API to Save Fan Club video Like Count
 * 
 */

@WebServlet("/FanClubEditVideo.json")
public class FanClubVideoEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FanClubVideoEditServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Comments fanClubPostsDTO = new Comments();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		
		String fanClubId = request.getParameter("fcId"); // Fan Club Id
		String videoId = request.getParameter("vId");
		String title = request.getParameter("title");		
		String description = request.getParameter("description");
		String catId = request.getParameter("catId");
		
		Integer cId = null;
		
		Integer fcId=null;
		Integer vId = null;
		
		try {
			System.out.println("[Fan Club video like  save]  " + " [cuId] " + customerIdStr  + "  [fanClubId]" + fanClubId  + "   [videoId]" + videoId);			

			
			try {
				cId = Integer.parseInt(catId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid catId Id:" + catId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}
			try {
				vId = Integer.parseInt(videoId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid video Id:" + videoId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}
			
			
			try {
				fcId = Integer.parseInt(fanClubId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Fan Club Id:" + fanClubId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println();
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}
			
			
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);

				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				fanClubPostsDTO.setSts(0);
				generateResponse(response, fanClubPostsDTO);
				return response;
			}

			fanClubPostsDTO.setFcId(fcId);
			fanClubPostsDTO.setCuId(customerId);
			fanClubPostsDTO.setvId(vId);
			fanClubPostsDTO.setDescription(description);
			fanClubPostsDTO.setTitle(title);
			fanClubPostsDTO.setCatId(catId);
			FanClubService.editFanClubVideos(fanClubPostsDTO);
			if(fanClubPostsDTO.getSts() == 1) {
				fanClubPostsDTO.setMsg("SUCCESS");
				
			}
			else {
				fanClubPostsDTO.setSts(0);
				fanClubPostsDTO.setMsg(URLUtil.genericErrorMsg);
				error.setDesc(URLUtil.genericErrorMsg);
				fanClubPostsDTO.setErr(error);
				
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			fanClubPostsDTO.setErr(error);
			fanClubPostsDTO.setSts(0);
			generateResponse(response, fanClubPostsDTO);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, fanClubPostsDTO);
		return response;
	}

	public void generateResponse(HttpServletResponse response, Comments fanClubPostsDTO)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Map<String, Comments> map = new HashMap<String, Comments>();
		map.put("VidInfo", fanClubPostsDTO);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
