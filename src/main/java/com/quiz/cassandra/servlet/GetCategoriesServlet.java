package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CategoryInfo;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.Category;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

/**
 * Servlet implementation class GetCategoriesServlet
 */

@WebServlet("/GetCategories.json")
public class GetCategoriesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(GetCategoriesServlet.class);

	public GetCategoriesServlet() {
		super();
	}

	
	 protected void doGet(HttpServletRequest request, HttpServletResponse
	 response) throws ServletException, IOException { 
	  	doPost(request, response);
	 }

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		CategoryInfo categoryInfo = new CategoryInfo();
		CassError error = new CassError();
		Date start = new Date();
		
		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		
		String customerIdStr = request.getParameter("cuId"); 		
		String appver = request.getParameter("ve");		
		String apprelease = request.getParameter("rel");

		try {
			/*CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				//error.setDesc("Customer Id is not Registered");
				error.setDesc(URLUtil.genericErrorMsg);
				categoryInfo.setErr(error);
				categoryInfo.setSts(0);
				generateResponse(response, categoryInfo);
				return response;
			}*/
			List<Category> catList = PollingSQLDaoUtil.getAllCategories();
			categoryInfo.setCatList(catList);
			categoryInfo.setSts(1);
			 
			resMsg = "Success:" + customerIdStr ;
		} catch (Exception e) {
			resMsg = "Error occured while Fetching Getting Categories Details. ";
			e.printStackTrace();
			error.setDesc("");
			categoryInfo.setErr(error);
			categoryInfo.setSts(0);
			generateResponse(response, categoryInfo);
			return response;

		} finally {
			/*
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg,
			 * contestId, customerId, start, new Date(),
			 * request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.
			 * getSts(),null);
			 * log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new
			 * Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
			 */
		}

		generateResponse(response, categoryInfo);
		return response;

	}
 
	public void generateResponse(HttpServletResponse response, CategoryInfo categoryInfo)
			throws ServletException, IOException {
		Map<String, CategoryInfo> map = new HashMap<String, CategoryInfo>();
		map.put("categoryInfo", categoryInfo);
		// String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

		String jsonPllingAnswerInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		// System.out.print(jsondashboardInfo);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonPllingAnswerInfo);
		out.flush();
	}

}
