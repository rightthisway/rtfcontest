package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.service.CustomerCommentService;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerMediaCommentsDeleteServlet.java
 * API to report abuse on videos / comments
 * 
 */

@WebServlet("/CustMediaCmtsDelete.json")
public class CustomerMediaCommentsDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public CustomerMediaCommentsDeleteServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Comments comments = new Comments();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");		
		
		String srcId = request.getParameter("srcId"); // Video  Id
		String cmtId = request.getParameter("cmtId");
		Integer sourceId = null;
		Integer commentId = null;
		
		
		
		
		try {
			System.out.println("[Cust Media Save Info]  " + " [cuId] " + customerIdStr );			

			
			
			try {
				sourceId = Integer.parseInt(srcId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid comment or video  Id:" + srcId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println("Invalid  video  Id:" + srcId);
				comments.setErr(error);
				comments.setSts(0);
				generateResponse(response, comments);
				return response;
			}		
			try {
				commentId = Integer.parseInt(cmtId.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid comment or video  Id:" + cmtId;
				error.setDesc(URLUtil.genericErrorMsg);
				System.out.println("Invalid comment :" + cmtId);
				comments.setErr(error);
				comments.setSts(0);
				generateResponse(response, comments);
				return response;
			}		
			
			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				comments.setErr(error);
				comments.setSts(0);

				generateResponse(response, comments);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				comments.setErr(error);
				comments.setSts(0);
				generateResponse(response, comments);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				comments.setErr(error);
				comments.setSts(0);
				generateResponse(response, comments);
				return response;
			}

			
			comments.setSrcId(sourceId);
			comments.setCmtId(commentId);					
			comments.setCuId(customer.getId());			
			CustomerCommentService.deleteMediaComments(comments);
			
			if(comments.getSts() == 1) {
				comments.setMsg("SUCCESS");
				
			}
			else {
				comments.setSts(0);
				comments.setMsg(URLUtil.genericErrorMsg);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			comments.setErr(error);
			comments.setSts(0);
			generateResponse(response, comments);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, comments);
		return response;
	}

	public void generateResponse(HttpServletResponse response, Comments comments)
			throws ServletException, IOException {
		Map<String, Comments> map = new HashMap<String, Comments>();
		map.put("Comments", comments);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}

}
