package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContSummaryInfo;
import com.quiz.cassandra.list.ContWinnerRewardsInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/GetContestSummary.json")
public class ContestSummaryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ContestSummaryServlet.class);
  
    public ContestSummaryServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	ContSummaryInfo contestSummary =new ContSummaryInfo();
	CassError error = new CassError();
	Date start = new Date();
	String resMsg = "";
	String platForm = request.getParameter("pfm");
	String deviceType = request.getParameter("dyType");
	Integer contestId = null;
	//String customerIdStr = request.getParameter("cuId");
	String contestIdStr = request.getParameter("coId");
	//String summaryTypeStr = request.getParameter("sType");
	try {
		
		/*QuizSummaryType quizSummaryType = null;
		try {
			quizSummaryType = QuizSummaryType.valueOf(summaryTypeStr);
			
		} catch(Exception e) {
			resMsg = "Summary Type is Invalid";
			error.setDesc("Summary Type is Invalid");
			contestSummary.setErr(error);
			contestSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
			return contestSummary;
		}*/
		
		/*if(!quizSummaryType.equals(QuizSummaryType.CONTEST)) {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				quizContestSummary.setErr(authError);
				quizContestSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
				return quizContestSummary;
			}
		}*/
		
		
		/*if(TextUtil.isEmptyOrNull(summaryTypeStr)){
			resMsg = "Summary Type is mandatory";
			error.setDesc("Summary Type is mandatory");
			quizContestSummary.setErr(error);
			quizContestSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
			return quizContestSummary;
		}*/
		
					
		/*if(TextUtil.isEmptyOrNull(customerIdStr)){
			resMsg = "Customer Id is mandatory";
			error.setDesc("Customer Id is mandatory");
			quizContestSummary.setErr(error);
			quizContestSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
			return quizContestSummary;
		}*/
					
		//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				resMsg = "Contest Id is mandatory:"+contestIdStr;
				//error.setDesc("Contest Id is mandatory");
				error.setDesc(URLUtil.genericErrorMsg);
				contestSummary.setErr(error);
				contestSummary.setSts(0);
				
				generateResponse(response, contestSummary);
				return response;
			}
			
			//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				//error.setDesc("Contest Id is Invalid");
				error.setDesc(URLUtil.genericErrorMsg);
				contestSummary.setErr(error);
				contestSummary.setSts(0);
				
				generateResponse(response, contestSummary);
				return response;
			}
			//List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByContestId(Integer.parseInt(contestIdStr));
			//List<CassContestWinners> contestWinners = CassContestUtil.getContestSummaryData(quizContest.getId());
			
			contestSummary = CassContestUtil.getContestSummaryDataWithLimit(quizContest.getId(),contestSummary);
			
			//contestSummary.setWinners(contestWinners);
			/*if(contestWinners != null) {
				contestSummary.setwCount(contestWinners.size());
				List<CassContestWinners> finalList = new ArrayList<CassContestWinners>();
				int count = 0;
				for (CassContestWinners cassContestWinners : contestWinners) {
					if(count>=100) {
						break;
					}
					count++;
					finalList.add(cassContestWinners);
				}
				contestSummary.setWinners(finalList);
			}*/
			
			
			/*if(Integer.parseInt(customerIdStr) != 0) {
				QuizContestWinners contestWinner = QuizContestUtil.getQuizContestWinnerByCustomerIdAndContestId(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
				quizContestSummary.setQuizContestWinner(contestWinner);
			}*/
			
		//} 
			/*else if(quizSummaryType.equals(QuizSummaryType.TILLDATE)) {
			
			List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByTillDate();
			QuizContestWinners contestWinner = null;
			if(contestWinners != null) {
				if(!TextUtil.isEmptyOrNull(customerIdStr)){
					Integer customerId = Integer.parseInt(customerIdStr);
					for (QuizContestWinners contestWinnerObj : contestWinners) {
						if(contestWinnerObj.getCustomerId().equals(customerId)) {
							contestWinner = contestWinnerObj;
						}
					}
				}
			}
			if(contestWinner == null) {
				if(!TextUtil.isEmptyOrNull(customerIdStr)){
					contestWinner = QuizContestUtil.getQuizContestWinnersByTillDateAndCustomerId(Integer.parseInt(customerIdStr));
				}
			}
			quizContestSummary.setQuizContestWinners(contestWinners);
			quizContestSummary.setQuizContestWinner(contestWinner);
			//quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
		} else if(quizSummaryType.equals(QuizSummaryType.THISWEEK)) {

			//This Week starts from Sunday to till date
			Calendar cal = Calendar.getInstance();
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			dayOfWeek = 1-dayOfWeek;
			cal.add(Calendar.DAY_OF_MONTH, dayOfWeek);
			String startDateStr = dbDateFormat.format(new Date(cal.getTimeInMillis()))+" 00:00:00";
			String toDateStr = dbDateFormat.format(new Date())+" 23:59:59";
			
			List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByThisWeek(startDateStr, toDateStr);
			QuizContestWinners contestWinner = null;
			if(contestWinners != null) {
				if(!TextUtil.isEmptyOrNull(customerIdStr)){
					Integer customerId = Integer.parseInt(customerIdStr);
					for (QuizContestWinners contestWinnerObj : contestWinners) {
						if(contestWinnerObj.getCustomerId().equals(customerId)) {
							contestWinner = contestWinnerObj;
						}
					}
				}
			}
			if(contestWinner == null) {
				if(!TextUtil.isEmptyOrNull(customerIdStr)){
					contestWinner = QuizContestUtil.getQuizContestWinnersByThisWeekAndCustomerId(startDateStr, toDateStr,Integer.parseInt(customerIdStr));
				}
			}
			quizContestSummary.setQuizContestWinners(contestWinners);
			quizContestSummary.setQuizContestWinner(contestWinner);
			//quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
		}*/
		
			contestSummary.setSts(1);
		//quizContestDetails.setQuizCustomer(quizCustomer);
		resMsg = "Success";
		resMsg = "Success:"+contestSummary.getwCount();
		//TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
	}catch(Exception e){
		resMsg = "Error occured while Fetching Contest Summary.";
		e.printStackTrace();
		//error.setDesc("Error occured while Fetching Contest Summary.");
		error.setDesc(URLUtil.genericErrorMsg);
		contestSummary.setErr(error);
		contestSummary.setSts(0);
		
		generateResponse(response, contestSummary);
		return response;
	} finally {
		TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.QUIZCONTESTSUMMARY, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null,contestSummary.getSts(),null);
		log.info("CASS SUMMARY : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : stype: "+request.getParameter("summaryType")+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
	}
	
	generateResponse(response, contestSummary);
	return response;


}

public void generateResponse(HttpServletResponse response,ContSummaryInfo contSummary) throws ServletException, IOException {
	Map<String, ContSummaryInfo> map = new HashMap<String, ContSummaryInfo>();
	map.put("contSummaryInfo", contSummary);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
