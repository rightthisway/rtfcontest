package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CassJoinContestInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class ExitContestServlet
 */

@WebServlet("/ExitContest.json")
public class ExitContestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ExitContestServlet.class);
  
    public ExitContestServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	//System.out.println("Exit Inside : "+request.getParameter("cuId")+" : "+ new Date());
	
	CassJoinContestInfo joinContInfo =new CassJoinContestInfo();
	CassError error = new CassError();
	Date start = new Date();
	String customerIdStr = request.getParameter("cuId");
	String contestIdStr = request.getParameter("coId");
	String platForm = request.getParameter("pfm");
	Integer customerId = null,contestId = null;
	String resMsg = "";
	String deviceType = request.getParameter("dyType");
	String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
	try {
		
		/*if(customerIdStr != null && customerIdStr.equals("0")) {
			customerIdStr = ""+CassContestUtil.getRandomCustomerIdForTest();
			QuizContest tempCont = CassContestUtil.getCurrentStartedContest("MOBILE");
			contestIdStr = ""+tempCont.getId();
		}*/
		try{
			customerId = Integer.parseInt(customerIdStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Customer Id:"+customerIdStr;
			//error.setDesc("Invalid Customer Id");
			error.setDesc(URLUtil.genericErrorMsg);
			joinContInfo.setErr(error);
			joinContInfo.setSts(0);
			
			generateResponse(response, joinContInfo);
			return response;
		}
		
		try{
			contestId = Integer.parseInt(contestIdStr.trim());
		}catch(Exception e){
			resMsg = "Invalid Contest Id:"+contestIdStr;
			//error.setDesc("Invalid Contest Id");
			error.setDesc(URLUtil.genericErrorMsg);
			joinContInfo.setErr(error);
			joinContInfo.setSts(0);
			
			generateResponse(response, joinContInfo);
			return response;
		}
//Phase 2 Commented
		//joinContInfo = CassContestUtil.updateQuizExitCustomersCount(joinContInfo,contestId, customerId);
		joinContInfo.setSts(1);
		try {
//Phase 2 Commented
			//CassContestUtil.updateExistingContestParticipantsStatusByExitContest(customerId);
//Phase 2 UnCommented
			CassContestUtil.updateExistingContestParticipantsStatusByExitContest(customerId,contestId);

			//CassandraDAORegistry.getContestParticipantsDAO().updateContestParticipantsExistContest(customerId, contestId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		resMsg = "Success:"+contestIdStr+":"+customerIdStr;
	}catch(Exception e){
		resMsg = "Error occurred while Fetching Exit contest Details.";
		e.printStackTrace();
		//error.setDesc("Error occurred while Fetching Exit contest Details.");
		error.setDesc(URLUtil.genericErrorMsg);
		joinContInfo.setErr(error);
		joinContInfo.setSts(0);
		
		generateResponse(response, joinContInfo);
		return response;
		
	} finally {
		TrackingUtil.contestAPITrackingForDeviceTimeTracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.EXITCONTEST, resMsg, contestId, 
				customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null,apiHitStartTimeStr,joinContInfo.getSts(),null);
		log.info("CASS EXIT : "+customerIdStr+" : coId: "+contestIdStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
	}
	
	generateResponse(response, joinContInfo);
	return response;
	
}

public void generateResponse(HttpServletResponse response,CassJoinContestInfo joinContInfo) throws ServletException, IOException {
	Map<String, CassJoinContestInfo> map = new HashMap<String, CassJoinContestInfo>();
	map.put("cassJoinContestInfo", joinContInfo);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
