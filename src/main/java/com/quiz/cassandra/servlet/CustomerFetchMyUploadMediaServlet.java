package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.MyVideoInfo;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.service.CustomerMediaServices;
import com.quiz.cassandra.utils.PollingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * Servlet implementation class CustomerFetchMyUploadMediaServlet.java
 * API to Add / remove favourite videos
 * 
 */

@WebServlet("/MyMedia.json")
public class CustomerFetchMyUploadMediaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public CustomerFetchMyUploadMediaServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		process(request, response);
	}

	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PollingVideoInfo pollingVideoInfo = new PollingVideoInfo();
		CassError error = new CassError();

		String customerIdStr = request.getParameter("cuId");

		String platForm = request.getParameter("pfm");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time

		try {
			System.out.println("[MyUploadedMedia Info]  " + " [cuId] " + customerIdStr );			

			ApplicationPlatForm applicationPlatForm = null;			

			try {
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			} catch (Exception e) {
				resMsg = "Please send valid application platform:" + platForm;
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);

				generateResponse(response, pollingVideoInfo);
				return response;
			}

			String contestType = "POLLING";
			if (applicationPlatForm != null && !applicationPlatForm.equals(ApplicationPlatForm.ANDROID)
					&& !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType = "WEB";
			}
			if (platForm.contains("ANDROID") || platForm.contains("IOS")) {
				loginIp = ((HttpServletRequest) request).getHeader("X-Forwarded-For");
			}

			try {
				customerId = Integer.parseInt(customerIdStr.trim());
			} catch (Exception e) {
				e.printStackTrace();
				resMsg = "Invalid Customer Id:" + customerIdStr;
				error.setDesc("Invalid Customer Id");
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if (customer == null) {
				resMsg = "Customer Id is not Registered:" + customerIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				pollingVideoInfo.setErr(error);
				pollingVideoInfo.setSts(0);
				generateResponse(response, pollingVideoInfo);
				return response;
			}

			pollingVideoInfo = PollingSQLDaoUtil.fetchCustomerUploadedVideo(customer.getId() );
			
			List<Integer> vidIdList = new ArrayList<Integer>();
			List<MyVideoInfo> vidInfoList = pollingVideoInfo.getMyVidInfoList();
			Map<Integer ,MyVideoInfo > vidInfoMap= new LinkedHashMap<Integer ,MyVideoInfo > ();
			if(vidInfoList != null  ) {			
				for(MyVideoInfo myVideoInfo: vidInfoList) {
					vidInfoMap.put(myVideoInfo.getvId(), myVideoInfo);
					//vidIdList.add(myVideoInfo.getvId());
				}
			}
			
			pollingVideoInfo.setMyVidInfoList(CustomerMediaServices.fetchLikedCountForVidIds(vidInfoMap,customer.getId()));
			
			/*
			 * List<Integer> vidIdList = new ArrayList<Integer>(vUrlLMap.keySet());
			 * Map<Integer , Integer> videoViewedCountMap =
			 * PollingSQLDaoUtil.getVideoViewCount(vidIdList) ; List<Integer>
			 * hasAlreadyLikedList = PollingSQLDaoUtil.getVideosLikedByCust(vidIdList ,
			 * pollingVideoInfo.getCuId()); Map<Integer, Integer> videoLikedCountMap =
			 * PollingSQLDaoUtil.getVideoLikedCount(vidIdList , pollingVideoInfo.getCuId() )
			 * ;
			 * 
			 * 
			 */
			
			
			
			List vidList = pollingVideoInfo.getMyVidInfoList();
			pollingVideoInfo.setSts(1);
			if(vidList == null || vidList.size() == 0) {
				pollingVideoInfo.setMsg(PollingUtil.EMPTY_MYUPLOADED_MEDIA);
			}

		} catch (Exception e) {
			resMsg = URLUtil.genericErrorMsg;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			pollingVideoInfo.setErr(error);
			pollingVideoInfo.setSts(0);
			generateResponse(response, pollingVideoInfo);
			return response;

		} finally { 
			try {

			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		generateResponse(response, pollingVideoInfo);
		return response;
	}

	public void generateResponse(HttpServletResponse response, PollingVideoInfo pollingVideoInfo)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Map<String, PollingVideoInfo> map = new HashMap<String, PollingVideoInfo>();
		map.put("PollingVideoInfo", pollingVideoInfo);
		String jsonMagicWandInfoInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsonMagicWandInfoInfo);
		out.flush();
	}
	
	public static void main(String[] args) {
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(2);
		PollingVideoInfo pollingVideoInfo = new PollingVideoInfo();
		//pollingVideoInfo.setCatId("DEF");
		pollingVideoInfo.setCatId("29");
		pollingVideoInfo.setAppver("n");
		pollingVideoInfo.setCuId(2);
		
		/*
		 * pollingVideoInfo =
		 * PollingSQLDaoUtil.fetchCustomerUploadedVideo(customer.getId() );
		 * 
		 * List<Integer> vidIdList = new ArrayList<Integer>(); List<MyVideoInfo>
		 * vidInfoList = pollingVideoInfo.getMyVidInfoList(); Map<Integer ,MyVideoInfo >
		 * vidInfoMap= new LinkedHashMap<Integer ,MyVideoInfo > (); if(vidInfoList !=
		 * null ) { for(MyVideoInfo myVideoInfo: vidInfoList) {
		 * vidInfoMap.put(myVideoInfo.getvId(), myVideoInfo);
		 * //vidIdList.add(myVideoInfo.getvId()); } }
		 * 
		 * pollingVideoInfo.setMyVidInfoList(CustomerMediaServices.
		 * fetchLikedCountForVidIds(vidInfoMap,customer.getId()));
		 */	//System.out.println("Final list " + pollingVideoInfo.getMyVidInfoList());
		
		
		pollingVideoInfo = CustomerMediaServices.fetchCustomerLikedVideos(customer.getId(),pollingVideoInfo );
		
	}

}
