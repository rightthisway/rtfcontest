package com.quiz.cassandra.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContSummaryInfo;
import com.quiz.cassandra.list.ContWinnerRewardsInfo;
import com.quiz.cassandra.list.CustomerDetails;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.TrackingUtil;
import com.web.util.GsonUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.TextUtil;

/**
 * Servlet implementation class ContApplyLifeServlet
 */

@WebServlet("/TrackNotificationDelivery.json")
public class TrackNotificationDeliveryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(TrackNotificationDeliveryServlet.class);
  
    public TrackNotificationDeliveryServlet() {
        super();       
    }
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
		
	}*/
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	CustomerDetails customerDetails =new CustomerDetails();
	try {
		
		customerDetails.setStatus(1);
		customerDetails.setMessage("");
		
		
	}catch(Exception e){
		e.printStackTrace();
		//contestSummary.setErr(error);
		customerDetails.setStatus(1);
		
		generateResponse(response, customerDetails);
		return response;
	} finally {
		/*TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.QUIZCONTESTSUMMARY, resMsg, 
				contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
		log.info("UPDATE NOTIFIC : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : stype: "+request.getParameter("summaryType")+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());*/
	}
	
	generateResponse(response, customerDetails);
	return response;


}

public void generateResponse(HttpServletResponse response,CustomerDetails customerDetails) throws ServletException, IOException {
	Map<String, CustomerDetails> map = new HashMap<String, CustomerDetails>();
	map.put("customerDetails", customerDetails);
	//String jsondashboardInfo = GsonUtil.getGsonInstance().toJson(map);

	String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
	//System.out.print(jsondashboardInfo);
	PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    out.print(jsondashboardInfo);
    out.flush(); 
}
	
	

}
