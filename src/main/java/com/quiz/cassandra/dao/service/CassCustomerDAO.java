package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassCustomer;

public interface CassCustomerDAO  {
	
	public void saveCustomer(CassCustomer obj);
	public void saveAll(List<CassCustomer> objList);
	public void updateCustomerLives(Integer customerId, Integer quizLives,CassCustomer customer);
	public CassCustomer getCustomerById(Integer customerId);
	public List<CassCustomer> getAll();
	public void deleteCustomer(Integer customerId);
	public void updateCustomerLives(Integer customerId, Integer quizLives);
	public void updateProfilePicName(Integer customerId, String imgPath);
	public void deleteAll();
	public void updateCustomerRewardsAndLives(Integer customerId, Integer quizLives,Double rewards);
	public void updateCustomerRewardsByCustomerId(Integer customerId, Double rewards);
	public void updateCustomerRtfPoints(Integer customerId, Integer rtfPoints);

}
