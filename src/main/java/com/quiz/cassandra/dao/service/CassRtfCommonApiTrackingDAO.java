package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassRtfCommonApiTracking;

public interface CassRtfCommonApiTrackingDAO  {
	
	public void save(CassRtfCommonApiTracking obj);
	public void saveForDeviceTimeTracking(CassRtfCommonApiTracking obj);
	public List<CassRtfCommonApiTracking> getAll();
	public void truncate();
}
