package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContDtls;

public interface CustContDtlsDAO  {
	
	public void save(CustContDtls obj) throws Exception;
	/*public void delete(CustContDtls custContDtls) throws Exception;*/
	public CustContDtls getCustContDtlsByCustId(Integer customerId) throws Exception;
	public void truncate() throws Exception;
}
