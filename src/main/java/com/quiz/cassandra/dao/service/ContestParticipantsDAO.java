package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.list.LiveCustDetails;

public interface ContestParticipantsDAO  {
	
	public void saveForJoin(ContestParticipants obj);
	public void updateForExitContest(ContestParticipants obj);
	/*public List<ContestParticipants> getAllParticipantsByContestIdAndContestId(Integer customerId,Integer contestId);*/
	public void deleteContestParticipantsByCustomerIdandContestId(Integer customerId,Integer contestId);
	/*public void updateExistingContestParticipantsByJoinContest(Integer customerId,Integer contestId);
	public void updateContestParticipantsForContestResetByContestId(Integer contestId);*/
	public List<ContestParticipants> getAllParticipantsByContestId(Integer contestId);
	public void truncate();
	public List<ContestParticipants> getAllParticipants();
	public List<LiveCustDetails> getAllLiveCustomerListForCache(Integer contestId);
	public Integer getLiveCustomerCount(Integer contestId);
	public void deleteContestParticipantsByCustomerId(Integer customerId);

}
