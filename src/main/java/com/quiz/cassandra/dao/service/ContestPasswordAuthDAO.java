package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.ContestPasswordAuth;

public interface ContestPasswordAuthDAO  {
	
	public void save(ContestPasswordAuth obj);
	public ContestPasswordAuth getContestPasswordAuthByCustomerId(Integer customerId);
	public List<ContestPasswordAuth> getAll();
	public void truncate();

}
