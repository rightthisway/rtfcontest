package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CustContAnswers;

public interface CustContAnswersDAO  {
	
	public void save(CustContAnswers obj);
	public void saveForApplyLife(CustContAnswers obj) throws Exception;
	/*public List<CustContAnswers> getCustContAnswersByCustIdAndContId(Integer customerId,Integer contestId);
	public CustContAnswers getCustContAnswersByCustIdAndQuestId(Integer customerId,Integer questionId);*/
	/*public List<CustContAnswers> getAllCustContAnswersByQuestId(Integer questionId);*/
	public void truncate();
	public List<CustContAnswers> getCustContAnswersByContIdandQuestionNo(Integer contestId,Integer questionNo);
	
}
