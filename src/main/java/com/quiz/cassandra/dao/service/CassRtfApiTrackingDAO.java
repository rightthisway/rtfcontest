package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassRtfApiTracking;

public interface CassRtfApiTrackingDAO  {
	
	public void save(CassRtfApiTracking obj);
	public void saveForDeviceTimeTracking(CassRtfApiTracking obj);
	public List<CassRtfApiTracking> getAll();
	public void truncate();
}
