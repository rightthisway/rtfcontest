package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassSuperFanWinner;

 
public class CassSuperFanWinnerDAO implements com.quiz.cassandra.dao.service.CassSuperFanWinnerDAO {

  
	public List<CassSuperFanWinner> getSuperFanWinnersByContestId(){
	   ResultSet results = CassandraConnector.getSession().execute(
	      "SELECT * from contest_superfan_winners  ");//WHERE coid=? ALLOW FILTERING
	   List<CassSuperFanWinner> contestWinners = new ArrayList<CassSuperFanWinner>();
		
	   if(results != null) {
		   for (Row row : results) {
			   contestWinners.add( new CassSuperFanWinner(
					   	 row.getInt("coid"),
					   	 row.getInt("cuid"), 
			    		 row.getLong("crdated"),
			    		 row.getInt("no_of_chances")));
		   }
	   }
	   return contestWinners;
	}
	 
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_superfan_winners");
	}
}