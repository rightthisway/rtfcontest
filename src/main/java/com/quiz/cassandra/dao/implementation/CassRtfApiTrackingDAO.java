package com.quiz.cassandra.dao.implementation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassRtfApiTracking;

 
public class CassRtfApiTrackingDAO implements com.quiz.cassandra.dao.service.CassRtfApiTrackingDAO {
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
 
	public void save(CassRtfApiTracking obj) {
		try {
			Statement statement = new SimpleStatement("INSERT INTO rtf_api_tracking(id,start_date,end_date,cust_ip_addr,api_name,platform,device_type,session_id,"
					+ "action_result,cust_id,contest_id,description,device_info,app_ver,res_status,question_no,node_id) VALUES (?, ?,?, ?,?, ?,?, ?,?, ?,?, ?,?,?,?,?,?)", 
					obj.getId(),df.format(obj.getStartDate()),df.format(obj.getEndDate()),obj.getCustIPAddr(),obj.getApiName(),obj.getPlatForm(),obj.getDeviceType(),obj.getSessionId(),
					obj.getActionResult(),obj.getCustId(),obj.getContestId(),obj.getDescription(),obj.getDeviceInfo(),null != obj.getAppVerion()?obj.getAppVerion():"",
							obj.getResStatus(),obj.getQuestSlNo(),obj.getNodeId());
			CassandraConnector.getSession().executeAsync(statement);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void saveForDeviceTimeTracking(CassRtfApiTracking obj) {
		try {
			Statement statement = new SimpleStatement("INSERT INTO rtf_api_tracking(id,start_date,end_date,cust_ip_addr,api_name,platform,device_type,session_id,"
					+ "action_result,cust_id,contest_id,description,device_info,app_ver,device_api_start_time,fb_callback_time,res_status,question_no,rt_count,ans_count,node_id) VALUES (?, ?,?, ?,?, ?,?, ?,?, ?,?, ?,?,?,?,?,?,?,?,?,?)", 
					obj.getId(),df.format(obj.getStartDate()),df.format(obj.getEndDate()),obj.getCustIPAddr(),obj.getApiName(),obj.getPlatForm(),obj.getDeviceType(),obj.getSessionId(),
					obj.getActionResult(),obj.getCustId(),obj.getContestId(),obj.getDescription(),obj.getDeviceInfo(),null != obj.getAppVerion()?obj.getAppVerion():"",obj.getDeviceApiStartTime(),obj.getFbCallbackTime(),
							obj.getResStatus(),obj.getQuestSlNo(),obj.getRetryCount(),obj.getAnsCount(),obj.getNodeId());
			CassandraConnector.getSession().executeAsync(statement);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<CassRtfApiTracking> getAll(){
		
	   List<CassRtfApiTracking> list = new ArrayList<CassRtfApiTracking>();
		
	   ResultSet results = CassandraConnector.getSession().execute("SELECT * from rtf_api_tracking");
			
	   if(results != null) {
		   for (Row row : results) {
			   list.add( new CassRtfApiTracking(
					   	  row.getUUID("id"),
			    		  row.getString("start_date"),
			    		  row.getString("end_date"),
			    		  row.getString("cust_ip_addr"),
			    		  row.getString("api_name"),
					   	  row.getString("platform"),
			    		  row.getString("device_type"),
			    		  row.getString("session_id"),
			    		  row.getString("action_result"),
			    		  row.getInt("cust_id"),
			    		  row.getInt("contest_id"),
			    		  row.getString("description"),
			    		  row.getString("device_info"),
			    		  row.getString("app_ver"),
			    		  row.getString("device_api_start_time"),
			    		  row.getString("fb_callback_time"),
			    		  row.getInt("res_status"),
			    		  row.getString("question_no"),
			    		  row.getInt("rt_count"),
			    		  row.getInt("ans_count"),
			    		  row.getInt("node_id")));
		   }
	   }
	   return list;
	}
	
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE rtf_api_tracking");
	}

  
}