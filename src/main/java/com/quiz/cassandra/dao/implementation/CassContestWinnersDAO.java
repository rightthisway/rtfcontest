package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;

 
public class CassContestWinnersDAO implements com.quiz.cassandra.dao.service.CassContestWinnersDAO {

 
	public void save(CassContestWinners obj) {
		Statement statement = new SimpleStatement("INSERT INTO contest_winners (cuid,coid,crdated,rwdtix,rwdpnts,no_of_chances) "
				+ " VALUES (?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getCrDated(),obj.getrTix(),obj.getrPoints(),obj.getNoOfWinningChance());
 
		CassandraConnector.getSession().executeAsync(statement);
	}
	
	public void saveAll(List<CassContestWinners> winners) {
		
		for (CassContestWinners obj : winners) {
			Statement statement = new SimpleStatement("INSERT INTO contest_winners (cuid,coid,crdated,rwdtix,rwdpnts,no_of_chances) "
					+ " VALUES (?, ?, ?, ?, ?, ?)", 
					obj.getCuId(),obj.getCoId(),obj.getCrDated(),obj.getrTix(),obj.getrPoints(),obj.getNoOfWinningChance());
	 
			CassandraConnector.getSession().executeAsync(statement);
		}
	}
public void saveAllWithBatchUpdate(List<CassContestWinners> winners) {
		
		Session session = CassandraConnector.getSession();		
		BatchStatement batchStatement = new BatchStatement();
		PreparedStatement preparedStatement = session.prepare("INSERT INTO contest_winners (cuid,coid,crdated,rwdtix,rwdpnts,no_of_chances) "
				+ " VALUES (?,?, ?,?, ?, ?)");
		int i = 0;
		for (CassContestWinners obj : winners) {
			batchStatement.add(preparedStatement.bind(obj.getCuId(),obj.getCoId(),obj.getCrDated(),obj.getrTix(),obj.getrPoints(),obj.getNoOfWinningChance()));
			++i;
		}
		try {
			ResultSet rs = session.execute(batchStatement);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public List<CassContestWinners> getContestWinnersByContestId(Integer contestId){
		   ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_winners  WHERE coid=? ", contestId);
		   List<CassContestWinners> contestWinners = new ArrayList<CassContestWinners>();
			
		   if(results != null) {
			   for (Row row : results) {
				   contestWinners.add( new CassContestWinners(
						   	 row.getInt("coid"),
						   	 row.getInt("cuid"),
				    		  row.getInt("rwdtix"),
				    		  row.getDouble("rwdpnts"),
				    		  row.getLong("crdated"),
				    		  row.getInt("no_of_chances")));
			   }
		   }
		   return contestWinners;
		}
	
	public Map<Integer, CassContestWinners> getContestWinnersMapByContestId(Integer contestId){
		   ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_winners  WHERE coid=? ", contestId);
		   Map<Integer, CassContestWinners> winnerMap = new HashMap<Integer, CassContestWinners>();
			
		   if(results != null) {
			   for (Row row : results) {
				   winnerMap.put(row.getInt("cuid"), new CassContestWinners(
						   	 row.getInt("coid"),
						   	 row.getInt("cuid"),
				    		  row.getInt("rwdtix"),
				    		  row.getDouble("rwdpnts"),
				    		  row.getLong("crdated"),
				    		  row.getInt("no_of_chances")));
			   }
		   }
		   return winnerMap;
		}
	
	/*public void updateContestWinnersRewards(Integer contestId,Double rewardPoints) {
		
		SimpleStatement statement = new SimpleStatement("UPDATE contest_winners SET rwdpnts=? WHERE coid=?)",rewardPoints,contestId); 
		CassandraConnector.getSession().executeAsync(statement);
	}*/
	/*public void updateContestWinnersRewards(List<CassContestWinners> winners) {
		
		for (CassContestWinners winner : winners) {
			SimpleStatement statement = new SimpleStatement("UPDATE contest_winners SET rwdpnts=? WHERE coid=? and cuid=?)",new Date(),winner.getCoId(),winner.getCuId()); 
			CassandraConnector.getSession().executeAsync(statement);
		}
		
	}*/
	
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_winners");
	}
	
  
}