package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.list.LiveCustDetails;

 
public class ContestParticipantsDAO implements com.quiz.cassandra.dao.service.ContestParticipantsDAO {

 
	public void save(ContestParticipants obj) {
		Statement statement = new SimpleStatement("INSERT INTO contest_participants (cuid,coid,ipadd,plform,jndate,exdate,status,uid) "
				+ " VALUES (?, ?,?, ?,?, ?,?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getIpAdd(),obj.getPfm(),obj.getJnDate(),obj.getExDate(),obj.getStatus(),obj.getuId());
 
		CassandraConnector.getSession().executeAsync(statement);
	}
	
	public void saveForJoin(ContestParticipants obj) {
		Statement statement = new SimpleStatement("INSERT INTO contest_participants (cuid,coid,ipadd,plform,jndate,status,uid) "
				+ " VALUES (?, ?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getIpAdd(),obj.getPfm(),obj.getJnDate(),obj.getStatus(),obj.getuId());
 
		CassandraConnector.getSession().executeAsync(statement);
	}

	public void updateForExitContest(ContestParticipants obj) {
		Statement statement = new SimpleStatement("UPDATE contest_participants set status = ? , exdate=?,coid = ? where cuid=? ", 
				obj.getStatus(),obj.getExDate(), obj.getCoId(),obj.getCuId());
 
		CassandraConnector.getSession().executeAsync(statement);
		//System.out.println("Exit Update : "+obj.getCuId()+" : "+new Date());
	}

	/*public List<ContestParticipants> getAllParticipantsByContestIdAndContestId(Integer customerId,Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_participants  WHERE cuid = ? and coid=?  ALLOW FILTERING", customerId,contestId);
		   List<ContestParticipants> participants = new ArrayList<ContestParticipants>();
			
		   if(results != null) {
			   for (Row row : results) {
				   participants.add( new ContestParticipants(
						   	 row.getUUID("id"),
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getString("ipadd"),
				    		  row.getString("plform"),
						   	  row.getTimestamp("jndate"),
				    		  row.getTimestamp("exdate"),
				    		  row.getString("status"),
				    		  row.getTimestamp("crdated")));
			   }
		   }
		   return participants;
	}*/
	
	public List<ContestParticipants> getAllParticipantsByContestId(Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT coid,cuid,plform,ipadd,jndate,exdate,status,uid from contest_participants  WHERE coid=?  ALLOW FILTERING",contestId);
		   List<ContestParticipants> participants = new ArrayList<ContestParticipants>();
			
		   if(results != null) {
			   for (Row row : results) {
				   participants.add( new ContestParticipants(
				    		  row.getInt("coid"),
				    		  row.getInt("cuid"),
				    		  row.getString("plform"),
				    		  row.getString("ipadd"),
						   	  row.getLong("jndate"),
				    		  row.getLong("exdate"),
				    		  row.getString("status"),
				    		  row.getString("uid")));
			   }
		   }
		   return participants;
	}
	
	public List<ContestParticipants> getAllParticipants(){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT coid,cuid,plform,ipadd,jndate,exdate,status,uid from contest_participants");
		   List<ContestParticipants> participants = new ArrayList<ContestParticipants>();
			
		   if(results != null) {
			   for (Row row : results) {
				   participants.add( new ContestParticipants(
						   row.getInt("coid"),
				    		  row.getInt("cuid"),
				    		  row.getString("plform"),
				    		  row.getString("ipadd"),
						   	  row.getLong("jndate"),
				    		  row.getLong("exdate"),
				    		  row.getString("status"),
				    		  row.getString("uid")));
			   }
		   }
		   return participants;
	}
	
	public List<LiveCustDetails> getAllLiveCustomerListForCache(Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
				      "SELECT cuid,uid from contest_participants  WHERE status='ACTIVE' LIMIT 100");
		   List<LiveCustDetails> participants = new ArrayList<LiveCustDetails>();
			
		   if(results != null) {
			   for (Row row : results) {
				   participants.add( new LiveCustDetails(
				    		  row.getInt("cuid"),
				    		  row.getString("uid")));
			   }
		   }
		   return participants;
	}
	
	public Integer getLiveCustomerCount(Integer contestId){
		Integer count = 0;
		
		ResultSet results = CassandraConnector.getSession().execute(
				      "SELECT count(*) as cCount from contest_participants  WHERE status='ACTIVE' ");
		   
		   if(results != null) {
			   Row row = results.one();
			   if(row != null) {
				   Long value = row.getLong("cCount");
				   count = value.intValue();
			   }
		   }
		   return count;
	}
	/*public void updateContestParticipantsExistContest(Integer customerId,Integer contestId) {
		SimpleStatement statement = new SimpleStatement("UPDATE contest_participants SET status='EXIT', exdate=? WHERE coid=? and cuid=? ALLOW FILTERING)",new Date(),contestId,customerId); 
		CassandraConnector.getSession().executeAsync(statement);
	}
	public void updateExistingContestParticipantsByJoinContest(Integer customerId,Integer contestId) {
		SimpleStatement statement = new SimpleStatement("UPDATE contest_participants SET status='EXIT' WHERE coid=? and cuid=? ALLOW FILTERING)",contestId,customerId); 
		CassandraConnector.getSession().executeAsync(statement);
	}
	public void updateContestParticipantsForContestResetByContestId(Integer contestId) {
		SimpleStatement statement = new SimpleStatement("UPDATE contest_participants SET status='EXIT', exdate=? WHERE coid=? ALLOW FILTERING)",new Date(),contestId); 
		CassandraConnector.getSession().executeAsync(statement);
	}*/
	
	public void deleteContestParticipantsByCustomerIdandContestId(Integer contestId,Integer customerId) {
		SimpleStatement statement = new SimpleStatement("DELETE from contest_participants WHERE coid=? and cuid=? )",contestId,customerId); 
		CassandraConnector.getSession().executeAsync(statement);
	}
	public void deleteContestParticipantsByCustomerId(Integer customerId) {
		SimpleStatement statement = new SimpleStatement("DELETE from contest_participants WHERE cuid=? )",customerId); 
		CassandraConnector.getSession().executeAsync(statement);
	}
	public void truncate() {
		System.out.println("Truncate Table");
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_participants");
	}
  
}