package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.ContestPasswordAuth;
import com.quiz.cassandra.utils.PollingUtil;

 
public class ContestPasswordAuthDAO implements com.quiz.cassandra.dao.service.ContestPasswordAuthDAO {
	

 
	public void save(ContestPasswordAuth obj) {
		CassandraConnector.getSession().execute(
				"INSERT INTO contest_password_auth (co_id,cu_id, created_date, is_auth, password, updated_date) VALUES (?, ?, ?, ?, ?,?)",
				obj.getCoId(), obj.getCuId(), obj.getCrDate(), obj.getIsAuth(), obj.getPwd(),obj.getUpDate());
	}
	
	public ContestPasswordAuth getContestPasswordAuthByCustomerId(Integer customerId){
	   ResultSet results = CassandraConnector.getSession().execute(
	      "SELECT * from contest_password_auth WHERE cu_id = ? ", customerId);//ALLOW FILTERING
	   //customer_id,user_id,email,phone,no_of_lives,active_reward_points,image_path,is_otp_verified,super_fan_chances
	   ContestPasswordAuth contPwdAuth = null;
	   if(results != null) {
		   Row row = results.one();
		   if(row != null) {
			   contPwdAuth =  new ContestPasswordAuth(
			    		  row.getInt("cu_id"),
			    		  row.getInt("co_id"),
			    		  row.getString("password"),
			    		  row.getBool("is_auth"),
			    		  row.getLong("created_date"),
			    		  row.getLong("updated_date")
					   );
		   }
	   }
	   return contPwdAuth;
	}
	
	
	public List<ContestPasswordAuth> getAll(){
		   ResultSet results = CassandraConnector.getSession().execute("SELECT * from contest_password_auth");
		   List<ContestPasswordAuth> customerList = new ArrayList<ContestPasswordAuth>();
	
		   if(results != null) {
			   for (Row row : results) {
				    
				   customerList.add( new ContestPasswordAuth(
				    		  row.getInt("cu_id"),
				    		  row.getInt("co_id"),
				    		  row.getString("password"),
				    		  row.getBool("is_auth"),
				    		  row.getLong("created_date"),
				    		  row.getLong("updated_date")
						   ));
			   }
		   }
		   return customerList;
	}
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_password_auth");
	}
	
}