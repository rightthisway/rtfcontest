package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassFbCallBackTracking;
import com.quiz.cassandra.data.CustContAnswers;

 
public class CassFbCallBackTrackingDAO implements com.quiz.cassandra.dao.service.CassFbCallBackTrackingDAO {

	public void save(CassFbCallBackTracking obj) {
		Statement statement = new SimpleStatement("INSERT INTO firebase_callback_tracking(cuid,coid,qno,crdated,ipadrs,model,osver,pform,cbtype,message) "
				+ " VALUES (?,?, ?,?, ?,?, ?,?,?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getqNo(),obj.getCreateDate(),obj.getIpAddr(),obj.getdModel(),obj.getOsVer(),obj.getPlatform(),obj.getCbType(),obj.getMessage());
 
		/*Statement statement = new SimpleStatement("INSERT INTO cust_contest_ans(cuid,coid,qid,qno,ans,iscrt,islife,"
				+ "crdated,updated,arwds,curwds,culife) VALUES (?,?, ?,?, ?,?, ?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getqId(),obj.getqNo(),obj.getAns(),obj.getIsCrt(),obj.getIsLife(),
				obj.getCrDate(),obj.getUpDate(),obj.getaRwds(),obj.getCuRwds(),obj.getCuLife());*/
 try {		
	 CassandraConnector.getSession().executeAsync(statement);
		}catch(QueryExecutionException  qex) {
			System.out.println(qex);
		}
		catch(QueryValidationException  qvx) {
		System.out.println(qvx);
		}
	}

	
	
	public List<CassFbCallBackTracking> getAllFbCallBackTrackings(){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT cuid,coid,qno,crdated,ipadrs,model,osver,pform,cbtype,message from firebase_callback_tracking ");
		   List<CassFbCallBackTracking> fbCallBackTrackings = new ArrayList<CassFbCallBackTracking>();
		   if(results != null) {
			   for (Row row : results) {
				   fbCallBackTrackings.add( new CassFbCallBackTracking(
						   	  row.getInt("coid"),  
						   	  row.getInt("cuid"),
				    		  row.getInt("qno"),
				    		  row.getString("pform"),
				    		  row.getString("ipadrs"),
						   	  row.getLong("crdated"),
				    		  row.getString("model"),
				    		  row.getString("osver"),
				    		  row.getString("cbtype"),
				    		  row.getString("message")));
			   }
		   }
		   return fbCallBackTrackings;
		}
	
	
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE firebase_callback_tracking");
	}
}