package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CustContDtls;

 
public class CustContDtlsDAO implements com.quiz.cassandra.dao.service.CustContDtlsDAO {

 
	public void save(CustContDtls obj) throws Exception {
		Statement statement = new SimpleStatement("INSERT INTO cust_contest_dtls(cuid,coid,lqno,lqcrt,lqlife,curwds,culife,sflrwds)"
				+ " VALUES (?,?, ?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getLqNo(),obj.getIsLqCrt(),obj.getIsLqLife(),obj.getCuRwds(),obj.getCuLife(),obj.getSflRwds());
 
		/*Statement statement = new SimpleStatement("INSERT INTO cust_contest_ans(cuid,coid,qid,qno,ans,iscrt,islife,"
				+ "crdated,updated,arwds,curwds,culife) VALUES (?,?, ?,?, ?,?, ?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getqId(),obj.getqNo(),obj.getAns(),obj.getIsCrt(),obj.getIsLife(),
				obj.getCrDate(),obj.getUpDate(),obj.getaRwds(),obj.getCuRwds(),obj.getCuLife());*/
 try {		
	 CassandraConnector.getSession().executeAsync(statement);
		}catch(QueryExecutionException  qex) {
			System.out.println(qex);
		}
		catch(QueryValidationException  qvx) {
		System.out.println(qvx);
		}
	}

	/*public void delete(CustContDtls custContDtls) throws Exception {
		//Statement statement = new SimpleStatement("delete cust_contest_ans where coid=?",contestId);
		Statement statement = new SimpleStatement("delete from cust_contest_dtls where cuid=? ",custContDtls.getCuId());
 
		CassandraConnector.getSession().executeAsync(statement);
	}*/
	
	
	public CustContDtls getCustContDtlsByCustId(Integer customerId) throws Exception {
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_dtls WHERE cuid = ? ", customerId);
			
		   CustContDtls custContDtls = null;
		   if(results != null) {
			   Row row = results.one();
			   if (row != null) {
				  custContDtls =  new CustContDtls(
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("lqno"),
				    		  row.getBool("lqcrt"),
				    		  row.getBool("lqlife"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
				    		  row.getDouble("sflrwds"));
			   }
		   }
		   return custContDtls;
		}
	
	
	public void truncate() throws Exception {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE cust_contest_dtls");
	}
}