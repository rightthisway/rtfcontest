package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CustContAnswers;

 
public class CustContAnswersDAO implements com.quiz.cassandra.dao.service.CustContAnswersDAO {

 
	public void save(CustContAnswers obj) {
		Statement statement = new SimpleStatement("INSERT INTO cust_contest_ans(cuid,coid,qid,qno,ans,iscrt,islife,"
				+ "crdated,updated,arwds,curwds,culife,fb_callback_time,ans_time,rt_count,auto_created) VALUES (?,?, ?,?, ?,?, ?,?, ?,?, ?,?,?,?,?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getqId(),obj.getqNo(),obj.getAns(),obj.getIsCrt(),obj.getIsLife(),
				obj.getCrDate(),obj.getUpDate(),obj.getaRwds(),obj.getCuRwds(),obj.getCuLife(),obj.getFbCallbackTime(),obj.getAnswerTime(),obj.getRetryCount(),obj.getIsAutoCreated());
 
		/*Statement statement = new SimpleStatement("INSERT INTO cust_contest_ans(cuid,coid,qid,qno,ans,iscrt,islife,"
				+ "crdated,updated,arwds,curwds,culife) VALUES (?,?, ?,?, ?,?, ?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getqId(),obj.getqNo(),obj.getAns(),obj.getIsCrt(),obj.getIsLife(),
				obj.getCrDate(),obj.getUpDate(),obj.getaRwds(),obj.getCuRwds(),obj.getCuLife());*/
 try {		
	 CassandraConnector.getSession().executeAsync(statement);
		}catch(QueryExecutionException  qex) {
			System.out.println(qex);
		}
		catch(QueryValidationException  qvx) {
		System.out.println(qvx);
		}
	}

	public void saveForApplyLife(CustContAnswers obj) throws Exception {
		Statement statement = new SimpleStatement("INSERT INTO cust_contest_ans(cuid,coid,qid,qno,iscrt,islife,"
				+ "crdated,updated,arwds,curwds,culife,auto_created) VALUES (?,?, ?,?, ?,?, ?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getqId(),obj.getqNo(),obj.getIsCrt(),obj.getIsLife(),
				obj.getCrDate(),obj.getUpDate(),obj.getaRwds(),obj.getCuRwds(),obj.getCuLife(),obj.getIsAutoCreated());
 
		CassandraConnector.getSession().executeAsync(statement);
	}
	
	/*public List<CustContAnswers> getCustContAnswersByCustIdAndContId(Integer customerId,Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_ans WHERE cuid = ? and coid=? ALLOW FILTERING", customerId,contestId);
		   List<CustContAnswers> custContAnsList = new ArrayList<CustContAnswers>();
			
		   if(results != null) {
			   for (Row row : results) {
				   custContAnsList.add( new CustContAnswers(
						   	 row.getUUID("id"),
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("qid"),
				    		  row.getInt("qno"),
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife"),
				    		  row.getDouble("arwds"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
						   	  row.getTimestamp("crdated"),
				    		  row.getTimestamp("updated")));
			   }
		   }
		   return custContAnsList;
		}
	public CustContAnswers getCustContAnswersByCustIdAndQuestId(Integer customerId,Integer questionId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_ans WHERE cuid = ? and qid=? ALLOW FILTERING", customerId,questionId);
			
		   CustContAnswers custContAns = null;
		   if(results != null) {
			   Row row = results.one();
			   if (row != null) {
				   custContAns =  new CustContAnswers(
						   	 row.getUUID("id"),
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("qid"),
				    		  row.getInt("qno"),
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife"),
				    		  row.getDouble("arwds"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
						   	  row.getTimestamp("crdated"),
				    		  row.getTimestamp("updated"));
			   }
		   }
		   return custContAns;
		}*/
	
	public List<CustContAnswers> getCustContAnswersByContIdandQuestionNo(Integer contestId,Integer questionNo){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT cuid,qid,ans,iscrt,islife from cust_contest_ans WHERE qno=?",questionNo);//coid,qno,//coid=? and
		   List<CustContAnswers> custContAnsList = new ArrayList<CustContAnswers>();
			
		   if(results != null) {
			   for (Row row : results) {
				   custContAnsList.add( new CustContAnswers(
				    		  row.getInt("cuid"),
				    		  contestId,
				    		  row.getInt("qid"),
				    		  questionNo,
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife")));
			   }
		   }
		   System.out.println("CUSTANS : "+contestId+" :qNo: "+questionNo+" :size: "+custContAnsList.size());
		   return custContAnsList;
		}
	
	
	
  
	/*public List<CustContAnswers> getAllCustContAnswersByQuestId(Integer questionId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_ans WHERE qid=? ALLOW FILTERING",questionId);
		   List<CustContAnswers> custContAnsList = new ArrayList<CustContAnswers>();
			
		   if(results != null) {
			   for (Row row : results) {
				   custContAnsList.add( new CustContAnswers(
						   	 row.getUUID("id"),
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("qid"),
				    		  row.getInt("qno"),
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife"),
				    		  row.getDouble("arwds"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
						   	  row.getTimestamp("crdated"),
				    		  row.getTimestamp("updated")));
			   }
		   }
		   return custContAnsList;
		}
	*/
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE cust_contest_ans");
	}
}