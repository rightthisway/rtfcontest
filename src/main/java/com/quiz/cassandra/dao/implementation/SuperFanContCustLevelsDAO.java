package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContDtls;
import com.quiz.cassandra.data.SuperFanContCustLevels;

 
public class SuperFanContCustLevelsDAO implements com.quiz.cassandra.dao.service.SuperFanContCustLevelsDAO {

 
	public void save(SuperFanContCustLevels obj) throws Exception {
		Statement statement = new SimpleStatement("INSERT INTO quiz_super_fan_contest_cust_levels(coid,cuid,sf_stars,q_sl_no)"
				+ " VALUES (?,?, ?,?)", 
				obj.getCoId(),obj.getCuId(),obj.getSfStars(),obj.getqNo());
 
 try {		
	 CassandraConnector.getSession().executeAsync(statement);
		}catch(QueryExecutionException  qex) {
			System.out.println(qex);
		}
		catch(QueryValidationException  qvx) {
		System.out.println(qvx);
		}
	}

	public List<SuperFanContCustLevels> getAllSuperFanContCustLevels(){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT coid,cuid,sf_stars,q_sl_no from quiz_super_fan_contest_cust_levels");
		   List<SuperFanContCustLevels> sfContCustLevels = new ArrayList<SuperFanContCustLevels>();
			
		   if(results != null) {
			   for (Row row : results) {
				   System.out.println("inside select statement "+row.getInt("coid")+" : "+row.getInt("cuid")+" : "+
				    		  row.getInt("sf_stars")+" : "+
				    		  row.getInt("q_sl_no"));
				   sfContCustLevels.add( new SuperFanContCustLevels(
							  row.getInt("cuid"),
							  row.getInt("coid"),
							  row.getInt("q_sl_no"),
				    		  row.getInt("sf_stars")));
			   }
		   }
		   return sfContCustLevels;
		}
	
	
	public void truncate() throws Exception {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE quiz_super_fan_contest_cust_levels");
	}
}