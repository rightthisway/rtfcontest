package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.utils.PollingUtil;

 
public class CassCustomerDAO implements com.quiz.cassandra.dao.service.CassCustomerDAO {
	

 
	public void saveCustomer(CassCustomer obj) {
		CassandraConnector.getSession().execute(
				"INSERT INTO customer (customer_id, user_id, email, phone, no_of_lives,active_reward_points,image_path,is_otp_verified,super_fan_chances,rtf_points,is_blocked,rtf_cur_val_bal"
				+ ") VALUES (?, ?, ?, ?, ?,?,?,?,?,?,?,?)",
				obj.getId(), obj.getuId(), obj.getEmail(), obj.getPhone(), obj.getqLive(),obj.getcRewardDbl(),obj.getImgP(),obj.getIsOtp(),
				obj.getSfcCount(),obj.getRtfPoints(),obj.getIsBlkd(),obj.getLoyaltyPoints());
	}
	
	public void saveAll(List<CassCustomer> objList) {
		
		Session session = CassandraConnector.getSession();		
		BatchStatement batchStatement = new BatchStatement();
		PreparedStatement preparedStatement = session.prepare("INSERT INTO customer (customer_id, user_id, email, phone, no_of_lives,active_reward_points,image_path,"
				+ "is_otp_verified,super_fan_chances,rtf_points,is_blocked,rtf_cur_val_bal) VALUES (?, ?, ?, ?, ?,?,?,?,?,?,?,?)");
		int i = 0;
		for (CassCustomer obj : objList) {
			batchStatement.add(preparedStatement.bind(obj.getId(), obj.getuId(), obj.getEmail(), obj.getPhone(), obj.getqLive(),obj.getcRewardDbl(),
					obj.getImgP(),obj.getIsOtp(),obj.getSfcCount(),obj.getRtfPoints(),obj.getIsBlkd(),obj.getLoyaltyPoints()));
			++i;
		}
		try {
			ResultSet rs = session.execute(batchStatement);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void updateCustomerLives(Integer customerId, Integer quizLives) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set no_of_lives = ? where customer_id = ? ", quizLives, customerId);
	}
	public void updateCustomerLives(Integer customerId, Integer quizLives,CassCustomer customer) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set no_of_lives = ? where customer_id = ?  ", quizLives, customerId);
	}
	
	public void updateProfilePicName(Integer customerId, String imgPath) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set image_path = ? where customer_id = ? ", imgPath, customerId);
	}

	public void updateCustomerRewardsAndLives(Integer customerId, Integer quizLives,Double rewards) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set no_of_lives = ?,active_reward_points=? where customer_id = ?  ", quizLives,rewards, customerId);
	}
	
	public void updateCustomerRewardsByCustomerId(Integer customerId, Double rewards) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set active_reward_points=? where customer_id = ?  ", rewards, customerId);
	}
 
	public CassCustomer getCustomerById(Integer customerId){
	   ResultSet results = CassandraConnector.getSession().execute(
	      "SELECT * from customer WHERE customer_id = ? ", customerId);//ALLOW FILTERING
	   //customer_id,user_id,email,phone,no_of_lives,active_reward_points,image_path,is_otp_verified,super_fan_chances
	    CassCustomer customer = null;
	   if(results != null) {
		   Row row = results.one();
		   if(row != null) {
			   customer =  new CassCustomer(
			    		  row.getInt("customer_id"),
			    		  row.getString("user_id"),
			    		  row.getString("email"),
			    		  row.getString("phone"),
			    		  row.getInt("no_of_lives"),
			    		  row.getDouble("active_reward_points"),
			    		  //row.getDecimal("active_reward_points")!= null?row.getDecimal("active_reward_points").doubleValue():null,
			    		  row.getString("image_path"),
			    		  //Boolean.valueOf(String.valueOf(row.getInt("is_otp_verified"))
			    		  row.getBool("is_otp_verified"),
			    		  row.getInt("super_fan_chances"),
			    		  PollingUtil.getZeroIfNull(row.getInt("magicwand")),
			    		  row.getInt("rtf_points"),
			    		  row.getBool("is_blocked"),
			    		  row.getInt("rtf_cur_val_bal")
			    		 
			    		  
					   );
		   }
	   }
	   return customer;
	}
	
	
	public List<CassCustomer> getAll(){
		   ResultSet results = CassandraConnector.getSession().execute("SELECT * from customer");
		   List<CassCustomer> customerList = new ArrayList<CassCustomer>();
	
		   if(results != null) {
			   for (Row row : results) {
				    
				   customerList.add( new CassCustomer(
				    		  row.getInt("customer_id"),
				    		  row.getString("user_id"),
				    		  row.getString("email"),
				    		  row.getString("phone"),
				    		  row.getInt("no_of_lives"),
				    		  row.getDouble("active_reward_points"),
				    		  //row.getDecimal("active_reward_points")!= null?row.getDecimal("active_reward_points").doubleValue():null,
				    		  row.getString("image_path"),
				    		  //Boolean.valueOf(String.valueOf(row.getInt("is_otp_verified"))
				    		  row.getBool("is_otp_verified"),
				    		  row.getInt("super_fan_chances"),
				    		  PollingUtil.getZeroIfNull(row.getInt("magicwand")),
				    		  row.getInt("rtf_points"),
				    		  row.getBool("is_blocked"),
				    		  row.getInt("rtf_cur_val_bal")
				    		  
				    		  
						   ));
			   }
		   }
		   return customerList;
	}
	
	public void deleteCustomer(Integer customerId){
	   String deleteString = "DELETE FROM customer WHERE customer_id = ?";
	   CassandraConnector.getSession().execute(deleteString,customerId);
	}
	
	public void deleteAll(){
	   String deleteString = "TRUNCATE customer";
	   CassandraConnector.getSession().execute(deleteString);
	}
	
	public void getCount(){
	   ResultSet results = CassandraConnector.getSession().execute("SELECT count(*) as tot from customer ");
	   if(results != null) {
		   Row row = results.one();
		   if(row != null) {
			   System.out.println(row.getInt("tot"));
		   }
	   }
	}
	public void updateCustomerRtfPoints(Integer customerId, Integer rtfPoints) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set rtf_points = ? where customer_id = ? ", rtfPoints, customerId);
	}
}