package com.quiz.cassandra.exception;

public class NoRewardConfigException extends Exception {
	
	
	public NoRewardConfigException() {
		
	}
	
	private String desc;
	private String eCode;

	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String geteCode() {
		return eCode;
	}
	public void seteCode(String eCode) {
		this.eCode = eCode;
	}
	@Override
	public String toString() {
		return "Error [des=" + desc + "]";
	}
}
