package com.quiz.cassandra.thread;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.quiz.cassandra.utils.CassContestUtil;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;

public class MegaJackpotThread implements Runnable{
	
	public static Map<String, Boolean> contestQuestionMap = new HashMap<String, Boolean>();
	private QuizContest contest;
	private QuizContestQuestions questionObj;
	HttpServletRequest request;
	
	public MegaJackpotThread(){
		
	}
	
	public MegaJackpotThread(QuizContest contest, QuizContestQuestions questionObj,HttpServletRequest request){
		this.contest = contest;
		this.questionObj = questionObj;
		this.request = request;
	}
	
	public static Boolean getProcessedQuestionMap(Integer contestId, Integer questionId) {
		return contestQuestionMap.get(contestId+"-"+questionId);
	}
	
	public static void refereshMap() {
		contestQuestionMap = new HashMap<String, Boolean>();
	}
	
	@Override
	public void run() {
		try {
			contestQuestionMap.put(contest.getId()+"-"+questionObj.getId(), true);
			CassContestUtil.computeContestMegaJackpotWinners(contest, questionObj);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
