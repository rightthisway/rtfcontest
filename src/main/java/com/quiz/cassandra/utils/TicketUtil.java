package com.quiz.cassandra.utils;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class TicketUtil {
	private static DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	//private static DecimalFormat decimalFormat = new DecimalFormat("#0.00");
	private static List<String> generalAdmissionList = new ArrayList<String>();;
	private static String textStyle = "justify";
	private static DecimalFormat df2 = new DecimalFormat(".##");
	private static DecimalFormat normalRoundOff = new DecimalFormat(".##");
	public static DecimalFormat percentageDF = new DecimalFormat("#.##");
	
	static{
		if(generalAdmissionList.size() <=0 ){
			generalAdmissionList.add("GA");
			generalAdmissionList.add("General Admission");
			generalAdmissionList.add("Gen Ad");
			generalAdmissionList.add("General Ad");
			generalAdmissionList.add("General Adm");
			generalAdmissionList.add("GenAd");
		}
	}
	
	
	public static Double getRoundedValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.DOWN);
		return Double.valueOf(df2.format(value));
	}
	
	public static Double getRoundedUpValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.UP);
		return Double.valueOf(df2.format(value));
	}
	
	public static Double getNormalRoundedValue(Double value) throws Exception {
		return Double.valueOf(df2.format(value));
	}
	
	
	public static String getRoundedValueString(Double value) throws Exception {
		return String.format( "%.2f", value);
	}
	
	public static Integer getRoundedValueIntger(Double value) throws Exception {
		value = Math.floor(value);
		return value.intValue();
	}
	 
	 
}
