package com.quiz.cassandra.utils;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassContestGrandWinner;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CassSuperFanWinner;
import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContDtls;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.AnswerCountInfo;
import com.quiz.cassandra.list.CassJoinContestInfo;
import com.quiz.cassandra.list.ContSummaryInfo;
import com.quiz.cassandra.list.HallOfFameDtls;
import com.quiz.cassandra.list.LiveCustDetails;
import com.quiz.cassandra.list.LiveCustomerListInfo;
import com.quiz.cassandra.thread.MegaJackpotThread;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.RTFBotsUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * 
 * @author Tamil
 *
 */
public class CassContestUtil {	
	
	
	static QuizContest startedContest = null;
//Phase 2 Commented
	//static  Map<String,CustContAnswers> customerQuestionAnsMap = new ConcurrentHashMap<String,CustContAnswers>();
	public static Map<Integer,CassCustomer> cassCustomerMap = new ConcurrentHashMap<Integer, CassCustomer>();
	static Map<Integer,QuizContestQuestions> contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
	static Map<Integer,SuperFanContCustLevels> sfContCustLevelsMap = new ConcurrentHashMap<Integer,SuperFanContCustLevels>();
	static Map<Integer,Double> sfQuestionLevelRewardsMap = new ConcurrentHashMap<Integer, Double>();
	//static  Map<Integer,CustContAnswers> customerContestAnsMap = new ConcurrentHashMap<Integer,CustContAnswers>();
//Phase 2 Commented
	//static Map<Integer,List<Integer>> customersCountMap = new ConcurrentHashMap<Integer, List<Integer>>();
	//static Map<Integer,CassCustomer> liveCustoemrDtlMap = new ConcurrentHashMap<Integer, CassCustomer>();
	//static List<CassContestWinners> contestSummaryList = new ArrayList<CassContestWinners>();
//Phase 2 Commented
	//static Map<Integer,ContestParticipants> contParticipantsMap = new ConcurrentHashMap<Integer, ContestParticipants>();
	static List<LiveCustDetails> liveCustomerListList = new ArrayList<LiveCustDetails>();
	static Map<Integer,QuizContestQuestions> contestCurrentQuestionsMap = new ConcurrentHashMap<Integer, QuizContestQuestions>(); // HC
	static Map<Integer,CassContestWinners> superFanWinnersMap = new ConcurrentHashMap<Integer,CassContestWinners>();
	
	static List<HallOfFameDtls> thisWeekHallOfFameList = new ArrayList<HallOfFameDtls>();
	static List<HallOfFameDtls> tillDateHallOfFameList = new ArrayList<HallOfFameDtls>();
	
	private static Logger log = LoggerFactory.getLogger(CassContestUtil.class);
	
	static Integer contestSummaryDataSize = 100;
	static Integer contestWinnersSummaryDataSize = 200;
	public static Integer hallOfFamePageMaxDataSize = 100;//25
	public static Boolean HIDE_DASHBOARD_REWARDS = Boolean.FALSE;
	
	private static Integer liveCustCacheCount = 100;
	private static Integer lastLiveCustFetchCount = 0;
	private static Long lastLiveCustFetchTime = null;
	private static Long liveCustRefreshInterval = 1000*30l;
	
	public static Boolean SuperFanLevelEnabled = Boolean.TRUE;
	
	
	public static Integer getHallOfFamePageMaxDataSize() {
		return hallOfFamePageMaxDataSize;
	}
	
	public static void updatePostMigrationStats() throws Exception {
		CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
		CassContestUtil.hallOfFamePageMaxDataSize = 100;
		
		refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
	}
	
	public static void refreshHallOfFameDataCache() throws Exception {
		CassContestUtil.hallOfFamePageMaxDataSize = 100;
		
		//refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
	}
	
	public static void loadApplicationValues() throws Exception {
		URLUtil.loadApplicationValues();
		CassandraConnector.loadApplicationValues();
		DatabaseConnections.loadApplicationValues();
		
		try {
			URLUtil.computeClusterNodeId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
	}
	
	public static void updateCassCustomerMap(CassCustomer customer) {
		cassCustomerMap.put(customer.getId(), customer);
	}
	public static CassCustomer getCasscustomerFromMap(Integer customerId) {
		CassCustomer customer = cassCustomerMap.get(customerId);
		if(customer == null) {
			//System.out.println("CUST to DB HIT : "+customerId);
			customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if(customer != null) {
				cassCustomerMap.put(customer.getId(), customer);
			}
			
		}
		return customer;
	}
	public static void clearCacheDataByContestId(Integer contestId) throws Exception {
		
//Phase 2 Commented
		//customerQuestionAnsMap = new ConcurrentHashMap<String, CustContAnswers>();
		//cassCustomerMap = new ConcurrentHashMap<Integer,CassCustomer>();
		contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
		startedContest = null;
		sfContCustLevelsMap = new ConcurrentHashMap<Integer, SuperFanContCustLevels>();
		sfQuestionLevelRewardsMap = new ConcurrentHashMap<Integer, Double>();
		
		//customerContestAnsMap = new ConcurrentHashMap<Integer, CustContAnswers>();
//Phase 2 Commented
		//customersCountMap.remove(contestId);
		
		//contestSummaryList = new ArrayList<CassContestWinners>();
//Phase 2 Commented
		//liveCustoemrDtlMap = new ConcurrentHashMap<Integer,CassCustomer>();
//Phase 2 Commented		
		//contParticipantsMap = new ConcurrentHashMap<Integer,ContestParticipants>();
		
		contestCurrentQuestionsMap = new ConcurrentHashMap<Integer,QuizContestQuestions>();
		
		MegaJackpotThread.refereshMap();
		
		liveCustomerListList = new ArrayList<LiveCustDetails>();
		lastLiveCustFetchCount = 0;
		lastLiveCustFetchTime = null;
//Fix This		
		refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
		
		//QuizDAORegistry.getQuizContestParticipantsDAO().updateContestParticipantsForContestResetByContestId(contestId);
		//forceManualefreshSummaryDataTable();
		//QuizConfigSettingsUtil.forceUpdateConfigSettings();

//Temp		
		
	}
	
	public static void refreshCacheDataByContestId(Integer contestId) throws Exception {
		
	}
	
	public static QuizContest getCurrentContestByContestId(Integer contestId) {
		if(startedContest == null) {
//Fix This			
			return  SQLDaoUtil.getQuizContestForId(String.valueOf(contestId));
		}
		if(startedContest != null && startedContest.getId().equals(contestId)) {
			return startedContest;
		}
		return null;
	}
	public static Boolean refreshStartContestData(String contestType) throws Exception {
//Fix This		
		QuizContest contest = SQLDaoUtil.getCurrentStartedContest(contestType);
		if(contest != null) {
			startedContest = contest;
			List<QuizContestQuestions> questionsList = SQLDaoUtil.getContestQuestions( String.valueOf(contest.getId())   );
			for (QuizContestQuestions question : questionsList) {
				contestQuestionMAp.put(question.getId(), question);
			}
			addSuperFanGameWinnersToCache(contest.getId());
			
			//fix This Level
			refreshSuperFanContCustLevelsMap(contest);
			return true;
		} 
		return false;
	}
	public static Boolean refreshSuperFanContCustLevelsMap(QuizContest contest) throws Exception {
		
		Map<Integer,SuperFanContCustLevels> tempMap = new ConcurrentHashMap<Integer,SuperFanContCustLevels>();
		List<SuperFanContCustLevels> sfContCustLevelsList = CassandraDAORegistry.getSuperFanContCustLevelsDAO().getAllSuperFanContCustLevels();
		for (SuperFanContCustLevels sfContCustLevel : sfContCustLevelsList) {
			tempMap.put(sfContCustLevel.getCuId(), sfContCustLevel);
		}
		sfContCustLevelsMap = new ConcurrentHashMap<Integer,SuperFanContCustLevels>(tempMap);
		
		System.out.println(" Refresh sfContCustLevelsList : "+ sfContCustLevelsList.size()+" : "+ new Date());
		
		/*List<QuizContestQuestions> questionsList = SQLDaoUtil.getContestQuestions(String.valueOf(contest.getId()));
		Map<Integer,QuizContestQuestions> tempQuestSlNoMap = new HashMap<Integer,QuizContestQuestions>();
		for (QuizContestQuestions question : questionsList) {
			tempQuestSlNoMap.put(question.getQuestionSNo(), question);
		}
		Double rewards = 0.0;
		for (int i=1;i<=contest.getNoOfQuestions();i++) {
			QuizContestQuestions question = tempQuestSlNoMap.get(i);
			if(question != null && question.getQuestionRewards() != null) {
				rewards = rewards + question.getQuestionRewards();
			}
			sfQuestionLevelRewardsMap.put(i, rewards);
		}*/
		return true;
	}
	
	public static SuperFanContCustLevels getSuperFanContCustLevels(Integer cuId,Integer coId) {

		return sfContCustLevelsMap.get(cuId);
	}
	public static Map<Integer,SuperFanContCustLevels> getSuperFanContCustLevelsMap() {
		return sfContCustLevelsMap;
	}
	
	public static void addSuperFanGameWinnersToCache(Integer contestId) {
		List<CassSuperFanWinner> superFanWinners = CassandraDAORegistry.getCassSuperFanWinnerDAO().getSuperFanWinnersByContestId();
		 if(null != superFanWinners && !superFanWinners.isEmpty()) {
			 Date now = new Date();
			 CassContestWinners contestWinnerObj = null;
			 Map<Integer,CassContestWinners> superFanWinnersMapTemp = new ConcurrentHashMap<Integer,CassContestWinners>();
			 for (CassSuperFanWinner superFanWinnerObj : superFanWinners) {
				 try {
					 contestWinnerObj = new CassContestWinners(contestId, superFanWinnerObj.getCuId(), 0, 0.00, now.getTime(),superFanWinnerObj.getNoOfWinningChance());
					 superFanWinnersMapTemp.put(superFanWinnerObj.getCuId(), contestWinnerObj);
				 }catch(Exception e) {
					 e.printStackTrace();
				 }
			 }
			 superFanWinnersMap.clear();
			 superFanWinnersMap= new HashMap<>(superFanWinnersMapTemp);
		 }else {
			 superFanWinnersMap.clear();
			 superFanWinnersMap= new HashMap<>(); 
		 }
	}
	
	public static QuizContest getContestByIDForStartContest(Integer contestId) {
		return  SQLDaoUtil.getQuizContestForId(String.valueOf(contestId));
	}
	public static void refreshEndContestData() {
		startedContest = null;
		contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
	}
	public static QuizContest getCurrentStartedContest(String contestType) {
		return startedContest;
	}
	public static QuizContestQuestions getQuizContestQuestionById(Integer questionId) {
		return contestQuestionMAp.get(questionId);
		//return QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(questionId);
	}
	
	public static QuizContestQuestions getQuizContestQuestionByContestIdandQuestionSlNo(Integer contestId,Integer questionNo) {
		List<QuizContestQuestions> questions = new ArrayList<QuizContestQuestions>(contestQuestionMAp.values());
		for (QuizContestQuestions quizQuestion : questions) {
			if(quizQuestion.getQuestionSNo().equals(questionNo)) {
				return quizQuestion;
			}
		}
		return null;
		//return QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(questionId);
	}

//Phase 2 Commented	Not In Use
	/*public static ContestParticipants getContestParticipantsByCustId(Integer customerId) {
		return contParticipantsMap.get(customerId);
	}
	public static ContestParticipants updateContestParticipantsMapByCustId(ContestParticipants contPArticipants) {
		return contParticipantsMap.put(contPArticipants.getCuId(),contPArticipants);
	}*/
	
	public static List<LiveCustDetails> getLiveCustomersList(Integer contestId) {
		Long now = new Date().getTime();
		if(lastLiveCustFetchTime == null || (now - lastLiveCustFetchTime) >= liveCustRefreshInterval || lastLiveCustFetchCount < liveCustCacheCount) {
			Integer count = refreshLiveCustomersList(contestId);
			lastLiveCustFetchTime = now;
			lastLiveCustFetchCount = count;
		}
		return new ArrayList<LiveCustDetails>(liveCustomerListList);
	}
	public static Integer refreshLiveCustomersList(Integer contestId) {
		List<LiveCustDetails> list = CassandraDAORegistry.getContestParticipantsDAO().getAllLiveCustomerListForCache(contestId);
		if(list != null && !list.isEmpty()) {
			liveCustomerListList = new ArrayList<LiveCustDetails>(list);
			return liveCustomerListList.size();
		}
		return 0;
	}

//Phase 2 Commented	Not In Use
	/*public static void updateExistingContestParticipantsStatusByJoinContest(Integer customerId) {
		ContestParticipants contParticipant = contParticipantsMap.get(customerId);
		if(contParticipant != null) {
			contParticipant.setStatus("EXIT");
			//contParticipant.setExDate(new Date());
			
			//CassandraDAORegistry.getContestParticipantsDAO().saveForJoin(contParticipant);
		}
	}*/
	
//Phase 2 Commented
	/*public static void updateExistingContestParticipantsStatusByExitContest(Integer customerId) {
		ContestParticipants contParticipant = contParticipantsMap.remove(customerId);
		if(contParticipant != null) {
			contParticipant.setStatus("EXIT");
			contParticipant.setExDate(new Date().getTime());

			//CassandraDAORegistry.getContestParticipantsDAO().updateForExitContest(contParticipant);
		}
	}*/
//Phase 2 Added
	public static void updateExistingContestParticipantsStatusByExitContest(Integer customerId,Integer contestId) {
		
		//CassandraDAORegistry.getContestParticipantsDAO().deleteContestParticipantsByCustomerId(customerId);
		
		ContestParticipants participant = new ContestParticipants();
		participant.setCoId(contestId);
		participant.setCuId(customerId);
		participant.setStatus("EXIT");
		participant.setExDate(new Date().getTime());
		CassandraDAORegistry.getContestParticipantsDAO().updateForExitContest(participant);
	}
	
	public static CassJoinContestInfo updateJoinContestCustomersCount(CassJoinContestInfo joinContestInfo,Integer contestId,CassCustomer customer) throws Exception {
		try {
		//Integer count=0;
		//boolean isExistingContestant=false;

//Phase 2 Commented
		/*List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount == null) {
			contestCount = new ArrayList<Integer>();
		}
		if(contestCount.contains(customer.getId())) {
			//isExistingContestant = true;
		} else {
			contestCount.add(customer.getId());
		}
		//count = contestCount.size();
		customersCountMap.put(contestId, contestCount);
		liveCustoemrDtlMap.put(customer.getId(), customer);*/
		
//to capture visitors count		
		/*Set<Integer> visitosList = contestVisitorsCountMap.get(contestId);
		if(visitosList == null) {
			visitosList = new HashSet<Integer>();
		}
		if(!visitosList.add(customerId)) {
			isExistingContestant = true;
		}
		contestVisitorsCountMap.put(contestId,visitosList);*/
//end		
		
		//joinContestInfo.setTotalUsersCount(count);
		//joinContestInfo.setIsExistingContestant(isExistingContestant);
		
		
		CustContDtls custContDtls = getCustomerAnswers(customer.getId(), contestId);
		if(custContDtls != null) {
			joinContestInfo.setLqNo(custContDtls.getLqNo());
			if((custContDtls.getIsLqCrt() != null && custContDtls.getIsLqCrt()) || 
					(custContDtls.getIsLqLife() != null && custContDtls.getIsLqLife())){
				joinContestInfo.setIsLstCrt(Boolean.TRUE);	
			} else {
				joinContestInfo.setIsLstCrt(Boolean.FALSE);
			}
			joinContestInfo.setCaRwds(custContDtls.getCuRwds());
			if(custContDtls.getCuLife() != null && custContDtls.getCuLife() > 0) {
				joinContestInfo.setIsLifeUsed(Boolean.TRUE);
			} else {
				joinContestInfo.setIsLifeUsed(Boolean.FALSE);
			}
			
			//isExistingContestant = true;
			
		} else {
			joinContestInfo.setLqNo(0);
			joinContestInfo.setIsLstCrt(Boolean.FALSE);
			joinContestInfo.setCaRwds(0.0);
		}
		

/*		
		CustContAnswers customerAnswer = getCustomerAnswers(customer.getId(), contestId);
		if(customerAnswer != null) {
			joinContestInfo.setLqNo(customerAnswer.getqNo());
			if((customerAnswer.getIsCrt() != null && customerAnswer.getIsCrt()) || 
					(customerAnswer.getIsLife() != null && customerAnswer.getIsLife())){
				joinContestInfo.setIsLstCrt(Boolean.TRUE);	
			} else {
				joinContestInfo.setIsLstCrt(Boolean.FALSE);
			}
			joinContestInfo.setCaRwds(customerAnswer.getCuRwds());
			if(customerAnswer.getCuLife() != null && customerAnswer.getCuLife() > 0) {
				joinContestInfo.setIsLifeUsed(Boolean.TRUE);
			} else {
				joinContestInfo.setIsLifeUsed(Boolean.FALSE);
			}
			
			//isExistingContestant = true;
			
		} else {
			joinContestInfo.setLqNo(0);
			joinContestInfo.setIsLstCrt(Boolean.FALSE);
			joinContestInfo.setCaRwds(0.0);
		}
		
*/		
		//if(!isExistingContestant) {
		//	isExistingContestant = QuizDAORegistry.getQuizContestParticipantsDAO().isExistingContestant(customerId, contestId);
		//}
		//quizJoinContestInfo.setIsExistingContestant(isExistingContestant);
		
		
//		quizJoinContestInfo.setIsContestLifeLineUsed(QuizContestUtil.checkCustomerContestLifeLineUsage(customerId, contestId));
		
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return joinContestInfo;
	}

//Phase 2 Commented
	/*public static CassJoinContestInfo updateQuizExitCustomersCount(CassJoinContestInfo joinContestInfo,Integer contestId,Integer customerId) throws Exception {
		//Integer count=0;
		//boolean isExistingContestant=false;
		List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount == null) {
			contestCount = new ArrayList<Integer>();
		}
		//if(contestCount.remove(customerId)) {
		//	isExistingContestant = true;
		//}
		contestCount.remove(customerId);
		//count = contestCount.size();
		customersCountMap.put(contestId, contestCount);

		liveCustoemrDtlMap.remove(customerId);
		
		return joinContestInfo;
	}*/
	
	public static Integer getContestCustomersCount(Integer contestId) throws Exception {
		Integer count=0;
		/*List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount != null) {
			count = contestCount.size();
		}*/		

//Phase 2 UnCommented
		count = CassandraDAORegistry.getContestParticipantsDAO().getLiveCustomerCount(contestId);

//Phase 2 Commented
		/*if(liveCustoemrDtlMap != null) {			
			count=liveCustoemrDtlMap.size();
			//System.out.println("New count @ " + new Date() + " is >>"  +  count);
			if(count == 0) { // Temp code... to remove if above count is correct
				//System.out.println("Old count @ " + new Date() + " is >>"  +  count);
				List<Integer> contestCount = customersCountMap.get(contestId);
				if(contestCount != null) {
					count = contestCount.size();				
				}			
			}
		}*/
		return count;
		
	}
	public static Boolean updateContestWinners(Integer contestId,Integer customerId) {
		
		/*QuizContest contest = getQuizContestByContestId(contestId);
		if(contest == null || contest.getProcessStatus() != null) {
			return false;
		}*/
		
		CassContestWinners contestWinner = new CassContestWinners();
		contestWinner.setCoId(contestId);
		contestWinner.setCuId(customerId);
		contestWinner.setCrDated(new Date().getTime());
		contestWinner.setrTix(0);
		contestWinner.setrPoints(0.0);
		contestWinner.setNoOfWinningChance(1);
		
		CassandraDAORegistry.getCassContestWinnersDAO().save(contestWinner);
		
		/*QuizContestWinners contestWinner = new QuizContestWinners();
		contestWinner.setCustomerId(customerId);
		contestWinner.setContestId(contestId);
		contestWinner.setCreatedDateTime(new Date());
		
		contestWinner.setRewardTickets(0);
		contestWinner.setRewardType(RewardType.POINTS);
		contestWinner.setRewardPoints(0.0);
		contestWinner.setRewardRank(0);
		QuizDAORegistry.getQuizContestWinnersDAO().save(contestWinner);*/

		//DAORegistry.getCustomerDAO().update(customer);
		//CassCustomerUtil.updatedCustomerUtil(customer);
		
/*		Map<Integer,QuizContestWinners> contestwinnersMap = qcu.contestWinnersMap.get(contestId);
		if(contestwinnersMap == null) {
			contestwinnersMap = new ConcurrentHashMap<Integer,QuizContestWinners>();
		}
		contestwinnersMap.put(customerId,contestWinner);
		qcu.contestWinnersMap.put(contestId, contestwinnersMap);
		
		//contestMap.put(contestId, contest);
		log.info( "[updateContestWinners -  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + contestwinnersMap.size());
*/		return true;
	}
//Phase 2 Commented
	/*public static CustContAnswers getCustomerQuestionAnswers(Integer customerId,Integer questionNo,Integer contestId) throws Exception {
		String key = customerId+"_"+questionNo;
		return customerQuestionAnsMap.get(key);
	}	
	public static Boolean updateCustomerQuestionAnswerMap(CustContAnswers customerAnswers) throws Exception {
		String key = customerAnswers.getCuId()+"_"+customerAnswers.getqNo();
		customerQuestionAnsMap.put(key, customerAnswers);
		return true;
	}*/
	
	/*public static CustContAnswers getCustomerAnswers(Integer customerId,Integer contestId) throws Exception {
		return customerContestAnsMap.get(customerId);
	}	
	public static Boolean updateCustomerAnswerMap(CustContAnswers customerAnswers) throws Exception {
		customerContestAnsMap.put(customerAnswers.getCuId(), customerAnswers);
		return true;
	}*/
	
	public static CustContDtls getCustomerAnswers(Integer customerId,Integer contestId) throws Exception {
		return CassandraDAORegistry.getCustContDtlsDAO().getCustContDtlsByCustId(customerId);
	}	
//Fix This
	/*public static Boolean updateCustomerAnswerMap(CustContAnswers customerAnswers) throws Exception {
		//customerContestAnsMap.put(customerAnswers.getCuId(), customerAnswers);
		//CassandraDAORegistry.getCustContDtlsDAO().save(obj);
		return true;
	}*/
	
//Fix This
	public static Boolean updateCustomerAnswers(CustContAnswers customerAnswers) throws Exception {
		boolean flag=true;
		
		/*customerQuestionAnsMap.put(customerAnswers.getCuId()+"_"+customerAnswers.getqNo(), customerAnswers);
		
		CustContAnswers mapAnsObj = getCustomerAnswers(customerAnswers.getCuId(), customerAnswers.getCoId());
		if(mapAnsObj == null || mapAnsObj.getqNo() <= customerAnswers.getqNo()) {
			customerContestAnsMap.put(customerAnswers.getCuId(), customerAnswers);	
		} else {
			mapAnsObj.setCuRwds(customerAnswers.getCuRwds());
			mapAnsObj.setCuLife(customerAnswers.getCuLife());
			customerContestAnsMap.put(mapAnsObj.getCuId(), mapAnsObj);
		}*/

		//capture customer contest answer rewards
		/*if(customerAnswers.getaRwds() != null && customerAnswers.getaRwds() > 0) {
			Map<Integer,Double> customerRewardsMap = customerContestAnswerRewardsMap.get(customerAnswers.getContestId());
			if(customerRewardsMap == null) {
				customerRewardsMap = new ConcurrentHashMap<Integer, Double>();
			}
			Double rewards = customerRewardsMap.get(customerAnswers.getCustomerId());
			if(rewards == null) {
				rewards = 0.0;
			}
			rewards = rewards + customerAnswers.getAnswerRewards();
			rewards = TicketUtil.getNormalRoundedValue(rewards);
			customerRewardsMap.put(customerAnswers.getCustomerId(), rewards);
			customerContestAnswerRewardsMap.put(customerAnswers.getContestId(),customerRewardsMap);
		}*/
		
		//Capture each questions correct and wrong answers count
		//Map<Integer,Map<String,Integer>> contestCountMap = questionOptionsCountMap.get(customerAnswers.getContestId());
		//if(contestCountMap == null) {
		//	contestCountMap = new ConcurrentHashMap<Integer, Map<String,Integer>>();
		//}
		//Map<String,Integer> questionCountMap = contestCountMap.get(customerAnswers.getQuestionSNo());
		//if(questionCountMap == null) {
		//	questionCountMap = new ConcurrentHashMap<String, Integer>();
		//}
		//Integer count = questionCountMap.get(customerAnswers.getAnswer());
		//if(count == null) {
		//	count=0;
		//}
		//count++;
		//questionCountMap.put(customerAnswers.getAnswer(), count);
		//contestCountMap.put(customerAnswers.getQuestionSNo(), questionCountMap);
		//questionOptionsCountMap.put(customerAnswers.getContestId(), contestCountMap);
		
		return flag;
	}
	
	public static Double getContestWinnerRewardPoints(Integer contestId) {
		
		List<CassContestWinners> winnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		if(winnersList != null && !winnersList.isEmpty()) {
			return winnersList.get(0).getrPoints();
		}
		
		return null;
	}
	public static Double updateContestWinnersRewadPoints(QuizContest contest) {
		Integer contestId = contest.getId();
		 Map<Integer, CassContestWinners> winnerMap = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersMapByContestId(contestId);
		
		 if(CassContestUtil.SuperFanLevelEnabled) {
			List<SuperFanContCustLevels> sfcontCustLevelsList = new ArrayList<>(sfContCustLevelsMap.values());
			CassContestWinners winnerObj = null;
			Date now = new Date();
			for (SuperFanContCustLevels sfContCustLevels : sfcontCustLevelsList) {
				if(sfContCustLevels.getqNo() >= contest.getNoOfQuestions()) {
					winnerObj = winnerMap.get(sfContCustLevels.getCuId());
					if(winnerObj == null) {
						try {
							winnerObj = new CassContestWinners(contestId, sfContCustLevels.getCuId(), 0, 0.00, now.getTime(),1);
							CassandraDAORegistry.getCassContestWinnersDAO().save(winnerObj);
							winnerMap.put(winnerObj.getCuId(), winnerObj);
						 }catch(Exception e) {
							 e.printStackTrace();
						 }
					}
				}
			}
			 try {
				 //CassandraDAORegistry.getSuperFanContCustLevelsDAO().truncate();
			 }catch(Exception e) {
				 e.printStackTrace();
			 }
		} else {
			 List<CassSuperFanWinner> superFanWinners = CassandraDAORegistry.getCassSuperFanWinnerDAO().getSuperFanWinnersByContestId();
			 if(null != superFanWinners && !superFanWinners.isEmpty()) {
				 Date now = new Date();
				 CassContestWinners contestWinnerObj = null;
				 for (CassSuperFanWinner superFanWinnerObj : superFanWinners) {
					 CassContestWinners winnerTempObject = winnerMap.get(superFanWinnerObj.getCuId());
					 if(null != winnerTempObject) {
						winnerTempObject.setNoOfWinningChance(superFanWinnerObj.getNoOfWinningChance());
						winnerMap.put(superFanWinnerObj.getCuId(), winnerTempObject);
						continue; 
					 }
					 try {
						 contestWinnerObj = new CassContestWinners(contestId, superFanWinnerObj.getCuId(), 0, 0.00, now.getTime(),superFanWinnerObj.getNoOfWinningChance());
						 CassandraDAORegistry.getCassContestWinnersDAO().save(contestWinnerObj);
						 winnerMap.put(superFanWinnerObj.getCuId(), contestWinnerObj);
					 }catch(Exception e) {
						 e.printStackTrace();
					 }
					 
				}
				 try {
					 CassandraDAORegistry.getCassSuperFanWinnerDAO().truncate();
				 }catch(Exception e) {
					 e.printStackTrace();
				 }
			 }
		}
		 
		Double rewardsPerWinner = 0.0;
		
		if(winnerMap != null && !winnerMap.isEmpty() && winnerMap.size() > 0) {
			//QuizContest contest = getCurrentContestByContestId(contestId);
			Integer winnersCount = winnerMap.size();
			Double contestTotalRewards = contest.getTotalRewards();
			
			rewardsPerWinner = Math.floor((contestTotalRewards/winnersCount)*100)/100;
			for (CassContestWinners contestWinner : winnerMap.values()) {
				contestWinner.setrPoints(rewardsPerWinner);
//				contestwinnersMap.put(contestWinner.getCustomerId(), contestWinner);
				
				CassandraDAORegistry.getCassContestWinnersDAO().save(contestWinner);
			}
			
			//QuizDAORegistry.getQuizContestWinnersDAO().saveOrUpdateAll(winnersList);
			contest.setPointsPerWinner(rewardsPerWinner);
			
			//QuizDAORegistry.getQuizContestDAO().update(contest);
			
			System.out.println("REWARD COMPUTE : "+winnersCount+" : "+rewardsPerWinner);
		}
		//log.info( "[updateContestWinnersRewadPoints -  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  );
		return rewardsPerWinner;
	}
	
public static List<CassContestWinners> computeContestGrandWinners(Integer contestId) {
		
		List<CassContestWinners> grandWinnersList = new ArrayList<CassContestWinners>();
		Date start = new Date();
		long process =0,postPros=0,dbUpdate=0,finalPros=0;
		List<CassContestWinners> winnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		if(winnersList != null && winnersList.size() > 0) {
			QuizContest contest = getCurrentContestByContestId(contestId);
			
			Integer winnersCount = winnersList.size();
			Integer grandwinnersCount = contest.getMaxFreeTicketWinners();
			
			if(winnersCount >= grandwinnersCount) {
				
				List<Integer> botCustIds = RTFBotsUtil.getAllBots();
				
				List<CassContestWinners> realWinners = new ArrayList<CassContestWinners>();
				Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
				Map<Integer, CassContestWinners> botsMap = new HashMap<Integer, CassContestWinners>();
				
				boolean considerAllWinnerasGrandWinner = false;
				
				if(null != botCustIds && !botCustIds.isEmpty()) {
					
					for (CassContestWinners winner : winnersList) {
						winnersMap.put(winner.getCuId(), winner);
					}
					for(Integer botCutId: botCustIds) {
						CassContestWinners winnerObj = winnersMap.remove(botCutId);
						if(null != winnerObj) {
							botsMap.put(winnerObj.getCuId(), winnerObj);
							continue;
						}
					}
					
					if(null == winnersMap || winnersMap.isEmpty() || winnersMap.size() <= 0) {
						realWinners.addAll(winnersList);
					}else {
						realWinners.addAll(winnersMap.values());
						winnersCount = winnersMap.size();
						if(winnersCount <= 0) {
							realWinners.addAll(winnersList);
						}else if(winnersCount < grandwinnersCount) {
							considerAllWinnerasGrandWinner = true;
						} 
					}
					
				}else {
					realWinners.addAll(winnersList);
				}
				
				if(considerAllWinnerasGrandWinner) {
					
					List<CassContestWinners> tempWinners = new ArrayList<CassContestWinners>();
					
					int realWinnerCountTemp = 0;
					if(null != realWinners && !realWinners.isEmpty() && realWinners.size() > 0) {
						realWinnerCountTemp = realWinners.size();
					}
					
					boolean considerAllBots = false;
					 
					if(realWinnerCountTemp == 0) {
						 //Consider all bots for winner list
						considerAllBots = true;
					}else{
						tempWinners.addAll(realWinners);
						considerAllBots = false;
					}
					int expectedCount = grandwinnersCount - realWinnerCountTemp;
					
					Set<Integer> botCustIdsIndex = new HashSet<Integer>();
					
					if(!considerAllBots && expectedCount > 0) {
						
						Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
						
						List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
						for (CassContestWinners winnerObj : botsMap.values()) {
							try {
								for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
									finalWinners.add(winnerObj);
								}	
							}catch(Exception e) {
								e.printStackTrace();
							}
						}
						winnersCount = finalWinners.size();
						
						for(int i=0;i<expectedCount;i++) {
							Boolean flag = true;
							while(flag) {
								Random random = new Random();
								int index = random.nextInt(winnersCount);
								if(botCustIdsIndex.add(index)) {
									CassContestWinners contestWinner = finalWinners.get(index);
									if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
										System.out.println("CASSWINNER RANDOM: FINAL WINNER USERIDBOTS->"+contestWinner.getCuId()+" WINNER CHANCES : "+contestWinner.getNoOfWinningChance());
										tempWinners.add(contestWinner);
										flag = false;
									}
									
								}
							}
						}
						
						grandWinnersList.addAll(tempWinners);
						
					} else if(considerAllBots) {
						
						List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
						for (CassContestWinners winnerObj : winnersList) {
							try {
								for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
									finalWinners.add(winnerObj);
								}	
							}catch(Exception e) {
								e.printStackTrace();
							}
						}
						
						winnersCount = finalWinners.size();
						Set<Integer> indexList = new HashSet<Integer>();
						Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
						for(int i=0;i<grandwinnersCount;i++) {
							Boolean flag = true;
							while(flag) {
								Random random = new Random();
								int index = random.nextInt(winnersCount);
								if(indexList.add(index)) {
									CassContestWinners contestWinner = finalWinners.get(index);
									if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
										grandWinnersList.add(contestWinner);
										flag = false;
									}
									
								}
							}
						}
					}else {
						grandWinnersList.addAll(tempWinners);
					}
					
					for (CassContestWinners grandWinner : grandWinnersList) {
						System.out.println("CASSWINNER RANDOM: FINAL WINNER USERID->"+grandWinner.getCuId()+" WINNER CHANCES : "+grandWinner.getNoOfWinningChance());
					}
					
				} else {
					List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
					for (CassContestWinners winnerObj : realWinners) {
						try {
							for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
								finalWinners.add(winnerObj);
							}	
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
					
					winnersCount = finalWinners.size();
					
					Set<Integer> indexList = new HashSet<Integer>();
					Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
					for(int i=0;i<grandwinnersCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(indexList.add(index) ) {
								CassContestWinners contestWinner = finalWinners.get(index);
								if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
									System.out.println("CASSWINNER RANDOM: FINAL WINNER USERID->"+contestWinner.getCuId()+" WINNER CHANCES : "+contestWinner.getNoOfWinningChance());
									grandWinnersList.add(contestWinner);
									flag = false;
								}
							}
						}
					}
				} 
			} else {
				grandWinnersList.addAll(winnersList);
			}
			process = (new Date().getTime()-start.getTime());
			Integer expairyDays = 1;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, expairyDays);
			Date expiryDate = new Date(cal.getTimeInMillis());
			
			List<CassContestGrandWinner> grandWinners = new ArrayList<CassContestGrandWinner>();
			for (CassContestWinners contestWinner : grandWinnersList) {
				contestWinner.setrTix(contest.getFreeTicketsPerWinner());
				
				CassContestGrandWinner grandWinner = new CassContestGrandWinner();
				grandWinner.setCuId(contestWinner.getCuId());
				grandWinner.setCoId(contestWinner.getCoId());
				grandWinner.setrTix(contest.getFreeTicketsPerWinner());
				grandWinner.setStatus("ACTIVE");
				grandWinner.setCrDated(new Date().getTime());
				grandWinner.setExDate(expiryDate.getTime());
				
				grandWinners.add(grandWinner);
			}
			postPros = new Date().getTime()-(start.getTime()+process);
			CassandraDAORegistry.getCassContestWinnersDAO().saveAll(grandWinnersList);
			CassandraDAORegistry.getCassContestGrandWinnerDAO().saveAll(grandWinners);
			//QuizDAORegistry.getQuizContestWinnersDAO().saveOrUpdateAll(grandWinnersList);
			//QuizDAORegistry.getContestGrandWinnerDAO().saveAll(grandWinners);

			dbUpdate = new Date().getTime()-(start.getTime()+process+postPros);
			//contest.setTicketWinnersCount(grandWinnersList.size());
			//QuizDAORegistry.getQuizContestDAO().update(contest);
		}
		
		for (CassContestWinners contestWinner : grandWinnersList) {
			CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
			if(customer != null) {
				contestWinner.setuId(customer.getuId());
				//contestWinner.setCustomerName(customer.getCustomerName());
				//contestWinner.setCustomerLastName(customer.getLastName());
				contestWinner.setCuId(customer.getId());
				contestWinner.setImgP(customer.getImgP());
				
			}
		}
		finalPros = new Date().getTime()-(start.getTime()+process+postPros+dbUpdate);
		log.info("Grand Winner Compute : "+contestId+" :process: "+process+" :postPros: "+postPros+" :dbUpdate: "+dbUpdate+" :final: "+finalPros+" : tot : "+(new Date().getTime()-start.getTime())+" : "+new Date());
//		log.info( "[computeContestGrandWinners - grandWinnersList size  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + grandWinnersList.size() );
		return grandWinnersList;
	}

public static List<CassContestWinners> computeContestJackpotWinners(Integer contestId,Integer questionId,Integer questionNo) throws Exception{
	
	List<CassContestWinners> grandWinnersList = new ArrayList<CassContestWinners>();
	Date start = new Date();
	long process =0,postPros=0,dbUpdate=0,finalPros=0;
	
	//List<CassContestWinners> winnersList = CassandraDAORegistry.getCustContAnswersDAO().getAllCorrectAnsCustomerByContestIdandQuestionId(contestId, questionId);
	
	List<CassContestWinners> winnersList = getQuestionAnsweredList(contestId, questionId,questionNo);
	if(CassContestUtil.SuperFanLevelEnabled) {
		List<SuperFanContCustLevels> sfcontCustLevelsList = new ArrayList<>(sfContCustLevelsMap.values());
		Map<Integer,CassContestWinners> tempWinnerMap = new HashMap<Integer,CassContestWinners>();
		for (CassContestWinners questionWinner : winnersList) {
			tempWinnerMap.put(questionWinner.getCuId(), questionWinner);
		}
		CassContestWinners winnerObj = null;
		Date now = new Date();
		for (SuperFanContCustLevels sfContCustLevels : sfcontCustLevelsList) {
			if(sfContCustLevels.getqNo() >= questionNo) {
				winnerObj = tempWinnerMap.remove(sfContCustLevels.getCuId());
				if(winnerObj == null) {
					winnerObj = new CassContestWinners(contestId, sfContCustLevels.getCuId(), 0, 0.00, now.getTime(),1);
					winnersList.add(winnerObj);
				}
			}
		}
	} else {
		if(null != superFanWinnersMap && !superFanWinnersMap.isEmpty() && superFanWinnersMap.size() > 0 ) {
			Map<Integer,CassContestWinners> superFanWinnersMapTemp = new HashMap<Integer,CassContestWinners>(superFanWinnersMap);
			if(winnersList != null && !winnersList.isEmpty() && winnersList.size() > 0) {
				
				
				for (CassContestWinners questionWinner : winnersList) {
					try {
						CassContestWinners sfWinnerObj = superFanWinnersMapTemp.remove(questionWinner.getCuId());
						if(null != sfWinnerObj ) {
							questionWinner.setNoOfWinningChance(sfWinnerObj.getNoOfWinningChance());
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				 
			}
			
			if(null != superFanWinnersMapTemp && !superFanWinnersMapTemp.isEmpty() && superFanWinnersMapTemp.size() > 0) {
				winnersList.addAll(superFanWinnersMapTemp.values());
			}
		} 
	}
	
	if(winnersList != null && winnersList.size() > 0) {
		System.out.println("MINI JACKPOT LOTTERY : QID: "+questionId+", ANSWEREDCOUNT: "+ winnersList.size());
		QuizContestQuestions question = getQuizContestQuestionById(questionId);
		
		Integer winnersCount = winnersList.size();
		Integer grandwinnersCount = question.getNoOfJackpotWinners();
		
		if(winnersCount >= grandwinnersCount) {
			
			List<Integer> botCustIds = RTFBotsUtil.getAllBotsForJackpot();
			List<CassContestGrandWinner> existingWinners = CassandraDAORegistry.getJackpotWinnerDAO().getJackpotWinners();
			
			if(null != existingWinners && !existingWinners.isEmpty() && existingWinners.size() > 0) {
				for (CassContestGrandWinner obj : existingWinners) {
					botCustIds.add(obj.getCuId());
				}
			}
			
			List<CassContestWinners> realWinners = new ArrayList<CassContestWinners>();
			Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
			Map<Integer, CassContestWinners> botsMap = new HashMap<Integer, CassContestWinners>();
			
			boolean considerAllWinnerasGrandWinner = false;
			
			if(null != botCustIds && !botCustIds.isEmpty()) {
				
				for (CassContestWinners winner : winnersList) {
					winnersMap.put(winner.getCuId(), winner);
				}
				for(Integer botCutId: botCustIds) {
					CassContestWinners winnerObj = winnersMap.remove(botCutId);
					if(null != winnerObj) {
						botsMap.put(winnerObj.getCuId(), winnerObj);
						continue;
					}
				}
				
				if(null == winnersMap || winnersMap.isEmpty() || winnersMap.size() <= 0) {
					realWinners.addAll(winnersList);
				}else {
					realWinners.addAll(winnersMap.values());
					winnersCount = realWinners.size();
					if(winnersCount <= 0) {
						realWinners.addAll(winnersList);
					}else if(winnersCount < grandwinnersCount) {
						considerAllWinnerasGrandWinner = true;
					} 
				}
				
			}else {
				realWinners.addAll(winnersList);
			}
			
			if(considerAllWinnerasGrandWinner) {
				
				List<CassContestWinners> tempWinners = new ArrayList<CassContestWinners>();
				
				int realWinnerCountTemp = 0;
				if(null != realWinners && !realWinners.isEmpty() && realWinners.size() > 0) {
					realWinnerCountTemp = realWinners.size();
				}
				
				boolean considerAllBots = false;
				
				if(realWinnerCountTemp == 0) {
					 //Consider all bots for winner list
					considerAllBots = true;
				}else{
					tempWinners.addAll(realWinners);
					considerAllBots = false;
				}
				int expectedCount = grandwinnersCount - realWinnerCountTemp;
				
				Set<Integer> botCustIdsIndex = new HashSet<Integer>();
				
				if(!considerAllBots && expectedCount > 0) { 
					
					Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
					List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
					for (CassContestWinners winnerObj : botsMap.values()) {
						try {
							for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
								finalWinners.add(winnerObj);
							}	
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
					
					winnersCount = finalWinners.size();
					
					for(int i=0;i<expectedCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(botCustIdsIndex.add(index)) {
								CassContestWinners contestWinner = finalWinners.get(index);
								if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
									System.out.println("MINIJACKPOTWINNER RANDOM: FINAL WINNER USERIDBOTS->"+contestWinner.getCuId()+", Chances: "+contestWinner.getNoOfWinningChance());
									tempWinners.add(contestWinner);
									flag = false;
								}
								
							}
						}
					}
					
					grandWinnersList.addAll(tempWinners);
					
				} else if(considerAllBots) {
					
					Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
					List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
					for (CassContestWinners winnerObj : winnersList) {
						try {
							for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
								finalWinners.add(winnerObj);
							}	
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
					
					winnersCount = finalWinners.size();
					Set<Integer> indexList = new HashSet<Integer>();
					for(int i=0;i<grandwinnersCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(indexList.add(index)) {
								CassContestWinners contestWinner = finalWinners.get(index);
								if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
									grandWinnersList.add(contestWinner);
									flag = false;
								}
								
							}
						}
					}
				}else {
					grandWinnersList.addAll(tempWinners);
				}
				
				for (CassContestWinners grandWinner : grandWinnersList) {
					System.out.println("MINIJACKPOTWINNER RANDOM: FINAL WINNER USERID->"+grandWinner.getCuId()+", Chances: "+grandWinner.getNoOfWinningChance());
				}
				
			} else {

				Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
				List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
				for (CassContestWinners winnerObj : realWinners) {
					try {
						for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
							finalWinners.add(winnerObj);
						}	
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
				
				winnersCount = finalWinners.size();
				
				Set<Integer> indexList = new HashSet<Integer>();
				for(int i=0;i<grandwinnersCount;i++) {
					Boolean flag = true;
					while(flag) {
						Random random = new Random();
						int index = random.nextInt(winnersCount);
						if(indexList.add(index)) {
							CassContestWinners contestWinner = finalWinners.get(index);
							if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
								System.out.println("MINIJACKPOTWINNER RANDOM: FINAL WINNER USERID->"+contestWinner.getCuId());
								grandWinnersList.add(contestWinner);
								flag = false;
							}
							
						}
					}
				}
			} 
		} else {
			grandWinnersList.addAll(winnersList);
		}
		process = (new Date().getTime()-start.getTime());
		Integer expairyDays = 1;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, expairyDays);
		Date expiryDate = new Date(cal.getTimeInMillis());
		
		List<CassContestGrandWinner> grandWinners = new ArrayList<CassContestGrandWinner>();
		for (CassContestWinners contestWinner : grandWinnersList) {
			CassContestGrandWinner grandWinner = new CassContestGrandWinner();
			grandWinner.setCuId(contestWinner.getCuId());
			grandWinner.setCoId(contestWinner.getCoId());
			grandWinner.setqId(questionId);
			grandWinner.setStatus("ACTIVE");
			grandWinner.setJackpotSt("");
			grandWinner.setCrDated(new Date().getTime());
			grandWinners.add(grandWinner);
		}
		postPros = new Date().getTime()-(start.getTime()+process);
		CassandraDAORegistry.getJackpotWinnerDAO().saveAll(grandWinners);
	}
	
	for (CassContestWinners contestWinner : grandWinnersList) {
		CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
		if(customer != null) {
			contestWinner.setuId(customer.getuId());
			contestWinner.setCuId(customer.getId());
			contestWinner.setImgP(customer.getImgP());
		}
	}
	finalPros = new Date().getTime()-(start.getTime()+process+postPros+dbUpdate);
	log.info("Grand Winner Compute : "+contestId+" :process: "+process+" :postPros: "+postPros+" :dbUpdate: "+dbUpdate+" :final: "+finalPros+" : tot : "+(new Date().getTime()-start.getTime())+" : "+new Date());
//	log.info( "[computeContestGrandWinners - grandWinnersList size  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + grandWinnersList.size() );
	return grandWinnersList;
}


public static List<CassContestWinners> computeContestMegaJackpotWinners(QuizContest contest, QuizContestQuestions questionObj) throws Exception {
	
	List<CassContestWinners> grandWinnersList = new ArrayList<CassContestWinners>();
	String jCreditText = "";
	Date start = new Date();
	long process =0,postPros=0,dbUpdate=0,finalPros=0;
	//List<CassContestWinners> winnersList = CassandraDAORegistry.getCustContAnswersDAO().getAllCorrectAnsCustomerByContestIdandQuestionId(contest.getId(), questionObj.getId());
	
	List<CassContestWinners> winnersList = getQuestionAnsweredList(contest.getId(), questionObj.getId(),questionObj.getQuestionSNo());
	if(CassContestUtil.SuperFanLevelEnabled) {
		List<SuperFanContCustLevels> sfcontCustLevelsList = new ArrayList<>(sfContCustLevelsMap.values());
		Map<Integer,CassContestWinners> tempWinnerMap = new HashMap<Integer,CassContestWinners>();
		for (CassContestWinners questionWinner : winnersList) {
			tempWinnerMap.put(questionWinner.getCuId(), questionWinner);
		}
		CassContestWinners winnerObj = null;
		Date now = new Date();
		for (SuperFanContCustLevels sfContCustLevels : sfcontCustLevelsList) {
			if(sfContCustLevels.getqNo() >= questionObj.getQuestionSNo()) {
				winnerObj = tempWinnerMap.remove(sfContCustLevels.getCuId());
				if(winnerObj == null) {
					winnerObj = new CassContestWinners(contest.getId(), sfContCustLevels.getCuId(), 0, 0.00, now.getTime(),1);
					winnersList.add(winnerObj);
				}
			}
		}
	} else {
		if(null != superFanWinnersMap && !superFanWinnersMap.isEmpty() && superFanWinnersMap.size() > 0) {
			Map<Integer,CassContestWinners> superFanWinnersMapTemp = new HashMap<Integer,CassContestWinners>(superFanWinnersMap);
			
			if(winnersList != null && !winnersList.isEmpty() && winnersList.size() > 0) {
				for (CassContestWinners questionWinner : winnersList) {
					try {
						CassContestWinners sfWinnerObj = superFanWinnersMapTemp.remove(questionWinner.getCuId());
						if(null != sfWinnerObj ) {
							questionWinner.setNoOfWinningChance(sfWinnerObj.getNoOfWinningChance());
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			if(null != superFanWinnersMapTemp && !superFanWinnersMapTemp.isEmpty() && superFanWinnersMapTemp.size() > 0) {
				winnersList.addAll(superFanWinnersMapTemp.values());
			}
		}
	}
	 
	 
	if(winnersList != null && !winnersList.isEmpty() && winnersList.size() > 0) {
		
		System.out.println("MEGA JACKPOT LOTTERY : QNO: "+questionObj.getQuestionSNo()+", ANSWEREDCOUNT: "+ winnersList.size());
		
		Integer winnersCount = winnersList.size();
		Integer grandwinnersCount =  questionObj.getNoOfJackpotWinners();;
		
		if(winnersCount >= grandwinnersCount) {
			
			List<Integer> botCustIds = RTFBotsUtil.getAllBotsForJackpot();
			List<CassContestGrandWinner> existingWinners = CassandraDAORegistry.getJackpotWinnerDAO().getJackpotWinners();
			if(null != existingWinners && !existingWinners.isEmpty() && existingWinners.size() > 0) {
				for (CassContestGrandWinner obj : existingWinners) {
					botCustIds.add(obj.getCuId());
				}
			}
			
			List<CassContestWinners> realWinners = new ArrayList<CassContestWinners>();
			Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
			Map<Integer, CassContestWinners> botsMap = new HashMap<Integer, CassContestWinners>();
			
			boolean considerAllWinnerasGrandWinner = false;
			
			if(null != botCustIds && !botCustIds.isEmpty()) {
				
				for (CassContestWinners winner : winnersList) {
					winnersMap.put(winner.getCuId(), winner);
				}
				for(Integer botCutId: botCustIds) {
					CassContestWinners winnerObj = winnersMap.remove(botCutId);
					if(null != winnerObj) {
						botsMap.put(winnerObj.getCuId(), winnerObj);
						continue;
					}
				}
				
				if(null == winnersMap || winnersMap.isEmpty() || winnersMap.size() <= 0) {
					realWinners.addAll(winnersList);
				}else {
					realWinners.addAll(winnersMap.values());
					winnersCount = realWinners.size();
					if(winnersCount <= 0) {
						realWinners.addAll(winnersList);
					}else if(winnersCount < grandwinnersCount) {
						considerAllWinnerasGrandWinner = true;
					} 
				}
				
			}else {
				realWinners.addAll(winnersList);
			}
			
			if(considerAllWinnerasGrandWinner) {
				
				List<CassContestWinners> tempWinners = new ArrayList<CassContestWinners>();
				
				int realWinnerCountTemp = 0;
				if(null != realWinners && !realWinners.isEmpty() && realWinners.size() > 0) {
					realWinnerCountTemp = realWinners.size();
				}
				
				boolean considerAllBots = false;
				
				if(realWinnerCountTemp == 0) {
					 //Consider all bots for winner list
					considerAllBots = true;
				}else{
					tempWinners.addAll(realWinners);
					considerAllBots = false;
				}
				int expectedCount = grandwinnersCount - realWinnerCountTemp;
				
				Set<Integer> botCustIdsIndex = new HashSet<Integer>();
				
				if(!considerAllBots && expectedCount > 0) {
					
					Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
					List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
					for (CassContestWinners winnerObj : botsMap.values()) {
						try {
							for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
								finalWinners.add(winnerObj);
							}	
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
					winnersCount = finalWinners.size();
					
					for(int i=0;i<expectedCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(botCustIdsIndex.add(index)) {
								CassContestWinners contestWinner = finalWinners.get(index);
								if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
									System.out.println("MEGAJACKPOTWINNER RANDOM: FINAL WINNER USERIDBOTS->"+contestWinner.getCuId()+",  Chances : "+contestWinner.getNoOfWinningChance());
									tempWinners.add(contestWinner);
									flag = false;
								}
								
							}
						}
					}
					
					grandWinnersList.addAll(tempWinners);
					
				} else if(considerAllBots) {
					Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
					List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
					for (CassContestWinners winnerObj : winnersList) {
						try {
							for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
								finalWinners.add(winnerObj);
							}	
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
					
					winnersCount = finalWinners.size();
					Set<Integer> indexList = new HashSet<Integer>();
					for(int i=0;i<grandwinnersCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(indexList.add(index)) {
								CassContestWinners contestWinner = finalWinners.get(index);
								if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
									grandWinnersList.add(contestWinner);
									flag = false;
								}
								
							}
						}
					}
				}else {
					grandWinnersList.addAll(tempWinners);
				}
				
				for (CassContestWinners grandWinner : grandWinnersList) {
					System.out.println("MEGAJACKPOTWINNER RANDOM: FINAL WINNER USERID->"+grandWinner.getCuId()+",  Chances : "+grandWinner.getNoOfWinningChance());
				}
				
			} else {
				
				Set<Integer> selectedGrandWinnerIds = new HashSet<Integer>();
				List<CassContestWinners> finalWinners = new ArrayList<CassContestWinners>();
				for (CassContestWinners winnerObj : realWinners) {
					try {
						for(int i =1; i <= winnerObj.getNoOfWinningChance(); i++) {
							finalWinners.add(winnerObj);
						}	
					}catch(Exception e) {
						e.printStackTrace();
					}
				} 
				
				winnersCount = finalWinners.size();
				
				Set<Integer> indexList = new HashSet<Integer>();
				for(int i=0;i<grandwinnersCount;i++) {
					Boolean flag = true;
					while(flag) {
						Random random = new Random();
						int index = random.nextInt(winnersCount);
						if(indexList.add(index)) {
							CassContestWinners contestWinner = finalWinners.get(index);
							if(selectedGrandWinnerIds.add(contestWinner.getCuId())) {
								System.out.println("MEGAJACKPOTWINNER RANDOM: FINAL WINNER USERID->"+contestWinner.getCuId()+", Chances : "+contestWinner.getNoOfWinningChance());
								grandWinnersList.add(contestWinner);
								flag = false;
							}
							
						}
					}
				}
			} 
		} else {
			grandWinnersList.addAll(winnersList);
		}
		process = (new Date().getTime()-start.getTime());
		Integer expairyDays = 1;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, expairyDays);
		Date expiryDate = new Date(cal.getTimeInMillis());
		
		jCreditText = getMegaJackpotWinnerCreditText(questionObj);
		
		List<CassContestGrandWinner> grandWinners = new ArrayList<CassContestGrandWinner>();
		for (CassContestWinners contestWinner : grandWinnersList) {
			CassContestGrandWinner grandWinner = new CassContestGrandWinner();
			grandWinner.setCuId(contestWinner.getCuId());
			grandWinner.setCoId(contestWinner.getCoId());
			grandWinner.setqId(questionObj.getId());
			grandWinner.setStatus("ACTIVE");
			grandWinner.setJackpotSt("Q"+questionObj.getQuestionSNo()+" WINNER");
			grandWinner.setjCreditSt(jCreditText);
			grandWinner.setCrDated(new Date().getTime());
			grandWinners.add(grandWinner);
		}
		postPros = new Date().getTime()-(start.getTime()+process);
		CassandraDAORegistry.getJackpotWinnerDAO().saveAll(grandWinners);
	}
	
	for (CassContestWinners contestWinner : grandWinnersList) {
		CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
		if(customer != null) {
			contestWinner.setuId(customer.getuId());
			contestWinner.setCuId(customer.getId());
			contestWinner.setImgP(customer.getImgP());
		}
		contestWinner.setrPointsSt("Q"+questionObj.getQuestionSNo()+" WINNER");
		contestWinner.setjCreditSt(jCreditText);
		contestWinner.setrPoints(0.00);
		contestWinner.setrTix(0);
	}
	finalPros = new Date().getTime()-(start.getTime()+process+postPros+dbUpdate);
	log.info("Grand Winner Compute : "+contest.getId()+" :process: "+process+" :postPros: "+postPros+" :dbUpdate: "+dbUpdate+" :final: "+finalPros+" : tot : "+(new Date().getTime()-start.getTime())+" : "+new Date());
//	log.info( "[computeContestGrandWinners - grandWinnersList size  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + grandWinnersList.size() );
	return grandWinnersList;
}

	public static String getMegaJackpotWinnerCreditText(QuizContestQuestions questionObj ) {
		
		String text = "";
		
		switch (questionObj.getjCreditType()) {
		
			case NONE:
				
				break;
				
			case GIFTCARD:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" GIFT CARDS";
				break;
				
			case LIFELINE:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" LIFELINE";
				break;
				
			case REWARD:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" REWARDS";
				break;
				
			case SUPERFANSTAR:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" SUPERFAN STAR";
				break;
				
			case TICKET:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" TICKETS";
				break;
				
			case POINTS:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" POINTS";
				break;
				
			default:
				break;
		}
		
		return text;
		
	}

	public static List<CassContestWinners> getContestGrandWinners(Integer contestId) {
	
		/*List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestWinners> list =   CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		for (CassContestWinners contestWinner : list) {
			if(contestWinner.getrTix() > 0) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
				contestWinner.setuId(customer.getuId());
				//contestWinner.setCustomerName(customer.getCustomerName());
				//contestWinner.setCustomerLastName(customer.getLastName());
				contestWinner.setCuId(customer.getId());
				contestWinner.setImgP(customer.getImgP());
			
				winners.add(contestWinner);
			}
		}
		return winners;*/
		
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getCassContestGrandWinnerDAO().getContestGrandWinnersByContestId(contestId);
		for (CassContestGrandWinner grandWinner : list) {
			//if(grandWinner.getrTix() > 0) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setrPoints(0.0);
				winner.setrTix(grandWinner.getrTix());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
			
				winners.add(winner);
			//}
		}
		return winners;
	}
	
	public static List<CassContestWinners> getContestSummaryData(Integer contestId)  {
		try {
			List<CassContestWinners> contestWinners = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
			if(contestWinners != null && !contestWinners.isEmpty()) {
				for (CassContestWinners winners : contestWinners) {
					//CassCustomer customer = CassCustomerUtil.getCustomerById(winners.getCuId());
					CassCustomer customer = CassContestUtil.getCasscustomerFromMap(winners.getCuId());
					if(customer != null) {
						winners.setCuId(customer.getId());
						winners.setuId(customer.getuId());
						winners.setImgP(customer.getImgP());
					}
				}
			}
			//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
			return contestWinners;
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<CassContestWinners> getContestMiniJackpotGrandWinners(Integer contestId, Integer questionId) {
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getJackpotWinnerDAO().getJackpotWinnersByContestId(contestId,questionId);
		for (CassContestGrandWinner grandWinner : list) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setqId(grandWinner.getqId());
				//winner.setJackpotSt(grandWinner.getJackpotSt());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
				winners.add(winner);
		}
		return winners;
	}
	
	public static List<CassContestWinners> getContestMegaJackpotGrandWinners(Integer contestId) {
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getJackpotWinnerDAO().getMegaJackpotWinnersByContestId(contestId);
		for (CassContestGrandWinner grandWinner : list) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setqId(grandWinner.getqId());
				winner.setJackpotSt(grandWinner.getJackpotSt());
				winner.setrPointsSt(grandWinner.getJackpotSt());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
				winner.setrPoints(0.00);
				winner.setjCreditSt(grandWinner.getjCreditSt());
				winner.setrTix(0);
				winners.add(winner);
		}
		return winners;
	}
	
	
	public static List<CassContestWinners> getContestMegaJackpotGrandWinners(Integer contestId,Integer questionId) {
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getJackpotWinnerDAO().getJackpotWinnersByContestId(contestId,questionId);
		for (CassContestGrandWinner grandWinner : list) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setqId(grandWinner.getqId());
				winner.setJackpotSt(grandWinner.getJackpotSt());
				winner.setrPointsSt(grandWinner.getJackpotSt());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
				winner.setrPoints(0.00);
				winner.setjCreditSt(grandWinner.getjCreditSt());
				winner.setrTix(0);
				winners.add(winner);
		}
		return winners;
	}
	
	
	public static ContSummaryInfo getContestSummaryDataWithLimit(Integer contestId,ContSummaryInfo contestSummary)  {
		try {
			List<CassContestWinners> contestWinners = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
			if(contestWinners != null && !contestWinners.isEmpty()) {
				
				contestSummary.setwCount(contestWinners.size());
				List<CassContestWinners> finalList = new ArrayList<CassContestWinners>();
				int count = 0;
				for (CassContestWinners cassContestWinners : contestWinners) {
					if(count>=100) {
						break;
					}
					CassCustomer customer = CassContestUtil.getCasscustomerFromMap(cassContestWinners.getCuId());
					if(customer != null) {
						cassContestWinners.setCuId(customer.getId());
						cassContestWinners.setuId(customer.getuId());
						cassContestWinners.setImgP(customer.getImgP());
					}
					finalList.add(cassContestWinners);
					count++;
				}
				contestSummary.setWinners(finalList);
				
			}
			//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return contestSummary;
	}
	
public static LiveCustomerListInfo getLiveCustomerDetailsInfoFromCache(Integer contestId,LiveCustomerListInfo liveCustListInfo) throws Exception {
		
		Integer totalCount=0;
		Boolean hasMoreCustomers = false;
		List<LiveCustDetails> liveCustomersList = getLiveCustomersList(contestId);
		
		if(liveCustomersList != null) {
			totalCount = liveCustomersList.size();
		}
		//MaxPerPage - Only for Mamta - Added on 11/21/20118 07:27 AM by Ulaganathan
		liveCustListInfo.setMaxPerPage(totalCount);
		liveCustListInfo.settCount(totalCount);
		liveCustListInfo.sethMore(hasMoreCustomers);
		liveCustListInfo.setCustList(liveCustomersList);
		
		return liveCustListInfo;
	}
	
//Phase 2 Commented
/*public static LiveCustomerListInfo getLiveCustomerDetailsInfo(Integer contestId,LiveCustomerListInfo liveCustListInfo,Integer pageNo,Integer maxRows) throws Exception {
		
		Integer totalCount=0;
		Boolean hasMoreCustomers = false;
		List<LiveCustDetails> customersList = null;
		
		List<Integer> customerIdList = null;
		List<Integer> contestCountList = customersCountMap.get(contestId);
		if(contestCountList != null) {
			totalCount = contestCountList.size();
			Integer fromIndex = maxRows*(pageNo - 1);
			Integer toIndex = maxRows*pageNo;
			if(totalCount > fromIndex) {
				if(totalCount <= toIndex) {
					toIndex = totalCount;
				} else {
					hasMoreCustomers = true;
				}
				customerIdList = new ArrayList<Integer>(contestCountList.subList(fromIndex, toIndex));
			}
			if(customerIdList != null) {
				LiveCustDetails customerDtl = null;
				customersList = new ArrayList<LiveCustDetails>();
				for (Integer customerId : customerIdList) {
					CassCustomer customer = liveCustoemrDtlMap.get(customerId);
					if(customer == null) {
						customer = CassContestUtil.getCasscustomerFromMap(customerId);
						//customer = CassCustomerUtil.getCustomerById(customerId);						
					}
					if(customer != null) {
						customerDtl = new LiveCustDetails(customer.getId(),customer.getuId(),customer.getImgP());
						customersList.add(customerDtl);
					}
					
				}
			}
		}
		//MaxPerPage - Only for Mamta - Added on 11/21/20118 07:27 AM by Ulaganathan
		liveCustListInfo.setMaxPerPage(totalCount);
		liveCustListInfo.settCount(totalCount);
		liveCustListInfo.sethMore(hasMoreCustomers);
		liveCustListInfo.setCustList(customersList);
		
		return liveCustListInfo;
	}*/
	
public static List<CassContestWinners> getQuestionAnsweredList(Integer contestId,Integer questionId,Integer questionNo) throws Exception {
	
	List<CassContestWinners> answeredList = new ArrayList<CassContestWinners>();
	
	//List<CustContAnswers> custansList = new ArrayList<CustContAnswers>(customerContestAnsMap.values());
	List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContIdandQuestionNo(contestId, questionNo);
	CassContestWinners contestWinners=null;
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqNo().equals(questionNo) && null != cans.getIsCrt() && cans.getIsCrt()) {
				contestWinners= new CassContestWinners();
				contestWinners.setCoId(contestId);
				contestWinners.setCuId(cans.getCuId());
				contestWinners.setqId(questionId);;
				answeredList.add(contestWinners);
			}
		}
	}
	return answeredList;
}

public static AnswerCountInfo getQuestAnsCount(Integer contestId,Integer questionId,Integer questionNo,AnswerCountInfo ansCountInfo,String isCustListReq) throws Exception {
	
	Integer aCount=0,bCount=0,cCount=0;
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByQuestId(questionId);
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByContestIdandQuestId(contestId,questionId);
	//List<CustContAnswers> custansList = new ArrayList<CustContAnswers>(customerContestAnsMap.values());	
	List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContIdandQuestionNo(contestId, questionNo);
	
	Set<CustContAnswers> questCorrectAnsList = new HashSet<CustContAnswers>(); 
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqNo().equals(questionNo) && cans.getAns() != null) {
				if(cans.getAns().equalsIgnoreCase("A")) {
					aCount = aCount + 1;
				} else if(cans.getAns().equalsIgnoreCase("B")) {
					bCount = bCount + 1;
				} else if(cans.getAns().equalsIgnoreCase("C")) {
					cCount = cCount + 1;
				}
			}
			if(cans.getIsCrt()) {
				questCorrectAnsList.add(cans);
			}
		}
		if(isCustListReq != null && isCustListReq.equals("Y")) {
			Integer randomCount = 10;
			Integer cuCount = 0;
			List<LiveCustDetails> custList = new ArrayList<LiveCustDetails>();
			for (CustContAnswers cans : questCorrectAnsList) {
				if(cans.getqNo().equals(questionNo) && cans.getIsCrt()) {
					CassCustomer customer = CassContestUtil.getCasscustomerFromMap(cans.getCuId());
					if(customer != null) {
						LiveCustDetails custDtl = new LiveCustDetails();
						custDtl.setCuId(customer.getId());
						custDtl.setuId(customer.getuId());
						custList.add(custDtl);
						cuCount++;
						
						if(cuCount >= randomCount) {
							break;
						}
					}
				}
			}
			ansCountInfo.setCustList(custList);	
		}
	}
	ansCountInfo.setaCount(aCount);
	ansCountInfo.setbCount(bCount);
	ansCountInfo.setcCount(cCount);
	
	return ansCountInfo;
}
/*public static List<LiveCustDetails> getQuestionAnsweredRandomcustomersByquestionNo(Integer contestId,Integer questionId,Integer questionNo) throws Exception {
	
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByQuestId(questionId);
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByContestIdandQuestId(contestId,questionId);
	//Set<CustContAnswers> custansList = new HashSet<CustContAnswers>(customerContestAnsMap.values());
	
//Fix This
	List<CustContAnswers> custansListFromDb = CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContIdandQuestionNo(contestId, questionNo);
	Set<CustContAnswers> custansList = new HashSet<CustContAnswers>(custansListFromDb);
	
	Integer randomCount = 10;
	Integer cuCount = 0;
	List<LiveCustDetails> custList = new ArrayList<LiveCustDetails>();
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqNo().equals(questionNo) && cans.getIsCrt()) {
				CassCustomer customer = CassContestUtil.getCasscustomerFromMap(cans.getCuId());
				if(customer != null) {
					LiveCustDetails custDtl = new LiveCustDetails();
					custDtl.setCuId(customer.getId());
					custDtl.setuId(customer.getuId());
					custList.add(custDtl);
					cuCount++;
					
					if(cuCount >= randomCount) {
						return custList;
					}
				}
			}
		}
	}
	return custList;
}*/

public static Integer getLifelineUSedCustomersCountByQuestionNo(Integer contestId,Integer questionNo) throws Exception {
	
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByQuestId(questionId);
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByContestIdandQuestId(contestId,questionId);
//	List<CustContAnswers> custansList = new ArrayList<CustContAnswers>(customerContestAnsMap.values());
	
//Fix This
	List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContIdandQuestionNo(contestId, questionNo);
	
	Integer totCount = 0;
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqNo().equals(questionNo) && cans.getIsLife()) {
				totCount++;
			}
		}
	}
	return totCount;
}

//Phase 2 Commented
/*public static CustContAnswers computeAutoCreateAnswers(Integer customerId,Integer contestId,Integer startQno,Integer endQno,String ansResMsg,
		CustContAnswers lastAnswer) throws Exception {
	
	try {
		boolean isAutoFlag = true;
		if(isAutoFlag) {
			for(int qNo=startQno; qNo<=endQno && endQno>0; qNo++) {
				
				CustContAnswers autoQuestionAns = CassContestUtil.getCustomerQuestionAnswers(customerId,qNo, contestId);
				if(autoQuestionAns != null) {
					ansResMsg += "AUTO:Answer Created:"+qNo+".";
				} else {

					QuizContestQuestions autoQuestion = CassContestUtil.getQuizContestQuestionByContestIdandQuestionSlNo(contestId, qNo);
					if(autoQuestion == null) {
						ansResMsg += "AUTO:Quest Not Exist:"+qNo+".";
						continue;
					}
					
					Double cumulativeRewards = 0.0;
					Integer cumulativeLifeLineUsed = 0;
					if(lastAnswer != null) {
						if(lastAnswer.getCuRwds() != null) {
							cumulativeRewards = lastAnswer.getCuRwds();
						}
						if(lastAnswer.getCuLife() != null) {
							cumulativeLifeLineUsed = lastAnswer.getCuLife();
						}
					}
					
					CustContAnswers custContAns = new CustContAnswers();
					custContAns.setCuId(customerId);
					custContAns.setCoId(contestId);
					custContAns.setqId(autoQuestion.getId());
					custContAns.setqNo(qNo);
					custContAns.setAns(null);
					custContAns.setIsCrt(Boolean.TRUE);
					custContAns.setCrDate(new Date().getTime());
					custContAns.setIsLife(false);
					custContAns.setFbCallbackTime(null);
					custContAns.setAnswerTime(null);
					custContAns.setRetryCount(0);
					custContAns.setIsAutoCreated(Boolean.TRUE);
				
					custContAns.setaRwds(autoQuestion.getQuestionRewards());
					if(autoQuestion.getQuestionRewards() != null) {
						cumulativeRewards = cumulativeRewards + autoQuestion.getQuestionRewards();	
					}
					custContAns.setCuRwds(cumulativeRewards);
					custContAns.setCuLife(cumulativeLifeLineUsed);
					
					CassandraDAORegistry.getCustContAnswersDAO().save(custContAns);
					CassContestUtil.updateCustomerAnswers(custContAns);
					
					lastAnswer = custContAns;
					
				}
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		//System.out.println("Error Occured in AUTO ANs Creation Block:"+customerIdStr+":"+contestIdStr+":"+questionNoStr+":"+new Date());
		throw e;
	}
	return lastAnswer;
}*/
public static CustContDtls computeAutoCreateAnswers(Integer customerId,Integer contestId,Integer startQno,Integer endQno,String ansResMsg,
		CustContDtls custContDtls) throws Exception {
	
	try {
		boolean isAutoFlag = true;
		if(isAutoFlag) {
			for(int qNo=startQno; qNo<=endQno && endQno>0; qNo++) {
				
				//CustContAnswers autoQuestionAns = CassContestUtil.getCustomerQuestionAnswers(customerId,qNo, contestId);
				//if(autoQuestionAns != null) {
				//	ansResMsg += "AUTO:Answer Created:"+qNo+".";
				//} else {

					QuizContestQuestions autoQuestion = CassContestUtil.getQuizContestQuestionByContestIdandQuestionSlNo(contestId, qNo);
					if(autoQuestion == null) {
						ansResMsg += "AUTO:Quest Not Exist:"+qNo+".";
						continue;
					}
					
					Double cumulativeRewards = 0.0;
					Integer cumulativeLifeLineUsed = 0;
					if(custContDtls != null) {
						if(custContDtls.getCuRwds() != null) {
							cumulativeRewards = custContDtls.getCuRwds();
						}
						if(custContDtls.getCuLife() != null) {
							cumulativeLifeLineUsed = custContDtls.getCuLife();
						}
					}
					
					/*if(answerOption.equalsIgnoreCase(quizQuestion.getAnswer())) {
						//quizAnswerInfo.setIsCrt(Boolean.TRUE);
						
						if(contest.getNoOfQuestions().equals(questionNo)) {
							CassContestUtil.updateContestWinners(contest.getId(), customerId);
						}
					}*/
					
					CustContAnswers custContAns = new CustContAnswers();
					custContAns.setCuId(customerId);
					custContAns.setCoId(contestId);
					custContAns.setqId(autoQuestion.getId());
					custContAns.setqNo(qNo);
					custContAns.setAns(null);
					custContAns.setIsCrt(Boolean.TRUE);
					custContAns.setCrDate(new Date().getTime());
					custContAns.setIsLife(false);
					custContAns.setFbCallbackTime(null);
					custContAns.setAnswerTime(null);
					custContAns.setRetryCount(0);
					custContAns.setIsAutoCreated(Boolean.TRUE);
				
					custContAns.setaRwds(autoQuestion.getQuestionRewards());
					if(autoQuestion.getQuestionRewards() != null) {
						cumulativeRewards = cumulativeRewards + autoQuestion.getQuestionRewards();	
					}
					custContAns.setCuRwds(cumulativeRewards);
					custContAns.setCuLife(cumulativeLifeLineUsed);
					
					CassandraDAORegistry.getCustContAnswersDAO().save(custContAns);
					CassContestUtil.updateCustomerAnswers(custContAns);

//Fix This			
					if(custContDtls == null) {
						custContDtls = new CustContDtls();
						custContDtls.setCoId(custContAns.getCoId());
						custContDtls.setCuId(custContAns.getCuId());
						
					} 
					if(custContDtls.getLqNo() == null || custContDtls.getLqNo() <= custContAns.getqNo()) {
						custContDtls.setLqNo(custContAns.getqNo());
						custContDtls.setIsLqCrt(custContAns.getIsCrt());
						custContDtls.setIsLqLife(custContAns.getIsLife());	
					}
					custContDtls.setCuRwds(custContAns.getCuRwds());
					custContDtls.setCuLife(custContAns.getCuLife());
					CassandraDAORegistry.getCustContDtlsDAO().save(custContDtls);
					//lastAnswer = custContAns;
					
				//}
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		//System.out.println("Error Occured in AUTO ANs Creation Block:"+customerIdStr+":"+contestIdStr+":"+questionNoStr+":"+new Date());
		throw e;
	}
	return custContDtls;
}
public static boolean refreshHallOfFameForThisWeekData()  {
	try { 
		//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize, null, null, null);
		List<HallOfFameDtls> winnersList = SQLDaoUtil.getHallOfFameByDateFromView(hallOfFamePageMaxDataSize);
		if(winnersList != null) {
			thisWeekHallOfFameList = new ArrayList<HallOfFameDtls>(winnersList);
		}
		
		
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
	return true;
}
public static boolean refreshHallOfFameTillDateData()  {
	
	 
	try {
		//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDate(contestSummaryDataSize);
		//List<HallOfFameDtls> winnersList = SQLDaoUtil.getHallOfFameByTillDateFromView(hallOfFamePageMaxDataSize);
		List<HallOfFameDtls> winnersList = SQLDaoUtil.getRtfLivtHallOfFameByTillDateFromView(hallOfFamePageMaxDataSize);
		if(winnersList != null) {
			tillDateHallOfFameList = new ArrayList<HallOfFameDtls>(winnersList);
		}
		
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
	return true;
}

public static List<HallOfFameDtls> getHallOfFameForThisWeekData()  {
	try {
		if(thisWeekHallOfFameList == null || thisWeekHallOfFameList.isEmpty()) {
			refreshHallOfFameForThisWeekData();
		}
		//return  new ArrayList<QuizContestWinners>(thisWeekSummaryList);
		return thisWeekHallOfFameList;
		
	}catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}
public static List<HallOfFameDtls> getHallOfFameForTillDateData()  {
	try {
		if(tillDateHallOfFameList == null || tillDateHallOfFameList.isEmpty()) {
			refreshHallOfFameTillDateData();
		}
		//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
		return tillDateHallOfFameList;
		
	}catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}

public static Boolean updateContestRewardsToCassCustomer(Integer contestId) throws Exception {
	
	/*Map<Integer,CustContAnswers> customerAnswersMap = new HashMap<Integer,CustContAnswers>(); 
	List<CustContAnswers> custAnsList =  CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContId(contestId);
	for (CustContAnswers custContAnswers : custAnsList) {
		CustContAnswers tempCustAns = customerAnswersMap.get(custContAnswers.getCuId());
		if(tempCustAns == null || tempCustAns.getqNo() < custContAnswers.getqNo()) {
			customerAnswersMap.put(custContAnswers.getCuId(),custContAnswers);
		}
	}
	
	List<CassContestWinners> winnersList =  CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
	Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
	if(winnersList != null) {
		for (CassContestWinners winner : winnersList) {
			winnersMap.put(winner.getCuId(), winner);
		}
	}
	
	if(customerAnswersMap != null) {
		List<Integer> keyList = new ArrayList<>(customerAnswersMap.keySet());
		for (Integer custId : keyList) {
			Double rewards = 0.0;
			Integer livesUsed = 0;
			CustContAnswers custAns = customerAnswersMap.remove(custId);
			rewards = custAns.getCuRwds();
			livesUsed = custAns.getCuLife();
		
			CassContestWinners winner = winnersMap.remove(custId);
			if(winner != null) {
				rewards = rewards + winner.getrPoints();
			}
			if(rewards > 0 || livesUsed > 0) {
				CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
				
				rewards = rewards + customer.getcRewardDbl();
				Integer quizLives = customer.getqLive() - livesUsed;
				
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), quizLives, rewards);
			}
		}
	}
	if(!winnersMap.isEmpty()) {
		List<Integer> keyList = new ArrayList<>(winnersMap.keySet());
		for (Integer custId : keyList) {
			Double rewards = 0.0;
			CassContestWinners winner = winnersMap.remove(custId);
			rewards = rewards + winner.getrPoints();
			if(rewards > 0) {
				CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
				rewards = rewards + customer.getcRewardDbl();
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(customer.getId(), rewards);
				//CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), quizLives, rewards);
			}
		}
	}*/

//Fix This
	/*List<CassContestWinners> winnersList =  CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId); 
	if(customerContestAnsMap != null) {
		Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
		if(winnersList != null) {
			for (CassContestWinners winner : winnersList) {
				winnersMap.put(winner.getCuId(), winner);
			}
		}
		
		List<Integer> keyList = new ArrayList<>(customerContestAnsMap.keySet());
		for (Integer custId : keyList) {
			Double rewards = 0.0;
			Integer livesUsed = 0;
			CustContAnswers custAns = customerContestAnsMap.remove(custId);
			rewards = custAns.getCuRwds();
			livesUsed = custAns.getCuLife();
		
			CassContestWinners winner = winnersMap.remove(custId);
			if(winner != null) {
				rewards = rewards + winner.getrPoints();
			}
			if(rewards > 0 || livesUsed > 0) {
				CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
				
				rewards = rewards + customer.getcRewardDbl();
				Integer quizLives = customer.getqLive() - livesUsed;
				
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), quizLives, rewards);
			}
		}
	}*/
	return true;
}
/*public static Boolean updateCustomerContestDetailsinCassandra(Integer contestId) throws Exception {

	QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
	if(!contest.getIsCustomerStatsUpdated()) {
		Date start = new Date();
		Date startOne = new Date();
		
		startOne = new Date();
		updateContestRewardsToCassCustomer(contestId);
		log.info("Time to Update CONT REWARDS to Customer : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		try {
			startOne = new Date();
		//QuizContestUtil.forceSummaryRefreshTable();
		//QuizContestUtil.refreshSummaryDataTable();
		//QuizContestUtil.refreshSummaryDataCache();
		//QuizContestUtil.forceManualefreshSummaryDataTable();
		log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("Time to Update ALL CONTEST STATS : "+(new Date().getTime()-start.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		contest.setIsCustomerStatsUpdated(true);
		QuizDAORegistry.getQuizContestDAO().update(contest);
		return true;
	}
	return false;
}*/
/*
public static Boolean updateCustomerPostContestDetailsinSQL(Integer contestId) throws Exception {

	//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
	//if(!contest.getIsCustomerStatsUpdated()) {
		Date fromDate = new Date();
		Date start = new Date();
		Date startOne = new Date();
		QuizDAORegistry.getQuizQueryManagerDAO().updateAllCustomersContestDataByContestId(contestId);
		//log.info("Time to Update Cust COntest Data : " + "[ " +  new QuizContestUtil().getIPAddress() + " ] " +(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		startOne = new Date();
		QuizDAORegistry.getQuizQueryManagerDAO().updateAllCustomersContestWinsDataByContestId(contestId);
		log.info("Time to Update Cust COntest Wins Data : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		Commented - this funtion commented in mobile screen itself. not in use 
		//startOne = new Date();
		//QuizCustomerPromocodeandContestOrderStatsScheduler.processCustomerPromoCodeAndContestOrderStats();
		//log.info("Time to Update Cust PRomo and co stats Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		startOne = new Date();
		CustomerUtil.updateCustomerUtilForContestCustomers(contestId, fromDate);
		log.info("Time to Update CUST UTIL for STATS : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		try {
			startOne = new Date();
		//QuizContestUtil.forceSummaryRefreshTable();
		//QuizContestUtil.refreshSummaryDataTable();
		//QuizContestUtil.refreshSummaryDataCache();
		QuizContestUtil.forceManualefreshSummaryDataTable();
		log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("Time to Update ALL CONTEST STATS : "+(new Date().getTime()-start.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		//contest.setIsCustomerStatsUpdated(true);
		//QuizDAORegistry.getQuizContestDAO().update(contest);
		return true;
	//}
	//return false;
}*/


public static Boolean updateContestCurrentQuestions(QuizContestQuestions contestQuestion) throws Exception {
	
	contestCurrentQuestionsMap.put(contestQuestion.getContestId(), contestQuestion);
	return true;
}
public static QuizContestQuestions getContestCurrentQuestions(Integer contestId) throws Exception {
	QuizContestQuestions questions = contestCurrentQuestionsMap.get(contestId);
	return questions;
}

public static void truncateCassandraContestData(String contestIdStr) throws Exception {
	
	try {
		/*Integer contestId = Integer.parseInt(contestIdStr.trim());
		
		List<ContestMigrationStats> migrationStats = DAORegistry.getContestMigrationStatsDAO().getAllStatsByContestId(contestId);
		
		if(null == migrationStats) {
			return;
		}
		
		boolean isCompleted = false;
		for (ContestMigrationStats obj : migrationStats) {
			if(obj.getStatus().equals("COMPLETED")) {
				isCompleted = true;
				break;
			}
		}
		if(isCompleted) {
			CassandraDAORegistry.getCustContAnswersDAO().truncate();
			CassandraDAORegistry.getCassContestGrandWinnerDAO().truncate();
			CassandraDAORegistry.getCassContestWinnersDAO().truncate();
			//CassandraDAORegistry.getContestParticipantsDAO().truncate();
		}*/
		
		CassandraDAORegistry.getCustContAnswersDAO().truncate();
		CassandraDAORegistry.getCassContestGrandWinnerDAO().truncate();
		CassandraDAORegistry.getCassContestWinnersDAO().truncate();
		CassandraDAORegistry.getJackpotWinnerDAO().truncate();
		
		CassandraDAORegistry.getCustContDtlsDAO().truncate();
		CassandraDAORegistry.getContestParticipantsDAO().truncate();
		CassandraDAORegistry.getContestPasswordAuthDAO().truncate();
		
	}catch(Exception e){
		throw e;
	}
//Fix This	
	CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
	
	/*MigrateApiTrackingUtil trackingUtil = new MigrateApiTrackingUtil();
	Thread t = new Thread(trackingUtil);
	t.start();*/
	
}

//Prod - 470000 to 626700
//Sandbox - 136422 to 206423
public static Integer startCustomerId = 470000;
public static Integer endCustomerId = 626700;
public static Integer currentCustomerId = startCustomerId;

public static Integer getRandomCustomerIdForTest() {
	if(currentCustomerId >= endCustomerId) {
		currentCustomerId = startCustomerId;
	}
	currentCustomerId +=1;
	return currentCustomerId;
}


}
