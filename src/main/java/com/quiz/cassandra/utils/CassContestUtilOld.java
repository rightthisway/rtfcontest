package com.quiz.cassandra.utils;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassContestGrandWinner;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CassSuperFanWinner;
import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.list.AnswerCountInfo;
import com.quiz.cassandra.list.CassJoinContestInfo;
import com.quiz.cassandra.list.ContSummaryInfo;
import com.quiz.cassandra.list.HallOfFameDtls;
import com.quiz.cassandra.list.LiveCustDetails;
import com.quiz.cassandra.list.LiveCustomerListInfo;
import com.quiz.cassandra.thread.MegaJackpotThread;
import com.zonesws.webservices.data.QuizContest;
import com.zonesws.webservices.data.QuizContestQuestions;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.RTFBotsUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * 
 * @author Tamil
 *
 */
public class CassContestUtilOld {	
	
	
	static QuizContest startedContest = null;
	static  Map<String,CustContAnswers> customerQuestionAnsMap = new ConcurrentHashMap<String,CustContAnswers>();
	public static Map<Integer,CassCustomer> cassCustomerMap = new ConcurrentHashMap<Integer, CassCustomer>();
	static Map<Integer,QuizContestQuestions> contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
	static  Map<Integer,CustContAnswers> customerContestAnsMap = new ConcurrentHashMap<Integer,CustContAnswers>();
	static Map<Integer,List<Integer>> customersCountMap = new ConcurrentHashMap<Integer, List<Integer>>();
	static Map<Integer,CassCustomer> liveCustoemrDtlMap = new ConcurrentHashMap<Integer, CassCustomer>();
	//static List<CassContestWinners> contestSummaryList = new ArrayList<CassContestWinners>();
	static Map<Integer,ContestParticipants> contParticipantsMap = new ConcurrentHashMap<Integer, ContestParticipants>();
	static List<LiveCustDetails> liveCustomerListList = new ArrayList<LiveCustDetails>();
	static Map<Integer,QuizContestQuestions> contestCurrentQuestionsMap = new ConcurrentHashMap<Integer, QuizContestQuestions>(); // HC
	
	static List<HallOfFameDtls> thisWeekHallOfFameList = new ArrayList<HallOfFameDtls>();
	static List<HallOfFameDtls> tillDateHallOfFameList = new ArrayList<HallOfFameDtls>();
	
	private static Logger log = LoggerFactory.getLogger(CassContestUtil.class);
	
	static Integer contestSummaryDataSize = 100;
	static Integer contestWinnersSummaryDataSize = 200;
	public static Integer hallOfFamePageMaxDataSize = 25;//100
	public static Boolean HIDE_DASHBOARD_REWARDS = Boolean.FALSE;
	
	private static Integer liveCustCacheCount = 100;
	private static Integer lastLiveCustFetchCount = 0;
	private static Long lastLiveCustFetchTime = null;
	private static Long liveCustRefreshInterval = 1000*30l;
	
	
	public static Integer getHallOfFamePageMaxDataSize() {
		return hallOfFamePageMaxDataSize;
	}
	
	public static void updatePostMigrationStats() throws Exception {
		CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
		CassContestUtil.hallOfFamePageMaxDataSize = 25;
		
		refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
	}
	
	public static void loadApplicationValues() throws Exception {
		URLUtil.loadApplicationValues();
		CassandraConnector.loadApplicationValues();
		DatabaseConnections.loadApplicationValues();
		
		refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
	}
	
	public static void updateCassCustomerMap(CassCustomer customer) {
		cassCustomerMap.put(customer.getId(), customer);
	}
	public static CassCustomer getCasscustomerFromMap(Integer customerId) {
		CassCustomer customer = cassCustomerMap.get(customerId);
		if(customer == null) {
			//System.out.println("CUST to DB HIT : "+customerId);
			customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if(customer != null) {
				cassCustomerMap.put(customer.getId(), customer);
			}
			
		}
		return customer;
	}
	public static void clearCacheDataByContestId(Integer contestId) throws Exception {
		
		customerQuestionAnsMap = new ConcurrentHashMap<String, CustContAnswers>();
		//cassCustomerMap = new ConcurrentHashMap<Integer,CassCustomer>();
		contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
		startedContest = null;
		customerContestAnsMap = new ConcurrentHashMap<Integer, CustContAnswers>();
		customersCountMap.remove(contestId);
		//contestSummaryList = new ArrayList<CassContestWinners>();
		liveCustoemrDtlMap = new ConcurrentHashMap<Integer,CassCustomer>();
		contParticipantsMap = new ConcurrentHashMap<Integer,ContestParticipants>();
		
		contestCurrentQuestionsMap = new ConcurrentHashMap<Integer,QuizContestQuestions>();
		
		MegaJackpotThread.refereshMap();
		
		liveCustomerListList = new ArrayList<LiveCustDetails>();
		lastLiveCustFetchCount = 0;
		lastLiveCustFetchTime = null;
//Fix This		
		refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
		
		//QuizDAORegistry.getQuizContestParticipantsDAO().updateContestParticipantsForContestResetByContestId(contestId);
		//forceManualefreshSummaryDataTable();
		//QuizConfigSettingsUtil.forceUpdateConfigSettings();
		
	}
	
	public static void refreshCacheDataByContestId(Integer contestId) throws Exception {
		
	}
	
	public static QuizContest getCurrentContestByContestId(Integer contestId) {
		if(startedContest == null) {
//Fix This			
			return  SQLDaoUtil.getQuizContestForId(String.valueOf(contestId));
		}
		if(startedContest != null && startedContest.getId().equals(contestId)) {
			return startedContest;
		}
		return null;
	}
	public static Boolean refreshStartContestData(String contestType) {
//Fix This		
		QuizContest contest = SQLDaoUtil.getCurrentStartedContest(contestType);
		if(contest != null) {
			startedContest = contest;
			List<QuizContestQuestions> questionsList = SQLDaoUtil.getContestQuestions( String.valueOf(contest.getId())   );
			for (QuizContestQuestions question : questionsList) {
				contestQuestionMAp.put(question.getId(), question);
			}
			return true;
		}
		return false;
	}
	
	public static QuizContest getContestByIDForStartContest(Integer contestId) {
		return  SQLDaoUtil.getQuizContestForId(String.valueOf(contestId));
	}
	public static void refreshEndContestData() {
		startedContest = null;
		contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
	}
	public static QuizContest getCurrentStartedContest(String contestType) {
		return startedContest;
	}
	public static QuizContestQuestions getQuizContestQuestionById(Integer questionId) {
		return contestQuestionMAp.get(questionId);
		//return QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(questionId);
	}
	
	public static QuizContestQuestions getQuizContestQuestionByContestIdandQuestionSlNo(Integer contestId,Integer questionNo) {
		List<QuizContestQuestions> questions = new ArrayList<QuizContestQuestions>(contestQuestionMAp.values());
		for (QuizContestQuestions quizQuestion : questions) {
			if(quizQuestion.getQuestionSNo().equals(questionNo)) {
				return quizQuestion;
			}
		}
		return null;
		//return QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(questionId);
	}
	
	public static ContestParticipants getContestParticipantsByCustId(Integer customerId) {
		return contParticipantsMap.get(customerId);
	}
	public static ContestParticipants updateContestParticipantsMapByCustId(ContestParticipants contPArticipants) {
		return contParticipantsMap.put(contPArticipants.getCuId(),contPArticipants);
	}
	
	public static List<LiveCustDetails> getLiveCustomersList(Integer contestId) {
		Long now = new Date().getTime();
		if(lastLiveCustFetchTime == null || (now - lastLiveCustFetchTime) >= liveCustRefreshInterval || lastLiveCustFetchCount < liveCustCacheCount) {
			lastLiveCustFetchTime = now;
			Integer count = refreshLiveCustomersList(contestId);
			lastLiveCustFetchCount = count;
		}
		return new ArrayList<LiveCustDetails>(liveCustomerListList);
	}
	public static Integer refreshLiveCustomersList(Integer contestId) {
		List<LiveCustDetails> list = CassandraDAORegistry.getContestParticipantsDAO().getAllLiveCustomerListForCache(contestId);
		if(list != null && !list.isEmpty()) {
			liveCustomerListList = new ArrayList<LiveCustDetails>(list);
			return liveCustomerListList.size();
		}
		return 0;
	}

	
	public static void updateExistingContestParticipantsStatusByJoinContest(Integer customerId) {
		ContestParticipants contParticipant = contParticipantsMap.get(customerId);
		if(contParticipant != null) {
			contParticipant.setStatus("EXIT");
			//contParticipant.setExDate(new Date());
			
			//CassandraDAORegistry.getContestParticipantsDAO().saveForJoin(contParticipant);
		}
	}
	public static void updateExistingContestParticipantsStatusByExitContest(Integer customerId) {
		ContestParticipants contParticipant = contParticipantsMap.remove(customerId);
		if(contParticipant != null) {
			contParticipant.setStatus("EXIT");
			contParticipant.setExDate(new Date().getTime());

			//CassandraDAORegistry.getContestParticipantsDAO().updateForExitContest(contParticipant);
		}
		//CassandraDAORegistry.getContestParticipantsDAO().deleteContestParticipantsByCustomerIdandContestId(customerId, contestId);
		
		/*ContestParticipants participant = new ContestParticipants();
		participant.setCoId(contestId);
		participant.setCuId(customerId);
		participant.setStatus("EXIT");
		CassandraDAORegistry.getContestParticipantsDAO().save(participant);*/
	}
	
	public static CassJoinContestInfo updateJoinContestCustomersCount(CassJoinContestInfo joinContestInfo,Integer contestId,CassCustomer customer) throws Exception {
		try {
		//Integer count=0;
		//boolean isExistingContestant=false;
		List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount == null) {
			contestCount = new ArrayList<Integer>();
		}
		if(contestCount.contains(customer.getId())) {
			//isExistingContestant = true;
		} else {
			contestCount.add(customer.getId());
		}
		//count = contestCount.size();
		customersCountMap.put(contestId, contestCount);

		liveCustoemrDtlMap.put(customer.getId(), customer);
		
//to capture visitors count		
		/*Set<Integer> visitosList = contestVisitorsCountMap.get(contestId);
		if(visitosList == null) {
			visitosList = new HashSet<Integer>();
		}
		if(!visitosList.add(customerId)) {
			isExistingContestant = true;
		}
		contestVisitorsCountMap.put(contestId,visitosList);*/
//end		
		
		//joinContestInfo.setTotalUsersCount(count);
		//joinContestInfo.setIsExistingContestant(isExistingContestant);
		
		
		CustContAnswers customerAnswer = getCustomerAnswers(customer.getId(), contestId);
		if(customerAnswer != null) {
			joinContestInfo.setLqNo(customerAnswer.getqNo());
			if((customerAnswer.getIsCrt() != null && customerAnswer.getIsCrt()) || 
					(customerAnswer.getIsLife() != null && customerAnswer.getIsLife())){
				joinContestInfo.setIsLstCrt(Boolean.TRUE);	
			} else {
				joinContestInfo.setIsLstCrt(Boolean.FALSE);
			}
			joinContestInfo.setCaRwds(customerAnswer.getCuRwds());
			if(customerAnswer.getCuLife() != null && customerAnswer.getCuLife() > 0) {
				joinContestInfo.setIsLifeUsed(Boolean.TRUE);
			} else {
				joinContestInfo.setIsLifeUsed(Boolean.FALSE);
			}
			
			//isExistingContestant = true;
			
		} else {
			joinContestInfo.setLqNo(0);
			joinContestInfo.setIsLstCrt(Boolean.FALSE);
			joinContestInfo.setCaRwds(0.0);
		}
		
		
		//if(!isExistingContestant) {
		//	isExistingContestant = QuizDAORegistry.getQuizContestParticipantsDAO().isExistingContestant(customerId, contestId);
		//}
		//quizJoinContestInfo.setIsExistingContestant(isExistingContestant);
		
		
//		quizJoinContestInfo.setIsContestLifeLineUsed(QuizContestUtil.checkCustomerContestLifeLineUsage(customerId, contestId));
		
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return joinContestInfo;
	}
	
	public static CassJoinContestInfo updateQuizExitCustomersCount(CassJoinContestInfo joinContestInfo,Integer contestId,Integer customerId) throws Exception {
		//Integer count=0;
		//boolean isExistingContestant=false;
		List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount == null) {
			contestCount = new ArrayList<Integer>();
		}
		//if(contestCount.remove(customerId)) {
		//	isExistingContestant = true;
		//}
		contestCount.remove(customerId);
		//count = contestCount.size();
		customersCountMap.put(contestId, contestCount);

		liveCustoemrDtlMap.remove(customerId);
		
		//quizJoinContestInfo.setTotalUsersCount(count);
		
		//quizJoinContestInfo.setIsExistingContestant(isExistingContestant);
		
		/*QuizCustomerContestAnswers customerAnswer = getCustomerAnswers(customerId, contestId);
		if(customerAnswer != null) {
			quizJoinContestInfo.setLastAnsweredQuestionNo(customerAnswer.getQuestionSNo());
			quizJoinContestInfo.setIsLastAnswerCorrect(customerAnswer.getIsCorrectAnswer());
		} else {
			quizJoinContestInfo.setLastAnsweredQuestionNo(0);
			quizJoinContestInfo.setIsLastAnswerCorrect(Boolean.FALSE);
		}
		quizJoinContestInfo.setIsContestLifeLineUsed(QuizContestUtil.checkCustomerContestLifeLineUsage(customerId, contestId));*/
		
		return joinContestInfo;
	}
	
	public static Integer getContestCustomersCount(Integer contestId) throws Exception {
		Integer count=0;
		/*List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount != null) {
			count = contestCount.size();
		}*/		
		
		//count = CassandraDAORegistry.getContestParticipantsDAO().getLiveCustomerCount(contestId);
		
		if(liveCustoemrDtlMap != null) {			
			count=liveCustoemrDtlMap.size();
			//System.out.println("New count @ " + new Date() + " is >>"  +  count);
			
			if(count == 0) { // Temp code... to remove if above count is correct
				//System.out.println("Old count @ " + new Date() + " is >>"  +  count);
				List<Integer> contestCount = customersCountMap.get(contestId);
				if(contestCount != null) {
					count = contestCount.size();				
				}			
			}
		}
		return count;
		
	}
	public static Boolean updateContestWinners(Integer contestId,Integer customerId) {
		
		/*QuizContest contest = getQuizContestByContestId(contestId);
		if(contest == null || contest.getProcessStatus() != null) {
			return false;
		}*/
		
		CassContestWinners contestWinner = new CassContestWinners();
		contestWinner.setCoId(contestId);
		contestWinner.setCuId(customerId);
		contestWinner.setCrDated(new Date().getTime());
		contestWinner.setrTix(0);
		contestWinner.setrPoints(0.0);
		
		CassandraDAORegistry.getCassContestWinnersDAO().save(contestWinner);
		
		/*QuizContestWinners contestWinner = new QuizContestWinners();
		contestWinner.setCustomerId(customerId);
		contestWinner.setContestId(contestId);
		contestWinner.setCreatedDateTime(new Date());
		
		contestWinner.setRewardTickets(0);
		contestWinner.setRewardType(RewardType.POINTS);
		contestWinner.setRewardPoints(0.0);
		contestWinner.setRewardRank(0);
		QuizDAORegistry.getQuizContestWinnersDAO().save(contestWinner);*/

		//DAORegistry.getCustomerDAO().update(customer);
		//CassCustomerUtil.updatedCustomerUtil(customer);
		
/*		Map<Integer,QuizContestWinners> contestwinnersMap = qcu.contestWinnersMap.get(contestId);
		if(contestwinnersMap == null) {
			contestwinnersMap = new ConcurrentHashMap<Integer,QuizContestWinners>();
		}
		contestwinnersMap.put(customerId,contestWinner);
		qcu.contestWinnersMap.put(contestId, contestwinnersMap);
		
		//contestMap.put(contestId, contest);
		log.info( "[updateContestWinners -  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + contestwinnersMap.size());
*/		return true;
	}
	
	public static CustContAnswers getCustomerQuestionAnswers(Integer customerId,Integer questionNo,Integer contestId) throws Exception {
		String key = customerId+"_"+questionNo;
		return customerQuestionAnsMap.get(key);
	}	
	public static Boolean updateCustomerQuestionAnswerMap(CustContAnswers customerAnswers) throws Exception {
		String key = customerAnswers.getCuId()+"_"+customerAnswers.getqNo();
		customerQuestionAnsMap.put(key, customerAnswers);
		return true;
	}
	public static CustContAnswers getCustomerAnswers(Integer customerId,Integer contestId) throws Exception {
		return customerContestAnsMap.get(customerId);
	}	
	public static Boolean updateCustomerAnswerMap(CustContAnswers customerAnswers) throws Exception {
		customerContestAnsMap.put(customerAnswers.getCuId(), customerAnswers);
		return true;
	}
	

	public static Boolean updateCustomerAnswers(CustContAnswers customerAnswers) throws Exception {
		boolean flag=true;
		
		customerQuestionAnsMap.put(customerAnswers.getCuId()+"_"+customerAnswers.getqNo(), customerAnswers);
		
		CustContAnswers mapAnsObj = getCustomerAnswers(customerAnswers.getCuId(), customerAnswers.getCoId());
		if(mapAnsObj == null || mapAnsObj.getqNo() <= customerAnswers.getqNo()) {
			customerContestAnsMap.put(customerAnswers.getCuId(), customerAnswers);	
		} else {
			mapAnsObj.setCuRwds(customerAnswers.getCuRwds());
			mapAnsObj.setCuLife(customerAnswers.getCuLife());
			customerContestAnsMap.put(mapAnsObj.getCuId(), mapAnsObj);
		}

		//capture customer contest answer rewards
		/*if(customerAnswers.getaRwds() != null && customerAnswers.getaRwds() > 0) {
			Map<Integer,Double> customerRewardsMap = customerContestAnswerRewardsMap.get(customerAnswers.getContestId());
			if(customerRewardsMap == null) {
				customerRewardsMap = new ConcurrentHashMap<Integer, Double>();
			}
			Double rewards = customerRewardsMap.get(customerAnswers.getCustomerId());
			if(rewards == null) {
				rewards = 0.0;
			}
			rewards = rewards + customerAnswers.getAnswerRewards();
			rewards = TicketUtil.getNormalRoundedValue(rewards);
			customerRewardsMap.put(customerAnswers.getCustomerId(), rewards);
			customerContestAnswerRewardsMap.put(customerAnswers.getContestId(),customerRewardsMap);
		}*/
		
		//Capture each questions correct and wrong answers count
		//Map<Integer,Map<String,Integer>> contestCountMap = questionOptionsCountMap.get(customerAnswers.getContestId());
		//if(contestCountMap == null) {
		//	contestCountMap = new ConcurrentHashMap<Integer, Map<String,Integer>>();
		//}
		//Map<String,Integer> questionCountMap = contestCountMap.get(customerAnswers.getQuestionSNo());
		//if(questionCountMap == null) {
		//	questionCountMap = new ConcurrentHashMap<String, Integer>();
		//}
		//Integer count = questionCountMap.get(customerAnswers.getAnswer());
		//if(count == null) {
		//	count=0;
		//}
		//count++;
		//questionCountMap.put(customerAnswers.getAnswer(), count);
		//contestCountMap.put(customerAnswers.getQuestionSNo(), questionCountMap);
		//questionOptionsCountMap.put(customerAnswers.getContestId(), contestCountMap);
		
		return flag;
	}
	
	public static Double getContestWinnerRewardPoints(Integer contestId) {
		
		List<CassContestWinners> winnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		if(winnersList != null && !winnersList.isEmpty()) {
			return winnersList.get(0).getrPoints();
		}
		
		return null;
	}
	public static Double updateContestWinnersRewadPoints(Integer contestId) {
		
		List<CassContestWinners> winnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		
		List<CassSuperFanWinner> superFanWinners = null;//CassandraDAORegistry.getCassSuperFanWinnerDAO().getSuperFanWinnersByContestId(contestId);
		 
		 if(null != superFanWinners && !superFanWinners.isEmpty()) {
			 
			 Map<Integer, Boolean> winnerMap = new HashMap<Integer, Boolean>();
			 for (CassContestWinners winnerObj : winnersList) {
				winnerMap.put(winnerObj.getCuId(), true);
			 }
			 
			 Date now = new Date();
			 CassContestWinners contestWinnerObj = null;
			 for (CassSuperFanWinner superFanWinnerObj : superFanWinners) {
				 Boolean isExist = winnerMap.get(superFanWinnerObj.getCuId());
				 if(null != isExist && isExist) {
					continue; 
				 }
				 try {
					 contestWinnerObj = new CassContestWinners(contestId, superFanWinnerObj.getCuId(), 0, 0.00, now.getTime(),superFanWinnerObj.getNoOfWinningChance());
					 CassandraDAORegistry.getCassContestWinnersDAO().save(contestWinnerObj);
					 winnersList.add(contestWinnerObj);
				 }catch(Exception e) {
					 e.printStackTrace();
				 }
				 
			}
			 try {
				 CassandraDAORegistry.getCassSuperFanWinnerDAO().truncate();
			 }catch(Exception e) {
				 e.printStackTrace();
			 }
			
		 }
		 
		Double rewardsPerWinner = 0.0;
		
		if(winnersList != null && !winnersList.isEmpty()) {
			QuizContest contest = getCurrentContestByContestId(contestId);
			Integer winnersCount = winnersList.size();
			Double contestTotalRewards = contest.getTotalRewards();
			
			rewardsPerWinner = Math.floor((contestTotalRewards/winnersCount)*100)/100;
			for (CassContestWinners contestWinner : winnersList) {
				contestWinner.setrPoints(rewardsPerWinner);
//				contestwinnersMap.put(contestWinner.getCustomerId(), contestWinner);
				
				CassandraDAORegistry.getCassContestWinnersDAO().save(contestWinner);
			}
			
			//QuizDAORegistry.getQuizContestWinnersDAO().saveOrUpdateAll(winnersList);
			contest.setPointsPerWinner(rewardsPerWinner);
			
			//QuizDAORegistry.getQuizContestDAO().update(contest);
			
			System.out.println("REWARD COMPUTE : "+winnersCount+" : "+rewardsPerWinner);
		}
		//log.info( "[updateContestWinnersRewadPoints -  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  );
		return rewardsPerWinner;
	}
	
public static List<CassContestWinners> computeContestGrandWinners(Integer contestId) {
		
		List<CassContestWinners> grandWinnersList = new ArrayList<CassContestWinners>();
		Date start = new Date();
		long process =0,postPros=0,dbUpdate=0,finalPros=0;
		List<CassContestWinners> winnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		if(winnersList != null && winnersList.size() > 0) {
			QuizContest contest = getCurrentContestByContestId(contestId);
			
			Integer winnersCount = winnersList.size();
			Integer grandwinnersCount = contest.getMaxFreeTicketWinners();
			
			if(winnersCount >= grandwinnersCount) {
				
				List<Integer> botCustIds = RTFBotsUtil.getAllBots();
				
				List<CassContestWinners> realWinners = new ArrayList<CassContestWinners>();
				Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
				Map<Integer, CassContestWinners> botsMap = new HashMap<Integer, CassContestWinners>();
				
				boolean considerAllWinnerasGrandWinner = false;
				
				if(null != botCustIds && !botCustIds.isEmpty()) {
					
					for (CassContestWinners winner : winnersList) {
						winnersMap.put(winner.getCuId(), winner);
					}
					for(Integer botCutId: botCustIds) {
						CassContestWinners winnerObj = winnersMap.remove(botCutId);
						if(null != winnerObj) {
							botsMap.put(winnerObj.getCuId(), winnerObj);
							continue;
						}
					}
					
					if(null == winnersMap || winnersMap.isEmpty() || winnersMap.size() <= 0) {
						realWinners.addAll(winnersList);
					}else {
						realWinners.addAll(winnersMap.values());
						winnersCount = realWinners.size();
						if(winnersCount <= 0) {
							realWinners.addAll(winnersList);
						}else if(winnersCount < grandwinnersCount) {
							considerAllWinnerasGrandWinner = true;
						} 
					}
					
				}else {
					realWinners.addAll(winnersList);
				}
				
				if(considerAllWinnerasGrandWinner) {
					
					List<CassContestWinners> tempWinners = new ArrayList<CassContestWinners>();
					
					int realWinnerCountTemp = 0;
					if(null != realWinners && !realWinners.isEmpty() && realWinners.size() > 0) {
						realWinnerCountTemp = realWinners.size();
					}
					
					boolean considerAllBots = false;
					
					if(realWinnerCountTemp == 0) {
						 //Consider all bots for winner list
						considerAllBots = true;
					}else{
						tempWinners.addAll(realWinners);
						considerAllBots = false;
					}
					int expectedCount = grandwinnersCount - realWinnerCountTemp;
					
					Set<Integer> botCustIdsIndex = new HashSet<Integer>();
					
					if(!considerAllBots && expectedCount > 0) {
						
						List<CassContestWinners> botWinnerList = new ArrayList<CassContestWinners>();
						
						botWinnerList.addAll(botsMap.values());
						
						winnersCount = botsMap.size();
						
						for(int i=0;i<expectedCount;i++) {
							Boolean flag = true;
							while(flag) {
								Random random = new Random();
								int index = random.nextInt(winnersCount);
								if(botCustIdsIndex.add(index)) {
									CassContestWinners contestWinner = botWinnerList.get(index);
									System.out.println("CASSWINNER RANDOM: FINAL WINNER USERIDBOTS->"+contestWinner.getCuId());
									tempWinners.add(contestWinner);
									flag = false;
								}
							}
						}
						
						grandWinnersList.addAll(tempWinners);
						
					} else if(considerAllBots) {
						winnersCount = winnersList.size();
						Set<Integer> indexList = new HashSet<Integer>();
						for(int i=0;i<grandwinnersCount;i++) {
							Boolean flag = true;
							while(flag) {
								Random random = new Random();
								int index = random.nextInt(winnersCount);
								if(indexList.add(index)) {
									CassContestWinners contestWinner = winnersList.get(index);
									
									grandWinnersList.add(contestWinner);
									flag = false;
								}
							}
						}
					}else {
						grandWinnersList.addAll(tempWinners);
					}
					
					for (CassContestWinners grandWinner : grandWinnersList) {
						System.out.println("CASSWINNER RANDOM: FINAL WINNER USERID->"+grandWinner.getCuId());
					}
					
				} else {
					
					winnersCount = realWinners.size();
					
					Set<Integer> indexList = new HashSet<Integer>();
					for(int i=0;i<grandwinnersCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(indexList.add(index)) {
								CassContestWinners contestWinner = realWinners.get(index);
								System.out.println("CASSWINNER RANDOM: FINAL WINNER USERID->"+contestWinner.getCuId());
								grandWinnersList.add(contestWinner);
								flag = false;
							}
						}
					}
				} 
			} else {
				grandWinnersList.addAll(winnersList);
			}
			process = (new Date().getTime()-start.getTime());
			Integer expairyDays = 1;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, expairyDays);
			Date expiryDate = new Date(cal.getTimeInMillis());
			
			List<CassContestGrandWinner> grandWinners = new ArrayList<CassContestGrandWinner>();
			for (CassContestWinners contestWinner : grandWinnersList) {
				contestWinner.setrTix(contest.getFreeTicketsPerWinner());
				
				CassContestGrandWinner grandWinner = new CassContestGrandWinner();
				grandWinner.setCuId(contestWinner.getCuId());
				grandWinner.setCoId(contestWinner.getCoId());
				grandWinner.setrTix(contest.getFreeTicketsPerWinner());
				grandWinner.setStatus("ACTIVE");
				grandWinner.setCrDated(new Date().getTime());
				grandWinner.setExDate(expiryDate.getTime());
				
				grandWinners.add(grandWinner);
			}
			postPros = new Date().getTime()-(start.getTime()+process);
			CassandraDAORegistry.getCassContestWinnersDAO().saveAll(grandWinnersList);
			CassandraDAORegistry.getCassContestGrandWinnerDAO().saveAll(grandWinners);
			//QuizDAORegistry.getQuizContestWinnersDAO().saveOrUpdateAll(grandWinnersList);
			//QuizDAORegistry.getContestGrandWinnerDAO().saveAll(grandWinners);

			dbUpdate = new Date().getTime()-(start.getTime()+process+postPros);
			//contest.setTicketWinnersCount(grandWinnersList.size());
			//QuizDAORegistry.getQuizContestDAO().update(contest);
		}
		
		for (CassContestWinners contestWinner : grandWinnersList) {
			CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
			if(customer != null) {
				contestWinner.setuId(customer.getuId());
				//contestWinner.setCustomerName(customer.getCustomerName());
				//contestWinner.setCustomerLastName(customer.getLastName());
				contestWinner.setCuId(customer.getId());
				contestWinner.setImgP(customer.getImgP());
				
			}
		}
		finalPros = new Date().getTime()-(start.getTime()+process+postPros+dbUpdate);
		log.info("Grand Winner Compute : "+contestId+" :process: "+process+" :postPros: "+postPros+" :dbUpdate: "+dbUpdate+" :final: "+finalPros+" : tot : "+(new Date().getTime()-start.getTime())+" : "+new Date());
//		log.info( "[computeContestGrandWinners - grandWinnersList size  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + grandWinnersList.size() );
		return grandWinnersList;
	}

public static List<CassContestWinners> computeContestJackpotWinners(Integer contestId,Integer questionId) throws Exception{
	
	List<CassContestWinners> grandWinnersList = new ArrayList<CassContestWinners>();
	Date start = new Date();
	long process =0,postPros=0,dbUpdate=0,finalPros=0;
	
	//List<CassContestWinners> winnersList = CassandraDAORegistry.getCustContAnswersDAO().getAllCorrectAnsCustomerByContestIdandQuestionId(contestId, questionId);
	
	List<CassContestWinners> winnersList = getQuestionAnsweredList(contestId, questionId);
	
	if(winnersList != null && winnersList.size() > 0) {
		System.out.println("MINI JACKPOT LOTTERY : QID: "+questionId+", ANSWEREDCOUNT: "+ winnersList.size());
		QuizContestQuestions question = getQuizContestQuestionById(questionId);
		
		Integer winnersCount = winnersList.size();
		Integer grandwinnersCount = question.getNoOfJackpotWinners();
		
		if(winnersCount >= grandwinnersCount) {
			
			List<Integer> botCustIds = RTFBotsUtil.getAllBotsForJackpot();
			List<CassContestGrandWinner> existingWinners = CassandraDAORegistry.getJackpotWinnerDAO().getJackpotWinners();
			
			if(null != existingWinners && !existingWinners.isEmpty() && existingWinners.size() > 0) {
				for (CassContestGrandWinner obj : existingWinners) {
					botCustIds.add(obj.getCuId());
				}
			}
			
			List<CassContestWinners> realWinners = new ArrayList<CassContestWinners>();
			Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
			Map<Integer, CassContestWinners> botsMap = new HashMap<Integer, CassContestWinners>();
			
			boolean considerAllWinnerasGrandWinner = false;
			
			if(null != botCustIds && !botCustIds.isEmpty()) {
				
				for (CassContestWinners winner : winnersList) {
					winnersMap.put(winner.getCuId(), winner);
				}
				for(Integer botCutId: botCustIds) {
					CassContestWinners winnerObj = winnersMap.remove(botCutId);
					if(null != winnerObj) {
						botsMap.put(winnerObj.getCuId(), winnerObj);
						continue;
					}
				}
				
				if(null == winnersMap || winnersMap.isEmpty() || winnersMap.size() <= 0) {
					realWinners.addAll(winnersList);
				}else {
					realWinners.addAll(winnersMap.values());
					winnersCount = realWinners.size();
					if(winnersCount <= 0) {
						realWinners.addAll(winnersList);
					}else if(winnersCount < grandwinnersCount) {
						considerAllWinnerasGrandWinner = true;
					} 
				}
				
			}else {
				realWinners.addAll(winnersList);
			}
			
			if(considerAllWinnerasGrandWinner) {
				
				List<CassContestWinners> tempWinners = new ArrayList<CassContestWinners>();
				
				int realWinnerCountTemp = 0;
				if(null != realWinners && !realWinners.isEmpty() && realWinners.size() > 0) {
					realWinnerCountTemp = realWinners.size();
				}
				
				boolean considerAllBots = false;
				
				if(realWinnerCountTemp == 0) {
					 //Consider all bots for winner list
					considerAllBots = true;
				}else{
					tempWinners.addAll(realWinners);
					considerAllBots = false;
				}
				int expectedCount = grandwinnersCount - realWinnerCountTemp;
				
				Set<Integer> botCustIdsIndex = new HashSet<Integer>();
				
				if(!considerAllBots && expectedCount > 0) {
					
					List<CassContestWinners> botWinnerList = new ArrayList<CassContestWinners>();
					
					botWinnerList.addAll(botsMap.values());
					
					winnersCount = botsMap.size();
					
					for(int i=0;i<expectedCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(botCustIdsIndex.add(index)) {
								CassContestWinners contestWinner = botWinnerList.get(index);
								System.out.println("MINIJACKPOTWINNER RANDOM: FINAL WINNER USERIDBOTS->"+contestWinner.getCuId());
								tempWinners.add(contestWinner);
								flag = false;
							}
						}
					}
					
					grandWinnersList.addAll(tempWinners);
					
				} else if(considerAllBots) {
					winnersCount = winnersList.size();
					Set<Integer> indexList = new HashSet<Integer>();
					for(int i=0;i<grandwinnersCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(indexList.add(index)) {
								CassContestWinners contestWinner = winnersList.get(index);
								
								grandWinnersList.add(contestWinner);
								flag = false;
							}
						}
					}
				}else {
					grandWinnersList.addAll(tempWinners);
				}
				
				for (CassContestWinners grandWinner : grandWinnersList) {
					System.out.println("MINIJACKPOTWINNER RANDOM: FINAL WINNER USERID->"+grandWinner.getCuId());
				}
				
			} else {
				
				winnersCount = realWinners.size();
				
				Set<Integer> indexList = new HashSet<Integer>();
				for(int i=0;i<grandwinnersCount;i++) {
					Boolean flag = true;
					while(flag) {
						Random random = new Random();
						int index = random.nextInt(winnersCount);
						if(indexList.add(index)) {
							CassContestWinners contestWinner = realWinners.get(index);
							System.out.println("MINIJACKPOTWINNER RANDOM: FINAL WINNER USERID->"+contestWinner.getCuId());
							grandWinnersList.add(contestWinner);
							flag = false;
						}
					}
				}
			} 
		} else {
			grandWinnersList.addAll(winnersList);
		}
		process = (new Date().getTime()-start.getTime());
		Integer expairyDays = 1;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, expairyDays);
		Date expiryDate = new Date(cal.getTimeInMillis());
		
		List<CassContestGrandWinner> grandWinners = new ArrayList<CassContestGrandWinner>();
		for (CassContestWinners contestWinner : grandWinnersList) {
			CassContestGrandWinner grandWinner = new CassContestGrandWinner();
			grandWinner.setCuId(contestWinner.getCuId());
			grandWinner.setCoId(contestWinner.getCoId());
			grandWinner.setqId(questionId);
			grandWinner.setStatus("ACTIVE");
			grandWinner.setJackpotSt("");
			grandWinner.setCrDated(new Date().getTime());
			grandWinners.add(grandWinner);
		}
		postPros = new Date().getTime()-(start.getTime()+process);
		CassandraDAORegistry.getJackpotWinnerDAO().saveAll(grandWinners);
	}
	
	for (CassContestWinners contestWinner : grandWinnersList) {
		CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
		if(customer != null) {
			contestWinner.setuId(customer.getuId());
			contestWinner.setCuId(customer.getId());
			contestWinner.setImgP(customer.getImgP());
		}
	}
	finalPros = new Date().getTime()-(start.getTime()+process+postPros+dbUpdate);
	log.info("Grand Winner Compute : "+contestId+" :process: "+process+" :postPros: "+postPros+" :dbUpdate: "+dbUpdate+" :final: "+finalPros+" : tot : "+(new Date().getTime()-start.getTime())+" : "+new Date());
//	log.info( "[computeContestGrandWinners - grandWinnersList size  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + grandWinnersList.size() );
	return grandWinnersList;
}


public static List<CassContestWinners> computeContestMegaJackpotWinners(QuizContest contest, QuizContestQuestions questionObj) throws Exception {
	
	List<CassContestWinners> grandWinnersList = new ArrayList<CassContestWinners>();
	String jCreditText = "";
	Date start = new Date();
	long process =0,postPros=0,dbUpdate=0,finalPros=0;
	//List<CassContestWinners> winnersList = CassandraDAORegistry.getCustContAnswersDAO().getAllCorrectAnsCustomerByContestIdandQuestionId(contest.getId(), questionObj.getId());
	
	List<CassContestWinners> winnersList = getQuestionAnsweredList(contest.getId(), questionObj.getId());
	
	if(winnersList != null && !winnersList.isEmpty() && winnersList.size() > 0) {
		
		System.out.println("MEGA JACKPOT LOTTERY : QNO: "+questionObj.getQuestionSNo()+", ANSWEREDCOUNT: "+ winnersList.size());
		
		Integer winnersCount = winnersList.size();
		Integer grandwinnersCount =  questionObj.getNoOfJackpotWinners();;
		
		if(winnersCount >= grandwinnersCount) {
			
			List<Integer> botCustIds = RTFBotsUtil.getAllBotsForJackpot();
			List<CassContestGrandWinner> existingWinners = CassandraDAORegistry.getJackpotWinnerDAO().getJackpotWinners();
			if(null != existingWinners && !existingWinners.isEmpty() && existingWinners.size() > 0) {
				for (CassContestGrandWinner obj : existingWinners) {
					botCustIds.add(obj.getCuId());
				}
			}
			
			List<CassContestWinners> realWinners = new ArrayList<CassContestWinners>();
			Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
			Map<Integer, CassContestWinners> botsMap = new HashMap<Integer, CassContestWinners>();
			
			boolean considerAllWinnerasGrandWinner = false;
			
			if(null != botCustIds && !botCustIds.isEmpty()) {
				
				for (CassContestWinners winner : winnersList) {
					winnersMap.put(winner.getCuId(), winner);
				}
				for(Integer botCutId: botCustIds) {
					CassContestWinners winnerObj = winnersMap.remove(botCutId);
					if(null != winnerObj) {
						botsMap.put(winnerObj.getCuId(), winnerObj);
						continue;
					}
				}
				
				if(null == winnersMap || winnersMap.isEmpty() || winnersMap.size() <= 0) {
					realWinners.addAll(winnersList);
				}else {
					realWinners.addAll(winnersMap.values());
					winnersCount = realWinners.size();
					if(winnersCount <= 0) {
						realWinners.addAll(winnersList);
					}else if(winnersCount < grandwinnersCount) {
						considerAllWinnerasGrandWinner = true;
					} 
				}
				
			}else {
				realWinners.addAll(winnersList);
			}
			
			if(considerAllWinnerasGrandWinner) {
				
				List<CassContestWinners> tempWinners = new ArrayList<CassContestWinners>();
				
				int realWinnerCountTemp = 0;
				if(null != realWinners && !realWinners.isEmpty() && realWinners.size() > 0) {
					realWinnerCountTemp = realWinners.size();
				}
				
				boolean considerAllBots = false;
				
				if(realWinnerCountTemp == 0) {
					 //Consider all bots for winner list
					considerAllBots = true;
				}else{
					tempWinners.addAll(realWinners);
					considerAllBots = false;
				}
				int expectedCount = grandwinnersCount - realWinnerCountTemp;
				
				Set<Integer> botCustIdsIndex = new HashSet<Integer>();
				
				if(!considerAllBots && expectedCount > 0) {
					
					List<CassContestWinners> botWinnerList = new ArrayList<CassContestWinners>();
					
					botWinnerList.addAll(botsMap.values());
					
					winnersCount = botsMap.size();
					
					for(int i=0;i<expectedCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(botCustIdsIndex.add(index)) {
								CassContestWinners contestWinner = botWinnerList.get(index);
								System.out.println("MEGAJACKPOTWINNER RANDOM: FINAL WINNER USERIDBOTS->"+contestWinner.getCuId());
								tempWinners.add(contestWinner);
								flag = false;
							}
						}
					}
					
					grandWinnersList.addAll(tempWinners);
					
				} else if(considerAllBots) {
					winnersCount = winnersList.size();
					Set<Integer> indexList = new HashSet<Integer>();
					for(int i=0;i<grandwinnersCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(indexList.add(index)) {
								CassContestWinners contestWinner = winnersList.get(index);
								
								grandWinnersList.add(contestWinner);
								flag = false;
							}
						}
					}
				}else {
					grandWinnersList.addAll(tempWinners);
				}
				
				for (CassContestWinners grandWinner : grandWinnersList) {
					System.out.println("MEGAJACKPOTWINNER RANDOM: FINAL WINNER USERID->"+grandWinner.getCuId());
				}
				
			} else {
				
				winnersCount = realWinners.size();
				
				Set<Integer> indexList = new HashSet<Integer>();
				for(int i=0;i<grandwinnersCount;i++) {
					Boolean flag = true;
					while(flag) {
						Random random = new Random();
						int index = random.nextInt(winnersCount);
						if(indexList.add(index)) {
							CassContestWinners contestWinner = realWinners.get(index);
							System.out.println("MEGAJACKPOTWINNER RANDOM: FINAL WINNER USERID->"+contestWinner.getCuId());
							grandWinnersList.add(contestWinner);
							flag = false;
						}
					}
				}
			} 
		} else {
			grandWinnersList.addAll(winnersList);
		}
		process = (new Date().getTime()-start.getTime());
		Integer expairyDays = 1;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, expairyDays);
		Date expiryDate = new Date(cal.getTimeInMillis());
		
		jCreditText = getMegaJackpotWinnerCreditText(questionObj);
		
		List<CassContestGrandWinner> grandWinners = new ArrayList<CassContestGrandWinner>();
		for (CassContestWinners contestWinner : grandWinnersList) {
			CassContestGrandWinner grandWinner = new CassContestGrandWinner();
			grandWinner.setCuId(contestWinner.getCuId());
			grandWinner.setCoId(contestWinner.getCoId());
			grandWinner.setqId(questionObj.getId());
			grandWinner.setStatus("ACTIVE");
			grandWinner.setJackpotSt("Q"+questionObj.getQuestionSNo()+" WINNER");
			grandWinner.setjCreditSt(jCreditText);
			grandWinner.setCrDated(new Date().getTime());
			grandWinners.add(grandWinner);
		}
		postPros = new Date().getTime()-(start.getTime()+process);
		CassandraDAORegistry.getJackpotWinnerDAO().saveAll(grandWinners);
	}
	
	for (CassContestWinners contestWinner : grandWinnersList) {
		CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
		if(customer != null) {
			contestWinner.setuId(customer.getuId());
			contestWinner.setCuId(customer.getId());
			contestWinner.setImgP(customer.getImgP());
		}
		contestWinner.setrPointsSt("Q"+questionObj.getQuestionSNo()+" WINNER");
		contestWinner.setjCreditSt(jCreditText);
		contestWinner.setrPoints(0.00);
		contestWinner.setrTix(0);
	}
	finalPros = new Date().getTime()-(start.getTime()+process+postPros+dbUpdate);
	log.info("Grand Winner Compute : "+contest.getId()+" :process: "+process+" :postPros: "+postPros+" :dbUpdate: "+dbUpdate+" :final: "+finalPros+" : tot : "+(new Date().getTime()-start.getTime())+" : "+new Date());
//	log.info( "[computeContestGrandWinners - grandWinnersList size  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + grandWinnersList.size() );
	return grandWinnersList;
}

	public static String getMegaJackpotWinnerCreditText(QuizContestQuestions questionObj ) {
		
		String text = "";
		
		switch (questionObj.getjCreditType()) {
		
			case NONE:
				
				break;
				
			case GIFTCARD:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" GIFT CARDS";
				break;
				
			case LIFELINE:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" LIFELINE";
				break;
				
			case REWARD:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" REWARDS";
				break;
				
			case SUPERFANSTAR:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" SUPERFAN STAR";
				break;
				
			case TICKET:
				text = questionObj.getJackpotQtyPerWinner().intValue()+" TICKETS";
				break; 
				
			default:
				break;
		}
		
		return text;
		
	}

	public static List<CassContestWinners> getContestGrandWinners(Integer contestId) {
	
		/*List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestWinners> list =   CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		for (CassContestWinners contestWinner : list) {
			if(contestWinner.getrTix() > 0) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
				contestWinner.setuId(customer.getuId());
				//contestWinner.setCustomerName(customer.getCustomerName());
				//contestWinner.setCustomerLastName(customer.getLastName());
				contestWinner.setCuId(customer.getId());
				contestWinner.setImgP(customer.getImgP());
			
				winners.add(contestWinner);
			}
		}
		return winners;*/
		
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getCassContestGrandWinnerDAO().getContestGrandWinnersByContestId(contestId);
		for (CassContestGrandWinner grandWinner : list) {
			//if(grandWinner.getrTix() > 0) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setrPoints(0.0);
				winner.setrTix(grandWinner.getrTix());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
			
				winners.add(winner);
			//}
		}
		return winners;
	}
	
	public static List<CassContestWinners> getContestSummaryData(Integer contestId)  {
		try {
			List<CassContestWinners> contestWinners = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
			if(contestWinners != null && !contestWinners.isEmpty()) {
				for (CassContestWinners winners : contestWinners) {
					//CassCustomer customer = CassCustomerUtil.getCustomerById(winners.getCuId());
					CassCustomer customer = CassContestUtil.getCasscustomerFromMap(winners.getCuId());
					if(customer != null) {
						winners.setCuId(customer.getId());
						winners.setuId(customer.getuId());
						winners.setImgP(customer.getImgP());
					}
				}
			}
			//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
			return contestWinners;
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<CassContestWinners> getContestMiniJackpotGrandWinners(Integer contestId, Integer questionId) {
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getJackpotWinnerDAO().getJackpotWinnersByContestId(contestId,questionId);
		for (CassContestGrandWinner grandWinner : list) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setqId(grandWinner.getqId());
				//winner.setJackpotSt(grandWinner.getJackpotSt());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
				winners.add(winner);
		}
		return winners;
	}
	
	public static List<CassContestWinners> getContestMegaJackpotGrandWinners(Integer contestId) {
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getJackpotWinnerDAO().getMegaJackpotWinnersByContestId(contestId);
		for (CassContestGrandWinner grandWinner : list) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setqId(grandWinner.getqId());
				winner.setJackpotSt(grandWinner.getJackpotSt());
				winner.setrPointsSt(grandWinner.getJackpotSt());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
				winner.setrPoints(0.00);
				winner.setjCreditSt(grandWinner.getjCreditSt());
				winner.setrTix(0);
				winners.add(winner);
		}
		return winners;
	}
	
	
	public static List<CassContestWinners> getContestMegaJackpotGrandWinners(Integer contestId,Integer questionId) {
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getJackpotWinnerDAO().getJackpotWinnersByContestId(contestId,questionId);
		for (CassContestGrandWinner grandWinner : list) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setqId(grandWinner.getqId());
				winner.setJackpotSt(grandWinner.getJackpotSt());
				winner.setrPointsSt(grandWinner.getJackpotSt());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
				winner.setrPoints(0.00);
				winner.setjCreditSt(grandWinner.getjCreditSt());
				winner.setrTix(0);
				winners.add(winner);
		}
		return winners;
	}
	
	
	public static ContSummaryInfo getContestSummaryDataWithLimit(Integer contestId,ContSummaryInfo contestSummary)  {
		try {
			List<CassContestWinners> contestWinners = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
			if(contestWinners != null && !contestWinners.isEmpty()) {
				
				contestSummary.setwCount(contestWinners.size());
				List<CassContestWinners> finalList = new ArrayList<CassContestWinners>();
				int count = 0;
				for (CassContestWinners cassContestWinners : contestWinners) {
					if(count>=100) {
						break;
					}
					CassCustomer customer = CassContestUtil.getCasscustomerFromMap(cassContestWinners.getCuId());
					if(customer != null) {
						cassContestWinners.setCuId(customer.getId());
						cassContestWinners.setuId(customer.getuId());
						cassContestWinners.setImgP(customer.getImgP());
					}
					finalList.add(cassContestWinners);
					count++;
				}
				contestSummary.setWinners(finalList);
				
			}
			//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return contestSummary;
	}
	
/*public static LiveCustomerListInfo getLiveCustomerDetailsInfoFromCache(Integer contestId,LiveCustomerListInfo liveCustListInfo) throws Exception {
		
		Integer totalCount=0;
		Boolean hasMoreCustomers = false;
		List<LiveCustDetails> liveCustomersList = getLiveCustomersList(contestId);
		
		if(liveCustomersList != null) {
			totalCount = liveCustomersList.size();
		}
		MaxPerPage - Only for Mamta - Added on 11/21/20118 07:27 AM by Ulaganathan
		liveCustListInfo.setMaxPerPage(totalCount);
		liveCustListInfo.settCount(totalCount);
		liveCustListInfo.sethMore(hasMoreCustomers);
		liveCustListInfo.setCustList(liveCustomersList);
		
		return liveCustListInfo;
	}*/
	
public static LiveCustomerListInfo getLiveCustomerDetailsInfo(Integer contestId,LiveCustomerListInfo liveCustListInfo,Integer pageNo,Integer maxRows) throws Exception {
		
		Integer totalCount=0;
		Boolean hasMoreCustomers = false;
		List<LiveCustDetails> customersList = null;
		
		List<Integer> customerIdList = null;
		List<Integer> contestCountList = customersCountMap.get(contestId);
		if(contestCountList != null) {
			totalCount = contestCountList.size();
			Integer fromIndex = maxRows*(pageNo - 1);
			Integer toIndex = maxRows*pageNo;
			if(totalCount > fromIndex) {
				if(totalCount <= toIndex) {
					toIndex = totalCount;
				} else {
					hasMoreCustomers = true;
				}
				customerIdList = new ArrayList<Integer>(contestCountList.subList(fromIndex, toIndex));
			}
			if(customerIdList != null) {
				LiveCustDetails customerDtl = null;
				customersList = new ArrayList<LiveCustDetails>();
				for (Integer customerId : customerIdList) {
					CassCustomer customer = liveCustoemrDtlMap.get(customerId);
					if(customer == null) {
						customer = CassContestUtil.getCasscustomerFromMap(customerId);
						//customer = CassCustomerUtil.getCustomerById(customerId);						
					}
					if(customer != null) {
						customerDtl = new LiveCustDetails(customer.getId(),customer.getuId(),customer.getImgP());
						customersList.add(customerDtl);
					}
					
				}
			}
		}
		/*MaxPerPage - Only for Mamta - Added on 11/21/20118 07:27 AM by Ulaganathan*/
		liveCustListInfo.setMaxPerPage(totalCount);
		liveCustListInfo.settCount(totalCount);
		liveCustListInfo.sethMore(hasMoreCustomers);
		liveCustListInfo.setCustList(customersList);
		
		return liveCustListInfo;
	}
	
public static List<CassContestWinners> getQuestionAnsweredList(Integer contestId,Integer questionId) throws Exception {
	
	List<CassContestWinners> answeredList = new ArrayList<CassContestWinners>();
	
	List<CustContAnswers> custansList = new ArrayList<CustContAnswers>(customerContestAnsMap.values());	
	CassContestWinners contestWinners=null;
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqId().equals(questionId) && null != cans.getIsCrt() && cans.getIsCrt()) {
				contestWinners= new CassContestWinners();
				contestWinners.setCoId(contestId);
				contestWinners.setCuId(cans.getCuId());
				contestWinners.setqId(questionId);;
				answeredList.add(contestWinners);
			}
		}
	}
	return answeredList;
}

public static AnswerCountInfo getQuestAnsCount(Integer contestId,Integer questionId,AnswerCountInfo ansCountInfo) throws Exception {
	
	Integer aCount=0,bCount=0,cCount=0;
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByQuestId(questionId);
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByContestIdandQuestId(contestId,questionId);
	List<CustContAnswers> custansList = new ArrayList<CustContAnswers>(customerContestAnsMap.values());	
	
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqId().equals(questionId) && cans.getAns() != null) {
				if(cans.getAns().equalsIgnoreCase("A")) {
					aCount = aCount + 1;
				} else if(cans.getAns().equalsIgnoreCase("B")) {
					bCount = bCount + 1;
				} else if(cans.getAns().equalsIgnoreCase("C")) {
					cCount = cCount + 1;
				}
			}
		}
	}
	ansCountInfo.setaCount(aCount);
	ansCountInfo.setbCount(bCount);
	ansCountInfo.setcCount(cCount);
	
	return ansCountInfo;
}
public static List<LiveCustDetails> getQuestionAnsweredRandomcustomersByquestionNo(Integer contestId,Integer questionId,Integer questionNo) throws Exception {
	
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByQuestId(questionId);
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByContestIdandQuestId(contestId,questionId);
	Set<CustContAnswers> custansList = new HashSet<CustContAnswers>(customerContestAnsMap.values());	
	
	Integer randomCount = 10;
	Integer cuCount = 0;
	List<LiveCustDetails> custList = new ArrayList<LiveCustDetails>();
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqId().equals(questionId) && cans.getIsCrt()) {
				CassCustomer customer = CassContestUtil.getCasscustomerFromMap(cans.getCuId());
				if(customer != null) {
					LiveCustDetails custDtl = new LiveCustDetails();
					custDtl.setCuId(customer.getId());
					custDtl.setuId(customer.getuId());
					custList.add(custDtl);
					cuCount++;
					
					if(cuCount >= randomCount) {
						return custList;
					}
				}
			}
		}
	}
	return custList;
}

public static Integer getLifelineUSedCustomersCountByQuestionNo(Integer contestId,Integer questionNo) throws Exception {
	
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByQuestId(questionId);
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByContestIdandQuestId(contestId,questionId);
	List<CustContAnswers> custansList = new ArrayList<CustContAnswers>(customerContestAnsMap.values());	
	
	Integer totCount = 0;
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqNo().equals(questionNo) && cans.getIsLife()) {
				totCount++;
			}
		}
	}
	return totCount;
}

public static CustContAnswers computeAutoCreateAnswers(Integer customerId,Integer contestId,Integer startQno,Integer endQno,String ansResMsg,
		CustContAnswers lastAnswer) throws Exception {
	
	try {
		boolean isAutoFlag = true;
		if(isAutoFlag) {
			for(int qNo=startQno; qNo<=endQno && endQno>0; qNo++) {
				
				CustContAnswers autoQuestionAns = CassContestUtilOld.getCustomerQuestionAnswers(customerId,qNo, contestId);
				if(autoQuestionAns != null) {
					ansResMsg += "AUTO:Answer Created:"+qNo+".";
				} else {

					QuizContestQuestions autoQuestion = CassContestUtil.getQuizContestQuestionByContestIdandQuestionSlNo(contestId, qNo);
					if(autoQuestion == null) {
						ansResMsg += "AUTO:Quest Not Exist:"+qNo+".";
						continue;
					}
					
					Double cumulativeRewards = 0.0;
					Integer cumulativeLifeLineUsed = 0;
					if(lastAnswer != null) {
						if(lastAnswer.getCuRwds() != null) {
							cumulativeRewards = lastAnswer.getCuRwds();
						}
						if(lastAnswer.getCuLife() != null) {
							cumulativeLifeLineUsed = lastAnswer.getCuLife();
						}
					}
					
					/*if(answerOption.equalsIgnoreCase(quizQuestion.getAnswer())) {
						//quizAnswerInfo.setIsCrt(Boolean.TRUE);
						
						if(contest.getNoOfQuestions().equals(questionNo)) {
							CassContestUtil.updateContestWinners(contest.getId(), customerId);
						}
					}*/
					
					CustContAnswers custContAns = new CustContAnswers();
					custContAns.setCuId(customerId);
					custContAns.setCoId(contestId);
					custContAns.setqId(autoQuestion.getId());
					custContAns.setqNo(qNo);
					custContAns.setAns(null);
					custContAns.setIsCrt(Boolean.TRUE);
					custContAns.setCrDate(new Date().getTime());
					custContAns.setIsLife(false);
					custContAns.setFbCallbackTime(null);
					custContAns.setAnswerTime(null);
					custContAns.setRetryCount(0);
					custContAns.setIsAutoCreated(Boolean.TRUE);
				
					custContAns.setaRwds(autoQuestion.getQuestionRewards());
					if(autoQuestion.getQuestionRewards() != null) {
						cumulativeRewards = cumulativeRewards + autoQuestion.getQuestionRewards();	
					}
					custContAns.setCuRwds(cumulativeRewards);
					custContAns.setCuLife(cumulativeLifeLineUsed);
					
					CassandraDAORegistry.getCustContAnswersDAO().save(custContAns);
					CassContestUtil.updateCustomerAnswers(custContAns);
					
					lastAnswer = custContAns;
					
				}
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		//System.out.println("Error Occured in AUTO ANs Creation Block:"+customerIdStr+":"+contestIdStr+":"+questionNoStr+":"+new Date());
		throw e;
	}
	return lastAnswer;
}
public static boolean refreshHallOfFameForThisWeekData()  {
	try { 
		//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize, null, null, null);
		List<HallOfFameDtls> winnersList = SQLDaoUtil.getHallOfFameByDateFromView(hallOfFamePageMaxDataSize);
		if(winnersList != null) {
			thisWeekHallOfFameList = new ArrayList<HallOfFameDtls>(winnersList);
		}
		
		
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
	return true;
}
public static boolean refreshHallOfFameTillDateData()  {
	
	 
	try {
		//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDate(contestSummaryDataSize);
		List<HallOfFameDtls> winnersList = SQLDaoUtil.getHallOfFameByTillDateFromView(hallOfFamePageMaxDataSize);
		if(winnersList != null) {
			tillDateHallOfFameList = new ArrayList<HallOfFameDtls>(winnersList);
		}
		
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
	return true;
}

public static List<HallOfFameDtls> getHallOfFameForThisWeekData()  {
	try {
		if(thisWeekHallOfFameList == null || thisWeekHallOfFameList.isEmpty()) {
			refreshHallOfFameForThisWeekData();
		}
		//return  new ArrayList<QuizContestWinners>(thisWeekSummaryList);
		return thisWeekHallOfFameList;
		
	}catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}
public static List<HallOfFameDtls> getHallOfFameForTillDateData()  {
	try {
		if(tillDateHallOfFameList == null || tillDateHallOfFameList.isEmpty()) {
			refreshHallOfFameTillDateData();
		}
		//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
		return tillDateHallOfFameList;
		
	}catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}

public static Boolean updateContestRewardsToCassCustomer(Integer contestId) throws Exception {
	
	/*Map<Integer,CustContAnswers> customerAnswersMap = new HashMap<Integer,CustContAnswers>(); 
	List<CustContAnswers> custAnsList =  CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContId(contestId);
	for (CustContAnswers custContAnswers : custAnsList) {
		CustContAnswers tempCustAns = customerAnswersMap.get(custContAnswers.getCuId());
		if(tempCustAns == null || tempCustAns.getqNo() < custContAnswers.getqNo()) {
			customerAnswersMap.put(custContAnswers.getCuId(),custContAnswers);
		}
	}
	
	List<CassContestWinners> winnersList =  CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
	Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
	if(winnersList != null) {
		for (CassContestWinners winner : winnersList) {
			winnersMap.put(winner.getCuId(), winner);
		}
	}
	
	if(customerAnswersMap != null) {
		List<Integer> keyList = new ArrayList<>(customerAnswersMap.keySet());
		for (Integer custId : keyList) {
			Double rewards = 0.0;
			Integer livesUsed = 0;
			CustContAnswers custAns = customerAnswersMap.remove(custId);
			rewards = custAns.getCuRwds();
			livesUsed = custAns.getCuLife();
		
			CassContestWinners winner = winnersMap.remove(custId);
			if(winner != null) {
				rewards = rewards + winner.getrPoints();
			}
			if(rewards > 0 || livesUsed > 0) {
				CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
				
				rewards = rewards + customer.getcRewardDbl();
				Integer quizLives = customer.getqLive() - livesUsed;
				
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), quizLives, rewards);
			}
		}
	}
	if(!winnersMap.isEmpty()) {
		List<Integer> keyList = new ArrayList<>(winnersMap.keySet());
		for (Integer custId : keyList) {
			Double rewards = 0.0;
			CassContestWinners winner = winnersMap.remove(custId);
			rewards = rewards + winner.getrPoints();
			if(rewards > 0) {
				CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
				rewards = rewards + customer.getcRewardDbl();
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(customer.getId(), rewards);
				//CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), quizLives, rewards);
			}
		}
	}*/
	
	List<CassContestWinners> winnersList =  CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId); 
	if(customerContestAnsMap != null) {
		Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
		if(winnersList != null) {
			for (CassContestWinners winner : winnersList) {
				winnersMap.put(winner.getCuId(), winner);
			}
		}
		
		List<Integer> keyList = new ArrayList<>(customerContestAnsMap.keySet());
		for (Integer custId : keyList) {
			Double rewards = 0.0;
			Integer livesUsed = 0;
			CustContAnswers custAns = customerContestAnsMap.remove(custId);
			rewards = custAns.getCuRwds();
			livesUsed = custAns.getCuLife();
		
			CassContestWinners winner = winnersMap.remove(custId);
			if(winner != null) {
				rewards = rewards + winner.getrPoints();
			}
			if(rewards > 0 || livesUsed > 0) {
				CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
				
				rewards = rewards + customer.getcRewardDbl();
				Integer quizLives = customer.getqLive() - livesUsed;
				
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), quizLives, rewards);
			}
		}
	}
	return true;
}
/*public static Boolean updateCustomerContestDetailsinCassandra(Integer contestId) throws Exception {

	QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
	if(!contest.getIsCustomerStatsUpdated()) {
		Date start = new Date();
		Date startOne = new Date();
		
		startOne = new Date();
		updateContestRewardsToCassCustomer(contestId);
		log.info("Time to Update CONT REWARDS to Customer : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		try {
			startOne = new Date();
		//QuizContestUtil.forceSummaryRefreshTable();
		//QuizContestUtil.refreshSummaryDataTable();
		//QuizContestUtil.refreshSummaryDataCache();
		//QuizContestUtil.forceManualefreshSummaryDataTable();
		log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("Time to Update ALL CONTEST STATS : "+(new Date().getTime()-start.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		contest.setIsCustomerStatsUpdated(true);
		QuizDAORegistry.getQuizContestDAO().update(contest);
		return true;
	}
	return false;
}*/
/*
public static Boolean updateCustomerPostContestDetailsinSQL(Integer contestId) throws Exception {

	//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
	//if(!contest.getIsCustomerStatsUpdated()) {
		Date fromDate = new Date();
		Date start = new Date();
		Date startOne = new Date();
		QuizDAORegistry.getQuizQueryManagerDAO().updateAllCustomersContestDataByContestId(contestId);
		//log.info("Time to Update Cust COntest Data : " + "[ " +  new QuizContestUtil().getIPAddress() + " ] " +(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		startOne = new Date();
		QuizDAORegistry.getQuizQueryManagerDAO().updateAllCustomersContestWinsDataByContestId(contestId);
		log.info("Time to Update Cust COntest Wins Data : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		Commented - this funtion commented in mobile screen itself. not in use 
		//startOne = new Date();
		//QuizCustomerPromocodeandContestOrderStatsScheduler.processCustomerPromoCodeAndContestOrderStats();
		//log.info("Time to Update Cust PRomo and co stats Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		startOne = new Date();
		CustomerUtil.updateCustomerUtilForContestCustomers(contestId, fromDate);
		log.info("Time to Update CUST UTIL for STATS : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		try {
			startOne = new Date();
		//QuizContestUtil.forceSummaryRefreshTable();
		//QuizContestUtil.refreshSummaryDataTable();
		//QuizContestUtil.refreshSummaryDataCache();
		QuizContestUtil.forceManualefreshSummaryDataTable();
		log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("Time to Update ALL CONTEST STATS : "+(new Date().getTime()-start.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		//contest.setIsCustomerStatsUpdated(true);
		//QuizDAORegistry.getQuizContestDAO().update(contest);
		return true;
	//}
	//return false;
}*/


public static Boolean updateContestCurrentQuestions(QuizContestQuestions contestQuestion) throws Exception {
	
	contestCurrentQuestionsMap.put(contestQuestion.getContestId(), contestQuestion);
	return true;
}
public static QuizContestQuestions getContestCurrentQuestions(Integer contestId) throws Exception {
	QuizContestQuestions questions = contestCurrentQuestionsMap.get(contestId);
	return questions;
}

public static void truncateCassandraContestData(String contestIdStr) {
	
	try {
		/*Integer contestId = Integer.parseInt(contestIdStr.trim());
		
		List<ContestMigrationStats> migrationStats = DAORegistry.getContestMigrationStatsDAO().getAllStatsByContestId(contestId);
		
		if(null == migrationStats) {
			return;
		}
		
		boolean isCompleted = false;
		for (ContestMigrationStats obj : migrationStats) {
			if(obj.getStatus().equals("COMPLETED")) {
				isCompleted = true;
				break;
			}
		}
		if(isCompleted) {
			CassandraDAORegistry.getCustContAnswersDAO().truncate();
			CassandraDAORegistry.getCassContestGrandWinnerDAO().truncate();
			CassandraDAORegistry.getCassContestWinnersDAO().truncate();
			//CassandraDAORegistry.getContestParticipantsDAO().truncate();
		}*/
		
		CassandraDAORegistry.getCustContAnswersDAO().truncate();
		CassandraDAORegistry.getCassContestGrandWinnerDAO().truncate();
		CassandraDAORegistry.getCassContestWinnersDAO().truncate();
		CassandraDAORegistry.getJackpotWinnerDAO().truncate();
	}catch(Exception e){
		throw e;
	}
//Fix This	
	CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
	
	/*MigrateApiTrackingUtil trackingUtil = new MigrateApiTrackingUtil();
	Thread t = new Thread(trackingUtil);
	t.start();*/
	
}

public static Integer startCustomerId = 493942;
public static Integer endCustomerId = 543946;
public static Integer currentCustomerId = startCustomerId;

public static Integer getRandomCustomerIdForTest() {
	if(currentCustomerId >= endCustomerId) {
		currentCustomerId = startCustomerId;
	}
	currentCustomerId +=1;
	return currentCustomerId;
}

}
