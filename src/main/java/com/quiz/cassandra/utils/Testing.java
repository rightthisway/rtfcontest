package com.quiz.cassandra.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class Testing {
	
	public static void main(String[] args) {
		
		List<Integer> grandWinnersList = new ArrayList<Integer>();
		List<Integer> winnersList = new ArrayList<Integer>();
		List<Integer> botCustIds = new ArrayList<Integer>();
		
		for(int i = 1000;i<=1050;i++) {
			winnersList.add(i);
			/*if(i % 5 == 0 || i%3 == 0) {
				botCustIds.add(i);
			}*/
			
			if(i != 1012 && i != 1015 && i != 1016 && i != 1017 && i != 1018 && i != 1019) {
				botCustIds.add(i);
			}
		}
		 
		
		System.out.println("botCustIdsArray : "+botCustIds);
		System.out.println("winnersListArray : "+winnersList);
		
		Integer winnersCount = winnersList.size();
		Integer grandwinnersCount = 5;
		
		if(winnersCount >= grandwinnersCount) {
			
			List<Integer> realWinners = new ArrayList<Integer>();
			Map<Integer, Integer> winnersMap = new HashMap<Integer, Integer>();
			
			boolean considerAllWinnerasGrandWinner = false;
			
			if(null != botCustIds && !botCustIds.isEmpty()) {
				
				for (Integer winner : winnersList) {
					winnersMap.put(winner, winner);
				}
				for(Integer botCutId: botCustIds) {
					Integer winnerObj = winnersMap.remove(botCutId);
					if(null != winnerObj) {
						continue;
					}
				}
				
				if(null == winnersMap || winnersMap.isEmpty() || winnersMap.size() <= 0) {
					realWinners.addAll(winnersList);
				}else {
					realWinners.addAll(winnersMap.values());
					winnersCount = realWinners.size();
					if(winnersCount <= 0) {
						realWinners.addAll(winnersList);
					}else if(winnersCount < grandwinnersCount) {
						considerAllWinnerasGrandWinner = true;
						/*grandWinnersList.addAll(realWinners);
						grandwinnersCount = grandwinnersCount - realWinners.size();*/
					} 
				}
				
			}else {
				realWinners.addAll(winnersList);
			}
			
			if(considerAllWinnerasGrandWinner) {
				
				List<Integer> tempWinners = new ArrayList<Integer>();
				
				int realWinnerCountTemp = 0;
				if(null != realWinners && !realWinners.isEmpty() && realWinners.size() > 0) {
					realWinnerCountTemp = realWinners.size();
				}
				
				boolean considerAllBots = false;
				Set<Integer> botCustIdsIndex = new HashSet<Integer>();
				
				if(realWinnerCountTemp == 0) {
					 //Consider all bots for winner list
					considerAllBots = true;
				}else{
					for (Integer intObj : tempWinners) {
						botCustIdsIndex.add(intObj);
					}
					tempWinners.addAll(realWinners);
					considerAllBots = false;
				}
				int expectedCount = grandwinnersCount - realWinnerCountTemp;
				
				winnersCount = winnersList.size();
				
				if(!considerAllBots && expectedCount > 0) {
					for(int i=0;i<expectedCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(botCustIdsIndex.add(index)) {
								Integer contestWinner = winnersList.get(index);
								tempWinners.add(contestWinner);
								flag = false;
							}
						}
					}
				}
				if(considerAllBots) {
					Set<Integer> indexList = new HashSet<Integer>();
					for(int i=0;i<grandwinnersCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(indexList.add(index)) {
								Integer contestWinner = winnersList.get(index);
								grandWinnersList.add(contestWinner);
								flag = false;
							}
						}
					}
				}else {
					grandWinnersList.addAll(tempWinners);
				}
				
			} else {
				
				winnersCount = realWinners.size();
				
				Set<Integer> indexList = new HashSet<Integer>();
				for(int i=0;i<grandwinnersCount;i++) {
					Boolean flag = true;
					while(flag) {
						Random random = new Random();
						int index = random.nextInt(winnersCount);
						if(indexList.add(index)) {
							Integer contestWinner = realWinners.get(index);
							System.out.println("CASSWINNER RANDOM: FINAL WINNER USERID->"+contestWinner);
							grandWinnersList.add(contestWinner);
							flag = false;
						}
					}
				}
			} 
		} else {
			grandWinnersList.addAll(winnersList);
		}
		
		for (Integer integer : grandWinnersList) {
			
			System.out.println(integer);
			
		}
	}

}
