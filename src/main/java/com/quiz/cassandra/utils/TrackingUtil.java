package com.quiz.cassandra.utils;

import java.util.Date;
import java.util.UUID;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassRtfApiTracking;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.utils.URLUtil;

public class TrackingUtil {

	
	/*public static void contestValidateAnswerAPITracking(String platForm,String deviceType, String sessionId,WebServiceActionType actionType,String actionResult,
			String questionNo, String questionId, String answer, Integer contestId,Integer custId, Date startDate, Date endDate,String ip){
		try{
			if(actionResult == null) {
				actionResult = "";
			}
			String description =  actionResult+" :qNo : "+questionNo+" :qId: "+questionId+ " :ans:"+answer;
			
			CassRtfApiTracking obj = new CassRtfApiTracking(UUID.randomUUID(), startDate, endDate, ip, actionType.toString(), platForm, deviceType, 
					sessionId, actionResult, custId, contestId, description, "","");
			CassandraDAORegistry.getCassRtfApiTrackingDAO().save(obj);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST VALIDATE ANSWER API - CASS");
		} 
	}*/
	public static void contestValidateAnswerAPITrackingForDeviceTimeTracking(String platForm,String deviceType, String sessionId,WebServiceActionType actionType,String actionResult,
			String questionNo, String questionId, String answer, Integer contestId,Integer custId, Date startDate, Date endDate,String ip,String apiHitStartTimeStr,String fbTimeStr,String deviceInfo,
			Integer resStatus,String questionSlNo,Integer retryCount,Integer ansCount,String description){
		try{
			if(actionResult == null) {
				actionResult = "";
			}
			if(description == null) {
				description =  actionResult+" :qNo : "+questionNo+" :qId: "+questionId+ " :ans:"+answer;
			}
			
			CassRtfApiTracking obj = new CassRtfApiTracking(UUID.randomUUID(), startDate, endDate, ip, actionType.toString(), platForm, deviceType, 
					sessionId, actionResult, custId, contestId, description, deviceInfo,"",apiHitStartTimeStr,fbTimeStr,resStatus,questionSlNo,retryCount,ansCount,URLUtil.clusterNodeId);
			CassandraDAORegistry.getCassRtfApiTrackingDAO().saveForDeviceTimeTracking(obj);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST VALIDATE ANSWER API - CASS");
		} 
	}
	public static void contestAPITrackingForDeviceTimeTracking(String platForm,String deviceType, String sessionId,WebServiceActionType actionType,String actionResult,
			Integer contestId,Integer custId, Date startDate, Date endDate,String ip, String appVersion,String apiHitStartTimeStr,Integer resStatus,String questionSlNo){
		try{
			if(actionResult == null) {
				actionResult = "";
			}
			
			if(appVersion == null) {
				appVersion = "";
			}
			
			CassRtfApiTracking obj = new CassRtfApiTracking(UUID.randomUUID(), startDate, endDate, ip, actionType.toString(), platForm, deviceType, 
					sessionId, actionResult, custId, contestId, actionResult, "",appVersion,apiHitStartTimeStr,"",resStatus,questionSlNo,URLUtil.clusterNodeId);
			
			CassandraDAORegistry.getCassRtfApiTrackingDAO().saveForDeviceTimeTracking(obj);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST API - CASS");
		} 
	}
	
	public static void contestAPITracking(String platForm,String deviceType, String sessionId,WebServiceActionType actionType,String actionResult,
			Integer contestId,Integer custId, Date startDate, Date endDate,String ip, String appVersion,Integer resStatus,String questionSlNo){
		try{
			if(actionResult == null) {
				actionResult = "";
			}
			
			if(appVersion == null) {
				appVersion = "";
			}
			
			CassRtfApiTracking obj = new CassRtfApiTracking(UUID.randomUUID(), startDate, endDate, ip, actionType.toString(), platForm, deviceType, 
					sessionId, actionResult, custId, contestId, actionResult, "",appVersion,resStatus,questionSlNo,URLUtil.clusterNodeId);
			
			CassandraDAORegistry.getCassRtfApiTrackingDAO().save(obj);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST API - CASS");
		} 
	}
	/*public static void contestCommonAPITracking(String platForm,String deviceType, String sessionId,WebServiceActionType actionType,String actionResult,
			Integer contestId,Integer custId, Date startDate, Date endDate,String ip){
		try{
			if(actionResult == null) {
				actionResult = "";
			}
			
			CassRtfApiTracking obj = new CassRtfApiTracking(UUID.randomUUID(), startDate, endDate, ip, actionType.toString(), platForm, deviceType, 
					sessionId, actionResult, custId, contestId, actionResult, "","");
			
			CassandraDAORegistry.getCassRtfApiTrackingDAO().save(obj);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST API - CASS");
		} 
	}*/
}
