package com.quiz.cassandra.utils;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;

public class CassCustomerUtil {
	
	public static CassCustomer getCustomerById(Integer customerId){
		try {
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			return customer;	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*public static void addCustomer(Customer customer, CustomerLoyalty customerLoyalty){
		try {
			CassCustomer obj = new CassCustomer(customer.getId(), customer.getUserId(), customer.getEmail(), 
					customer.getPhone(), customer.getQuizCustomerLives(), null != customerLoyalty?customerLoyalty.getActivePointsAsDouble():0.00, 
					customer.getCustImagePath(), customer.getIsOtpVerified());
			CassandraDAORegistry.getCassCustomerDAO().saveCustomer(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void updateCustomerLives(Customer customer){
		try {
			CassandraDAORegistry.getCassCustomerDAO().updateCustomerLives(customer.getId(), customer.getQuizCustomerLives());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	 
	
	 

}
