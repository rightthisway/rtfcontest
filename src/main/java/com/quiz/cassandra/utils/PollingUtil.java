package com.quiz.cassandra.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang.StringUtils;

import com.zonesws.webservices.data.PollingCategory;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class PollingUtil {

	public static Integer WINNER_MSG_PERCENTAGE = 60;
	public static final Integer VID_PLAY_FETCH_SIZE = 10;
	public static final String POLLINGTYPE_RTF = "RTF";
	public static final String POLLINGTYPE_SPONSOR = "SPONSOR";
	public static final String POLLINGTYPE_FEEDBACK = "FEEDBACK";
	public static final String AWS_WINNER_UPLOAD_FOLDER = "winneruploads";
	public static final String AWS_GENERAL_UPLOAD_FOLDER = "generaluploads";
	public static final String AWS_GENERAL_FLAG = "G";
	public static final String AWS_WINNER_FLAG = "W";
	public static final Integer MAX_MEDIA_UPLOAD_COUNT = 5;
	public static String BLANK_STRING = "";
	public static String YES = "Y";
	public static String NO = "N";
	public static final String ABUSETYPE_VIDEO = "VIDEO";
	public static final String ABUSETYPE_COMMENT = "COMMENT";
	public static final String ABUSETYPE_FANCLUB = "FANCLUB";
	public static final String ABUSETYPE_FANCLUB_EVENT = "FANCLUBEVENT";
	
	public static final String ACTIVE="ACTIVE";
	public static final String DELETED="DELETED";
	public static final Integer MAX_PAGE_ROWS = 20;
	

	public static final String MOBILE_MSG_SUCCESS = "Success";
	public static final String ERR_NO_QUES = "Insufficient Questions for Customer From Question Bank";
	public static final String ERR_MAX_QUES = "You have Answered all the Questions for the day";
	public static final String MOBILE_MSG_SUCCESS_POLL_PRIZE = "Congrats! You won!";
	public static final String MOBILE_MSG_WRONG_ANSWER = "SORRY! NOT THIS TIME! TRY AGAIN!";
	public static final String MOBILE_MSG_INSUFFICIENT_REDEEM_QTY = "You Do Not have Sufficient quantity to redeem";
	public static final String MOBILE_MSG_NOT_ALLOWED_REDEEM_TYPE = "You Cannot Redeem Magic Wand.";
	public static final String MOBILE_MSG_REDEEMED_SUCCESS = "Redeemed Successfully";
	public static final String MOBILE_MSG_NO_VIDEO = "Sorry. Videos are not available to play at this moment";
	public static final String MOBILE_MSG_NO_QUESTIONS = "Sorry. Questions are not available at this moment. \n Please try again in some time";
	public static final String MOBILE_MSG_ERR_PROCESSING_QUESTIONS = "Sorry, We had trouble fetching the Question for you at this moment";
	public static final String MOBILE_MSG_NO_CONTEST_TDB = "Next Contest will be Announced Shortly ";
	public static final String MOBILE_MSG_NXT_CONTEST_DETS = "Next Live Trivia: ";
	public static final String MOBILE_FUN_MESSAGE = "Awesome! Love that choice! \n Unfortunately, no prize this time!  \n Try again in XX Mins!";
	public static final String MOBILE_FUN_MESSAGE_ALT = "Awesome! Love that choice! \n Unfortunately, no prize this time!  \n Try again in a few Mins!";
	public static final String SHOW_INTRO_REWARD_MSG_CORRECT_ANS = "CONGRATS! You got it correct! \n You have won "
			+ "\n" + " 1 Life " + "\n" + " 1  SuperFan Star " + "\n" + " 1 MagicWand ";
	public static final String SHOW_INTRO_REWARD_MSG_WRONG_ANS = "Oops You got it wrong!. \n  CONGRATS! You have won "
			+ "\n" + " 1 Life " + "\n" + " 1  SuperFan Star " + "\n" + " 1 MagicWand ";
	public static final String SHOW_INTRO_CORRECT_ANS_OPT = "C";
	public static final String MOBILE_MSG_UPLOAD_SUCCESS = "Your File has been uploaded Successfully";
	public static final String MOBILE_MSG_UPLOAD_FAILURE = "Sorry. We had trouble while uploading your file. Pleaase try again in some time";
	public static final String SUCCESS = "Success";
	public static final String COMMON_FAILURE = "Sorry. We had trouble processing your request";
	public static final String MOBILE_MSG_MAX_CUST_UPLOAD = "Sorry. You cannot upload more than 5 files per day";
	public static final String MOBILE_MSG_WINNER_MAX_UPLOAD = "Sorry. You Have Already Uploaded Your Winning Moments ";
	public static final String MOBILE_MSG_ONLY_WINNER_CAN_UPLOAD = "Sorry. You Have Not Won Any Trivia Recently. ";
	public static final String MOBILE_MSG_NO_QUESTIONS_CONFIGURED = "Sorry, We are unable to serve you questions at this moment ";

	public static final String POLL_RWD_MSG_1 = "Love that choice! \n You just earned XX for playing!";
	public static final String POLL_RWD_MSG_2 = "Are you in the majority? \n Well, you just earned XX!";
	public static final String MOBILE_MSG_NO_VIDEO_CATEGORIES_DEFINED ="No Video Categories Available at this moment";
	//public static final String POLL_RWD_MSG_3 = "Weird choice, but still cool! \n You just earned XX for playing!";
	
	public static final String UPLOAD_VID_MSG = "YAY...Awesome! Please share your winning moments! "
			+ " with us. \n";
	public static final String VIDEO_REWARD_MGS1 = "YAY...Awesome! You just earned XX YYY \n for watching our video! ";
	public static final String GENERAL_ERROR = "Something went wrong. Please try again in some time";		
	//public static final String VIDEO_REWARD_MAX_ERROR = "Max Rewards given for the day";
	public static final String VIDEO_REWARD_MAX_ERROR = "Max Rewards given for the video";
	public static final String VIDEO_REWARD_MAX_NOTSET_ERROR = "Max Rewards Not Configured";
	public static final String VIDEO_REWARD_NOTSET_ERROR = "Rewards Not Configured for Viewing Video ";
	public static final String EMPTY_MYUPLOADED_MEDIA = " You have not uploaded any videos ";
	public static final String EMPTY_MYLIKED_MEDIA = " You have not Favourited any videos ";
	public static final String WINNER_UPLOAD_TEXT =" Winnner ";
	public static final String GENERAL_UPLOAD_TEXT = "General ";
	public static final String VIDEO_VIEW_REWARD_DUPLICATE="You have recieved reward earlier for watching same video";
	public static final String CUST_VIDEO_UPLOAD_SUCCESS="Your Video has been Uploaded to RTF Channel";
	public static String aws_s3_cdp_url = "https://rtfservermedia.s3.amazonaws.com/dp/";
	
	/**
	 * NEW REWARD MESSAGES 
	 */
	public static String LIKE_VIDEO_REWARDS = "Hey! You just got Rewarded with 00 points for liking videos";
	public static String VIDEO_WATCH_REWARDS = "Hey! You just got Rewarded with 00 points for watching our video";
	public static String FANCLUB_CREATE_POST = "Hey! You just got Rewarded with 00 points for creating a Post";
	public static String FANCLUB_CREATE_EVENT = "Hey! You just got Rewarded with 00 points for creating an Event";
	public static String UPLOAD_FANCLUB_VIDEO = "Hey! You just got Rewarded with 00 points for uploading a Video";
	public static String UPLOAD_FAN_FREAKOUT_VIDEO = "Hey! You just got Rewarded with 00 points for uploading a Video";
	public static String SHOW_INTEREST_TO_EVENT = "Hey! You just got Rewarded with 00 points for showing Interest in the Event";
	public static String LIKE_POST = "Hey! You just got Rewarded with 00 points for liking a Post";
	public static String VIDEO_SHARES = "Hey! You just got Rewarded with 00 Superfan stars for Sharing a Video";
	//FANCLUB MESSAGES START 
	public static final String NO_FANCLUB_POSTS_FOUND="Hey ! Be the first one to post something here";
	public static final String NO_FANCLUB_POSTS_COMMENTS_FOUND="Hey ! Be the first one to comment";
	public static final String NO_FANCLUB_VIDEOS_FOUND="Hey ! Be the first one to upload a video";
	public static final String NO_FANCLUB_VIDEO_COMMENTS_FOUND="Hey ! Be the first one to comment";
	//FANCLUB MESSAGES END 
	
	
	
	// Above message(MOBILE_MSG_ERR_PROCESSING_QUESTIONS) to be changed ...

	/**
	 * Reward Currency Types
	 */public static final String VIDEO_REWARD_COMPUTE_ERROR = "Customer Rewards Not Computed correctly for Viewing Video ";
	public static String REWARD_CRYSTALL_BALL = "cb";
	public static String REWARD_MAGICWAND = "mw";
	public static String REWARD_GOLDBARS = "gb";
	public static String REWARD_HIFIVE = "hf";
	public static String REWARD_LIFE = "lf";
	public static String REWARD_SFSTARS = "sf";
	public static String REWARD_FUNMESSAGE = "ms";
	public static String REWARD_RTFPOINTS = "pt";

	public static String REDEEM_TO_LIFE = "lf";
	public static String REDEEM_TO_STARS = "sf";
	public static String REDEEM_TO_REWD_DOLLARS = "rd";
	public static String COLUMN_FUNMSG = "funmsg";

	public static String REWARD_CRYSTALL_BALL_DISPLAY = "Crystal Ball";
	public static String REWARD_MAGICWAND_DISPLAY = "Magic Wand";
	public static String REWARD_GOLDBARS_DISPLAY = "Gold Bricks";
	public static String REWARD_HIFIVE_DISPLAY = "HiFive";
	final static Map<String, String> rewardTypeMap;
	final static Map<String, String> rewardColumMap;
	final static List<String> redeemToList;
	final static List<String> redeemFromList;

	final static Map<String, String> cassColumnMap;
	final static Map<String, String> pollingRewardStatsColumnMap;
	final static List<String> pollingRewardMessageList;	
	final static List<String> questionRewardMessageList;
	final static List<String> pollingVideoRewardList;
	
	static {
		pollingVideoRewardList = new ArrayList<String>();
		pollingVideoRewardList.add(REWARD_LIFE);
		pollingVideoRewardList.add(REWARD_MAGICWAND);
		pollingVideoRewardList.add(REWARD_SFSTARS);
	}
	
	static {
		pollingRewardMessageList = new ArrayList<String>();
		pollingRewardMessageList.add(POLL_RWD_MSG_1);
		pollingRewardMessageList.add(POLL_RWD_MSG_2);
		//pollingRewardMessageList.add(POLL_RWD_MSG_3);		
	}
	
	static {
		questionRewardMessageList = new ArrayList<String>();
		questionRewardMessageList.add(POLL_RWD_MSG_1);
		questionRewardMessageList.add(POLL_RWD_MSG_2);			
	}
	

	static {
		cassColumnMap = new HashMap<String, String>();
		cassColumnMap.put(REWARD_CRYSTALL_BALL, "crystalball");
		cassColumnMap.put(REWARD_MAGICWAND, "magicwand");
		cassColumnMap.put(REWARD_GOLDBARS, "goldbar");
		cassColumnMap.put(REWARD_HIFIVE, "hifive");
		cassColumnMap.put(REDEEM_TO_LIFE, "no_of_lives");
		cassColumnMap.put(REDEEM_TO_STARS, "super_fan_chances");
		cassColumnMap.put(REDEEM_TO_REWD_DOLLARS, "active_reward_points");
	}

	static {
		
		pollingRewardStatsColumnMap = new HashMap<String, String>();
		pollingRewardStatsColumnMap.put(REWARD_MAGICWAND, "magicwand");
		pollingRewardStatsColumnMap.put(REDEEM_TO_LIFE, "lives");
		pollingRewardStatsColumnMap.put(REDEEM_TO_STARS, "sfstars");
		pollingRewardStatsColumnMap.put(REWARD_FUNMESSAGE, COLUMN_FUNMSG);
		pollingRewardStatsColumnMap.put(REWARD_RTFPOINTS, "rtfpoints");
	}
	
	public static List<String> getpollingVideoRewardList() {
		return pollingVideoRewardList;
	}
	  
	public static String getRewardStatsColumnForRwdType(String key) {
		return pollingRewardStatsColumnMap.get(key);
	}

	public static String getCassColumnForRwdType(String key) {
		return cassColumnMap.get(key);
	}

	static {
		redeemToList = new ArrayList<String>();
		redeemToList.add(REDEEM_TO_LIFE);
		redeemToList.add(REDEEM_TO_STARS);
		redeemToList.add(REDEEM_TO_REWD_DOLLARS);
	}
	static {
		redeemFromList = new ArrayList<String>();
		redeemFromList.add(REWARD_CRYSTALL_BALL);
		redeemFromList.add(REWARD_GOLDBARS);
		redeemFromList.add(REWARD_HIFIVE);

	}

	static {
		rewardTypeMap = new HashMap<String, String>();
		rewardTypeMap.put(REWARD_CRYSTALL_BALL, REWARD_CRYSTALL_BALL_DISPLAY);
		rewardTypeMap.put(REWARD_MAGICWAND, REWARD_MAGICWAND_DISPLAY);
		rewardTypeMap.put(REWARD_GOLDBARS, REWARD_GOLDBARS_DISPLAY);
		rewardTypeMap.put(REWARD_HIFIVE, REWARD_HIFIVE_DISPLAY);
	}
	static {
		rewardColumMap = new HashMap<String, String>();
		rewardColumMap.put(REWARD_CRYSTALL_BALL, "crystalball");
		rewardColumMap.put(REWARD_MAGICWAND, "magicwand");
		rewardColumMap.put(REWARD_GOLDBARS, "goldbar");
		rewardColumMap.put(REWARD_HIFIVE, "hifive");
		rewardColumMap.put(REDEEM_TO_STARS, "super_fan_chances");
		rewardColumMap.put(REDEEM_TO_REWD_DOLLARS, "active_reward_points");
		rewardColumMap.put(REWARD_FUNMESSAGE, "funmsg");
		rewardColumMap.put(REWARD_LIFE, "no_of_lives");
		rewardColumMap.put(REWARD_RTFPOINTS, "rtf_points");
	}

	public static List<String> getRedeemToTypeList() {
		return redeemToList;
	}

	public static List<String> getRedeemFromTypeList() {
		return redeemFromList;
	}

	public static String getRewardDisplayMap(String key) {
		return rewardTypeMap.get(key);
	}

	public static String getPollingRewardSQLColumn(String key) {
		return rewardColumMap.get(key);
	}

	public static PollingCategory fetchRandomPollCategoryType(List<PollingCategory> pollcatTypeList) {
		int randomElementIndex = ThreadLocalRandom.current().nextInt(pollcatTypeList.size()) % pollcatTypeList.size();

		return pollcatTypeList.get(randomElementIndex);
	}

	public static List getAnsOptionList() {
		List<String> ansList = new ArrayList<String>();
		ansList.add("A");
		ansList.add("B");
		ansList.add("C");
		return ansList;
	}

	public static String fetchRandomRewardType(List<String> rewardTypesList) {
		int randomElementIndex = ThreadLocalRandom.current().nextInt(rewardTypesList.size()) % rewardTypesList.size();

		return rewardTypesList.get(randomElementIndex);
	}

	public static int getRandomRwdUnit(int maxUnit) {
		Random rand = new Random();
		return rand.ints(1, (maxUnit + 1)).findFirst().getAsInt();
	}

	public static String getCreatedDateStr() {
		Calendar cal = Calendar.getInstance();
		java.sql.Timestamp timestamp = new java.sql.Timestamp(cal.getTimeInMillis());

		return String.valueOf(timestamp);
	}

	public static Integer getZeroIfNull(Integer data) {

		if (data == null)
			return Integer.valueOf(0);
		return data;

	}

	public static Double getZeroIfDblNull(Double data) {

		if (data == null)
			return Double.valueOf(0);
		return data;

	}

	public static String getNextGameDetails() throws Exception {

		List<String> gameList = PollingSQLDaoUtil.getNextContestDetails();
		if (gameList == null || gameList.size() == 0) {
			return MOBILE_MSG_NO_CONTEST_TDB;
		}
		String gameName = gameList.get(0);
		String startDateStr = gameList.get(1);
		//System.out.println(" [PollingUtil][Next game is] " + gameName);
		//System.out.println("[PollingUtil] [ Next game time is ] " + startDateStr);
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date startDate = dateFormater.parse(startDateStr);
		SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy");
		SimpleDateFormat timeFt = new SimpleDateFormat("hh:mmaa z");
		startDateStr = dt.format(startDate);
		String contestStartTime = "";
		String displayMessage = MOBILE_MSG_NXT_CONTEST_DETS + "\n" + gameName;
		try {
			Date today = dt.parse(dt.format(new Date()));
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			Date tomorrow = dt.parse(dt.format(new Date(cal.getTimeInMillis())));
			Date contestDate = dt.parse(dt.format(startDate));
			if (contestDate.compareTo(today) == 0) {
				contestStartTime = "Today " + timeFt.format(startDate);
				displayMessage = displayMessage + "\n " + contestStartTime;
				return displayMessage;
			} else if (contestDate.compareTo(tomorrow) == 0) {
				contestStartTime = "Tomorrow " + timeFt.format(startDate);
				displayMessage = displayMessage + "\n " + contestStartTime;
				return displayMessage;
			}
			contestStartTime = dt.format(startDate) + " " + timeFt.format(startDate);
		} catch (Exception e) {
			e.printStackTrace();
			return MOBILE_MSG_NO_CONTEST_TDB;
		}
		displayMessage = displayMessage + " " + contestStartTime;
		System.out.println(" [PollingUtil] " + displayMessage);
		return displayMessage;

	}

	public static void main(String a[]) throws Exception {
		System.out.println("contest date time is " + getNextGameDetails());

	}

	public static List<Integer> getRandomQueNunList(Integer maxQuestions) {
		return fetchRandomQIDList(maxQuestions, WINNER_MSG_PERCENTAGE);
	}

	public static int generateRandom(int start, int end, List<Integer> exclude) {
		Random rand = new Random();
		int range = end - start + 1 - exclude.size();
		int random = rand.nextInt(range) + 1;
		for (int i = 0; i < exclude.size(); i++) {
			if (!exclude.contains(random))
				return random;
			random++;
		}

		return random;
	}

	public static List<Integer> fetchRandomQIDList(Integer maxQues, Integer pctNoPrize) {
		if (maxQues == null || maxQues <= 3)
			return null;
		List<Integer> userRandomList = new ArrayList<Integer>();
		try {			
			int min = 1;
			int randomNumbersRequired = maxQues * pctNoPrize / 100;
			Random random = new Random();
			int randomNumber = random.nextInt(maxQues - min + 1) + min;
			if (randomNumber == 0)
				return null;
			List<Integer> exclude = new ArrayList<Integer>();
			userRandomList.add(randomNumber);
			exclude.add(randomNumber - 1);
			exclude.add(randomNumber);
			exclude.add(randomNumber + 1);
			for (int i = 0; i < randomNumbersRequired - 1; i++) {
				int newRandom = generateRandom(min, maxQues, exclude);
				userRandomList.add(newRandom);
				//exclude.add(newRandom - 1);
				exclude.add(newRandom);
				//exclude.add(newRandom + 1);

			}
		} catch (Exception ex) {
			//ex.printStackTrace();
			return userRandomList;
		}
		System.out.println("User Random List is " + userRandomList);
		return userRandomList;

	}

	public static Boolean isEmptyOrNull(String s) {
		return StringUtils.isBlank(s);
	}
	public static List<String> getpollingRewardMessageList() {
		return pollingRewardMessageList;
	}
	
	public static String getPollingRwdMessage(String rwdType , Integer rwdQty) {		
		List<String> msgList = getpollingRewardMessageList();
		Collections.shuffle(msgList);
		String msg = msgList.get(0);
		try {
		if(msg != null && msg.contains("XX")) {
			String rwdTy = (String) pollingRewardStatsColumnMap.get(rwdType);
			String replaceTxt = ""+ rwdQty + " " + rwdTy;
			msg = msg.replace("XX", replaceTxt);
		}
		}catch(Exception ex) {
			ex.printStackTrace();
			msg = MOBILE_MSG_SUCCESS_POLL_PRIZE;
		}
				
		return msg;
		
	}
	public static String getEmptyifNull(String str) {
		if(StringUtils.isEmpty(str)) 
			return BLANK_STRING;
		return str;
	}

	public static String encodeStringUrl(String url) {
	    String encodedUrl =null;
	    try {
	         encodedUrl = URLEncoder.encode(url, "UTF-8");
	    } catch (UnsupportedEncodingException e) {
	        return encodedUrl;
	    }
	    return encodedUrl;
	}

	public static String decodeStringUrl(String encodedUrl) {
	    String decodedUrl =null;
	    try {
	         decodedUrl = URLDecoder.decode(encodedUrl, "UTF-8");
	    } catch (UnsupportedEncodingException e) {
	        return decodedUrl;
	    }
	    return decodedUrl;
	}

}
