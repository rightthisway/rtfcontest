package com.quiz.cassandra.data;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("ValidateAnsReqDtls")
public class ValidateAnsReqDtls  implements Serializable{
	
	private Integer cuId;
	private Integer coId;
	private Integer qId;
	private Integer qNo;
	private String ans;
	private String fbctm;
	private String anstm;
	private Integer rtc=0;
	
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	public Integer getqId() {
		return qId;
	}
	public void setqId(Integer qId) {
		this.qId = qId;
	}
	public Integer getqNo() {
		return qNo;
	}
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}
	public String getAns() {
		return ans;
	}
	public void setAns(String ans) {
		this.ans = ans;
	}
	public String getFbctm() {
		return fbctm;
	}
	public void setFbctm(String fbctm) {
		this.fbctm = fbctm;
	}
	public String getAnstm() {
		return anstm;
	}
	public void setAnstm(String anstm) {
		this.anstm = anstm;
	}
	
	public Integer getRtc() {
		return rtc;
	}
	public void setRtc(Integer rtc) {
		this.rtc = rtc;
	}
	@Override
	public String toString() {
		return "qId=" + qId + ",qNo=" + qNo + ",ans=" + ans + ", rtc=" + rtc + ".";
	}
	
	
	
}
