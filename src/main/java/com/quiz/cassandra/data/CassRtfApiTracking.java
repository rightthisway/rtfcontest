package com.quiz.cassandra.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Cust")
public class CassRtfApiTracking {
	
	private UUID id;
	private Date startDate;
	private Date endDate;
	private String startDateStr;
	private String endDateStr;
	private String custIPAddr;
	private String apiName;
	private String platForm;
	private String deviceType;
	private String sessionId;
	private String actionResult;
	private Integer custId;
	private Integer contestId;
	private String description;
	private String deviceInfo;
	private String appVerion;
	private String fbCallbackTime;
	private String deviceApiStartTime;
	private String questSlNo;
	private Integer resStatus;
	private Integer retryCount;
	private Integer ansCount;
	private Integer nodeId;
	
	public CassRtfApiTracking(UUID id, String startDateStr, String endDateStr, String custIPAddr, String apiName, String platForm,
			String deviceType, String sessionId, String actionResult, Integer custId, Integer contestId,
			String description, String deviceInfo,String appVerion,String deviceApiStartTime,String fbCallbackTime,Integer resStatus,String questionSlNo,
			Integer retryCount,Integer ansCount,Integer nodeId) {
		super();
		this.id = id;
		this.startDateStr = startDateStr;
		this.endDateStr = endDateStr;
		this.custIPAddr = custIPAddr;
		this.apiName = apiName;
		this.platForm = platForm;
		this.deviceType = deviceType;
		this.sessionId = sessionId;
		this.actionResult = actionResult;
		this.custId = custId;
		this.contestId = contestId;
		this.description = description;
		this.deviceInfo = deviceInfo;
		this.appVerion = appVerion;
		this.deviceApiStartTime = deviceApiStartTime;
		this.fbCallbackTime = fbCallbackTime;
		this.resStatus=resStatus;
		this.questSlNo=questionSlNo;
		this.retryCount=retryCount;
		this.ansCount=ansCount;
		this.nodeId=nodeId;
		
	}
	
	public CassRtfApiTracking(UUID id, Date startDate, Date endDate, String custIPAddr, String apiName, String platForm,
			String deviceType, String sessionId, String actionResult, Integer custId, Integer contestId,
			String description, String deviceInfo,String appVerion,Integer resStatus,String questionSlNo,Integer nodeId) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.custIPAddr = custIPAddr;
		this.apiName = apiName;
		this.platForm = platForm;
		this.deviceType = deviceType;
		this.sessionId = sessionId;
		this.actionResult = actionResult;
		this.custId = custId;
		this.contestId = contestId;
		this.description = description;
		this.deviceInfo = deviceInfo;
		this.appVerion = appVerion;
		this.resStatus=resStatus;
		this.questSlNo=questionSlNo;
		this.nodeId=nodeId;
	}
	
	public CassRtfApiTracking(UUID id, Date startDate, Date endDate, String custIPAddr, String apiName, String platForm,
			String deviceType, String sessionId, String actionResult, Integer custId, Integer contestId,
			String description, String deviceInfo,String appVerion,String deviceApiStartTime,String fbCallbackTime,
			Integer resStatus,String questionSlNo,Integer nodeId) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.custIPAddr = custIPAddr;
		this.apiName = apiName;
		this.platForm = platForm;
		this.deviceType = deviceType;
		this.sessionId = sessionId;
		this.actionResult = actionResult;
		this.custId = custId;
		this.contestId = contestId;
		this.description = description;
		this.deviceInfo = deviceInfo;
		this.appVerion = appVerion;
		this.deviceApiStartTime = deviceApiStartTime;
		this.fbCallbackTime = fbCallbackTime;
		this.resStatus=resStatus;
		this.questSlNo=questionSlNo;
		this.nodeId=nodeId;
		
	}
	public CassRtfApiTracking(UUID id, Date startDate, Date endDate, String custIPAddr, String apiName, String platForm,
			String deviceType, String sessionId, String actionResult, Integer custId, Integer contestId,
			String description, String deviceInfo,String appVerion,String deviceApiStartTime,String fbCallbackTime,
			Integer resStatus,String questionSlNo,Integer retryCount,Integer ansCount,Integer nodeId) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.custIPAddr = custIPAddr;
		this.apiName = apiName;
		this.platForm = platForm;
		this.deviceType = deviceType;
		this.sessionId = sessionId;
		this.actionResult = actionResult;
		this.custId = custId;
		this.contestId = contestId;
		this.description = description;
		this.deviceInfo = deviceInfo;
		this.appVerion = appVerion;
		this.deviceApiStartTime = deviceApiStartTime;
		this.fbCallbackTime = fbCallbackTime;
		this.resStatus=resStatus;
		this.questSlNo=questionSlNo;
		this.retryCount=retryCount;
		this.ansCount=ansCount;
		this.nodeId=nodeId;
		
	}
	 
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getCustIPAddr() {
		if(null == custIPAddr) {
			custIPAddr = "";
		}
		return custIPAddr;
	}
	public void setCustIPAddr(String custIPAddr) {
		this.custIPAddr = custIPAddr;
	}
	public String getApiName() {
		return apiName;
	}
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	public String getPlatForm() {
		if(null == platForm) {
			platForm = "";
		}
		return platForm;
	}
	public void setPlatForm(String platForm) {
		this.platForm = platForm;
	}
	public String getDeviceType() {
		if(null == deviceType) {
			deviceType = "";
		}
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getSessionId() {
		if(null == sessionId) {
			sessionId = "";
		}
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getActionResult() {
		if(null == actionResult) {
			actionResult = "";
		}
		return actionResult;
	}
	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
	public Integer getCustId() {
		if(null == custId) {
			custId = 0;
		}
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Integer getContestId() {
		if(null == contestId) {
			contestId = 0;
		}
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public String getDescription() {
		if(null == description) {
			description = "";
		}
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDeviceInfo() {
		if(null == deviceInfo) {
			deviceInfo = "";
		}
		return deviceInfo;
	}
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getAppVerion() {
		if(null == appVerion) {
			appVerion = "";
		}
		return appVerion;
	}

	public void setAppVerion(String appVerion) {
		this.appVerion = appVerion;
	}

	public String getFbCallbackTime() {
		if(null == fbCallbackTime) {
			fbCallbackTime = "";
		}
		return fbCallbackTime;
	}

	public void setFbCallbackTime(String fbCallbackTime) {
		this.fbCallbackTime = fbCallbackTime;
	}

	public String getDeviceApiStartTime() {
		if(null == deviceApiStartTime) {
			deviceApiStartTime = "";
		}
		return deviceApiStartTime;
	}

	public void setDeviceApiStartTime(String deviceApiStartTime) {
		this.deviceApiStartTime = deviceApiStartTime;
	}

	public String getQuestSlNo() {
		if(questSlNo == null) {
			questSlNo="";
		}
		return questSlNo;
	}

	public void setQuestSlNo(String questSlNo) {
		this.questSlNo = questSlNo;
	}

	public Integer getResStatus() {
		if(resStatus == null) {
			resStatus=-1;
		}
		return resStatus;
	}

	public void setResStatus(Integer resStatus) {
		this.resStatus = resStatus;
	}

	public Integer getRetryCount() {
		if(retryCount == null) {
			retryCount=0;
		}
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public Integer getAnsCount() {
		if(ansCount == null) {
			ansCount=0;
		}
		return ansCount;
	}

	public void setAnsCount(Integer ansCount) {
		this.ansCount = ansCount;
	}

	public Integer getNodeId() {
		if(nodeId==null) {
			nodeId=0;
		}
		return nodeId;
	}

	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	
	 
}
