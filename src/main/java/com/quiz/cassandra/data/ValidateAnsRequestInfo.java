package com.quiz.cassandra.data;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("ValidateAnsRequestInfo")
public class ValidateAnsRequestInfo  implements Serializable{
	
	List<ValidateAnsReqDtls> ansDtls;

	public List<ValidateAnsReqDtls> getAnsDtls() {
		return ansDtls;
	}

	public void setAnsDtls(List<ValidateAnsReqDtls> ansDtls) {
		this.ansDtls = ansDtls;
	}

	@Override
	public String toString() {
		return "ValidateAnsRequestInfo [ansDtls=" + ansDtls + "]";
	}
	

}
