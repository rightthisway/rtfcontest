package com.quiz.cassandra.data;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * represents CassSuperFanWinner entity
 * @author Tamil
 *
 */
public class CassSuperFanWinner  implements Serializable{
	
	private Integer coId;
	private Integer cuId; 
	private Long crDated;
	private Integer noOfWinningChance;
	 
	public CassSuperFanWinner( ) { }
	
	public CassSuperFanWinner(Integer coId, Integer cuId, Long crDated,Integer noOfWinningChance) {
		this.coId = coId;
		this.cuId = cuId; 
		this.crDated = crDated;
		this.noOfWinningChance = noOfWinningChance;
	}

	public Integer getCoId() {
		return coId;
	}

	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public Long getCrDated() {
		return crDated;
	}

	public void setCrDated(Long crDated) {
		this.crDated = crDated;
	}

	public Integer getNoOfWinningChance() {
		if(null == noOfWinningChance) {
			noOfWinningChance = 1;
		}
		return noOfWinningChance;
	}

	public void setNoOfWinningChance(Integer noOfWinningChance) {
		this.noOfWinningChance = noOfWinningChance;
	}

}
