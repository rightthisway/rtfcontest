package com.quiz.cassandra.data;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.quiz.cassandra.utils.TicketUtil;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.URLUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("ContestWinners")
public class CassContestWinners  implements Serializable{
	
	private Integer coId;
	private Integer cuId;
	private Integer qId;
	private Integer rTix;
	private Double rPoints;
	
	@JsonIgnore
	private Long crDated;
	
	@JsonIgnore
	private String imgP;//profile image path
	private String imgU;// profile image URL
	
	private String uId;
	private String rPointsSt;
	@JsonIgnore
	private String jackpotSt;
	
	private String jCreditSt;
	
	@JsonIgnore
	private Integer noOfWinningChance;
	
	public CassContestWinners( ) { }
	
	public CassContestWinners(Integer coId, Integer cuId, Integer rTix, Double rPoints, Long crDated, Integer noOfWinningChance) {
		this.coId = coId;
		this.cuId = cuId;
		this.rTix = rTix;
		this.rPoints = rPoints;
		this.crDated = crDated;
		this.noOfWinningChance = noOfWinningChance;
	}
	
	public CassContestWinners(Integer coId, Integer cuId) {
		this.coId = coId;
		this.cuId = cuId; 
	}
	
	public CassContestWinners(Integer coId, Integer cuId, Integer rTix, String jackpotSt, Long crDated, Integer noOfWinningChance) {
		this.coId = coId;
		this.cuId = cuId;
		this.rTix = rTix;
		this.jackpotSt = jackpotSt;
		this.crDated = crDated;
		this.noOfWinningChance = noOfWinningChance;
	}
	
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public Integer getrTix() {
		return rTix;
	}
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}

	public Double getrPoints() {
		if(rPoints == null) {
			rPoints = 0.0;
		}
		return rPoints;
	}
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}

	public Long getCrDated() {
		return crDated;
	}
	public void setCrDated(Long crDated) {
		this.crDated = crDated;
	}

	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getImgP() {
		return imgP;
	}
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}

	public String getImgU() {
		//imgU = URLUtil.profilePicWebURByImageName(this.imgP);
		imgU = URLUtil.profilePicForSummary(this.imgP);
		return imgU;
	}
	
	
	
	
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	public String getrPointsSt() {
		String jakcpotStr = getJackpotSt();
		if(null != jakcpotStr && !jakcpotStr.isEmpty()) {
			rPointsSt = jakcpotStr;
		}else {
			if(rPoints != null) {
				try {
					rPointsSt = TicketUtil.getRoundedValueString(rPoints);
				} catch (Exception e) {
					rPointsSt="0.00";
					e.printStackTrace();
				}
			} else {
				rPointsSt="0.00";
			}
		}
		
		return rPointsSt;
	}
	public void setrPointsSt(String rPointsSt) {
		this.rPointsSt = rPointsSt;
	}

	public String getJackpotSt() {
		if(null == jackpotSt) {
			jackpotSt = "";
		}
		return jackpotSt;
	}

	public void setJackpotSt(String jackpotSt) {
		this.jackpotSt = jackpotSt;
	}

	public Integer getqId() {
		return qId;
	}

	public void setqId(Integer qId) {
		this.qId = qId;
	}

	public String getjCreditSt() {
		if(null == jCreditSt) {
			jCreditSt = "";
		}
		return jCreditSt;
	}

	public void setjCreditSt(String jCreditSt) {
		this.jCreditSt = jCreditSt;
	}

	public Integer getNoOfWinningChance() {
		if(null == noOfWinningChance) {
			noOfWinningChance = 1;
		}
		return noOfWinningChance;
	}

	public void setNoOfWinningChance(Integer noOfWinningChance) {
		this.noOfWinningChance = noOfWinningChance;
	}



}
