package com.quiz.cassandra.data;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("CustContAnswers")
public class CustContAnswers  implements Serializable{
	
	private Integer cuId;
	private Integer coId;
	private Integer qId;
	private Integer qNo;
	private String ans;
	private Boolean isCrt = false;
	private Boolean isLife;
	private Double aRwds;
	
	private String fbCallbackTime;
	private String answerTime;
	private Integer retryCount;
	private Boolean isAutoCreated;
	
	@JsonIgnore
	private Double cuRwds;
	
	@JsonIgnore
	private Integer cuLife;
	
	@JsonIgnore
	private Long crDate;
	@JsonIgnore
	private Long upDate;
	
	
	public CustContAnswers() {}
	
	public CustContAnswers(Integer cuId, Integer coId, Integer qId, Integer qNo, String ans, Boolean isCrt,
			Boolean isLife, Double aRwds, Double cuRwds, Integer cuLife, Long crDate,Long upDate,
			String fbCallbackTime,String answerTime,Integer retryCount,Boolean isAutoCreated) {
		this.cuId = cuId;
		this.coId = coId;
		this.qId = qId;
		this.qNo = qNo;
		this.ans = ans;
		this.isCrt = isCrt;
		this.isLife = isLife;
		this.aRwds = aRwds;
		this.cuRwds = cuRwds;
		this.cuLife = cuLife;
		this.crDate = crDate;
		this.upDate = upDate;
		this.fbCallbackTime=fbCallbackTime;
		this.answerTime=answerTime;
		this.retryCount=retryCount;
		this.isAutoCreated=isAutoCreated;
	}
	public CustContAnswers(Integer cuId, Integer coId, Integer qId, Integer qNo, String ans, Boolean isCrt,
			Boolean isLife) {
		this.cuId = cuId;
		this.coId = coId;
		this.qId = qId;
		this.qNo = qNo;
		this.ans = ans;
		this.isCrt = isCrt;
		this.isLife = isLife;
	}
	
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	public Integer getqId() {
		return qId;
	}
	public void setqId(Integer qId) {
		this.qId = qId;
	}
	public Integer getqNo() {
		return qNo;
	}
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}
	public String getAns() {
		return ans;
	}
	public void setAns(String ans) {
		this.ans = ans;
	}
	public Boolean getIsCrt() {
		return isCrt;
	}
	public void setIsCrt(Boolean isCrt) {
		this.isCrt = isCrt;
	}

	public Boolean getIsLife() {
		return isLife;
	}

	public void setIsLife(Boolean isLife) {
		this.isLife = isLife;
	}

	public Double getaRwds() {
		if(aRwds == null) {
			aRwds=0.0;
		}
		return aRwds;
	}

	public void setaRwds(Double aRwds) {
		this.aRwds = aRwds;
	}

	public Double getCuRwds() {
		if(cuRwds == null) {
			cuRwds = 0.0;
		}
		return cuRwds;
	}

	public void setCuRwds(Double cuRwds) {
		this.cuRwds = cuRwds;
	}

	public Integer getCuLife() {
		if(cuLife == null) {
			cuLife = 0;
		}
		return cuLife;
	}

	public void setCuLife(Integer cuLife) {
		this.cuLife = cuLife;
	}

	public Long getCrDate() {
		return crDate;
	}

	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	public Long getUpDate() {
		return upDate;
	}

	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}

	public String getFbCallbackTime() {
		if(fbCallbackTime == null) {
			fbCallbackTime = "";
		}
		return fbCallbackTime;
	}

	public void setFbCallbackTime(String fbCallbackTime) {
		this.fbCallbackTime = fbCallbackTime;
	}

	public Integer getRetryCount() {
		if(retryCount == null) {
			retryCount = 0;
		}
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public String getAnswerTime() {
		if(answerTime == null) {
			answerTime = "";
		}
		return answerTime;
	}

	public void setAnswerTime(String answerTime) {
		this.answerTime = answerTime;
	}

	public Boolean getIsAutoCreated() {
		if(isAutoCreated == null) {
			isAutoCreated = false;
		}
		return isAutoCreated;
	}

	public void setIsAutoCreated(Boolean isAutoCreated) {
		this.isAutoCreated = isAutoCreated;
	}

	

}
