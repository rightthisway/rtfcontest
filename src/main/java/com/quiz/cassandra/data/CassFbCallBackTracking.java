package com.quiz.cassandra.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CassFbCallBackTracking")
public class CassFbCallBackTracking implements Serializable{

	
	private Integer id;
	private Integer coId;
	private Integer cuId;
	private Integer qNo;
	private String platform;
	private String ipAddr;
	private Long createDate;
	private String dModel;
	private String osVer;
	private String cbType;
	private String message;
	

	public CassFbCallBackTracking() {
	}
	
	public CassFbCallBackTracking(Integer coId, Integer cuId, Integer qNo, String platform, String ipAddr,
			Long createDate, String dModel, String osVer, String cbType, String message) {
		this.coId = coId;
		this.cuId = cuId;
		this.qNo = qNo;
		this.platform = platform;
		this.ipAddr = ipAddr;
		this.createDate = createDate;
		this.dModel = dModel;
		this.osVer = osVer;
		this.cbType = cbType;
		this.message = message;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCoId() {
		return coId;
	}

	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public Integer getqNo() {
		return qNo;
	}

	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}

	public String getPlatform() {
		if(platform == null) {
			platform = "";
		}
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getIpAddr() {
		if(ipAddr == null) {
			ipAddr = "";
		}
		return ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public Long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Long createDate) {
		this.createDate = createDate;
	}

	public String getdModel() {
		if(dModel == null) {
			dModel = "";
		}
		return dModel;
	}

	public void setdModel(String dModel) {
		this.dModel = dModel;
	}

	public String getOsVer() {
		if(osVer == null) {
			osVer = "";
		}
		return osVer;
	}

	public void setOsVer(String osVer) {
		this.osVer = osVer;
	}

	public String getCbType() {
		if(cbType == null) {
			cbType = "";
		}
		return cbType;
	}

	public void setCbType(String cbType) {
		this.cbType = cbType;
	}

	public String getMessage() {
		if(message == null) {
			message = "";
		}
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

	
}
