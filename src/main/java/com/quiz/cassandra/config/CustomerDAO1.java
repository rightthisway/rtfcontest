package com.quiz.cassandra.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

 
public class CustomerDAO1 {
	

 
	public void saveCustomer(Customer obj) {
		CassandraConnector.getSession().execute(
				"INSERT INTO customer (customer_id, user_id, email, phone, no_of_lives) VALUES (?, ?, ?, ?, ?)",
				obj.getId(), obj.getUserId(), obj.getEmail(), obj.getPhone(), obj.getNoOfLives());
	}

 
	public Customer queryByCustomerId(Integer customerId){
	   final ResultSet results = CassandraConnector.getSession().execute(
	      "SELECT * from customer WHERE customer_id = ? ", customerId);
	    Customer customer = null;
	   if(results != null) {
		   Row row = results.one();
		   if(row != null) {
			   customer =  new Customer(
			    		  row.getInt("customer_id"),
			    		  row.getString("user_id"),
			    		  row.getString("email"),
			    		  row.getString("phone"),
			    		  row.getInt("no_of_lives"));
		   }
	   }
	   return customer;
	}
	
	public List<Customer> getAll(){
		   ResultSet results = CassandraConnector.getSession().execute("SELECT * from customer");
		   List<Customer> customerList = new ArrayList<Customer>();
	
		   if(results != null) {
			   for (Row row : results) {
				   customerList.add(new Customer(
				    		  row.getInt("customer_id"),
				    		  row.getString("user_id"),
				    		  row.getString("email"),
				    		  row.getString("phone"),
				    		  row.getInt("no_of_lives")));
			   }
		   }
		   return customerList;
	}
	
	public void deleteCustomer(Integer customerId){
	   final String deleteString = "DELETE FROM customer WHERE customer_id = ?";
	   CassandraConnector.getSession().execute(deleteString,customerId);
	}
}