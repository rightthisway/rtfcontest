package com.quiz.cassandra.config;



import java.util.Date;
import java.util.List;

import com.quiz.cassandra.dao.implementation.CassCustomerDAO;
import com.quiz.cassandra.dao.implementation.ContestParticipantsDAO;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.ContestParticipants;

public class Test {
	
	 
	public static void main(String[] args) throws Exception {
		
		CassandraConnector.connect();
		
		CassCustomerDAO customerDAO = new CassCustomerDAO();
		
		CassCustomer obj = null; // new CassCustomer(1245, "NATHAN", "ulaganathantoall@gmail.com", "9884512528", 10,100123.25,"12456.jpg",true,10 , 
				// new Integer("50"),10,10,true 
	    		///);
		customerDAO.saveCustomer(obj);
		
		//CustContAnswers ans = new CustContAnswers(UUID.randomUUID(), "123", "262", "2432", "1", "A", true, false, 0.05, 0.05, 0, new Date(), new Date());
		CassCustomer obj1 = new CassCustomerDAO().getCustomerById(1245);
		System.out.println(obj1.getuId()+" : "+obj1.getcReward()+" : "+obj1.getIsOtp());
		
		/*List<CustContAnswers> list1 = new CustContAnswersDAO().getData();
		for (CustContAnswers ans : list1) {
			System.out.println("1:"+ans.getCuId()+" : "+ans.getCoId()+" : "+ans.getqId()+" : "+ans.getqNo()+" : "+ans.getId());
			//new CustContAnswersDAO().delete(ans);	
		}*/
	
		Date now = new Date();
		ContestParticipants participant = new ContestParticipants();
		participant.setCoId(1);
		participant.setCuId(1);
		participant.setPfm("TEST");
		participant.setIpAdd("12345");
		participant.setJnDate(now.getTime());
		participant.setStatus("ACTIVE");
		new ContestParticipantsDAO().saveForJoin(participant);
		participant.setIpAdd("1111");
		new ContestParticipantsDAO().saveForJoin(participant);
		
		List<ContestParticipants> list = new ContestParticipantsDAO().getAllParticipantsByContestId(1);
		for (ContestParticipants cpObj : list) {
			System.out.println("1:"+cpObj.getCuId()+" : "+cpObj.getCoId()+" : "+cpObj.getJnDate()+" : "+new Date(cpObj.getJnDate())+" : "+now+" : "+now.getTime());
		}
		//CassandraDAORegistry.getContestParticipantsDAO().save(participant);
		//new ContestParticipantsDAO().getAllParticipantsByContestId(1)
		
		/* List<CustContAnswers> list = new CustContAnswersDAO().getAllCustContAnswersByQuestId(3422);
		 for (CustContAnswers ans : list) {
			System.out.println("2:"+ans.getCuId()+" : "+ans.getCoId()+" : "+ans.getqId()+" : "+ans.getqNo()+" : "+ans.getId());
		}*/
	}

	
}