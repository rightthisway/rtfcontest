package com.quiz.cassandra.config;

import static java.lang.System.out;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import com.datastax.driver.core.AuthProvider;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.CodecRegistry;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.LoadBalancingPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.quiz.cassandra.dao.implementation.CassCustomerDAO;
import com.quiz.cassandra.dao.implementation.CassRtfApiTrackingDAO;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.DashboardInfo;
 
public class CassandraConnector  {
	
	private static Cluster cluster;
	private static Session session;
	private static  String node;//="34.202.149.190";// "10.0.1.236";
	private static  int port;// = 9042;
	private static  String keySpace;// = "rtfquizmasterks";
	private static  String userName;// = "rtfquizmasterks";
	private static  String password;// = "rtfquizmasterks";
	private static  String nodeTwo;// ="10.0.1.172" ;// "10.0.1.236" "52.45.225.146";
	

	/*private static  String node ="54.172.34.15";// "10.0.1.236";
	private static  String nodeTwo ="52.45.225.146";// "10.0.1.236";
	private static  int port = 9042;
	private static  String keySpace = "rtfquizmasterks";
	private static  String userName = "rtfquizmasterks";
	private static  String password = "rtfquizmasterks";
	*/
private static Boolean isSessionCreated= false;
	
	/*
		Production details:
		HOSTED on : 100.24.150.226:9042, 10.0.1.171:9042
		Userid: cassandra
		Password: cassandra
		Keyspace: rtfquizmasterks
		Cluster name: RTFQuizMasterLinuxProdCluster
	*/
	
	/*
	 	Sandbox Details:
		HOSTED on : 34.234.156.58:9042, 10.0.1.236:9042
		Userid: cassandra
		Password: cassandra
		Keyspace: rtfquizmasterks
		Cluster name: RTFQuizMasterLinuxCluster
	*/

	 
	//@Override
	//public void afterPropertiesSet() throws Exception {
	public static void loadApplicationValues() throws Exception {
		System.out.println("Cassandra After Property Set invoked : "+ new Date());
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			node =resourceBundle.getString("rtf.cassandra.db.host.ip");
			port = Integer.parseInt(resourceBundle.getString("rtf.cassandra.db.port").trim());
			keySpace =resourceBundle.getString("rtf.cassandra.db.keySpace");
			userName = resourceBundle.getString("rtf.cassandra.db.user.name");
			password = resourceBundle.getString("rtf.cassandra.db.password");
			
			connect();
			//connectWithClusters();
			
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		
	}
	
	static LoadBalancingPolicy loadBalancingPolicy;
	static  PoolingOptions poolingOptions;
	static Boolean isWithPooling = false;
			
	public  static void connect() throws Exception {
		
		/*CodecRegistry codecRegistry = new CodecRegistry();
		codecRegistry.register(new DateCodec(TypeCodec.date(), Date.class));*/
		
		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions
		  .setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
		  .setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);
		
		tempPoolingOptions
	    .setConnectionsPerHost(HostDistance.LOCAL,  4, 10)
	    .setConnectionsPerHost(HostDistance.REMOTE, 2, 4);
		 
		System.out.println("node: "+node+", port:"+port);
		
		cluster = Cluster.builder().addContactPoint(node).withPort(port)
				.withCredentials(userName, password).withPoolingOptions(tempPoolingOptions)
				//.withCodecRegistry(codecRegistry)
				.build();
		final Metadata metadata = cluster.getMetadata();
		out.printf("Connected to cluster: %s\n", metadata.getClusterName());
		for (final Host host : metadata.getAllHosts()) {
			out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect(keySpace);
		
		ProtocolVersion myCurrentVersion = cluster.getConfiguration()
			    .getProtocolOptions()
			    .getProtocolVersion();
		
		System.out.println("node: "+node+", port:"+port+" :keySpace: "+keySpace+" : sess: "+session+": protocol version : "+myCurrentVersion);
		
		
		
		loadBalancingPolicy =
			    cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions =
				cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}
 
	public  static void connectWithClusters() throws Exception {
		
		/*CodecRegistry codecRegistry = new CodecRegistry();
		codecRegistry.register(new DateCodec(TypeCodec.date(), Date.class));*/
		
		System.out.println("Cluster connect node: "+node+" node: "+nodeTwo+", port:"+port);
		
		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions
						.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
						.setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);
		
		tempPoolingOptions
	    .setConnectionsPerHost(HostDistance.LOCAL,  4, 10)
	    .setConnectionsPerHost(HostDistance.REMOTE, 2, 4);
		
		/*LoadBalancingPolicy lbp = new TokenAwarePolicy(
			       DCAwareRoundRobinPolicy.builder()
			       .withLocalDc("myDC")
			       .build()
			);*/
		LoadBalancingPolicy lbp = DCAwareRoundRobinPolicy.builder().withLocalDc("myDC").build();
		
		/*SocketOptions so = new SocketOptions()
			           .setReadTimeoutMillis(3000)
			           .setConnectTimeoutMillis(3000);*/
		QueryOptions qo = new QueryOptions().setConsistencyLevel(ConsistencyLevel.ALL);

		AuthProvider authProvider = new PlainTextAuthProvider(userName, password);
		cluster = Cluster.builder()
			       .addContactPoints(node,nodeTwo)
			       .withPort(port)
			       .withAuthProvider(authProvider)
			       //.withLoadBalancingPolicy(lbp)
			       //.withSocketOptions(so)
			       .withPoolingOptions(poolingOptions)
			       .withQueryOptions(qo)
			       .build();
			//Session session = cluster.connect();

		/*cluster = Cluster.builder().addContactPoint(node).withPort(port)
				//.withCredentials(userName, password)
				.withAuthProvider(authProvider)
				.withPoolingOptions(tempPoolingOptions)
				.withLoadBalancingPolicy(lbp)
				//.withCodecRegistry(codecRegistry)
				.build();*/
		
		final Metadata metadata = cluster.getMetadata();
		out.printf("Connected to cluster: %s\n", metadata.getClusterName());
		for (final Host host : metadata.getAllHosts()) {
			out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect(keySpace);
		
		ProtocolVersion myCurrentVersion = cluster.getConfiguration()
			    .getProtocolOptions()
			    .getProtocolVersion();
		
		System.out.println("node: "+node+", port:"+port+" :keySpace: "+keySpace+" : sess: "+session+": protocol version : "+myCurrentVersion);
		
		
		
		loadBalancingPolicy =
			    cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions =
				cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}
		
	public static void getCassandraState() throws Exception {
		
		if(isWithPooling) {
			 Session.State state = session.getState();
		        for (Host host : state.getConnectedHosts()) {
		            HostDistance distance = loadBalancingPolicy.distance(host);
		            int connections = state.getOpenConnections(host);
		            int inFlightQueries = state.getInFlightQueries(host);
		            System.out.printf("CASS STAT %s connections=%d, current load=%d, max load=%d  dist=%s, date:%s%n",
		                host, connections, inFlightQueries,
		                connections *
		                poolingOptions.getMaxRequestsPerConnection(distance),distance,new Date());
		            //System.out.println("distance : "+distance);
		        }
		}
	}

	public static Session getSession() {
		try {
			if(!isSessionCreated) {
				connect();
				isSessionCreated = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return session;
	}
	
	 /*public static Session getDataStaxSession() {
	        synchronized (DataStaxPlugin.class) {
	            try {
	                if (instance == null) {
	                    instance = new DataStaxPlugin();
	                    instance.connect();
	                }

	                return session;
	            } finally {
	            }
	        }
	    }*/

	 
	public void close() {
		cluster.close();
	}

	public static String getKeySpace() {
		return keySpace;
	}
	public static void main(String[] args) throws Exception {
		//connectWithClusters();
		connect();
		getCassandraState();
		
		CassRtfApiTrackingDAO dao = new CassRtfApiTrackingDAO();
		//dao.getAll();
		
		getCassandraState();
		
		
		CassCustomerDAO custDao = new CassCustomerDAO();
		CassCustomer cassCus = custDao.getCustomerById(415336);
		System.out.println(""+cassCus.getcReward()+":"+cassCus.getId()+":"+cassCus.getEmail()+":"+cassCus.getRtfPoints());
		//cassCus.setcRewardDbl(5000.00);
		cassCus.setRtfPoints(100);
		custDao.saveCustomer(cassCus);
		cassCus = custDao.getCustomerById(415336);
		System.out.println(""+cassCus.getcReward()+":"+cassCus.getId()+":"+cassCus.getEmail()+":"+cassCus.getRtfPoints());

		getCassandraState();
		
	}
	
}