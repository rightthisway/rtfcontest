package com.quiz.cassandra.config;

public class Customer {
	
	private Integer id;
	private String userId;
	private String email;
	private String phone;
	private Integer noOfLives;
	
	
	public Customer(Integer id, String userId, String email, String phone, Integer noOfLives) {
		super();
		this.id = id;
		this.userId = userId;
		this.email = email;
		this.phone = phone;
		this.noOfLives = noOfLives;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getNoOfLives() {
		return noOfLives;
	}
	public void setNoOfLives(Integer noOfLives) {
		this.noOfLives = noOfLives;
	}

}
