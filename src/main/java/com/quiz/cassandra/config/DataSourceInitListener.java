package com.quiz.cassandra.config;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import com.zonesws.webservices.utils.DataBaseConnectionsPooled;
import com.zonesws.webservices.utils.DatabaseConnections;

public class DataSourceInitListener implements ServletContextListener {

	 private static DataSource rtfdatasource = null;
	  private static DataSource zonesdatasource = null;
	  private static DataSource rtfEcommdatasource = null;

    public void contextInitialized(ServletContextEvent sce) {
       /* Map<String, String> dummyCache = new HashMap<String, String>();
        dummyCache.put("greeting", "Hello Word!");

        ServletContext context = sce.getServletContext();
        context.setAttribute("dummyCache", dummyCache);*/
        System.out.println("Test Initi called in load on startup");
        try
        {
          InitialContext initialContext = new InitialContext();
    			/*
    			 * if ( initialContext == null ) {
    			 * 
    			 * String message =
    			 * "There was no InitialContext in RTFConfigInitializerServlet. ..... problems."
    			 * ; System.err.println("*** " + message); throw new Exception(message); }
    			 */
         
          rtfdatasource = (DataSource) initialContext.lookup( "java:/comp/env/jdbc/rtfdb" );
          zonesdatasource =  (DataSource) initialContext.lookup( "java:/comp/env/jdbc/zonesdb" );
          rtfEcommdatasource =  (DataSource) initialContext.lookup( "java:/comp/env/jdbc/rtfecomdb" );
          if ( rtfdatasource == null  || zonesdatasource == null || rtfEcommdatasource == null)
          {
            String message = "Could not find our DataSource in RTFConfigInitializerServlet.... problems.";
            System.err.println("*** " + message);
            throw new Exception(message);
          }
         // context.setAttribute("rtfdatasource", rtfdatasource);
         // context.setAttribute("zonesdatasource", zonesdatasource); 
          DataBaseConnectionsPooled conPool = new DataBaseConnectionsPooled();
          //conPool.setRtfConnection(rtfdatasource.getConnection());
          
          //conPool.setZonesConnection(zonesdatasource.getConnection())
         // context.setAttribute("rtfdatasourceConn", rtfdatasource.getConnection());
          //context.setAttribute("zonesdatasourceConn", zonesdatasource.getConnection()); 
          
          DatabaseConnections.setRtfDatasource(rtfdatasource);
          DatabaseConnections.setZonesDatasource(zonesdatasource);
          DatabaseConnections.setRtfEcomDataSource(rtfEcommdatasource);
         
          System.out.println("rtfdatasource added in global  context on startup");
          System.out.println("[rtfdatasource]" + rtfdatasource.getConnection().getCatalog()  + " [zonesdatasource]" + zonesdatasource.getConnection().getCatalog()+ " [rtfEcommdatasource]" + rtfEcommdatasource.getConnection().getCatalog());
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        	System.err.println("** ** Serious Issue .. cannot Proceed with htis Configutration of DB ");
        
        }
        
        
        
        
    }

    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        context.removeAttribute("dummyCache");
    }

}