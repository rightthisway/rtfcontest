package com.quiz.cassandra.list;

import java.util.List;

public class CommentsDTO {
	
	Integer srcId;
	List<Comments> cLst;
	
	private CassError err; 
	private String msg;
	private Integer sts;
	private Integer cuId;
	
	
	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	

	public List<Comments> getcLst() {
		return cLst;
	}

	public void setcLst(List<Comments> cLst) {
		this.cLst = cLst;
	}

	public Integer getSrcId() {
		return srcId;
	}

	public void setSrcId(Integer srcId) {
		this.srcId = srcId;
	}
	
	//CommentsInfo commentsInfo;

}
