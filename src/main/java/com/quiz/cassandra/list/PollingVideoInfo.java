package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PollingVideoInfo")
public class PollingVideoInfo {

	private Integer vidId;	
	private Integer sts;
	private String nxtCTxt;
	private CassError err; 
	private String msg;
	private String vUrl;

	private String deviceIp;
	private String pfm;
	private Integer cuId;
	private List<String> vUrlLst;
	private String wTy;
	private Integer isVid;
	private String catId;
	private List<String> vCatList;
	private List myVidInfoList;
	private String tUrl;
	private String title;
	private String description;
	private String splogo;
	private Integer lkCnt=0;
	private Integer vwCnt=0;
	private Integer isLk = 0;
	private List<String> vUrlListRel1;
	private List<String> vCatListRel1;
	private RewardsInfo rwdInfo;
	
	public RewardsInfo getRwdInfo() {
		return rwdInfo;
	}
	public void setRwdInfo(RewardsInfo rwdInfo) {
		this.rwdInfo = rwdInfo;
	}
	public List<String> getvCatListRel1() {
		return vCatListRel1;
	}
	public void setvCatListRel1(List<String> vCatListRel1) {
		this.vCatListRel1 = vCatListRel1;
	}
	public List<String> getvUrlListRel1() {
		return vUrlListRel1;
	}
	public void setvUrlListRel1(List<String> vUrlListRel1) {
		this.vUrlListRel1 = vUrlListRel1;
	}
	public Integer getLkCnt() {
		if(lkCnt == null || lkCnt < 0 ) lkCnt = 0;
		return lkCnt;
	}
	public void setLkCnt(Integer lkCnt) {
		this.lkCnt = lkCnt;
	}
	public Integer getVwCnt() {
		if(vwCnt == null || vwCnt < 0) vwCnt = 0;
		return vwCnt;
	}
	public void setVwCnt(Integer vwCnt) {
		this.vwCnt = vwCnt;
	}
	public Integer getIsLk() {
		if(isLk == null) isLk = 0;
		return isLk;
	}
	public void setIsLk(Integer isLk) {
		this.isLk = isLk;
	}
	
	public String getSplogo() {
		return splogo;
	}
	public void setSplogo(String splogo) {
		this.splogo = splogo;
	}
	public Integer getVidId() {
		return vidId;
	}
	public void setVidId(Integer vidId) {
		this.vidId = vidId;
	}
	public String getDescription() {
		if(description == null) description = "";
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		if(title ==null || "null".equals(title) )title = "";
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String gettUrl() {
		return tUrl;
	}
	public void settUrl(String tUrl) {
		this.tUrl = tUrl;
	}
	public List getMyVidInfoList() {
		return myVidInfoList;
	}
	public void setMyVidInfoList(List myVidInfoList) {
		this.myVidInfoList = myVidInfoList;
	}
	String appver;
	
	public Integer getIsVid() {
		return isVid;
	}
	public void setIsVid(Integer isVid) {
		this.isVid = isVid;
	}
	public String getwTy() {
		return wTy;
	}
	public void setwTy(String wTy) {
		this.wTy = wTy;
	}


	
	public List<String> getvUrlLst() {
		return vUrlLst;
	}
	public void setvUrlLst(List<String> vUrlLst) {
		this.vUrlLst = vUrlLst;
	}

	public Integer getCuId() {
		return cuId;
	}


	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	
	
	public String getNxtCTxt() {
		return nxtCTxt;
	}


	public void setNxtCTxt(String nxtCTxt) {
		this.nxtCTxt = nxtCTxt;
	}


	public Integer getSts() {
		return sts;
	}


	public void setSts(Integer sts) {
		this.sts = sts;
	}


	public CassError getErr() {
		return err;
	}


	public void setErr(CassError err) {
		this.err = err;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}


	public String getvUrl() {
		return vUrl;
	}


	public void setvUrl(String vUrl) {
		this.vUrl = vUrl;
	}
	
	public String getCatId() {
		return catId;
	}
	public void setCatId(String catId) {
		this.catId = catId;
	}
	public List<String> getvCatList() {
		return vCatList;
	}
	public void setvCatList(List<String> vCatList) {
		this.vCatList = vCatList;
	}
	public String getAppver() {
		return appver;
	}
	public void setAppver(String appver) {
		this.appver = appver;
	}

	

		
}