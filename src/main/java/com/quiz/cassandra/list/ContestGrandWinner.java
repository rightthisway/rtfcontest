package com.quiz.cassandra.list;

import java.io.Serializable;
import java.text.SimpleDateFormat;
 
public class ContestGrandWinner implements Serializable{

	private Integer id;
	private Integer contestId;
	private Integer customerId;
	private Integer rewardTickets;
	private String expiryDateStr;
	private String contestName;
	private SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	public final Integer getId() {
		return id;
	}
	public final void setId(Integer id) {
		this.id = id;
	}
	public final Integer getContestId() {
		return contestId;
	}
	public final void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public final Integer getCustomerId() {
		return customerId;
	}
	public final void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public final Integer getRewardTickets() {
		return rewardTickets;
	}
	public final void setRewardTickets(Integer rewardTickets) {
		this.rewardTickets = rewardTickets;
	}
	public final String getExpiryDateStr() {
		return expiryDateStr;
	}
	public final void setExpiryDateStr(String expiryDateStr) {
		this.expiryDateStr = expiryDateStr;
	}
	public final String getContestName() {
		return contestName;
	}
	public final void setContestName(String contestName) {
		this.contestName = contestName;
	}
	public final SimpleDateFormat getDateTimeFormat1() {
		return dateTimeFormat1;
	}
	public final void setDateTimeFormat1(SimpleDateFormat dateTimeFormat1) {
		this.dateTimeFormat1 = dateTimeFormat1;
	}
}
