package com.quiz.cassandra.list;

import java.util.List;

import com.quiz.cassandra.data.CassContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ContSummaryInfo")
public class ContSummaryInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private Integer wCount;
	List<CassContestWinners> winners;
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getwCount() {
		if(wCount == null) {
			wCount = 0;
		}
		return wCount;
	}
	public void setwCount(Integer wCount) {
		this.wCount = wCount;
	}
	
	public List<CassContestWinners> getWinners() {
		return winners;
	}
	public void setWinners(List<CassContestWinners> winners) {
		this.winners = winners;
	}
	
	
		
}
