package com.quiz.cassandra.list;

import com.quiz.cassandra.data.CassCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("DbdInfoRoot")
public class DashboardInfoRoot {
	
	private DashboardInfo dashboardInfo;

	public DashboardInfo getDashboardInfo() {
		return dashboardInfo;
	}

	public void setDashboardInfo(DashboardInfo dashboardInfo) {
		this.dashboardInfo = dashboardInfo;
	}
	
	
	

}
