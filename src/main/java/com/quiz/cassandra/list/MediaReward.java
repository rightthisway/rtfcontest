package com.quiz.cassandra.list;

public class MediaReward {
	
	private Integer cuId;
	private Integer mId;
	private String rwdCode;
	private Integer rwdQty;
	private Double rwdDolars;
	private String rwdText;
	private String rewardSourceType;
	private Integer sts;
 	private String nxtCTxt;
	private String msg;
	private CassError err;
	
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getmId() {
		return mId;
	}
	public void setmId(Integer mId) {
		this.mId = mId;
	}
	public String getRwdCode() {
		return rwdCode;
	}
	public void setRwdCode(String rwdCode) {
		this.rwdCode = rwdCode;
	}
	public Integer getRwdQty() {
		return rwdQty;
	}
	public void setRwdQty(Integer rwdQty) {
		this.rwdQty = rwdQty;
	}
	public Double getRwdDolars() {
		return rwdDolars;
	}
	public void setRwdDolars(Double rwdDolars) {
		this.rwdDolars = rwdDolars;
	}
	public String getRwdText() {
		return rwdText;
	}
	public void setRwdText(String rwdText) {
		this.rwdText = rwdText;
	}
	public String getRewardSourceType() {
		return rewardSourceType;
	}
	public void setRewardSourceType(String rewardSourceType) {
		this.rewardSourceType = rewardSourceType;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public String getNxtCTxt() {
		return nxtCTxt;
	}
	public void setNxtCTxt(String nxtCTxt) {
		this.nxtCTxt = nxtCTxt;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	
	
	

}
