package com.quiz.cassandra.list;

public class CustDailyRewardsVO {
	private Integer custId;
	private Integer totalAction;
	private Integer rewardedAction;
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Integer getTotalAction() {
		return totalAction;
	}
	public void setTotalAction(Integer totalAction) {
		this.totalAction = totalAction;
	}
	public Integer getRewardedAction() {
		return rewardedAction;
	}
	public void setRewardedAction(Integer rewardedAction) {
		this.rewardedAction = rewardedAction;
	}

}
