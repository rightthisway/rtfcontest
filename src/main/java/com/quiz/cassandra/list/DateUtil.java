package com.quiz.cassandra.list;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {

	 
	public static Date getLocalDate(Date utcDate) {
		if(null == utcDate) {
			return null;
		}
		DateFormat estFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		estFormat.setTimeZone(TimeZone.getTimeZone("EST"));
		String estDateStr = estFormat.format(utcDate);
		Date estDate = null;
		try {
			estDate = estFormat.parse(estDateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return estDate;
	}
	
	public static void main(String[] args) throws ParseException {
		DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

		Date date = utcFormat.parse("2018-12-10T06:23:02.038Z");

		DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		pstFormat.setTimeZone(TimeZone.getTimeZone("EST"));

		System.out.println(pstFormat.format(date));
		
	}
}
