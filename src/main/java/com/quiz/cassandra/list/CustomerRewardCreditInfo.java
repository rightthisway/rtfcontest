package com.quiz.cassandra.list;

public class CustomerRewardCreditInfo {
	
	
	private Integer custId;
	private String rewardType;
	private Double rewardQty;
	private String enumTrackerValue;
	private Integer refId;
	private String sourceType;
	private String description;
		
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	public Double getRewardQty() {
		return rewardQty;
	}
	public void setRewardQty(Double rewardQty) {
		this.rewardQty = rewardQty;
	}
	public String getEnumTrackerValue() {
		return enumTrackerValue;
	}
	public void setEnumTrackerValue(String enumTrackerValue) {
		this.enumTrackerValue = enumTrackerValue;
	}
	public Integer getRefId() {
		return refId;
	}
	public void setRefId(Integer refId) {
		this.refId = refId;
	}
	public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
