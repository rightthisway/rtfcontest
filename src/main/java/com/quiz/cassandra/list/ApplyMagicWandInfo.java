package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ApplyMagicWandInfo")
public class ApplyMagicWandInfo {
	 	private Integer sts;
	 	private String nxtCTxt;
		private String msg;
		private CassError err;
		private Integer cuId;
		private Integer coId;
		
		public Integer getCuId() {
			return cuId;
		}
		public void setCuId(Integer cuId) {
			this.cuId = cuId;
		}
		public Integer getCoId() {
			return coId;
		}
		public void setCoId(Integer coId) {
			this.coId = coId;
		}
		
		public Integer getSts() {
			return sts;
		}
		public void setSts(Integer sts) {
			this.sts = sts;
		}
		public String getNxtCTxt() {
			return nxtCTxt;
		}
		public void setNxtCTxt(String nxtCTxt) {
			this.nxtCTxt = nxtCTxt;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public CassError getErr() {
			return err;
		}
		public void setErr(CassError err) {
			this.err = err;
		}
		
	
	
}
