package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Error;

@XStreamAlias("QuizGenericResponse")
public class QuizGenericResponse {

	private Error error;
	private String message;
	private Integer status;
	private Integer orderId;
	private Boolean isShippingAddress;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Boolean getIsShippingAddress() {
		return isShippingAddress;
	}
	public void setIsShippingAddress(Boolean isShippingAddress) {
		this.isShippingAddress = isShippingAddress;
	}
	
	
	
	
}
