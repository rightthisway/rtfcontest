package com.quiz.cassandra.list;

import java.util.List;

public class FanClubCommentsDTO {
	
	Integer fcId;
	List<Comments> cLst;	
	private CassError err; 
	private String msg;
	private Integer sts;
	private Integer cuId;
	private Integer cmtId;
	private String cmtTxt;
	private String vidUrl;
	private String tUrl;
	private String imgUrl;
	private String status;	
	
	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	

	public List<Comments> getcLst() {
		return cLst;
	}

	public void setcLst(List<Comments> cLst) {
		this.cLst = cLst;
	}

	public Integer getFcId() {
		return fcId;
	}

	public void setFcId(Integer fcId) {
		this.fcId = fcId;
	}

	public Integer getCmtId() {
		return cmtId;
	}

	public void setCmtId(Integer cmtId) {
		this.cmtId = cmtId;
	}

	public String getCmtTxt() {
		return cmtTxt;
	}

	public void setCmtTxt(String cmtTxt) {
		this.cmtTxt = cmtTxt;
	}

	public String getVidUrl() {
		return vidUrl;
	}

	public void setVidUrl(String vidUrl) {
		this.vidUrl = vidUrl;
	}

	public String gettUrl() {
		return tUrl;
	}

	public void settUrl(String tUrl) {
		this.tUrl = tUrl;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	//CommentsInfo commentsInfo;

}
