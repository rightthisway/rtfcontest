package com.quiz.cassandra.list;

import java.util.List;

import com.quiz.cassandra.data.CassContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("HallOfFameInfo")
public class HallOfFameInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private Integer wCount;
	HallOfFameDtls custHof;
	List<HallOfFameDtls> list;
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getwCount() {
		if(wCount == null) {
			wCount = 0;
		}
		return wCount;
	}
	public void setwCount(Integer wCount) {
		this.wCount = wCount;
	}
	public List<HallOfFameDtls> getList() {
		return list;
	}
	public void setList(List<HallOfFameDtls> list) {
		this.list = list;
	}
	public HallOfFameDtls getCustHof() {
		return custHof;
	}
	public void setCustHof(HallOfFameDtls custHof) {
		this.custHof = custHof;
	}
	
	
	
		
}
