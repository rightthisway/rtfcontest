package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ContApplyLifeLineInfo")
public class ContApplyLifeInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private Boolean isLifeUsed= false;
	
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Boolean getIsLifeUsed() {
		return isLifeUsed;
	}
	public void setIsLifeUsed(Boolean isLifeUsed) {
		this.isLifeUsed = isLifeUsed;
	}
	

	
}
