package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PollingQuestionInfo")
public class PollingQuestionInfo {
	
	private Integer id;
	private Integer sts;
	private CassError err; 
	private String msg;
	private String qTxt;
	private Integer qId;
	private List<String> ansOpts;
	private Integer pCtId;
	private Integer erIdx;
	private String nxtCTxt;
	private Integer isErsr;
	private Integer pCatId;
	private Integer custId;
	private String answer;
	private String pollType;
	private String crtAnsTxt;
	private Integer maxQuestions;		
	private String spimg;
	private Integer oQId;
	
	
	
	public Integer getoQId() {
		return oQId;
	}
	public void setoQId(Integer oQId) {
		this.oQId = oQId;
	}
	public String getSpimg() {
		return spimg;
	}
	public void setSpimg(String spimg) {
		this.spimg = spimg;
	}
	public Integer getMaxQuestions() {
		return maxQuestions;
	}
	public void setMaxQuestions(Integer maxQuestions) {
		this.maxQuestions = maxQuestions;
	}
	public String getCrtAnsTxt() {
		return crtAnsTxt;
	}
	public void setCrtAnsTxt(String crtAnsTxt) {
		this.crtAnsTxt = crtAnsTxt;
	}
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getqTxt() {
		return qTxt;
	}
	public void setqTxt(String qTxt) {
		this.qTxt = qTxt;
	}
	public Integer getqId() {
		return qId;
	}
	public void setqId(Integer qId) {
		this.qId = qId;
	}
	public List<String> getAnsOpts() {
		return ansOpts;
	}
	public void setAnsOpts(List<String> ansOpts) {
		this.ansOpts = ansOpts;
	}
	public Integer getpCtId() {
		return pCtId;
	}
	public void setpCtId(Integer pCtId) {
		this.pCtId = pCtId;
	}
	public Integer getErIdx() {
		return erIdx;
	}
	public void setErIdx(Integer erIdx) {
		this.erIdx = erIdx;
	}
	public String getNxtCTxt() {
		return nxtCTxt;
	}
	public void setNxtCTxt(String nxtCTxt) {
		this.nxtCTxt = nxtCTxt;
	}
	public Integer getIsErsr() {
		return isErsr;
	}
	public void setIsErsr(Integer isErsr) {
		this.isErsr = isErsr;
	}
	public Integer getpCatId() {
		return pCatId;
	}
	public void setpCatId(Integer pCatId) {
		this.pCatId = pCatId;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getPollType() {
		return pollType;
	}
	public void setPollType(String pollType) {
		this.pollType = pollType;
	}
	
}
