package com.quiz.cassandra.list;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PollingRedeemInfo")
public class PollingRedeemInfo {
	Integer cuId;
	@JsonIgnore
	Integer fmQty;
	@JsonIgnore
	Double toQty;
	@JsonIgnore
    String fmRwdTy;
	@JsonIgnore
    String toRwdTy;
    String sucsMsg;
    private Integer sts;
	private String nxtCTxt;
	private String msg;
	private CassError err;
	
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getFmQty() {
		return fmQty;
	}
	public void setFmQty(Integer fmQty) {
		this.fmQty = fmQty;
	}
	public Double getToQty() {
		return toQty;
	}
	public void setToQty(Double toQty) {
		this.toQty = toQty;
	}
	public String getFmRwdTy() {
		return fmRwdTy;
	}
	public void setFmRwdTy(String fmRwdTy) {
		this.fmRwdTy = fmRwdTy;
	}
	public String getToRwdTy() {
		return toRwdTy;
	}
	public void setToRwdTy(String toRwdTy) {
		this.toRwdTy = toRwdTy;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public String getNxtCTxt() {
		return nxtCTxt;
	}
	public void setNxtCTxt(String nxtCTxt) {
		this.nxtCTxt = nxtCTxt;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getSucsMsg() {
		return sucsMsg;
	}
	public void setSucsMsg(String sucsMsg) {
		this.sucsMsg = sucsMsg;
	}
	
	
	
	}