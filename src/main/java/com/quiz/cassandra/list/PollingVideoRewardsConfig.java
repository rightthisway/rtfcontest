package com.quiz.cassandra.list;

public class PollingVideoRewardsConfig {
	private Integer id;
	private Integer minSecs;
	private Integer minTimeBetnRewards;
	private Integer maxRwdsPerDay;
	private Integer maxLives;
	private Integer maxStars;
	private Integer maxMagicWand;
	private Boolean isRewardsEnabled;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getMinSecs() {
		return minSecs;
	}
	public void setMinSecs(Integer minSecs) {
		this.minSecs = minSecs;
	}
	public Integer getMinTimeBetnRewards() {
		return minTimeBetnRewards;
	}
	public void setMinTimeBetnRewards(Integer minTimeBetnRewards) {
		this.minTimeBetnRewards = minTimeBetnRewards;
	}
	public Integer getMaxRwdsPerDay() {
		return maxRwdsPerDay;
	}
	public void setMaxRwdsPerDay(Integer maxRwdsPerDay) {
		this.maxRwdsPerDay = maxRwdsPerDay;
	}
	public Integer getMaxLives() {
		return maxLives;
	}
	public void setMaxLives(Integer maxLives) {
		this.maxLives = maxLives;
	}
	public Integer getMaxStars() {
		return maxStars;
	}
	public void setMaxStars(Integer maxStars) {
		this.maxStars = maxStars;
	}
	public Integer getMaxMagicWand() {
		return maxMagicWand;
	}
	public void setMaxMagicWand(Integer maxMagicWand) {
		this.maxMagicWand = maxMagicWand;
	}
	public Boolean getIsRewardsEnabled() {
		return isRewardsEnabled;
	}
	public void setIsRewardsEnabled(Boolean isRewardsEnabled) {
		this.isRewardsEnabled = isRewardsEnabled;
	} 
	

}
