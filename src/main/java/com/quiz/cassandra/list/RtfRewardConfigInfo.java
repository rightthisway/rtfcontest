package com.quiz.cassandra.list;

public class RtfRewardConfigInfo {
	
	private String rewardType;
	private Integer sfStars;
	private Integer lives;
	private Integer magicWands;
	private Integer rtfPoints;
	private Double rwdDollars;
	private Integer minVidPlayTime;
	private Integer minRwdInterval;
	private Integer maxRwdPerDay;
	private Integer maxActionsPerDay;
	private Integer batchSizePerDay;
	
	public RtfRewardConfigInfo(){
		
	}
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	public Integer getSfStars() {
		return sfStars;
	}
	public void setSfStars(Integer sfStars) {
		this.sfStars = sfStars;
	}
	public Integer getLives() {
		return lives;
	}
	public void setLives(Integer lives) {
		this.lives = lives;
	}
	public Integer getMagicWands() {
		return magicWands;
	}
	public void setMagicWands(Integer magicWands) {
		this.magicWands = magicWands;
	}
	public Integer getRtfPoints() {
		return rtfPoints;
	}
	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}
	public Double getRwdDollars() {
		return rwdDollars;
	}
	public void setRwdDollars(Double rwdDollars) {
		this.rwdDollars = rwdDollars;
	}
	public Integer getMinVidPlayTime() {
		return minVidPlayTime;
	}
	public void setMinVidPlayTime(Integer minVidPlayTime) {
		this.minVidPlayTime = minVidPlayTime;
	}
	public Integer getMinRwdInterval() {
		return minRwdInterval;
	}
	public void setMinRwdInterval(Integer minRwdInterval) {
		this.minRwdInterval = minRwdInterval;
	}
	public Integer getMaxRwdPerDay() {
		return maxRwdPerDay;
	}
	public void setMaxRwdPerDay(Integer maxRwdPerDay) {
		this.maxRwdPerDay = maxRwdPerDay;
	}
	public Integer getMaxActionsPerDay() {
		return maxActionsPerDay;
	}
	public void setMaxActionsPerDay(Integer maxActionsPerDay) {
		this.maxActionsPerDay = maxActionsPerDay;
	}
	public Integer getBatchSizePerDay() {
		return batchSizePerDay;
	}
	public void setBatchSizePerDay(Integer batchSizePerDay) {
		this.batchSizePerDay = batchSizePerDay;
	}
	 

}
