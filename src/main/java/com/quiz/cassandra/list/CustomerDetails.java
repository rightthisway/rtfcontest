package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CustomerInfo")
public class CustomerDetails {
	private Integer status;
	private Error error; 
	private String message;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
