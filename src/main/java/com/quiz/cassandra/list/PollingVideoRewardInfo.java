package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PollingVideoRewardInfo")
public class PollingVideoRewardInfo {

	private Integer sts;
	private CassError err; 
	private String msg;	
	
	private String rewardTtxt;
	private String rwdType;
	private String imgUrl;
	private Integer rwdQty;
	private Integer gcId;
	private String showMsg;	
	private Integer cuId;		
	private String nxtCTxt;
	private String clientIp;
	private String awsVideoId;
	private String source;  // from fanclub or rtf tv  .. "FC" // "TV"
	private RewardsInfo rwdInfo;
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getRewardTtxt() {
		return rewardTtxt;
	}
	public void setRewardTtxt(String rewardTtxt) {
		this.rewardTtxt = rewardTtxt;
	}
	public String getRwdType() {
		return rwdType;
	}
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Integer getRwdQty() {
		return rwdQty;
	}
	public void setRwdQty(Integer rwdQty) {
		this.rwdQty = rwdQty;
	}
	public Integer getGcId() {
		return gcId;
	}
	public void setGcId(Integer gcId) {
		this.gcId = gcId;
	}
	public String getShowMsg() {
		return showMsg;
	}
	public void setShowMsg(String showMsg) {
		this.showMsg = showMsg;
	}
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public String getNxtCTxt() {
		return nxtCTxt;
	}
	public void setNxtCTxt(String nxtCTxt) {
		this.nxtCTxt = nxtCTxt;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getAwsVideoId() {
		if(null == awsVideoId) {
			awsVideoId = "";
		}
		return awsVideoId;
	}
	public void setAwsVideoId(String awsVideoId) {
		this.awsVideoId = awsVideoId;
	}
	public RewardsInfo getRwdInfo() {
		return rwdInfo;
	}
	public void setRwdInfo(RewardsInfo rwdInfo) {
		this.rwdInfo = rwdInfo;
	}
	 
	
	
	
}