package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PollingShowVideoInfo")
public class PollingShowVideoInfo {

	private Integer sts;
	private String nxtCTxt;
	private CassError err; 
	private String msg;
	private Integer isVid;
	private Integer cuId; 
	
	
	public Integer getIsVid() {
		return isVid;
	}


	public void setIsVid(Integer isVid) {
		this.isVid = isVid;
	}


	public Integer getCuId() {
		return cuId;
	}


	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
 
	
	public String getNxtCTxt() {
		if(null == nxtCTxt) {
			nxtCTxt = "";
		}
		return nxtCTxt;
	}


	public void setNxtCTxt(String nxtCTxt) {
		this.nxtCTxt = nxtCTxt;
	}


	public Integer getSts() {
		return sts;
	}


	public void setSts(Integer sts) {
		this.sts = sts;
	}


	public CassError getErr() {
		return err;
	}


	public void setErr(CassError err) {
		this.err = err;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}



		
}