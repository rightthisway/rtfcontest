package com.quiz.cassandra.list;

import java.util.Date;



public class CustomerDailyRewardLimits {
	
	private Integer customerId;
	private Integer totFcCrPo;
	private Integer totFcEv;
	private Integer totFcEv_lk;
	private Integer totFcPoLk;
	private Integer totFcVdUl;
	private Integer totVdVw;
	private Integer totFfo_vd_ul;
	private Integer tot_sh_vd;
	private Integer tot_vd_lk;
	private Integer tot_vd_ul;
	private Date rwd_date;
	private Integer rwdFc_cr_po;
	private Integer rwdFcEv;
	private Integer rwdFcEv_lk;
	private Integer rwdFc_po_lk;
	private Integer rwdFc_vd_ul;
	private Integer rwdFfo_vd_ul;
	private Integer rwd_sh_vd;
	private Integer rwd_vd_lk;
	private Integer rwd_vd_ul;
	private Integer rwd_vd_vw;
	private Date upd_dttm;
	private Date fcCrPostDate;
	private Date fcEvDate;
	private Date fcEvLkDate;
	private Date fcPostLikeDate;
	private Date fcVidUpldDate;
	private Date ffoVidUpldDate;
	private Date shVidDate;
	private Date vidLkDate;
	private Date vdVwDate;
	private Date currDate;
	
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getTotFcCrPo() {
		return totFcCrPo;
	}
	public void setTotFcCrPo(Integer totFcCrPo) {
		this.totFcCrPo = totFcCrPo;
	}
	public Integer getTotFcEv() {
		return totFcEv;
	}
	public void setTotFcEv(Integer totFcEv) {
		this.totFcEv = totFcEv;
	}
	public Integer getTotFcEv_lk() {
		return totFcEv_lk;
	}
	public void setTotFcEv_lk(Integer totFcEv_lk) {
		this.totFcEv_lk = totFcEv_lk;
	}
	public Integer getTotFcPoLk() {
		return totFcPoLk;
	}
	public void setTotFcPoLk(Integer totFcPoLk) {
		this.totFcPoLk = totFcPoLk;
	}
	public Integer getTotFcVdUl() {
		return totFcVdUl;
	}
	public void setTotFcVdUl(Integer totFcVdUl) {
		this.totFcVdUl = totFcVdUl;
	}
	public Integer getTotFfo_vd_ul() {
		return totFfo_vd_ul;
	}
	public void setTotFfo_vd_ul(Integer totFfo_vd_ul) {
		this.totFfo_vd_ul = totFfo_vd_ul;
	}
	public Integer getTot_sh_vd() {
		return tot_sh_vd;
	}
	public void setTot_sh_vd(Integer tot_sh_vd) {
		this.tot_sh_vd = tot_sh_vd;
	}
	public Integer getTot_vd_lk() {
		return tot_vd_lk;
	}
	public void setTot_vd_lk(Integer tot_vd_lk) {
		this.tot_vd_lk = tot_vd_lk;
	}
	public Integer getTot_vd_ul() {
		return tot_vd_ul;
	}
	public void setTot_vd_ul(Integer tot_vd_ul) {
		this.tot_vd_ul = tot_vd_ul;
	}
	public Date getRwd_date() {
		return rwd_date;
	}
	public void setRwd_date(Date rwd_date) {
		this.rwd_date = rwd_date;
	}
	public Integer getRwdFc_cr_po() {
		return rwdFc_cr_po;
	}
	public void setRwdFc_cr_po(Integer rwdFc_cr_po) {
		this.rwdFc_cr_po = rwdFc_cr_po;
	}
	public Integer getRwdFcEv() {
		return rwdFcEv;
	}
	public void setRwdFcEv(Integer rwdFcEv) {
		this.rwdFcEv = rwdFcEv;
	}
	public Integer getRwdFcEv_lk() {
		return rwdFcEv_lk;
	}
	public void setRwdFcEv_lk(Integer rwdFcEv_lk) {
		this.rwdFcEv_lk = rwdFcEv_lk;
	}
	public Integer getRwdFc_po_lk() {
		return rwdFc_po_lk;
	}
	public void setRwdFc_po_lk(Integer rwdFc_po_lk) {
		this.rwdFc_po_lk = rwdFc_po_lk;
	}
	public Integer getRwdFc_vd_ul() {
		return rwdFc_vd_ul;
	}
	public void setRwdFc_vd_ul(Integer rwdFc_vd_ul) {
		this.rwdFc_vd_ul = rwdFc_vd_ul;
	}
	public Integer getRwdFfo_vd_ul() {
		return rwdFfo_vd_ul;
	}
	public void setRwdFfo_vd_ul(Integer rwdFfo_vd_ul) {
		this.rwdFfo_vd_ul = rwdFfo_vd_ul;
	}
	public Integer getRwd_sh_vd() {
		return rwd_sh_vd;
	}
	public void setRwd_sh_vd(Integer rwd_sh_vd) {
		this.rwd_sh_vd = rwd_sh_vd;
	}
	public Integer getRwd_vd_lk() {
		return rwd_vd_lk;
	}
	public void setRwd_vd_lk(Integer rwd_vd_lk) {
		this.rwd_vd_lk = rwd_vd_lk;
	}
	public Integer getRwd_vd_ul() {
		return rwd_vd_ul;
	}
	public void setRwd_vd_ul(Integer rwd_vd_ul) {
		this.rwd_vd_ul = rwd_vd_ul;
	}
	public Date getUpd_dttm() {
		return upd_dttm;
	}
	public void setUpd_dttm(Date upd_dttm) {
		this.upd_dttm = upd_dttm;
	}
	public Date getFcCrPostDate() {
		return fcCrPostDate;
	}
	public void setFcCrPostDate(Date fcCrPostDate) {
		this.fcCrPostDate = fcCrPostDate;
	}
	public Date getFcEvDate() {
		return fcEvDate;
	}
	public void setFcEvDate(Date fcEvDate) {
		this.fcEvDate = fcEvDate;
	}
	public Date getFcEvLkDate() {
		return fcEvLkDate;
	}
	public void setFcEvLkDate(Date fcEvLkDate) {
		this.fcEvLkDate = fcEvLkDate;
	}
	public Date getFcPostLikeDate() {
		return fcPostLikeDate;
	}
	public void setFcPostLikeDate(Date fcPostLikeDate) {
		this.fcPostLikeDate = fcPostLikeDate;
	}
	public Date getFcVidUpldDate() {
		return fcVidUpldDate;
	}
	public void setFcVidUpldDate(Date fcVidUpldDate) {
		this.fcVidUpldDate = fcVidUpldDate;
	}
	public Date getFfoVidUpldDate() {
		return ffoVidUpldDate;
	}
	public void setFfoVidUpldDate(Date ffoVidUpldDate) {
		this.ffoVidUpldDate = ffoVidUpldDate;
	}
	public Date getShVidDate() {
		return shVidDate;
	}
	public void setShVidDate(Date shVidDate) {
		this.shVidDate = shVidDate;
	}
	public Date getVidLkDate() {
		return vidLkDate;
	}
	public void setVidLkDate(Date vidLkDate) {
		this.vidLkDate = vidLkDate;
	}

	@Override
	public String toString() {
		return "CustomerDailyRewardLimits [customerId=" + customerId + ", totFcCrPo=" + totFcCrPo + ", totFcEv="
				+ totFcEv + ", totFcEv_lk=" + totFcEv_lk + ", totFcPoLk=" + totFcPoLk + ", totFcVdUl=" + totFcVdUl
				+ ", totFfo_vd_ul=" + totFfo_vd_ul + ", tot_sh_vd=" + tot_sh_vd + ", tot_vd_lk=" + tot_vd_lk
				+ ", tot_vd_ul=" + tot_vd_ul + ", rwd_date=" + rwd_date + ", rwdFc_cr_po=" + rwdFc_cr_po + ", rwdFcEv="
				+ rwdFcEv + ", rwdFcEv_lk=" + rwdFcEv_lk + ", rwdFc_po_lk=" + rwdFc_po_lk + ", rwdFc_vd_ul="
				+ rwdFc_vd_ul + ", rwdFfo_vd_ul=" + rwdFfo_vd_ul + ", rwd_sh_vd=" + rwd_sh_vd + ", rwd_vd_lk="
				+ rwd_vd_lk + ", rwd_vd_ul=" + rwd_vd_ul + ", upd_dttm=" + upd_dttm + ", fcCrPostDate=" + fcCrPostDate
				+ ", fcEvDate=" + fcEvDate + ", fcEvLkDate=" + fcEvLkDate + ", fcPostLikeDate=" + fcPostLikeDate
				+ ", fcVidUpldDate=" + fcVidUpldDate + ", ffoVidUpldDate=" + ffoVidUpldDate + ", shVidDate=" + shVidDate
				+ ", vidLkDate=" + vidLkDate + ", getCustomerId()=" + getCustomerId() + ", getTotFcCrPo()="
				+ getTotFcCrPo() + ", getTotFcEv()=" + getTotFcEv() + ", getTotFcEv_lk()=" + getTotFcEv_lk()
				+ ", getTotFcPoLk()=" + getTotFcPoLk() + ", getTotFcVdUl()=" + getTotFcVdUl() + ", getTotFfo_vd_ul()="
				+ getTotFfo_vd_ul() + ", getTot_sh_vd()=" + getTot_sh_vd() + ", getTot_vd_lk()=" + getTot_vd_lk()
				+ ", getTot_vd_ul()=" + getTot_vd_ul() + ", getRwd_date()=" + getRwd_date() + ", getRwdFc_cr_po()="
				+ getRwdFc_cr_po() + ", getRwdFcEv()=" + getRwdFcEv() + ", getRwdFcEv_lk()=" + getRwdFcEv_lk()
				+ ", getRwdFc_po_lk()=" + getRwdFc_po_lk() + ", getRwdFc_vd_ul()=" + getRwdFc_vd_ul()
				+ ", getRwdFfo_vd_ul()=" + getRwdFfo_vd_ul() + ", getRwd_sh_vd()=" + getRwd_sh_vd()
				+ ", getRwd_vd_lk()=" + getRwd_vd_lk() + ", getRwd_vd_ul()=" + getRwd_vd_ul() + ", getUpd_dttm()="
				+ getUpd_dttm() + ", getFcCrPostDate()=" + getFcCrPostDate() + ", getFcEvDate()=" + getFcEvDate()
				+ ", getFcEvLkDate()=" + getFcEvLkDate() + ", getFcPostLikeDate()=" + getFcPostLikeDate()
				+ ", getFcVidUpldDate()=" + getFcVidUpldDate() + ", getFfoVidUpldDate()=" + getFfoVidUpldDate()
				+ ", getShVidDate()=" + getShVidDate() + ", getVidLkDate()=" + getVidLkDate() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	public Date getCurrDate() {
		return currDate;
	}
	public void setCurrDate(Date currDate) {
		this.currDate = currDate;
	}
	public Integer getTotVdVw() {
		return totVdVw;
	}
	public void setTotVdVw(Integer totVdVw) {
		this.totVdVw = totVdVw;
	}
	public Integer getRwd_vd_vw() {
		return rwd_vd_vw;
	}
	public void setRwd_vd_vw(Integer rwd_vd_vw) {
		this.rwd_vd_vw = rwd_vd_vw;
	}
	public Date getVdVwDate() {
		return vdVwDate;
	}
	public void setVdVwDate(Date vdVwDate) {
		this.vdVwDate = vdVwDate;
	}

}
