package com.quiz.cassandra.list;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PollingRewardsInfo")
public class PollingRewardsInfo {

		private Integer id;
		
		private Integer contestId;		
		private Integer eraserPerQue;
		private Integer gbricksPerQue;
		private Integer hifivePerQue;
		private Integer crstballPerQue;
		private Integer lifePerQue;
		private Integer supFanStarsPerQue;		
		private Integer rtfPointsPerQue;
		
		public Integer getRtfPointsPerQue() {
			return rtfPointsPerQue;
		}
		public void setRtfPointsPerQue(Integer rtfPointsPerQue) {
			this.rtfPointsPerQue = rtfPointsPerQue;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Integer getContestId() {
			return contestId;
		}
		public void setContestId(Integer contestId) {
			this.contestId = contestId;
		}
		public Integer getEraserPerQue() {
			return eraserPerQue;
		}
		public void setEraserPerQue(Integer eraserPerQue) {
			this.eraserPerQue = eraserPerQue;
		}
		public Integer getGbricksPerQue() {
			return gbricksPerQue;
		}
		public void setGbricksPerQue(Integer gbricksPerQue) {
			this.gbricksPerQue = gbricksPerQue;
		}
		public Integer getHifivePerQue() {
			return hifivePerQue;
		}
		public void setHifivePerQue(Integer hifivePerQue) {
			this.hifivePerQue = hifivePerQue;
		}
		public Integer getCrstballPerQue() {
			return crstballPerQue;
		}
		public void setCrstballPerQue(Integer crstballPerQue) {
			this.crstballPerQue = crstballPerQue;
		}
		public Integer getLifePerQue() {
			return lifePerQue;
		}
		public void setLifePerQue(Integer lifePerQue) {
			this.lifePerQue = lifePerQue;
		}
		public Integer getSupFanStarsPerQue() {
			return supFanStarsPerQue;
		}
		public void setSupFanStarsPerQue(Integer supFanStarsPerQue) {
			this.supFanStarsPerQue = supFanStarsPerQue;
		}
		

}
