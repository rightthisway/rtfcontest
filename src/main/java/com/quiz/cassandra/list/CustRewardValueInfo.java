package com.quiz.cassandra.list;

public class CustRewardValueInfo {
	
	
	private Boolean isRewarded;
	private Integer rwdQty;
	
	public Boolean getIsRewarded() {
		if(isRewarded == null) isRewarded = Boolean.FALSE;
		return isRewarded;
	}
	public void setIsRewarded(Boolean isRewarded) {
		this.isRewarded = isRewarded;
	}
	public Integer getRwdQty() {
		if(rwdQty == null) rwdQty = 0;
		return rwdQty;
	}
	public void setRwdQty(Integer rwdQty) {
		this.rwdQty = rwdQty;
	}
	
	

}
