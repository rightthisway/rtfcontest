package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PollingAnswerInfo")
public class PollingAnswerInfo {

	private Integer sts;
	private CassError err; 
	private String msg;
	
	private Integer isAnsCrt;
	private String crtAnsTxt;	
	private String rewardTtxt;
	private String rwdType;
	private String imgUrl;
	private Integer rwdQty;
	private Integer gcId;
	private String showMsg;
	private String answer;
	private Integer cuId;
	private Integer pCtId;
	private Integer id; // Question Id in table : polling_answer_validation
	private String correctAns;
	private String nxtCTxt;
	private String pollingType;
	private String vidUpdMsg;
	private Integer oQId;
	
	
	public Integer getoQId() {
		return oQId;
	}
	public void setoQId(Integer oQId) {
		this.oQId = oQId;
	}	
	
	public String getPollingType() {
		return pollingType;
	}
	public void setPollingType(String pollingType) {
		this.pollingType = pollingType;
	}
	public String getNxtCTxt() {
		return nxtCTxt;
	}
	public void setNxtCTxt(String nxtCTxt) {
		this.nxtCTxt = nxtCTxt;
	}
	public String getCorrectAns() {
		return correctAns;
	}
	public void setCorrectAns(String correctAns) {
		this.correctAns = correctAns;
	}
	public Integer getpCtId() {
		return pCtId;
	}
	public void setpCtId(Integer pCtId) {
		this.pCtId = pCtId;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}	
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	private String  lastAnsTm;

	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getIsAnsCrt() {
		return isAnsCrt;
	}
	public void setIsAnsCrt(Integer isAnsCrt) {
		this.isAnsCrt = isAnsCrt;
	}
	public String getCrtAnsTxt() {
		return crtAnsTxt;
	}
	public void setCrtAnsTxt(String crtAnsTxt) {
		this.crtAnsTxt = crtAnsTxt;
	}
	public String getRewardTtxt() {
		return rewardTtxt;
	}
	public void setRewardTtxt(String rewardTtxt) {
		this.rewardTtxt = rewardTtxt;
	}
	public String getRwdType() {
		return rwdType;
	}
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Integer getRwdQty() {
		return rwdQty;
	}
	public void setRwdQty(Integer rwdQty) {
		this.rwdQty = rwdQty;
	}
	public Integer getGcId() {
		return gcId;
	}
	public void setGcId(Integer gcId) {
		this.gcId = gcId;
	}	
	
	public String getShowMsg() {
		return showMsg;
	}
	public void setShowMsg(String showMsg) {
		this.showMsg = showMsg;
	}
	public String getLastAnsTm() {
		return lastAnsTm;
	}
	public void setLastAnsTm(String lastAnsTm) {
		this.lastAnsTm = lastAnsTm;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getVidUpdMsg() {
		return vidUpdMsg;
	}
	public void setVidUpdMsg(String vidUpdMsg) {
		this.vidUpdMsg = vidUpdMsg;
	}
	
}