package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Card")
public class Card {
	
	private String cardName;

	public final String getCardName() {
		return cardName;
	}

	public final void setCardName(String cardName) {
		this.cardName = cardName;
	}
	 
}
