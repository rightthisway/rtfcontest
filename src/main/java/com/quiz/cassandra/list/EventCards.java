package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("EventCards")
public class EventCards {
	private Integer status;
	private com.zonesws.webservices.data.Error error; 
	private String alertMessage;
	private Boolean showMoreButton;
	private List<Card> cards;
	private Boolean eligibleToShareRefCode;
	private String shareErrorMessage;
	private String PriceDescText;
	
	
	public com.zonesws.webservices.data.Error getError() {
		return error;
	}
	public void setError(com.zonesws.webservices.data.Error error) {
		this.error = error;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<Card> getCards() {
		return cards;
	}
	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
	
	public String getAlertMessage() {
		if(alertMessage == null){
			alertMessage= "";
		}
		return alertMessage;
	}
	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}
	public Boolean getShowMoreButton() {
		if(null == showMoreButton ){
			showMoreButton= true;
		}
		return showMoreButton;
	}
	public void setShowMoreButton(Boolean showMoreButton) {
		this.showMoreButton = showMoreButton;
	}

	public Boolean getEligibleToShareRefCode() {
		if(null == eligibleToShareRefCode){
			eligibleToShareRefCode = false;
		}
		return eligibleToShareRefCode;
	}
	public void setEligibleToShareRefCode(Boolean eligibleToShareRefCode) {
		this.eligibleToShareRefCode = eligibleToShareRefCode;
	}
	
	public String getShareErrorMessage() {
		if(null == shareErrorMessage){
			shareErrorMessage="";
		}
		return shareErrorMessage;
	}
	public void setShareErrorMessage(String shareErrorMessage) {
		this.shareErrorMessage = shareErrorMessage;
	}
	public String getPriceDescText() {
		PriceDescText = "PRICES ARE SET BY SELLERS AND MAY BE ABOVE OR BELOW THE FACE VALUE";
		return PriceDescText;
	}
	public void setPriceDescText(String priceDescText) {
		PriceDescText = priceDescText;
	}
	
	
}
