package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.FanClubEvent;

@XStreamAlias("FanClubEventInfo")
public class FanClubEventInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private FanClubEvent fcEvent;
	private List<FanClubEvent> fceList;
	private Boolean hme = false;//has More Events List
	private RewardsInfo rwdInfo;
	
	public RewardsInfo getRwdInfo() {
		return rwdInfo;
	}

	public void setRwdInfo(RewardsInfo rwdInfo) {
		this.rwdInfo = rwdInfo;
	}

	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		if(msg == null) {
			msg="";
		}
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public FanClubEvent getFcEvent() {
		return fcEvent;
	}

	public void setFcEvent(FanClubEvent fcEvent) {
		this.fcEvent = fcEvent;
	}

	public List<FanClubEvent> getFceList() {
		return fceList;
	}

	public void setFceList(List<FanClubEvent> fceList) {
		this.fceList = fceList;
	}

	public Boolean getHme() {
		if(hme == null) {
			hme = false;
		}
		return hme;
	}

	public void setHme(Boolean hme) {
		this.hme = hme;
	}

	
	
	
}
