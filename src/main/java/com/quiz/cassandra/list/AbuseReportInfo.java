package com.quiz.cassandra.list;

public class AbuseReportInfo {


//	id                        int
//	abuse_text          varchar(120)
//	abuse_info          varchar(250)
//	abuse_type         varchar(15) --(video / comment / all )
//	status                  bit    (0 / 1 -- inactive / active)
//	created_date       datetime
//	updated_date      datetime
//	created_by           varchar(30)
//	updated_by          varchar(30)	

	
	private Integer id;
	private String abuseTxt;
	private String abuseInfo;
	private String abuseType;
	private Boolean status;
	private Integer isAns;
	private Integer srcId; // to be passed from app . id of comment or video id 
	private Integer commentId;
	private Integer vidId;
	private Integer sts;	
	private CassError err; 
	private String msg;
	private Integer cuId;	
	private Integer abuseOptionId;
	public Integer getAbuseOptionId() {
		return abuseOptionId;
	}
	public void setAbuseOptionId(Integer abuseOptionId) {
		this.abuseOptionId = abuseOptionId;
	}
	
	public Integer getSrcId() {
		return srcId;
	}
	public void setSrcId(Integer srcId) {
		this.srcId = srcId;
	}
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAbuseTxt() {
		return abuseTxt;
	}
	public void setAbuseTxt(String abuseTxt) {
		this.abuseTxt = abuseTxt;
	}
	public String getAbuseInfo() {
		return abuseInfo;
	}
	public void setAbuseInfo(String abuseInfo) {
		this.abuseInfo = abuseInfo;
	}
	public String getAbuseType() {
		return abuseType;
	}
	public void setAbuseType(String abuseType) {
		this.abuseType = abuseType;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Integer getIsAns() {
		if(null == isAns) {
			isAns =0;
		}
		return isAns;
	}
	public void setIsAns(Integer isAns) {
		this.isAns = isAns;
	}
	public Integer getCommentId() {
		return commentId;
	}
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}
	public Integer getVidId() {
		return vidId;
	}
	public void setVidId(Integer vidId) {
		this.vidId = vidId;
	}
	
	

}
