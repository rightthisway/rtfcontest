package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.FanClubEvent;

@XStreamAlias("RtfPointsDetailInfo")
public class RtfPointsDetailInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private List<RtfPointsDetails> list;
	private String title;
	
	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		if(msg == null) {
			msg="";
		}
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<RtfPointsDetails> getList() {
		return list;
	}

	public void setList(List<RtfPointsDetails> list) {
		this.list = list;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	
	
	
}
