package com.quiz.cassandra.list;

import java.util.List;

public class AbuseReportDTO {
	
private Integer srcId; // to be passed from app . id of video
private Integer commentId;//to be passed from app .. id of Comment 
private Integer sts;	
private CassError err; 
private String msg;
private Integer cuId;
private String abuseType;
private List<AbuseReportInfo> abuseReporOptionList;
private Boolean isAllOptionsActive;
	
public Integer getCommentId() {
	return commentId;
}

public void setCommentId(Integer commentId) {
	this.commentId = commentId;
}

public String getAbuseType() {
	return abuseType;
}

public void setAbuseType(String abuseType) {
	this.abuseType = abuseType;
}

public Integer getSrcId() {
	return srcId;
}

public void setSrcId(Integer srcId) {
	this.srcId = srcId;
}

public Integer getSts() {
	return sts;
}

public void setSts(Integer sts) {
	this.sts = sts;
}

public CassError getErr() {
	return err;
}

public void setErr(CassError err) {
	this.err = err;
}

public String getMsg() {
	return msg;
}

public void setMsg(String msg) {
	this.msg = msg;
}

public Integer getCuId() {
	return cuId;
}

public void setCuId(Integer cuId) {
	this.cuId = cuId;
}

public List<AbuseReportInfo> getAbuseReporOptionList() {
	return abuseReporOptionList;
}

public void setAbuseReporOptionList(List<AbuseReportInfo> abuseReporOptionList) {
	this.abuseReporOptionList = abuseReporOptionList;
}


public Boolean getIsAllOptionsActive() {
	if(null == isAllOptionsActive) {
		isAllOptionsActive = true;
	}
	return isAllOptionsActive;
}

public void setIsAllOptionsActive(Boolean isAllOptionsActive) {
	this.isAllOptionsActive = isAllOptionsActive;
}




}
