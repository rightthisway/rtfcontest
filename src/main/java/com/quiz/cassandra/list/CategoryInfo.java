package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Category;

@XStreamAlias("CategoryInfo")
public class CategoryInfo {

	private Integer sts;
	private CassError err; 
	private String msg; 
	private List<Category> catList;
	
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<Category> getCatList() {
		return catList;
	}
	public void setCatList(List<Category> catList) {
		this.catList = catList;
	} 
	
	
		
}