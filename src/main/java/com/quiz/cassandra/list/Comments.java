package com.quiz.cassandra.list;

import java.util.Date;

public class Comments {
	
	private Integer srcId;
	private Integer cmtId;
	private String cmtTxt;
	private String vidUrl;
	private String tUrl;
	private String imgUrl;
	private Date creDate;
	private Integer cuId;
	private String usr;
	private CassError err; 
	private String msg;
	private Integer sts;
	private String dpImg;
	private String rtftvUrl;
	private String createDtStr;
	private Integer postId;
	private Integer cmtCnt;
	private Boolean isLiked;
	private Integer lkCnt;
	private Boolean isMem;
	private String title;
	private Integer fcId;
	private String catId;
	private String description;
	private Integer vId;
	private String ivTxt = "http://onelink.to/5xwz4g";
	private String shPTxt = "http://onelink.to/5xwz4g";
	private String shVTxt = "http://onelink.to/5xwz4g";
	private Integer vwCnt;
	private String vwStr;
	
	public String getVwStr() {
		return vwCnt + " views";
	}
	public void setVwStr(String vwStr) {
		this.vwStr = vwStr;
	}
	private RewardsInfo rwdInfo;
	public RewardsInfo getRwdInfo() {
		return rwdInfo;
	}
	public void setRwdInfo(RewardsInfo rwdInfo) {
		this.rwdInfo = rwdInfo;
	}
	public String getIvTxt() {
		return ivTxt;
	}
	public void setIvTxt(String ivTxt) {
		this.ivTxt = ivTxt;
	}
	public String getShPTxt() {
		return shPTxt;
	}
	public void setShPTxt(String shPTxt) {
		this.shPTxt = shPTxt;
	}
	public String getShVTxt() {
		return shVTxt;
	}
	public void setShVTxt(String shVTxt) {
		this.shVTxt = shVTxt;
	}
	public Integer getvId() {
		return vId;
	}
	public void setvId(Integer vId) {
		this.vId = vId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCatId() {
		return catId;
	}
	public void setCatId(String catId) {
		this.catId = catId;
	}
	public Integer getFcId() {
		return fcId;
	}
	public void setFcId(Integer fcId) {
		this.fcId = fcId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Boolean getIsMem() {
		return isMem;
	}
	public void setIsMem(Boolean isMem) {
		this.isMem = isMem;
	}
	public Integer getPostId() {
		return postId;
	}
	public void setPostId(Integer postId) {
		this.postId = postId;
	}
	public Integer getCmtCnt() {
		if(cmtCnt == null) cmtCnt = 0;
		return cmtCnt;
	}
	public void setCmtCnt(Integer cmtCnt) {
		this.cmtCnt = cmtCnt;
	}
	public Boolean getIsLiked() {
		if(isLiked == null) return false;
		return isLiked;
	}
	public void setIsLiked(Boolean isLiked) {
		this.isLiked = isLiked;
	}
	public Integer getLkCnt() {
		if(lkCnt == null) return 0;
		return lkCnt;
	}
	public void setLkCnt(Integer lkCnt) {
		this.lkCnt = lkCnt;
	}
	public String getRtftvUrl() {
		return rtftvUrl;
	}
	public void setRtftvUrl(String rtftvUrl) {	
		this.rtftvUrl = rtftvUrl;
	}
	public String getDpImg() {
		return dpImg;
	}
	public void setDpImg(String dpImg) {
		this.dpImg = dpImg;
	}
	public String getUsr() {
		return usr;
	}
	public void setUsr(String usr) {
		this.usr = usr;
	}
	public Date getCreDate() {
		return creDate;
	}
	public void setCreDate(Date creDate) {
		this.creDate = creDate;
	}
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public Integer getSrcId() {
		return srcId;
	}
	public void setSrcId(Integer srcId) {
		this.srcId = srcId;
	}
	public Integer getCmtId() {
		return cmtId;
	}
	public void setCmtId(Integer cmtId) {
		this.cmtId = cmtId;
	}
	public String getCmtTxt() {
		return cmtTxt;
	}
	public void setCmtTxt(String cmtTxt) {
		this.cmtTxt = cmtTxt;
	}
	public String getVidUrl() {
		return vidUrl;
	}
	public void setVidUrl(String vidUrl) {
		this.vidUrl = vidUrl;
	}
	public String gettUrl() {
		return tUrl;
	}
	public void settUrl(String tUrl) {
		this.tUrl = tUrl;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public String getCreateDtStr() {
		return createDtStr;
	}
	public void setCreateDtStr(String createDtStr) {
		this.createDtStr = createDtStr;
	}
	public Integer getVwCnt() {
		if(vwCnt == null) return 0;
		return vwCnt;
	}
	public void setVwCnt(Integer vwCnt) {
		this.vwCnt = vwCnt;
	}
	
	
	

}
