package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PollingVideoInfo")
public class PollingVideoPlayedInfo {
	private Integer sts;
	private CassError err; 
	private String msg;
	private String vUrl;

	private String deviceIp;
	private String pfm;
	private Integer cuId;
	private String wTy;
	private Integer vid;
	
	public Integer getVid() {
		return vid;
	}
	public void setVid(Integer vid) {
		this.vid = vid;
	}
	public String getwTy() {
		return wTy;
	}
	public void setwTy(String wTy) {
		this.wTy = wTy;
	}

	public Integer getCuId() {
		return cuId;
	}


	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	
	


	public Integer getSts() {
		return sts;
	}


	public void setSts(Integer sts) {
		this.sts = sts;
	}


	public CassError getErr() {
		return err;
	}


	public void setErr(CassError err) {
		this.err = err;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}


	public String getvUrl() {
		return vUrl;
	}


	public void setvUrl(String vUrl) {
		this.vUrl = vUrl;
	}
	
		
}