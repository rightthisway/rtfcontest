package com.quiz.cassandra.list;

import java.util.List;

public class FanClubVidPlayListDTO {

	private List<FCPlayListInfo> playLst;

	private CassError err;
	private String msg;
	private Integer sts;
	private Integer vId;
	private Integer cuId;
	private Integer fcId;
	private Boolean hme;
	public Boolean getHme() {
		
		if(hme == null) {
			hme = false;
		}
		return hme;
	}

	public void setHme(Boolean hme) {
		this.hme = hme;
	}

	public List<FCPlayListInfo> getPlayLst() {
		return playLst;
	}

	public void setPlayLst(List<FCPlayListInfo> playLst) {
		this.playLst = playLst;
	}

	public Integer getFcId() {
		return fcId;
	}

	public void setFcId(Integer fcId) {
		this.fcId = fcId;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public Integer getvId() {
		return vId;
	}

	public void setvId(Integer vId) {
		this.vId = vId;
	}

	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

}
