package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ValidateAnsInfo")
public class ContestValidateAnsInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private Boolean isCrt= false;
	
	
	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		if(msg == null) {
			msg="";
		}
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Boolean getIsCrt() {
		return isCrt;
	}

	public void setIsCrt(Boolean isCrt) {
		this.isCrt = isCrt;
	}

	@Override
	public String toString() {
		return "ContestValidateAnsInfo [status=" + sts + ", error=" + err + ", message=" + msg
				+ ", isCorrectAnswer=" + isCrt + "]";
	}
	
	
	
	
}
