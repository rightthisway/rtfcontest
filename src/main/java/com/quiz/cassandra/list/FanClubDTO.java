package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.web.util.FanClubUtil;
import com.zonesws.webservices.data.FanClub;

@XStreamAlias("FanClubDTO")
public class FanClubDTO {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private Boolean hme;
	private List<FanClub> fanClubs; 
	private FanClub fanClubObj;
	private String anonErrorMsg;
	
	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		if(msg == null) {
			msg="";
		}
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<FanClub> getFanClubs() {
		return fanClubs;
	}

	public void setFanClubs(List<FanClub> fanClubs) {
		this.fanClubs = fanClubs;
	}

	public FanClub getFanClubObj() {
		return fanClubObj;
	}

	public void setFanClubObj(FanClub fanClubObj) {
		this.fanClubObj = fanClubObj;
	}

	public Boolean getHme() {
		if(null == hme) {
			hme = true;
		}
		return hme;
	}

	public void setHme(Boolean hme) {
		this.hme = hme;
	}

	public String getAnonErrorMsg() {
		anonErrorMsg = FanClubUtil.anonymousErrorMessage;
		return anonErrorMsg;
	}

	public void setAnonErrorMsg(String anonErrorMsg) {
		this.anonErrorMsg = anonErrorMsg;
	}
}
