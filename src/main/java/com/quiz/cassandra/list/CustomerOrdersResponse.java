package com.quiz.cassandra.list;

import java.util.List;

import com.quiz.cassandra.list.CustomerOrder;

public class CustomerOrdersResponse {

	private Integer status;
	private com.zonesws.webservices.data.Error error;
	private String svgWebViewUrl;
	private List<com.quiz.cassandra.list.CustomerOrder> customerOrders;
	private List<CustomerOrder> pastOrders;
	private List<ContestGrandWinner> contestGrandWinners;
	private String contestTicketsConfirmDialog;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.zonesws.webservices.data.Error getError() {
		return error;
	}
	public void setError(com.zonesws.webservices.data.Error error) {
		this.error = error;
	}
	public List<CustomerOrder> getCustomerOrders() {
		return customerOrders;
	}
	public void setCustomerOrders(List<CustomerOrder> customerOrders) {
		this.customerOrders = customerOrders;
	}
	public String getSvgWebViewUrl() {
		return svgWebViewUrl;
	}
	public void setSvgWebViewUrl(String svgWebViewUrl) {
		this.svgWebViewUrl = svgWebViewUrl;
	}
	/*public String getTicketDownloadUrl() {
		return ticketDownloadUrl;
	}
	public void setTicketDownloadUrl(String ticketDownloadUrl) {
		this.ticketDownloadUrl = ticketDownloadUrl;
	}*/
	public List<CustomerOrder> getPastOrders() {
		return pastOrders;
	}
	public void setPastOrders(List<CustomerOrder> pastOrders) {
		this.pastOrders = pastOrders;
	}
	/*public Boolean getShowInfoButton() {
		return showInfoButton;
	}
	public void setShowInfoButton(Boolean showInfoButton) {
		this.showInfoButton = showInfoButton;
	}
	public String getDeliveryInfo() {
		return deliveryInfo;
	}
	public void setDeliveryInfo(String deliveryInfo) {
		this.deliveryInfo = deliveryInfo;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}*/
	public List<ContestGrandWinner> getContestGrandWinners() {
		return contestGrandWinners;
	}
	public void setContestGrandWinners(List<ContestGrandWinner> contestGrandWinners) {
		this.contestGrandWinners = contestGrandWinners;
	}
	public String getContestTicketsConfirmDialog() {
		return contestTicketsConfirmDialog;
	}
	public void setContestTicketsConfirmDialog(String contestTicketsConfirmDialog) {
		this.contestTicketsConfirmDialog = contestTicketsConfirmDialog;
	}
	
}
