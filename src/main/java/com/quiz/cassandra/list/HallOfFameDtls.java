package com.quiz.cassandra.list;

import java.io.Serializable;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.quiz.cassandra.utils.TicketUtil;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.URLUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("HallOfFameDtls")
public class HallOfFameDtls  implements Serializable{
	
	@JsonIgnore
	private UUID id;
	private Integer cuId;
	private Integer rTix;
	private Double rPoints;
	private Integer gameWon = 0;;
	
	@JsonIgnore
	private String imgP;//profile image path
	private String imgU;// profile image URL
	
	private String uId;
	private String rPointsSt;
	private Integer rRank;
	
	public HallOfFameDtls( ) { }
	
	public HallOfFameDtls(UUID id, Integer cuId, Integer rTix, Double rPoints) {
		this.id = id;
		this.cuId = cuId;
		this.rTix = rTix;
		this.rPoints = rPoints;
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public Integer getrTix() {
		if(rTix == null) {
			rTix = 0;
		}
		return rTix;
	}
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}

	public Double getrPoints() {
		return rPoints;
	}
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}

	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getImgP() {
		return imgP;
	}
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}

	public String getImgU() {
		//imgU = URLUtil.profilePicWebURByImageName(this.imgP);
		imgU = URLUtil.profilePicForSummary(this.imgP);
		return imgU;
	}
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	public String getrPointsSt() {
		if(rPoints != null) {
			try {
				rPointsSt = TicketUtil.getRoundedValueString(rPoints);
			} catch (Exception e) {
				rPointsSt="0.00";
				e.printStackTrace();
			}
		} else {
			rPointsSt="0.00";
		}
		return rPointsSt;
	}
	public void setrPointsSt(String rPointsSt) {
		this.rPointsSt = rPointsSt;
	}

	public Integer getrRank() {
		if(rRank == null) {
			rRank = 0;
		}
		return rRank;
	}

	public void setrRank(Integer rRank) {
		this.rRank = rRank;
	}

	public Integer getGameWon() {
		return gameWon;
	}

	public void setGameWon(Integer gameWon) {
		this.gameWon = gameWon;
	}



}
