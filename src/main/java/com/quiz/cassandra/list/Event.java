package com.quiz.cassandra.list;

import java.io.Serializable;

public class Event implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer eventId;
	public final Integer getEventId() {
		return eventId;
	}
	public final void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	 
}
