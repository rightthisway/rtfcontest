package com.quiz.cassandra.list;

import com.quiz.cassandra.data.CassCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("DbdInfo")
public class DashboardInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private CassCustomer cust;
	private Boolean hRwds;
	private Integer sflqNo;
	private Integer cartCount;
	
	
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public CassCustomer getCust() {
		return cust;
	}
	public void setCust(CassCustomer cust) {
		this.cust = cust;
	}
	public Boolean gethRwds() {
		if(hRwds == null) {
			hRwds = false;
		}
		return hRwds;
	}
	public void sethRwds(Boolean hRwds) {
		this.hRwds = hRwds;
	}
	public Integer getSflqNo() {
		if(sflqNo == null) {
			sflqNo = 0;
		}
		return sflqNo;
	}

	public void setSflqNo(Integer sflqNo) {
		this.sflqNo = sflqNo;
	}
	public Integer getCartCount() {
		return cartCount;
	}
	public void setCartCount(Integer cartCount) {
		this.cartCount = cartCount;
	}

}
