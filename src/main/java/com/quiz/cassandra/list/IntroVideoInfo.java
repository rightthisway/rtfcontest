package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("IntroVideoInfo")
public class IntroVideoInfo {
	
	 	private Integer sts;
	 	private String nxtCTxt;
		private String msg;
		private CassError err;
		private Integer cuId;
		private String ans;
		private Integer isAnsCrt;
		private String crtAnsTxt;
		private String rwdType;
		private String showMsg;
		
		
		public Integer getIsAnsCrt() {
			return isAnsCrt;
		}
		public void setIsAnsCrt(Integer isAnsCrt) {
			this.isAnsCrt = isAnsCrt;
		}
		public String getCrtAnsTxt() {
			return crtAnsTxt;
		}
		public void setCrtAnsTxt(String crtAnsTxt) {
			this.crtAnsTxt = crtAnsTxt;
		}
		public String getRwdType() {
			return rwdType;
		}
		public void setRwdType(String rwdType) {
			this.rwdType = rwdType;
		}
		public String getShowMsg() {
			return showMsg;
		}
		public void setShowMsg(String showMsg) {
			this.showMsg = showMsg;
		}	
		
		public String getAns() {
			return ans;
		}
		public void setAns(String ans) {
			this.ans = ans;
		}
		public Integer getCuId() {
			return cuId;
		}
		public void setCuId(Integer cuId) {
			this.cuId = cuId;
		}		
		
		public Integer getSts() {
			return sts;
		}
		public void setSts(Integer sts) {
			this.sts = sts;
		}
		public String getNxtCTxt() {
			return nxtCTxt;
		}
		public void setNxtCTxt(String nxtCTxt) {
			this.nxtCTxt = nxtCTxt;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public CassError getErr() {
			return err;
		}
		public void setErr(CassError err) {
			this.err = err;
		}
		
	
	
}
