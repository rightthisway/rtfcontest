package com.quiz.cassandra.list;

public class RtfPointsDetails {
	
	private String title;
	private Integer rtfPoints;
	private Integer sfStars;
	private Integer erasers;
	private Integer lives;
	private Double dollars;
	private String pointsText;
	private String livText;
	private String eraText;
	private String staText;
	private String dolText;
	
	public RtfPointsDetails(){
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getRtfPoints() {
		if(rtfPoints == null) {
			rtfPoints = 0;
		}
		return rtfPoints;
	}

	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}

	public String getPointsText() {
		if(rtfPoints != null && rtfPoints > 0) {
			pointsText = rtfPoints + " Points";
		} else {
			pointsText = "";
		}
		return pointsText;
	}

	public void setPointsText(String pointsText) {
		this.pointsText = pointsText;
	}

	public Integer getSfStars() {
		if(sfStars == null) {
			sfStars = 0;
		}
		return sfStars;
	}

	public void setSfStars(Integer sfStars) {
		this.sfStars = sfStars;
	}

	public Integer getErasers() {
		if(erasers == null) {
			erasers = 0;
		}
		return erasers;
	}

	public void setErasers(Integer erasers) {
		this.erasers = erasers;
	}

	public Integer getLives() {
		if(lives == null) {
			lives = 0;
		}
		return lives;
	}

	public void setLives(Integer lives) {
		this.lives = lives;
	}

	public Double getDollars() {
		if(dollars == null) {
			dollars = 0.0;
		}
		return dollars;
	}

	public void setDollars(Double dollars) {
		this.dollars = dollars;
	}

	public String getLivText() {
		if(lives != null && lives > 0) {
			livText = lives + " Lives";
		} else {
			livText = "";
		}
		return livText;
	}

	public void setLivText(String livText) {
		this.livText = livText;
	}

	public String getEraText() {
		if(erasers != null && erasers > 0) {
			eraText = erasers + " Erasers";
		} else {
			eraText = "";
		}
		return eraText;
	}

	public void setEraText(String eraText) {
		this.eraText = eraText;
	}

	public String getStaText() {
		if(sfStars != null && sfStars > 0) {
			staText = sfStars + " Stars";
		} else {
			staText = "";
		}
		return staText;
	}

	public void setStaText(String staText) {
		this.staText = staText;
	}

	public String getDolText() {
		if(dollars != null && dollars > 0) {
			dolText = String.format( "%.2f", dollars) + " Dollars";
		} else {
			dolText = "";
		}
		return dolText;
	}

	public void setDolText(String dolText) {
		this.dolText = dolText;
	}
	

}
