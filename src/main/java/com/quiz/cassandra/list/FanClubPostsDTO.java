package com.quiz.cassandra.list;

import java.util.List;

public class FanClubPostsDTO {
	
	Integer fcId;
	List<Comments> cLst;	
	private CassError err; 
	private String msg;
	private Integer sts;
	private Integer cuId;
	private Integer cmtId;
	private String cmtTxt;
	private String vidUrl;
	private String tUrl;
	private String imgUrl;
	private String status;
	private Integer pId;
	private Integer cmtCnt;
	private Boolean isLiked;
	private Integer lkCnt;
	private Integer vId;
	private Boolean hme;
	private String title;
	private String description;
	private RewardsInfo rwdInfo;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Boolean getHme() {
		
		if(hme == null) {
			hme = false;
		}
		return hme;
	}

	public void setHme(Boolean hme) {
		this.hme = hme;
	}

	public Integer getvId() {
		return vId;
	}

	public void setvId(Integer vId) {
		this.vId = vId;
	}

	private List<Integer> postIdList;
	
	public List<Integer> getPostIdList() {
		return postIdList;
	}

	public void setPostIdList(List<Integer> postIdList) {
		this.postIdList = postIdList;
	}

	public Integer getpId() {
		return pId;
	}

	public void setpId(Integer pId) {
		this.pId = pId;
	}

	public Integer getCmtCnt() {
		if(cmtCnt == null) cmtCnt = 0;
		return cmtCnt;
	}

	public void setCmtCnt(Integer cmtCnt) {
		this.cmtCnt = cmtCnt;
	}

	public Boolean getIsLiked() {
		return isLiked;
	}

	public void setIsLiked(Boolean isLiked) {
		this.isLiked = isLiked;
	}

	public Integer getLkCnt() {
		if(lkCnt == null)  lkCnt = 0;
		return lkCnt;
	}

	public void setLkCnt(Integer lkCnt) {
		this.lkCnt = lkCnt;
	}

	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	

	public List<Comments> getcLst() {
		return cLst;
	}

	public void setcLst(List<Comments> cLst) {
		this.cLst = cLst;
	}

	public RewardsInfo getRwdInfo() {
		return rwdInfo;
	}

	public void setRwdInfo(RewardsInfo rwdInfo) {
		this.rwdInfo = rwdInfo;
	}

	public Integer getFcId() {
		return fcId;
	}

	public void setFcId(Integer fcId) {
		this.fcId = fcId;
	}

	public Integer getCmtId() {
		return cmtId;
	}

	public void setCmtId(Integer cmtId) {
		this.cmtId = cmtId;
	}

	public String getCmtTxt() {
		return cmtTxt;
	}

	public void setCmtTxt(String cmtTxt) {
		this.cmtTxt = cmtTxt;
	}

	public String getVidUrl() {
		return vidUrl;
	}

	public void setVidUrl(String vidUrl) {
		this.vidUrl = vidUrl;
	}

	public String gettUrl() {
		return tUrl;
	}

	public void settUrl(String tUrl) {
		this.tUrl = tUrl;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	//CommentsInfo commentsInfo;

}
