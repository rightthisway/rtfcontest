package com.quiz.cassandra.list;

public class RewardsInfo {
	
	public RewardsInfo() {
		
	}
	public RewardsInfo(String msg, Integer sts, Integer pts, Double rDlrs) {
		super();
		this.msg = msg;
		this.sts = sts;
		this.pts = pts;
		this.rDlrs = rDlrs;
	}
	private String msg = "Hey! You just got Rewarded with 10 pointss for creating a new Post";
	private Integer sts = 1;
	private Integer pts = 200;
	private Double rDlrs = 200.55;
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public Integer getPts() {
		return pts;
	}
	public void setPts(Integer pts) {
		this.pts = pts;
	}
	public Double getrDlrs() {
		return rDlrs;
	}
	public void setrDlrs(Double rDlrs) {
		this.rDlrs = rDlrs;
	}
	
}
