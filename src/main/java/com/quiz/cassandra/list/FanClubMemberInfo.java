package com.quiz.cassandra.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.FanClubEvent;

@XStreamAlias("FanClubMemberInfo")
public class FanClubMemberInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;

	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		if(msg == null) {
			msg="";
		}
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
