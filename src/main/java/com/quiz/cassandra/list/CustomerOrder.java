package com.quiz.cassandra.list;

import java.io.Serializable;
 
public class CustomerOrder implements Serializable{

	private static final long serialVersionUID = -7941769011539361245L;
	private Integer id;
	public final Integer getId() {
		return id;
	}
	public final void setId(Integer id) {
		this.id = id;
	}
}
