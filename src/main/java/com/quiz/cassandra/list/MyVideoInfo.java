package com.quiz.cassandra.list;

public class MyVideoInfo {
	private Integer vId;
	private String vUrl;
	private String vImg;
	private String vTitle;
	private String awsfname;
	private Integer vwCnt;
	private Integer lkCnt;
	private String spLogoUrl;
	private String myLikeSts;
	private String rtfTvMediaDets;
	private String updDte;
	private String isWinr;
	private String postrImg;
	private String rtfUpdVideoStr;
	private String desc;
	private Integer catId;
	
	
	public String getDesc() {
		if(desc == null) desc = "";
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Integer getCatId() {
		return catId;
	}
	public void setCatId(Integer catId) {
		this.catId = catId;
	}
	public String getPostrImg() {
		return postrImg;
	}
	public void setPostrImg(String postrImg) {
		this.postrImg = postrImg;
	}
	public String getUpdDte() {
		return updDte;
	}
	public void setUpdDte(String updDte) {
		this.updDte = updDte;
	}
	public String getIsWinr() {
		return isWinr;
	}
	public void setIsWinr(String isWinr) {
		this.isWinr = isWinr;
	}
	
	public String getRtfUpdVideoStr() {
		return vId + "," + awsfname + "," + "0" + "," + "0" + " ," + "0" + "," + null + "," + vTitle + "," + desc + "," + "https://rewardthefan.com/sh/playvid.html?k="+vId ;
	}
	public void setRtfUpdVideoStr(String rtfUpdVideoStr) {
		this.rtfUpdVideoStr = rtfUpdVideoStr;
	}
	
	
	
	public String getRtfTvMediaDets()  {
		
		// id , url , viewcount , 1 , likecount , sponsorlogo , title , description 
		return vId + "," + vUrl + "," + vwCnt + "," + myLikeSts + " ," + lkCnt + "," + spLogoUrl + "," + vTitle + "," + desc + "," + "https://rewardthefan.com/sh/playvid.html?k="+vId;
		
		
	}
	
	
	public void setRtfTvMediaDets(String rtfTvMediaDets) {
		this.rtfTvMediaDets = rtfTvMediaDets;
	}
	public Integer getVwCnt() {
		if(vwCnt == null) vwCnt = 0;
		return vwCnt;
	}
	public void setVwCnt(Integer vwCnt) {
		this.vwCnt = vwCnt;
	}
	public Integer getLkCnt() {
		if(lkCnt == null)lkCnt = 0;
		return lkCnt;
	}
	public void setLkCnt(Integer lkCnt) {
		this.lkCnt = lkCnt;
	}
	public String getSpLogoUrl() {
		return spLogoUrl;
	}
	public void setSpLogoUrl(String spLogoUrl) {
		this.spLogoUrl = spLogoUrl; 
	}
	public String getMyLikeSts() {
		if(myLikeSts == null) myLikeSts = "0";
		return myLikeSts;
	}
	public void setMyLikeSts(String myLikeSts) {
		this.myLikeSts = myLikeSts;
	}	
	
	public String getAwsfname() {
		return awsfname;
	}
	public void setAwsfname(String awsfname) {
		this.awsfname = awsfname;
	}
	public Integer getvId() {
		return vId;
	}
	public void setvId(Integer vId) {
		this.vId = vId;
	}
	public String getvUrl() {
		return vUrl;
	}
	public void setvUrl(String vUrl) {
		this.vUrl = vUrl;
	}
	public String getvImg() {
		return vImg;
	}
	public void setvImg(String vImg) {
		this.vImg = vImg;
	}
	public String getvTitle() {
		if(vTitle == null) vTitle = "";
		return vTitle;
	}
	public void setvTitle(String vTitle) {
		this.vTitle = vTitle;
	}

}
