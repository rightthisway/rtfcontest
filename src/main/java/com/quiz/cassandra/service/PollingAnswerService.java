package com.quiz.cassandra.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.quiz.cassandra.list.PollingAnswerInfo;
import com.quiz.cassandra.list.PollingRewardsInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;

public class PollingAnswerService {	


public static PollingAnswerInfo processValidateAnswerResponse( PollingAnswerInfo pollingAnswerInfo) throws Exception{		
	
	pollingAnswerInfo = PollingSQLDaoUtil.fetchPollingAnswerDets(pollingAnswerInfo);	
	String correctAns = pollingAnswerInfo.getCorrectAns();	
	List <String> ansOpList = PollingUtil.getAnsOptionList();
	String userAnsIdx = pollingAnswerInfo.getAnswer();
	String userAns = ansOpList.get(Integer.parseInt(userAnsIdx));
	pollingAnswerInfo.setAnswer(userAns);
	//System.out.println("[PollingAnswerService][USER ANS][CORRECT ANS]" + userAns + "-"  + correctAns);
	
	if(PollingUtil.POLLINGTYPE_FEEDBACK.equals(pollingAnswerInfo.getPollingType()))
	{
		correctAns=userAns;		//  SKIP VALIDATION FOR FEEDBACK..
	}
	if(correctAns.equals(userAns)) {		
		
		pollingAnswerInfo = setRewardsForCustomer(pollingAnswerInfo);
		//String showMsg = PollingUtil.MOBILE_MSG_SUCCESS_POLL_PRIZE;
		String showMsg = PollingUtil.getPollingRwdMessage(pollingAnswerInfo.getRwdType() , pollingAnswerInfo.getRwdQty());
		if(showMsg != null )  showMsg = showMsg.replace("magicwand", "eraser");
		pollingAnswerInfo.setShowMsg(showMsg);
		updateAnswerRecordsForCustomer(pollingAnswerInfo);
		PollingSQLDaoUtil.updateCustomerRewardStats(pollingAnswerInfo);
		PollingSQLDaoUtil.cassUpdateCustomerRewards(pollingAnswerInfo );
		updateCustomerSQLRewardStats(pollingAnswerInfo );
		pollingAnswerInfo.setIsAnsCrt(1);
		pollingAnswerInfo.setSts(1);
		
		if(PollingUtil.REWARD_FUNMESSAGE.equals(pollingAnswerInfo.getRwdType()))
		{
			setMessageAsReward(pollingAnswerInfo);
			try {
			Integer pollInterval = PollingSQLDaoUtil.getPollingInterval(pollingAnswerInfo.getpCtId());
			if(pollInterval == null || pollInterval <= 0  ) {
				pollingAnswerInfo.setShowMsg(PollingUtil.MOBILE_FUN_MESSAGE_ALT);	
			}else {
				String interval = String.valueOf(pollInterval);
				String msg = PollingUtil.MOBILE_FUN_MESSAGE.replace("XX", interval);
				pollingAnswerInfo.setShowMsg(msg);
			}
			}catch(Exception ex) {
				ex.printStackTrace();
				pollingAnswerInfo.setShowMsg(PollingUtil.MOBILE_FUN_MESSAGE_ALT);	
			}
		}
		
		if(!PollingUtil.REWARD_FUNMESSAGE.equals(pollingAnswerInfo.getRwdType())) {
			if(pollingAnswerInfo.getRwdType() != null && pollingAnswerInfo.getRwdQty() > 0  ) {
				// upadte rewards  tracking ...
				try {					
					
					if(PollingUtil.REWARD_MAGICWAND.equals(pollingAnswerInfo.getRwdType()))
					{						
						/**
						 * 						
							(Integer customerId, String trxType, 
							SourceType sourceType, Integer sourceRefId,
							Integer lives,Integer magicWands, Integer sfStars, 
							Integer rtfPoints, Double rewardDollar, String description)
						 */
						SQLDaoUtil.saveCustomerRewardHistory(pollingAnswerInfo.getCuId(), "CR",
								SourceType.LIVE_POOLING, 0, 
								0, pollingAnswerInfo.getRwdQty(), 0,0, 0.0,
								"Polling Question " + pollingAnswerInfo.getId());				
						
					}
					else if(PollingUtil.REWARD_LIFE.equals(pollingAnswerInfo.getRwdType()))
					{
						SQLDaoUtil.saveCustomerRewardHistory(pollingAnswerInfo.getCuId(), "CR",
								SourceType.LIVE_POOLING, 0, 
								pollingAnswerInfo.getRwdQty(), 0, 0,0, 0.0,
								"Polling Question " + pollingAnswerInfo.getId());	
					}
					else if(PollingUtil.REWARD_SFSTARS.equals(pollingAnswerInfo.getRwdType()))
					{
						SQLDaoUtil.saveCustomerRewardHistory(pollingAnswerInfo.getCuId(), "CR",
								SourceType.LIVE_POOLING, 0, 
								0, 0, pollingAnswerInfo.getRwdQty(),0, 0.0,
								"Polling Question " + pollingAnswerInfo.getId());	
					}
					else if(PollingUtil.REWARD_RTFPOINTS.equals(pollingAnswerInfo.getRwdType())) 
					{
						SQLDaoUtil.saveCustomerRewardHistory(pollingAnswerInfo.getCuId(), "CR", SourceType.LIVE_POOLING, 0, 0, 0, 0,
								pollingAnswerInfo.getRwdQty(), 0.0, "Polling Question " + pollingAnswerInfo.getId());
						
					}
					else {
						//do nothing ...
					}
					

				} catch (Exception ex) {
					ex.printStackTrace();
					//do nothing ...as only audit has failed
				}
				
				
			}
			
		}
		
		
		
		//System.out.println("[PollingAnswerService] [Success Msg]" + showMsg);
	}
	else {
		
		pollingAnswerInfo.setShowMsg(PollingUtil.MOBILE_MSG_WRONG_ANSWER);
		pollingAnswerInfo.setRwdQty(0);
		updateAnswerRecordsForCustomer(pollingAnswerInfo);
		//System.out.println("[PollingAnswerService] [WRONG ANSWER][CORRECT ANS TEXT]" + pollingAnswerInfo.getCrtAnsTxt() );
		pollingAnswerInfo.setIsAnsCrt(0);
		pollingAnswerInfo.setSts(1);
	}
	
	pollingAnswerInfo.setNxtCTxt(PollingUtil.getNextGameDetails());
	
	return pollingAnswerInfo;
}

private static void setMessageAsReward(PollingAnswerInfo pollingAnswerInfo) {
	List <String> msgList = PollingSQLDaoUtil.getMessageListForCustomerRewards();
	Collections.shuffle(msgList);
	String msg = msgList.get(0);
	pollingAnswerInfo.setShowMsg(msg);
	
}

private static Integer updateCustomerSQLRewardStats(PollingAnswerInfo pollingAnswerInfo) {
	
	String rewardType = pollingAnswerInfo.getRwdType();
	Integer rewardQty = PollingUtil.getZeroIfNull(pollingAnswerInfo.getRwdQty());
	
	if(PollingUtil.isEmptyOrNull(rewardType))  return 0;
	if(rewardQty == 0 ) return 0;
	if(PollingUtil.REWARD_LIFE.equals(rewardType)) {
		return	SQLDaoUtil.rewardCustomerWithLives(pollingAnswerInfo.getCuId(), rewardQty);
	}
	else if(PollingUtil.REWARD_SFSTARS.equals(rewardType)) {
		
		try {
			PollingSQLDaoUtil.saveOrUpdatePollingRedeemSuperFanStars(pollingAnswerInfo.getCuId(), rewardQty);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return SQLDaoUtil.rewardCustomerWithSuperFanStars(pollingAnswerInfo.getCuId(),rewardQty);
		
	}
	else if (PollingUtil.REWARD_MAGICWAND.equals(rewardType)){
		// PollingSQLDaoUtil.updateCustomerRewardStats(pollingAnswerInfo);
		
		 return SQLDaoUtil.rewardCustomerWithMagicWands(pollingAnswerInfo.getCuId(),rewardQty);
	}
	else if (PollingUtil.REWARD_RTFPOINTS.equals(rewardType)) {
		 SQLDaoUtil.rewardCustomerWithRTFPoints(pollingAnswerInfo.getCuId(),rewardQty);
		 return 1;
	}
	else {
		return 0;
	}

}

private static void updateAnswerRecordsForCustomer(PollingAnswerInfo pollingAnswerInfo) {
	PollingSQLDaoUtil.updateCustomerAnswerDetails(pollingAnswerInfo);
	
}

private static PollingAnswerInfo setRewardsForCustomer(PollingAnswerInfo pollingAnswerInfo) {
	
	boolean isMessageTypeReward = isAnswerEligibleForRewards(pollingAnswerInfo);
	if(isMessageTypeReward == true) {
		pollingAnswerInfo.setRwdType(PollingUtil.REWARD_FUNMESSAGE);
		pollingAnswerInfo.setRwdQty(0);
		return pollingAnswerInfo;
	}
	
	PollingRewardsInfo pollingRewards = PollingSQLDaoUtil.getPollingRewardsForContest(pollingAnswerInfo.getpCtId()) ;
	//System.out.println("[PollingAnswerService][pollingRewards] "  + pollingRewards.getContestId());
	Map<String, Integer> pollingRwdMap = new HashMap<String, Integer>();
		/*
		 * if(pollingRewards.getCrstballPerQue() != null &&
		 * pollingRewards.getCrstballPerQue() > 0) {
		 * pollingRwdMap.put(PollingUtil.REWARD_CRYSTALL_BALL ,
		 * pollingRewards.getCrstballPerQue()); }
		 */
	
		/*
		 * if(pollingRewards.getGbricksPerQue() != null &&
		 * pollingRewards.getGbricksPerQue() > 0) {
		 * pollingRwdMap.put(PollingUtil.REWARD_GOLDBARS ,
		 * pollingRewards.getGbricksPerQue()); }
		 */
		/*
		 * if(pollingRewards.getHifivePerQue() != null &&
		 * pollingRewards.getHifivePerQue() >0) {
		 * pollingRwdMap.put(PollingUtil.REWARD_HIFIVE,
		 * pollingRewards.getHifivePerQue()); }
		 */
	if(pollingRewards.getEraserPerQue() != null  && pollingRewards.getEraserPerQue() > 0) {
		pollingRwdMap.put(PollingUtil.REWARD_MAGICWAND , pollingRewards.getEraserPerQue());
	}
	if(pollingRewards.getLifePerQue() != null   && pollingRewards.getLifePerQue() >0) {
		pollingRwdMap.put(PollingUtil.REWARD_LIFE, pollingRewards.getLifePerQue());
	}
	if(pollingRewards.getSupFanStarsPerQue() != null   && pollingRewards.getSupFanStarsPerQue() >0) {
		pollingRwdMap.put(PollingUtil.REWARD_SFSTARS, pollingRewards.getSupFanStarsPerQue());
	}
	if(pollingRewards.getRtfPointsPerQue() != null   && pollingRewards.getRtfPointsPerQue() >0) {
		pollingRwdMap.put(PollingUtil.REWARD_RTFPOINTS, pollingRewards.getRtfPointsPerQue());
	}
	
	
	List<String> rwdCcyList = new ArrayList<String>();
	rwdCcyList.addAll(pollingRwdMap.keySet());
			//System.out.println("[PollingAnswerService][RewardCurrency List]-" + rwdCcyList.size() );
			//System.out.println(rwdCcyList);
	String ccyTpe= PollingUtil.fetchRandomRewardType(rwdCcyList);
			//System.out.println("[PollingAnswerService][RANDOM CCY]--"  + ccyTpe);
	int maxUnit = pollingRwdMap.get(ccyTpe);
			//System.out.println("[PollingAnswerService][ CCY MAX UNIT]--"  + maxUnit);
	int rwdUnit = PollingUtil.getRandomRwdUnit(maxUnit);
	pollingAnswerInfo.setRwdType(ccyTpe);
	pollingAnswerInfo.setRwdQty(rwdUnit);
			//System.out.println("[PollingAnswerService][ CCY RANDOM REWARD UNIT]--"  + rwdUnit);
	return pollingAnswerInfo;
}

private static boolean isAnswerEligibleForRewards(PollingAnswerInfo pollingAnswerInfo) {		
	Integer quesCount = PollingSQLDaoUtil.getQuestioNumOftheContestForCustomer(pollingAnswerInfo);
	if(quesCount == null)  return false;
	if(quesCount <=0 )  return false;	
	String randMsgQidStr = PollingSQLDaoUtil.fetchFunMessageQuestioNums(pollingAnswerInfo);
	if(randMsgQidStr == null ) return false;
	List<String> list = 
			  Stream.of(randMsgQidStr.split(","))
			  .collect(Collectors.toCollection(ArrayList<String>::new));
	if(list == null  || list.size() <= 0 )  return false;
	if(list.contains(String.valueOf(quesCount)))	
	return true;
	
	//Integer oQId = pollingAnswerInfo.getoQId();
	boolean qIdWonRwds = PollingSQLDaoUtil.validateIfPollQWonRwardsEarlier(pollingAnswerInfo);
	if(qIdWonRwds == true ) return true;
	return false;
}

public static void main(String a[]) throws Exception{
	PollingAnswerInfo pollingAnswerInfo = new PollingAnswerInfo();
	pollingAnswerInfo.setCuId(2);
	pollingAnswerInfo.setpCtId(1);
	pollingAnswerInfo.setId(1);
	pollingAnswerInfo.setAnswer("1");
	processValidateAnswerResponse( pollingAnswerInfo) ;
	
	System.out.println(pollingAnswerInfo.getNxtCTxt());
}

}
