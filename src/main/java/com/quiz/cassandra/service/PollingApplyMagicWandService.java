package com.quiz.cassandra.service;

import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.ApplyMagicWandInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class PollingApplyMagicWandService {
	
	public static ApplyMagicWandInfo processApplyMagicWand(ApplyMagicWandInfo applyMagicWandInfo , CassCustomer customer)  throws Exception {		
		
		Integer custId = customer.getId();
		Integer mwQty = PollingUtil.getZeroIfNull(customer.getMw());		
		String rwdType = PollingUtil.REWARD_MAGICWAND;
		Integer updateQty = mwQty - 1;
		if(updateQty <= 0 ) updateQty = 0 ;
		updateCassCustomerStats(custId , updateQty , rwdType);
		updateSQLCustomerStats(custId , 1 , rwdType);
		return applyMagicWandInfo;
		
	}

	private static void updateSQLCustomerStats(Integer custId, Integer updateQty, String rwdType) throws Exception {
		PollingSQLDaoUtil.updateCustomerAccountForRedeemedFromQty(custId,rwdType ,1);
		PollingSQLDaoUtil.applyCustomerMagicWandStat(custId, 1);
		
	}

	private static void updateCassCustomerStats(Integer custId, Integer updateQty, String rwdType) throws Exception {		
		String cassSQL = " update customer set magicwand = "  + updateQty  + "  where  customer_id = " + custId;
		CassandraConnector.getSession().execute(cassSQL);
		
	}
	
	public static void main (String a []) throws Exception{
		ApplyMagicWandInfo magicWandInfo  = new ApplyMagicWandInfo();
		magicWandInfo.setCuId(2);
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(2);
		
		processApplyMagicWand( magicWandInfo ,  customer);
		
		
	}
	
	
	

}
