package com.quiz.cassandra.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.exception.NoRewardConfigException;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CustRewardValueInfo;
import com.quiz.cassandra.list.MediaReward;
import com.quiz.cassandra.list.MyVideoInfo;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.PollingVideoRewardInfo;
import com.quiz.cassandra.list.PollingVideoRewardsConfig;
import com.quiz.cassandra.list.RewardsInfo;
import com.quiz.cassandra.list.RtfRewardConfigInfo;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;
import com.zonesws.webservices.utils.URLUtil;

public class CustomerMediaServices {

	public static VideoLikeInfo processVideoFavourite(VideoLikeInfo videoLikeInfo, CassCustomer customer)
			throws Exception {

		Integer videoId = videoLikeInfo.getVidId();
		Integer cnt = manageVideoFavouriteStats(videoId, customer.getId(), videoLikeInfo.getIsFavAdd());
		videoLikeInfo.setSts(cnt);
		return videoLikeInfo;

	}

	private static Integer manageVideoFavouriteStats(Integer videoId, Integer custId, String isFavAdd)
			throws Exception {
		if (isFavAdd == null || PollingUtil.NO.equals(isFavAdd))
			isFavAdd = "0";
		else
			isFavAdd = "1";

		Integer cnt = PollingSQLDaoUtil.updateVideoFavStats(videoId, custId, isFavAdd);
		if (cnt <= 0) {
			System.out.println(" No Records as cnt <= 0  : " + cnt);
			cnt = PollingSQLDaoUtil.insertVideoFavStats(videoId, custId, isFavAdd);
		} else {
			System.out.println("Update count as cnt > 0 =  : " + cnt);
		}
		return cnt;
	}

	public static VideoLikeInfo processMediaShare(CassCustomer customer, VideoLikeInfo videoInfo) throws Exception {
		Integer rtfPoints = 0;
		try {

			int updCnt = PollingSQLDaoUtil.insertMediaSharedStats(videoInfo);
					
			try {
				CustRewardValueInfo cro  = CustomerRewardLimtService.processShareVideoRewards( customer);
				Integer credStatus = 0;
				if (cro.getIsRewarded() == true) {
					Integer currentPoint = customer.getSfcCount();
					if (currentPoint == null)
						currentPoint = 0;
					Integer gainedRtfPoints = cro.getRwdQty();
					if (gainedRtfPoints == null)
						gainedRtfPoints = 0;
					Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;										
					PollingSQLDaoUtil.saveOrUpdatePollingRedeemSuperFanStars(customer.getId(),gainedRtfPoints);
					PollingSQLDaoUtil.updateCassSFStars(cassFinalrtfPoints , customer.getId());					
					
						String msg = PollingUtil.VIDEO_SHARES.replace("00", String.valueOf(gainedRtfPoints));
						Integer sts = 1;
						Integer pts = cassFinalrtfPoints;
						Double rDlrs = customer.getcRewardDbl();
						RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
						videoInfo.setRwdInfo(rwdInfo);						
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", SourceType.VIDEO_SHARES, videoInfo.getVidId(),
								0, 0, gainedRtfPoints, 0, 0.0, " Video shared " + videoInfo.getSource());
					}
				
				videoInfo.setSts(1);
			}catch (Exception ex) {
				ex.printStackTrace();
				videoInfo.setSts(0);
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			videoInfo.setSts(updCnt);
		} catch (Exception ex) {
			System.out.println("Exception ... Sharing Video ..");
			ex.printStackTrace();
			videoInfo.setSts(0);
		} finally {
			try {
				SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", SourceType.VIDEO_SHARES, 0, 0, 0, 0,
						rtfPoints, 0.0, "Shared Video Id: " + videoInfo.getmId());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return videoInfo;
	}

	public static Integer allotMediaUploadRTFPoints(CassCustomer customer, VideoLikeInfo videoInfo) {
		Integer status = 0;
		Integer currRtfPoints = customer.getRtfPoints();
		if (currRtfPoints == null)
			currRtfPoints = 0;
		Integer rwdConfigPts = 0;
		try {

			RtfRewardConfigInfo rtfRwdConfigInfo = CustomerRewardCreditService
					.getRewardConfigurationForRTFActionType(SourceType.VIDEO_UPLOAD.toString());
			if (rtfRwdConfigInfo == null) {
				rwdConfigPts = 0;
				throw new NoRewardConfigException();
			} else
				rwdConfigPts = rtfRwdConfigInfo.getRtfPoints();

			if (rwdConfigPts == null || rwdConfigPts < 0)
				rwdConfigPts = 0;
			Integer finalRtfPoints = currRtfPoints + rwdConfigPts;
			System.out.println("-finalRtfPoints video TFF Share allot- " + finalRtfPoints);

			CustomerRewardCreditService.sqlCreditToCustomerRTFPoints(customer.getId(), rwdConfigPts);
			CustomerRewardCreditService.cassCreditCustomerRTFPoints(customer.getId(), finalRtfPoints);
			status = 1;
		} catch (NoRewardConfigException noRewEx) {
			CassError err = new CassError();
			err.setDesc("Please configure the rewards for Video Uploads");
			videoInfo.setErr(err);
			videoInfo.setSts(0);
			status = 0;
			return 0;
		}

		catch (Exception ex) {
			ex.printStackTrace();
			status = 0;
			return 0;
		} finally {
			try {
				SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", SourceType.VIDEO_UPLOAD, 0, 0, 0, 0,
						rwdConfigPts, 0.0, "TFF Approved uploaded Video Id: " + videoInfo.getmId());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return status;
	}

	/*
	 * (Integer customerId, String trxType, SourceType sourceType, Integer
	 * sourceRefId, Integer lives,Integer magicWands, Integer sfStars, Integer
	 * rtfPoints, Double rewardDollar, String description)
	 */
	public static MediaReward allotRandomMediaRewardsToCustomer(CassCustomer customer, SourceType sourceType,
			Integer mediaId) {
		
		Integer status = 0;
		Integer rwdConfigPts = 0;
		Integer rwdLife = 0;
		Integer rwdStars = 0;
		Integer magicWands = 0;
		List<String> rwdTypeList = new ArrayList<String>();
		MediaReward mediaReward = new MediaReward();
		mediaReward.setCuId(customer.getId());
		mediaReward.setmId(mediaId);
		
		CassError casserror = new CassError();
		try {

			RtfRewardConfigInfo rtfRwdConfigInfo = CustomerRewardCreditService
					.getRewardConfigurationForRTFActionType(sourceType.toString());
			if (rtfRwdConfigInfo == null) {
				rwdConfigPts = 0;
				throw new NoRewardConfigException();
			} else {
				rwdConfigPts = rtfRwdConfigInfo.getRtfPoints();
				rwdLife = rtfRwdConfigInfo.getLives();
				magicWands = rtfRwdConfigInfo.getMagicWands();
				rwdStars = rtfRwdConfigInfo.getSfStars();
			}

			if (rwdConfigPts != null && rwdConfigPts > 0) {
				rwdTypeList.add(PollingUtil.REWARD_RTFPOINTS);
			} 
			if (rwdLife != null && rwdLife > 0) {
				rwdTypeList.add(PollingUtil.REWARD_LIFE);
			} 
			if (rwdStars != null && rwdStars > 0) {
				rwdTypeList.add(PollingUtil.REWARD_SFSTARS);
			} 
			if (magicWands != null && magicWands > 0) {
				rwdTypeList.add(PollingUtil.REWARD_MAGICWAND);
			}

			if (rwdTypeList.size() == 0) {
				throw new NoRewardConfigException();
			}
			Collections.shuffle(rwdTypeList);
			String rewardTypeSelected = rwdTypeList.get(0);

			if (PollingUtil.REWARD_RTFPOINTS.equals(rewardTypeSelected)) {

				Integer sqlPts = rwdConfigPts;
				Integer currRtfPoints = customer.getRtfPoints();
				if (currRtfPoints == null)
					currRtfPoints = 0;
				Integer cassFinalPts = currRtfPoints + rwdConfigPts;
				status = updateRTFPointsforCustomer(customer.getId(), sqlPts, cassFinalPts);

				try {
					if (status == 1) {
						mediaReward.setRwdCode(PollingUtil.REWARD_RTFPOINTS);
						mediaReward.setSts(1);
						mediaReward.setRwdQty(sqlPts);
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, mediaId, 0, 0,
								0, rwdConfigPts, 0.0, " Video Id: " + mediaId);
						return mediaReward;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					casserror.setDesc(ex.getStackTrace().toString());
					mediaReward.setSts(0);
					mediaReward.setMsg(URLUtil.genericErrorMsg);
					return mediaReward;
				}
			}

			if (PollingUtil.REWARD_LIFE.equals(rewardTypeSelected)) {

				Integer sqlPts = rwdConfigPts;
				Integer currLives = customer.getqLive();
				if (currLives == null)
					currLives = 0;
				Integer cassFinalLives = currLives + rwdConfigPts;
				status = updateRTFLivesforCustomer(customer.getId(), sqlPts, cassFinalLives);

				try {
					if (status == 1) {
						
						mediaReward.setRwdCode(PollingUtil.REWARD_LIFE);
						mediaReward.setSts(1);
						mediaReward.setRwdQty(sqlPts);
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, mediaId,
								rwdConfigPts, 0, 0, 0, 0.0, " Video Id: " + mediaId);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					casserror.setDesc(ex.getStackTrace().toString());
					mediaReward.setSts(0);
					mediaReward.setMsg(URLUtil.genericErrorMsg);
					return mediaReward;
				}
			}

			if (PollingUtil.REWARD_MAGICWAND.equals(rewardTypeSelected)) {

				Integer sqlPts = rwdConfigPts;
				Integer currMw = customer.getMw();
				if (currMw == null)
					currMw = 0;
				Integer cassFinalLives = currMw + rwdConfigPts;
				status = updateRTFMagicWandforCustomer(customer.getId(), sqlPts, cassFinalLives);

				try {
					if (status == 1) {
						mediaReward.setRwdCode(PollingUtil.REWARD_MAGICWAND);
						mediaReward.setSts(1);
						mediaReward.setRwdQty(sqlPts);
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, mediaId, 0,
								rwdConfigPts, 0, 0, 0.0, " Video Id: " + mediaId);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					casserror.setDesc(ex.getStackTrace().toString());
					mediaReward.setSts(0);
					mediaReward.setMsg(URLUtil.genericErrorMsg);
					return mediaReward;
				}
			}

			if (PollingUtil.REWARD_SFSTARS.equals(rewardTypeSelected)) {

				Integer sqlPts = rwdConfigPts;
				Integer currMw = customer.getMw();
				if (currMw == null)
					currMw = 0;
				Integer cassFinalLives = currMw + rwdConfigPts;
				status = updateRTFSFStarforCustomer(customer.getId(), sqlPts, cassFinalLives);

				try {
					if (status == 1) {
						mediaReward.setRwdCode(PollingUtil.REWARD_SFSTARS);
						mediaReward.setSts(1);
						mediaReward.setRwdQty(sqlPts);
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, mediaId, 0, 0,
								rwdConfigPts, 0, 0.0, " Video Id: " + mediaId);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					casserror.setDesc(ex.getStackTrace().toString());
					mediaReward.setSts(0);
					mediaReward.setMsg(URLUtil.genericErrorMsg);
					return mediaReward;
				}
			}

		} catch (NoRewardConfigException noRewEx) {			
			System.out.println("NoRewardConfigException: Please configure  rewards for " + sourceType.toString()  );
			casserror.setDesc(" NoRewardConfigException: Please configure  rewards for " + sourceType.toString());
			mediaReward.setSts(0);
			mediaReward.setMsg(URLUtil.genericErrorMsg);
			return mediaReward;
		}

		catch (Exception ex) {
			ex.printStackTrace();
			casserror.setDesc(ex.getStackTrace().toString());
			mediaReward.setSts(0);
			mediaReward.setMsg(URLUtil.genericErrorMsg);
			return mediaReward;
		} finally {
			try {
				//Add clean up objects ..
				} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return mediaReward;
	}

	private static Integer updateRTFLivesforCustomer(Integer cuId, Integer sqlPts, Integer cassFinalStat) {
		Integer sts = CustomerRewardCreditService.sqlCreditToCustomerLives(cuId, sqlPts);
		if (sts > 0) {
			sts = CustomerRewardCreditService.cassCreditToCustomerLives(cuId, cassFinalStat);
		}
		return sts;

	}

	private static Integer updateRTFPointsforCustomer(Integer cuId, Integer sqlPts, Integer cassFinalPts) {
		Integer sts = CustomerRewardCreditService.sqlCreditToCustomerRTFPoints(cuId, sqlPts);
		if (sts > 0) {
			sts = CustomerRewardCreditService.cassCreditCustomerRTFPoints(cuId, cassFinalPts);
		}
		return sts;
	}

	private static Integer updateRTFMagicWandforCustomer(Integer cuId, Integer sqlPts, Integer cassFinalPts) {
		Integer sts = CustomerRewardCreditService.creditCustomerRTFMagicWands(cuId, cassFinalPts, sqlPts);
		return sts;
	}

	private static Integer updateRTFSFStarforCustomer(Integer cuId, Integer sqlPts, Integer cassFinalPts) {
		Integer sts = CustomerRewardCreditService.creditCustomerRTFSuperFanStar(cuId, cassFinalPts, sqlPts);
		return sts;
	}

	public static PollingVideoInfo fetchCustomerLikedVideos(Integer cuId, PollingVideoInfo pollingVideoInfo) {

		Map<Integer, MyVideoInfo> videoInfoMap = PollingSQLDaoUtil.fetchCustLikedVideos(cuId);
		if (videoInfoMap == null || videoInfoMap.size() == 0)
			return pollingVideoInfo;
		List<Integer> vIdList = videoInfoMap.keySet().stream().collect(Collectors.toList());
		// id , url , viewcount , 1 , likecount , sponsorlogo
		// fetch liked videos id and url ;
		// fetch liked count , viewed count

		//Map<Integer, Integer> vwCntMap = PollingSQLDaoUtil.getVideoViewCount(vIdList);
		//Map<Integer, Integer> lkCntMap = PollingSQLDaoUtil.getVideoLikedCount(vIdList, 0);
		for (Integer id : vIdList) {

		//	if (videoInfoMap.containsKey(id)) {
			//	((MyVideoInfo) videoInfoMap.get(id)).setVwCnt(vwCntMap.get(id));
			//	((MyVideoInfo) videoInfoMap.get(id)).setLkCnt(lkCntMap.get(id));
				((MyVideoInfo) videoInfoMap.get(id)).setMyLikeSts("1");
				String vidInfoStr = ((MyVideoInfo) videoInfoMap.get(id)).getRtfTvMediaDets();
				System.out.println("== my liked videos vidInfoStr==" + vidInfoStr);
				((MyVideoInfo) videoInfoMap.get(id)).setRtfTvMediaDets(vidInfoStr);

		//	}

		}
		List<MyVideoInfo> vidInfoList = new ArrayList<MyVideoInfo>(videoInfoMap.values());
		pollingVideoInfo.setMyVidInfoList(vidInfoList);
		return pollingVideoInfo;
	}
	
	public static PollingVideoRewardInfo processVideoViewRewards(CassCustomer customer , PollingVideoRewardInfo pollingVideoRewardInfo) throws Exception{
		try {
		pollingVideoRewardInfo.setSts(0);
		PollingVideoRewardsConfig pollingVideoRewardsConfig  = PollingSQLDaoUtil.getVideoRewardsConfig();
		if(pollingVideoRewardsConfig == null || pollingVideoRewardsConfig.getId() == null) {
			pollingVideoRewardInfo.setSts(0);
			//pollingVideoRewardInfo.setShowMsg(PollingUtil.GENERAL_ERROR);
			return pollingVideoRewardInfo;
		}		
		Integer maxCustRwdLimit = pollingVideoRewardsConfig.getMaxRwdsPerDay();
		if(maxCustRwdLimit == null || maxCustRwdLimit == 0) {
			pollingVideoRewardInfo.setSts(0);
			//pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_MAX_NOTSET_ERROR);
			System.out.println("Error as table for video rewards not configured" + PollingUtil.VIDEO_REWARD_MAX_NOTSET_ERROR);
			return pollingVideoRewardInfo;
		}
		
		Integer videoRewardCountforTheDay = PollingSQLDaoUtil.getVideoRewardCountForTheDay(pollingVideoRewardInfo.getCuId());
		if(videoRewardCountforTheDay == null ) videoRewardCountforTheDay = 0 ; 
		if(videoRewardCountforTheDay > 0) {
			pollingVideoRewardInfo.setSts(0);
			System.out.println("Error as table for video rewards not configured "  + PollingUtil.VIDEO_REWARD_MAX_ERROR);
			//pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_MAX_ERROR);
			return pollingVideoRewardInfo;
		}		
		
		Integer rewardCntForVideoId = PollingSQLDaoUtil.custRewardCountForSameVideo(pollingVideoRewardInfo.getCuId() , pollingVideoRewardInfo.getAwsVideoId());
		if(rewardCntForVideoId > 0) {			
			pollingVideoRewardInfo.setSts(0);
			System.out.println( PollingUtil.VIDEO_VIEW_REWARD_DUPLICATE);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_VIEW_REWARD_DUPLICATE);
			return pollingVideoRewardInfo;
		}	
		
		MediaReward mediaReward= CustomerMediaServices.allotRandomMediaRewardsToCustomer(customer,SourceType.VIDEO_WATCH,Integer.valueOf(pollingVideoRewardInfo.getAwsVideoId()));
		if(mediaReward == null || mediaReward.getSts() ==0) throw new Exception();
		pollingVideoRewardInfo.setSts(mediaReward.getSts());	
		pollingVideoRewardInfo.setRwdType(mediaReward.getRwdCode());
		pollingVideoRewardInfo.setRwdQty(mediaReward.getRwdQty());
		String rwdMsg = PollingUtil.VIDEO_REWARD_MGS1;			
		if(pollingVideoRewardInfo.getSts() == 1 ) {
			
			String rewardCode = pollingVideoRewardInfo.getRwdType();
			String rewardTxt  = (String)PollingUtil.getRewardStatsColumnForRwdType(rewardCode);
			rwdMsg = rwdMsg.replace("XX", rewardTxt);
			rwdMsg = rwdMsg.replace("YYY", String.valueOf(pollingVideoRewardInfo.getRwdQty()))	;	
		}
		else {
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.GENERAL_ERROR);
		}
		
		}catch(Exception ex) {
			ex.printStackTrace();
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.GENERAL_ERROR);
		}
		return pollingVideoRewardInfo;	
	}
	
	
	public static void main(String a[]) throws Exception {
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(2);
		PollingVideoInfo pollingVideoInfo = new PollingVideoInfo();
		//pollingVideoInfo.setCatId("DEF");
		pollingVideoInfo.setCatId("29");
		pollingVideoInfo.setAppver("n");
		pollingVideoInfo.setCuId(2);
		//pollingVideoInfo.setr
		// System.out.print(processVideoUrlforCustomer(customer , pollingVideoInfo ));
		//System.out.print(processVideoUrlListforCustomer(customer, pollingVideoInfo).getvUrlLst());
		CustomerMediaServices.fetchVideoPlayList( customer,  pollingVideoInfo);
		
	}
	
	
	

	/*
	 * public static void main(String a[]) { PollingVideoInfo pollingVideoInfo =
	 * fetchCustomerLikedVideos(351111, new PollingVideoInfo());
	 * System.out.println(pollingVideoInfo);
	 * 
	 * 
	 * 
	 * }
	 */

	public static PollingVideoInfo fetchVideoPlayList(CassCustomer customer, PollingVideoInfo pollingVideoInfo) {
	
			Integer cuId = customer.getId();			
			Map<Integer, PollingVideoInfo> vUrlLMap = PollingSQLDaoUtil.getVideoUrlListForCustomer(pollingVideoInfo.getCatId() , cuId);
			
			if(vUrlLMap == null || vUrlLMap.size() == 0 ) {
				PollingSQLDaoUtil.deletePollingVideoPlayedListByCustomer(cuId);
				vUrlLMap = PollingSQLDaoUtil.getVideoUrlListForCustomer(pollingVideoInfo.getCatId() , cuId);
			}
			
			if(vUrlLMap == null || vUrlLMap.size() == 0 ) {
				pollingVideoInfo.setSts(0);
				return pollingVideoInfo;
			}		
			if(vUrlLMap != null && vUrlLMap.size() <5 ) {
				
			
				vUrlLMap = PollingSQLDaoUtil.getAddtnlVideoUrlListForCustomer(pollingVideoInfo.getCatId() , cuId, new ArrayList<Integer>(vUrlLMap.keySet()),vUrlLMap);
			
			}	
			 List<Integer> vidIdList =  new ArrayList<Integer>(vUrlLMap.keySet());
			// Map<Integer , Integer> videoViewedCountMap = PollingSQLDaoUtil.getVideoViewCount(vidIdList) ;
			 List<Integer> hasAlreadyLikedList = PollingSQLDaoUtil.getVideosLikedByCust(vidIdList , pollingVideoInfo.getCuId());
			// Map<Integer, Integer>  videoLikedCountMap = PollingSQLDaoUtil.getVideoLikedCount(vidIdList , pollingVideoInfo.getCuId() ) ;
			
		    // updateViewCountInfo(videoViewedCountMap,vUrlLMap);
		    // updateLikeCountInfo(videoLikedCountMap,vUrlLMap);
		     updateMyLikeInfo(hasAlreadyLikedList ,vUrlLMap);
		     pollingVideoInfo =  prepareVideoPlaylistDataMessage(vidIdList,vUrlLMap,pollingVideoInfo);
		     
		     List<String> catList = PollingSQLDaoUtil.getVideoCategoryList();
		     pollingVideoInfo.setvCatListRel1(catList);
		     return pollingVideoInfo;
	}

	private static PollingVideoInfo prepareVideoPlaylistDataMessage(List<Integer> vidIdList,Map<Integer, PollingVideoInfo> vUrlLMap, PollingVideoInfo pollingVideoInfo) {
		
		
		 List <String> finalUrlList =new ArrayList<String>(); 
		 for (Integer id: vidIdList)  {
			 PollingVideoInfo vidInfo  = (PollingVideoInfo)vUrlLMap.get(id);					 
			 String tmp = id + "," + vidInfo.getvUrl() + "," +  vidInfo.getVwCnt() + "," +  
			 vidInfo.getIsLk() + "," +  vidInfo.getLkCnt() + "," +  vidInfo.getSplogo() +
			 "," + vidInfo.getTitle() + "," + vidInfo.getDescription() + "," + "https://rewardthefan.com/sh/playvid.html?k="+id;
			 finalUrlList.add(tmp);			 
		 }		
		 pollingVideoInfo.setvUrlListRel1(finalUrlList);
		 
		 System.out.println( " new -- " + pollingVideoInfo.getvUrlListRel1());
		 return pollingVideoInfo;		
	}


	private static void updateViewCountInfo(Map<Integer, Integer> videoViewedCountMap,
			Map<Integer, PollingVideoInfo> vUrlLMap) {
		 List<Integer> vidIdList =  new ArrayList<Integer>(videoViewedCountMap.keySet());
		for(Integer vidId:vidIdList) {
			vUrlLMap.get(vidId).setVwCnt(videoViewedCountMap.get(vidId));
		}
		
	}
	private static void updateLikeCountInfo(Map<Integer, Integer> videoLikedCountMap,
			Map<Integer, PollingVideoInfo> vUrlLMap) {
		 List<Integer> vidIdList =  new ArrayList<Integer>(videoLikedCountMap.keySet());
		for(Integer vidId:vidIdList) {
			vUrlLMap.get(vidId).setLkCnt(videoLikedCountMap.get(vidId));
		}
		
	}
	private static void updateMyLikeInfo(List<Integer> hasAlreadyLikedList,
			Map<Integer, PollingVideoInfo> vUrlLMap) {
		
		for(Integer vidId:hasAlreadyLikedList) {
			vUrlLMap.get(vidId).setIsLk(1);
		}
		
	}
	
	public static List<MyVideoInfo> fetchLikedCountForVidIds(Map<Integer,MyVideoInfo> videoInfoMap,Integer cuId) {
		
		if( videoInfoMap  == null || videoInfoMap.size() == 0  )  return new ArrayList<MyVideoInfo>();				
		
		try {
		List<Integer> vidIdList =  new ArrayList<Integer>(videoInfoMap.keySet());
			/*
			 * Map<Integer , Integer> videoViewedCountMap =
			 * PollingSQLDaoUtil.getVideoViewCount(vidIdList);
			 * 
			 * for(Integer vidId:vidIdList) {
			 * videoInfoMap.get(vidId).setVwCnt(videoViewedCountMap.get(vidId)); }
			 */
		
		 List<Integer> hasAlreadyLikedList = PollingSQLDaoUtil.getVideosLikedByCust(vidIdList ,cuId	);
		 System.out.println("hasAlreadyLikedList my media " + hasAlreadyLikedList);
		 for(Integer vidId:hasAlreadyLikedList) {
			 System.out.println(" before set of is like   " + videoInfoMap.get(vidId).getRtfTvMediaDets() ) ;
			 videoInfoMap.get(vidId).setMyLikeSts("1");
			 
			 System.out.println(" After set of is like   " + videoInfoMap.get(vidId).getRtfTvMediaDets() ) ;
		 }
		 
			/*
			 * Map<Integer, Integer> videoLikedCountMap =
			 * PollingSQLDaoUtil.getVideoLikedCount(vidIdList , cuId);
			 * 
			 * for(Integer vidId:vidIdList) {
			 * videoInfoMap.get(vidId).setVwCnt(videoLikedCountMap.get(vidId)); }
			 */
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		 List<MyVideoInfo> finalList =  new ArrayList<MyVideoInfo>(videoInfoMap.values());	
		 return finalList;
	}
	
	
	
	public static PollingVideoRewardInfo processVideoWatchRewards(CassCustomer customer , PollingVideoRewardInfo pollingVideoRewardInfo) {
		try {
		CustRewardValueInfo cro  = CustomerRewardLimtService.processVideoViewRewards( customer,
				 pollingVideoRewardInfo);
		Integer credStatus = 0;
		if (cro.getIsRewarded() == true) {
			Integer currentPoint = customer.getRtfPoints();
			if (currentPoint == null)
				currentPoint = 0;
			Integer gainedRtfPoints = cro.getRwdQty();
			if (gainedRtfPoints == null)
				gainedRtfPoints = 0;
			Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
			credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
					gainedRtfPoints);
			if (credStatus > 0) {
				String msg = PollingUtil.VIDEO_WATCH_REWARDS.replace("00", String.valueOf(gainedRtfPoints));
				Integer sts = 1;
				Integer pts = cassFinalrtfPoints;
				Double rDlrs = customer.getcRewardDbl();
				RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
				pollingVideoRewardInfo.setRwdInfo(rwdInfo);
				
				SQLDaoUtil.saveCustomerRewardHistory(
						customer.getId(), "CR", 
						SourceType.WATCH_VIDEO, 
						Integer.valueOf(pollingVideoRewardInfo.getAwsVideoId()),
						0, 0, 0, gainedRtfPoints, 0.0, 
						" Video watched on " + pollingVideoRewardInfo.getSource());
			}
		}
		pollingVideoRewardInfo.setSts(1);
	}catch (Exception ex) {
		ex.printStackTrace();
		pollingVideoRewardInfo.setSts(0);
	}
		
		return pollingVideoRewardInfo;
	
	}

}
