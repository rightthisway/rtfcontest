package com.quiz.cassandra.service;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.IntroVideoInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class IntroVideoService {
	
	public static IntroVideoInfo processIntroVideRewards(IntroVideoInfo introVideoInfo , CassCustomer customer)  throws Exception {		
		
		Integer custId = customer.getId();
		Integer mwQty = PollingUtil.getZeroIfNull(customer.getMw());		
		String rwdType = PollingUtil.REWARD_FUNMESSAGE;
		Integer updateQty = mwQty + 1;
		
		introVideoInfo.setSts(1);
		introVideoInfo.setRwdType(rwdType);
		
		if(PollingUtil.SHOW_INTRO_CORRECT_ANS_OPT.equals(introVideoInfo.getAns())) {
			introVideoInfo.setIsAnsCrt(1);
			introVideoInfo.setShowMsg(PollingUtil.SHOW_INTRO_REWARD_MSG_CORRECT_ANS);
		}	else {
			introVideoInfo.setIsAnsCrt(0);
			introVideoInfo.setShowMsg(PollingUtil.SHOW_INTRO_REWARD_MSG_WRONG_ANS);
		}
		try {
		updateCassCustomerStats(custId , updateQty , rwdType);
		updateSQLCustomerStats(custId , 1 , rwdType);
		}catch(Exception ex) {
			ex.printStackTrace();
			//Do not throw or create Error Object As It is for Intro Video 
		}
		return introVideoInfo;
		
	}

	private static void updateSQLCustomerStats(Integer custId, Integer updateQty, String rwdType) throws Exception {
		PollingSQLDaoUtil.insertIntroVideoMWReward(custId, PollingUtil.REWARD_MAGICWAND);		
		
		PollingSQLDaoUtil.updateIntroVideoLife(custId , 1);
		//PollingSQLDaoUtil.insertIntroVideoSuperFanStars(custId , 1);
		PollingSQLDaoUtil.saveOrUpdatePollingIntroSuperFanStars(custId , 1);
		try {
			CustomerRewardCreditService.sqlCreditToCustomerMagicWands(custId, 1);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private static void updateCassCustomerStats(Integer custId, Integer updateQty, String rwdType) throws Exception {		
		PollingSQLDaoUtil.updateCassCustomerStatsForIntroVideo(custId);
		
	}
	
	public static void main (String a []) throws Exception{
		IntroVideoInfo introVideoInfo  = new IntroVideoInfo();
		introVideoInfo.setCuId(2);
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(2);
		processIntroVideRewards(introVideoInfo , customer);
		//processApplyMagicWand( introVideoInfo ,  customer);
		
		
	}
	
	
	

}
