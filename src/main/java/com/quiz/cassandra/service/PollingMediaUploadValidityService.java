package com.quiz.cassandra.service;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.IntroVideoInfo;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class PollingMediaUploadValidityService {
	
	public static PollingVideoInfo processMediaUploadEligibility(PollingVideoInfo pollingVideoInfo , CassCustomer customer)  throws Exception {		
		
		Integer custId = customer.getId();
		
		try {
		pollingVideoInfo.setIsVid(0);
		Integer uploadCnt = 	PollingSQLDaoUtil.validateMediaUploadEligibilityQuota(custId );
		//System.out.println("TODAYS UPLOAD COUNT FOR CUST_ID " + custId  +   " IS "  + uploadCnt);
		uploadCnt = PollingUtil.getZeroIfNull(uploadCnt)	;
		if(uploadCnt >= PollingUtil.MAX_MEDIA_UPLOAD_COUNT) {
			pollingVideoInfo.setIsVid(0);
			pollingVideoInfo.setMsg(PollingUtil.MOBILE_MSG_MAX_CUST_UPLOAD); 
			
		}
		else {
			pollingVideoInfo.setIsVid(1);			 
		}	
			pollingVideoInfo.setSts(1);
			
			//System.out.println("CAN UPLOAD VIDEO IS  " + pollingVideoInfo.getIsVid() );
		}catch(Exception ex) {
			ex.printStackTrace();
			pollingVideoInfo.setSts(0);
			pollingVideoInfo.setMsg(PollingUtil.COMMON_FAILURE); 
			
		}
		return pollingVideoInfo;
		
	}

	
	public static void main (String a []) throws Exception{
		PollingVideoInfo pollingVideoInfo  = new PollingVideoInfo();
		pollingVideoInfo.setCuId(415193);
		pollingVideoInfo.setwTy("G");
		pollingVideoInfo.setvUrl("TEST.MP4");
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(415193);
		processMediaUploadEligibility(pollingVideoInfo , customer);
		
	}
	
	
	

}
