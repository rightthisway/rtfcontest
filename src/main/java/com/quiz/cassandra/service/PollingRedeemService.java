package com.quiz.cassandra.service;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.PollingRedeemInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.data.PollingCustomerRewards;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class PollingRedeemService {
	
	public static PollingRedeemInfo processCustPollingRwdsRedeem(PollingRedeemInfo pollingRedeemInfo) throws Exception {
		
		String fromType = pollingRedeemInfo.getFmRwdTy();
		String toType = pollingRedeemInfo.getToRwdTy();
		Integer fromQty = pollingRedeemInfo.getFmQty();
		Double   toQty = pollingRedeemInfo.getToQty();
		PollingCustomerRewards pollingCustomerRewards ; 
		
		try {
		pollingCustomerRewards =  PollingSQLDaoUtil.fetchCustomerPollingRewards(pollingRedeemInfo.getCuId());
		
		if(PollingUtil.REWARD_CRYSTALL_BALL.equals(fromType)) {
			//Check if Customer has sufficient FROM QTY of crystall balls
			if(fromQty > pollingCustomerRewards.getCrystalBall()) {
				//System.out.println("CRYSTAL BALL " + PollingUtil.MOBILE_MSG_INSUFFICIENT_REDEEM_QTY);
				pollingRedeemInfo.setSts(0);
				pollingRedeemInfo.setMsg(PollingUtil.MOBILE_MSG_INSUFFICIENT_REDEEM_QTY);
				return pollingRedeemInfo;				
			}			
		}
		if(PollingUtil.REWARD_GOLDBARS.equals(fromType)) {
			//Check if Customer has Suffiecient GoldBars ...
			if(fromQty > pollingCustomerRewards.getGoldBar()) {
				//System.out.println("GOLD BAR " + PollingUtil.MOBILE_MSG_INSUFFICIENT_REDEEM_QTY);
				pollingRedeemInfo.setSts(0);
				pollingRedeemInfo.setMsg(PollingUtil.MOBILE_MSG_INSUFFICIENT_REDEEM_QTY);
				return pollingRedeemInfo;				
			}	
			
		}
		
		if(PollingUtil.REWARD_HIFIVE.equals(fromType)) {
		//Check if Customer has Sufficient HI Five....
			if(fromQty > pollingCustomerRewards.getHiFive()) {
				System.out.println("HI FIVE " + PollingUtil.MOBILE_MSG_INSUFFICIENT_REDEEM_QTY);
				pollingRedeemInfo.setSts(0);
				pollingRedeemInfo.setMsg(PollingUtil.MOBILE_MSG_INSUFFICIENT_REDEEM_QTY);
				return pollingRedeemInfo;				
			}	
		}
		
		if(PollingUtil.REWARD_MAGICWAND.equals(fromType)) {
			
			//System.out.println("MAGIC WAND " + PollingUtil.MOBILE_MSG_NOT_ALLOWED_REDEEM_TYPE);
			pollingRedeemInfo.setSts(0);
			pollingRedeemInfo.setMsg(PollingUtil.MOBILE_MSG_NOT_ALLOWED_REDEEM_TYPE);
			return pollingRedeemInfo;
		}
		
		
		if(PollingUtil.REDEEM_TO_LIFE.equals(toType)) {
			// Process Life Line details
			//System.out.println("[PollingRedeemService] [updating life count "  );
			PollingSQLDaoUtil.updatePollingRedeemLife(pollingRedeemInfo.getCuId(),pollingRedeemInfo.getToQty().intValue());
			deductCustomerSQLAccountForRedeemedFromQty(pollingRedeemInfo.getCuId() , fromType,fromQty);
			
			 updateCassCustomerStatsForRedeem(pollingRedeemInfo.getCuId(),  fromType ,  toType,  fromQty,  toQty.intValue());
			
			pollingRedeemInfo.setSts(1);
			pollingRedeemInfo.setMsg(PollingUtil.MOBILE_MSG_REDEEMED_SUCCESS);
			
		}
		else if (PollingUtil.REDEEM_TO_STARS.equals(toType)) {
			// Process Super Fan Stars 
			//System.out.println("[PollingRedeemService] [Save or Update : Super fan stars] " ) ;
			PollingSQLDaoUtil.saveOrUpdatePollingRedeemSuperFanStars(pollingRedeemInfo.getCuId(), pollingRedeemInfo.getToQty().intValue());
			deductCustomerSQLAccountForRedeemedFromQty(pollingRedeemInfo.getCuId() , fromType,fromQty);
			 updateCassCustomerStatsForRedeem(pollingRedeemInfo.getCuId(),  fromType ,  toType,  fromQty,  toQty.intValue());
			pollingRedeemInfo.setSts(1);
			pollingRedeemInfo.setMsg(PollingUtil.MOBILE_MSG_REDEEMED_SUCCESS);
			//update tracking select * RTFQuizMaster_Sanbox.dbo.quiz_super_fan_tracking
		}
		else if(PollingUtil.REDEEM_TO_REWD_DOLLARS.equals(toType)) {
			//Process Reward Dollars ....Ulaga currency ....			
			deductCustomerSQLAccountForRedeemedFromQty(pollingRedeemInfo.getCuId() , fromType,fromQty);
			updateCassCustomerStatsForRedeemRwdDollars(pollingRedeemInfo.getCuId(),  fromType ,  toType,  fromQty,  toQty);
			pollingRedeemInfo.setSts(1);
			pollingRedeemInfo.setMsg(PollingUtil.MOBILE_MSG_REDEEMED_SUCCESS);
		}
		}catch(Exception ex) {
			
			pollingRedeemInfo.setSts(1);
			
			throw ex;
		}
		
		return pollingRedeemInfo;
	}
	
	private static void deductCustomerSQLAccountForRedeemedFromQty(Integer cuId, String fromType, Integer fromQty) throws Exception{		
		try {
		PollingSQLDaoUtil.updateCustomerAccountForRedeemedFromQty(cuId , fromType , fromQty );
		}catch (Exception ex) {
			throw  ex;
		}
	}
	
	
	
	private static Integer updateCassCustomerStatsForRedeem(Integer custId, String fromType , String toType, Integer fromQty, Integer toQty) throws Exception {
		String cassColumnFromType = PollingUtil.getCassColumnForRwdType(fromType);
		String cassColumnToType = PollingUtil.getCassColumnForRwdType(toType);
		fromQty = PollingUtil.getZeroIfNull(fromQty);
		toQty = PollingUtil.getZeroIfNull(toQty);
				
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
		
		Integer currentFromStats = PollingSQLDaoUtil.fetchCassCachedCustPollingStats(customer,fromType);
		Integer currentToStats = PollingSQLDaoUtil.fetchCassCachedCustPollingStats(customer,toType);
		
		Integer updateFromQty = PollingUtil.getZeroIfNull(currentFromStats) - fromQty;
		Integer updateToQty = PollingUtil.getZeroIfNull(currentToStats) + toQty;
		
		String cassSQLForRewardRedeemUpdate  = " UPDATE customer set "
				+  cassColumnFromType + " =  " + updateFromQty + " ,  "				
				+  cassColumnToType + " = " + updateToQty 
				+ " where customer_id =  " + custId ;
		//System.out.println("CASS REDEEM CQLSH : cassSQLForRewardRedeemUpdate  " +  cassSQLForRewardRedeemUpdate );
		Integer retCnt = 0;
		try {
		PollingSQLDaoUtil.cassExecuteCQL(cassSQLForRewardRedeemUpdate);
		retCnt = 1;
		}		
		catch(Exception ex) {
			ex.printStackTrace();
			retCnt = 0;
			throw ex;
		}		
		return retCnt;
	}
	
	
	private static Integer updateCassCustomerStatsForRedeemRwdDollars(Integer custId, String fromType , String toType, Integer fromQty, Double toQty) throws Exception {
		String cassColumnFromType = PollingUtil.getCassColumnForRwdType(fromType);
		String cassColumnToType = PollingUtil.getCassColumnForRwdType(toType);
		fromQty = PollingUtil.getZeroIfNull(fromQty);
		toQty = PollingUtil.getZeroIfDblNull(toQty);
				
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
		
		Integer currentFromStats = PollingSQLDaoUtil.fetchCassCachedCustPollingStats(customer,fromType);
		Double currentToStats = customer.getcRewardDbl();
		
		Integer updateFromQty = PollingUtil.getZeroIfNull(currentFromStats) - fromQty;
		Double updateToQty = PollingUtil.getZeroIfDblNull(currentToStats) + toQty;
		
		String cassSQLForRewardRedeemUpdate  = " UPDATE customer set "
				+  cassColumnFromType + " =  " + updateFromQty + " ,  "				
				+  cassColumnToType + " = " + updateToQty 
				+ " where customer_id =  " + custId ;
		//System.out.println("CASS REDEEM CQLSH : cassSQLForRewardRedeemUpdate  " +  cassSQLForRewardRedeemUpdate );
		Integer retCnt = 0;
		try {
		PollingSQLDaoUtil.cassExecuteCQL(cassSQLForRewardRedeemUpdate);
		retCnt = 1;
		}		
		catch(Exception ex) {
			ex.printStackTrace();
			retCnt = 0;
			throw ex;
		}		
		return retCnt;
	}

	

	public static void main(String a[]) throws Exception {
		
		PollingRedeemInfo pollingRedeemInfo = new PollingRedeemInfo();
		pollingRedeemInfo.setCuId(2);
		pollingRedeemInfo.setFmQty(2);
		pollingRedeemInfo.setFmRwdTy("cb");
		pollingRedeemInfo.setToQty(Double.valueOf("5.43"));
		pollingRedeemInfo.setToRwdTy("rd");
		processCustPollingRwdsRedeem(pollingRedeemInfo);
	}

}
