package com.quiz.cassandra.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quiz.cassandra.list.PollingVideoRewardInfo;
import com.quiz.cassandra.list.PollingVideoRewardsConfig;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class PollingVideoRewardService {
	
	/*
	 * 1) check customer max limit for the day 
	 * 2) check customer time interval for the day  
	 * 3) get random rewards for the day
	 * 4) set message with rewards ... 
	 * 5) insert rewards details for the customer 
	 * 6) update sql with rewards 
	 * 7) update cassandra with rewards
	 */
	public static PollingVideoRewardInfo processVideoRewards( PollingVideoRewardInfo pollingVideoRewardInfo) throws Exception{
		try {
		pollingVideoRewardInfo.setSts(0);
		PollingVideoRewardsConfig pollingVideoRewardsConfig  = PollingSQLDaoUtil.getVideoRewardsConfig();
		if(pollingVideoRewardsConfig == null || pollingVideoRewardsConfig.getId() == null) {
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.GENERAL_ERROR);
			return pollingVideoRewardInfo;
		}		
		Integer maxCustRwdLimit = pollingVideoRewardsConfig.getMaxRwdsPerDay();
		if(maxCustRwdLimit == null || maxCustRwdLimit == 0) {
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_MAX_NOTSET_ERROR);
			return pollingVideoRewardInfo;
		}
		
		Integer videoRewardCountforTheDay = PollingSQLDaoUtil.getVideoRewardCountForTheDay(pollingVideoRewardInfo.getCuId());
		if(videoRewardCountforTheDay == null ) videoRewardCountforTheDay = 0 ; 
		if(videoRewardCountforTheDay > 0) {
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_MAX_ERROR);
			return pollingVideoRewardInfo;
		}
		
		/*Integer videoRewardCountforTheDay = PollingSQLDaoUtil.getVideoRewardCountForTheDay(pollingVideoRewardInfo.getCuId());
		if(videoRewardCountforTheDay == null ) videoRewardCountforTheDay = 0 ; 
		if(videoRewardCountforTheDay.compareTo(maxCustRwdLimit) >= 0) {
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_MAX_ERROR);
			return pollingVideoRewardInfo;
		}*/
		Map<String , Integer> rewardMap = getRewardMapConfiguredForVideo(pollingVideoRewardsConfig);
		System.out.println("--configured rewardMap--" + rewardMap);
		if(rewardMap == null || rewardMap.size() ==0) {
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_NOTSET_ERROR);
			return pollingVideoRewardInfo;
		}
		Map<String , Integer > custRewardMap = fetchCustRewardMapDets(rewardMap);
		
		System.out.println("---custRewardMap ---"  + custRewardMap);
		if(custRewardMap == null || custRewardMap.size() ==0) {
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_COMPUTE_ERROR);
			return pollingVideoRewardInfo;
		}		
		processCustomerVideoRewards(custRewardMap ,pollingVideoRewardInfo );
		pollingVideoRewardInfo.setSts(1);
		}catch(Exception ex) {
			ex.printStackTrace();
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_COMPUTE_ERROR);
		}		
		String rwdMsg = PollingUtil.VIDEO_REWARD_MGS1;			
		if(pollingVideoRewardInfo.getSts() == 1 ) {
			
			String rewardCode = pollingVideoRewardInfo.getRwdType();
			String rewardTxt  = (String)PollingUtil.getRewardStatsColumnForRwdType(rewardCode);
			rwdMsg = rwdMsg.replace("XX", rewardTxt);
			rwdMsg = rwdMsg.replace("YYY", String.valueOf(pollingVideoRewardInfo.getRwdQty()))	;	
		}
		else {
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.GENERAL_ERROR);
		}		
		return pollingVideoRewardInfo;	
	}

	private static PollingVideoRewardInfo processCustomerVideoRewards(Map<String, Integer> custRewardMap, PollingVideoRewardInfo pollingVideoRewardInfo) {
		try {
			String rewardCode = (String)custRewardMap.keySet().toArray()[0];
			Integer rwdQty = (Integer)custRewardMap.get(rewardCode);
			Integer custId = pollingVideoRewardInfo.getCuId();		
			PollingSQLDaoUtil.insertCustomerVideoRewardStats(rewardCode , rwdQty , custId , pollingVideoRewardInfo.getClientIp(),pollingVideoRewardInfo.getAwsVideoId() );
			updateSQLStatsForCust(rewardCode , rwdQty , custId);
			PollingSQLDaoUtil.updateCassCustomerVideoRewards(rewardCode , rwdQty , custId);
			pollingVideoRewardInfo.setRwdQty(rwdQty);
			pollingVideoRewardInfo.setRwdType(rewardCode);
		}catch(Exception ex) {
			ex.printStackTrace();
			pollingVideoRewardInfo.setSts(0);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_REWARD_COMPUTE_ERROR);
		}
		return pollingVideoRewardInfo;
	}



	private static Integer  updateSQLStatsForCust(String rewardCode, Integer rwdQty, Integer custId) {
		if(PollingUtil.REWARD_LIFE.equals(rewardCode)) {
			return	PollingSQLDaoUtil.updatePollingRedeemLife(custId, rwdQty);
		}
		else if(PollingUtil.REWARD_SFSTARS.equals(rewardCode)) {
			return PollingSQLDaoUtil.saveOrUpdatePollingRedeemSuperFanStars(custId, rwdQty);
		}
		else if (PollingUtil.REWARD_MAGICWAND.equals(rewardCode)){
			 PollingSQLDaoUtil.saveUpdateCustMagicWandRewardStats(custId, rwdQty);
			 return 1;
		}
		else {
			return 0;
		}
		
	}



	private static Map<String, Integer> fetchCustRewardMapDets(Map<String, Integer> rewardMap) {
		List<String> rwdKeyList = new ArrayList<String>(rewardMap.keySet());
		Collections.shuffle(rwdKeyList);		
		String rewardType= (String)rwdKeyList.get(0);
		Integer rwdMaxCount = (Integer) rewardMap.get(rewardType);
		Integer rwdCount = PollingUtil.getRandomRwdUnit(rwdMaxCount);
		if(rwdCount == null || rwdCount.compareTo(0) == 0) rwdCount = 1;
		Map<String , Integer> custRwdMap =  new HashMap<String ,Integer>();
		custRwdMap.put(rewardType, rwdCount);
		return custRwdMap;
	}



	private static Map<String, Integer> getRewardMapConfiguredForVideo(PollingVideoRewardsConfig pollingVideoRewardsConfig) {
		Map<String , Integer> rewardMap = new HashMap<String , Integer>();
		Integer lifeReward = pollingVideoRewardsConfig.getMaxLives();
		Integer sfStarReward = pollingVideoRewardsConfig.getMaxStars();
		Integer mwReward = pollingVideoRewardsConfig.getMaxMagicWand();
		if(lifeReward != null && lifeReward.compareTo(0) > 0)
			rewardMap.put(PollingUtil.REWARD_LIFE, lifeReward);
		if(sfStarReward != null && sfStarReward.compareTo(0) > 0)
			rewardMap.put(PollingUtil.REWARD_SFSTARS, sfStarReward);
		if(mwReward != null && mwReward.compareTo(0) > 0)
			rewardMap.put(PollingUtil.REWARD_MAGICWAND, mwReward);		
		return rewardMap;
	}



	public static void main(String[] args) throws Exception{
		PollingVideoRewardInfo pollingVideoRewardInfo = new PollingVideoRewardInfo();
		pollingVideoRewardInfo.setCuId(2);
		processVideoRewards(pollingVideoRewardInfo);
		System.out.println("Status " + pollingVideoRewardInfo.getSts()  + " --- " + 
				"message " + pollingVideoRewardInfo.getShowMsg()  + " --- " + 
		pollingVideoRewardInfo.getRwdType() + " - "  + pollingVideoRewardInfo.getRwdQty());

	}

}
