package com.quiz.cassandra.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.list.CommentsDTO;
import com.quiz.cassandra.list.CustRewardValueInfo;
import com.quiz.cassandra.list.FanClubEventInfo;
import com.quiz.cassandra.list.FanClubEventIntInfo;
import com.quiz.cassandra.list.FanClubPostsDTO;
import com.quiz.cassandra.list.FanClubVidPlayListDTO;
import com.quiz.cassandra.list.RewardsInfo;
import com.quiz.cassandra.list.RtfRewardConfigInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.dao.implementation.FanClubEventInterestSQLDAO;
import com.zonesws.webservices.dao.implementation.FanClubEventSQLDAO;
import com.zonesws.webservices.data.FanClubEvent;
import com.zonesws.webservices.data.FanClubEventInterest;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;

public class FanClubService {

	public static FanClubPostsDTO saveCommentsOnFanClubPosts(FanClubPostsDTO fanClubPostsDTO) {
		Integer updCnt = PollingSQLDaoUtil.saveCommentsOnFanClubPosts(fanClubPostsDTO);		
		try {
		Integer cmtCnt = 	PollingSQLDaoUtil.getFCPostCommentsCount(fanClubPostsDTO.getpId());
		if(cmtCnt == null) cmtCnt = 0;
		fanClubPostsDTO.setCmtCnt(cmtCnt);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		fanClubPostsDTO.setSts(updCnt);
		
		System.out.println(updCnt);
		return fanClubPostsDTO;
	}

	public static CommentsDTO fetchMediaComments(CommentsDTO commentsDTO) {
		try {
			List<Comments> cmtList = PollingSQLDaoUtil.fetchMediaComments(commentsDTO.getSrcId());
			commentsDTO.setcLst(cmtList);
			commentsDTO.setSts(1);
			return commentsDTO;
		} catch (Exception ex) { // need to change as exception will not be propogated
			ex.printStackTrace();
			commentsDTO.setSts(0);
		}
		return commentsDTO;

	}

	public static Comments deleteMediaComments(Comments comments) {
		try {
			Integer sts = PollingSQLDaoUtil.deleteMediaComments(comments.getCmtId());
			comments.setSts(sts);
			return comments;
		} catch (Exception ex) {
			ex.printStackTrace();
			comments.setSts(0);
		}
		return comments;

	}

	public static Comments editMediaComments(Comments comments) {
		try {
			Integer sts = PollingSQLDaoUtil.editMediaComments(comments);
			comments.setSts(sts);
			return comments;
		} catch (Exception ex) {
			ex.printStackTrace();
			comments.setSts(0);
		}
		return comments;

	}

	public static void main(String a[]) {

		//FanClubCommentsDTO c = new FanClubCommentsDTO();
		FanClubPostsDTO c = new FanClubPostsDTO();
		//fanClubPostsDTO.setFcId(26);
		//Comments c = new Comments();
		c.setFcId(5);
		c.setpId(1);
		c.setCuId(415336);
		c.setImgUrl("lloyd4.jpg");
		c.setVidUrl("videourtestupload.mp4");
		c.settUrl("thumbnaiilurl");
		
		//c.setDescription(" this is upload.description . test insert long text .. this should be first   comment 555555555555555555  ");
		// saveCustomerFanClubComments(c);
		//c.setTitle("lloyd test title");
		//c.setCatId("31");
		//uploadFanClubVideo(c);
		//fanClubPostsDTO.setpId(1);
		//fanClubPostsDTO.setFcId(26);
		fetchFanClubVideos(c, "1");
		
		//fetchFanClubCommentsOnPosts( c , "1");
		
		//saveCommentsOnFanClubPosts(c);
		//fanClubPostsDTO = fetchFanClubPosts( fanClubPostsDTO, "1");
		
		
		//FanClubService.uploadFanClubVideo(vidUpdInfo);
		
		//System.out.println(fanClubPostsDTO.getcLst().size());
		// System.out.println(commentsDTO.getcLst().size());
	}

	public static FanClubPostsDTO editCustomerFanClubComments(FanClubPostsDTO fanClubPostsDTO) {

		try {
			Integer sts = PollingSQLDaoUtil.editFanClubPostComments(fanClubPostsDTO);
			fanClubPostsDTO.setSts(sts);
			return fanClubPostsDTO;
		} catch (Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}
		return fanClubPostsDTO;
	}

	public static FanClubPostsDTO deleteCustomerFanClubComments(FanClubPostsDTO fanClubPostsDTO) {

		try {
			Integer sts = PollingSQLDaoUtil.deleteFanClubPostsComments(fanClubPostsDTO.getCmtId());
			fanClubPostsDTO.setSts(sts);
			return fanClubPostsDTO;
		} catch (Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}
		return fanClubPostsDTO;

	}

	public static FanClubPostsDTO saveCustomerFanClubPost(CassCustomer customer,FanClubPostsDTO fanClubPostsDTO) {
		
		try {
		Integer updCnt = PollingSQLDaoUtil.saveFanClubPosts(fanClubPostsDTO);		
		CustRewardValueInfo cro = CustomerRewardLimtService.processCreatePostRewards(customer,fanClubPostsDTO);
		Integer credStatus = 0;
		if (cro.getIsRewarded() == true) {
			Integer currentPoint = customer.getRtfPoints();
			if (currentPoint == null)
				currentPoint = 0;
			Integer gainedRtfPoints = cro.getRwdQty();
			if (gainedRtfPoints == null)
				gainedRtfPoints = 0;
			Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
			credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
					gainedRtfPoints);
			if (credStatus > 0) {
				String msg = PollingUtil.FANCLUB_CREATE_POST.replace("00", String.valueOf(gainedRtfPoints));
				Integer sts = 1;
				Integer pts = cassFinalrtfPoints;
				Double rDlrs = customer.getcRewardDbl();
				RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
				fanClubPostsDTO.setRwdInfo(rwdInfo);
				SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR",
						SourceType.CREATE_POST, fanClubPostsDTO.getpId(),
						0, 0, 0, gainedRtfPoints, 0.0, " Post created " );
			
			}
		}
		
		fanClubPostsDTO.setSts(1);
		}catch(Exception ex) {
			fanClubPostsDTO.setSts(0);
			ex.printStackTrace();
		}
		return fanClubPostsDTO;
	}

	public static FanClubPostsDTO editCustomerFanClubPost(FanClubPostsDTO fanClubPostsDTO) {
		Integer updCnt = PollingSQLDaoUtil.editFanClubPosts(fanClubPostsDTO);
		fanClubPostsDTO.setSts(updCnt);
		return fanClubPostsDTO;
	}

	public static FanClubPostsDTO deleteCustomerFanClubPost(FanClubPostsDTO fanClubPostsDTO) {
		try {
			Integer sts = PollingSQLDaoUtil.deleteFanClubPosts(fanClubPostsDTO.getpId());
			fanClubPostsDTO.setSts(sts);
			return fanClubPostsDTO;
		} catch (Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}
		return fanClubPostsDTO;

	}

	public static FanClubPostsDTO fetchFanClubPosts(FanClubPostsDTO fanClubPostsDTO, String pageNo) {
		try {
			
			System.out.println("******FCPOST-1 before fetch in service " );
		Map<Integer, Comments> cmtMap = PollingSQLDaoUtil.getFanClubPosts(pageNo, fanClubPostsDTO);
		System.out.println("******FCPOST-2 after  fetch post in service " );
		if (cmtMap == null || cmtMap.size() == 0) {
			fanClubPostsDTO.setSts(1);
			fanClubPostsDTO.setMsg(PollingUtil.NO_FANCLUB_POSTS_FOUND);
			return fanClubPostsDTO;
		}

		List<Integer> postIdList = cmtMap.keySet().stream().collect(Collectors.toList());
		Map<Integer, Integer> postCmtCountMap = PollingSQLDaoUtil.getCommentCountforPost(postIdList);
		System.out.println("******FCPOST-3 after  fetch comment count of post in service " );
		List<Integer> postCmtCountList = cmtMap.keySet().stream().collect(Collectors.toList());
		
	
		for (Integer id : postCmtCountList) {
			cmtMap.get(id).setCmtCnt(postCmtCountMap.get(id));
		}
		List <Integer> likedPostList = PollingSQLDaoUtil.fetchLikedPostIds(postIdList,fanClubPostsDTO.getCuId());
		System.out.println("******FCPOST-4 after fetch liked post count of post in service " );
		if(likedPostList != null && likedPostList.size() > 0 ) {
		for (Integer id : likedPostList) {
			cmtMap.get(id).setIsLiked(Boolean.TRUE);
		}
		}
		List<Comments> commentList = cmtMap.values().stream().collect(Collectors.toList());
		
		System.out.println("******FCPOST-5 after fetch liked post count of post in service " );
		fanClubPostsDTO.setcLst(commentList);
		fanClubPostsDTO.setSts(1);
		
		if(commentList.size() >= PollingUtil.MAX_PAGE_ROWS) {
			fanClubPostsDTO.setHme(Boolean.TRUE);
		}
		}catch (Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}

		return fanClubPostsDTO;

	}

	public static FanClubPostsDTO fetchFanClubCommentsOnPosts(FanClubPostsDTO fanClubPostsDTO, String pageNo) {
		try {
			Map<Integer, Comments> cmtMap = PollingSQLDaoUtil.getFanClubCommentsOnPosts(pageNo, fanClubPostsDTO);
			fanClubPostsDTO.setSts(1);
			if (cmtMap == null || cmtMap.size() == 0) {				
				return fanClubPostsDTO;
			}
			List<Comments> cmtList = cmtMap.values().stream().collect(Collectors.toList());
			fanClubPostsDTO.setcLst(cmtList);			
			if(cmtList.size() >= PollingUtil.MAX_PAGE_ROWS)  fanClubPostsDTO.setHme(Boolean.TRUE);
		} catch (Exception ex) {
			fanClubPostsDTO.setSts(0);

		}
		return fanClubPostsDTO;
	}

	public static Comments uploadFanClubVideo(Comments vidUpdInfo,CassCustomer customer ) {
		Integer createSts = 0;
		Integer vidId = PollingSQLDaoUtil.insertFanClubVideoMaster(vidUpdInfo);
		if (vidId == null) {
			vidUpdInfo.setSts(0);
			return vidUpdInfo;
		} else {

			createSts = PollingSQLDaoUtil.insertFanClubTvMedia(vidUpdInfo.getCuId(), vidUpdInfo.getVidUrl(), vidUpdInfo.gettUrl(),
					vidUpdInfo.getCatId(), vidUpdInfo.getTitle(), vidUpdInfo.getDescription(), vidId);
			
			try {
				
				CustRewardValueInfo cro = CustomerRewardLimtService.processFanClubVideoUploadRewards(customer);
				Integer credStatus = 0;
				if (cro.getIsRewarded() == true) {
					Integer currentPoint = customer.getRtfPoints();
					if (currentPoint == null)
						currentPoint = 0;
					Integer gainedRtfPoints = cro.getRwdQty();
					if (gainedRtfPoints == null)
						gainedRtfPoints = 0;
					Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
					credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
							gainedRtfPoints);
					if (credStatus > 0) {
						String msg = PollingUtil.UPLOAD_FANCLUB_VIDEO.replace("00", String.valueOf(gainedRtfPoints));
						Integer sts = 1;
						Integer pts = cassFinalrtfPoints;
						Double rDlrs = customer.getcRewardDbl();
						RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
						vidUpdInfo.setRwdInfo(rwdInfo);
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR",
								SourceType.UPLOAD_FANCLUB_VIDEO, vidId,
								0, 0, 0, gainedRtfPoints, 0.0, "FC Video upload with FC VID ID " );					
					}
				}				
				vidUpdInfo.setSts(1);
			}catch(Exception ex) {
				ex.printStackTrace();
				vidUpdInfo.setSts(0);
			}
		}		
		return vidUpdInfo;
	}

	public static FanClubPostsDTO fetchFanClubVideos(FanClubPostsDTO fanClubPostsDTO,String pageNo) {
	try {
		
		System.out.println("******FCVIDEO-1  fetch Video in service " );
		Map<Integer, Comments> vidMap = PollingSQLDaoUtil.getFanClubVideos(pageNo, fanClubPostsDTO);		
		System.out.println("******FCVIDEO-2  after sql execute fetch Video in service " );
		fanClubPostsDTO.setSts(1);
		if(vidMap == null || vidMap.size() ==0 ) {
			fanClubPostsDTO.setMsg(PollingUtil.NO_FANCLUB_VIDEOS_FOUND);
			return fanClubPostsDTO;
		}
		List<Integer> vidIdList = vidMap.keySet().stream().collect(Collectors.toList());
		System.out.println("******FCVIDEO-3  before liked  Video in service " );
		List <Integer> likedVidList = PollingSQLDaoUtil.fetchLikedFanClubVideoIds(vidIdList,fanClubPostsDTO.getCuId());	
		System.out.println("******FCVIDEO-4  after liked  Video in service " );
		if(likedVidList != null && likedVidList.size() > 0) {
		for (Integer id : likedVidList) {
			vidMap.get(id).setIsLiked(Boolean.TRUE);
		}
		}
		List<Comments> commentList = vidMap.values().stream().collect(Collectors.toList());
		fanClubPostsDTO.setcLst(commentList);
		fanClubPostsDTO.setSts(1);
		
		System.out.println("******FCVIDEO-5  befroe response of Video in service " );
		if(commentList.size() >= PollingUtil.MAX_PAGE_ROWS) {
			fanClubPostsDTO.setHme(Boolean.TRUE);
		}
		
	}catch(Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
	}
	return fanClubPostsDTO;
	}

	public static FanClubPostsDTO saveCommentsOnFanClubVideos(FanClubPostsDTO fanClubPostsDTO) {
		Integer updCnt = PollingSQLDaoUtil.saveCommentsOnFanClubVideos(fanClubPostsDTO);
		fanClubPostsDTO.setSts(updCnt);
		return fanClubPostsDTO;
		
	}

	public static FanClubPostsDTO deleteCustomerFanClubCommentsonVideo(FanClubPostsDTO fanClubPostsDTO) {
		try {
			Integer sts = PollingSQLDaoUtil.deleteFanClubVideoomments(fanClubPostsDTO.getCmtId());
			fanClubPostsDTO.setSts(sts);
			return fanClubPostsDTO;
		} catch (Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}
		return fanClubPostsDTO;
		
	}

	public static FanClubPostsDTO editCustomerFanClubCommentsOnVideo(FanClubPostsDTO fanClubPostsDTO) {
		try {
			Integer sts = PollingSQLDaoUtil.editFanClubPostComments(fanClubPostsDTO);
			fanClubPostsDTO.setSts(sts);
			return fanClubPostsDTO;
		} catch (Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}
		return fanClubPostsDTO;
		
	}

	public static FanClubPostsDTO fetchFanClubCommentsOnVideos(FanClubPostsDTO fanClubPostsDTO, String pNoStr) {
		Map<Integer, Comments> cmtMap = null;
		try {
			cmtMap = PollingSQLDaoUtil.getFanClubCommentsOnVideos(pNoStr, fanClubPostsDTO);		
			fanClubPostsDTO.setSts(1);
			if (cmtMap == null || cmtMap.size() == 0) {
				
				//fanClubPostsDTO.setMsg(PollingUtil.NO_FANCLUB_POSTS_COMMENTS_FOUND);
				return fanClubPostsDTO;
			}
			List<Comments> cmtIdList = cmtMap.values().stream().collect(Collectors.toList());
			fanClubPostsDTO.setcLst(cmtIdList);
			if(cmtIdList.size() >= PollingUtil.MAX_PAGE_ROWS) {
				fanClubPostsDTO.setHme(Boolean.TRUE);
			}
		}catch ( Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}
	 return fanClubPostsDTO;
		
	}

	public static FanClubPostsDTO saveLikesOnFanClubPosts(CassCustomer customer, FanClubPostsDTO fanClubPostsDTO) {
		Integer status = 0;
		try {
			status = PollingSQLDaoUtil.saveLikesOnFanClubPosts(fanClubPostsDTO);
			if (status == 1) {
				status = PollingSQLDaoUtil.updateFanClubPostLikeCount(fanClubPostsDTO);
			}
		//	if(status != 1 ) throw new Exception("Something wrong while updating post like stats");
			CustRewardValueInfo cro = CustomerRewardLimtService.fanClubPostLikeRewards(customer.getId());
			Integer credStatus = 0;
			if (cro.getIsRewarded() == true) {
				Integer currentPoint = customer.getRtfPoints();
				if (currentPoint == null)
					currentPoint = 0;
				Integer gainedRtfPoints = cro.getRwdQty();
				if (gainedRtfPoints == null)
					gainedRtfPoints = 0;
				Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
				credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
						gainedRtfPoints);
				if (credStatus > 0) {
					String msg = PollingUtil.LIKE_POST.replace("00", String.valueOf(gainedRtfPoints));
					Integer sts = 1;
					Integer pts = cassFinalrtfPoints;
					Double rDlrs = customer.getcRewardDbl();
					RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
					fanClubPostsDTO.setRwdInfo(rwdInfo);
					SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", SourceType.LIKE_POST,
							fanClubPostsDTO.getpId(), 0, 0, 0, gainedRtfPoints, 0.0, " Post liked ");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}
		fanClubPostsDTO.setSts(status);
		return fanClubPostsDTO;
	}
	
	public static FanClubPostsDTO updateDisLikesOnFanClubPosts(FanClubPostsDTO fanClubPostsDTO) {
		Integer status = 0 ;
		try {
			status = PollingSQLDaoUtil.updateDisLikesOnFanClubPosts(fanClubPostsDTO);
			if(status >= 1 ) {
				status = PollingSQLDaoUtil.updateFanClubPostDisLikeCount(fanClubPostsDTO);
				CustomerRewardLimtService.fanClubPostUnLikeRewards(fanClubPostsDTO.getCuId());
				
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			status = 0;
		}
		fanClubPostsDTO.setSts(status);
		return fanClubPostsDTO;
		
	}
	
	
	public static FanClubPostsDTO saveLikesOnFanClubVideos(FanClubPostsDTO fanClubPostsDTO,CassCustomer customer) {
		Integer status = 0 ;
		try {			
			Boolean hasLikedEarlier = PollingSQLDaoUtil.isFanClubVideoLikedEarlier(fanClubPostsDTO.getvId(),fanClubPostsDTO.getCuId());
			if(hasLikedEarlier) {
				System.out.println(" fanclub vid like present");
				PollingSQLDaoUtil.updateExistingLikesOnFanClubVideos(fanClubPostsDTO);
			}
			else {
				System.out.println(" fanclub vid like new entry");
				PollingSQLDaoUtil.insertLikesOnFanClubVideos(fanClubPostsDTO);
			}
					
		Integer inventoryVidId = PollingSQLDaoUtil.fetchInventoryIdForFanClubVideo(String.valueOf(fanClubPostsDTO.getvId())) ;
			
			/*
			 * Integer rewardCntForVideoId =
			 * PollingSQLDaoUtil.custRewardCountForSameVideo(fanClubPostsDTO.getCuId() ,
			 * String.valueOf(inventoryVidId)); if(rewardCntForVideoId > 0) {
			 * fanClubPostsDTO.setSts(1); System.out.println(
			 * PollingUtil.VIDEO_VIEW_REWARD_DUPLICATE);
			 * fanClubPostsDTO.setMsg(PollingUtil.VIDEO_VIEW_REWARD_DUPLICATE); return
			 * fanClubPostsDTO; }
			 */		
			
			CustRewardValueInfo cro = CustomerRewardLimtService.isVideoLikeActionRewarded(customer.getId(),
					SourceType.LIKE_VIDEO.name());
			Integer credStatus = 0;
			if (cro.getIsRewarded() == true) {
				Integer currentPoint = customer.getRtfPoints();
				if (currentPoint == null)
					currentPoint = 0;
				Integer gainedRtfPoints = cro.getRwdQty();
				if (gainedRtfPoints == null)
					gainedRtfPoints = 0;
				Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
				credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
						gainedRtfPoints);
				if (credStatus > 0) {
					String msg = PollingUtil.LIKE_VIDEO_REWARDS.replace("00", String.valueOf(gainedRtfPoints));
					Integer sts = 1;
					Integer pts = cassFinalrtfPoints;
					Double rDlrs = customer.getcRewardDbl();
					RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
					fanClubPostsDTO.setRwdInfo(rwdInfo);				
					
					SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR",
							SourceType.LIKE_VIDEO, inventoryVidId,
							0, 0, 0, gainedRtfPoints, 0.0, " Video Liked on FANCLUB " );
				
				}
			}
			
			
		PollingSQLDaoUtil.updateFanClubVideoLikeCount(fanClubPostsDTO);            
		fanClubPostsDTO.setSts(1);
		}catch(Exception ex) {
			ex.printStackTrace();
			fanClubPostsDTO.setSts(0);
		}		
		return fanClubPostsDTO;		
	}

	public static FanClubPostsDTO updateDisLikesOnFanClubVideos(FanClubPostsDTO fanClubPostsDTO) {
		Integer status = 0 ;
		try {
			
			PollingSQLDaoUtil.updateDisLikesOnFanClubVideos(fanClubPostsDTO);			
			PollingSQLDaoUtil.updateFanClubVideoDisLikeCount(fanClubPostsDTO);
			CustomerRewardLimtService.videoUnikeRewards(fanClubPostsDTO.getCuId());
			status = 1;
		}catch(Exception ex) {
			ex.printStackTrace();
			status = 0;
		}
		fanClubPostsDTO.setSts(status);
		return fanClubPostsDTO;
		
	}
	public static Comments editFanClubVideos(Comments vidInfo) {
		Integer updSts = 0;
		try {
			 updSts = PollingSQLDaoUtil.updateFanClubVideoMaster(vidInfo);			
		}catch(Exception ex) {
			ex.printStackTrace();
			updSts = 0;
		}
		vidInfo.setSts(updSts);
		return vidInfo;		
	}

	public static FanClubVidPlayListDTO fetchFanClubVideoPlayList(FanClubVidPlayListDTO fanClubVidPlayListDTO, String pageNo) {
		
		try {
			fanClubVidPlayListDTO = PollingSQLDaoUtil.getFanClubVideoPlayList(pageNo, fanClubVidPlayListDTO);		
			fanClubVidPlayListDTO.setSts(1);
		}catch(Exception ex) {
			ex.printStackTrace();
			fanClubVidPlayListDTO.setSts(0);
		}
		return fanClubVidPlayListDTO;
		
	}

	public static FanClubEvent createFanClubEvent(CassCustomer customer,FanClubEvent fcEvent , FanClubEventInfo fanClubEventInfo) {
		fcEvent = FanClubEventSQLDAO.saveFanClubEvent(fcEvent);
		try {
			CustRewardValueInfo cro  = 	CustomerRewardLimtService.processCreateEventRewards( customer,
						 fanClubEventInfo);
			Integer evntId = 0;
			if(fcEvent != null)   evntId = fcEvent.getId();
			Integer credStatus = 0;
			if (cro.getIsRewarded() == true) {
				Integer currentPoint = customer.getRtfPoints();
				if (currentPoint == null)
					currentPoint = 0;
				Integer gainedRtfPoints = cro.getRwdQty();
				if (gainedRtfPoints == null)
					gainedRtfPoints = 0;
				Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
				credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
						gainedRtfPoints);
				if (credStatus > 0) {
					String msg = PollingUtil.FANCLUB_CREATE_EVENT.replace("00", String.valueOf(gainedRtfPoints));
					Integer sts = 1;
					Integer pts = cassFinalrtfPoints;
					Double rDlrs = customer.getcRewardDbl();
					RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
					fanClubEventInfo.setRwdInfo(rwdInfo);
					SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR",
							SourceType.CREATE_EVENT, evntId,
							0, 0, 0, gainedRtfPoints, 0.0, " Event created " );
				
				}
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return fcEvent;
	}
	
	public static FanClubEventInterest saveFCInterestShown(FanClubEventInterest fceInterest,CassCustomer customer,FanClubEventIntInfo fceIntInfo ) {
		FanClubEventInterestSQLDAO.saveFanClubEventInterest(fceInterest);
		
		try {
			CustRewardValueInfo cro  = 	CustomerRewardLimtService.fanClubEventInterestRewards(customer.getId());
			Integer evntId = 0;			
			Integer credStatus = 0;
			if (cro.getIsRewarded() == true) {
				Integer currentPoint = customer.getRtfPoints();
				if (currentPoint == null)
					currentPoint = 0;
				Integer gainedRtfPoints = cro.getRwdQty();
				if (gainedRtfPoints == null)
					gainedRtfPoints = 0;
				Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
				credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
						gainedRtfPoints);
				if (credStatus > 0) {
					String msg = PollingUtil.SHOW_INTEREST_TO_EVENT.replace("00", String.valueOf(gainedRtfPoints));
					Integer sts = 1;
					Integer pts = cassFinalrtfPoints;
					Double rDlrs = customer.getcRewardDbl();
					RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
					fceIntInfo.setRwdInfo(rwdInfo);
					SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR",
							SourceType.SHOW_INTEREST_TO_EVENT, evntId,
							0, 0, 0, gainedRtfPoints, 0.0, " Event created " );				
				}
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}		
		return fceInterest;
	}
	
	
	
}
