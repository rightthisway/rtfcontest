package com.quiz.cassandra.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;

import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingContest;
import com.quiz.cassandra.list.PollingQuestionInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.data.PollingCategory;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class PollingQuestionService {
	

	public static PollingQuestionInfo processQuestionForCustomer(Integer custId) throws Exception {
		PollingQuestionInfo pollingQuestionInfo = new PollingQuestionInfo();

		try {

			PollingContest pollingContest = PollingSQLDaoUtil.getActivePollingContest();
			
			if(pollingContest == null || pollingContest.getId() == null) {
				CassError error = new CassError();
				error.setDesc(PollingUtil.MOBILE_MSG_NO_QUESTIONS_CONFIGURED);
				pollingQuestionInfo.setErr(error);
				return pollingQuestionInfo;				
			}
			
			
			Integer contestId = pollingContest.getId();
			//System.out.println("[PollingQuestion][contest id ] " + contestId);
			pollingQuestionInfo.setpCtId(contestId);
			pollingQuestionInfo.setCustId(custId);

			Integer qCount = PollingSQLDaoUtil.getPollQuestionAnsweredCountForCustomer(pollingQuestionInfo);
			
			//System.out.println("[PollingQuestion][contest id ][MAX que ]" + "" + pollingContest.getMaxQuePerCust()	+ "[Customer Count] " + qCount);
			pollingQuestionInfo.setMaxQuestions(pollingContest.getMaxQuePerCust());
			
			if (qCount >= pollingContest.getMaxQuePerCust()) {
				System.out.println("[PollingQuestion][contest id ][MAX que reached][ " + contestId + "][" + custId + " ]");
				pollingQuestionInfo = setError(pollingQuestionInfo, PollingUtil.ERR_MAX_QUES);
				CassError error = new CassError();
				error.setDesc(PollingUtil.ERR_MAX_QUES);
				pollingQuestionInfo.setErr(error);
				return pollingQuestionInfo;
			}

			pollingQuestionInfo = fetchRandomQuestionForCustomer(contestId, pollingQuestionInfo);

			//System.out.println("[PollingQuestion][ Question is] " + pollingQuestionInfo.getqTxt());
			if (pollingQuestionInfo == null || pollingQuestionInfo.getqId() == null
					|| pollingQuestionInfo.getAnswer() == null) {
				pollingQuestionInfo = setError(pollingQuestionInfo, PollingUtil.MOBILE_MSG_NO_QUESTIONS);
				//System.out.println("ERROR : [PollingQuestion] [insufficient questions for Customer ");
				return pollingQuestionInfo;
			}

			PollingSQLDaoUtil.processAnswerText(pollingQuestionInfo);	
			
			
			
			PollingSQLDaoUtil.insertCustomerQuestionInfo(pollingQuestionInfo);
			updateRandomMessageSerNumsForQuestions(pollingQuestionInfo);
			try {
				fetchSponsorIcon(pollingQuestionInfo);
			}
			catch(Exception ex) {
				ex.printStackTrace();
				
			}
			//System.out.println("question id is " + pollingQuestionInfo.getqId());
			pollingQuestionInfo.setSts(1);
			pollingQuestionInfo.setMsg(PollingUtil.MOBILE_MSG_SUCCESS);

		} catch (Exception ex) {
			ex.printStackTrace();
			setError(pollingQuestionInfo, PollingUtil.MOBILE_MSG_ERR_PROCESSING_QUESTIONS);
		}

		return pollingQuestionInfo;
	}

	private static String updateRandomMessageSerNumsForQuestions(PollingQuestionInfo pollingQuestionInfo) {
	Integer isFirstQue = 	PollingSQLDaoUtil.isFirstQuestion(pollingQuestionInfo);
	if(isFirstQue != null && isFirstQue ==1) { 
		
		List<Integer> randomQuesNumList=PollingUtil.getRandomQueNunList(pollingQuestionInfo.getMaxQuestions());
		if(randomQuesNumList != null && randomQuesNumList.size() > 0 ) {
			List<String> randomQuesNumStrList = randomQuesNumList.stream().map(Object::toString)
                    .collect(Collectors.toList());
			String msgQueNumStr = StringUtils.join(randomQuesNumStrList, ',');
			//System.out.println("  UPDATING RANDOM MESSAGES FOR QID AS  FIRST QUESTION ");
			PollingSQLDaoUtil.updateRandomMessageRewardsQueNums(pollingQuestionInfo , msgQueNumStr );			
			return msgQueNumStr;
			
		}
	}else{
		//System.out.println(" NOT UPDATING RANDOM MESSAGES FOR QID AS NOT FIRST QUESTION ");
	}
	return null;
		
	}

	private static PollingQuestionInfo fetchRandomQuestionForCustomer(Integer contestId,
			PollingQuestionInfo pollingQuestionInfo) throws Exception {

		List<PollingCategory> catMapList = PollingSQLDaoUtil.getCategoryForPollingId(contestId);
		//System.out.println("No of categories == " + catMapList.size());
		Collections.shuffle(catMapList);		
		for(PollingCategory pollcategroy: catMapList)
		{
			Integer catId = pollcategroy.getCategoryId();			
			pollingQuestionInfo =  PollingSQLDaoUtil.getRTFQuestionForCustomerFromSponsor(pollingQuestionInfo,
					catId);	
			if(pollingQuestionInfo != null && pollingQuestionInfo.getqId() != null) {
				pollingQuestionInfo.setpCatId(catId);
				pollingQuestionInfo.setPollType(pollcategroy.getPollingType());
				//System.out.println(" QUESTION FOUND FOR CATEGORY ID --- " + catId);
				break;
				
			} else {
				System.out.println("NO QUESTION FOUND FOR CATEGORY ID TRY FOR NEXT CATEGORY--- " + catId);
			}
			
		}
		
		if(pollingQuestionInfo == null || pollingQuestionInfo.getqId() == null) 
		{
			setError(pollingQuestionInfo , PollingUtil.MOBILE_MSG_NO_QUESTIONS);
		}
	

		return pollingQuestionInfo;

	}

	private static PollingQuestionInfo fetchRandomQuestionFromRTFQueBank(PollingQuestionInfo pollingQuestionInfo)
			throws Exception {
		PollingSQLDaoUtil.getRTFQuestionForCustomer(pollingQuestionInfo);
		return pollingQuestionInfo;
	}

	private static PollingQuestionInfo fetchRandomQuestionFromSponsorQueBank(PollingQuestionInfo pollingQuestionInfo,
			PollingCategory pollingCategory) throws Exception {		
		Integer pollCategoryId = pollingCategory.getCategoryId();
		pollingQuestionInfo = PollingSQLDaoUtil.getRTFQuestionForCustomerFromSponsor(pollingQuestionInfo,
				pollCategoryId);

		/*
		 * 
		 * List<PollingCategory> sponsorPollList = new ArrayList<PollingCategory>();
		 * 
		 * System.out.println("FETCHING FOR SPONSOR / FEEDBACK  " ) ; for
		 * (PollingCategory pollingCategory : catMapList) {
		 * 
		 * if(PollingUtil.POLLINGTYPE_SPONSOR.equals(pollingCategory.getPollingType())
		 * || PollingUtil.POLLINGTYPE_FEEDBACK.equals(pollingCategory.getPollingType())
		 * ) { System.out.println("Adding to Sponsor Polling List " +
		 * pollingCategory.getPollingType()); sponsorPollList.add(pollingCategory); } }
		 * for (PollingCategory pollingCategory : sponsorPollList) {
		 * 
		 * Integer pollCategoryId = pollingCategory.getCategoryId();
		 * System.out.println("FETCHING FOR SPONSOR / FEEDBACK TYPES FOR CATEGORY " +
		 * pollCategoryId ) ; pollingQuestionInfo =
		 * PollingSQLDaoUtil.getRTFQuestionForCustomerFromSponsor( pollingQuestionInfo ,
		 * pollCategoryId); if(pollingQuestionInfo != null &&
		 * pollingQuestionInfo.getqId() != null ) { System.out.println("Question Ser
		 * 
		 * vice found Question for Sponsor category -- " + pollCategoryId ); break; }
		 * else { setError(pollingQuestionInfo , PollingUtil.MOBILE_MSG_NO_QUESTIONS);
		 * //pollingQuestionInfo.setSts(0);
		 * //pollingQuestionInfo.setMsg(PollingUtil.MOBILE_MSG_NO_QUESTIONS); } }
		 */
		return pollingQuestionInfo;
	}
	
	private static void  fetchSponsorIcon(PollingQuestionInfo pollingQuestionInfo) {
		Integer catId = pollingQuestionInfo.getpCatId();		
		String iconUrl = PollingSQLDaoUtil.getPollingSponsorLogo(catId);		
		pollingQuestionInfo.setSpimg(iconUrl);
		
	}

	private static PollingQuestionInfo setError(PollingQuestionInfo pollingQuestionInfo, String errMsg) {
		
		CassError error = new CassError();
		error.setDesc(errMsg);
		pollingQuestionInfo.setErr(error);		
		pollingQuestionInfo.setSts(0);
		pollingQuestionInfo.setMsg(errMsg);
		return pollingQuestionInfo;
	}
	
	public static void main(String a[]) throws Exception
	{
		processQuestionForCustomer(2); 
	}
	
	

}
