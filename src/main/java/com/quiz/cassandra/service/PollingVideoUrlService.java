package com.quiz.cassandra.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.PollingVideoPlayedInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.data.PollingVideoInventory;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class PollingVideoUrlService {

	public static String processVideoUrlforCustomer(CassCustomer customer, PollingVideoInfo pollingVideoInfo)
			throws Exception {
		String vUrl = "0";
		try {
			Integer cuId = customer.getId();
			PollingVideoInventory pollingVideoInventory = PollingSQLDaoUtil.getVideoUrlForCustomer(cuId);
			if (pollingVideoInventory == null)
				return vUrl;
			// PollingSQLDaoUtil.updatePollingVideoPlayedStats(cuId ,
			// pollingVideoInventory);
			vUrl = pollingVideoInventory.getVideoUrl();
			//System.out.println(" VIDEO URL FOR CUST ID [" + cuId + "--" + vUrl);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return vUrl;

	}

	public static PollingVideoInfo processVideoUrlListforCustomer(CassCustomer customer,
			PollingVideoInfo pollingVideoInfo) throws Exception {

		try {
			Integer cuId = customer.getId();
			//TODO change to reduce fetch size when inventory size is large .. 
			// To use not in played BY CUSTOMER videos clause 
			Map<Integer, String> vUrlLMap = PollingSQLDaoUtil.getVideoUrlListFromInventory(pollingVideoInfo.getCatId(),cuId);
			
			System.out.println("--vUrlLMap--" + vUrlLMap);
			List<Integer> vidPlayedList = PollingSQLDaoUtil.getPollingVideoPlayedListByCustomer(cuId);			
			if(vidPlayedList.size() >= vUrlLMap.size() ) {
				// All videos are seen by Customer ..Truncate PlayList.
				PollingSQLDaoUtil.deletePollingVideoPlayedListByCustomer(cuId);
				vidPlayedList = new ArrayList<Integer>();
			}
			List vUrlList = getVidUrlPlayList(vUrlLMap, vidPlayedList);
			System.out.println("--vUrlList--" + vUrlList);
			if (vUrlList == null || vUrlList.size() == 0) {
				pollingVideoInfo.setSts(0);
				pollingVideoInfo.setMsg(PollingUtil.MOBILE_MSG_NO_VIDEO);
				CassError error = new CassError();
				error.setDesc(PollingUtil.MOBILE_MSG_NO_VIDEO);
				pollingVideoInfo.setErr(error);
				return pollingVideoInfo;
				
			}
			List<String> playList = (List<String>) vUrlList.get(1);
			pollingVideoInfo.setvUrlLst((List<String>) vUrlList.get(1));
			List<Integer> vidIdList = (List<Integer>) vUrlList.get(0);
			// update Video played List for Customer ....
			System.out.println(vUrlList.get(1));
			if("n".equals(pollingVideoInfo.getAppver())) {  // Patch to support old & new Apps.
				pollingVideoInfo = fetchVideoViewCountnLikeStats(pollingVideoInfo , vidIdList , vUrlLMap);
			
			}/*
			 * try { new Thread(new Runnable() { public void run() {
			 * PollingSQLDaoUtil.insertPollingVideoPlayedStats(cuId, vidIdList);
			 * PollingSQLDaoUtil.insertPollingVideoPlayedHistory(cuId, vidIdList); }
			 * }).start(); }catch(Exception e){ e.printStackTrace(); }
			 */
			//System.out.println(pollingVideoInfo);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return pollingVideoInfo;

	}
	
	public static PollingVideoInfo processVideoCategoryListforCustomer(
			PollingVideoInfo pollingVideoInfo) {
		List<String> vidCatList = PollingSQLDaoUtil.getPollingVideoCategoryList();
		pollingVideoInfo.setvCatList(vidCatList);
		return pollingVideoInfo;
	}

	private static List getVidUrlPlayList(Map<Integer, String> vUrlLMap, List<Integer> vPlayedIdList) {

		List vUrlList = new ArrayList();
		if (vUrlLMap == null || vUrlLMap.size() == 0)
			return null;
		List<Integer> invIdList = vUrlLMap.keySet().stream().collect(Collectors.toList());
		System.out.println("VIDEO INVENTORY LIST "  +  invIdList);		
		invIdList.removeAll(vPlayedIdList);		
		System.out.println("VIDEO INVENTORY LIST AFTER REMOVING PLAY LIST  "  +  invIdList);
		List<Integer> randomIdList = new ArrayList<Integer>();			
		if (invIdList.size() >= PollingUtil.VID_PLAY_FETCH_SIZE) {
			//System.out.println("MORE THAN 10 VIDEOS : invIdList.size() " + invIdList.size());
			randomIdList = invIdList.subList(0, PollingUtil.VID_PLAY_FETCH_SIZE);
		} else {
			//System.out.println("LESS THAN 10 VIDEOS invIdList.size() " + invIdList.size());
			randomIdList.addAll(invIdList);			
			//System.out.println("  FINAL PLAYED LIST SIZE " + randomIdList);
		}
		System.out.println(" RETURN ID LIST   FINAL PLAYED LIST SIZE " + randomIdList);
		List<String> urlList = fetchMapUrlValuesForKeyList(randomIdList, vUrlLMap);
		vUrlList.add(randomIdList);
		vUrlList.add(urlList);

		return vUrlList;
	}

	public static List<String> fetchMapUrlValuesForKeyList(List<Integer> randomIdList, Map<Integer, String> vUrlLMap) {

		if (randomIdList == null || randomIdList.size() == 0)
			return null;
		if (vUrlLMap == null || vUrlLMap.size() == 0)
			return null;
		List<String> urlList = new ArrayList<String>();
		for (Integer id : randomIdList) {
			if (vUrlLMap.containsKey(id))
				urlList.add((String) vUrlLMap.get(id));
		}
		return urlList;

	}
	
	
	private static PollingVideoInfo fetchVideoViewCountnLikeStats(PollingVideoInfo pollingVideoInfo , List<Integer> vidIdList,Map<Integer, String> vUrlLMap) {
		// id , url , viewcount, true  , likeCount,sponsorlogo
		Map<Integer, Integer> videoViewedCountMap = null;
		List<Integer> hasAlreadyLikedList = null;
		Map<Integer , Integer> videoLikedCountMap = null;
		Map<Integer,String> videoSponsorLogoMap = null;
		if(vidIdList  == null || vidIdList.size() <= 0 ) return pollingVideoInfo;
		 videoViewedCountMap = PollingSQLDaoUtil.getVideoViewCount(vidIdList) ;
		 System.out.println("**************videoViewedCountMap*********" +videoViewedCountMap);
		 hasAlreadyLikedList = PollingSQLDaoUtil.getVideosLikedByCust(vidIdList , pollingVideoInfo.getCuId());
		 videoLikedCountMap = PollingSQLDaoUtil.getVideoLikedCount(vidIdList , pollingVideoInfo.getCuId() ) ;
		 System.out.println("**************videoLikedCountMap*********" +videoLikedCountMap);
		 videoSponsorLogoMap = PollingSQLDaoUtil.getVideoSponsorLogos(vidIdList) ;
		 List <String> finalUrlList =new ArrayList<String>(); 
		 for (Integer id: vidIdList)  {
			 String tmp = "";			 
			 tmp = id + ","   ;
			 
			 if(vUrlLMap.containsKey(id)) {
				 tmp = tmp +  (String)vUrlLMap.get(id) + ",";
			 }
			 
			 if(videoViewedCountMap.containsKey(id)) {
				 tmp = tmp +  videoViewedCountMap.get(id) + ",";
			 }
			 else {
				 tmp = tmp + 0  + ",";
			 }
			 
			 if(hasAlreadyLikedList.contains(id)) {
				 tmp = tmp + 1 + ",";
			 }
			 else {
				 tmp = tmp + 0   + ",";
			 }
			 
			 if(videoLikedCountMap.containsKey(id)) {
				 tmp = tmp +  videoLikedCountMap.get(id) + ",";
			 }
			 else {
				 tmp = tmp + 0 + ",";
			 }
			 
			 if(videoSponsorLogoMap.containsKey(id)) {
				 tmp = tmp +  videoSponsorLogoMap.get(id);
			 }
			 else {
				 tmp = tmp + "";
			 }
				 finalUrlList.add(tmp);			 
		 }		
		 pollingVideoInfo.setvUrlLst(finalUrlList); 
		 return pollingVideoInfo;
	}	
	

	public static PollingVideoPlayedInfo processVideoPlayedForCustomer(CassCustomer customer,
			PollingVideoPlayedInfo pollingVideoPlayedInfo) throws Exception {

		try {
			Integer cuId = customer.getId();

			PollingSQLDaoUtil.insertPollingVideoPlayed(cuId, pollingVideoPlayedInfo.getVid());
			PollingSQLDaoUtil.insertPollingVideoPlayedHist(cuId, pollingVideoPlayedInfo.getVid());
			PollingSQLDaoUtil.updateVideoViewCount(cuId, pollingVideoPlayedInfo.getVid());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return pollingVideoPlayedInfo;

	}

	public static void main(String a[]) throws Exception {
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(2);
		PollingVideoPlayedInfo pollingVideoInfo = new PollingVideoPlayedInfo();
		pollingVideoInfo.setVid(32);
		pollingVideoInfo.setCuId(2);
		processVideoPlayedForCustomer(customer,pollingVideoInfo);
		//pollingVideoInfo.setCatId("DEF");
		//pollingVideoInfo.setAppver("n");
		// System.out.print(processVideoUrlforCustomer(customer , pollingVideoInfo ));
		//System.out.print(processVideoUrlListforCustomer(customer, pollingVideoInfo).getvUrlLst());
		//CustomerMediaServices.fetchVideoPlayList( customer,  pollingVideoInfo);
		
		//System.out.println(pollingVideoInfo.getvCatListRel1());
	}

}
