package com.quiz.cassandra.service;

import com.quiz.cassandra.list.CustomerDailyRewardLimits;
import com.quiz.cassandra.list.RtfRewardConfigInfo;
import com.zonesws.webservices.utils.CassDaoUtil;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;

public class CustomerRewardCreditService {	
	
	public static RtfRewardConfigInfo getRewardConfigurationForRTFActionType(String actionType) {
		RtfRewardConfigInfo RtfRewardConfigInfo =  SQLDaoUtil.getAllRtfRewardConfigurations(actionType);
		return RtfRewardConfigInfo;
	}
	
	
	public static int sqlCreditToCustomerRTFPoints(Integer cuId , Integer rtfPoints) {		
		int updCnt = SQLDaoUtil.rewardCustomerWithRTFPoints(cuId, rtfPoints);
		return updCnt;		
	}
	
	public static int cassCreditCustomerRTFPoints(Integer cuId, Integer rtfPoints) {
		int updCnt = CassDaoUtil.updateCustomerRTFPoints(cuId, rtfPoints);
		return updCnt;		
	}
	
	
	public static int creditCustomerRTFPoints(Integer cuId, Integer cassFinalrtfPoints, Integer gainedRtfPoints) {
		int updCnt = sqlCreditToCustomerRTFPoints(cuId , gainedRtfPoints);
		if(updCnt > 0 ) 
			updCnt = cassCreditCustomerRTFPoints(cuId,cassFinalrtfPoints);
		return updCnt;  // Need to add RollBack to orig points in SQL if Cass Update Fails ..
	}


	public static int sqlCreditToCustomerLives(Integer cuId, Integer sqlPts) {
		int updCnt = SQLDaoUtil.rewardCustomerWithLives(cuId, sqlPts);
		return updCnt;	
	}


	public static Integer cassCreditToCustomerLives(Integer cuId, Integer cassFinalStat) {
		int updCnt = CassDaoUtil.updateCustomerLives(cuId, cassFinalStat);
		return updCnt;
	}
	
	public static int creditCustomerRTFMagicWands(Integer cuId, Integer cassFinalMw, Integer gainedMagicwands) {
		int updCnt = sqlCreditToCustomerMagicWands(cuId , gainedMagicwands);
		if(updCnt > 0 ) 
			updCnt = cassCreditToCustomerMagicWands(cuId,cassFinalMw);
		return updCnt;  // Need to add RollBack to orig points in SQL if Cass Update Fails ..
	}

	public static Integer sqlCreditToCustomerMagicWands(Integer cuId, Integer sqlPts) {		
		int updCnt = SQLDaoUtil.rewardCustomerWithMagicWands(cuId, sqlPts);
		return updCnt;
	}
	
	public static Integer cassCreditToCustomerMagicWands(Integer cuId, Integer cassFinalStat) {
		int updCnt = CassDaoUtil.updateCustomerMagicWand(cuId, cassFinalStat);
		return updCnt;
	}
	public static int creditCustomerRTFSuperFanStar(Integer cuId, Integer cassFinalsfStar, Integer gainedsfStar) {
		int updCnt = sqlCreditToCustomerSuperFanStars(cuId , gainedsfStar);
		if(updCnt > 0 ) 
			updCnt = cassCreditToCustomerSuperFanStars(cuId,cassFinalsfStar);
		return updCnt;  // Need to add RollBack to orig points in SQL if Cass Update Fails ..
	}
	public static Integer sqlCreditToCustomerSuperFanStars(Integer cuId, Integer sqlPts) {		
		int updCnt = SQLDaoUtil.rewardCustomerWithSuperFanStars(cuId, sqlPts);
		return updCnt;
	}
	
	public static Integer cassCreditToCustomerSuperFanStars(Integer cuId, Integer cassFinalStat) {
		int updCnt = CassDaoUtil.updateCustomerSFStars(cuId, cassFinalStat);
		return updCnt;
	}
	
	public static Integer applyCustomerMagicWandStat(Integer custId, Integer cnt) {
		int updCnt = PollingSQLDaoUtil.applyCustomerMagicWandStat(custId, cnt);
		return updCnt;
	}
	
	public static CustomerDailyRewardLimits fetchCustDailyRewardLimitConfig(Integer cuId) {
		CustomerDailyRewardLimits custDailyRewardsDetails = PollingSQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		return custDailyRewardsDetails;
	}
	
	
	public static void main(String[] args) {
		CustomerDailyRewardLimits cdrl = fetchCustDailyRewardLimitConfig(2);
		System.out.println(cdrl);
	}
	
}
