package com.quiz.cassandra.service;

import java.sql.Connection;
import java.util.Date;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CustRewardValueInfo;
import com.quiz.cassandra.list.CustomerDailyRewardLimits;
import com.quiz.cassandra.list.MediaReward;
import com.quiz.cassandra.list.RewardsInfo;
import com.quiz.cassandra.list.RtfRewardConfigInfo;
import com.quiz.cassandra.list.VideoLikeInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;

public class PollingVideoLikeService {

	public static VideoLikeInfo processVideoLike(VideoLikeInfo videoLikeInfo, CassCustomer customer) throws Exception {
		Integer videoId = videoLikeInfo.getVidId();
		updateVideoLikeStats(videoId, customer, videoLikeInfo);
		return videoLikeInfo;
	}

	public static VideoLikeInfo processVideoDisLike(VideoLikeInfo videoLikeInfo, CassCustomer customer)
			throws Exception {
		Integer videoId = videoLikeInfo.getVidId();
		PollingSQLDaoUtil.updateVideoDisLikeStats(videoId, customer.getId());
		PollingSQLDaoUtil.updateVidInventoryDisLikeCount(videoId);
		CustomerRewardLimtService.videoUnikeRewards(videoLikeInfo.getCuId());
		return videoLikeInfo;
	}

	private static void updateVideoLikeStats(Integer videoId, CassCustomer customer, VideoLikeInfo videoLikeInfo)
			throws Exception {
		Integer updStats = 0;
		try {

			PollingSQLDaoUtil.updateVidInventoryLikeCount(videoId);
			Boolean hasLikedEarlier = PollingSQLDaoUtil.isVideoLikedEarlier(videoId, customer.getId());
			if (hasLikedEarlier) {
				System.out.println("Existing like sts re updated for vid id  " + videoId + " cuid " + customer.getId());
				PollingSQLDaoUtil.updateExistingLikeStatsToLike(videoId, customer.getId());
				// PollingSQLDaoUtil.updateVideoLikeStats(videoId,customer.getId());
			} else {
				PollingSQLDaoUtil.insertVideoLikeStats(videoId, customer.getId());
				System.out.println("new like sts insereted for vid id  " + videoId + " cuid " + customer.getId());
			}

			CustRewardValueInfo cro = CustomerRewardLimtService.isVideoLikeActionRewarded(customer.getId(),
					SourceType.LIKE_VIDEO.name());
			Integer credStatus = 0;
			if (cro.getIsRewarded() == true) {
				Integer currentPoint = customer.getRtfPoints();
				if (currentPoint == null)
					currentPoint = 0;
				Integer gainedRtfPoints = cro.getRwdQty();
				if (gainedRtfPoints == null)
					gainedRtfPoints = 0;
				Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
				credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
						gainedRtfPoints);
				if (credStatus > 0) {
					String msg = PollingUtil.LIKE_VIDEO_REWARDS.replace("00", String.valueOf(gainedRtfPoints));
					Integer sts = 1;
					Integer pts = cassFinalrtfPoints;
					Double rDlrs = customer.getcRewardDbl();
					RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
					videoLikeInfo.setRwdInfo(rwdInfo);				
					
					SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR",
							SourceType.LIKE_VIDEO, videoLikeInfo.getVidId(),
							0, 0, 0, gainedRtfPoints, 0.0, " Video Liked on RTF TV " );
				
				}
			}
			// TODOD Common connection
			// RtfRewardConfigInfo rtfRwdConfigInfo =
			// SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.LIKE_VIDEO.name());

			// if(!hasLikedEarlier) {
			// MediaReward mediarwd =
			// CustomerMediaServices.allotRandomMediaRewardsToCustomer(customer,SourceType.VIDEO_LIKES,videoId);
			// videoLikeInfo.setSts(mediarwd.getSts());
			// }else {
			// videoLikeInfo.setSts(1);
			// }

			videoLikeInfo.setSts(1);

		} catch (Exception ex) {
			ex.printStackTrace();
			videoLikeInfo.setSts(0);
		}

	}

	public static void main(String a[]) throws Exception {

		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(351111);
		Integer cuId = customer.getId();
		VideoLikeInfo videoLikeInfo = new VideoLikeInfo();
		videoLikeInfo.setCuId(cuId);
		videoLikeInfo.setVidId(4);

		// processVideoLike( videoLikeInfo , customer);
		Connection conn = DatabaseConnections.getRtfConnection();
		Boolean isRewarded = false;
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.LIKE_VIDEO.name());
		System.out.println(rtfRwdConfigInfo);
		Integer rewardPointsToBeGiven = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer batchSize = rtfRwdConfigInfo.getBatchSizePerDay();
		Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxActionsPerDay();
		Integer newLikeCnt = 0;

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);

		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTot_vd_lk(1);
			cDrl.setRwd_vd_lk(0);
			SQLDaoUtil.insertCustDailyRwdsLimitVidLikeStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid like   " + cuId);

		} else {
			// Existing record for vide likes ..
			Date rwdDate = custDlyRwds.getVidLkDate();
			Date currDBDate = custDlyRwds.getCurrDate();
			Integer currentLkCnt = custDlyRwds.getTot_vd_lk();
			Integer rewardedLkCount = custDlyRwds.getRwd_vd_lk();
			System.out.println("rwdDate " + rwdDate);
			System.out.println("currDBDate " + currDBDate);
			if (rwdDate == null) {
				throw new Exception("Reward date is null for video likes."
						+ "Something wrong while inserting or updaing" + " this table rtf_cust_daily_reward_limits ");
			}

			if (rwdDate.compareTo(currDBDate) < 0) {

				// Historic Record ...migrate prev balance likes/unlikes to new record
				System.out.println("rwdDate is less than current date " + rwdDate);
				if (currentLkCnt > rewardedLkCount && currentLkCnt < maxActionPermitedPerDay) {
					// case where upto 4 likes added after getting 45 rewards which needs to be
					// carried forward to next day
					// rewardedLkCount = 0 ;
					newLikeCnt = (currentLkCnt - rewardedLkCount) + 1;
					// check if newLikeCnt is of batch size to credit rewards ..
					// Credit Rewards ...
					if (newLikeCnt % batchSize == 0 && newLikeCnt <= batchSize) {
						// Credit fresh Rewards with new date ...
						rewardedLkCount = batchSize;
						isRewarded = true;
						System.out.println(" Old Date Like : User will be  Rewarded for video like " + cuId);
					} else {
						// just update new like count with todays date record
						rewardedLkCount = 0;
						System.out.println(" Old Date Like : User will NOT be  Rewarded for video like " + cuId);
					}
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(rewardedLkCount);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println(
							"User had liked  some more videos but below BATCH SIZE entry vid like -cu id" + cuId);

				}
				if (currentLkCnt < rewardedLkCount) {
					// User had disliked after getting full rewards ..
					newLikeCnt = currentLkCnt - rewardedLkCount + 1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println(
							"User had disliked  after getting  full rewards should be negative entry vid like -cu id"
									+ cuId);
				}

				if (currentLkCnt >= maxActionPermitedPerDay) {
					// User had liked more after getting full rewards for the day..
					// Make everything to 0
					newLikeCnt = 1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println("User had liked more  after getting  full rewards should be 1 - 0 id" + cuId);
				}

			}
			if (rwdDate.compareTo(currDBDate) == 0) {
				// Current date Record Exists.
				newLikeCnt = currentLkCnt + 1;
				if (rewardedLkCount < maxActionPermitedPerDay && newLikeCnt <= maxActionPermitedPerDay) {
					if (newLikeCnt % batchSize == 0 && newLikeCnt > rewardedLkCount) {

						CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
						cDrl.setCustomerId(cuId);
						cDrl.setTot_vd_lk(newLikeCnt);
						cDrl.setRwd_vd_lk(rewardedLkCount + batchSize);
						SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
						System.out.println(" Same Date Like : User  Rewarded for video like " + cuId);
						isRewarded = true;
					} else {
						// just update record with like counts increased by 1 (newLikeCnt)
						CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
						cDrl.setCustomerId(cuId);
						cDrl.setTot_vd_lk(newLikeCnt);
						cDrl.setRwd_vd_lk(rewardedLkCount);
						SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
						System.out.println(" Same Date Like : User Not  Rewarded for video like " + cuId);

					}
				} else {
					// just update record with like counts increased by 1 (newLikeCnt)
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(rewardedLkCount);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println(" Same Date Like : User Not  Rewarded for video like " + cuId);

				}
			}
		}
		conn.close();

	}

}
