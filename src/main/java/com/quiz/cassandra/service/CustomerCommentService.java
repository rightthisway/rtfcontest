package com.quiz.cassandra.service;

import java.util.List;

import com.quiz.cassandra.list.Comments;
import com.quiz.cassandra.list.CommentsDTO;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class CustomerCommentService {
	
	public static Comments saveCustomerMediaComments(Comments comments) {		
		Integer updCnt = PollingSQLDaoUtil.saveCommentsOnMedia(comments) ;
		comments.setSts(updCnt);
		return comments;		
	}
	
	
	public static CommentsDTO fetchMediaComments(CommentsDTO commentsDTO) {
		try {
			List<Comments> cmtList = PollingSQLDaoUtil.fetchMediaComments(commentsDTO.getSrcId() );	
			commentsDTO.setcLst(cmtList);
			commentsDTO.setSts(1);
			return commentsDTO;	
		}catch(Exception ex) {  // need to change as exception will not be propogated
			ex.printStackTrace();
			commentsDTO.setSts(0);
		}
		return commentsDTO;	
			
	}
	
	
	public static Comments deleteMediaComments(Comments comments) {
		try {
			Integer sts = PollingSQLDaoUtil.deleteMediaComments(comments.getCmtId() );			
			comments.setSts(sts);
			return comments;	
		}catch(Exception ex) {  
			ex.printStackTrace();
			comments.setSts(0);
		}
		return comments;	
			
	}
	
	public static Comments editMediaComments(Comments comments) {
		try {
			Integer sts = PollingSQLDaoUtil.editMediaComments(comments );			
			comments.setSts(sts);
			return comments;	
		}catch(Exception ex) {  
			ex.printStackTrace();
			comments.setSts(0);
		}
		return comments;	
			
	}
	

public static void main(String a[]) {
	Comments c = new Comments();
	c.setSrcId(1234);
	c.setCuId(2);
	c.setImgUrl("lloyd4.jpg");
	c.setVidUrl("videourl4.mp4");
	c.settUrl("thumbnaiilurl");
	c.setCmtTxt(" this is edited text .. this should be first   comment 555555555555555555  " );
	c.setCmtId(3);
	//editMediaComments(c);
	//deleteMediaComments(c);
	//saveCustomerMediaComments(c);
	CommentsDTO commentsDTO = new CommentsDTO();
	commentsDTO.setSrcId(31);
	fetchMediaComments(commentsDTO);
	
	System.out.println(commentsDTO.getcLst().size());
	//System.out.println(commentsDTO.getcLst().size());
}
}