package com.quiz.cassandra.service;

import java.sql.Connection;
import java.util.Date;

import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CustDailyRewardsVO;
import com.quiz.cassandra.list.CustRewardValueInfo;
import com.quiz.cassandra.list.CustomerDailyRewardLimits;
import com.quiz.cassandra.list.FanClubEventInfo;
import com.quiz.cassandra.list.FanClubPostsDTO;
import com.quiz.cassandra.list.PollingVideoRewardInfo;
import com.quiz.cassandra.list.RtfRewardConfigInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.DatabaseConnections;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;

public class CustomerRewardLimtService {

	public static CustRewardValueInfo isVideoLikeActionRewarded(Integer cuId, String actionType) throws Exception {
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		cvo.setIsRewarded(Boolean.FALSE);
		Connection conn = DatabaseConnections.getRtfConnection();
		Boolean isRewarded = false;
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.LIKE_VIDEO.name());
		System.out.println(rtfRwdConfigInfo);
		Integer rewardPointsToBeGiven = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer batchSize = rtfRwdConfigInfo.getBatchSizePerDay();
	//	Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxActionsPerDay();
		Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getRtfPoints();
		Integer newLikeCnt = 0;

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);

		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTot_vd_lk(1);
			cDrl.setRwd_vd_lk(0);
			SQLDaoUtil.insertCustDailyRwdsLimitVidLikeStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid like   " + cuId);
			cvo.setIsRewarded(Boolean.FALSE);
			cvo.setRwdQty(0);
			return cvo;
		} else {
			// Existing record for vide likes ..
			System.out.println(" custDlyRwd existing recs found  for Customer   " + cuId);
			Date rwdDate = custDlyRwds.getVidLkDate();
			Date currDBDate = custDlyRwds.getCurrDate();
			Integer currentLkCnt = custDlyRwds.getTot_vd_lk();
			Integer rewardedLkCount = custDlyRwds.getRwd_vd_lk();
			System.out.println("rwdDate " + rwdDate);
			System.out.println("currDBDate " + currDBDate);			


			if (rwdDate == null ) {
				System.out.println(" custDlyRwd existing recs but no recs for video like for Customer   " + cuId);
				CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
				cDrl.setCustomerId(cuId);
				cDrl.setTot_vd_lk(1);
				cDrl.setRwd_vd_lk(0);
				SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				return cvo;
				
			}

			if (rwdDate.compareTo(currDBDate) < 0) {

				// Historic Record ...migrate prev balance likes/unlikes to new record
				System.out.println("rwdDate is less than current date " + rwdDate);
				if (currentLkCnt > rewardedLkCount && currentLkCnt < maxActionPermitedPerDay) {
					// case where upto 4 likes added after getting 45 rewards which needs to be
					// carried forward to next day
					// rewardedLkCount = 0 ;
					newLikeCnt = (currentLkCnt - rewardedLkCount) + 1;
					// check if newLikeCnt is of batch size to credit rewards ..
					// Credit Rewards ...
					if (newLikeCnt % batchSize == 0 && newLikeCnt <= batchSize) {
						// Credit fresh Rewards with new date ...
						rewardedLkCount = batchSize;
						isRewarded = true;
						cvo.setIsRewarded(Boolean.TRUE);
						cvo.setRwdQty(rewardPointsforCredit);
						
						System.out.println(" Old Date Like : User will be  Rewarded for video like " + cuId);
					} else {
						// just update new like count with todays date record
						rewardedLkCount = 0;
						System.out.println(" Old Date Like : User will NOT be  Rewarded for video like " + cuId);
						cvo.setIsRewarded(Boolean.FALSE);
						cvo.setRwdQty(0);
					}
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(rewardedLkCount);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println(
							"User had liked  some more videos but below BATCH SIZE entry vid like -cu id" + cuId);
						return cvo;
				}
				if (currentLkCnt <= rewardedLkCount) {
					// User had disliked after getting full rewards ..
					newLikeCnt = currentLkCnt - rewardedLkCount + 1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println(
							"User had disliked  after getting  full rewards should be negative entry vid like -cu id"
									+ cuId);
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}

				if (currentLkCnt >= maxActionPermitedPerDay) {
					// User had liked more after getting full rewards for the day..
					// Make everything to 0
					newLikeCnt = 1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println("User had liked more  after getting  full rewards should be 1 - 0 id" + cuId);
				
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}

			}
			if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
				// Current date Record Exists.
				newLikeCnt = currentLkCnt + 1;
				if (rewardedLkCount < maxActionPermitedPerDay && newLikeCnt <= maxActionPermitedPerDay) {
					if (newLikeCnt % batchSize == 0 && newLikeCnt > rewardedLkCount) {

						CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
						cDrl.setCustomerId(cuId);
						cDrl.setTot_vd_lk(newLikeCnt);
						cDrl.setRwd_vd_lk(rewardedLkCount + batchSize);
						SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
						System.out.println(" Same Date Like : User  Rewarded for video like " + cuId);
						isRewarded = true;
						cvo.setIsRewarded(Boolean.TRUE);
						cvo.setRwdQty(rewardPointsforCredit);
						return cvo;
					} else {
						// just update record with like counts increased by 1 (newLikeCnt)
						CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
						cDrl.setCustomerId(cuId);
						cDrl.setTot_vd_lk(newLikeCnt);
						cDrl.setRwd_vd_lk(rewardedLkCount);
						SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
						System.out.println(" Same Date Like : User Not  Rewarded for video like " + cuId);
						cvo.setIsRewarded(Boolean.FALSE);
						cvo.setRwdQty(0);
						return cvo;
					}
				} else {
					// just update record with like counts increased by 1 (newLikeCnt)
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(rewardedLkCount);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println(" Same Date Like : User Not  Rewarded for video like " + cuId);
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}
			}
		}
		conn.close();
		return cvo;

	}

	

	public static CustRewardValueInfo processVideoViewRewards(CassCustomer customer,
			PollingVideoRewardInfo pollingVideoRewardInfo) {
		
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		try {
			
		
		if("FC".equalsIgnoreCase(pollingVideoRewardInfo.getSource())) {			
			Integer inventoryVidId = PollingSQLDaoUtil.fetchInventoryIdForFanClubVideo(pollingVideoRewardInfo.getAwsVideoId()) ;
			if(inventoryVidId <= 0 )  throw new Exception ("NO Video mapped in inventory for " + pollingVideoRewardInfo.getAwsVideoId());
			pollingVideoRewardInfo.setAwsVideoId(String.valueOf(inventoryVidId));
			try {
			PollingSQLDaoUtil.insertFanClubVideoPlayedHist(customer.getId(), inventoryVidId);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
			}
		
		Integer rewardCntForVideoId = PollingSQLDaoUtil.custRewardCountForSameVideo(pollingVideoRewardInfo.getCuId() , pollingVideoRewardInfo.getAwsVideoId());
		if(rewardCntForVideoId > 0) {			
			pollingVideoRewardInfo.setSts(0);
			System.out.println( PollingUtil.VIDEO_VIEW_REWARD_DUPLICATE);
			pollingVideoRewardInfo.setShowMsg(PollingUtil.VIDEO_VIEW_REWARD_DUPLICATE);
			return cvo;
		}
		
		//***************  START REWARDS CREDIT  ***************************//
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.WATCH_VIDEO.name());
		System.out.println(rtfRwdConfigInfo);
		
		
		Integer maxRewardsPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getRtfPoints();		
		Integer cuId = pollingVideoRewardInfo.getCuId();

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		Date rwdDate = custDlyRwds.getVdVwDate();
		Date currDBDate = custDlyRwds.getCurrDate();
		Integer currentCnt = custDlyRwds.getTotVdVw();
		Integer rewardedCount = custDlyRwds.getRwd_vd_vw();
		if(currentCnt == null )  currentCnt = 0;
		if(rewardedCount == null) rewardedCount = 0;
		Connection conn = DatabaseConnections.getRtfConnection();
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTotVdVw(1);
			cDrl.setRwd_vd_vw(rewardPointsforCredit);
			SQLDaoUtil.insertCustDailyRwdsLimitVidViewStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid view   " + cuId);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		

		if(rewardedCount == 0 ) rewardedCount = 0;
		System.out.println("rwdDate " + rwdDate);
		System.out.println("currDBDate " + currDBDate);		
		if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
			// Historic Record ...update with todays date the first record
			System.out.println("rwdDate is less than current date " + rwdDate);
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTotVdVw(1);
			cDrl.setRwd_vd_vw(rewardPointsforCredit);
			SQLDaoUtil.updateCustDailyRwdsLimitVidViewStats(cDrl, conn);
			System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTotVdVw(currentCnt + 1);
			// Current date Record .. check for max  reward likit 
			System.out.println("rwdDate is  current date " + rwdDate);			
			if(rewardedCount < maxRewardsPermitedPerDay) {
				cDrl.setRwd_vd_vw(rewardedCount + rewardPointsforCredit);
				//*** CREDIT REWARDS AND RETURN  .....
				cvo.setIsRewarded(Boolean.TRUE);
				cvo.setRwdQty(rewardPointsforCredit);
				System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
				
			}else {
				cDrl.setRwd_vd_vw(rewardedCount );
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				System.out.println("NO REWARDS AS MAX REACHED HAS A/C  POINT = " + rewardedCount);
			}
			SQLDaoUtil.updateCustDailyRwdsLimitVidViewStats(cDrl, conn);
			DatabaseConnections.closeConnection(conn);
			return cvo;
		}		
		}catch(Exception ex) {			
			pollingVideoRewardInfo.setSts(0);
			ex.getStackTrace();
			System.out.println(ex.getMessage());			
			return cvo;			
		}
	
		return cvo;
	}
	
	

	public static CustRewardValueInfo processCreatePostRewards(CassCustomer customer,
			FanClubPostsDTO fanClubPostsDTO) {
		
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		try {
			
		
			
		//***************  START REWARDS CREDIT  ***************************//
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.
				getAllRtfRewardConfigurations(SourceType.CREATE_POST.name());
		System.out.println(rtfRwdConfigInfo);
		
		
		Integer maxRewardsPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getRtfPoints();		
		Integer cuId = fanClubPostsDTO.getCuId();

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		Date rwdDate = custDlyRwds.getFcCrPostDate();
		Date currDBDate = custDlyRwds.getCurrDate();
		Integer currentCnt = custDlyRwds.getTotFcCrPo();
		Integer rewardedCount = custDlyRwds.getRwdFc_cr_po();
		if(currentCnt == null )  currentCnt = 0;
		if(rewardedCount == null) rewardedCount = 0;
		Connection conn = DatabaseConnections.getRtfConnection();
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			
			SQLDaoUtil.insertCustDailyRwdsLimitCreatePostStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid view   " + cuId);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		

		if(rewardedCount == 0 ) rewardedCount = 0;
		System.out.println("rwdDate " + rwdDate);
		System.out.println("currDBDate " + currDBDate);		
		if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
			// Historic Record ...update with todays date the first record
			System.out.println("rwdDate is less than current date " + rwdDate);
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			SQLDaoUtil.updateCustDailyRwdsLimitCreatePostStats(cDrl, conn);
			System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(currentCnt + 1);
			
			
			// Current date Record .. check for max  reward likit 
			System.out.println("rwdDate is  current date " + rwdDate);			
			if(rewardedCount < maxRewardsPermitedPerDay) {
				
				cDrl.setRewardedAction(rewardedCount + rewardPointsforCredit);
				//*** CREDIT REWARDS AND RETURN  .....
				cvo.setIsRewarded(Boolean.TRUE);
				cvo.setRwdQty(rewardPointsforCredit);
				System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
				
			}else {
				cDrl.setRewardedAction(rewardedCount );
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				System.out.println("NO REWARDS AS MAX REACHED HAS A/C  POINT = " + rewardedCount);
			}
			SQLDaoUtil.updateCustDailyRwdsLimitCreatePostStats(cDrl, conn);
			DatabaseConnections.closeConnection(conn);
			return cvo;
		}		
		}catch(Exception ex) {			
			fanClubPostsDTO.setSts(0);
			ex.getStackTrace();
			System.out.println(ex.getMessage());			
			return cvo;			
		}
	
		return cvo;
	}
	

	public static CustRewardValueInfo processCreateEventRewards(CassCustomer customer,
			FanClubEventInfo fanClubEventInfo) {
		
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		try {
			
		
			
		//***************  START REWARDS CREDIT  ***************************//
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.
				getAllRtfRewardConfigurations(SourceType.CREATE_EVENT.name());
		System.out.println(rtfRwdConfigInfo);
		
		
		Integer maxRewardsPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getRtfPoints();		
		Integer cuId = customer.getId();

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		Date rwdDate = custDlyRwds.getFcEvDate();
		Date currDBDate = custDlyRwds.getCurrDate();
		Integer currentCnt = custDlyRwds.getTotFcEv();
		Integer rewardedCount = custDlyRwds.getRwdFcEv();
		if(currentCnt == null )  currentCnt = 0;
		if(rewardedCount == null) rewardedCount = 0;
		Connection conn = DatabaseConnections.getRtfConnection();
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			
			SQLDaoUtil.insertCustDailyRwdsLimitCreateEventStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid view   " + cuId);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		

		if(rewardedCount == 0 ) rewardedCount = 0;
		System.out.println("rwdDate " + rwdDate);
		System.out.println("currDBDate " + currDBDate);		
		if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
			// Historic Record ...update with todays date the first record
			System.out.println("rwdDate is less than current date " + rwdDate);
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			SQLDaoUtil.updateCustDailyRwdsLimitCreateEventStats(cDrl, conn);
			System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(currentCnt + 1);
			
			
			// Current date Record .. check for max  reward likit 
			System.out.println("rwdDate is  current date " + rwdDate);			
			if(rewardedCount < maxRewardsPermitedPerDay) {
				
				cDrl.setRewardedAction(rewardedCount + rewardPointsforCredit);
				//*** CREDIT REWARDS AND RETURN  .....
				cvo.setIsRewarded(Boolean.TRUE);
				cvo.setRwdQty(rewardPointsforCredit);
				System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
				
			}else {
				cDrl.setRewardedAction(rewardedCount );
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				System.out.println("NO REWARDS AS MAX REACHED HAS A/C  POINT = " + rewardedCount);
			}
			SQLDaoUtil.updateCustDailyRwdsLimitCreateEventStats(cDrl, conn);
			DatabaseConnections.closeConnection(conn);
			return cvo;
		}		
		}catch(Exception ex) {			
			fanClubEventInfo.setSts(0);
			ex.getStackTrace();
			System.out.println(ex.getMessage());			
			return cvo;			
		}
	
		return cvo;
	}
	
	
	public static CustRewardValueInfo processFanClubVideoUploadRewards(CassCustomer customer
			) {
		
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		try {
			
		
			
		//***************  START REWARDS CREDIT  ***************************//
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.
				getAllRtfRewardConfigurations(SourceType.UPLOAD_FANCLUB_VIDEO.name());
		System.out.println(rtfRwdConfigInfo);
		
		
		Integer maxRewardsPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getRtfPoints();		
		Integer cuId = customer.getId();

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		Date rwdDate = custDlyRwds.getFcVidUpldDate();
		Date currDBDate = custDlyRwds.getCurrDate();
		Integer currentCnt = custDlyRwds.getTotFcVdUl();
		Integer rewardedCount = custDlyRwds.getRwdFc_vd_ul();
		if(currentCnt == null )  currentCnt = 0;
		if(rewardedCount == null) rewardedCount = 0;
		Connection conn = DatabaseConnections.getRtfConnection();
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			
			SQLDaoUtil.insertCustDailyRwdsLimitFCVideoUploadStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid view   " + cuId);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		

		if(rewardedCount == 0 ) rewardedCount = 0;
		System.out.println("rwdDate " + rwdDate);
		System.out.println("currDBDate " + currDBDate);		
		if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
			// Historic Record ...update with todays date the first record
			System.out.println("rwdDate is less than current date " + rwdDate);
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			SQLDaoUtil.updateCustDailyRwdsLimitFCVideoUploadStats(cDrl, conn);
			System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(currentCnt + 1);
			
			
			// Current date Record .. check for max  reward likit 
			System.out.println("rwdDate is  current date " + rwdDate);			
			if(rewardedCount < maxRewardsPermitedPerDay) {
				
				cDrl.setRewardedAction(rewardedCount + rewardPointsforCredit);
				//*** CREDIT REWARDS AND RETURN  .....
				cvo.setIsRewarded(Boolean.TRUE);
				cvo.setRwdQty(rewardPointsforCredit);
				System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
				
			}else {
				cDrl.setRewardedAction(rewardedCount );
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				System.out.println("NO REWARDS AS MAX REACHED HAS A/C  POINT = " + rewardedCount);
			}
			SQLDaoUtil.updateCustDailyRwdsLimitFCVideoUploadStats(cDrl, conn);
			DatabaseConnections.closeConnection(conn);
			return cvo;
		}		
		}catch(Exception ex) {			
			//fanClubEventInfo.setSts(0);
			ex.getStackTrace();
			System.out.println(ex.getMessage());			
			return cvo;			
		}
	
		return cvo;
	}	
	public static CustRewardValueInfo processFFOVideoUploadRewards
	(CassCustomer customer
			) {
		
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		try {		
		
			
		//***************  START REWARDS CREDIT  ***************************//
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.
				getAllRtfRewardConfigurations(SourceType.UPLOAD_FAN_FREAKOUT_VIDEO.name());
		System.out.println(rtfRwdConfigInfo);
		
		
		Integer maxRewardsPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getRtfPoints();		
		Integer cuId = customer.getId();

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		Date rwdDate = custDlyRwds.getFfoVidUpldDate();
		Date currDBDate = custDlyRwds.getCurrDate();
		Integer currentCnt = custDlyRwds.getTotFfo_vd_ul();
		Integer rewardedCount = custDlyRwds.getRwdFfo_vd_ul();
		if(currentCnt == null )  currentCnt = 0;
		if(rewardedCount == null) rewardedCount = 0;
		Connection conn = DatabaseConnections.getRtfConnection();
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			
			SQLDaoUtil.insertCustDailyRwdsLimitFFOVideoUploadStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid view   " + cuId);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		

		if(rewardedCount == 0 ) rewardedCount = 0;
		System.out.println("rwdDate " + rwdDate);
		System.out.println("currDBDate " + currDBDate);		
		if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
			// Historic Record ...update with todays date the first record
			System.out.println("rwdDate is less than current date " + rwdDate);
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			SQLDaoUtil.updateCustDailyRwdsLimitFFOVideoUploadStats(cDrl, conn);
			System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(currentCnt + 1);
			
			
			// Current date Record .. check for max  reward likit 
			System.out.println("rwdDate is  current date " + rwdDate);			
			if(rewardedCount < maxRewardsPermitedPerDay) {
				
				cDrl.setRewardedAction(rewardedCount + rewardPointsforCredit);
				//*** CREDIT REWARDS AND RETURN  .....
				cvo.setIsRewarded(Boolean.TRUE);
				cvo.setRwdQty(rewardPointsforCredit);
				System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
				
			}else {
				cDrl.setRewardedAction(rewardedCount );
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				System.out.println("NO REWARDS AS MAX REACHED HAS A/C  POINT = " + rewardedCount);
			}
			SQLDaoUtil.updateCustDailyRwdsLimitFFOVideoUploadStats(cDrl, conn);
			DatabaseConnections.closeConnection(conn);
			return cvo;
		}		
		}catch(Exception ex) {		
			cvo.setIsRewarded(Boolean.FALSE);
			cvo.setRwdQty(0);
			ex.getStackTrace();						
			return cvo;			
		}
	
		return cvo;
	}
	
	public static CustRewardValueInfo processShareVideoRewards(CassCustomer customer) {
		
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		try {		
		
			
		//***************  START REWARDS CREDIT  ***************************//
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.
				getAllRtfRewardConfigurations(SourceType.SHARE_VIDEO.name());
		System.out.println(rtfRwdConfigInfo);		
		
		Integer maxRewardsPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getSfStars();		
		Integer cuId = customer.getId();

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		Date rwdDate = custDlyRwds.getShVidDate();
		Date currDBDate = custDlyRwds.getCurrDate();
		Integer currentCnt = custDlyRwds.getTot_sh_vd();
		Integer rewardedCount = custDlyRwds.getRwd_sh_vd();
		if(currentCnt == null )  currentCnt = 0;
		if(rewardedCount == null) rewardedCount = 0;
		Connection conn = DatabaseConnections.getRtfConnection();
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			
			SQLDaoUtil.insertCustDailyRwdsLimitShareVideoStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid view   " + cuId);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		

		if(rewardedCount == 0 ) rewardedCount = 0;
		System.out.println("rwdDate " + rwdDate);
		System.out.println("currDBDate " + currDBDate);		
		if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
			// Historic Record ...update with todays date the first record
			System.out.println("rwdDate is less than current date " + rwdDate);
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(1);
			cDrl.setRewardedAction(rewardPointsforCredit);
			SQLDaoUtil.updateCustDailyRwdsLimitShareVideoStats(cDrl, conn);
			System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
			//*** CREDIT REWARDS AND RETURN  .....
			cvo.setIsRewarded(Boolean.TRUE);
			cvo.setRwdQty(rewardPointsforCredit);
			return cvo;
		}
		if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
			CustDailyRewardsVO cDrl = new CustDailyRewardsVO();
			cDrl.setCustId(cuId);
			cDrl.setTotalAction(currentCnt + 1);
			
			
			// Current date Record .. check for max  reward likit 
			System.out.println("rwdDate is  current date " + rwdDate);			
			if(rewardedCount < maxRewardsPermitedPerDay) {
				
				cDrl.setRewardedAction(rewardedCount + rewardPointsforCredit);
				//*** CREDIT REWARDS AND RETURN  .....
				cvo.setIsRewarded(Boolean.TRUE);
				cvo.setRwdQty(rewardPointsforCredit);
				System.out.println("Rewarding customer with [points] " + rewardPointsforCredit);
				
			}else {
				cDrl.setRewardedAction(rewardedCount );
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				System.out.println("NO REWARDS AS MAX REACHED HAS A/C  POINT = " + rewardedCount);
			}
			SQLDaoUtil.updateCustDailyRwdsLimitShareVideoStats(cDrl, conn);
			DatabaseConnections.closeConnection(conn);
			return cvo;
		}		
		}catch(Exception ex) {		
			cvo.setIsRewarded(Boolean.FALSE);
			cvo.setRwdQty(0);
			ex.getStackTrace();						
			return cvo;			
		}
	
		return cvo;
	}
	
	

	public static CustRewardValueInfo fanClubEventInterestRewards(Integer cuId) throws Exception {
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		cvo.setIsRewarded(Boolean.FALSE);
		Connection conn = DatabaseConnections.getRtfConnection();
		Boolean isRewarded = false;
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.SHOW_INTEREST_TO_EVENT.name());
		System.out.println(rtfRwdConfigInfo);
		Integer rewardPointsToBeGiven = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer batchSize = rtfRwdConfigInfo.getBatchSizePerDay();
	//	Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxActionsPerDay();
		Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getRtfPoints();
		Integer newLikeCnt = 0;

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);

		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTotFcEv_lk(1);  
			cDrl.setRwdFcEv_lk(0);
			SQLDaoUtil.insertCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid like   " + cuId);
			cvo.setIsRewarded(Boolean.FALSE);
			cvo.setRwdQty(0);
			return cvo;
		} else {
			// Existing record for vide likes ..
			System.out.println(" custDlyRwd existing recs found  for Customer   " + cuId);
			Date rwdDate = custDlyRwds.getFcEvLkDate();
			Date currDBDate = custDlyRwds.getCurrDate();
			Integer currentLkCnt = custDlyRwds.getTotFcEv_lk();
			Integer rewardedLkCount = custDlyRwds.getRwdFcEv_lk();
			System.out.println("rwdDate " + rwdDate);
			System.out.println("currDBDate " + currDBDate);			
			
			if(rwdDate == null) {
				CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
				cDrl.setCustomerId(cuId);
				cDrl.setTotFcEv_lk(1);
				cDrl.setRwdFcEv_lk(0);				
				SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				return cvo;
			}
			if ( rwdDate.compareTo(currDBDate) < 0) {

				// Historic Record ...migrate prev balance likes/unlikes to new record
				System.out.println("rwdDate is less than current date " + rwdDate);
				if (currentLkCnt > rewardedLkCount && currentLkCnt < maxActionPermitedPerDay) {
					// case where upto 4 likes added after getting 45 rewards which needs to be
					// carried forward to next day
					// rewardedLkCount = 0 ;
					newLikeCnt = (currentLkCnt - rewardedLkCount) + 1;
					// check if newLikeCnt is of batch size to credit rewards ..
					// Credit Rewards ...
					if (newLikeCnt % batchSize == 0 && newLikeCnt <= batchSize) {
						// Credit fresh Rewards with new date ...
						rewardedLkCount = batchSize;
						isRewarded = true;
						cvo.setIsRewarded(Boolean.TRUE);
						cvo.setRwdQty(rewardPointsforCredit);
						
						System.out.println(" Old Date Like : User will be  Rewarded for video like " + cuId);
					} else {
						// just update new like count with todays date record
						rewardedLkCount = 0;
						System.out.println(" Old Date Like : User will NOT be  Rewarded for video like " + cuId);
						cvo.setIsRewarded(Boolean.FALSE);
						cvo.setRwdQty(0);
					}
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcEv_lk(newLikeCnt);
					cDrl.setRwdFcEv_lk(rewardedLkCount);
					
					SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
					System.out.println(
							"User had liked  some more videos but below BATCH SIZE entry vid like -cu id" + cuId);
						return cvo;
				}
				if (currentLkCnt <= rewardedLkCount) {
					// User had disliked after getting full rewards ..
					newLikeCnt = currentLkCnt - rewardedLkCount + 1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcEv_lk(newLikeCnt);
					cDrl.setRwdFcEv_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
					System.out.println(
							"User had disliked  after getting  full rewards should be negative entry vid like -cu id"
									+ cuId);
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}

				if (currentLkCnt >= maxActionPermitedPerDay) {
					// User had liked more after getting full rewards for the day..
					// Make everything to 0
					newLikeCnt = 1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcEv_lk(newLikeCnt);
					cDrl.setRwdFcEv_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
					System.out.println("User had liked more  after getting  full rewards should be 1 - 0 id" + cuId);
				
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}

			}
			if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
				// Current date Record Exists.
				newLikeCnt = currentLkCnt + 1;
				if (rewardedLkCount < maxActionPermitedPerDay && newLikeCnt <= maxActionPermitedPerDay) {
					if (newLikeCnt % batchSize == 0 && newLikeCnt > rewardedLkCount) {

						CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
						cDrl.setCustomerId(cuId);
						cDrl.setTotFcEv_lk(newLikeCnt);
						cDrl.setRwdFcEv_lk(rewardedLkCount + batchSize);
						SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
						System.out.println(" Same Date Like : User  Rewarded for video like " + cuId);
						isRewarded = true;
						cvo.setIsRewarded(Boolean.TRUE);
						cvo.setRwdQty(rewardPointsforCredit);
						return cvo;
					} else {
						// just update record with like counts increased by 1 (newLikeCnt)
						CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
						cDrl.setCustomerId(cuId);
						cDrl.setTotFcEv_lk(newLikeCnt);
						cDrl.setRwdFcEv_lk(rewardedLkCount);
						SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
						System.out.println(" Same Date Like : User Not  Rewarded for video like " + cuId);
						cvo.setIsRewarded(Boolean.FALSE);
						cvo.setRwdQty(0);
						return cvo;
					}
				} else {
					// just update record with like counts increased by 1 (newLikeCnt)
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcEv_lk(newLikeCnt);
					cDrl.setRwdFcEv_lk(rewardedLkCount);
					SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
					System.out.println(" Same Date Like : User Not  Rewarded for video like " + cuId);
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}
			}
		}
		conn.close();
		return cvo;

	}

	
	public static CustRewardValueInfo fanClubPostLikeRewards(Integer cuId) throws Exception {
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		cvo.setIsRewarded(Boolean.FALSE);
		Connection conn = DatabaseConnections.getRtfConnection();
		Boolean isRewarded = false;
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.LIKE_POST.name());
		System.out.println(rtfRwdConfigInfo);
		Integer rewardPointsToBeGiven = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer batchSize = rtfRwdConfigInfo.getBatchSizePerDay();
	//	Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxActionsPerDay();
		Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();
		Integer rewardPointsforCredit = rtfRwdConfigInfo.getRtfPoints();
		Integer newLikeCnt = 0;

		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);

		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTotFcPoLk(1);  
			cDrl.setRwdFc_po_lk(0);
			SQLDaoUtil.insertCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid like   " + cuId);
			cvo.setIsRewarded(Boolean.FALSE);
			cvo.setRwdQty(0);
			return cvo;
		} else {
			// Existing record for customer ..
			System.out.println(" custDlyRwd existing recs found  for Customer   " + cuId);
			Date rwdDate = custDlyRwds.getFcPostLikeDate();
			Date currDBDate = custDlyRwds.getCurrDate();
			Integer currentLkCnt = custDlyRwds.getTotFcPoLk();
			Integer rewardedLkCount = custDlyRwds.getRwdFc_po_lk();
			System.out.println("rwdDate " + rwdDate);
			System.out.println("currDBDate " + currDBDate);
			
			
			if (rwdDate == null ) {
				System.out.println(" custDlyRwd existing recs but no recs for fcpostlike for Customer   " + cuId);
				CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
				cDrl.setCustomerId(cuId);
				cDrl.setTotFcPoLk(1);
				cDrl.setRwdFc_po_lk(0);				
				SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
				cvo.setIsRewarded(Boolean.FALSE);
				cvo.setRwdQty(0);
				return cvo;
				
			}
			

			if ( rwdDate.compareTo(currDBDate) < 0) {

				// Historic Record ...migrate prev balance likes/unlikes to new record
				System.out.println("rwdDate is less than current date " + rwdDate);
				if (currentLkCnt > rewardedLkCount && currentLkCnt < maxActionPermitedPerDay) {
					// case where upto 4 likes added after getting 45 rewards which needs to be
					// carried forward to next day
					// rewardedLkCount = 0 ;
					newLikeCnt = (currentLkCnt - rewardedLkCount) + 1;
					// check if newLikeCnt is of batch size to credit rewards ..
					// Credit Rewards ...
					if (newLikeCnt % batchSize == 0 && newLikeCnt <= batchSize) {
						// Credit fresh Rewards with new date ...
						rewardedLkCount = batchSize;
						isRewarded = true;
						cvo.setIsRewarded(Boolean.TRUE);
						cvo.setRwdQty(rewardPointsforCredit);
						
						System.out.println(" Old Date Like : User will be  Rewarded for video like " + cuId);
					} else {
						// just update new like count with todays date record
						rewardedLkCount = 0;
						System.out.println(" Old Date Like : User will NOT be  Rewarded for video like " + cuId);
						cvo.setIsRewarded(Boolean.FALSE);
						cvo.setRwdQty(0);
					}
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcPoLk(newLikeCnt);
					cDrl.setRwdFc_po_lk(rewardedLkCount);
					
					SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
					System.out.println(
							"User had liked  some more videos but below BATCH SIZE entry vid like -cu id" + cuId);
						return cvo;
				}
				if (currentLkCnt <= rewardedLkCount) {
					// User had disliked after getting full rewards ..
					newLikeCnt = currentLkCnt - rewardedLkCount + 1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcPoLk(newLikeCnt);
					cDrl.setRwdFc_po_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
					System.out.println(
							"User had disliked  after getting  full rewards should be negative entry vid like -cu id"
									+ cuId);
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}

				if (currentLkCnt >= maxActionPermitedPerDay) {
					// User had liked more after getting full rewards for the day..
					// Make everything to 0
					newLikeCnt = 1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcPoLk(newLikeCnt);
					cDrl.setRwdFc_po_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
					System.out.println("User had liked more  after getting  full rewards should be 1 - 0 id" + cuId);				
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}

			}
			if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
				// Current date Record Exists.
				newLikeCnt = currentLkCnt + 1;
				if (rewardedLkCount < maxActionPermitedPerDay && newLikeCnt <= maxActionPermitedPerDay) {
					if (newLikeCnt % batchSize == 0 && newLikeCnt > rewardedLkCount) {
						CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
						cDrl.setCustomerId(cuId);
						cDrl.setTotFcPoLk(newLikeCnt);
						cDrl.setRwdFc_po_lk(rewardedLkCount + batchSize);
						SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
						System.out.println(" Same Date Like : User  Rewarded for video like " + cuId);
						isRewarded = true;
						cvo.setIsRewarded(Boolean.TRUE);
						cvo.setRwdQty(rewardPointsforCredit);
						return cvo;
					} else {
						// just update record with like counts increased by 1 (newLikeCnt)
						CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
						cDrl.setCustomerId(cuId);
						cDrl.setTotFcPoLk(newLikeCnt);
						cDrl.setRwdFc_po_lk(rewardedLkCount);
						SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
						System.out.println(" Same Date Like : User Not  Rewarded for video like " + cuId);
						cvo.setIsRewarded(Boolean.FALSE);
						cvo.setRwdQty(0);
						return cvo;
					}
				} else {
					// just update record with like counts increased by 1 (newLikeCnt)
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcPoLk(newLikeCnt);
					cDrl.setRwdFc_po_lk(rewardedLkCount);
					SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
					System.out.println(" Same Date Like : User Not  Rewarded for video like " + cuId);
					cvo.setIsRewarded(Boolean.FALSE);
					cvo.setRwdQty(0);
					return cvo;
				}
			}
		}
		conn.close();
		return cvo;

	}	
	
	

	public static CustRewardValueInfo fanClubPostUnLikeRewards(Integer cuId) throws Exception {
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		cvo.setIsRewarded(Boolean.FALSE);
		Connection conn = DatabaseConnections.getRtfConnection();	
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.LIKE_POST.name());
		System.out.println(rtfRwdConfigInfo);		
		Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();		
		Integer newLikeCnt = 0;
		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTotFcPoLk(-1);  
			cDrl.setRwdFc_po_lk(0);
			SQLDaoUtil.insertCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for post un like   " + cuId);
			cvo.setIsRewarded(Boolean.FALSE);
			cvo.setRwdQty(0);
			return cvo;
		} else {
			// Existing record for vide likes ..
			System.out.println(" custDlyRwd existing recs found  for Customer   " + cuId);
			Date rwdDate = custDlyRwds.getFcPostLikeDate();
			Date currDBDate = custDlyRwds.getCurrDate();
			Integer currentLkCnt = custDlyRwds.getTotFcPoLk();
			Integer rewardedLkCount = custDlyRwds.getRwdFc_po_lk();
			System.out.println("rwdDate " + rwdDate);
			System.out.println("currDBDate " + currDBDate);
			
			if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
				// Historic Record ...migrate prev balance likes/unlikes to new record
				System.out.println("rwdDate is less than current date " + rwdDate);
				if (currentLkCnt > rewardedLkCount && currentLkCnt < maxActionPermitedPerDay) {
					// case where upto 4 likes added after getting 45 rewards which needs to be
					// carried forward to next day
					// rewardedLkCount = 0 ;
					newLikeCnt = (currentLkCnt - rewardedLkCount) - 1;					
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcPoLk(newLikeCnt);
					cDrl.setRwdFc_po_lk(0);					
					SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
					System.out.println(
							"User had Unliked   posts today after liking some yesterday -cu id" + cuId);
						return cvo;
				}
				if (currentLkCnt <= rewardedLkCount) {
					// User had disliked after getting full rewards ..
					newLikeCnt = currentLkCnt - rewardedLkCount -1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcPoLk(newLikeCnt);
					cDrl.setRwdFc_po_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);
					System.out.println(
							"User had disliked  after getting  full rewards should be negative entry post like -cu id"
									+ cuId);					
					return cvo;
				}
				if (currentLkCnt >= maxActionPermitedPerDay) {
					// User had liked more after getting full rewards for the day..
					// Make everything to 0
					newLikeCnt = -1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcPoLk(newLikeCnt);
					cDrl.setRwdFc_po_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);					
					return cvo;
				}
			}
			if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
				// Current date Record Exists.
				newLikeCnt = currentLkCnt - 1;
				CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
				cDrl.setCustomerId(cuId);
				cDrl.setTotFcPoLk(newLikeCnt);
				cDrl.setRwdFc_po_lk(rewardedLkCount);
				SQLDaoUtil.updateCustDailyRwdsLimitFCPostLikeStats(cDrl, conn);				
			}
		}
		conn.close();
		return cvo;
	}	
	

	public static CustRewardValueInfo videoUnikeRewards(Integer cuId) throws Exception {
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		cvo.setIsRewarded(Boolean.FALSE);
		Connection conn = DatabaseConnections.getRtfConnection();	
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.LIKE_VIDEO.name());
		System.out.println(rtfRwdConfigInfo);		
		Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();		
		Integer newLikeCnt = 0;
		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTot_vd_lk(-1);  
			cDrl.setRwd_vd_lk(0);
			SQLDaoUtil.insertCustDailyRwdsLimitVidLikeStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for vid unlike   " + cuId);
			cvo.setIsRewarded(Boolean.FALSE);
			cvo.setRwdQty(0);
			return cvo;
		} else {
			// Existing record for vide likes ..
			System.out.println(" custDlyRwd existing recs found  for Customer   " + cuId);
			Date rwdDate = custDlyRwds.getVidLkDate();
			Date currDBDate = custDlyRwds.getCurrDate();
			Integer currentLkCnt = custDlyRwds.getTot_vd_lk();
			Integer rewardedLkCount = custDlyRwds.getRwd_vd_lk();
			System.out.println("rwdDate " + rwdDate);
			System.out.println("currDBDate " + currDBDate);
			
			if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
				// Historic Record ...migrate prev balance likes/unlikes to new record
				System.out.println("rwdDate is less than current date " + rwdDate);
				if (currentLkCnt > rewardedLkCount && currentLkCnt < maxActionPermitedPerDay) {
					// case where upto 4 likes added after getting 45 rewards which needs to be
					// carried forward to next day
					// rewardedLkCount = 0 ;
					newLikeCnt = (currentLkCnt - rewardedLkCount) - 1;					
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(0);					
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println(
							"User had Unliked   videos today after liking some yesterday -cu id" + cuId);
						return cvo;
				}
				if (currentLkCnt <= rewardedLkCount) {
					// User had disliked after getting full rewards ..
					newLikeCnt = currentLkCnt - rewardedLkCount -1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);
					System.out.println(
							"User had disliked  after getting  full rewards should be negative entry vid like -cu id"
									+ cuId);					
					return cvo;
				}
				if (currentLkCnt >= maxActionPermitedPerDay) {
					// User had liked more after getting full rewards for the day..
					// Make everything to 0
					newLikeCnt = -1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTot_vd_lk(newLikeCnt);
					cDrl.setRwd_vd_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);					
					return cvo;
				}
			}
			if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
				// Current date Record Exists.
				newLikeCnt = currentLkCnt - 1;
				CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
				cDrl.setCustomerId(cuId);
				cDrl.setTot_vd_lk(newLikeCnt);
				cDrl.setRwd_vd_lk(rewardedLkCount);
				SQLDaoUtil.updateCustDailyRwdsLimitVidLikeStats(cDrl, conn);				
			}
		}
		conn.close();
		return cvo;
	}

	public static CustRewardValueInfo fanClubEventUnikeRewards(Integer cuId) throws Exception {
		CustRewardValueInfo cvo = new CustRewardValueInfo();
		cvo.setIsRewarded(Boolean.FALSE);
		Connection conn = DatabaseConnections.getRtfConnection();	
		RtfRewardConfigInfo rtfRwdConfigInfo = SQLDaoUtil.getAllRtfRewardConfigurations(SourceType.SHOW_INTEREST_TO_EVENT.name());
		System.out.println(rtfRwdConfigInfo);		
		Integer maxActionPermitedPerDay = rtfRwdConfigInfo.getMaxRwdPerDay();		
		Integer newLikeCnt = 0;
		CustomerDailyRewardLimits custDlyRwds = SQLDaoUtil.fetchCustDailyRewardLimitConfig(cuId);
		System.out.println(custDlyRwds);
		if (custDlyRwds == null || custDlyRwds.getCustomerId() == null) {
			System.out.println(" custDlyRwds is null no history found  ");
			// Insert into cust daily table and record first like . for video
			CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
			cDrl.setCustomerId(cuId);
			cDrl.setTotFcEv_lk(-1);  
			cDrl.setRwdFcEv_lk(0);
			SQLDaoUtil.insertCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
			System.out.println(" custDlyRwd no recs found Creating new record First time for event unlike   " + cuId);
			cvo.setIsRewarded(Boolean.FALSE);
			cvo.setRwdQty(0);
			return cvo;
		} else {
			// Existing record for vide likes ..
			System.out.println(" custDlyRwd existing recs found  for Customer   " + cuId);
			Date rwdDate = custDlyRwds.getFcEvLkDate();
			Date currDBDate = custDlyRwds.getCurrDate();
			Integer currentLkCnt = custDlyRwds.getTotFcEv_lk();
			Integer rewardedLkCount = custDlyRwds.getRwdFcEv_lk();
			System.out.println("rwdDate " + rwdDate);
			System.out.println("currDBDate " + currDBDate);
			
			if (rwdDate == null || rwdDate.compareTo(currDBDate) < 0) {
				// Historic Record ...migrate prev balance likes/unlikes to new record
				System.out.println("rwdDate is less than current date " + rwdDate);
				if (currentLkCnt > rewardedLkCount && currentLkCnt < maxActionPermitedPerDay) {
					// case where upto 4 likes added after getting 45 rewards which needs to be
					// carried forward to next day
					// rewardedLkCount = 0 ;
					newLikeCnt = (currentLkCnt - rewardedLkCount) - 1;					
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcEv_lk(newLikeCnt);
					cDrl.setRwdFcEv_lk(0);					
					SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
					System.out.println(
							"User had Unliked   events today after liking some yesterday -cu id" + cuId);
						return cvo;
				}
				if (currentLkCnt <= rewardedLkCount) {
					// User had disliked after getting full rewards ..
					newLikeCnt = currentLkCnt - rewardedLkCount -1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcEv_lk(newLikeCnt);
					cDrl.setRwdFcEv_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);
					System.out.println(
							"User had disliked events after getting  full rewards should be negative entry vid like -cu id"
									+ cuId);					
					return cvo;
				}
				if (currentLkCnt >= maxActionPermitedPerDay) {
					// User had liked more after getting full rewards for the day..
					// Make everything to 0
					newLikeCnt = -1;
					CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
					cDrl.setCustomerId(cuId);
					cDrl.setTotFcEv_lk(newLikeCnt);
					cDrl.setRwdFcEv_lk(0);
					SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);					
					return cvo;
				}
			}
			if (rwdDate != null && rwdDate.compareTo(currDBDate) == 0) {
				// Current date Record Exists.
				newLikeCnt = currentLkCnt - 1;
				CustomerDailyRewardLimits cDrl = new CustomerDailyRewardLimits();
				cDrl.setCustomerId(cuId);
				cDrl.setTotFcEv_lk(newLikeCnt);
				cDrl.setRwdFcEv_lk(rewardedLkCount);
				SQLDaoUtil.updateCustDailyRwdsLimitFCEventLikeStats(cDrl, conn);				
			}
		}
		conn.close();
		return cvo;
	}
	

}