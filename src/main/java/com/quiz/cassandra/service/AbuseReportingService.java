package com.quiz.cassandra.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.quiz.cassandra.list.AbuseReportDTO;
import com.quiz.cassandra.list.AbuseReportInfo;
import com.zonesws.webservices.dao.implementation.FanClubAbuseSQLDAO;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;

public class AbuseReportingService {

	public static AbuseReportDTO fetchAbuseReportOptions(AbuseReportDTO abuseReportDTO) {

		abuseReportDTO.setSts(0);
		try {
			Map<Integer, AbuseReportInfo> abuseOptionMap = PollingSQLDaoUtil
					.fetchAbuseOptions(abuseReportDTO.getAbuseType());
			List<Integer> abusedIds = PollingSQLDaoUtil.fetchAbuseOptionSelectedbyCustomerForMedia(
					abuseReportDTO.getAbuseType(), abuseReportDTO.getCuId(), abuseReportDTO.getSrcId(),abuseReportDTO.getCommentId());

			for (Integer id : abuseOptionMap.keySet()) {
				System.out.println("key: " + id);
				if (abusedIds.contains(id)) {
					abuseOptionMap.get(id).setIsAns(1);
				}
			}

			if (abuseOptionMap != null && abuseOptionMap.size() > 0) {
				abuseReportDTO.setSts(1);				
				List<AbuseReportInfo> listofAbuseOptions= new ArrayList<AbuseReportInfo>(abuseOptionMap.values());
				abuseReportDTO.setAbuseReporOptionList(listofAbuseOptions);
				
			} else {
				abuseReportDTO.setSts(0);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			abuseReportDTO.setSts(0);
		}
		return abuseReportDTO;
	}
	
	public static AbuseReportInfo saveAbuseReport(AbuseReportInfo abuseReportInfo) {
		Integer updCnt = 0;
		abuseReportInfo.setSts(updCnt);
		try {
			
			if(abuseReportInfo.getCommentId() == null) abuseReportInfo.setCommentId(-1);
			updCnt = PollingSQLDaoUtil.saveAbuseReported(abuseReportInfo.getCuId() ,
				abuseReportInfo.getSrcId() ,abuseReportInfo.getAbuseType() , abuseReportInfo.getCommentId() , abuseReportInfo.getAbuseOptionId() );
			abuseReportInfo.setSts(updCnt);
		
		}catch(Exception ex) {
			abuseReportInfo.setSts(updCnt);
			System.out.println(ex);
		}
		return abuseReportInfo;		
	}
	public static AbuseReportDTO fetchAbuseReportOptionsForFanClubPost(AbuseReportDTO abuseReportDTO) {

		abuseReportDTO.setSts(0);
		try {
			Map<Integer, AbuseReportInfo> abuseOptionMap = PollingSQLDaoUtil
					.fetchAbuseOptionsForFanClubPosts(abuseReportDTO.getAbuseType());
			List<Integer> abusedIds = PollingSQLDaoUtil.fetchAbuseOptionSelectedbyCustomerForFCPosts(
					abuseReportDTO.getAbuseType(), abuseReportDTO.getCuId(), abuseReportDTO.getSrcId(),abuseReportDTO.getCommentId());

			for (Integer id : abuseOptionMap.keySet()) {
				System.out.println("key: " + id);
				if (null != abusedIds && abusedIds.contains(id)) {
					abuseOptionMap.get(id).setIsAns(1);
				}
			}

			if (abuseOptionMap != null && abuseOptionMap.size() > 0) {
				abuseReportDTO.setSts(1);				
				List<AbuseReportInfo> listofAbuseOptions= new ArrayList<AbuseReportInfo>(abuseOptionMap.values());
				abuseReportDTO.setAbuseReporOptionList(listofAbuseOptions);
				
			} else {
				abuseReportDTO.setSts(0);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			abuseReportDTO.setSts(0);
		}
		return abuseReportDTO;
	}
	
	public static AbuseReportDTO fetchAbuseFanClubRelatedAbuseByActionType(AbuseReportDTO abuseReportDTO) {

		abuseReportDTO.setSts(0);
		try {
			Map<Integer, AbuseReportInfo> abuseOptionMap = PollingSQLDaoUtil
					.fetchAbuseOptionsForFanClubPosts(abuseReportDTO.getAbuseType());
			List<Integer> abusedIds = FanClubAbuseSQLDAO.fetchAbuseIdByActionType(abuseReportDTO.getAbuseType(), abuseReportDTO.getCuId(), abuseReportDTO.getSrcId());
			for (Integer id : abuseOptionMap.keySet()) {
				System.out.println("key: " + id);
				if (null != abusedIds && abusedIds.contains(id)) {
					abuseOptionMap.get(id).setIsAns(1);
					abuseReportDTO.setIsAllOptionsActive(false);
				}
			}

			if (abuseOptionMap != null && abuseOptionMap.size() > 0) {
				abuseReportDTO.setSts(1);				
				List<AbuseReportInfo> listofAbuseOptions= new ArrayList<AbuseReportInfo>(abuseOptionMap.values());
				abuseReportDTO.setAbuseReporOptionList(listofAbuseOptions);
				
			} else {
				abuseReportDTO.setSts(0);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			abuseReportDTO.setSts(0);
		}
		return abuseReportDTO;
	}
	
	
	public static AbuseReportInfo saveFanClubPostAbuseReport(AbuseReportInfo abuseReportInfo) {
		Integer updCnt = 0;
		abuseReportInfo.setSts(updCnt);
		try {
			
			if(abuseReportInfo.getCommentId() == null) abuseReportInfo.setCommentId(-1);
			updCnt = PollingSQLDaoUtil.saveFanClubPostAbuseReported(abuseReportInfo.getCuId() ,
				abuseReportInfo.getSrcId() ,abuseReportInfo.getAbuseType() , abuseReportInfo.getCommentId() , abuseReportInfo.getAbuseOptionId() );
			abuseReportInfo.setSts(updCnt);
		
		}catch(Exception ex) {
			abuseReportInfo.setSts(updCnt);
			System.out.println(ex);
		}
		return abuseReportInfo;		
	}
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		//AbuseReportDTO abuseReportInfo = new AbuseReportDTO();
		AbuseReportInfo abuseReportInfo = new AbuseReportInfo();
		abuseReportInfo.setAbuseType("COMMENT");
		abuseReportInfo.setSrcId(1);
		abuseReportInfo.setCuId(2);
		abuseReportInfo.setAbuseOptionId(4);
		//abuseReportInfo = fetchAbuseReportOptions(abuseReportInfo);
		saveAbuseReport(abuseReportInfo);
		System.out.println("Status is " + abuseReportInfo.getSts());		
	}

	public static AbuseReportInfo saveFanClubVideoAbuseReport(AbuseReportInfo abuseReportInfo) {
		Integer updCnt = 0;
		abuseReportInfo.setSts(updCnt);
		try {
			updCnt = PollingSQLDaoUtil.saveFanClubVideoAbuseReported(abuseReportInfo );
			abuseReportInfo.setSts(updCnt);
		
		}catch(Exception ex) {
			abuseReportInfo.setSts(updCnt);
			System.out.println(ex);
		}
		return abuseReportInfo;
		
	}

	public static AbuseReportInfo saveFanClubPostCommentsAbuseReport(AbuseReportInfo abuseReportInfo) {
		Integer updCnt = 0;
		abuseReportInfo.setSts(updCnt);
		try {
			updCnt = PollingSQLDaoUtil.saveFanClubPostCommentsAbuseReported(abuseReportInfo );
			abuseReportInfo.setSts(updCnt);
		
		}catch(Exception ex) {
			abuseReportInfo.setSts(updCnt);
			System.out.println(ex);
		}
		return abuseReportInfo;
		
	}

}
