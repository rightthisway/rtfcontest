package com.quiz.cassandra.service;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CustRewardValueInfo;
import com.quiz.cassandra.list.PollingVideoInfo;
import com.quiz.cassandra.list.RewardsInfo;
import com.quiz.cassandra.utils.PollingUtil;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.PollingSQLDaoUtil;
import com.zonesws.webservices.utils.SQLDaoUtil;
import com.zonesws.webservices.utils.URLUtil;

public class PollingMediaUploadService {
	
	public static PollingVideoInfo processMediaUpload(PollingVideoInfo pollingVideoInfo , CassCustomer customer)  throws Exception {		
		
		Integer custId = customer.getId();
		String isWinnerOrGeneral = pollingVideoInfo.getwTy();
		Integer isWinnerOrGeneralTy = 0;
		if(PollingUtil.AWS_WINNER_FLAG.equals(isWinnerOrGeneral))
		{
			isWinnerOrGeneralTy = 1;
		}
		String mediaFileName = pollingVideoInfo.getvUrl();
		String turl = pollingVideoInfo.gettUrl();		
		String catId = pollingVideoInfo.getCatId();
		String title = pollingVideoInfo.getTitle();
		String description = pollingVideoInfo.getDescription();
		
		try {
			Integer sts = updateMediaUploadStats(pollingVideoInfo , customer , isWinnerOrGeneralTy , mediaFileName,turl,catId,title,description);
			pollingVideoInfo.setSts(sts);
			
		}catch(Exception ex) {
			ex.printStackTrace();
			pollingVideoInfo.setSts(0);
			pollingVideoInfo.setMsg(URLUtil.genericErrorMsg); 
			
		}
		return pollingVideoInfo;
		
	}

	private static Integer updateMediaUploadStats(PollingVideoInfo pollingVideoInfo ,CassCustomer customer, Integer isWinnerOrGeneralTy, String mediaFileName,String turl,String catId,String title,String description) throws Exception {		
				
		Integer cnt = 0;
		Integer custVideoRefId = PollingSQLDaoUtil.insertCustomerMediaUploadStats(customer.getId() , isWinnerOrGeneralTy , mediaFileName,turl,catId,title,description);	
		if(custVideoRefId == null ) return cnt;
		
		cnt =  PollingSQLDaoUtil.insertRTFTvMedia(customer.getId(),mediaFileName,turl,catId,title,description, custVideoRefId);	
		if(cnt <= 0 ) return 0;
		
		try {				
			
		CustRewardValueInfo cro  = CustomerRewardLimtService.processFFOVideoUploadRewards( customer );
		
		Integer credStatus = 0;
		if (cro.getIsRewarded() == true) {
			Integer currentPoint = customer.getRtfPoints();
			if (currentPoint == null)
				currentPoint = 0;
			Integer gainedRtfPoints = cro.getRwdQty();
			if (gainedRtfPoints == null)
				gainedRtfPoints = 0;
			Integer cassFinalrtfPoints = currentPoint + gainedRtfPoints;
			credStatus = CustomerRewardCreditService.creditCustomerRTFPoints(customer.getId(), cassFinalrtfPoints,
					gainedRtfPoints);
			if (credStatus > 0) {
				String msg = PollingUtil.UPLOAD_FAN_FREAKOUT_VIDEO.replace("00", String.valueOf(gainedRtfPoints));
				Integer sts = 1;
				Integer pts = cassFinalrtfPoints;
				Double rDlrs = customer.getcRewardDbl();
				RewardsInfo rwdInfo = new RewardsInfo(msg, sts, pts, rDlrs);
				pollingVideoInfo.setRwdInfo(rwdInfo);
				SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR",
						SourceType.UPLOAD_FAN_FREAKOUT_VIDEO, custVideoRefId,
						gainedRtfPoints, 0, 0, 0, 0.0, " CUST VIDEO ID " );			
			}
		}			
			cnt = 1;
		}catch(Exception ex) {
			ex.printStackTrace();
			cnt = 0 ;
		}
			return cnt;
	}
	
	public static void main (String a []) throws Exception{
		PollingVideoInfo pollingVideoInfo  = new PollingVideoInfo();
		pollingVideoInfo.setCuId(2);
		pollingVideoInfo.setwTy("G");
		pollingVideoInfo.setvUrl("https://customermediaupload.s3.amazonaws.com/rtfmedia/video/415336_1573134066147.3gp");
		pollingVideoInfo.settUrl("https://customermediaupload.s3.amazonaws.com/rtfmedia/poster/415336_1576592974306.jpg");
		pollingVideoInfo.setCatId("35");
		pollingVideoInfo.setTitle("testtitle");
		pollingVideoInfo.setDescription("test desrciption ");
		CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(2);
		processMediaUpload(pollingVideoInfo , customer);
		System.out.println("iNSERT STATIUS IS --> " + pollingVideoInfo.getSts());
		
	}
	
	
	

}
